<?php //ouverture des pokémons
$_SESSION['changement']=1;
$_SESSION['grele']=0;
$_SESSION['zenith']=0;
$_SESSION['tempete_sable']=0;
$_SESSION['danse_pluie']=0;
$_SESSION['last_attaque']=0;
$_SESSION['last_attaque_adv']=0;
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_blind_battles WHERE id_battles=:id_battles AND pseudo!=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id_battles' => $_POST['id_match'], 'pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();//pokemon adverse
if(isset($donnees['id']))
	{
	$classe=$donnees['classe'];
	$_SESSION['id_pokemon_adv']=$donnees['id_pokemon'];
	$id_liste_pokemon_adv=$donnees['id_liste_pokemons'];
	$_SESSION['pseudo_adv']=$donnees['pseudo'];
	$adversaire=$donnees['pseudo'];
	$_SESSION['id_adv'] = $donnees['id'];
	$_SESSION['id_pokedex_adv'] = $donnees['id_pokedex'];
	$_SESSION['shiney_adv']= $donnees['shiney'];
	$_SESSION['sexe_adv']= $donnees['sexe'];
	$_SESSION['lvl_adv'] = $donnees['lvl'];
	$_SESSION['pv_adv'] = $donnees['pv_max'];
	$_SESSION['pv_max_adv'] = $donnees['pv_max'];
	$_SESSION['att_max_adv']=$donnees['att'];
	$_SESSION['def_max_adv']=$donnees['def'];
	$_SESSION['vit_max_adv']=$donnees['vit'];
	$_SESSION['attspe_max_adv']=$donnees['attspe'];
	$_SESSION['defspe_max_adv']=$donnees['defspe'];
	$_SESSION['att_adv']=$donnees['att'];
	$_SESSION['def_adv']=$donnees['def'];
	$_SESSION['vit_adv']=$donnees['vit'];
	$_SESSION['attspe_adv']=$donnees['attspe'];
	$_SESSION['defspe_adv']=$donnees['defspe'];
	$_SESSION['attaque1_nb_adv']=$donnees['attaque1'];
	$_SESSION['attaque2_nb_adv']=$donnees['attaque2'];
	$_SESSION['attaque3_nb_adv']=$donnees['attaque3'];
	$_SESSION['attaque4_nb_adv']=$donnees['attaque4'];	
	$_SESSION['bonheur_adv']=$donnees['bonheur'];
	$_SESSION['objet_adv']=$donnees['objet'];
	$_SESSION['esq_adv']=0;
	$_SESSION['pre_adv']=0;
	$_SESSION['statut_confus_adv']=-1;
	$_SESSION['statut_poison_adv']=0;
	$_SESSION['statut_poison_grave_adv']=0;
	$_SESSION['statut_gel_adv']=0;
	$_SESSION['statut_paralyse_adv']=0;
	$_SESSION['statut_brule_adv']=0;
	$_SESSION['statut_dodo_adv']=0;
	$_SESSION['recharge_adv']=0;
	$_SESSION['mur_lumiere_adv']=0;
	$_SESSION['protection_adv']=0;
	$_SESSION['rune_protect_adv']=0;
	$_SESSION['charge_adv']=0;
	$_SESSION['attraction_adv']=0;
	$_SESSION['statut_vampigraine_adv']=0;
	$_SESSION['danse_flamme_adv']=0;
	$_SESSION['ligotage_adv']=0;
	$_SESSION['esquive_adv']=0;
	$_SESSION['attaque_auto_adv']=0;
	$_SESSION['bonus_cc_adv']=0;
	$_SESSION['lance_boue_adv']=0;
	$_SESSION['tourniquet_adv']=0;
	$_SESSION['mania_adv']=0;
	$_SESSION['abri_adv']=100;
	$_SESSION['destin_adv']=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_SESSION['attaque1_nb_adv']));  
$donnees = $reponse->fetch();
	$_SESSION['nom_attaque1_adv'] = $donnees['nom']; $_SESSION['type_attaque1_adv'] = $donnees['type']; $_SESSION['puissance_attaque1_adv'] = $donnees['puissance'];
	$_SESSION['prec_attaque1_adv'] = $donnees['prec']; $_SESSION['cc_attaque1_adv'] = $donnees['cc']; $_SESSION['classe_attaque1_adv'] = $donnees['classe'];
	$_SESSION['priorite_attaque1_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque1_adv'] = $donnees['esquive']; $_SESSION['cible_attaque1_adv'] = $donnees['cible'];
	$_SESSION['id_effet_attaque1_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque1_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque1_adv'] = $donnees['proba'];
$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_SESSION['attaque2_nb_adv']));  
$donnees = $reponse->fetch();
	$_SESSION['nom_attaque2_adv'] = $donnees['nom']; $_SESSION['type_attaque2_adv'] = $donnees['type']; $_SESSION['puissance_attaque2_adv'] = $donnees['puissance'];
	$_SESSION['prec_attaque2_adv'] = $donnees['prec']; $_SESSION['cc_attaque2_adv'] = $donnees['cc']; $_SESSION['classe_attaque2_adv'] = $donnees['classe'];
	$_SESSION['priorite_attaque2_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque2_adv'] = $donnees['esquive']; $_SESSION['cible_attaque2_adv'] = $donnees['cible'];
	$_SESSION['id_effet_attaque2_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque2_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque2_adv'] = $donnees['proba'];
$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_SESSION['attaque3_nb_adv']));  
$donnees = $reponse->fetch();
	$_SESSION['nom_attaque3_adv'] = $donnees['nom']; $_SESSION['type_attaque3_adv'] = $donnees['type']; $_SESSION['puissance_attaque3_adv'] = $donnees['puissance'];
	$_SESSION['prec_attaque3_adv'] = $donnees['prec']; $_SESSION['cc_attaque3_adv'] = $donnees['cc']; $_SESSION['classe_attaque3_adv'] = $donnees['classe'];
	$_SESSION['priorite_attaque3_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque3_adv'] = $donnees['esquive']; $_SESSION['cible_attaque3_adv'] = $donnees['cible'];
	$_SESSION['id_effet_attaque3_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque3_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque3_adv'] = $donnees['proba'];
$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_SESSION['attaque4_nb_adv']));  
$donnees = $reponse->fetch();
	$_SESSION['nom_attaque4_adv'] = $donnees['nom']; $_SESSION['type_attaque4_adv'] = $donnees['type']; $_SESSION['puissance_attaque4_adv'] = $donnees['puissance'];
	$_SESSION['prec_attaque4_adv'] = $donnees['prec']; $_SESSION['cc_attaque4_adv'] = $donnees['cc']; $_SESSION['classe_attaque4_adv'] = $donnees['classe'];
	$_SESSION['priorite_attaque4_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque4_adv'] = $donnees['esquive']; $_SESSION['cible_attaque4_adv'] = $donnees['cible'];
	$_SESSION['id_effet_attaque4_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque4_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque4_adv'] = $donnees['proba'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_SESSION['id_pokemon_adv']));  
$donnees = $reponse->fetch();
	$_SESSION['id_pokedex_adv'] = $donnees['id_pokedex'];
	$_SESSION['nom_pokemon_adv'] = $donnees['nom'];
	$_SESSION['type_adv'] = $donnees['type1'];
	$_SESSION['type2_adv'] = $donnees['type2'];
	$_SESSION['tdc_adv']= $donnees['tdc'];
	$_SESSION['poids_adv']= $donnees['poids'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_blind_battles WHERE id_battles=:id_battles AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id_battles' => $_POST['id_match'], 'pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();//pokemon à soi
	$_SESSION['id_pokemon']=$donnees['id_pokemon'];
	$id_liste_pokemon=$donnees['id_liste_pokemons'];
	$_SESSION['lvl']=$donnees['lvl'];
	$_SESSION['shiney']=$donnees['shiney'];
	$_SESSION['sexe']= $donnees['sexe'];
	$_SESSION['pv']=$donnees['pv_max'];
	$_SESSION['pv_max']=$donnees['pv_max'];
	$_SESSION['att']=$donnees['att'];
	$_SESSION['def']=$donnees['def'];
	$_SESSION['vit']=$donnees['vit'];
	$_SESSION['attspe']=$donnees['attspe'];
	$_SESSION['defspe']=$donnees['defspe'];
	$_SESSION['att_max']=$donnees['att'];
	$_SESSION['def_max']=$donnees['def'];
	$_SESSION['vit_max']=$donnees['vit'];
	$_SESSION['attspe_max']=$donnees['attspe'];
	$_SESSION['defspe_max']=$donnees['defspe'];
	$_SESSION['attaque1_nb']=$donnees['attaque1'];
	$_SESSION['attaque2_nb']=$donnees['attaque2'];
	$_SESSION['attaque3_nb']=$donnees['attaque3'];
	$_SESSION['attaque4_nb']=$donnees['attaque4'];
	$_SESSION['bonheur']=$donnees['bonheur'];
	$_SESSION['objet']=$donnees['objet'];
	$_SESSION['esq']=0;
	$_SESSION['pre']=0;
	$_SESSION['statut_confus']=-1;
	$_SESSION['statut_poison']=0;
	$_SESSION['statut_poison_grave']=0;
	$_SESSION['statut_gel']=0;
	$_SESSION['statut_paralyse']=0;
	$_SESSION['statut_brule']=0;
	$_SESSION['statut_dodo']=0;
	$_SESSION['recharge']=0;
	$_SESSION['mur_lumiere']=0;
	$_SESSION['protection']=0;
	$_SESSION['rune_protect']=0;
	$_SESSION['charge']=0;
	$_SESSION['attraction']=0;
	$_SESSION['statut_vampigraine']=0;
	$_SESSION['danse_flamme']=0;
	$_SESSION['ligotage']=0;
	$_SESSION['esquive']=0;
	$_SESSION['attaque_auto']=0;
	$_SESSION['bonus_cc']=0;
	$_SESSION['lance_boue']=0;
	$_SESSION['tourniquet']=0;
	$_SESSION['mania']=0;
	$_SESSION['abri']=100;
	$_SESSION['destin']=0;
	$_SESSION['victoires']=$donnees['victoires'];
	$_SESSION['defaites']=$donnees['defaites'];
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['attaque1_nb']));  
	$donnees = $reponse->fetch();
		$_SESSION['nom_attaque1'] = $donnees['nom']; $_SESSION['type_attaque1'] = $donnees['type']; $_SESSION['puissance_attaque1'] = $donnees['puissance'];
		$_SESSION['prec_attaque1'] = $donnees['prec']; $_SESSION['cc_attaque1'] = $donnees['cc']; $_SESSION['classe_attaque1'] = $donnees['classe'];
		$_SESSION['priorite_attaque1'] = $donnees['priorite']; $_SESSION['esquive_attaque1'] = $donnees['esquive']; $_SESSION['cible_attaque1'] = $donnees['cible'];
		$_SESSION['id_effet_attaque1'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque1'] = $donnees['id_effet2']; $_SESSION['proba_attaque1'] = $donnees['proba'];
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['attaque2_nb']));  
	$donnees = $reponse->fetch();
		$_SESSION['nom_attaque2'] = $donnees['nom']; $_SESSION['type_attaque2'] = $donnees['type']; $_SESSION['puissance_attaque2'] = $donnees['puissance'];
		$_SESSION['prec_attaque2'] = $donnees['prec']; $_SESSION['cc_attaque2'] = $donnees['cc']; $_SESSION['classe_attaque2'] = $donnees['classe'];
		$_SESSION['priorite_attaque2'] = $donnees['priorite']; $_SESSION['esquive_attaque2'] = $donnees['esquive']; $_SESSION['cible_attaque2'] = $donnees['cible'];
		$_SESSION['id_effet_attaque2'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque2'] = $donnees['id_effet2']; $_SESSION['proba_attaque2'] = $donnees['proba'];
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['attaque3_nb']));  
	$donnees = $reponse->fetch();
		$_SESSION['nom_attaque3'] = $donnees['nom']; $_SESSION['type_attaque3'] = $donnees['type']; $_SESSION['puissance_attaque3'] = $donnees['puissance'];
		$_SESSION['prec_attaque3'] = $donnees['prec']; $_SESSION['cc_attaque3'] = $donnees['cc']; $_SESSION['classe_attaque3'] = $donnees['classe'];
		$_SESSION['priorite_attaque3'] = $donnees['priorite']; $_SESSION['esquive_attaque3'] = $donnees['esquive']; $_SESSION['cible_attaque3'] = $donnees['cible'];
		$_SESSION['id_effet_attaque3'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque3'] = $donnees['id_effet2']; $_SESSION['proba_attaque3'] = $donnees['proba'];
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['attaque4_nb']));  
	$donnees = $reponse->fetch();
		$_SESSION['nom_attaque4'] = $donnees['nom']; $_SESSION['type_attaque4'] = $donnees['type']; $_SESSION['puissance_attaque4'] = $donnees['puissance'];
		$_SESSION['prec_attaque4'] = $donnees['prec']; $_SESSION['cc_attaque4'] = $donnees['cc']; $_SESSION['classe_attaque4'] = $donnees['classe'];
		$_SESSION['priorite_attaque4'] = $donnees['priorite']; $_SESSION['esquive_attaque4'] = $donnees['esquive']; $_SESSION['cible_attaque4'] = $donnees['cible'];
		$_SESSION['id_effet_attaque4'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque4'] = $donnees['id_effet2']; $_SESSION['proba_attaque4'] = $donnees['proba'];
	$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['id_pokemon']));
	$donnees = $reponse->fetch();
		$_SESSION['nom_pokemon']=$donnees['nom'];	
		$_SESSION['id_pokedex'] = $donnees['id_pokedex'];
		$_SESSION['type'] = $donnees['type1'];
		$_SESSION['type2'] = $donnees['type2'];
		$_SESSION['poids']= $donnees['poids'];
?>


<?php //combat
$nb_tours=1;
$victoire=0;$defaite=0;
while($victoire==0 AND $defaite==0 AND $nb_tours<30)
	{ 
	$statut_confus_begin=0;$statut_confus_begin_adv=0;
	//choix de l'attaque adv
		if($_SESSION['attaque_auto_adv']==1)
			{$attaque_lancee_adv=$_SESSION['id_attaque_adv'];$_SESSION['attaque_auto_adv']=0;}
		else
			{
			$attaque_lancee_adv=0;
			while($attaque_lancee_adv==0)
				{
				$n_att_adv=rand(1,4);
				if($n_att_adv==1){$attaque_lancee_adv=$_SESSION['attaque1_nb_adv'];} 
				if($n_att_adv==2){$attaque_lancee_adv=$_SESSION['attaque2_nb_adv'];} 		
				if($n_att_adv==3){$attaque_lancee_adv=$_SESSION['attaque3_nb_adv'];} 
				if($n_att_adv==4){$attaque_lancee_adv=$_SESSION['attaque4_nb_adv'];} 
				}
			}
		//carac de l'attaque
		$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' =>$attaque_lancee_adv)); 
		$donnees = $reponse->fetch();
		$_SESSION['id_attaque_adv']=$donnees['id'];$_SESSION['nom_attaque_adv'] = $donnees['nom']; $_SESSION['type_attaque_adv'] = $donnees['type']; $_SESSION['puissance_attaque_adv'] = $donnees['puissance'];
		$_SESSION['prec_attaque_adv'] = $donnees['prec']; $_SESSION['cc_attaque_adv'] = $donnees['cc']; $_SESSION['classe_attaque_adv'] = $donnees['classe'];
		$_SESSION['priorite_attaque_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque_adv'] = $donnees['esquive']; $_SESSION['cible_attaque_adv'] = $donnees['cible'];
		$_SESSION['id_effet_attaque_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque_adv'] = $donnees['proba'];
		if($_SESSION['nom_attaque']=="abri"){$_SESSION['prec_attaque']=$_SESSION['abri'];$_SESSION['abri']=$_SESSION['abri']/2;}
			//balayage
		if($_SESSION['id_effet_attaque_adv']==98 OR $_SESSION['id_effet2_attaque_adv']==98)
			{
			if($_SESSION['poids']>200){$_SESSION['puissance_attaque_adv']=120;}
			elseif($_SESSION['poids']>100){$_SESSION['puissance_attaque_adv']=100;}
			elseif($_SESSION['poids']>50){$_SESSION['puissance_attaque_adv']=80;}
			elseif($_SESSION['poids']>25){$_SESSION['puissance_attaque_adv']=60;}
			elseif($_SESSION['poids']>10){$_SESSION['puissance_attaque_adv']=40;}
			else{$_SESSION['puissance_attaque_adv']=20;}
			}
		//cadeau
		if($_SESSION['id_effet_attaque_adv']==99 OR $_SESSION['id_effet2_attaque_adv']==99)
			{
			$_SESSION['id_attaque_adv']=$donnees['id'];$_SESSION['nom_attaque_adv'] = $donnees['nom']; $_SESSION['type_attaque_adv'] = $donnees['type']; 
			$_SESSION['prec_attaque_adv'] = $donnees['prec'];  $_SESSION['classe_attaque_adv'] = $donnees['classe'];
			$_SESSION['priorite_attaque_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque_adv'] = $donnees['esquive']; $_SESSION['cible_attaque_adv'] = $donnees['cible'];
			$_SESSION['id_effet_attaque_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque_adv'] = $donnees['proba'];
			$rand_cadeau=rand(1,10);
			if($rand_cadeau<=4){$_SESSION['puissance_attaque_adv'] = 40;$_SESSION['cc_attaque_adv']=1;}
			elseif($rand_cadeau<=7){$_SESSION['puissance_attaque_adv'] = 80;$_SESSION['cc_attaque_adv']=1;}
			elseif($rand_cadeau<=8){$_SESSION['puissance_attaque_adv'] = 120;$_SESSION['cc_attaque_adv']=1;}
			else
				{
				$_SESSION['puissance_attaque_adv'] = 0;$_SESSION['cc_attaque_adv']=1;
				$_SESSION['pv']=$_SESSION['pv']+80;if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}
				}
			}

			
		//choix de l'attaque alliée
		if($_SESSION['attaque_auto']==1)
			{$attaque_lancee=$_SESSION['id_attaque'];$_SESSION['attaque_auto']=0;}
		else
			{
			$attaque_lancee=0;
			while($attaque_lancee==0)
				{
				$n_att=rand(1,4);
				if($n_att==1){$attaque_lancee=$_SESSION['attaque1_nb'];} 
				if($n_att==2){$attaque_lancee=$_SESSION['attaque2_nb'];} 		
				if($n_att==3){$attaque_lancee=$_SESSION['attaque3_nb'];} 
				if($n_att==4){$attaque_lancee=$_SESSION['attaque4_nb'];} 
				}
			}
		//carac de l'attaque
		$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' =>$attaque_lancee)); 
		$donnees = $reponse->fetch();
		$_SESSION['id_attaque']=$donnees['id'];$_SESSION['nom_attaque'] = $donnees['nom']; $_SESSION['type_attaque'] = $donnees['type']; $_SESSION['puissance_attaque'] = $donnees['puissance'];
		$_SESSION['prec_attaque'] = $donnees['prec']; $_SESSION['cc_attaque'] = $donnees['cc']; $_SESSION['classe_attaque'] = $donnees['classe'];
		$_SESSION['priorite_attaque'] = $donnees['priorite']; $_SESSION['esquive_attaque'] = $donnees['esquive']; $_SESSION['cible_attaque'] = $donnees['cible'];
		$_SESSION['id_effet_attaque'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque'] = $donnees['id_effet2']; $_SESSION['proba_attaque'] = $donnees['proba'];
		if($_SESSION['nom_attaque']=="abri"){$_SESSION['prec_attaque']=$_SESSION['abri'];$_SESSION['abri']=$_SESSION['abri']/2;}
			//balayage
		if($_SESSION['id_effet_attaque']==98 OR $_SESSION['id_effet2_attaque']==98)
			{
			if($_SESSION['poids_adv']>200){$_SESSION['puissance_attaque']=120;}
			elseif($_SESSION['poids_adv']>100){$_SESSION['puissance_attaque']=100;}
			elseif($_SESSION['poids_adv']>50){$_SESSION['puissance_attaque']=80;}
			elseif($_SESSION['poids_adv']>25){$_SESSION['puissance_attaque']=60;}
			elseif($_SESSION['poids_adv']>10){$_SESSION['puissance_attaque']=40;}
			else{$_SESSION['puissance_attaque']=20;}
			}
		//cadeau
		if($_SESSION['id_effet_attaque']==99 OR $_SESSION['id_effet2_attaque']==99)
			{
			$_SESSION['id_attaque']=$donnees['id'];$_SESSION['nom_attaque'] = $donnees['nom']; $_SESSION['type_attaque'] = $donnees['type']; 
			$_SESSION['prec_attaque'] = $donnees['prec'];  $_SESSION['classe_attaque'] = $donnees['classe'];
			$_SESSION['priorite_attaque'] = $donnees['priorite']; $_SESSION['esquive_attaque'] = $donnees['esquive']; $_SESSION['cible_attaque'] = $donnees['cible'];
			$_SESSION['id_effet_attaque'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque'] = $donnees['id_effet2']; $_SESSION['proba_attaque'] = $donnees['proba'];
			$rand_cadeau=rand(1,10);
			if($rand_cadeau<=4){$_SESSION['puissance_attaque'] = 40;$_SESSION['cc_attaque']=1;}
			elseif($rand_cadeau<=7){$_SESSION['puissance_attaque'] = 80;$_SESSION['cc_attaque']=1;}
			elseif($rand_cadeau<=8){$_SESSION['puissance_attaque'] = 120;$_SESSION['cc_attaque']=1;}
			else
				{
				$_SESSION['puissance_attaque'] = 0;$_SESSION['cc_attaque']=1;
				$_SESSION['pv_adv']=$_SESSION['pv_adv']+80;if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}
				}
			}

			
		//dégats avant attaques
	//DESC 0
	if($_SESSION['grele']>5)
	{
	if($_SESSION['type'] !="glace" AND $_SESSION['type']!="glace"){$desc_0_1=$_SESSION['nom_pokemon'].' subit des dégats à cause de la grêle <br />';$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);if($_SESSION['pv']<0){$_SESSION['pv']=0;}}
	if($_SESSION['type_adv'] !="glace" AND $_SESSION['type_adv']!="glace"){$desc_0_2=$_SESSION['nom_pokemon_adv'].' subit des dégats à cause de la grêle <br />';$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}}
	$_SESSION['grele']=$_SESSION['grele']-1;
	if($_SESSION['grele']==0){$desc_0_3='Il a cessé de grêler <br />';}
	}
	if($_SESSION['tempete_sable']>0)
	{
	if($_SESSION['type'] !="sol" AND $_SESSION['type2']!="sol" AND $_SESSION['type'] !="roche" AND $_SESSION['type2']!="roche" AND $_SESSION['type'] !="acier" AND $_SESSION['type2']!="acier"){$desc_0_1=$_SESSION['nom_pokemon'].' subit des dégats à cause de la tempête de sable <br />';$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
	if($_SESSION['type_adv'] !="sol" AND $_SESSION['type2_adv']!="sol" AND $_SESSION['type_adv'] !="roche" AND $_SESSION['type2_adv']!="roche" AND $_SESSION['type_adv'] !="acier" AND $_SESSION['type2_adv']!="acier"){$desc_0_2=$_SESSION['nom_pokemon_adv'].' subit des dégats à cause de la tempête de sable <br />';$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
	$_SESSION['tempete_sable']=$_SESSION['tempete_sable']-1;
	if($_SESSION['tempete_sable']==0){$desc_0_3= 'La tempête de sable s\'est arretée <br />';}
	}
	if($_SESSION['zenith']>0)
	{
	$_SESSION['zenith']=$_SESSION['zenith']-1;
	if($_SESSION['zenith']==0){$desc_0_3= 'Le soleil cesse de briller <br />';}
	}
	if($_SESSION['danse_pluie']>0)
	{
	$_SESSION['danse_pluie']=$_SESSION['danse_pluie']-1;
	if($_SESSION['danse_pluie']==0){$desc_0_3= 'La pluie s\'est arretée <br />';}
	}
	$desc_0=$desc_0_1.''.$desc_0_2.''.$desc_0_3;
	$req = $bdd->prepare('INSERT INTO pokemons_description_blind_battles (id_battles, n_tour, desc_0) VALUES(:id_battles, :n_tour, :desc_0)') or die(print_r($bdd->errorInfo()));
								$req->execute(array(
										'id_battles' => $_POST['id_match'], 
										'n_tour' => $nb_tours,
										'desc_0' => $desc_0
										))or die(print_r($bdd->errorInfo()));
	//DEBUT DES HOSTILITES	
	if($_SESSION['priorite_attaque_adv']==$_SESSION['priorite_attaque']){if($_SESSION['vit'] >= $_SESSION['vit_adv']) {$premier_coup=1;} else {$premier_coup=-1;}}
	elseif($_SESSION['priorite_attaque_adv']>$_SESSION['priorite_attaque']){$premier_coup=-1;}
	elseif($_SESSION['priorite_attaque_adv']<$_SESSION['priorite_attaque']){$premier_coup=1;}
	elseif($_SESSION['vit'] >= $_SESSION['vit_adv']) {$premier_coup=1;} else {$premier_coup=-1;}
	//if($_SESSION['priorite_attaque_adv']==1 AND $_SESSION['priorite_attaque']==1){if($_SESSION['vit'] >= $_SESSION['vit_adv']) {$premier_coup=1;} else {$premier_coup=-1;}}
	//elseif($_SESSION['priorite_attaque_adv']==1){$premier_coup=-1;}
	//elseif($_SESSION['priorite_attaque']==1){$premier_coup=1;}
	//elseif($_SESSION['vit'] >= $_SESSION['vit_adv']) {$premier_coup=1;} else {$premier_coup=-1;}
	$attaque_donne=0; $ordre_attaque_adv=0; $ordre_attaque=0;
	while($premier_coup<2)
		{
		if($premier_coup==-1)//attaque du pokémon sauvage
			{
			$_SESSION['destin_adv']=0;
			if($attaque_donne==0){$ordre_attaque_adv=1;}if($attaque_donne==1){$ordre_attaque_adv=2;}
			$echec_attaque_adv=0;
			$degel_adv=0;$reveil_adv=0;$fuite_adv=0;$recharge_faite_adv=0;$statut_dodo_adv=0;$proba_confus_adv=0;$statut_gel_adv=0;$proba_paralyse_adv=0;$proba_attraction=0;$charge_faite_adv=0;$sangsue_adv=0;$triplattaque_brule_adv=0;$triplattaque_paralyse_adv=0;$triplattaque_gel_adv=0;$statut_poison_adv=0;$statut_poison_grave_adv=0;$statut_brule_adv=0;$danse_flamme_adv=0;$ligotage_adv=0;$statut_vampigraine_adv=0;
			//78 métronome
			if($_SESSION['id_effet_attaque_adv']==78 OR $_SESSION['id_effet2_attaque_adv']==78) 
				{
				while($proba_metronome==0 OR $proba_metronome==17 OR $proba_metronome==40 OR $proba_metronome==45 OR $proba_metronome==84 OR $proba_metronome==140){$proba_metronome=rand(16,251);}
				$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id' =>$proba_metronome));
				$donnees = $reponse->fetch();
				$id_attaque_adv=$proba_metronome;
				$_SESSION['nom_attaque_adv'] = $donnees['nom']; $_SESSION['type_attaque_adv'] = $donnees['type']; $_SESSION['puissance_attaque_adv'] = $donnees['puissance'];
				$_SESSION['prec_attaque_adv'] = $donnees['prec']; $_SESSION['cc_attaque_adv'] = $donnees['cc']; $_SESSION['classe_attaque_adv'] = $donnees['classe'];
				$_SESSION['priorite_attaque_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque_adv'] = $donnees['esquive']; $_SESSION['cible_attaque_adv'] = $donnees['cible'];
				$_SESSION['id_effet_attaque_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque_adv'] = $donnees['proba'];
				}			
			//110 blabla dodo
                        if($_SESSION['id_effet_attaque_adv']==110 OR $_SESSION['id_effet2_attaque_adv']==110) 
                                {
                                $blabla_dodo_ok=0;$test_blabla_dodo=0;
                                while($blabla_dodo_ok==0 AND $test_blabla_dodo<10){
                                    $rand_blabladodo=rand(1,4);
                                    if($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque1_nb_adv'];}
                                    elseif($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque2_nb_adv'];}
                                    elseif($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque3_nb_adv'];}
                                    elseif($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque4_nb_adv'];}
                                    $test_blabla_dodo++;
                                    if($proba_blabla_dodo!=45 AND $proba_blabla_dodo!=342 AND $proba_blabla_dodo!=17 AND $proba_blabla_dodo!=21 AND $proba_blabla_dodo!=140 AND $proba_blabla_dodo!=84 AND $proba_blabla_dodo!=312 AND $proba_blabla_dodo!=40 AND $proba_blabla_dodo!=325){$blabla_dodo_ok=1;}
                                }
                                $echec_blabla_dodo=0;
                                if($blabla_dodo_ok==0){$echec_blabla_dodo=1;}
                                $reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
                                $reponse->execute(array('id' =>$proba_blabla_dodo));
                                $donnees = $reponse->fetch();
                                $attaque_lancee_adv=$proba_metronome;
                                $_SESSION['nom_attaque_adv'] = $donnees['nom']; $_SESSION['type_attaque_adv'] = $donnees['type']; $_SESSION['puissance_attaque_adv'] = $donnees['puissance'];
                                $_SESSION['prec_attaque_adv'] = $donnees['prec']; $_SESSION['cc_attaque_adv'] = $donnees['cc']; $_SESSION['classe_attaque_adv'] = $donnees['classe'];
                                $_SESSION['priorite_attaque_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque_adv'] = $donnees['esquive']; $_SESSION['cible_attaque_adv'] = $donnees['cible'];
                                $_SESSION['id_effet_attaque_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque_adv'] = $donnees['proba'];
                                }   
			//EFFET des CLIMATS sur les attaques
			$soins_adv=0.5;
			if($_SESSION['danse_pluie']>0) 
				{
				if($_SESSION['type_attaque_adv']=="feu"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*0.5;} 
				if($_SESSION['type_attaque_adv']=="eau"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*1.5;} 
				if($_SESSION['id_attaque_adv']==40){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']/2;}//lance soleil
				if($_SESSION['id_attaque_adv']==43){$_SESSION['prec_attaque_adv']=100;}//fatal-foudre
				if($_SESSION['id_attaque_adv']==68 OR $_SESSION['id_attaque_adv']==141){$soins_adv=0.25;}//synthèse et rayon lune
				//vent violent et aurore pas encore implémenté
				}
			if($_SESSION['zenith']>0) 
				{
				if($_SESSION['type_attaque_adv']=="feu"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*1.5;} 
				if($_SESSION['type_attaque_adv']=="eau"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*0.5;} 
				//lance soleil en un tour dans "charge"
				if($_SESSION['id_attaque_adv']==68 OR $_SESSION['id_attaque_adv']==141){$soins_adv=0.66;}//synthèse et rayon lune
				}	
			if($_SESSION['tempete_sable']>0) 
				{
				if($_SESSION['id_attaque_adv']==40){$_SESSION['puissance_attaque_adv']=60;}//lance soleil
				if($_SESSION['id_attaque_adv']==68 OR $_SESSION['id_attaque_adv']==141){$soins_adv=0.25;}//synthèse et rayon lune
				//aurore pas encore implémenté
				}	
			if($_SESSION['grele']>0) 
				{
				if($_SESSION['id_attaque_adv']==33){$_SESSION['prec_attaque_adv']=100;}//blizarre
				if($_SESSION['id_attaque_adv']==40){$_SESSION['puissance_attaque_adv']=60;}//lance soleil
				if($_SESSION['id_attaque_adv']==68 OR $_SESSION['id_attaque_adv']==141){$soins_adv=0.25;}//synthèse et rayon lune
				//aurore pas encore implémenté
				}	
			
			
			$proba_effet_adv=rand(1,100);
			if($_SESSION['proba_attaque_adv']>=$proba_effet_adv){$effet_adv=1;} else {$effet_adv=0;}

			//ECHEC AUTOMATIQUE
			//cage éclair sur roche
			if($attaque_lancee_adv==121 AND $_SESSION['type']=="roche" OR $attaque_lancee_adv==121 AND $_SESSION['type2']=="roche" OR $attaque_lancee_adv==121 AND $_SESSION['type']=="sol" OR $attaque_lancee_adv==121 AND $_SESSION['type2']=="sol"){$effet_adv=0;}
			//guillotine et empla'korn sur spectre
			if($attaque_lancee_adv==216 AND $_SESSION['type']=="spectre" OR $attaque_lancee_adv==216 AND $_SESSION['type2']=="spectre" OR $attaque_lancee_adv==133 AND $_SESSION['type']=="spectre" OR $attaque_lancee_adv==133 AND $_SESSION['type2']=="spectre"){$effet_adv=0;}
			//abime sur vol
			if($attaque_lancee_adv==157 AND $_SESSION['type']=="vol" OR $attaque_lancee_adv==157 AND $_SESSION['type2']=="vol"){$effet_adv=0;}
		
		
			//calcul du multiplicateur
				$reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('type' => $_SESSION['type_attaque_adv'], 'sur' => $_SESSION['type']));  
				$donnees = $reponse->fetch();
				$multiplicateur1=$donnees['effet'];
				if($_SESSION['type2']!="0")
				{$reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('type' => $_SESSION['type_attaque_adv'], 'sur' => $_SESSION['type2']));  
				$donnees = $reponse->fetch();
				$multiplicateur2=$donnees['effet'];}
				else {$multiplicateur2=1;}
				$multiplicateur_adv=$multiplicateur1*$multiplicateur2;
				
			if($multiplicateur_adv==0 AND $_SESSION['cible_attaque_adv']==1 AND $_SESSION['puissance_attaque_adv']>0){$effet_adv=0;}
			//verification que touche
			$hasard_precision_adv=rand(1,100);
			$hasard_precision_adv=$hasard_precision_adv + $_SESSION['esq']-$_SESSION['pre_adv'];
			if($echec_blabla_dodo==1){$hasard_precision_adv=1000;}
                                //diminution tour
					//confusion
					if($_SESSION['statut_confus_adv']>0){$_SESSION['statut_confus_adv']=$_SESSION['statut_confus_adv']-1;}
					if($_SESSION['statut_confus_adv']==0){$statut_confus_end_adv=1;$_SESSION['statut_confus_adv']=$_SESSION['statut_confus_adv']-1;}
					//rune protect
					if($_SESSION['rune_protect_adv']>0){$_SESSION['rune_protect_adv']=$_SESSION['rune_protect_adv']-1;}
					//danse flamme
					if($_SESSION['danse_flamme_adv']>0){$_SESSION['danse_flamme_adv']=$_SESSION['danse_flamme_adv']-1;}
					//ligotage
					if($_SESSION['ligotage_adv']>0){$_SESSION['ligotage_adv']=$_SESSION['ligotage_adv']-1;}
					//gel
					if($_SESSION['statut_gel_adv']==1){$proba_fin_gel=rand(1,10); if($proba_fin_gel==1){$_SESSION['statut_gel_adv']=0; $degel_adv=1;}}
					//dodo
					if($_SESSION['statut_dodo_adv']==1){$_SESSION['fin_dodo_adv']=$_SESSION['fin_dodo_adv']-1;	if($_SESSION['fin_dodo_adv']==0){$_SESSION['statut_dodo_adv']=0; $reveil_adv=1;}}
					//mur lumière
					if($_SESSION['mur_lumiere_adv']>0){$_SESSION['mur_lumiere_adv']=$_SESSION['mur_lumiere_adv']-1;}
					//protection
					if($_SESSION['protection_adv']>0){$_SESSION['protection_adv']=$_SESSION['protection_adv']-1;}
			$proba_paralyse_adv=rand(1,100);
			$proba_attraction=rand(1,2); 
			$proba_confus_adv=rand(1,100);
			if($_SESSION['recharge_adv']==1)	
				//souffre de recharge (48)
				{
				$_SESSION['recharge_adv']=0; $recharge_faite_adv=1;$hasard_precision_adv=1000;
				$_SESSION['charge_adv']=0;
				}
			elseif($_SESSION['statut_dodo_adv']==1)
				//souffre du sommeil(66/69)
				{
				$statut_dodo_adv=1;$hasard_precision_adv=1000;
				$_SESSION['charge_adv']=0;
				}
			elseif($_SESSION['statut_gel_adv']==1)	
				//souffre du gel (47)
				{
				if($_SESSION['id_attaque_adv']!=331)
                                    {
                                    $statut_gel_adv=1;
                                    $hasard_precision_adv=1000;
                                    $_SESSION['charge_adv']=0;
                                    }else
                                    {
                                    $_SESSION['statut_gel_adv']=0;
                                    $degel_adv=1;
                                    }
				}
			elseif($_SESSION['statut_paralyse_adv']==1 AND $proba_paralyse_adv>75)
				//souffre de paralysie(55)
				{
				$hasard_precision_adv=1000;
				$_SESSION['charge_adv']=0;
				}
			elseif($peur_adv==1)
				//souffre de peur (71)
				{
				$hasard_precision_adv=1000;
				$_SESSION['charge_adv']=0;
				}
			elseif($_SESSION['attraction']==1 AND $proba_attraction==1)
				//souffre de attraction (67)
				{
				$_SESSION['charge_adv']=0;
				$hasard_precision_adv=1000;
				}
			elseif($_SESSION['statut_confus_adv']>0 AND $proba_confus_adv>50)
				{
				//souffre de confusion (39)
				$hasard_precision_adv=1000;
				$_SESSION['charge_adv']=0;
				$statut_confus_adv=1;
				$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/8); if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
				}
			else
				//54 charge
				{
				if($_SESSION['id_effet_attaque_adv']==54 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==54 AND $effet_adv==1) {if($_SESSION['charge_adv']==0 AND $_SESSION['zenith']==0 OR $_SESSION['zenith']>0 AND $_SESSION['charge_adv']==0 AND $_SESSION['id_attaque_adv']!=40){$_SESSION['cible_attaque_adv']=0;$_SESSION['charge_adv']=1;$charge_faite_adv=1;$hasard_precision_adv=1000;$_SESSION['attaque_auto_adv']=1;} else {$_SESSION['charge_adv']=0;}}
				}
				//87 si dodo
			if($_SESSION['id_effet_attaque_adv']==87 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==87 AND $effet_adv==1) {if($_SESSION['statut_dodo']!=1){$hasard_precision_adv=200;$_SESSION['charge_adv']=0;}}
				
				// vol et tunnel au tour 1 non offenssif
			if($_SESSION['esquive_attaque_adv']==1 AND $_SESSION['charge_adv']==0){$_SESSION['cible_attaque_adv']=0;}	
				
				//esquive (abri)
			if($_SESSION['esquive']==1){if($_SESSION['cible_attaque_adv']==1 AND $_SESSION['id_attaque_adv']!=44 OR $_SESSION['cible_attaque_adv']==1 AND $_SESSION['id_attaque']!=45){$hasard_precision_adv=1500;$_SESSION['charge_adv']=0;}$_SESSION['esquive']=0;}	
			if($hasard_precision_adv<=$_SESSION['prec_attaque_adv'])
				{
				//effets avant attaque
					//esquive (vol, tunnel)
				$_SESSION['last_attaque_adv']=$attaque_lancee_adv;
				if($_SESSION['esquive_attaque_adv']==1){if($_SESSION['charge_adv']==0){$_SESSION['charge_adv']=1;$charge_faite_adv=1;$hasard_precision_adv=1000;$_SESSION['esquive_adv']=1;$_SESSION['attaque_auto_adv']=1;$_SESSION['puissance_attaque_adv']=0;} else {$_SESSION['charge_adv']=0;}}
					//38 echoue
				if($_SESSION['id_effet_attaque_adv']==38 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==38 AND $effet_adv==1) {if($degats > 0) {$_SESSION['puissance_attaque_adv']=0;$echec_attaque_adv=1;}}
					//44 rafale
				$rafale_adv=1;
				if($_SESSION['id_effet_attaque_adv']==44 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==44 AND $effet_adv==1) {$proba_rafale_adv=rand(1,100); if($proba_rafale_adv<38){$rafale_adv=2;} elseif($proba_rafale_adv<=75){$rafale_adv=3;} elseif($proba_rafale_adv<=87){$rafale_adv=4;}else{$rafale_adv=5;}}
					//45 puissance cachée
				if($_SESSION['id_effet_attaque_adv']==45 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==45 AND $effet_adv==1) {$proba_type_attaque_adv=rand(1,17); $reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE id=:id') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id' => $proba_type_attaque_adv)); $donnees = $reponse->fetch();$_SESSION['type_attaque_adv'] = $donnees['sur']; $_SESSION['puissance_attaque_adv'] = rand(1,100); }
					//46 zénith
				if($_SESSION['id_effet_attaque_adv']==46 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==46 AND $effet_adv==1) {$_SESSION['zenith']=5;$_SESSION['grele']=0;$_SESSION['tempete_sable']=0;$_SESSION['danse_pluie']=0;}
				if($_SESSION['zenith']>0) {if($_SESSION['type_attaque_adv']=="feu"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*1.5;} if($_SESSION['type_attaque_adv']=="eau"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*0.5;} }			
					//43 grêle
				if($_SESSION['id_effet_attaque_adv']==43 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==43 AND $effet_adv==1) {$_SESSION['grele']=5;$_SESSION['zenith']=0;$_SESSION['tempete_sable']=0;$_SESSION['danse_pluie']=0;}
					//51 danse pluie
				if($_SESSION['id_effet_attaque_adv']==51 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==51 AND $effet_adv==1) {$_SESSION['danse_pluie']=5;$_SESSION['grele']=0;$_SESSION['zenith']=0;$_SESSION['tempete_sable']=0;}		
				if($_SESSION['danse_pluie']>0) {if($_SESSION['type_attaque_adv']=="feu"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*0.5;} if($_SESSION['type_attaque_adv']=="eau"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*1.5;} }
					//103 tempete de sable
				if($_SESSION['id_effet_attaque_adv']==103 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==103 AND $effet_adv==1) {$_SESSION['tempete_sable']=5;$_SESSION['grele']=0;$_SESSION['zenith']=0;$_SESSION['danse_pluie']=0;}						
					//104 ball'meteo
				if($_SESSION['id_effet_attaque_adv']==104 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==104 AND $effet_adv==1) 
					{
					if($_SESSION['danse_pluie']>0){$_SESSION['puissance_attaque_adv']=100;$_SESSION['type_attaque_adv']='eau';}
					elseif($_SESSION['zenith']>0){$_SESSION['puissance_attaque_adv']=100;$_SESSION['type_attaque_adv']='feu';}
					elseif($_SESSION['grele']>0){$_SESSION['puissance_attaque_adv']=100;$_SESSION['type_attaque_adv']='glace';}
					elseif($_SESSION['tempete_sable']>0){$_SESSION['puissance_attaque_adv']=100;$_SESSION['type_attaque_adv']='roche';}
					//calcul du multiplicateur
					$reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('type' => $_SESSION['type_attaque_adv'], 'sur' => $_SESSION['type']));  
					$donnees = $reponse->fetch();
					$multiplicateur1=$donnees['effet'];
					if($_SESSION['type2']!="0")
					{$reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('type' => $_SESSION['type_attaque_adv'], 'sur' => $_SESSION['type2']));  
					$donnees = $reponse->fetch();
					$multiplicateur2=$donnees['effet'];}
					else {$multiplicateur2=1;}
					$multiplicateur_adv=$multiplicateur1*$multiplicateur2;
					}
                                        //115 force cachée
                                if($_SESSION['id_effet_attaque_adv']==115 AND $effet_adv==1 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==115 AND $effet_adv==1 AND $_SESSION['statut_brule']==0 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}
                                if($_SESSION['id_effet_attaque_adv']==115 AND $effet_adv==1 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==115 AND $effet_adv==1 AND $_SESSION['rune_protect']==0) 
                                    {
                                    if($_SESSION['danse_pluie']>0){$_SESSION['id_effet_attaque_adv']=2;}
                                    elseif($_SESSION['zenith']>0){$_SESSION['id_effet_attaque_adv']=71;}
                                    elseif($_SESSION['grele']>0){$_SESSION['id_effet_attaque_adv']=47;}
                                    elseif($_SESSION['tempete_sable']>0){$_SESSION['id_effet_attaque_adv']=35;}
                                    else{$_SESSION['id_effet_attaque_adv']=55;}
                                    }
				//48 recharge
				if($_SESSION['id_effet_attaque_adv']==48 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==48 AND $effet_adv==1) {$_SESSION['recharge_adv']=1; }
					//49 mur lumière
				if($_SESSION['id_effet_attaque_adv']==49 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==49 AND $effet_adv==1) {$_SESSION['mur_lumiere_adv']=5; }
					//50 abri
				if($_SESSION['id_effet_attaque_adv']==50 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==50 AND $effet_adv==1) {$abri_adv=1; $_SESSION['esquive_adv']=1;}
					//125 tenacite
                                $tenacite_adv=0;
                                if($_SESSION['id_effet_attaque_adv']==125 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==125 AND $effet_adv==1) {$tenacite_adv=1;}	
                                //53 rune protect
				if($_SESSION['id_effet_attaque_adv']==53 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==53 AND $effet_adv==1) {$_SESSION['rune_protect_adv']=6;}
					//57 casse brique
				if($_SESSION['id_effet_attaque_adv']==57 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==57 AND $effet_adv==1) {$_SESSION['mur_lumiere']=0;$_SESSION['protection']=0;}
					//58 esq +
				if($_SESSION['id_effet_attaque_adv']==58 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==58 AND $effet_adv==1) {$_SESSION['esq_adv']=$_SESSION['esq_adv']+5;if($_SESSION['esq_adv']>30){$_SESSION['esq_adv']=30;}if($_SESSION['esq_adv']<-30){$_SESSION['esq_adv']=-30;}}
					//59 esq ++
				if($_SESSION['id_effet_attaque_adv']==59 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==59 AND $effet_adv==1) {$_SESSION['esq_adv']=$_SESSION['esq_adv']+10;if($_SESSION['esq_adv']>30){$_SESSION['esq_adv']=30;}if($_SESSION['esq_adv']<-30){$_SESSION['esq_adv']=-30;}}
					//60 esq +++
				if($_SESSION['id_effet_attaque_adv']==60 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==60 AND $effet_adv==1) {$_SESSION['esq_adv']=$_SESSION['esq_adv']+15;if($_SESSION['esq_adv']>30){$_SESSION['esq_adv']=30;}if($_SESSION['esq_adv']<-30){$_SESSION['esq_adv']=-30;}}
					//61 esq -
				if($_SESSION['id_effet_attaque_adv']==61 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==61 AND $effet_adv==1) {$_SESSION['esq']=$_SESSION['esq']-5;if($_SESSION['esq']>30){$_SESSION['esq']=30;}if($_SESSION['esq']<-30){$_SESSION['esq']=-30;}}
					//62 esq --
				if($_SESSION['id_effet_attaque_adv']==62 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==62 AND $effet_adv==1) {$_SESSION['esq']=$_SESSION['esq']-10;if($_SESSION['esq']>30){$_SESSION['esq']=30;}if($_SESSION['esq']<-30){$_SESSION['esq']=-30;}}
					//63 esq ---
				if($_SESSION['id_effet_attaque_adv']==63 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==63 AND $effet_adv==1) {$_SESSION['esq']=$_SESSION['esq']-15;if($_SESSION['esq']>30){$_SESSION['esq']=30;}if($_SESSION['esq']<-30){$_SESSION['esq']=-30;}}
					//64 protection
				if($_SESSION['id_effet_attaque_adv']==64 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==64 AND $effet_adv==1) {$_SESSION['protection_adv']=5; }
					//66 repos
				if($_SESSION['id_effet_attaque_adv']==66 AND $_SESSION['pv_adv']==$_SESSION['pv_max_adv'] OR $_SESSION['id_effet2_attaque_adv']==66 AND $_SESSION['pv_adv']==$_SESSION['pv_max_adv']){$effet_adv=0;}		
				if($_SESSION['id_effet_attaque_adv']==66 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==66 AND $effet_adv==1) {$lance_repos_adv=1;$_SESSION['statut_dodo_adv']=1;$_SESSION['fin_dodo_adv']=3;$_SESSION['statut_poison_adv']=0;$_SESSION['statut_poison_grave_adv']=0;if($_SESSION['statut_paralyse_adv']==1){$_SESSION['vit_adv']=$donnees['vit_adv']*4;}$_SESSION['statut_paralyse_adv']=0;$_SESSION['statut_brule_adv']=0;$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}
					//67 attraction
				if($_SESSION['id_effet_attaque_adv']==67 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==67 AND $effet_adv==1) {if($_SESSION['sexe']=="M" AND $_SESSION['sexe_adv']=="F" OR $_SESSION['sexe']=="F" AND $_SESSION['sexe_adv']=="M"){$_SESSION['attraction_adv']=1 ;}}
					//70 soin
				if($_SESSION['id_effet_attaque_adv']==70 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==70 AND $effet_adv==1) {$_SESSION['pv_adv']=$_SESSION['pv_adv']+floor($_SESSION['pv_max_adv']*$soins_adv);if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}}
					//107 aurore
                            if($_SESSION['zenith']>0){$multi_soins_climat=0.66;}
                            elseif($_SESSION['danse_pluie']>0 OR $_SESSION['tempete_sable']>0 OR $_SESSION['grele']>0){$multi_soins_climat=0.25;}
                            if($_SESSION['id_effet_attaque_adv']==107 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==107 AND $effet_adv==1) {$_SESSION['pv_adv']=$_SESSION['pv_adv']+floor($_SESSION['pv_max_adv']*$multi_soins_climat);if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}}
                                        //71 peur
				if($_SESSION['id_effet_attaque_adv']==71 AND $ordre_attaque_adv==2 OR $_SESSION['id_effet2_attaque_adv']==71 AND $ordre_attaque_adv==2) {$effet_adv=0;}
				if($_SESSION['id_effet_attaque_adv']==71 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==71 AND $effet_adv==1) {$peur=1;}
					//123 piqué appeurement
				if($_SESSION['id_effet_attaque_adv']==123 AND $ordre_attaque_adv==2 OR $_SESSION['id_effet2_attaque_adv']==123 AND $ordre_attaque_adv==2) {$effet_adv=0;}
                                if($_SESSION['id_effet_attaque_adv']==123 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==123 AND $effet_adv==1) {$rand_pr=rand(1,100); if($rand_pr<=30){$peur=1;}}
				//124 ronflementt
                                if($_SESSION['id_effet_attaque_adv']==124 AND $ordre_attaque_adv==2 OR $_SESSION['id_effet2_attaque_adv']==124 AND $ordre_attaque_adv==2) {$effet_adv=0;}
                                if($_SESSION['id_effet_attaque_adv']==124 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==124 AND $effet_adv==1) {if($_SESSION['statut_dodo_adv']>0){$rand_pr=rand(1,100); if($rand_pr<=30){$peur=1;}}}
				//72 danse flamme
				if($_SESSION['id_effet_attaque_adv']==72 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==72 AND $effet_adv==1) {$_SESSION['danse_flamme']=rand(3,6); }
					//73 tour rapide
				if($_SESSION['id_effet_attaque_adv']==73 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==73 AND $effet_adv==1) {$_SESSION['statut_vampigraine_adv']=0;$_SESSION['danse_flamme_adv']=0;$_SESSION['ligotage_adv']=0;}	
					//74 cc++
				if($_SESSION['id_effet_attaque_adv']==74 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==74 AND $effet_adv==1) {$_SESSION['bonus_cc_adv']=10; }	
					//75 croc fatal
				if($_SESSION['id_effet_attaque_adv']==75 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==75 AND $effet_adv==1) {$_SESSION['pv']=ceil($_SESSION['pv']/2); }	
					//109 balance
                                if($_SESSION['id_effet_attaque_adv']==109 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==109 AND $effet_adv==1) {$pv_balance=floor(($_SESSION['pv_adv']+$_SESSION['pv'])/2);$_SESSION['pv_adv']=$pv_balance;if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}$_SESSION['pv']=$pv_balance;if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}}		
				//77 ligotage
				if($_SESSION['id_effet_attaque_adv']==77 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==77 AND $effet_adv==1) {$_SESSION['ligotage']=rand(3,6);}	
					//79 esuna
				if($_SESSION['id_effet_attaque_adv']==79 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==79 AND $effet_adv==1) { $_SESSION['statut_confus_adv']=-1;$_SESSION['statut_poison_adv']=0;$_SESSION['statut_poison_grave_adv']=0;$_SESSION['statut_gel_adv']=0;if($_SESSION['statut_paralyse_adv']==1){$_SESSION['vit_adv']=$donnees['vit_adv']*4;}$_SESSION['statut_paralyse_adv']=0;$_SESSION['statut_brule_adv']=0;$_SESSION['statut_dodo_adv']=0;}	
					//84 lance boue
				if($_SESSION['id_effet_attaque_adv']==84 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==84 AND $effet_adv==1) {$_SESSION['lance_boue_adv']=1;}	
					//97 tourniquet
				if($_SESSION['id_effet_attaque_adv']==97 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==97 AND $effet_adv==1) {$_SESSION['tourniquet_adv']=1;}	
					//89 morphing
				if($_SESSION['id_effet_attaque_adv']==89 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==89 AND $effet_adv==1) 	
					{
					$_SESSION['type_adv']=$_SESSION['type'];$_SESSION['type2_adv']=$_SESSION['type2'];
					$_SESSION['attaque1_nb_adv']=$_SESSION['attaque1_nb'];$_SESSION['attaque2_nb_adv']=$_SESSION['attaque2_nb'];$_SESSION['attaque3_nb_adv']=$_SESSION['attaque3_nb'];$_SESSION['attaque4_nb_adv']=$_SESSION['attaque4_nb'];
					$_SESSION['nom_attaque1_adv'] = $_SESSION['nom_attaque1']; $_SESSION['type_attaque1_adv'] = $_SESSION['type_attaque1']; $_SESSION['puissance_attaque1_adv'] = $_SESSION['puissance_attaque1'];
					$_SESSION['prec_attaque1_adv'] = $_SESSION['prec_attaque1']; $_SESSION['cc_attaque1_adv'] = $_SESSION['cc_attaque1']; $_SESSION['classe_attaque1_adv'] = $_SESSION['classe_attaque1'];
					$_SESSION['priorite_attaque1_adv'] = $_SESSION['priorite_attaque1']; $_SESSION['esquive_attaque1_adv'] = $_SESSION['esquive_attaque1']; $_SESSION['cible_attaque1_adv'] = $_SESSION['cible_attaque1'];
					$_SESSION['id_effet_attaque1_adv'] = $_SESSION['id_effet_attaque1']; $_SESSION['id_effet2_attaque1_adv'] = $_SESSION['id_effet2_attaque1']; $_SESSION['proba_attaque1_adv'] = $_SESSION['proba_attaque1'];
					$_SESSION['nom_attaque2_adv'] = $_SESSION['nom_attaque2']; $_SESSION['type_attaque2_adv'] = $_SESSION['type_attaque2']; $_SESSION['puissance_attaque2_adv'] = $_SESSION['puissance_attaque2'];
					$_SESSION['prec_attaque2_adv'] = $_SESSION['prec_attaque2']; $_SESSION['cc_attaque2_adv'] = $_SESSION['cc_attaque2']; $_SESSION['classe_attaque2_adv'] = $_SESSION['classe_attaque2'];
					$_SESSION['priorite_attaque2_adv'] = $_SESSION['priorite_attaque2']; $_SESSION['esquive_attaque2_adv'] = $_SESSION['esquive_attaque2']; $_SESSION['cible_attaque2_adv'] = $_SESSION['cible_attaque2'];
					$_SESSION['id_effet_attaque2_adv'] = $_SESSION['id_effet_attaque2']; $_SESSION['id_effet2_attaque2_adv'] = $_SESSION['id_effet2_attaque2']; $_SESSION['proba_attaque2_adv'] = $_SESSION['proba_attaque2'];
					$_SESSION['nom_attaque3_adv'] = $_SESSION['nom_attaque3']; $_SESSION['type_attaque3_adv'] = $_SESSION['type_attaque3']; $_SESSION['puissance_attaque3_adv'] = $_SESSION['puissance_attaque3'];
					$_SESSION['prec_attaque3_adv'] = $_SESSION['prec_attaque3']; $_SESSION['cc_attaque3_adv'] = $_SESSION['cc_attaque3']; $_SESSION['classe_attaque3_adv'] = $_SESSION['classe_attaque3'];
					$_SESSION['priorite_attaque3_adv'] = $_SESSION['priorite_attaque3']; $_SESSION['esquive_attaque3_adv'] = $_SESSION['esquive_attaque3']; $_SESSION['cible_attaque3_adv'] = $_SESSION['cible_attaque3'];
					$_SESSION['id_effet_attaque3_adv'] = $_SESSION['id_effet_attaque3']; $_SESSION['id_effet2_attaque3_adv'] = $_SESSION['id_effet2_attaque3']; $_SESSION['proba_attaque3_adv'] = $_SESSION['proba_attaque3'];
					$_SESSION['nom_attaque4_adv'] = $_SESSION['nom_attaque4']; $_SESSION['type_attaque4_adv'] = $_SESSION['type_attaque4']; $_SESSION['puissance_attaque4_adv'] = $_SESSION['puissance_attaque4'];
					$_SESSION['prec_attaque4_adv'] = $_SESSION['prec_attaque4']; $_SESSION['cc_attaque4_adv'] = $_SESSION['cc_attaque4']; $_SESSION['classe_attaque4_adv'] = $_SESSION['classe_attaque4'];
					$_SESSION['priorite_attaque4_adv'] = $_SESSION['priorite_attaque4']; $_SESSION['esquive_attaque4_adv'] = $_SESSION['esquive_attaque4']; $_SESSION['cible_attaque4_adv'] = $_SESSION['cible_attaque4'];
					$_SESSION['id_effet_attaque4_adv'] = $_SESSION['id_effet_attaque4']; $_SESSION['id_effet2_attaque4_adv'] = $_SESSION['id_effet2_attaque4']; $_SESSION['proba_attaque4_adv'] = $_SESSION['proba_attaque4'];
					$_SESSION['att_adv']=$_SESSION['att'];
					$_SESSION['def_adv']=$_SESSION['def'];
					$_SESSION['vit_adv']=$_SESSION['vit'];
					$_SESSION['attspe_adv']=$_SESSION['attspe'];
					$_SESSION['defspe_adv']=$_SESSION['defspe'];
					$_SESSION['att_max_adv']=$_SESSION['att_max'];
					$_SESSION['def_max_adv']=$_SESSION['def_max'];
					$_SESSION['vit_max_adv']=$_SESSION['vit_max'];
					$_SESSION['attspe_max_adv']=$_SESSION['attspe_max'];
					$_SESSION['defspe_max_adv']=$_SESSION['defspe_max'];
					}
				//96 mania
				if($_SESSION['id_effet_attaque_adv']==96 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==96 AND $effet_adv==1) 
					{
					if($_SESSION['mania_adv']==0){$_SESSION['mania_adv']=rand(1,2);$_SESSION['attaque_auto_adv']=1;$effet_adv=0;}
					elseif($_SESSION['mania_adv']==1){$_SESSION['mania_adv']=0;$_SESSION['statut_confus_adv']=rand(2,5);$statut_confus_begin_adv=1;}
					elseif($_SESSION['mania_adv']>1){$_SESSION['mania_adv']=$_SESSION['mania_adv']-1;$_SESSION['attaque_auto_adv']=1;$effet_adv=0;}
					}
				//100 gribouille
				if($_SESSION['id_effet_attaque_adv']==100 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==100 AND $effet_adv==1) 
					{
					if($_SESSION['last_attaque']!=0 AND $_SESSION['last_attaque']!=252) //pas lutte
						{
						$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_blind_battles WHERE id_battles=:id_battles AND pseudo!=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id_battles' => $_POST['id_match'], 'pseudo' => $adversaire));  
						$donnees = $reponse->fetch();//pokemon à soi
						if($donnees['attaque1']==300)
							{
							$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons_blind_battles SET attaque1=:attaque1 WHERE id_battles=:id_battles AND pseudo!=:pseudo') or die(print_r($bdd->errorInfo()));
							$reponse->execute(array('attaque1' => $_SESSION['last_attaque'], 'id_battles' => $_POST['id_match'], 'pseudo' => $adversaire)); 
							}
						elseif($donnees['attaque2']==300)
							{
							$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons_blind_battles SET attaque2=:attaque2 WHERE id_battles=:id_battles AND pseudo!=:pseudo') or die(print_r($bdd->errorInfo()));
							$reponse->execute(array('attaque2' => $_SESSION['last_attaque'], 'id_battles' => $_POST['id_match'], 'pseudo' => $adversaire)); 
							}
						elseif($donnees['attaque3']==300)
							{
							$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons_blind_battles SET attaque3=:attaque3 WHERE id_battles=:id_battles AND pseudo!=:pseudo') or die(print_r($bdd->errorInfo()));
							$reponse->execute(array('attaque3' => $_SESSION['last_attaque'], 'id_battles' => $_POST['id_match'], 'pseudo' => $adversaire));
							}
						elseif($donnees['attaque4']==300)
							{
							$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons_blind_battles SET attaque4=:attaque4 WHERE id_battles=:id_battles AND pseudo!=:pseudo') or die(print_r($bdd->errorInfo()));
							$reponse->execute(array('attaque4' => $_SESSION['last_attaque'], 'id_battles' => $_POST['id_match'], 'pseudo' => $adversaire));
							}
						else{$echec_attaque=1;}
						$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_blind_battles WHERE id_battles=:id_battles AND pseudo!=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id_battles' => $_POST['id_match'], 'pseudo' => $adversaire));  
						$donnees = $reponse->fetch();//pokemon à soi
						$_SESSION['attaque1_nb_adv']=$donnees['attaque1'];
						$_SESSION['attaque2_nb_adv']=$donnees['attaque2'];
						$_SESSION['attaque3_nb_adv']=$donnees['attaque3'];
						$_SESSION['attaque4_nb_adv']=$donnees['attaque4'];
						$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $_SESSION['attaque1_nb']));  
						$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $_SESSION['attaque1_nb_adv']));  
						$donnees = $reponse->fetch();
							$_SESSION['nom_attaque1_adv'] = $donnees['nom']; $_SESSION['type_attaque1_adv'] = $donnees['type']; $_SESSION['puissance_attaque1_adv'] = $donnees['puissance'];
							$_SESSION['prec_attaque1_adv'] = $donnees['prec']; $_SESSION['cc_attaque1_adv'] = $donnees['cc']; $_SESSION['classe_attaque1_adv'] = $donnees['classe'];
							$_SESSION['priorite_attaque1_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque1_adv'] = $donnees['esquive']; $_SESSION['cible_attaque1_adv'] = $donnees['cible'];
							$_SESSION['id_effet_attaque1_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque1_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque1_adv'] = $donnees['proba'];
						$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $_SESSION['attaque2_nb_adv']));  
						$donnees = $reponse->fetch();
							$_SESSION['nom_attaque2_adv'] = $donnees['nom']; $_SESSION['type_attaque2_adv'] = $donnees['type']; $_SESSION['puissance_attaque2_adv'] = $donnees['puissance'];
							$_SESSION['prec_attaque2_adv'] = $donnees['prec']; $_SESSION['cc_attaque2_adv'] = $donnees['cc']; $_SESSION['classe_attaque2_adv'] = $donnees['classe'];
							$_SESSION['priorite_attaque2_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque2_adv'] = $donnees['esquive']; $_SESSION['cible_attaque2_adv'] = $donnees['cible'];
							$_SESSION['id_effet_attaque2_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque2_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque2_adv'] = $donnees['proba'];
						$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $_SESSION['attaque3_nb_adv']));  
						$donnees = $reponse->fetch();
							$_SESSION['nom_attaque3_adv'] = $donnees['nom']; $_SESSION['type_attaque3_adv'] = $donnees['type']; $_SESSION['puissance_attaque3_adv'] = $donnees['puissance'];
							$_SESSION['prec_attaque3_adv'] = $donnees['prec']; $_SESSION['cc_attaque3_adv'] = $donnees['cc']; $_SESSION['classe_attaque3_adv'] = $donnees['classe'];
							$_SESSION['priorite_attaque3_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque3_adv'] = $donnees['esquive']; $_SESSION['cible_attaque3_adv'] = $donnees['cible'];
							$_SESSION['id_effet_attaque3_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque3_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque3_adv'] = $donnees['proba'];
						$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $_SESSION['attaque4_nb_adv']));  
						$donnees = $reponse->fetch();
							$_SESSION['nom_attaque4_adv'] = $donnees['nom']; $_SESSION['type_attaque4_adv'] = $donnees['type']; $_SESSION['puissance_attaque4_adv'] = $donnees['puissance'];
							$_SESSION['prec_attaque4_adv'] = $donnees['prec']; $_SESSION['cc_attaque4_adv'] = $donnees['cc']; $_SESSION['classe_attaque4_adv'] = $donnees['classe'];
							$_SESSION['priorite_attaque4_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque4_adv'] = $donnees['esquive']; $_SESSION['cible_attaque4_adv'] = $donnees['cible'];
							$_SESSION['id_effet_attaque4_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque4_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque4_adv'] = $donnees['proba'];
						$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));}
					else{$echec_attaque=1;}
					}	
				//105 prélevement destin	
					if($_SESSION['id_effet_attaque_adv']==105 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==105 AND $effet_adv==1) {$_SESSION['destin_adv']=1;}
                                //106 ampleur	
                                        if($_SESSION['id_effet_attaque_adv']==106 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==106 AND $effet_adv==1) {$_SESSION['puissance_attaque_adv']=rand(10,150);if($_SESSION['esquive_attaque']==1 AND $_SESSION['last_attaque']==45){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*2;}}
			        //117 contre
                                    if($_SESSION['id_effet_attaque_adv']==117 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==117 AND $effet_adv==1) {
                                        $contre=($_SESSION['pv_adv']/$_SESSION['pv_max_adv'])*100;
                                        if($contre<3.12){$_SESSION['puissance_attaque_adv']=200;}
                                        elseif($contre<9.37){$_SESSION['puissance_attaque_adv']=150;}
                                        elseif($contre<20.31){$_SESSION['puissance_attaque_adv']=100;}
                                        elseif($contre<34.37){$_SESSION['puissance_attaque_adv']=80;}
                                        elseif($contre<67.19){$_SESSION['puissance_attaque_adv']=40;}
                                        else{$_SESSION['puissance_attaque_adv']=20;}
                                        }
                                //119 éruption	
                                if($_SESSION['id_effet_attaque_adv']==119 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==119 AND $effet_adv==1) {$_SESSION['puissance_attaque_adv']=150*$_SESSION['pv_adv']/$_SESSION['pv_max_adv'];}
                                //114 façade
                                if($_SESSION['id_effet_attaque_adv']==114 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==114 AND $effet_adv==1) {if($_SESSION['statut_poison_adv']>0 OR $_SESSION['statut_poison_grave_adv']>0 OR $_SESSION['statut_brule_adv']>0 OR $_SESSION['statut_paralyse_adv']>0){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*2;}}
                                //112 façade
                                if($_SESSION['id_effet_attaque_adv']==112 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==112 AND $effet_adv==1) {$_SESSION['puissance_attaque_adv']=102-ceil($_SESSION['bonheur_adv']*2/5);if($_SESSION['puissance_attaque_adv']<1){$_SESSION['puissance_attaque_adv']=1;}if($_SESSION['puissance_attaque_adv']>102){$_SESSION['puissance_attaque_adv']=102;}}
                                //113 retour
                                if($_SESSION['id_effet_attaque_adv']==113 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==113 AND $effet_adv==1) {$_SESSION['puissance_attaque_adv']=ceil($_SESSION['bonheur_adv']*2/5);if($_SESSION['puissance_attaque_adv']<1){$_SESSION['puissance_attaque_adv']=1;}if($_SESSION['puissance_attaque_adv']>102){$_SESSION['puissance_attaque_adv']=102;}}
			
				//effets variation de statut
					//39 confusion
				if($_SESSION['id_effet_attaque_adv']==39 AND $effet_adv==1 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==39 AND $effet_adv==1 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}		
				if($_SESSION['id_effet_attaque_adv']==39 AND $effet_adv==1 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==39 AND $effet_adv==1 AND $_SESSION['rune_protect']==0) {if($_SESSION['statut_confus']==-1){$_SESSION['statut_confus']=rand(2,5);if($ordre_attaque_adv==2){$statut_confus_begin=1;}}else{$effet_adv=0;}}			
					//41 empoisonnement
				if($_SESSION['id_effet_attaque_adv']==41 AND $effet_adv==1 AND $_SESSION['statut_poison']==0 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==41 AND $effet_adv==1 AND $_SESSION['statut_poison']==0 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}			
				if($_SESSION['id_effet_attaque_adv']==41 AND $effet_adv==1 AND $_SESSION['statut_poison']!=0 OR $_SESSION['id_effet2_attaque_adv']==41 AND $effet_adv==1 AND $_SESSION['statut_poison']!=0){$effet_adv=0;}
				if($_SESSION['id_effet_attaque_adv']==41 AND $_SESSION['type']=="poison" OR $_SESSION['id_effet_attaque_adv']==41 AND $_SESSION['type2']=="poison" OR $_SESSION['id_effet_attaque_adv']==41 AND $_SESSION['type']=="acier" OR $_SESSION['id_effet_attaque_adv']==41 AND $_SESSION['type2']=="acier"){$effet_adv=0;}			
				if($_SESSION['id_effet_attaque_adv']==41 AND $effet_adv==1 AND $_SESSION['statut_poison']==0 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==41 AND $effet_adv==1 AND $_SESSION['statut_poison']==0 AND $_SESSION['rune_protect']==0) {$_SESSION['statut_poison']=1;}
					//42 empoisonnement grave
				if($_SESSION['id_effet_attaque_adv']==42 AND $effet_adv==1 AND $_SESSION['statut_poison_grave']==0 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==42 AND $effet_adv==1 AND $_SESSION['statut_poison_grave']==0 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}
				if($_SESSION['id_effet_attaque_adv']==42 AND $_SESSION['type']=="poison" OR $_SESSION['id_effet_attaque_adv']==42 AND $_SESSION['type2']=="poison" OR $_SESSION['id_effet_attaque_adv']==42 AND $_SESSION['type']=="acier" OR $_SESSION['id_effet_attaque_adv']==42 AND $_SESSION['type2']=="acier"){$effet_adv=0;}			
				if($_SESSION['id_effet_attaque_adv']==42 AND $effet_adv==1 AND $_SESSION['statut_poison_grave']==0 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==42 AND $effet_adv==1 AND $_SESSION['statut_poison_grave']==0 AND $_SESSION['rune_protect']==0) {$_SESSION['statut_poison_grave']=1;}
					//47 gel
				if($_SESSION['id_effet_attaque_adv']==47 AND $_SESSION['type']=="glace" OR $_SESSION['id_effet_attaque_adv']==47 AND $_SESSION['type2']=="glace"){$effet_adv=0;}
				if($_SESSION['id_effet_attaque_adv']==47 AND $effet_adv==1 AND $_SESSION['statut_gel']!=1 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==47 AND $effet_adv==1 AND $_SESSION['statut_gel']!=1 AND $_SESSION['rune_protect']!=0) {$effet_adv=0; }				
				if($_SESSION['id_effet_attaque_adv']==47 AND $effet_adv==1 AND $_SESSION['statut_gel']!=0 OR $_SESSION['id_effet2_attaque_adv']==47 AND $effet_adv==1 AND $_SESSION['statut_gel']!=0){$effet_adv=0;}	
				if($_SESSION['id_effet_attaque_adv']==47 AND $effet_adv==1 AND $_SESSION['statut_gel']!=1 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==47 AND $effet_adv==1 AND $_SESSION['statut_gel']!=1 AND $_SESSION['rune_protect']==0) {$_SESSION['statut_gel']=1; }
					//55 paralysie
				if($_SESSION['id_effet_attaque_adv']==55 AND $effet_adv==1 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==55 AND $effet_adv==1 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}
				if($_SESSION['id_effet_attaque_adv']==55 AND $effet_adv==1 AND $_SESSION['statut_paralyse']!=0 OR $_SESSION['id_effet2_attaque_adv']==55 AND $effet_adv==1 AND $_SESSION['statut_paralyse']!=0){$effet_adv=0;}	
				if($_SESSION['id_effet_attaque_adv']==55 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==55 AND $_SESSION['rune_protect']==0 AND $effet_adv==1 AND $_SESSION['rune_protect']==0) {$_SESSION['statut_paralyse']=1; $_SESSION['vit']=$_SESSION['vit']/4;}
					//65 brule
				if($_SESSION['id_effet_attaque_adv']==65 AND $_SESSION['type']=="feu" OR $_SESSION['id_effet_attaque_adv']==65 AND $_SESSION['type2']=="feu"){$effet_adv=0;}
				if($_SESSION['id_effet_attaque_adv']==65 AND $effet_adv==1 AND $_SESSION['statut_brule']==0 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==65 AND $effet_adv==1 AND $_SESSION['statut_brule']==0 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}
				if($_SESSION['id_effet_attaque_adv']==65 AND $effet_adv==1 AND $_SESSION['statut_brule']!=0 OR $_SESSION['id_effet2_attaque_adv']==65 AND $effet_adv==1 AND $_SESSION['statut_brule']!=0){$effet_adv=0;}
				if($_SESSION['id_effet_attaque_adv']==65 AND $effet_adv==1 AND $_SESSION['statut_brule']==0 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==65 AND $effet_adv==1 AND $_SESSION['statut_brule']==0 AND $_SESSION['rune_protect']==0) {$_SESSION['statut_brule']=1;$_SESSION['att']=$_SESSION['att']/2;}
					//69 sommeil
				if($_SESSION['id_effet_attaque_adv']==69 AND $effet_adv==1 AND $_SESSION['statut_dodo']!=1 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==69 AND $effet_adv==1 AND $_SESSION['statut_dodo']!=1 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}
				if($_SESSION['id_effet_attaque_adv']==69 AND $effet_adv==1 AND $_SESSION['statut_dodo']!=0 OR $_SESSION['id_effet2_attaque_adv']==69 AND $effet_adv==1 AND $_SESSION['statut_dodo']!=0){$effet_adv=0;}	
				if($_SESSION['id_effet_attaque_adv']==69 AND $effet_adv==1 AND $_SESSION['statut_dodo']!=1 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==69 AND $effet_adv==1 AND $_SESSION['statut_dodo']!=1 AND $_SESSION['rune_protect']==0) {$_SESSION['statut_dodo']=1;$_SESSION['fin_dodo']=rand(2,8);if($ordre_attaque_adv==2){$dodo_now=1;}}
					//80 triplattaque
				if($_SESSION['id_effet_attaque_adv']==80 AND $effet_adv==1 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==80 AND $effet_adv==1 AND $_SESSION['statut_brule']==0 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}
				if($_SESSION['id_effet_attaque_adv']==80 AND $effet_adv==1 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==80 AND $effet_adv==1 AND $_SESSION['rune_protect']==0) {$triplattaque1_adv=rand(1,100);$triplattaque2_adv=rand(1,100); $triplattaque3_adv=rand(1,100);  if($triplattaque1_adv<=20){$_SESSION['statut_brule']=1;$triplattaque_brule_adv=1;}elseif($triplattaque2_adv<=20){$_SESSION['statut_paralyse']=1;$_SESSION['vit']=$donnees['vit']/4;$triplattaque_paralyse_adv=1;}elseif($triplattaque3_adv<=20){$_SESSION['statut_gel']=1;$triplattaque_gel_adv=1;}}
					//68 vampigraine
				if($_SESSION['id_effet_attaque_adv']==68 AND $effet_adv==1 AND $_SESSION['statut_vampigraine']!=0 OR $_SESSION['id_effet2_attaque_adv']==68 AND $effet_adv==1 AND $_SESSION['statut_vampigraine']!=0){$effet_adv=0;}
				if($_SESSION['id_effet_attaque_adv']==68 AND $effet_adv==1 AND $_SESSION['statut_vampigraine']==0 OR $_SESSION['id_effet2_attaque_adv']==68 AND $effet_adv==1 AND $_SESSION['statut_vampigraine']==0) {$_SESSION['statut_vampigraine']=1;}
					//1 ko
				if($_SESSION['id_effet_attaque_adv']==1 AND $effet_adv==1 AND $_SESSION['lvl_adv']>=$_SESSION['lvl'] OR $_SESSION['id_effet2_attaque_adv']==1 AND $effet_adv==1 AND $_SESSION['lvl_adv']>=$_SESSION['lvl']) {$_SESSION['pv']=0;}	
				if($_SESSION['id_effet_attaque_adv']==1 AND $effet_adv==1 AND $_SESSION['lvl_adv']<$_SESSION['lvl'] OR $_SESSION['id_effet2_attaque_adv']==1 AND $effet_adv==1 AND $_SESSION['lvl_adv']<=$_SESSION['lvl']) {$effet_adv=0;}
				
				if($_SESSION['def_adv']<=0){$_SESSION['def_adv']=1;}if($_SESSION['att_adv']<=0){$_SESSION['att_adv']=1;}if($_SESSION['vit_adv']<=0){$_SESSION['vit_adv']=1;}if($_SESSION['defspe_adv']<=0){$_SESSION['defspe_adv']=1;}if($_SESSION['attspe_adv']<=0){$_SESSION['attspe_adv']=1;}			
				if($_SESSION['def']<=0){$_SESSION['def']=1;}if($_SESSION['att']<=0){$_SESSION['att']=1;}if($_SESSION['vit']<=0){$_SESSION['vit']=1;}if($_SESSION['defspe']<=0){$_SESSION['defspe']=1;}if($_SESSION['attspe']<=0){$_SESSION['attspe']=1;}			
			//calcul des dégats
				$hasard_degats=rand(85,115);
				$hasard_cc=rand(1,100);
				$hasard_cc=$hasard_cc+$_SESSION['bonus_cc_adv'];
				if($_SESSION['cc_attaque_adv']==1){if($hasard_cc<7){$cc_adv=1.5;}else{$cc_adv=1;}}
				if($_SESSION['cc_attaque_adv']==2){if($hasard_cc<15){$cc_adv=1.5;}else{$cc_adv=1;}}
				$degats_adv=0;
				
				//influence des items
			if($_SESSION['objet_adv']==102 AND $_SESSION['type_attaque_adv']=="acier"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//peau métal
			if($_SESSION['objet_adv']==103 AND $_SESSION['type_attaque_adv']=="electrique"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//aimant
			if($_SESSION['objet_adv']==104 AND $_SESSION['type_attaque_adv']=="vol"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//bec pointu
			if($_SESSION['objet_adv']==105 AND $_SESSION['type_attaque_adv']=="combat"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//ceinture noire
			if($_SESSION['objet_adv']==106 AND $_SESSION['type_attaque_adv']=="feu"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//charbon
			if($_SESSION['objet_adv']==107 AND $_SESSION['type_attaque_adv']=="dragon"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//croc dragon
			if($_SESSION['objet_adv']==108 AND $_SESSION['type_attaque_adv']=="psy"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//cuillertordue
			if($_SESSION['objet_adv']==109 AND $_SESSION['type_attaque_adv']=="eau"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//eau mystique
			if($_SESSION['objet_adv']==110 AND $_SESSION['type_attaque_adv']=="glace"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//glaceternelle
			if($_SESSION['objet_adv']==111 AND $_SESSION['type_attaque_adv']=="plante"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//grain miracle
			if($_SESSION['objet_adv']==112 AND $_SESSION['type_attaque_adv']=="normal"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//mouchoir soie
			if($_SESSION['objet_adv']==113 AND $_SESSION['type_attaque_adv']=="poison"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//pic venin
			if($_SESSION['objet_adv']==114 AND $_SESSION['type_attaque_adv']=="insecte"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//poudre argent
			if($_SESSION['objet_adv']==115 AND $_SESSION['type_attaque_adv']=="sol"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//sable doux
			if($_SESSION['objet_adv']==117 AND $_SESSION['type_attaque_adv']=="spectre"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//rune sort
			if($_SESSION['objet_adv']==118 AND $_SESSION['type_attaque_adv']=="roche"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//pierre dure
			if($_SESSION['objet_adv']==119 AND $_SESSION['type_attaque_adv']=="tenebre"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//lunette noir
			
			
				if($_SESSION['classe_attaque_adv']=="physique"){$degats_adv=/*$_SESSION['lvl_adv']*/100*0.4;$degats_adv=$degats_adv+2; $degats_adv=$degats_adv*$_SESSION['att_adv']*$_SESSION['puissance_attaque_adv'];$defense_adv=$_SESSION['def']*50;$degats_adv=$degats_adv/$defense_adv;$degats_adv=$degats_adv+2; $degats_adv=$degats_adv*$multiplicateur_adv;$degats_adv=$degats_adv*$cc_adv*$hasard_degats*$rafale_adv/100;if($_SESSION['protection']>0){$degats_adv=$degats_adv/2;}}	
				if($_SESSION['classe_attaque_adv']=="speciale"){$degats_adv=/*$_SESSION['lvl_adv']*/100*0.4;$degats_adv=$degats_adv+2; $degats_adv=$degats_adv*$_SESSION['attspe_adv']*$_SESSION['puissance_attaque_adv'];$defense_adv=$_SESSION['defspe']*50;$degats_adv=$degats_adv/$defense_adv;$degats_adv=$degats_adv+2; $degats_adv=$degats_adv*$multiplicateur_adv;$degats_adv=$degats_adv*$cc_adv*$hasard_degats*$rafale_adv/100;if($_SESSION['mur_lumiere']>0){$degats_adv=$degats_adv/2;}}	
				if($_SESSION['type_attaque_adv']==$_SESSION['type_adv'] OR $_SESSION['type_attaque_adv']==$_SESSION['type2_adv']){$degats_adv=$degats_adv*1.5;}				
				if($_SESSION['puissance_attaque_adv']==0){$degats_adv=0;}
				//82 frappe atlas
				if($_SESSION['id_effet_attaque_adv']==82 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==82 AND $effet_adv==1) { if($multiplicateur_adv!=0){$degats_adv=$_SESSION['lvl_adv'];}}
				//86 sonicboom
				if($_SESSION['id_effet_attaque_adv']==86 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==86 AND $effet_adv==1) { if($multiplicateur_adv!=0){$degats_adv=20;}}
				//118 effort
                                if($_SESSION['id_effet_attaque_adv']==118 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==118 AND $effet_adv==1) {if($_SESSION['pv']>$_SESSION['pv_adv']){$degats_adv=$_SESSION['pv']-$_SESSION['pv_adv'];}}
                                //95 draco-rage
				if($_SESSION['id_effet_attaque_adv']==95 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==95 AND $effet_adv==1) { if($multiplicateur_adv!=0){$degats_adv=40;}}
				//88 25%deg
				if($_SESSION['id_effet_attaque_adv']==88 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==88 AND $effet_adv==1) { if($multiplicateur_adv!=0){$degats_adv=floor($_SESSION['pv_max']/4);}}
				//84 effet lance-boue
				if($_SESSION['type_attaque_adv']=="electrique" AND $_SESSION['lance_boue']==1){$degats_adv=floor($degats_adv/2);}
				//97 effet tourniquet
				if($_SESSION['type_attaque_adv']=="feu" AND $_SESSION['tourniquet']==1){$degats_adv=floor($degats_adv/2);}
				//101 riposte
				if($_SESSION['classe_attaque']=="physique" AND $_SESSION['id_effet_attaque_adv']==101){$degats_adv=$degats*2;$_SESSION['puissance_attaque_adv']=1;}elseif($_SESSION['classe_attaque']!="physique" AND $_SESSION['id_effet_attaque_adv']==101){$echec_attaque_adv=1;}
				//102 voile miroir
				if($_SESSION['classe_attaque']=="speciale" AND $_SESSION['id_effet_attaque_adv']==102){$degats_adv=$degats*2;$_SESSION['puissance_attaque_adv']=1;}elseif($_SESSION['classe_attaque']!="speciale" AND $_SESSION['id_effet_attaque_adv']==102){$echec_attaque_adv=1;}
				//effet abri
				if($abri==1){$degats_adv=0;$abri=0;}
				
				$degats_adv=floor($degats_adv);if($degats_adv<1 AND $degats_adv!=0) {$degats_adv=1;}
                                if($degats_adv>$_SESSION['pv']){$degats_adv=$_SESSION['pv'];}
				$_SESSION['pv']=$_SESSION['pv']-$degats_adv; if($_SESSION['pv']<0){$_SESSION['pv']=0;} 
				//120 faux-chage
                                if($_SESSION['pv']==0 AND $_SESSION['id_effet_attaque_adv']==120){$_SESSION['pv']=1; $degats_adv=$degats_adv-1;}
                                //effet tenacite
                                if($_SESSION['pv']<=0 AND $tenacite==1){$_SESSION['pv']=1; $degats_adv=$degats_adv-1;}
			
				//2 att -
				if($_SESSION['id_effet_attaque_adv']==2 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==2 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//3 def -
				if($_SESSION['id_effet_attaque_adv']==3 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==3 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
					//4 vit -
				if($_SESSION['id_effet_attaque_adv']==4 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==4 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					}
					//5 att spe -
				if($_SESSION['id_effet_attaque_adv']==5 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==5 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//6 defspe -
				if($_SESSION['id_effet_attaque_adv']==6 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==6 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
					//7 att --
				if($_SESSION['id_effet_attaque_adv']==7 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==7 AND $effet_adv==1)
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//8 def --
				if($_SESSION['id_effet_attaque_adv']==8 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==8 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
					//9 vit --
				if($_SESSION['id_effet_attaque_adv']==9 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==9 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					}
					//10 att spe --
				if($_SESSION['id_effet_attaque_adv']==10 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==10 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//11 defspe --
				if($_SESSION['id_effet_attaque_adv']==11 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==11 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
					//12 att ---
				if($_SESSION['id_effet_attaque_adv']==12 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==12 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//13 def ---
				if($_SESSION['id_effet_attaque_adv']==13 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==13 AND $effet_adv==1)
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
					//14 vit ---
				if($_SESSION['id_effet_attaque_adv']==14 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==14 AND $effet_adv==1)
					{
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					}
					//15 att spe ---
				if($_SESSION['id_effet_attaque_adv']==15 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==15 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//16 defspe ---
				if($_SESSION['id_effet_attaque_adv']==16 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==16 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
					//17 att +
				if($_SESSION['id_effet_attaque_adv']==17 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==17 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//18 def +
				if($_SESSION['id_effet_attaque_adv']==18 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==18 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}
					//19 vit +
				if($_SESSION['id_effet_attaque_adv']==19 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==19 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					}
					//20 att spe +
				if($_SESSION['id_effet_attaque_adv']==20 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==20 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//21 defspe +
				if($_SESSION['id_effet_attaque_adv']==21 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==21 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
					//93 all stat +
				if($_SESSION['id_effet_attaque_adv']==93 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==93 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
					//22 att ++
				if($_SESSION['id_effet_attaque_adv']==22 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==22 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//23 def ++
				if($_SESSION['id_effet_attaque_adv']==23 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==23 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}
					//24 vit ++
				if($_SESSION['id_effet_attaque_adv']==24 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==24 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					}
					//25 att spe ++
				if($_SESSION['id_effet_attaque_adv']==25 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==25 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//83 att spe lanceur --
				if($_SESSION['id_effet_attaque_adv']==83 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==83 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//26 defspe ++
				if($_SESSION['id_effet_attaque_adv']==26 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==26 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
					//27 att +++
				if($_SESSION['id_effet_attaque_adv']==27 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==27 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//28 def +++
				if($_SESSION['id_effet_attaque_adv']==28 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==28 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}
					//29 vit +++
				if($_SESSION['id_effet_attaque_adv']==29 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==29 AND $effet_adv==1)
					{
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					}
					//30 att spe +++
				if($_SESSION['id_effet_attaque_adv']==30 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==30 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//31 defspe +++
				if($_SESSION['id_effet_attaque_adv']==31 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==31 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
                                        //111 boost
				if($_SESSION['id_effet_attaque_adv']==111 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==111 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$ex_mul;
                                        $ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$ex_mul;
                                        $ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$ex_mul;
                                        $ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$ex_mul;
                                        $ex_mul=$_SESSION['def_spe']/$_SESSION['def_spe_max'];
					$_SESSION['def_spe_adv']=$_SESSION['def_spe_max_adv']*$ex_mul;
					}  
                                        //116 cognobidon
				if($_SESSION['id_effet_attaque']==116 AND $effet==1 OR $_SESSION['id_effet2_attaque']==116 AND $effet==1) 
					{
					$mul=4;
                                        $_SESSION['pv']=ceil($_SESSION['pv_adv']/2);
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
                                //121 att spe ennemi +
				if($_SESSION['id_effet_attaque']==121 AND $effet==1 OR $_SESSION['id_effet2_attaque']==121 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
                                //126 att ennemi ++
				if($_SESSION['id_effet_attaque']==126 AND $effet==1 OR $_SESSION['id_effet2_attaque']==126 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//32 pré +
				if($_SESSION['id_effet_attaque_adv']==32 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==32 AND $effet_adv==1) {$_SESSION['pre_adv']=$_SESSION['pre_adv']+5; if($_SESSION['pre_adv']>30){$_SESSION['pre_adv']=30;}if($_SESSION['pre_adv']<-30){$_SESSION['pre_adv']=-30;}}
					//33 pré ++
				if($_SESSION['id_effet_attaque_adv']==33 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==33 AND $effet_adv==1) {$_SESSION['pre_adv']=$_SESSION['pre_adv']+10; if($_SESSION['pre_adv']>30){$_SESSION['pre_adv']=30;}if($_SESSION['pre_adv']<-30){$_SESSION['pre_adv']=-30;}}
					//34 pré +++
				if($_SESSION['id_effet_attaque_adv']==34 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==34 AND $effet_adv==1) {$_SESSION['pre_adv']=$_SESSION['pre_adv']+15;if($_SESSION['pre_adv']>30){$_SESSION['pre_adv']=30;}if($_SESSION['pre_adv']<-30){$_SESSION['pre_adv']=-30;}}
					//35 pré -
				if($_SESSION['id_effet_attaque_adv']==35 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==35 AND $effet_adv==1) {$_SESSION['pre']=$_SESSION['pre']-5;if($_SESSION['pre']>30){$_SESSION['pre']=30;}if($_SESSION['pre']<-30){$_SESSION['pre']=-30;}}
					//36 pré --
				if($_SESSION['id_effet_attaque_adv']==36 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==36 AND $effet_adv==1) {$_SESSION['pre']=$_SESSION['pre']-10;if($_SESSION['pre']>30){$_SESSION['pre']=30;}if($_SESSION['pre']<-30){$_SESSION['pre']=-30;}}
					//37 pré---
				if($_SESSION['id_effet_attaque_adv']==37 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==37 AND $effet_adv==1) {$_SESSION['pre']=$_SESSION['pre']-15;if($_SESSION['pre']>30){$_SESSION['pre']=30;}if($_SESSION['pre']<-30){$_SESSION['pre']=-30;}}
					//90 self-att -
				if($_SESSION['id_effet_attaque_adv']==90 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==90 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//91 self-def -
				if($_SESSION['id_effet_attaque_adv']==91 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==91 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}	
				//94 self-attspe --
				if($_SESSION['id_effet_attaque_adv']==94 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==94 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
				}
			else
				{
				if($hasard_precision_adv!=1000){$echec_attaque_adv=1;}
				if($hasard_precision_adv>=1000){$_SESSION['mania_adv']=0;}
				}
				
			//effet après attaque
			
				//85 self-k.o.
			if($_SESSION['id_effet_attaque_adv']==85 OR $_SESSION['id_effet2_attaque_adv']==85) {if($hasard_precision_adv!=1000){$_SESSION['pv_adv']=0;}}	
				//76 damocles
			if($_SESSION['id_effet_attaque_adv']==76 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==76 AND $effet_adv==1) {$damocles_adv=floor($degats_adv/3); $_SESSION['pv_adv']=$_SESSION['pv_adv']-$damocles_adv; if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}}	
				//92 bélier
			if($_SESSION['id_effet_attaque_adv']==92 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==92 AND $effet_adv==1) {$damocles_adv=floor($degats_adv/4); $_SESSION['pv_adv']=$_SESSION['pv_adv']-$damocles_adv; if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}}
				//52 sangsue
			if($_SESSION['id_effet_attaque_adv']==52 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==52 AND $effet_adv==1) {$sangsue_adv=floor($degats_adv/2); $_SESSION['pv_adv']=$_SESSION['pv_adv']+$sangsue_adv; if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$sangsue_adv=$_SESSION['pv_max_adv']-$_SESSION['pv_adv']+$sangsue_adv;$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}}		
				//souffre de poison (41)
			if($_SESSION['statut_poison_adv']!=0){$statut_poison_adv=1;$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
				//souffre de poison gravement (42)
			if($_SESSION['statut_poison_grave_adv']!=0){$statut_poison_grave_adv=1;$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['statut_poison_grave_adv']*$_SESSION['pv_max_adv']/16);$_SESSION['statut_poison_grave_adv']=$_SESSION['statut_poison_grave_adv']+1;}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
				//souffre de brule(65)
			if($_SESSION['statut_brule_adv']!=0){$statut_brule_adv=1;$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
				//souffre de danse flamme(72)
			if($_SESSION['danse_flamme_adv']>0){$danse_flamme_adv=1;$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
				//souffre de ligotage(77)
			if($_SESSION['ligotage_adv']>0){$ligotage_adv;$ligotage_adv=1; $_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
				//souffre de vampigraine(68)
			if($_SESSION['statut_vampigraine_adv']==1){if($_SESSION['pv']!=0){$statut_vampigraine_adv=1;$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);$_SESSION['pv']=$_SESSION['pv']+floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}}
			//victoire defaite?			
			if($_SESSION['pv']==0){$defaite=1;if($_SESSION['destin']==1){$_SESSION['pv_adv']=0;}}
			elseif($_SESSION['pv_adv']==0){$victoire=1;}	
			if($defaite==1){$attaque_donne=1;}
			if($victoire==1){$attaque_donne=1;}
			
			//DESC 1
			$desc_1_1="";$desc_1_2="";$desc_1_3="";$desc_1_4="";$desc_1_5="";$desc_1_6="";$desc_1_7="";$desc_1_8="";$desc_1_9="";$desc_1_10="";$desc_1_11="";$desc_1_12="";$desc_1_13="";$desc_1_14="";$desc_1_15="";$desc_1_16="";$desc_1_17="";$desc_1_18="";$desc_1_19="";$desc_1_20="";$desc_1_21="";$desc_1_22="";$desc_1_23="";$desc_1_24="";$desc_1_25="";$desc_1_26="";$desc_1_27="";$desc_1_28="";$desc_1_29="";$desc_1_30="";
			if($degel_adv==1){$desc_1_1=$_SESSION['nom_pokemon_adv'].' n\'est plus gelé <br />';}
			if($reveil_adv==1){$desc_1_2= $_SESSION['nom_pokemon_adv'].' est reveillé <br />';}
			if($statut_confus_end_adv==1){$desc_1_3= $_SESSION['nom_pokemon_adv'].' n\'est plus confus <br />'; $statut_confus_end_adv=0;}
			if($_SESSION['statut_confus_adv']>0 AND $statut_confus_begin_adv!=1){$desc_1_4= $_SESSION['nom_pokemon_adv'].' est confus <br />';}
			$desc_1_5= $_SESSION['nom_pokemon_adv'].' tente d\'utiliser l\'attaque <b><span style="font-size:20px;">'.$_SESSION['nom_attaque_adv'].'</span></b><br />';
			if($fuite_adv==1){$echec_attaque_adv=1;}
			if($echec_attaque_adv==1){$desc_1_6= 'l\'attaque a échoué <br />';}
			elseif($recharge_faite_adv==1){$desc_1_7= $_SESSION['nom_pokemon_adv'].' se repose <br />';}
			elseif($statut_dodo_adv==1){$desc_1_8= $_SESSION['nom_pokemon_adv'].' est endormi, il ne peut pas attaquer <br />';}
			elseif($statut_gel_adv==1){$desc_1_9= $_SESSION['nom_pokemon_adv'].' est gelé, il ne peut attaquer <br />';} 
			elseif($proba_paralyse_adv>75 AND $_SESSION['statut_paralyse_adv']==1){$desc_1_10= $_SESSION['nom_pokemon_adv'].' est paralysé, il ne peut attaquer <br />';}	
			elseif($peur_adv==1){$desc_1_11= $_SESSION['nom_pokemon_adv'].' a peur, il n\'attaque pas <br />';}					
			elseif($proba_attraction==1 AND $_SESSION['attraction']==1){$desc_1_12= $_SESSION['nom_pokemon_adv'].' refuse d\'attaquer <br />';}
			elseif($proba_confus_adv>50 AND $_SESSION['statut_confus_adv']>0){$desc_1_13= $_SESSION['nom_pokemon_adv'].' est confus, sa folie lui inflige des dégâts <br />';}
			elseif($charge_faite_adv==1){$desc_1_14= $_SESSION['nom_pokemon_adv'].' charge son attaque, dés le prochain tour, il pourra la lancer <br />';}
			else
				{
				if($cc_adv==1.5 AND $degats_adv!=0){$desc_1_15= 'coup critique!<br />';}
				if ($rafale_adv>1){$desc_1_16= 'Touché '.$rafale_adv.' fois <br />';}
				if ($_SESSION['puissance_attaque_adv']!=0){if ($multiplicateur_adv>1) {$desc_1_17= 'C\'est très efficace! <br />';} if($multiplicateur_adv==0 AND $degats_adv==0) {if($degats_adv==0){$desc_1_17= 'Ca n\'a aucun effet <br />';}} elseif($multiplicateur_adv<1){$desc_1_17= 'C\'est peu efficace <br />';}}
				if ($_SESSION['puissance_attaque_adv']!=0){$desc_1_18= 'Il inflige '.$degats_adv.' dégâts à '.$_SESSION['nom_pokemon'].'.<br />';}
				if($sangsue_adv>0){$desc_1_19= 'Il gagne '.$sangsue_adv.' PV <br />';}
				if ($_SESSION['id_effet_attaque_adv']!=0) 
					{
					$reponse = $bdd->prepare('SELECT * FROM pokemon_base_effets WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('id' => $_SESSION['id_effet_attaque_adv']));  
					$donnees = $reponse->fetch();
					if($_SESSION['id_effet_attaque_adv']==99){if($_SESSION['puissance_attaque_adv']){$donnees['descr_reussi']="";}} //cadeau
					if($effet_adv==1 AND $donnees['descr_reussi']!="") {$desc_1_20= $donnees['descr_reussi'].'<br />';} if($effet_adv==0 AND $donnees['descr_echec']!="") {$desc_1_20= $donnees['descr_echec'].'<br />';}
					}
				if ($_SESSION['id_effet2_attaque_adv']!=0) 
					{
					$reponse = $bdd->prepare('SELECT * FROM pokemon_base_effets WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('id' => $_SESSION['id_effet2_attaque_adv']));  
					$donnees = $reponse->fetch();
					if($effet_adv==1 AND $donnees['descr_reussi']!="") {$desc_1_21= $donnees['descr_reussi'].'<br />';} if($effet_adv==0 AND $donnees['descr_echec']!="") {$desc_1_21= $donnees['descr_echec'].'<br />';}
					}
				}
			if($triplattaque_brule_adv==1){$desc_1_22= $_SESSION['nom_pokemon'].' est brûlé par la triplattaque <br />';}
			if($triplattaque_paralyse_adv==1){$desc_1_23= $_SESSION['nom_pokemon'].' est paralysé par la triplattaque <br />';}
			if($triplattaque_gel_adv==1){$desc_1_24= $_SESSION['nom_pokemon'].' est gelé par la triplattaque <br />';}
			if($statut_poison_adv==1 ){$desc_1_25= $_SESSION['nom_pokemon_adv'].' souffre du poison <br />';} //poison
			if($statut_poison_grave_adv==1 ){$desc_1_26= $_SESSION['nom_pokemon_adv'].' souffre du poison <br />';} 
			if($statut_brule_adv==1 ){$desc_1_27= $_SESSION['nom_pokemon_adv'].' souffre de la brûlure <br />';} 
			if($danse_flamme_adv==1){$desc_1_28= $_SESSION['nom_pokemon_adv'].' souffre de danse flamme <br />';} 
			if($ligotage_adv==1){$desc_1_29= $_SESSION['nom_pokemon_adv'].' souffre de l\'étreinte <br />';} 
			if($statut_vampigraine_adv==1) {$desc_1_30= $_SESSION['nom_pokemon_adv'].' se fait drainer par la vampigraine<br />';} 
			$desc_1=$desc_1_1.''.$desc_1_2.''.$desc_1_3.''.$desc_1_4.''.$desc_1_5.''.$desc_1_6.''.$desc_1_7.''.$desc_1_8.''.$desc_1_9.''.$desc_1_10.''.$desc_1_11.''.$desc_1_12.''.$desc_1_13.''.$desc_1_14.''.$desc_1_15.''.$desc_1_16.''.$desc_1_17.''.$desc_1_18.''.$desc_1_19.''.$desc_1_20.''.$desc_1_21.''.$desc_1_22.''.$desc_1_23.''.$desc_1_24.''.$desc_1_25.''.$desc_1_26.''.$desc_1_27.''.$desc_1_28.''.$desc_1_29.''.$desc_1_30;
			$reponse = $bdd->prepare('UPDATE pokemons_description_blind_battles SET desc_1=:desc_1, pos_desc_1=:pos_desc_1 WHERE id_battles=:id_battles AND n_tour=:n_tour') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('desc_1' => $desc_1, 'pos_desc_1' => $ordre_attaque_adv, 'id_battles' =>$_POST['id_match'], 'n_tour' =>$nb_tours)); 
			
			//fin de peur
			$peur_adv=0;
			//ordre de tour
			if($attaque_donne==1){$premier_coup=3;$nb_tours=$nb_tours+1;} else {$premier_coup=1;}
			$attaque_donne=1;
			}
		if($premier_coup==1)//ATTAQUE DU POKEMON ALLIE
			{
			$_SESSION['destin']=0;
			if($attaque_donne==0){$ordre_attaque=1;}if($attaque_donne==1){$ordre_attaque=2;}
			$echec_attaque=0;
			$degel=0;$reveil=0;$fuite=0;$recharge_faite=0;$statut_dodo=0;$proba_confus=0;$statut_gel=0;$proba_paralyse=0;$proba_attraction_adv=0;$peur=0;$charge_faite=0;$sangsue=0;$triplattaque_brule=0;$triplattaque_paralyse=0;$triplattaque_gel=0;$statut_poison=0;$statut_poison_grave=0;$statut_brule=0;$danse_flamme=0;$ligotage=0;$statut_vampigraine=0;
			//78 métronome
			if($_SESSION['id_effet_attaque']==78 OR $_SESSION['id_effet2_attaque']==78) 
				{
				while($proba_metronome==0 OR $proba_metronome==17 OR $proba_metronome==40 OR $proba_metronome==45 OR $proba_metronome==84 OR $proba_metronome==140){$proba_metronome=rand(16,251);}
				$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id' =>$proba_metronome));
				$donnees = $reponse->fetch();
				$id_attaque=$proba_metronome;
				$_SESSION['nom_attaque'] = $donnees['nom']; $_SESSION['type_attaque'] = $donnees['type']; $_SESSION['puissance_attaque'] = $donnees['puissance'];
				$_SESSION['prec_attaque'] = $donnees['prec']; $_SESSION['cc_attaque'] = $donnees['cc']; $_SESSION['classe_attaque'] = $donnees['classe'];
				$_SESSION['priorite_attaque'] = $donnees['priorite']; $_SESSION['esquive_attaque'] = $donnees['esquive']; $_SESSION['cible_attaque'] = $donnees['cible'];
				$_SESSION['id_effet_attaque'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque'] = $donnees['id_effet2']; $_SESSION['proba_attaque'] = $donnees['proba'];
				}
                        //110 blabla dodo
                        if($_SESSION['id_effet_attaque']==110 OR $_SESSION['id_effet2_attaque']==110) 
                                {
                                $blabla_dodo_ok=0;$test_blabla_dodo=0;
                                while($blabla_dodo_ok==0 AND $test_blabla_dodo<10){
                                    $rand_blabladodo=rand(1,4);
                                    if($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque1_nb'];}
                                    elseif($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque2_nb'];}
                                    elseif($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque3_nb'];}
                                    elseif($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque4_nb'];}
                                    $test_blabla_dodo++;
                                    if($proba_blabla_dodo!=45 AND $proba_blabla_dodo!=342 AND $proba_blabla_dodo!=17 AND $proba_blabla_dodo!=21 AND $proba_blabla_dodo!=140 AND $proba_blabla_dodo!=84 AND $proba_blabla_dodo!=312 AND $proba_blabla_dodo!=40 AND $proba_blabla_dodo!=325){$blabla_dodo_ok=1;}
                                }
                                $echec_blabla_dodo=0;
                                if($blabla_dodo_ok==0){$echec_blabla_dodo=1;}
                                $reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
                                $reponse->execute(array('id' =>$proba_blabla_dodo));
                                $donnees = $reponse->fetch();
                                $attaque_lancee=$proba_metronome;
                                $_SESSION['nom_attaque'] = $donnees['nom']; $_SESSION['type_attaque'] = $donnees['type']; $_SESSION['puissance_attaque'] = $donnees['puissance'];
                                $_SESSION['prec_attaque'] = $donnees['prec']; $_SESSION['cc_attaque'] = $donnees['cc']; $_SESSION['classe_attaque'] = $donnees['classe'];
                                $_SESSION['priorite_attaque'] = $donnees['priorite']; $_SESSION['esquive_attaque'] = $donnees['esquive']; $_SESSION['cible_attaque'] = $donnees['cible'];
                                $_SESSION['id_effet_attaque'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque'] = $donnees['id_effet2']; $_SESSION['proba_attaque'] = $donnees['proba'];
                                }  
			$proba_effet=rand(1,100);
			if($_SESSION['proba_attaque']>=$proba_effet){$effet=1;} else {$effet=0;}	
			
			//EFFET des CLIMATS sur les attaques
			$soins=0.5;
			if($_SESSION['danse_pluie']>0) 
				{
				if($_SESSION['type_attaque']=="feu"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*0.5;} 
				if($_SESSION['type_attaque']=="eau"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*1.5;} 
				if($_SESSION['id_attaque']==40){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']/2;}//lance soleil
				if($_SESSION['id_attaque']==43){$_SESSION['prec_attaque']=100;}//fatal-foudre
				if($_SESSION['id_attaque']==68 OR $_SESSION['id_attaque']==141){$soins=0.25;}//synthèse et rayon lune
				//vent violent et aurore pas encore implémenté
				}
			if($_SESSION['zenith']>0) 
				{
				if($_SESSION['type_attaque']=="feu"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*1.5;} 
				if($_SESSION['type_attaque']=="eau"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*0.5;} 
				//lance soleil en un tour dans "charge"
				if($_SESSION['id_attaque']==68 OR $_SESSION['id_attaque']==141){$soins=0.66;}//synthèse et rayon lune
				}	
			if($_SESSION['tempete_sable']>0) 
				{
				if($_SESSION['id_attaque']==40){$_SESSION['puissance_attaque']=60;}//lance soleil
				if($_SESSION['id_attaque']==68 OR $_SESSION['id_attaque']==141){$soins=0.25;}//synthèse et rayon lune
				//aurore pas encore implémenté
				}	
			if($_SESSION['grele']>0) 
				{
				if($_SESSION['id_attaque']==33){$_SESSION['prec_attaque']=100;}//blizarre
				if($_SESSION['id_attaque']==40){$_SESSION['puissance_attaque']=60;}//lance soleil
				if($_SESSION['id_attaque']==68 OR $_SESSION['id_attaque']==141){$soins=0.25;}//synthèse et rayon lune
				//aurore pas encore implémenté
				}	
			
			//ECHEC AUTOMATIQUE
			//cage éclair sur roche
			if($id_attaque==121 AND $_SESSION['type_adv']=="roche" OR $id_attaque==121 AND $_SESSION['type2_adv']=="roche" OR $id_attaque==121 AND $_SESSION['type_adv']=="sol" OR $id_attaque==121 AND $_SESSION['type2_adv']=="sol"){$effet=0;}
			//guillotine et empla'korn sur spectre
			if($id_attaque==216 AND $_SESSION['type_adv']=="spectre" OR $id_attaque==216 AND $_SESSION['type2_adv']=="spectre" OR $id_attaque==133 AND $_SESSION['type_adv']=="spectre" OR $id_attaque==133 AND $_SESSION['type2_adv']=="spectre"){$effet=0;}
			//abime sur vol
			if($id_attaque==157 AND $_SESSION['type_adv']=="vol" OR $id_attaque==157 AND $_SESSION['type2_adv']=="vol"){$effet=0;}
		
				//calcul du multiplicateur
				$reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('type' => $_SESSION['type_attaque'], 'sur' => $_SESSION['type_adv']));  
				$donnees = $reponse->fetch();
				$multiplicateur1=$donnees['effet'];
				if($_SESSION['type2_adv']!="0")
				{$reponse2 = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
				$reponse2->execute(array('type' => $_SESSION['type_attaque'], 'sur' => $_SESSION['type2_adv']));  
				$donnees2 = $reponse2->fetch();
				$multiplicateur2=$donnees2['effet'];}
				else {$multiplicateur2=1;}
				$multiplicateur=$multiplicateur1*$multiplicateur2;
			
			if($multiplicateur==0 AND $_SESSION['cible_attaque']==1 AND $_SESSION['puissance_attaque']>0){$effet=0;}
			//verification que touche
			$hasard_precision=rand(1,100);
			$hasard_precision=$hasard_precision + $_SESSION['esq_adv']-$_SESSION['pre'];
                        if($echec_blabla_dodo==1){$hasard_precision=1000;}
				//diminution tour
					//confusion
					if($_SESSION['statut_confus']>0){$_SESSION['statut_confus']=$_SESSION['statut_confus']-1;}
					if($_SESSION['statut_confus']==0){$statut_confus_end=1;$_SESSION['statut_confus']=$_SESSION['statut_confus']-1;}
					//rune protect
					if($_SESSION['rune_protect']>0){$_SESSION['rune_protect']=$_SESSION['rune_protect']-1;}
					//danse flamme
					if($_SESSION['danse_flamme']>0){$_SESSION['danse_flamme']=$_SESSION['danse_flamme']-1;}
					//ligotage
					if($_SESSION['ligotage']>0){$_SESSION['ligotage']=$_SESSION['ligotage']-1;}
					//gel
					if($_SESSION['statut_gel']==1){$proba_fin_gel=rand(1,10); if($proba_fin_gel==1){$_SESSION['statut_gel']=0; $degel=1;}}
					//dodo
					if($_SESSION['statut_dodo']==1){$_SESSION['fin_dodo']=$_SESSION['fin_dodo']-1;	if($_SESSION['fin_dodo']==0){$_SESSION['statut_dodo']=0; $reveil=1;}}
					//mur lumiere
					if($_SESSION['mur_lumiere']>0){$_SESSION['mur_lumiere']=$_SESSION['mur_lumiere']-1;}
					//protection
					if($_SESSION['protection']>0){$_SESSION['protection']=$_SESSION['protection']-1;}
			$proba_paralyse=rand(1,100);
			$proba_attraction_adv=rand(1,2); 
			$proba_confus=rand(1,100);
			if($_SESSION['recharge']==1)	
				//souffre de recharge (48)
				{
				if($_SESSION['recharge']==1){$_SESSION['recharge']=0; $recharge_faite=1;$hasard_precision=1000;}
				}
			elseif($_SESSION['statut_dodo']==1)
				//souffre du sommeil(66/69)
				{
				if($_SESSION['statut_dodo']==1){$statut_dodo=1;$hasard_precision=1000;}			
				}
			elseif($_SESSION['statut_gel']==1)	
				//souffre du gel (47)
				{
                                if($_SESSION['id_attaque']!=331)
                                    {
                                    $statut_gel=1;
                                    $hasard_precision=1000;
                                    $_SESSION['charge']=0;
                                    }else
                                    {
                                    $_SESSION['statut_gel']=0;
                                    $degel=1;
                                    }
				}
			elseif($_SESSION['statut_paralyse']==1 AND $proba_paralyse>75)
				//souffre de paralysie(55)
				{
				$hasard_precision=1000;
				}
			elseif($peur==1)
				//souffre de peur (71)
				{
				if($peur==1){$hasard_precision=1000;}
				}
			elseif($_SESSION['attraction_adv']==1 AND $proba_attraction_adv==1)
				//souffre de attraction (67)
				{
				$hasard_precision=1000;
				$_SESSION['charge']=0;
				}
			elseif($_SESSION['statut_confus']>0 AND $proba_confus>50)
				//souffre de confusion
				{
				$hasard_precision=1000;
				$_SESSION['charge']=0;
				$statut_confus=1;
				$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/8); if($_SESSION['pv']<0){$_SESSION['pv']=0;}
				}
			else
				//54 charge
				{
				if($_SESSION['id_effet_attaque']==54 AND $effet==1 OR $_SESSION['id_effet2_attaque']==54 AND $effet==1) {if($_SESSION['charge']==0 AND $_SESSION['zenith']==0 OR $_SESSION['zenith']>0 AND $_SESSION['charge']==0 AND $_SESSION['id_attaque']!=40){$_SESSION['charge']=1;$charge_faite=1;$hasard_precision=1000;$_SESSION['attaque_auto']=1;} else {$_SESSION['charge']=0;}}
				}
				
				//87 si dodo
			if($_SESSION['id_effet_attaque']==87 AND $effet==1 OR $_SESSION['id_effet2_attaque']==87 AND $effet==1) {if($_SESSION['statut_dodo_adv']!=1){$hasard_precision=200;}}
				
				// vol et tunnel au tour 1 non offenssif
			if($_SESSION['esquive_attaque']==1 AND $_SESSION['charge']==0){$_SESSION['cible_attaque']=0;}	
				
				//esquive (abri)
			if($_SESSION['esquive_adv']==1){if($_SESSION['cible_attaque']==1 AND $_SESSION['id_attaque']!=44 OR $_SESSION['cible_attaque']==1 AND $_SESSION['id_attaque_adv']!=45){$hasard_precision=1500;}$_SESSION['esquive_adv']=0;}	
				if($hasard_precision<=$_SESSION['prec_attaque'])
				{
				//effets avant attaque
				$_SESSION['last_attaque']=$_POST['attaque'];
					//esquive (vol, tunnel)
				if($_SESSION['esquive_attaque']==1){if($_SESSION['charge']==0){$_SESSION['charge']=1;$charge_faite=1;$hasard_precision=1000;$_SESSION['esquive']=1;$_SESSION['attaque_auto']=1;$_SESSION['puissance_attaque']=0;} else {$_SESSION['charge']=0;}}
					//38 echoue
				if($_SESSION['id_effet_attaque']==38 AND $effet==1 OR $_SESSION['id_effet2_attaque']==38 AND $effet==1) {if($degats > 0) {$_SESSION['puissance_attaque']=0;$echec_attaque=1;}}
					//46 zénith
				if($_SESSION['id_effet_attaque']==46 AND $effet==1 OR $_SESSION['id_effet2_attaque']==46 AND $effet==1) {$_SESSION['zenith']=5;$_SESSION['grele']=0;$_SESSION['tempete_sable']=0;$_SESSION['danse_pluie']=0;}
				if($_SESSION['zenith']>0) {if($_SESSION['type_attaque']=="feu"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*1.5;} if($_SESSION['type_attaque']=="eau"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*0.5;} }			
					//43 grêle
				if($_SESSION['id_effet_attaque']==43 AND $effet==1 OR $_SESSION['id_effet2_attaque']==43 AND $effet==1) {$_SESSION['grele']=5;$_SESSION['zenith']=0;$_SESSION['tempete_sable']=0;$_SESSION['danse_pluie']=0;}
					//51 danse pluie
				if($_SESSION['id_effet_attaque']==51 AND $effet==1 OR $_SESSION['id_effet2_attaque']==51 AND $effet==1) {$_SESSION['danse_pluie']=5;$_SESSION['grele']=0;$_SESSION['zenith']=0;$_SESSION['tempete_sable']=0;}		
				if($_SESSION['danse_pluie']>0) {if($_SESSION['type_attaque']=="feu"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*0.5;} if($_SESSION['type_attaque']=="eau"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*1.5;} }
					//103 tempete de sable
				if($_SESSION['id_effet_attaque']==103 AND $effet==1 OR $_SESSION['id_effet2_attaque']==103 AND $effet==1) {$_SESSION['tempete_sable']=5;$_SESSION['grele']=0;$_SESSION['zenith']=0;$_SESSION['danse_pluie']=0;}									
					//104 ball'meteo
				if($_SESSION['id_effet_attaque']==104 AND $effet==1 OR $_SESSION['id_effet2_attaque']==104 AND $effet==1) 
					{
					if($_SESSION['danse_pluie']>0){$_SESSION['puissance_attaque']=100;$_SESSION['type_attaque']='eau';}
					elseif($_SESSION['zenith']>0){$_SESSION['puissance_attaque']=100;$_SESSION['type_attaque']='feu';}
					elseif($_SESSION['grele']>0){$_SESSION['puissance_attaque']=100;$_SESSION['type_attaque']='glace';}
					elseif($_SESSION['tempete_sable']>0){$_SESSION['puissance_attaque']=100;$_SESSION['type_attaque']='roche';}
					//calcul du multiplicateur
					$reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('type' => $_SESSION['type_attaque'], 'sur' => $_SESSION['type_adv']));  
					$donnees = $reponse->fetch();
					$multiplicateur1=$donnees['effet'];
					if($_SESSION['type2_adv']!="0")
					{$reponse2 = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
					$reponse2->execute(array('type' => $_SESSION['type_attaque'], 'sur' => $_SESSION['type2_adv']));  
					$donnees2 = $reponse2->fetch();
					$multiplicateur2=$donnees2['effet'];}
					else {$multiplicateur2=1;}
					$multiplicateur=$multiplicateur1*$multiplicateur2;
					}
                                        //115 force cachée
                                if($_SESSION['id_effet_attaque']==115 AND $effet==1 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==115 AND $effet==1 AND $_SESSION['statut_brule_adv']==0 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
                                if($_SESSION['id_effet_attaque']==115 AND $effet==1 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==115 AND $effet==1 AND $_SESSION['rune_protect_adv']==0) 
                                    {
                                    if($_SESSION['danse_pluie']>0){$_SESSION['id_effet_attaque']=2;}
                                    elseif($_SESSION['zenith']>0){$_SESSION['id_effet_attaque']=71;}
                                    elseif($_SESSION['grele']>0){$_SESSION['id_effet_attaque']=47;}
                                    elseif($_SESSION['tempete_sable']>0){$_SESSION['id_effet_attaque']=35;}
                                    else{$_SESSION['id_effet_attaque']=55;}
                                    }
				//44 rafale
				$rafale=1;
				if($_SESSION['id_effet_attaque']==44 AND $effet==1 OR $_SESSION['id_effet2_attaque']==44 AND $effet==1) {$proba_rafale=rand(1,100); if($proba_rafale<38){$rafale=2;} elseif($proba_rafale<=75){$rafale=3;} elseif($proba_rafale<=87){$rafale=4;}else{$rafale=5;}}
					//45 puissance cachée
				if($_SESSION['id_effet_attaque']==45 AND $effet==1 OR $_SESSION['id_effet2_attaque']==45 AND $effet==1) {$proba_type_attaque=rand(1,17); $reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE id=:id') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id' => $proba_type_attaque)); $donnees = $reponse->fetch();$_SESSION['type_attaque'] = $donnees['sur']; $_SESSION['puissance_attaque'] = rand(1,100); }
					//48 recharge
				if($_SESSION['id_effet_attaque']==48 AND $effet==1 OR $_SESSION['id_effet2_attaque']==48 AND $effet==1) {$_SESSION['recharge']=1; }
					//49 mur lumière
				if($_SESSION['id_effet_attaque']==49 AND $effet==1 OR $_SESSION['id_effet2_attaque']==49 AND $effet==1) {$_SESSION['mur_lumiere']=5; }
					//50 abri
				if($_SESSION['id_effet_attaque']==50 AND $effet==1 OR $_SESSION['id_effet2_attaque']==50 AND $effet==1) {$abri=1; $_SESSION['esquive']=1;}
					//125 tenacite
                                $tenacite=0;
                                if($_SESSION['id_effet_attaque']==125 AND $effet==1 OR $_SESSION['id_effet2_attaque']==125 AND $effet==1) {$tenacite=1;}	
                                        //53 rune protect
				if($_SESSION['id_effet_attaque']==53 AND $effet==1 OR $_SESSION['id_effet2_attaque']==53 AND $effet==1) {$_SESSION['rune_protect']=6;}
					//57 casse brique
				if($_SESSION['id_effet_attaque']==57 AND $effet==1 OR $_SESSION['id_effet2_attaque']==57 AND $effet==1) {$_SESSION['mur_lumiere_adv']=0;$_SESSION['protection_adv']=0;}
					//58 esq +
				if($_SESSION['id_effet_attaque']==58 AND $effet==1 OR $_SESSION['id_effet2_attaque']==58 AND $effet==1) {$_SESSION['esq']=$_SESSION['esq']+5;if($_SESSION['esq']>30){$_SESSION['esq']=30;}if($_SESSION['esq']<-30){$_SESSION['esq']=-30;}}
					//59 esq ++
				if($_SESSION['id_effet_attaque']==59 AND $effet==1 OR $_SESSION['id_effet2_attaque']==59 AND $effet==1) {$_SESSION['esq']=$_SESSION['esq']+10;if($_SESSION['esq']>30){$_SESSION['esq']=30;}if($_SESSION['esq']<-30){$_SESSION['esq']=-30;}}
					//60 esq +++
				if($_SESSION['id_effet_attaque']==60 AND $effet==1 OR $_SESSION['id_effet2_attaque']==60 AND $effet==1) {$_SESSION['esq']=$_SESSION['esq']+15;if($_SESSION['esq']>30){$_SESSION['esq']=30;}if($_SESSION['esq']<-30){$_SESSION['esq']=-30;}}
					//61 esq -
				if($_SESSION['id_effet_attaque']==61 AND $effet==1 OR $_SESSION['id_effet2_attaque']==61 AND $effet==1) {$_SESSION['esq_adv']=$_SESSION['esq_adv']-5;if($_SESSION['esq_adv']>30){$_SESSION['esq_adv']=30;}if($_SESSION['esq_adv']<-30){$_SESSION['esq_adv']=-30;}}
					//62 esq --
				if($_SESSION['id_effet_attaque']==62 AND $effet==1 OR $_SESSION['id_effet2_attaque']==62 AND $effet==1) {$_SESSION['esq_adv']=$_SESSION['esq_adv']-10;if($_SESSION['esq_adv']>30){$_SESSION['esq_adv']=30;}if($_SESSION['esq_adv']<-30){$_SESSION['esq_adv']=-30;}}
					//63 esq ---
				if($_SESSION['id_effet_attaque']==63 AND $effet==1 OR $_SESSION['id_effet2_attaque']==63 AND $effet==1) {$_SESSION['esq_adv']=$_SESSION['esq_adv']-15;if($_SESSION['esq_adv']>30){$_SESSION['esq_adv']=30;}if($_SESSION['esq_adv']<-30){$_SESSION['esq_adv']=-30;}}
					//64 protection
				if($_SESSION['id_effet_attaque']==64 AND $effet==1 OR $_SESSION['id_effet2_attaque']==64 AND $effet==1) {$_SESSION['protection']=5; }
					//66 repos
				if($_SESSION['id_effet_attaque']==66 AND $_SESSION['pv']==$_SESSION['pv_max'] OR $_SESSION['id_effet2_attaque']==66 AND $_SESSION['pv']==$_SESSION['pv_max']){$effet=0;}
				if($_SESSION['id_effet_attaque']==66 AND $effet==1 OR $_SESSION['id_effet2_attaque']==66 AND $effet==1) {$lance_repos=1;$_SESSION['statut_dodo']=1;$_SESSION['fin_dodo']=3;$_SESSION['statut_poison']=0;$_SESSION['statut_poison_grave']=0;if($_SESSION['statut_paralyse']==1){$_SESSION['vit']=$donnees['vit']*4;}$_SESSION['statut_paralyse']=0;$_SESSION['statut_brule']=0;$_SESSION['pv']=$_SESSION['pv_max'];}
					//67 attraction
				if($_SESSION['id_effet_attaque']==67 AND $effet==1 OR $_SESSION['id_effet2_attaque']==67 AND $effet==1) {if($_SESSION['sexe']=="M" AND $_SESSION['sexe_adv']=="F" OR $_SESSION['sexe']=="F" AND $_SESSION['sexe_adv']=="M"){$_SESSION['attraction']=1 ;}}
					//70 soin
				if($_SESSION['id_effet_attaque']==70 AND $effet==1 OR $_SESSION['id_effet2_attaque']==70 AND $effet==1) {$_SESSION['pv']=$_SESSION['pv']+floor($_SESSION['pv_max']*$soins);if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}}
					//107 aurore
                                if($_SESSION['zenith']>0){$multi_soins_climat=0.66;}
                                elseif($_SESSION['danse_pluie']>0 OR $_SESSION['tempete_sable']>0 OR $_SESSION['grele']>0){$multi_soins_climat=0.25;}
                                if($_SESSION['id_effet_attaque']==107 AND $effet==1 OR $_SESSION['id_effet2_attaque']==107 AND $effet==1) {$_SESSION['pv']=$_SESSION['pv']+floor($_SESSION['pv_max']*$multi_soins_climat);if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}}
                                //71 peur
				if($_SESSION['id_effet_attaque']==71 AND $ordre_attaque==2 OR $_SESSION['id_effet2_attaque']==71 AND $ordre_attaque==2) {$effet=0;}
				if($_SESSION['id_effet_attaque']==71 AND $effet==1 OR $_SESSION['id_effet2_attaque']==71 AND $effet==1) {$peur_adv=1;}
				//123 piqué appeurement
				if($_SESSION['id_effet_attaque']==123 AND $ordre_attaque==2 OR $_SESSION['id_effet2_attaque']==123 AND $ordre_attaque==2) {$effet=0;}
                                if($_SESSION['id_effet_attaque']==123 AND $effet==1 OR $_SESSION['id_effet2_attaque']==123 AND $effet==1) {$rand_pr=rand(1,100); if($rand_pr<=30){$peur_adv=1;}}
				//124 ronflementt
                                if($_SESSION['id_effet_attaque']==124 AND $ordre_attaque==2 OR $_SESSION['id_effet2_attaque']==124 AND $ordre_attaque==2) {$effet=0;}
                                if($_SESSION['id_effet_attaque']==124 AND $effet==1 OR $_SESSION['id_effet2_attaque']==124 AND $effet==1) {if($_SESSION['statut_dodo']>0){$rand_pr=rand(1,100); if($rand_pr<=30){$peur_adv=1;}}}
				//72 danse flamme
				if($_SESSION['id_effet_attaque']==72 AND $effet==1 OR $_SESSION['id_effet2_attaque']==72 AND $effet==1) {$_SESSION['danse_flamme_adv']=rand(3,6);}
					//73 tour rapide
				if($_SESSION['id_effet_attaque']==73 AND $effet==1 OR $_SESSION['id_effet2_attaque']==73 AND $effet==1) {$_SESSION['statut_vampigraine']=0;$_SESSION['danse_flamme']=0;$_SESSION['ligotage']=0;}	
					//74 cc++
				if($_SESSION['id_effet_attaque']==74 AND $effet==1 OR $_SESSION['id_effet2_attaque']==74 AND $effet==1) {$_SESSION['bonus_cc']=10; }	
					//75 croc fatal
				if($_SESSION['id_effet_attaque']==75 AND $effet==1 OR $_SESSION['id_effet2_attaque']==75 AND $effet==1) {$_SESSION['pv_adv']=ceil($_SESSION['pv_adv']/2); }		
					//109 balance
                                if($_SESSION['id_effet_attaque']==109 AND $effet==1 OR $_SESSION['id_effet2_attaque']==109 AND $effet==1) {$pv_balance=floor(($_SESSION['pv_adv']+$_SESSION['pv'])/2);$_SESSION['pv_adv']=$pv_balance;if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}$_SESSION['pv']=$pv_balance;if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}}		
				//77 ligotage
				if($_SESSION['id_effet_attaque']==77 AND $effet==1 OR $_SESSION['id_effet2_attaque']==77 AND $effet==1) {$_SESSION['ligotage_adv']=rand(3,6);}	
					//79 esuna
				if($_SESSION['id_effet_attaque']==79 AND $effet==1 OR $_SESSION['id_effet2_attaque']==79 AND $effet==1) { $_SESSION['statut_confus']=-1;$_SESSION['statut_poison']=0;$_SESSION['statut_poison_grave']=0;$_SESSION['statut_gel']=0;if($_SESSION['statut_paralyse']==1){$_SESSION['vit']=$donnees['vit']*4;}$_SESSION['statut_paralyse']=0;$_SESSION['statut_brule']=0;$_SESSION['statut_dodo']=0;}	
					//84 lance boue
				if($_SESSION['id_effet_attaque']==84 AND $effet==1 OR $_SESSION['id_effet2_attaque']==84 AND $effet==1) {$_SESSION['lance_boue']=1;}	
					//97 tourniquet
				if($_SESSION['id_effet_attaque']==97 AND $effet==1 OR $_SESSION['id_effet2_attaque']==97 AND $effet==1) {$_SESSION['tourniquet']=1;}	
					//89 morphing
				if($_SESSION['id_effet_attaque']==89 AND $effet==1 OR $_SESSION['id_effet2_attaque']==89 AND $effet==1) 	
					{
					$_SESSION['type']=$_SESSION['type_adv'];$_SESSION['type2']=$_SESSION['type2_adv'];
					$_SESSION['attaque1_nb']=$_SESSION['attaque1_nb_adv'];$_SESSION['attaque2_nb']=$_SESSION['attaque2_nb_adv'];$_SESSION['attaque3_nb']=$_SESSION['attaque3_nb_adv'];$_SESSION['attaque4_nb']=$_SESSION['attaque4_nb_adv'];
					$_SESSION['nom_attaque1'] = $_SESSION['nom_attaque1_adv']; $_SESSION['type_attaque1'] = $_SESSION['type_attaque1_adv']; $_SESSION['puissance_attaque1'] = $_SESSION['puissance_attaque1_adv'];
					$_SESSION['prec_attaque1'] = $_SESSION['prec_attaque1_adv']; $_SESSION['cc_attaque1'] = $_SESSION['cc_attaque1_adv']; $_SESSION['classe_attaque1'] = $_SESSION['classe_attaque1_adv'];
					$_SESSION['priorite_attaque1'] = $_SESSION['priorite_attaque1_adv']; $_SESSION['esquive_attaque1'] = $_SESSION['esquive_attaque1_adv']; $_SESSION['cible_attaque1'] = $_SESSION['cible_attaque1_adv'];
					$_SESSION['id_effet_attaque1'] = $_SESSION['id_effet_attaque1_adv']; $_SESSION['id_effet2_attaque1'] = $_SESSION['id_effet2_attaque1_adv']; $_SESSION['proba_attaque1'] = $_SESSION['proba_attaque1_adv'];
					$_SESSION['nom_attaque2'] = $_SESSION['nom_attaque2_adv']; $_SESSION['type_attaque2'] = $_SESSION['type_attaque2_adv']; $_SESSION['puissance_attaque2'] = $_SESSION['puissance_attaque2_adv'];
					$_SESSION['prec_attaque2'] = $_SESSION['prec_attaque2_adv']; $_SESSION['cc_attaque2'] = $_SESSION['cc_attaque2_adv']; $_SESSION['classe_attaque2'] = $_SESSION['classe_attaque2_adv'];
					$_SESSION['priorite_attaque2'] = $_SESSION['priorite_attaque2_adv']; $_SESSION['esquive_attaque2'] = $_SESSION['esquive_attaque2_adv']; $_SESSION['cible_attaque2'] = $_SESSION['cible_attaque2_adv'];
					$_SESSION['id_effet_attaque2'] = $_SESSION['id_effet_attaque2_adv']; $_SESSION['id_effet2_attaque2'] = $_SESSION['id_effet2_attaque2_adv']; $_SESSION['proba_attaque2'] = $_SESSION['proba_attaque2_adv'];
					$_SESSION['nom_attaque3'] = $_SESSION['nom_attaque3_adv']; $_SESSION['type_attaque3'] = $_SESSION['type_attaque3_adv']; $_SESSION['puissance_attaque3'] = $_SESSION['puissance_attaque3_adv'];
					$_SESSION['prec_attaque3'] = $_SESSION['prec_attaque3_adv']; $_SESSION['cc_attaque3'] = $_SESSION['cc_attaque3_adv']; $_SESSION['classe_attaque3'] = $_SESSION['classe_attaque3_adv'];
					$_SESSION['priorite_attaque3'] = $_SESSION['priorite_attaque3_adv']; $_SESSION['esquive_attaque3'] = $_SESSION['esquive_attaque3_adv']; $_SESSION['cible_attaque3'] = $_SESSION['cible_attaque3_adv'];
					$_SESSION['id_effet_attaque3'] = $_SESSION['id_effet_attaque3_adv']; $_SESSION['id_effet2_attaque3'] = $_SESSION['id_effet2_attaque3_adv']; $_SESSION['proba_attaque3'] = $_SESSION['proba_attaque3_adv'];
					$_SESSION['nom_attaque4'] = $_SESSION['nom_attaque4_adv']; $_SESSION['type_attaque4'] = $_SESSION['type_attaque4_adv']; $_SESSION['puissance_attaque4'] = $_SESSION['puissance_attaque4_adv'];
					$_SESSION['prec_attaque4'] = $_SESSION['prec_attaque4_adv']; $_SESSION['cc_attaque4'] = $_SESSION['cc_attaque4_adv']; $_SESSION['classe_attaque4'] = $_SESSION['classe_attaque4_adv'];
					$_SESSION['priorite_attaque4'] = $_SESSION['priorite_attaque4_adv']; $_SESSION['esquive_attaque4'] = $_SESSION['esquive_attaque4_adv']; $_SESSION['cible_attaque4'] = $_SESSION['cible_attaque4_adv'];
					$_SESSION['id_effet_attaque4'] = $_SESSION['id_effet_attaque4_adv']; $_SESSION['id_effet2_attaque4'] = $_SESSION['id_effet2_attaque4_adv']; $_SESSION['proba_attaque4'] = $_SESSION['proba_attaque4_adv'];
					$_SESSION['att']=$_SESSION['att_adv'];
					$_SESSION['def']=$_SESSION['def_adv'];
					$_SESSION['vit']=$_SESSION['vit_adv'];
					$_SESSION['attspe']=$_SESSION['attspe_adv'];
					$_SESSION['defspe']=$_SESSION['defspe_adv'];
					$_SESSION['att_max']=$_SESSION['att_max_adv'];
					$_SESSION['def_max']=$_SESSION['def_max_adv'];
					$_SESSION['vit_max']=$_SESSION['vit_max_adv'];
					$_SESSION['attspe_max']=$_SESSION['attspe_max_adv'];
					$_SESSION['defspe_max']=$_SESSION['defspe_max_adv'];
					}
				//96 mania
				if($_SESSION['id_effet_attaque']==96 AND $effet==1 OR $_SESSION['id_effet2_attaque']==96 AND $effet==1) 
					{
					if($_SESSION['mania']==0){$_SESSION['mania']=rand(1,2);$_SESSION['attaque_auto']=1;$effet=0;}
					elseif($_SESSION['mania']==1){$_SESSION['mania']=0;$_SESSION['statut_confus']=rand(2,5);$statut_confus_begin=1;}
					elseif($_SESSION['mania']>1){$_SESSION['mania']=$_SESSION['mania']-1;$_SESSION['attaque_auto']=1;$effet=0;}
					}
				//100 gribouille
				if($_SESSION['id_effet_attaque']==100 AND $effet==1 OR $_SESSION['id_effet2_attaque']==100 AND $effet==1) 
					{
					if($_SESSION['last_attaque_adv']!=0 AND $_SESSION['last_attaque_adv']!=252) //pas lutte
						{
						$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_blind_battles WHERE id_battles=:id_battles AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id_battles' => $_POST['id_match'], 'pseudo' => $_SESSION['pseudo']));  
						$donnees = $reponse->fetch();//pokemon à soi
						if($donnees['attaque1']==300)
							{
							$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons_blind_battles SET attaque1=:attaque1 WHERE id_battles=:id_battles AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
							$reponse->execute(array('attaque1' => $_SESSION['last_attaque_adv'], 'id_battles' => $_POST['id_match'], 'pseudo' => $_SESSION['pseudo'])); 
							}
						elseif($donnees['attaque2']==300)
							{
							$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons_blind_battles SET attaque2=:attaque2 WHERE id_battles=:id_battles AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
							$reponse->execute(array('attaque2' => $_SESSION['last_attaque_adv'], 'id_battles' => $_POST['id_match'], 'pseudo' => $_SESSION['pseudo'])); 
							}
						elseif($donnees['attaque3']==300)
							{
							$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons_blind_battles SET attaque3=:attaque3 WHERE id_battles=:id_battles AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
							$reponse->execute(array('attaque3' => $_SESSION['last_attaque_adv'], 'id_battles' => $_POST['id_match'], 'pseudo' => $_SESSION['pseudo']));
							}
						elseif($donnees['attaque4']==300)
							{
							$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons_blind_battles SET attaque4=:attaque4 WHERE id_battles=:id_battles AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
							$reponse->execute(array('attaque4' => $_SESSION['last_attaque_adv'], 'id_battles' => $_POST['id_match'], 'pseudo' => $_SESSION['pseudo']));
							}
						else{$echec_attaque=1;}
						$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_blind_battles WHERE id_battles=:id_battles AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id_battles' => $_POST['id_match'], 'pseudo' => $_SESSION['pseudo']));  
						$donnees = $reponse->fetch();//pokemon à soi
						$_SESSION['attaque1_nb']=$donnees['attaque1'];
						$_SESSION['attaque2_nb']=$donnees['attaque2'];
						$_SESSION['attaque3_nb']=$donnees['attaque3'];
						$_SESSION['attaque4_nb']=$donnees['attaque4'];
						$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $_SESSION['attaque1_nb']));  
						$donnees = $reponse->fetch();
								$_SESSION['nom_attaque1'] = $donnees['nom']; $_SESSION['type_attaque1'] = $donnees['type']; $_SESSION['puissance_attaque1'] = $donnees['puissance'];
								$_SESSION['prec_attaque1'] = $donnees['prec']; $_SESSION['cc_attaque1'] = $donnees['cc']; $_SESSION['classe_attaque1'] = $donnees['classe'];
								$_SESSION['priorite_attaque1'] = $donnees['priorite']; $_SESSION['esquive_attaque1'] = $donnees['esquive']; $_SESSION['cible_attaque1'] = $donnees['cible'];
								$_SESSION['id_effet_attaque1'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque1'] = $donnees['id_effet2']; $_SESSION['proba_attaque1'] = $donnees['proba'];
						$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $_SESSION['attaque2_nb']));  
						$donnees = $reponse->fetch();
								$_SESSION['nom_attaque2'] = $donnees['nom']; $_SESSION['type_attaque2'] = $donnees['type']; $_SESSION['puissance_attaque2'] = $donnees['puissance'];
								$_SESSION['prec_attaque2'] = $donnees['prec']; $_SESSION['cc_attaque2'] = $donnees['cc']; $_SESSION['classe_attaque2'] = $donnees['classe'];
								$_SESSION['priorite_attaque2'] = $donnees['priorite']; $_SESSION['esquive_attaque2'] = $donnees['esquive']; $_SESSION['cible_attaque2'] = $donnees['cible'];
								$_SESSION['id_effet_attaque2'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque2'] = $donnees['id_effet2']; $_SESSION['proba_attaque2'] = $donnees['proba'];
						$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $_SESSION['attaque3_nb']));  
						$donnees = $reponse->fetch();
								$_SESSION['nom_attaque3'] = $donnees['nom']; $_SESSION['type_attaque3'] = $donnees['type']; $_SESSION['puissance_attaque3'] = $donnees['puissance'];
								$_SESSION['prec_attaque3'] = $donnees['prec']; $_SESSION['cc_attaque3'] = $donnees['cc']; $_SESSION['classe_attaque3'] = $donnees['classe'];
								$_SESSION['priorite_attaque3'] = $donnees['priorite']; $_SESSION['esquive_attaque3'] = $donnees['esquive']; $_SESSION['cible_attaque3'] = $donnees['cible'];
								$_SESSION['id_effet_attaque3'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque3'] = $donnees['id_effet2']; $_SESSION['proba_attaque3'] = $donnees['proba'];
						$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $_SESSION['attaque4_nb']));  
						$donnees = $reponse->fetch();
								$_SESSION['nom_attaque4'] = $donnees['nom']; $_SESSION['type_attaque4'] = $donnees['type']; $_SESSION['puissance_attaque4'] = $donnees['puissance'];
								$_SESSION['prec_attaque4'] = $donnees['prec']; $_SESSION['cc_attaque4'] = $donnees['cc']; $_SESSION['classe_attaque4'] = $donnees['classe'];
								$_SESSION['priorite_attaque4'] = $donnees['priorite']; $_SESSION['esquive_attaque4'] = $donnees['esquive']; $_SESSION['cible_attaque4'] = $donnees['cible'];
								$_SESSION['id_effet_attaque4'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque4'] = $donnees['id_effet2']; $_SESSION['proba_attaque4'] = $donnees['proba'];
						}
					else{$echec_attaque=1;}
					}
						//105 prélevement destin	
					if($_SESSION['id_effet_attaque']==105 AND $effet==1 OR $_SESSION['id_effet2_attaque']==105 AND $effet==1) {$_SESSION['destin']=1;}
                                                //106 ampleur	
                                        if($_SESSION['id_effet_attaque']==106 AND $effet==1 OR $_SESSION['id_effet2_attaque']==106 AND $effet==1) {$_SESSION['puissance_attaque']=rand(10,150);if($_SESSION['esquive_attaque_adv']==1 AND $_SESSION['last_attaque_adv']==45){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*2;}}
                                                //117 contre
                                        if($_SESSION['id_effet_attaque']==117 AND $effet==1 OR $_SESSION['id_effet2_attaque']==117 AND $effet==1) {
                                            $contre=($_SESSION['pv']/$_SESSION['pv_max'])*100;
                                            if($contre<3.12){$_SESSION['puissance_attaque']=200;}
                                            elseif($contre<9.37){$_SESSION['puissance_attaque']=150;}
                                            elseif($contre<20.31){$_SESSION['puissance_attaque']=100;}
                                            elseif($contre<34.37){$_SESSION['puissance_attaque']=80;}
                                            elseif($contre<67.19){$_SESSION['puissance_attaque']=40;}
                                            else{$_SESSION['puissance_attaque']=20;}
                                            }
                                            //119 éruption	
                                        if($_SESSION['id_effet_attaque']==119 AND $effet==1 OR $_SESSION['id_effet2_attaque']==119 AND $effet==1) {$_SESSION['puissance_attaque']=150*$_SESSION['pv']/$_SESSION['pv_max'];}
                                            //114 façade
                                        if($_SESSION['id_effet_attaque']==114 AND $effet==1 OR $_SESSION['id_effet2_attaque']==114 AND $effet==1) {if($_SESSION['statut_poison']>0 OR $_SESSION['statut_poison_grave']>0 OR $_SESSION['statut_brule']>0 OR $_SESSION['statut_paralyse']>0){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*2;}}
                                        //112 frustration
                                        if($_SESSION['id_effet_attaque']==112 AND $effet==1 OR $_SESSION['id_effet2_attaque']==112 AND $effet==1) {$_SESSION['puissance_attaque']=102-ceil($_SESSION['bonheur']*2/5);if($_SESSION['puissance_attaque']<1){$_SESSION['puissance_attaque']=1;}if($_SESSION['puissance_attaque']>102){$_SESSION['puissance_attaque']=102;}}
                                        //113 retour
                                        if($_SESSION['id_effet_attaque']==113 AND $effet==1 OR $_SESSION['id_effet2_attaque']==113 AND $effet==1) {$_SESSION['puissance_attaque']=ceil($_SESSION['bonheur']*2/5);if($_SESSION['puissance_attaque']<1){$_SESSION['puissance_attaque']=1;}if($_SESSION['puissance_attaque']>102){$_SESSION['puissance_attaque']=102;}}
			
				//effets variation de statut
					//39 confusion
				if($_SESSION['id_effet_attaque']==39 AND $effet==1 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==39 AND $effet==1 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
				if($_SESSION['id_effet_attaque']==39 AND $effet==1 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==39 AND $effet==1 AND $_SESSION['rune_protect_adv']==0) {if($_SESSION['statut_confus_adv']==-1){$_SESSION['statut_confus_adv']=rand(1,4);if($ordre_attaque==2){$statut_confus_begin_adv=1;}}else{$effet=0;}}
					//41 empoisonnement
				if($_SESSION['id_effet_attaque']==41 AND $effet==1 AND $_SESSION['statut_poison_adv']==0 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==41 AND $effet==1 AND $_SESSION['statut_poison_adv']==0 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
				if($_SESSION['id_effet_attaque']==41 AND $effet==1 AND $_SESSION['statut_poison_adv']!=0 OR $_SESSION['id_effet2_attaque']==41 AND $effet==1 AND $_SESSION['statut_poison_adv']!=0){$effet=0;}
				if($_SESSION['id_effet_attaque']==41 AND $_SESSION['type_adv']=="poison" OR $_SESSION['id_effet_attaque']==41 AND $_SESSION['type2_adv']=="poison" OR $_SESSION['id_effet_attaque']==41 AND $_SESSION['type_adv']=="acier" OR $_SESSION['id_effet_attaque']==41 AND $_SESSION['type2_adv']=="acier"){$effet=0;}
				if($_SESSION['id_effet_attaque']==41 AND $effet==1 AND $_SESSION['statut_poison_adv']==0 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==41 AND $effet==1 AND $_SESSION['statut_poison_adv']==0 AND $_SESSION['rune_protect_adv']==0) {$_SESSION['statut_poison_adv']=1;}
					//42 empoisonnement grave
				if($_SESSION['id_effet_attaque']==42 AND $effet==1 AND $_SESSION['statut_poison_grave_adv']==0 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==42 AND $effet==1 AND $_SESSION['statut_poison_grave_adv']==0 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
				if($_SESSION['id_effet_attaque']==42 AND $_SESSION['type_adv']=="poison" OR $_SESSION['id_effet_attaque']==42 AND $_SESSION['type2_adv']=="poison" OR $_SESSION['id_effet_attaque']==42 AND $_SESSION['type_adv']=="acier" OR $_SESSION['id_effet_attaque']==42 AND $_SESSION['type2_adv']=="acier"){$effet=0;}			
				if($_SESSION['id_effet_attaque']==42 AND $effet==1 AND $_SESSION['statut_poison_grave_adv']==0 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==42 AND $effet==1 AND $_SESSION['statut_poison_grave_adv']==0 AND $_SESSION['rune_protect_adv']==0) {$_SESSION['statut_poison_grave_adv']=1;}
					//47 gel
				if($_SESSION['id_effet_attaque']==47 AND $_SESSION['type_adv']=="glace" OR $_SESSION['id_effet_attaque']==47 AND $_SESSION['type2_adv']=="glace"){$effet=0;}
				if($_SESSION['id_effet_attaque']==47 AND $effet==1 AND $_SESSION['statut_gel_adv']!=1 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==47 AND $effet==1 AND $_SESSION['statut_gel_adv']!=1 AND $_SESSION['rune_protect_adv']!=0) {$effet=0; }
				if($_SESSION['id_effet_attaque']==47 AND $effet==1 AND $_SESSION['statut_gel_adv']!=0 OR $_SESSION['id_effet2_attaque']==47 AND $effet==1 AND $_SESSION['statut_gel_adv']!=0){$effet=0;}				
				if($_SESSION['id_effet_attaque']==47 AND $effet==1 AND $_SESSION['statut_gel_adv']!=1 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==47 AND $effet==1 AND $_SESSION['statut_gel_adv']!=1 AND $_SESSION['rune_protect_adv']==0) {$_SESSION['statut_gel_adv']=1; }
					//55 paralysie
				if($_SESSION['id_effet_attaque']==55 AND $effet==1 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==55 AND $effet==1 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
				if($_SESSION['id_effet_attaque']==55 AND $effet==1 AND $_SESSION['statut_paralyse_adv']!=0 OR $_SESSION['id_effet2_attaque']==55 AND $effet==1 AND $_SESSION['statut_paralyse_adv']!=0){$effet=0;}	
				if($_SESSION['id_effet_attaque']==55 AND $effet==1 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==55 AND $effet==1 AND $_SESSION['rune_protect_adv']==0) {$_SESSION['statut_paralyse_adv']=1; $_SESSION['vit_adv']=$_SESSION['vit_adv']/4;}
					//65 brule
				if($_SESSION['id_effet_attaque']==65 AND $_SESSION['type_adv']=="feu" OR $_SESSION['id_effet_attaque']==65 AND $_SESSION['type2_adv']=="feu"){$effet=0;}
				if($_SESSION['id_effet_attaque']==65 AND $effet==1 AND $_SESSION['statut_brule_adv']==0 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==65 AND $effet==1 AND $_SESSION['statut_brule_adv']==0 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
				if($_SESSION['id_effet_attaque']==65 AND $effet==1 AND $_SESSION['statut_brule_adv']!=0 OR $_SESSION['id_effet2_attaque']==65 AND $effet==1 AND $_SESSION['statut_brule_adv']!=0){$effet=0;}
				if($_SESSION['id_effet_attaque']==65 AND $effet==1 AND $_SESSION['statut_brule_adv']==0 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==65 AND $effet==1 AND $_SESSION['statut_brule_adv']==0 AND $_SESSION['rune_protect_adv']==0) {$_SESSION['statut_brule_adv']=1;$_SESSION['att_adv']=$_SESSION['att_adv']/2;}
					//69 sommeil
				if($_SESSION['id_effet_attaque']==69 AND $effet==1 AND $_SESSION['statut_dodo_adv']!=1 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==69 AND $effet==1 AND $_SESSION['statut_dodo_adv']!=1 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
				if($_SESSION['id_effet_attaque']==69 AND $effet==1 AND $_SESSION['statut_dodo_adv']!=0 OR $_SESSION['id_effet2_attaque']==69 AND $effet==1 AND $_SESSION['statut_dodo_adv']!=0){$effet=0;}	
				if($_SESSION['id_effet_attaque']==69 AND $effet==1 AND $_SESSION['statut_dodo_adv']!=1 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==69 AND $effet==1 AND $_SESSION['statut_dodo_adv']!=1 AND $_SESSION['rune_protect_adv']==0) {$_SESSION['statut_dodo_adv']=1;$_SESSION['fin_dodo_adv']=rand(1,7);if($ordre_attaque==2){$dodo_adv_now=1;}}
					//80 triplattaque
				if($_SESSION['id_effet_attaque']==80 AND $effet==1 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==80 AND $effet==1 AND $_SESSION['statut_brule_adv']==0 AND $_SESSION['rune_protect_adv']!=0) {$effet_adv=0;}
				if($_SESSION['id_effet_attaque']==80 AND $effet==1 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==80 AND $effet==1 AND $_SESSION['rune_protect_adv']==0) {$triplattaque1=rand(1,100);$triplattaque2=rand(1,100);$triplattaque3=rand(1,100); if($triplattaque1<=20){$_SESSION['statut_brule_adv']=1;$triplattaque_brule=1;}elseif($triplattaque2<=20){$_SESSION['statut_paralyse_adv']=1;$_SESSION['vit_adv']=$donnees['vit_adv']/4;$triplattaque_paralyse=1;}elseif($triplattaque3<=20){$_SESSION['statut_gel_adv']=1;$triplattaque_gel=1;}}
					//68 vampigraine
				if($_SESSION['id_effet_attaque']==68 AND $effet==1 AND $_SESSION['statut_vampigraine_adv']!=0 OR $_SESSION['id_effet2_attaque']==68 AND $effet==1 AND $_SESSION['statut_vampigraine_adv']!=0){$effet=0;}
				if($_SESSION['id_effet_attaque']==68 AND $effet==1 AND $_SESSION['statut_vampigraine_adv']==0 OR $_SESSION['id_effet2_attaque']==68 AND $effet==1 AND $_SESSION['statut_vampigraine_adv']==0) {$_SESSION['statut_vampigraine_adv']=1;}
					//1 ko
				if($_SESSION['id_effet_attaque']==1 AND $effet==1 AND $_SESSION['lvl']>=$_SESSION['lvl_adv'] OR $_SESSION['id_effet2_attaque']==1 AND $effet==1 AND $_SESSION['lvl']>=$_SESSION['lvl_adv']) {$_SESSION['pv_adv']=0;}	
				if($_SESSION['id_effet_attaque']==1 AND $effet==1 AND $_SESSION['lvl']<$_SESSION['lvl_adv'] OR $_SESSION['id_effet2_attaque']==1 AND $effet==1 AND $_SESSION['lvl']<=$_SESSION['lvl_adv']) {$effet=0;}
					
				if($_SESSION['def_adv']<=0){$_SESSION['def_adv']=1;}if($_SESSION['att_adv']<=0){$_SESSION['att_adv']=1;}if($_SESSION['vit_adv']<=0){$_SESSION['vit_adv']=1;}if($_SESSION['defspe_adv']<=0){$_SESSION['defspe_adv']=1;}if($_SESSION['attspe_adv']<=0){$_SESSION['attspe_adv']=1;}			
				if($_SESSION['def']<=0){$_SESSION['def']=1;}if($_SESSION['att']<=0){$_SESSION['att']=1;}if($_SESSION['vit']<=0){$_SESSION['vit']=1;}if($_SESSION['defspe']<=0){$_SESSION['defspe']=1;}if($_SESSION['attspe']<=0){$_SESSION['attspe']=1;}				
				//calcul des dégats
				$hasard_degats=rand(85,100);
				$hasard_cc=rand(1,100);
				$hasard_cc=$hasard_cc+$_SESSION['bonus_cc'];
				if($_SESSION['cc_attaque']==1){if($hasard_cc<7){$cc=1.5;}else{$cc=1;}}
				if($_SESSION['cc_attaque']==2){if($hasard_cc<15){$cc=1.5;}else{$cc=1;}}
				$degats=0;
				
				//influence des items
				if($_SESSION['objet']==102 AND $_SESSION['type_attaque']=="acier"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//peau métal
				if($_SESSION['objet']==103 AND $_SESSION['type_attaque']=="electrique"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//aimant
				if($_SESSION['objet']==104 AND $_SESSION['type_attaque']=="vol"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//bec pointu
				if($_SESSION['objet']==105 AND $_SESSION['type_attaque']=="combat"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//ceinture noire
				if($_SESSION['objet']==106 AND $_SESSION['type_attaque']=="feu"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//charbon
				if($_SESSION['objet']==107 AND $_SESSION['type_attaque']=="dragon"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//croc dragon
				if($_SESSION['objet']==108 AND $_SESSION['type_attaque']=="psy"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//cuillertordue
				if($_SESSION['objet']==109 AND $_SESSION['type_attaque']=="eau"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//eau mystique
				if($_SESSION['objet']==110 AND $_SESSION['type_attaque']=="glace"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//glaceternelle
				if($_SESSION['objet']==111 AND $_SESSION['type_attaque']=="plante"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//grain miracle
				if($_SESSION['objet']==112 AND $_SESSION['type_attaque']=="normal"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//mouchoir soie
				if($_SESSION['objet']==113 AND $_SESSION['type_attaque']=="poison"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//pic venin
				if($_SESSION['objet']==114 AND $_SESSION['type_attaque']=="insecte"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//poudre argent
				if($_SESSION['objet']==115 AND $_SESSION['type_attaque']=="sol"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//sable doux
				if($_SESSION['objet']==117 AND $_SESSION['type_attaque']=="spectre"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//rune sort
				if($_SESSION['objet']==118 AND $_SESSION['type_attaque']=="roche"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//pierre dure
				if($_SESSION['objet']==119 AND $_SESSION['type_attaque']=="tenebre"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//lunette noir
			
				
				if($_SESSION['classe_attaque']=="physique"){$degats=$_SESSION['lvl']*0.4;$degats=$degats+2; $degats=$degats*$_SESSION['att']*$_SESSION['puissance_attaque'];$defense=$_SESSION['def_adv']*50;$degats=$degats/$defense;$degats=$degats+2; $degats=$degats*$multiplicateur;$degats=$degats*$cc*$hasard_degats*$rafale/100;if($_SESSION['protection_adv']>0){$degats=$degats/2;}}	
				if($_SESSION['classe_attaque']=="speciale"){$degats=$_SESSION['lvl']*0.4;$degats=$degats+2; $degats=$degats*$_SESSION['attspe']*$_SESSION['puissance_attaque'];$defense=$_SESSION['defspe_adv']*50;$degats=$degats/$defense;$degats=$degats+2; $degats=$degats*$multiplicateur;$degats=$degats*$cc*$hasard_degats*$rafale/100;if($_SESSION['mur_lumiere_adv']>0){$degats=$degats/2;}}	
				if($_SESSION['type_attaque']==$_SESSION['type'] OR $_SESSION['type_attaque']==$_SESSION['type2']){$degats=$degats*1.5;}				
				if($_SESSION['puissance_attaque']==0){$degats=0;}
				//82 frappe atlas
				if($_SESSION['id_effet_attaque']==82 AND $effet==1 OR $_SESSION['id_effet2_attaque']==82 AND $effet==1) { if($multiplicateur!=0){$degats=$_SESSION['lvl'];}}	
				//86 sonicboom
				if($_SESSION['id_effet_attaque']==86 AND $effet==1 OR $_SESSION['id_effet2_attaque']==86 AND $effet==1) { if($multiplicateur!=0){$degats=20;}}
				//118 effort
                                if($_SESSION['id_effet_attaque']==118 AND $effet==1 OR $_SESSION['id_effet2_attaque']==118 AND $effet==1) {if($_SESSION['pv_adv']>$_SESSION['pv']){$degats=$_SESSION['pv_adv']-$_SESSION['pv'];}}
                                //95 draco-rage
				if($_SESSION['id_effet_attaque']==95 AND $effet==1 OR $_SESSION['id_effet2_attaque']==95 AND $effet==1) { $degats=40;}
				//88 25%deg
				if($_SESSION['id_effet_attaque']==88 AND $effet==1 OR $_SESSION['id_effet2_attaque']==88 AND $effet==1) { if($multiplicateur!=0){$degats=floor($_SESSION['pv_max_adv']/4);}}
				//84 effet lance-boue
				if($_SESSION['type_attaque']=="electrique" AND $_SESSION['lance_boue_adv']==1){$degats=floor($degats/2);}
				//97 effet tourniquet
				if($_SESSION['type_attaque']=="feu" AND $_SESSION['tourniquet_adv']==1){$degats=floor($degats/2);}
				//101 riposte
				if($_SESSION['classe_attaque_adv']=="physique" AND $_SESSION['id_effet_attaque']==101){$degats=$degats_adv*2;$_SESSION['puissance_attaque']=1;}elseif($_SESSION['classe_attaque_adv']!="physique" AND $_SESSION['id_effet_attaque']==101){$echec_attaque=1;}
				//102 voile miroir
				if($_SESSION['classe_attaque_adv']=="speciale" AND $_SESSION['id_effet_attaque']==102){$degats=$degats_adv*2;$_SESSION['puissance_attaque']=1;}elseif($_SESSION['classe_attaque_adv']!="speciale" AND $_SESSION['id_effet_attaque']==102){$echec_attaque=1;}
				// effet abri
				if($abri_adv==1){$degats=0;	$abri_adv=0;}
				
				$degats=floor($degats);if($degats<1 AND $degats!=0) {$degats=1;}
				if($degats>$_SESSION['pv_adv']){$degats=$_SESSION['pv_adv'];}
				$_SESSION['pv_adv']=$_SESSION['pv_adv']-$degats; if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
				//120 faux-chage
                                if($_SESSION['pv_adv']==0 AND $_SESSION['id_effet_attaque']==120){$_SESSION['pv_adv']=1; $degats=$degats-1;}
                                //effet tenacite
                                if($_SESSION['pv_adv']<=0 AND $tenacite_adv==1){$_SESSION['pv_adv']=1; $degats=$degats-1;}
			
				//2 att -
				if($_SESSION['id_effet_attaque']==2 AND $effet==1 OR $_SESSION['id_effet2_attaque']==2 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//3 def -
				if($_SESSION['id_effet_attaque']==3 AND $effet==1 OR $_SESSION['id_effet2_attaque']==3 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}
					//4 vit -
				if($_SESSION['id_effet_attaque']==4 AND $effet==1 OR $_SESSION['id_effet2_attaque']==4 AND $effet==1)
					{
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					}
					//5 att spe -
				if($_SESSION['id_effet_attaque']==5 AND $effet==1 OR $_SESSION['id_effet2_attaque']==5 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//6 defspe -
				if($_SESSION['id_effet_attaque']==6 AND $effet==1 OR $_SESSION['id_effet2_attaque']==6 AND $effet==1) 
					{
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
					//7 att --
				if($_SESSION['id_effet_attaque']==7 AND $effet==1 OR $_SESSION['id_effet2_attaque']==7 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//8 def --
				if($_SESSION['id_effet_attaque']==8 AND $effet==1 OR $_SESSION['id_effet2_attaque']==8 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}
					//9 vit --
				if($_SESSION['id_effet_attaque']==9 AND $effet==1 OR $_SESSION['id_effet2_attaque']==9 AND $effet==1) 
					{
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					}
					//10 att spe --
				if($_SESSION['id_effet_attaque']==10 AND $effet==1 OR $_SESSION['id_effet2_attaque']==10 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//11 defspe --
				if($_SESSION['id_effet_attaque']==11 AND $effet==1 OR $_SESSION['id_effet2_attaque']==11 AND $effet==1)
					{
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
					//12 att ---
				if($_SESSION['id_effet_attaque']==12 AND $effet==1 OR $_SESSION['id_effet2_attaque']==12 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//13 def ---
				if($_SESSION['id_effet_attaque']==13 AND $effet==1 OR $_SESSION['id_effet2_attaque']==13 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}
					//14 vit ---
				if($_SESSION['id_effet_attaque']==14 AND $effet==1 OR $_SESSION['id_effet2_attaque']==14 AND $effet==1) 
					{
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					}
					//15 att spe ---
				if($_SESSION['id_effet_attaque']==15 AND $effet==1 OR $_SESSION['id_effet2_attaque']==15 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//16 defspe ---
				if($_SESSION['id_effet_attaque']==16 AND $effet==1 OR $_SESSION['id_effet2_attaque']==16 AND $effet==1) 
					{
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
					//17 att +
				if($_SESSION['id_effet_attaque']==17 AND $effet==1 OR $_SESSION['id_effet2_attaque']==17 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//18 def +
				if($_SESSION['id_effet_attaque']==18 AND $effet==1 OR $_SESSION['id_effet2_attaque']==18 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
					//19 vit +
				if($_SESSION['id_effet_attaque']==19 AND $effet==1 OR $_SESSION['id_effet2_attaque']==19 AND $effet==1) 
					{
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					}
					//20 att spe +
				if($_SESSION['id_effet_attaque']==20 AND $effet==1 OR $_SESSION['id_effet2_attaque']==20 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//21 defspe +
				if($_SESSION['id_effet_attaque']==21 AND $effet==1 OR $_SESSION['id_effet2_attaque']==21 AND $effet==1)
					{
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
					//93 vent argenté
				if($_SESSION['id_effet_attaque']==93 AND $effet==1 OR $_SESSION['id_effet2_attaque']==93 AND $effet==1)
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}	
					//22 att ++
				if($_SESSION['id_effet_attaque']==22 AND $effet==1 OR $_SESSION['id_effet2_attaque']==22 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//23 def ++
				if($_SESSION['id_effet_attaque']==23 AND $effet==1 OR $_SESSION['id_effet2_attaque']==23 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
					//24 vit ++
				if($_SESSION['id_effet_attaque']==24 AND $effet==1 OR $_SESSION['id_effet2_attaque']==24 AND $effet==1) 
					{
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					}
					//25 att spe ++
				if($_SESSION['id_effet_attaque']==25 AND $effet==1 OR $_SESSION['id_effet2_attaque']==25 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//83 att spe lanceur --
				if($_SESSION['id_effet_attaque']==83 AND $effet==1 OR $_SESSION['id_effet2_attaque']==83 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//26 defspe ++
				if($_SESSION['id_effet_attaque']==26 AND $effet==1 OR $_SESSION['id_effet2_attaque']==26 AND $effet==1) 
					{
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
					//27 att +++
				if($_SESSION['id_effet_attaque']==27 AND $effet==1 OR $_SESSION['id_effet2_attaque']==27 AND $effet==1)
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//28 def +++
				if($_SESSION['id_effet_attaque']==28 AND $effet==1 OR $_SESSION['id_effet2_attaque']==28 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
					//29 vit +++
				if($_SESSION['id_effet_attaque']==29 AND $effet==1 OR $_SESSION['id_effet2_attaque']==29 AND $effet==1) 
					{
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					}
					//30 att spe +++
				if($_SESSION['id_effet_attaque']==30 AND $effet==1 OR $_SESSION['id_effet2_attaque']==30 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//31 defspe +++
				if($_SESSION['id_effet_attaque']==31 AND $effet==1 OR $_SESSION['id_effet2_attaque']==31 AND $effet==1) 
					{
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
                                        //111 boost
				if($_SESSION['id_effet_attaque']==111 AND $effet==1 OR $_SESSION['id_effet2_attaque']==111 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					$_SESSION['att']=$_SESSION['att_max']*$ex_mul;
                                        $ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					$_SESSION['def']=$_SESSION['def_max']*$ex_mul;
                                        $ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					$_SESSION['vit']=$_SESSION['vit_max']*$ex_mul;
                                        $ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					$_SESSION['attspe']=$_SESSION['attspe_max']*$ex_mul;
                                        $ex_mul=$_SESSION['def_spe_adv']/$_SESSION['def_spe_max_adv'];
					$_SESSION['def_spe']=$_SESSION['def_spe_max']*$ex_mul;
					}
                                        //116 cognobidon
				if($_SESSION['id_effet_attaque_adv']==116 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==116 AND $effet_adv==1) 
					{
					$mul=4;
                                        $_SESSION['pv']=ceil($_SESSION['pv_adv']/2);
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
                                //121 att spe ennemi +
				if($_SESSION['id_effet_attaque_adv']==121 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==121 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
                                //126 att ennemi ++
				if($_SESSION['id_effet_attaque_adv']==126 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==126 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//32 pre +
				if($_SESSION['id_effet_attaque']==32 AND $effet==1 OR $_SESSION['id_effet2_attaque']==32 AND $effet==1) {$_SESSION['pre']=$_SESSION['pre']+5;if($_SESSION['pre']>30){$_SESSION['pre']=30;}if($_SESSION['pre']<-30){$_SESSION['pre']=-30;}}
					//33 pre +
				if($_SESSION['id_effet_attaque']==33 AND $effet==1 OR $_SESSION['id_effet2_attaque']==33 AND $effet==1) {$_SESSION['pre']=$_SESSION['pre']+10;if($_SESSION['pre']>30){$_SESSION['pre']=30;}if($_SESSION['pre']<-30){$_SESSION['pre']=-30;}}
					//34 pre +
				if($_SESSION['id_effet_attaque']==34 AND $effet==1 OR $_SESSION['id_effet2_attaque']==34 AND $effet==1) {$_SESSION['pre']=$_SESSION['pre']+15;if($_SESSION['pre']>30){$_SESSION['pre']=30;}if($_SESSION['pre']<-30){$_SESSION['pre']=-30;}}
					//35 pre -
				if($_SESSION['id_effet_attaque']==35 AND $effet==1 OR $_SESSION['id_effet2_attaque']==35 AND $effet==1) {$_SESSION['pre_adv']=$_SESSION['pre_adv']-5;if($_SESSION['pre_adv']>30){$_SESSION['pre_adv']=30;}if($_SESSION['pre_adv']<-30){$_SESSION['pre_adv']=-30;}}
					//36 pre--
				if($_SESSION['id_effet_attaque']==36 AND $effet==1 OR $_SESSION['id_effet2_attaque']==36 AND $effet==1) {$_SESSION['pre_adv']=$_SESSION['pre_adv']-10;if($_SESSION['pre_adv']>30){$_SESSION['pre_adv']=30;}if($_SESSION['pre_adv']<-30){$_SESSION['pre_adv']=-30;}}
					//37 pre---
				if($_SESSION['id_effet_attaque']==37 AND $effet==1 OR $_SESSION['id_effet2_attaque']==37 AND $effet==1) {$_SESSION['pre_adv']=$_SESSION['pre_adv']-15;if($_SESSION['pre_adv']>30){$_SESSION['pre_adv']=30;}if($_SESSION['pre_adv']<-30){$_SESSION['pre_adv']=-30;}}
				//90 self-att -
				if($_SESSION['id_effet_attaque']==90 AND $effet==1 OR $_SESSION['id_effet2_attaque']==90 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
				//91 self-def -
				if($_SESSION['id_effet_attaque']==91 AND $effet==1 OR $_SESSION['id_effet2_attaque']==91 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}	
				//94 self-attspe --
				if($_SESSION['id_effet_attaque']==94 AND $effet==1 OR $_SESSION['id_effet2_attaque']==94 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
				}
			else
				{
				if($hasard_precision!=1000){$echec_attaque=1;}
				if($hasard_precision>=1000){$_SESSION['mania']=0;}
				}
			
			//effet après attaque
			
				//85 self-k.o.
			if($_SESSION['id_effet_attaque']==85 OR $_SESSION['id_effet2_attaque']==85) {if($hasard_precision!=1000){$_SESSION['pv']=0;}}		
				//76 damocles
			if($_SESSION['id_effet_attaque']==76 AND $effet==1 OR $_SESSION['id_effet2_attaque']==76 AND $effet==1) {$damocles=floor($degats/3); $_SESSION['pv']=$_SESSION['pv']-$damocles; if($_SESSION['pv']<0){$_SESSION['pv']=0;}}
				//92 bélier
			if($_SESSION['id_effet_attaque']==92 AND $effet==1 OR $_SESSION['id_effet2_attaque']==92 AND $effet==1) {$damocles=floor($degats/4); $_SESSION['pv']=$_SESSION['pv']-$damocles; if($_SESSION['pv']<0){$_SESSION['pv']=0;}}
				//52 sangsue
			if($_SESSION['id_effet_attaque']==52 AND $effet==1 OR $_SESSION['id_effet2_attaque']==52 AND $effet==1) {$sangsue=floor($degats/2); $_SESSION['pv']=$_SESSION['pv']+$sangsue; if($_SESSION['pv']>$_SESSION['pv_max']){$sangsue=$_SESSION['pv_max']-$_SESSION['pv']+$sangsue;$_SESSION['pv']=$_SESSION['pv_max'];}}				
				//souffre de poison (41)
			if($_SESSION['statut_poison']==1){$statut_poison=1;$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
				//souffre de poison gravement (42)
			if($_SESSION['statut_poison_grave']!=0){$statut_poison_grave=1;$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['statut_poison_grave']*$_SESSION['pv_max']/16);$_SESSION['statut_poison_grave']=$_SESSION['statut_poison_grave']+1;}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
				//souffre de brule (65)
			if($_SESSION['statut_brule']==1){$statut_brule=1;$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
				//souffre de danse flamme(72)
			if($_SESSION['danse_flamme']>0){$danse_flamme=1;$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
				//souffre de ligotage(77)
			if($_SESSION['ligotage']>0){$ligotage=1;$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
				//souffre de vampigraine(68)
			if($_SESSION['statut_vampigraine']==1){if($_SESSION['pv_adv']!=0){$statut_vampigraine=1;$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);$_SESSION['pv_adv']=$_SESSION['pv_adv']+floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}}				
			//victoire défaite?
			if($_SESSION['pv_adv']==0){$victoire=1;if($_SESSION['destin_adv']==1){$_SESSION['pv']=0;}}
			elseif($_SESSION['pv']==0){$defaite=1;}

			if($victoire==1){;$attaque_donne=1;}
			if($defaite==1){$attaque_donne=1;}
			
			//DESC 2
			$desc_2_1="";$desc_2_2="";$desc_2_3="";$desc_2_4="";$desc_2_5="";$desc_2_6="";$desc_2_7="";$desc_2_8="";$desc_2_9="";$desc_2_10="";$desc_2_11="";$desc_2_12="";$desc_2_13="";$desc_2_14="";$desc_2_15="";$desc_2_16="";$desc_2_17="";$desc_2_18="";$desc_2_19="";$desc_2_20="";$desc_2_21="";$desc_2_22="";$desc_2_23="";$desc_2_24="";$desc_2_25="";$desc_2_26="";$desc_2_27="";$desc_2_28="";$desc_2_29="";$desc_2_30="";
			if($degel==1){$desc_2_1= $_SESSION['nom_pokemon'].' n\'est plus gelé <br />';}
			if($reveil==1){$desc_2_2= $_SESSION['nom_pokemon'].' est reveillé <br />';}
			if($statut_confus_end==1){$desc_2_3= $_SESSION['nom_pokemon'].' n\'est plus confus <br />'; $statut_confus_end=0;}
			if($_SESSION['statut_confus']>0 AND $statut_confus_begin!=1){$desc_2_4= $_SESSION['nom_pokemon'].' est confus <br />';}
			$desc_2_5= $_SESSION['nom_pokemon'].' tente d\'utiliser l\'attaque <b><span style="font-size:20px;">'.$_SESSION['nom_attaque'].'</span></b>.<br />';
			if($fuite==1){$echec_attaque=1;}
			if($echec_attaque==1){$desc_2_6= 'l\'attaque a échoué. <br />';}
			elseif($recharge_faite==1){$desc_2_7= $_SESSION['nom_pokemon'].' se repose <br />';}
			elseif($statut_dodo==1){$desc_2_8= $_SESSION['nom_pokemon'].' est endormi, il ne peut pas attaquer <br />';}		
			elseif($statut_gel==1){$desc_2_9= $_SESSION['nom_pokemon'].' est gelé, il ne peut attaquer <br />';}	
			elseif($proba_paralyse>75 AND $_SESSION['statut_paralyse']==1){$desc_2_10= $_SESSION['nom_pokemon'].' est paralysé, il ne peut attaquer <br />';}
			elseif($peur==1){$desc_2_11= $_SESSION['nom_pokemon'].' a peur, il n\'attaque pas <br />';}			
			elseif($proba_attraction_adv==1 AND $_SESSION['attraction_adv']==1){$desc_2_12= $_SESSION['nom_pokemon'].' refuse d\'attaquer <br />';}
			elseif($proba_confus>50 AND $_SESSION['statut_confus']>0){$desc_2_13= $_SESSION['nom_pokemon'].' est confus, sa folie lui inflige des dégâts <br />';}
			elseif($charge_faite==1){$desc_2_14= $_SESSION['nom_pokemon'].' charge son attaque, dés le prochain tour, il pourra la lancer <br />';}
			else
				{
				if($cc==1.5 AND $degats!=0){$desc_2_15= 'coup critique!<br />';}
				if ($rafale>1){$desc_2_16= 'Touché '.$rafale.' fois <br />';}
				if ($_SESSION['puissance_attaque']!=0){if ($multiplicateur>1) {$desc_2_17= 'C\'est très efficace! <br />';} if($multiplicateur==0) {if($degats==0){$desc_2_17= 'Ca n\'a aucun effet <br />';}} elseif($multiplicateur<1){$desc_2_17= 'C\'est peu efficace <br />';}}
				if ($_SESSION['puissance_attaque']!=0){$desc_2_18= 'Il inflige '.$degats.' dégâts à '.$_SESSION['nom_pokemon_adv'].'.<br />';}
				if($sangsue>0){$desc_2_19= 'Il gagne '.$sangsue.' PV <br />';}
				if ($_SESSION['id_effet_attaque']!=0) 
					{
					$reponse = $bdd->prepare('SELECT * FROM pokemon_base_effets WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('id' => $_SESSION['id_effet_attaque']));  
					$donnees = $reponse->fetch();
					if($_SESSION['id_effet_attaque']==99){if($_SESSION['puissance_attaque']){$donnees['descr_reussi']="";}} //cadeau
					if($effet==1 AND $donnees['descr_reussi']!="") {$desc_2_20= $donnees['descr_reussi'].'<br />';} if($effet==0 AND $donnees['descr_echec']!="") {$desc_2_20= $donnees['descr_echec'].'<br />';}
					}
				if ($_SESSION['id_effet2_attaque']!=0) 
					{
					$reponse = $bdd->prepare('SELECT * FROM pokemon_base_effets WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('id' => $_SESSION['id_effet2_attaque']));  
					$donnees = $reponse->fetch();
					if($effet==1 AND $donnees['descr_reussi']!="") {$desc_2_21= $donnees['descr_reussi'].'<br />';} if($effet==0 AND $donnees['descr_echec']!="") {$desc_2_21=$donnees['descr_echec'].'<br />';}
					}
				}
			if($triplattaque_brule==1){$desc_2_22= $_SESSION['nom_pokemon_adv'].' est brûlé par la triplattaque <br />';}
			if($triplattaque_paralyse==1){$desc_2_23= $_SESSION['nom_pokemon_adv'].' est paralysé par la triplattaque <br />';}
			if($triplattaque_gel==1){$desc_2_24= $_SESSION['nom_pokemon_adv'].' est gelé par la triplattaque <br />';}
			if($statut_poison==1){$desc_2_25= $_SESSION['nom_pokemon'].' souffre du poison <br />';} //poison
			if($statut_poison_grave==1){$desc_2_26= $_SESSION['nom_pokemon'].' souffre du poison <br />';}
			if($statut_brule==1){$desc_2_27= $_SESSION['nom_pokemon'].' souffre de la brûlure <br />';}
			if($danse_flamme==1){$desc_2_28= $_SESSION['nom_pokemon'].' souffre de danse flamme <br />';} 
			if($ligotage==1){$desc_2_29= $_SESSION['nom_pokemon'].' souffre de l\'étreinte <br />';} 			
			if($statut_vampigraine==1) {$desc_2_30= $_SESSION['nom_pokemon'].' se fait drainer par la vampigraine<br />';} 
			$desc_2=$desc_2_1.''.$desc_2_2.''.$desc_2_3.''.$desc_2_4.''.$desc_2_5.''.$desc_2_6.''.$desc_2_7.''.$desc_2_8.''.$desc_2_9.''.$desc_2_10.''.$desc_2_11.''.$desc_2_12.''.$desc_2_13.''.$desc_2_14.''.$desc_2_15.''.$desc_2_16.''.$desc_2_17.''.$desc_2_18.''.$desc_2_19.''.$desc_2_20.''.$desc_2_21.''.$desc_2_22.''.$desc_2_23.''.$desc_2_24.''.$desc_2_25.''.$desc_2_26.''.$desc_2_27.''.$desc_2_28.''.$desc_2_29.''.$desc_2_30;			
			$reponse = $bdd->prepare('UPDATE pokemons_description_blind_battles SET desc_2=:desc_2, pos_desc_2=:pos_desc_2 WHERE id_battles=:id_battles AND n_tour=:n_tour') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('desc_2' => $desc_2, 'pos_desc_2' => $ordre_attaque, 'id_battles' =>$_POST['id_match'], 'n_tour' =>$nb_tours)); 
			
			//fin de peur
			$peur=0;
			//ordre de tour
			if($attaque_donne==1){$premier_coup=3;$nb_tours=$nb_tours+1;} else {$premier_coup=-1;}			
			$attaque_donne=1;
			}
		}
	}
//FIN DU MATCH
if($victoire==1 OR $nb_tours>=30 AND $_SESSION['pv']>=$_SESSION['pv_adv']) {$vainqueur=$_SESSION['pseudo'];$perdant=$_SESSION['pseudo_adv'];$id_pokemon_vainqueur=$_SESSION['id_pokemon'];$id_pokemon_perdant=$_SESSION['id_pokemon_adv'];} 
elseif($defaite==1 OR $nb_tours>=30 AND $_SESSION['pv']<$_SESSION['pv_adv']){$vainqueur=$_SESSION['pseudo_adv'];$perdant=$_SESSION['pseudo'];$id_pokemon_vainqueur=$_SESSION['id_pokemon_adv'];$id_pokemon_perdant=$_SESSION['id_pokemon'];}
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo ') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $vainqueur)); 
$donnees = $reponse->fetch();
$bds_max_vainqueur=$donnees['bds'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $vainqueur));
while($donnees = $reponse->fetch()){$nb_pokemons_vainqueur=$nb_pokemons_vainqueur+1;} 
if($bds_max_vainqueur<=$nb_pokemons_vainqueur){$max_pokemon_atteint=1;}else{$max_pokemon_atteint=0;}
//récompense
if($classe=="D")
	{
	$rec=rand(1,10000);
	if($rec<=5000){$genre_recompense="item"; $recompense=1;$nb_recompense=1;}
	elseif($rec<=7500){$genre_recompense="argent"; $recompense=250;$nb_recompense=1;}
	elseif($rec<=9000){$genre_recompense="item"; $recompense=10;$nb_recompense=5;}
	elseif($rec<=9500){$genre_recompense="item"; $recompense=2;$nb_recompense=1;}
	elseif($rec<=9900){$genre_recompense="item"; $recompense=3;$nb_recompense=1;}
	elseif($rec<=9990){$genre_recompense="argent"; $recompense=1000;$nb_recompense=1;}
	elseif($max_pokemon_atteint!=1)
		{
		if($rec<=9999){$genre_recompense="pokemon"; $recompense=143;$nb_recompense=0;}
		elseif($rec==10000){$genre_recompense="pokemon"; $recompense=143;$nb_recompense=1;}
		}
	else
		{
		if($rec<=10000){$genre_recompense="argent"; $recompense=1000;$nb_recompense=1;}
		}
	}
if($classe=="C")
	{
	$rec=rand(1,10000);
	if($rec<=5000){$genre_recompense="item"; $recompense=2;$nb_recompense=1;}
	elseif($rec<=7500){$genre_recompense="argent"; $recompense=500;$nb_recompense=1;}
	elseif($rec<=9000){$genre_recompense="item"; $recompense=11;$nb_recompense=5;}
	elseif($rec<=9500){$genre_recompense="item"; $recompense=3;$nb_recompense=1;}
	elseif($rec<=9900){$genre_recompense="argent"; $recompense=2000;$nb_recompense=1;}
	elseif($rec<=9950){$genre_recompense="item"; $recompense=23;$nb_recompense=1;}
	elseif($max_pokemon_atteint!=1)
		{
		if($rec<=9960){$genre_recompense="pokemon"; $recompense=7;$nb_recompense=0;}
		elseif($rec<=9970){$genre_recompense="pokemon"; $recompense=11;$nb_recompense=0;}
		elseif($rec<=9980){$genre_recompense="pokemon"; $recompense=14;$nb_recompense=0;}
		elseif($rec<=9990){$genre_recompense="pokemon"; $recompense=32;$nb_recompense=0;}
		elseif($rec<=10000){$genre_recompense="pokemon"; $recompense=144;$nb_recompense=0;}
		}
	else
		{
		if($rec<=10000){$genre_recompense="item"; $recompense=23;$nb_recompense=1;}
		}
	}
if($classe=="B")
	{
	$rec=rand(1,10000);
	if($rec<=5000){$genre_recompense="item"; $recompense=3;$nb_recompense=1;}
	elseif($rec<=7500){$genre_recompense="argent"; $recompense=1000;$nb_recompense=1;}
	elseif($rec<=9000){$genre_recompense="item"; $recompense=12;$nb_recompense=5;}
	elseif($rec<=9400){$genre_recompense="argent"; $recompense=2000;$nb_recompense=1;}
	elseif($rec<=9500){$genre_recompense="item"; $recompense=5;$nb_recompense=1;}
	elseif($rec<=9600){$genre_recompense="item"; $recompense=6;$nb_recompense=1;}
	elseif($rec<=9700){$genre_recompense="item"; $recompense=7;$nb_recompense=1;}
	elseif($rec<=9800){$genre_recompense="item"; $recompense=8;$nb_recompense=1;}
	elseif($rec<=9900){$genre_recompense="item"; $recompense=9;$nb_recompense=1;}
	elseif($max_pokemon_atteint!=1)
		{
		if($rec<=9990){$genre_recompense="pokemon"; $recompense=158;$nb_recompense=0;}
		elseif($rec<=10000){$genre_recompense="pokemon"; $recompense=158;$nb_recompense=1;}
		}
	else
		{
		if($rec<=10000){$genre_recompense="item"; $recompense=9;$nb_recompense=1;}
		}
	}
if($classe=="A")
	{
	$rec=rand(1,10000);
	if($rec<=5000){$genre_recompense="item"; $recompense=3;$nb_recompense=5;}
	elseif($rec<=7500){$genre_recompense="argent"; $recompense=2000;$nb_recompense=1;}
	elseif($rec<=9000){$genre_recompense="item"; $recompense=13;$nb_recompense=5;}
	elseif($rec<=9500){$genre_recompense="item"; $recompense=4;$nb_recompense=1;}
	elseif($max_pokemon_atteint!=1)
		{
		if($rec<=9590){$genre_recompense="pokemon"; $recompense=7;$nb_recompense=0;}
		elseif($rec<=9600){$genre_recompense="pokemon"; $recompense=7;$nb_recompense=1;}
		elseif($rec<=9690){$genre_recompense="pokemon"; $recompense=11;$nb_recompense=0;}
		elseif($rec<=9700){$genre_recompense="pokemon"; $recompense=11;$nb_recompense=1;}
		elseif($rec<=9790){$genre_recompense="pokemon"; $recompense=14;$nb_recompense=0;}
		elseif($rec<=9800){$genre_recompense="pokemon"; $recompense=14;$nb_recompense=1;}
		elseif($rec<=9890){$genre_recompense="pokemon"; $recompense=32;$nb_recompense=0;}
		elseif($rec<=9900){$genre_recompense="pokemon"; $recompense=32;$nb_recompense=1;}
		elseif($rec<=9990){$genre_recompense="pokemon"; $recompense=144;$nb_recompense=0;}
		elseif($rec<=10000){$genre_recompense="pokemon"; $recompense=144;$nb_recompense=1;}
		}
	else
		{
		if($rec<=10000){$genre_recompense="item"; $recompense=4;$nb_recompense=1;}
		}
	}
//Touchage de récompense
if($genre_recompense=="item")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pseudo' => $vainqueur, 'id_item' => $recompense));  
		$donnees = $reponse->fetch();
	if(isset($donnees['id']))
		{
		$quantite_total=$nb_recompense + $donnees['quantite'];
		$reponse = $bdd->prepare('UPDATE pokemons_inventaire SET quantite=:quantite WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('quantite' =>$quantite_total ,'pseudo' => $vainqueur, 'id_item' => $recompense)); 
		}
	else
		{
		$req = $bdd->prepare('INSERT INTO pokemons_inventaire (pseudo, id_item, quantite) VALUES(:pseudo, :id_item, :quantite)') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $vainqueur, 'id_item' => $recompense,'quantite' => $nb_recompense))or die(print_r($bdd->errorInfo()));
		}			
	}
if($genre_recompense=="argent")
	{	
	$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pseudo' => $vainqueur));  
		$donnees = $reponse->fetch();
	$argent=$donnees['pokedollar'];
	$argent_now=$argent+$recompense;
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar  WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pokedollar' => $argent_now, 'pseudo' => $vainqueur));		
	}
if($genre_recompense=="pokemon")
	{	
	$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $recompense));
	$donnees = $reponse->fetch();
	if($donnees['sexe']=="Y"){$proba_sexe=rand(1,2); if($proba_sexe==1){$sexe="M";} elseif($proba_sexe==2){$sexe="F";}}
	if($donnees['sexe']=="M"){$sexe="M";}
	if($donnees['sexe']=="F"){$sexe="F";}
	if($donnees['sexe']=="N"){$sexe="";}
	$randpv=rand(0,4);$randatt=rand(0,4);$randdef=rand(0,4);$randvit=rand(0,4);$randattspe=rand(0,4);$randdefspe=rand(0,4);
	$pv=$donnees['pv']+4*$donnees['bonus_pv']+$randpv;
	$att=$donnees['att']+4*$donnees['bonus_att']+$randatt;
	$def=$donnees['def']+4*$donnees['bonus_def']+$randdef;
	$vit=$donnees['vit']+4*$donnees['bonus_vit']+$randvit;
	$attspe=$donnees['attspe']+4*$donnees['bonus_attspe']+$randattspe;
	$defspe=$donnees['defspe']+4*$donnees['bonus_defspe']+$randdefspe;
	if($donnees['lvl1']!=0){$attaque1=$donnees['lvl1'];}else{$attaque1=0;}
	if($donnees['lvl2']!=0){$attaque2=$donnees['lvl2'];}else{$attaque2=0;}
	if($donnees['lvl5']!=0){$attaque3=$donnees['lvl5'];}else{$attaque3=0;}
	$req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons (pseudo, id_pokemon, shiney, sexe, lvl, xp, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, actif, pa_restant, pa_max, pa_bonus) VALUES(:pseudo, :id_pokemon, :shiney, :sexe, 5, 100, :pv,:pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, 0, 0, 10, 10, 5)') or die(print_r($bdd->errorInfo()));
							$req->execute(array(
									'pseudo' => $vainqueur, 
									'id_pokemon' => $recompense,
									'shiney' => $nb_recompense,
									'sexe' => $sexe,
									'pv' => $pv,
									'pv_max' => $pv,
									'att' => $att,
									'def' => $def,
									'vit' => $vit,
									'attspe' => $attspe,
									'defspe' => $defspe,
									'attaque1' => $attaque1,
									'attaque2' => $attaque2,
									'attaque3' => $attaque3	
									))or die(print_r($bdd->errorInfo()));
	}

//ajout de la victoire/défaite dans la bdd
$time = time();
$req = $bdd->prepare('INSERT INTO pokemons_description_blind_battles (id_battles, n_tour, desc_1, vainqueur, pokemon_vainqueur, perdant, pokemon_perdant, genre_recompense, recompense, nb_recompense, quand) VALUES(:id_battles, 0, :desc_1, :vainqueur, :pokemon_vainqueur, :perdant, :pokemon_perdant, :genre_recompense, :recompense, :nb_recompense, :quand)') or die(print_r($bdd->errorInfo()));							
								$req->execute(array(
										'id_battles' => $_POST['id_match'], 
										'desc_1' => $classe,
										'vainqueur' => $vainqueur, 
										'pokemon_vainqueur' => $id_pokemon_vainqueur,
										'perdant' => $perdant, 
										'pokemon_perdant' => $id_pokemon_perdant,
										'genre_recompense' => $genre_recompense, 
										'recompense' => $recompense, 
										'nb_recompense' => $nb_recompense,
										'quand' => $time
										))or die(print_r($bdd->errorInfo()));																	
//touchage d'xp
if($classe=="D"){$xp_vainqueur=50;}if($classe=="C"){$xp_vainqueur=100;}if($classe=="B"){$xp_vainqueur=150;}if($classe=="A"){$xp_vainqueur=200;}
if($classe=="D"){$xp_perdant=25;}if($classe=="C"){$xp_perdant=50;}if($classe=="B"){$xp_perdant=75;}if($classe=="A"){$xp_perdant=100;}
	
if($victoire==1){$id_liste_pokemon_xp=$id_liste_pokemon;}else{$id_liste_pokemon_xp=$id_liste_pokemon_adv;}
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $id_liste_pokemon_xp));  
$donnees = $reponse->fetch();
$xp_pokemon_actif=$donnees['xp'];
$victoires_totales=$donnees['victoires']+1;
$objet=$donnees['objet'];
$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET victoires=:victoires WHERE id=:id') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('victoires' => $victoires_totales,'id' => $id_liste_pokemon_xp)); 
$gain_xp=$xp_vainqueur;
if($objet==128){$gain_xp=$gain_xp*1.33;}
$xp_apres_combat=$xp_pokemon_actif+$gain_xp;
$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET xp=:xp WHERE id=:id') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('xp' => $xp_apres_combat,'id' => $id_liste_pokemon_xp)); 

if($defaite==1){$id_liste_pokemon_xp=$id_liste_pokemon;}else{$id_liste_pokemon_xp=$id_liste_pokemon_adv;}
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $id_liste_pokemon_xp));  
$donnees = $reponse->fetch();
$objet=$donnees['objet'];
$xp_pokemon_actif=$donnees['xp'];
$gain_xp=$xp_perdant;
if($objet==128){$gain_xp=$gain_xp*1.33;}
$xp_apres_combat=$xp_pokemon_actif+$gain_xp;
$bonheur_total=$donnees['bonheur']-1;
$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET xp=:xp, bonheur=:bonheur WHERE id=:id') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('xp' => $xp_apres_combat, 'bonheur'=>$bonheur_total, 'id' => $id_liste_pokemon_xp)); 	

//envoi mp
if($genre_recompense=="item")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $recompense));  
						$donnees = $reponse->fetch();
	$nom_item=$donnees['nom'];
	}
if($genre_recompense=="pokemon")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $recompense));  
						$donnees = $reponse->fetch();
	$nom_pokemon=$donnees['nom'];
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $id_pokemon_perdant));  
	$donnees = $reponse->fetch();
$nom_pokemon_perdant=$donnees['nom'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $id_pokemon_vainqueur));  
	$donnees = $reponse->fetch();
$nom_pokemon_vainqueur=$donnees['nom'];
if($genre_recompense=="item"){$desc_recompense='Vous avez reçu '.$nb_recompense.' '.$nom_item.'.';}
if($genre_recompense=="argent"){$desc_recompense='Vous avez reçu '.$recompense.'$.';}
if($genre_recompense=="pokemon"){if($nb_recompense==1){$brille='shiney!';}else{$brille="";}$desc_recompense='Vous avez reçu un '.$nom_pokemon.''.$brille; }
	//vainqueur
$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("Prof Saul", :destinataire, "non lu", :titre, :message, now())') or die(print_r($bdd->errorInfo()));
		$req->execute(array(
			'destinataire' => $vainqueur,	
			'titre'	=> 'Victoire en Blind Battles classe '.$classe.'',
			'message' => 'félicitation, vous avez remporté le match de classe '.$classe.' opposant '.$perdant.' ('.$nom_pokemon_perdant.') à '.$vainqueur.' ('.$nom_pokemon_vainqueur.').
'.$desc_recompense.'
Vous pouvez voir le rapport du combat à la page :
http://www.pokemon-origins.com/rapport_blind_battles.php?id='.$_POST['id_match'].''
			))
			or die(print_r($bdd->errorInfo()));	
	//perdant
$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("Prof Saul", :destinataire, "non lu", :titre, :message, now())') or die(print_r($bdd->errorInfo()));
		$req->execute(array(
			'destinataire' => $perdant,	
			'titre'	=> 'Défaite en Blind Battles classe '.$classe.'',	
			'message' => 'Vous avez perdu le match de classe '.$classe.' opposant '.$perdant.' ('.$nom_pokemon_perdant.') à '.$vainqueur.' ('.$nom_pokemon_vainqueur.').
Vous pouvez voir le rapport du combat à la page :
http://www.pokemon-origins.com/rapport_blind_battles.php?id='.$_POST['id_match'].''
			))
			or die(print_r($bdd->errorInfo()));			
//Ajout victoire au vainqueur

$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $vainqueur));
	$donnees = $reponse->fetch();
$points_quetes=$donnees['points_quetes'];
$victoires_D=$donnees['victoires_D'];
$victoires_C=$donnees['victoires_C'];
$victoires_B=$donnees['victoires_B'];
$victoires_A=$donnees['victoires_A'];
if($classe=="D")
	{
	$difficulte_quete_now=$points_quetes+1;
	$victoires_D=$victoires_D+1;
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET points_quetes=:points_quetes, victoires_D=:victoires_D WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
	$reponse->execute(array('points_quetes' => $difficulte_quete_now,'victoires_D' => $victoires_D, 'pseudo' => $vainqueur)); 
	}
if($classe=="C")
	{
	$difficulte_quete_now=$points_quetes+2;
	$victoires_C=$victoires_C+1;
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET points_quetes=:points_quetes, victoires_C=:victoires_C WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
	$reponse->execute(array('points_quetes' => $difficulte_quete_now,'victoires_C' => $victoires_C, 'pseudo' => $vainqueur)); 
	}
if($classe=="B")
	{
	$difficulte_quete_now=$points_quetes+3;
	$victoires_B=$victoires_B+1;
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET points_quetes=:points_quetes, victoires_B=:victoires_B WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
	$reponse->execute(array('points_quetes' => $difficulte_quete_now,'victoires_B' => $victoires_B, 'pseudo' => $vainqueur)); 
	}
if($classe=="A")
	{
	$difficulte_quete_now=$points_quetes+4;
	$victoires_A=$victoires_A+1;
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET points_quetes=:points_quetes, victoires_A=:victoires_A WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
	$reponse->execute(array('points_quetes' => $difficulte_quete_now,'victoires_A' => $victoires_A, 'pseudo' => $vainqueur)); 
	}	
//deletage du choix
$reponse = $bdd->prepare('DELETE FROM pokemons_blind_battles WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $_POST['id_match'])); 
					
?>




