<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">


<?php
if(isset($_SESSION['pseudo']))
{
?>

<h1> Votre compte </h1>

<?php //changement de sexe
if(isset($_POST['sexe']))
	{
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET sexe=:sexe WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('sexe' => $_POST['sexe'], 'pseudo' => $_SESSION['pseudo'])); 
	echo '<b>Les nouvelles données ont bien été prises en compte.</b><br /><br />';
			
	}
if(isset($_POST['newsletter']))
	{
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET newsletter=:newsletter WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('newsletter' => $_POST['newsletter'], 'pseudo' => $_SESSION['pseudo'])); 
	echo '<b>Les nouvelles données ont bien été prises en compte.</b><br /><br />';
			
	}
?>
<?php //changement de préférence pour le pvp
if(isset($_POST['pvp']))
	{
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET pvp=:pvp WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('pvp' => $_POST['pvp'], 'pseudo' => $_SESSION['pseudo'])); 
	echo '<b>Les nouvelles données ont bien été prises en compte.</b><br /><br />';
			
	}
?>

<?php  //changer de mdp
if(isset($_POST['action']) AND $_POST['action']=="changemdp")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 
			  $donnees = $reponse->fetch();
	if ($donnees['mdp'] == $_POST['mdp'])
		{
		if ($_POST['mdpc'] == $_POST['mdpc2'])
			{
			$reponse = $bdd->prepare('UPDATE pokemons_membres SET mdp=:mdp WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('mdp' => $_POST['mdpc'], 'pseudo' => $_SESSION['pseudo'])); 
			echo 'Mot de passe bien changé!';
			}	
		else
			{
			echo 'Erreur dans la confirmation de votre mot de passe';
			}
		}
		else 
		{
		echo 'Mauvais mot de passe';
		}	
	echo '<br /><br />';
	}
?>		
<?php  //changer de description
if(isset($_POST['action']) AND $_POST['action']=="description")
	{
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET description=:description WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	    $reponse->execute(array('description' => stripslashes(htmlspecialchars($_POST['description'])), 'pseudo' => $_SESSION['pseudo'])); 
	echo 'Description bien changée!';
	echo '<br /><br />';
	}

$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
$sexe_joueur=$donnees['sexe'];
$accepte_pvp=$donnees['pvp'];
$newsletter=$donnees['newsletter'];
?>		   




<b>Changer de mot de passe : </b>
<form action="compte.php" method="post">
    <table>   
	<tr><td><label for="mdp">Votre ancien mot de passe</label> : </td><td><input type="password" name="mdp"  id="mdp" /> </td></tr>
	<tr><td><label for="mdpc">Votre nouveau mot de passe</label> : </td><td><input type="password" name="mdpc"  id="mdpc" /> </td></tr>	 
    <tr><td><label for="mdpc2">Confirmez votre nouveau mot de passe</label> : </td><td><input type="password" name="mdpc2"  id="mdpc2" /> </td></tr>	 			
    <input type="hidden" name="action" value="changemdp"/>
	<tr><td><input type="submit" value="Valider" /></td></tr>
	</table>
</form>	
<br /><br />

<b>Votre sexe : </b>
<form action="compte.php" method="post">
<input type="radio" name="sexe" value="0" <?php if($sexe_joueur==0){echo 'checked';}?>>	Dresseur 	<input type="radio" name="sexe" value="1" <?php if($sexe_joueur==1){echo 'checked';}?>> Dresseuse <br />		
<input type="submit" value="Valider" />
</form>	
<br /><br />
<b>Recevoir les newsletters : </b>
<form action="compte.php" method="post">
<input type="radio" name="newsletter" value="0" <?php if($newsletter==0){echo 'checked';}?>>	Oui 	<input type="radio" name="newsletter" value="1" <?php if($newsletter==1){echo 'checked';}?>> Non <br />		
<input type="submit" value="Valider" />
</form>	
<br /><br />
<b>PvP : </b><br />
Accepter que d'autres joueurs vous défie en PvP? <br />
<form action="compte.php" method="post">
<input type="radio" name="pvp" value="0" <?php if($accepte_pvp==0){echo 'checked';}?>>	Oui	<input type="radio" name="pvp" value="1" <?php if($accepte_pvp==1){echo 'checked';}?>> Non <br />		
<input type="submit" value="Valider" />
</form>	
<br />

<b>Les joueurs que vous bloquez actuellement : </b><br />
<?php
$reponse = $bdd->prepare('SELECT * FROM pokemons_black_liste WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 
while($donnees = $reponse->fetch())
	{
	echo '<a href="/profil.php?profil='.$donnees['cible'].'">'.$donnees['cible'].'</a><br />';
	}
?>
<br />
<br />

<?php //calcul avantages
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 
$donnees = $reponse->fetch();
$time=time();
$bonus_duel=$donnees['bonus_duel'];
if($bonus_duel>$time){$bonus_duel_restant=$bonus_duel-$time;$bonus_duel_jours=floor($bonus_duel_restant/86400);$reste=$bonus_duel_restant%86400;$bonus_duel_heures=floor($reste/3600); $reste=$reste%3600;$bonus_duel_minutes=floor($reste/60);$bonus_duel_secondes=$reste%60;}
else{$reponse = $bdd->prepare('UPDATE pokemons_membres SET bonus_duel=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));$reponse->execute(array('pseudo' => $_SESSION['pseudo']));}
$bonus_naissance=$donnees['bonus_naissance'];
if($bonus_naissance>$time){$bonus_naissance_restant=$bonus_naissance-$time;$bonus_naissance_jours=floor($bonus_naissance_restant/86400);$reste=$bonus_naissance_restant%86400;$bonus_naissance_heures=floor($reste/3600); $reste=$reste%3600;$bonus_naissance_minutes=floor($reste/60);$bonus_naissance_secondes=$reste%60;}
else{$reponse = $bdd->prepare('UPDATE pokemons_membres SET bonus_naissance=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));$reponse->execute(array('pseudo' => $_SESSION['pseudo']));}
$bonus_xp=$donnees['bonus_xp'];
if($bonus_xp>$time){$bonus_xp_restant=$bonus_xp-$time;$bonus_xp_jours=floor($bonus_xp_restant/86400);$reste=$bonus_xp_restant%86400;$bonus_xp_heures=floor($reste/3600); $reste=$reste%3600;$bonus_xp_minutes=floor($reste/60);$bonus_xp_secondes=$reste%60;}
else{$reponse = $bdd->prepare('UPDATE pokemons_membres SET bonus_xp=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));$reponse->execute(array('pseudo' => $_SESSION['pseudo']));}
$bonus_racketteur=$donnees['bonus_racketteur'];
if($bonus_racketteur>$time){$bonus_racketteur_restant=$bonus_racketteur-$time;$bonus_racketteur_jours=floor($bonus_racketteur_restant/86400);$reste=$bonus_racketteur_restant%86400;$bonus_racketteur_heures=floor($reste/3600); $reste=$reste%3600;$bonus_racketteur_minutes=floor($reste/60);$bonus_racketteur_secondes=$reste%60;}
else{$reponse = $bdd->prepare('UPDATE pokemons_membres SET bonus_racketteur=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));$reponse->execute(array('pseudo' => $_SESSION['pseudo']));}
		
?>
<b>Vos avantages prémium : </b>
<table id="votre_pokemon" width="550px" cellpadding="4" cellspacing="4" style="text-align:center;">
<colgroup><COL WIDTH=15%><COL WIDTH=50%><COL WIDTH=35%></COLGROUP>
<tr><th>NOM</th><th>DESCRIPTION</th><th>ECHEANCE</th></tr>

<tr><td>Premium dueliste</td><td>Vous pouvez faire 10 duels supplémentaires contre des PNJ par jour</td><td>
<?php if($bonus_duel_restant>0){echo $bonus_duel_jours.' Jours '.$bonus_duel_heures.' Heures <br />'.$bonus_duel_minutes.' Minutes '.$bonus_duel_secondes.' Secondes ';}else{echo 'Non activé';} ?></td></tr>
<tr><td>Premium accoucheur</td><td>Votre bonus cumulatif d'accouchement augmente de 7 au lieu de 1. Vous avez beaucoup plus de chance d'avoir un oeuf à la penssion.</td><td>
<?php if($bonus_naissance_restant>0){echo $bonus_naissance_jours.' Jours '.$bonus_naissance_heures.' Heures <br />'.$bonus_naissance_minutes.' Minutes '.$bonus_naissance_secondes.' Secondes ';}else{echo 'Non activé';} ?></td></tr>
<tr><td>Premium chasseur</td><td>Vous gagnez 25% plus d'xp en combattant des pokémons sauvages</td><td>
<?php if($bonus_xp_restant>0){echo $bonus_xp_jours.' Jours '.$bonus_xp_heures.' Heures <br />'.$bonus_xp_minutes.' Minutes '.$bonus_xp_secondes.' Secondes ';}else{echo 'Non activé';} ?></td></tr>
<tr><td>Premium racketteur</td><td>Vous voyez les objets tenus par les pokémons</td><td>
<?php if($bonus_racketteur_restant>0){echo $bonus_racketteur_jours.' Jours '.$bonus_racketteur_heures.' Heures <br />'.$bonus_racketteur_minutes.' Minutes '.$bonus_racketteur_secondes.' Secondes ';}else{echo 'Non activé';} ?></td></tr>

</table>	
Pour accéder à d'autres avantages prémium, rendez-vous dans la <b><a href="http://www.pokemon-origins.com/shop.php?items=premium" style="color:black;">boutique</a></b>.
<br /><br />

<b>Votre avatar : </b><br />
<?php  //envoi des étiquettes

if(isset($_POST['what']) AND $_POST['what'] == "avatar")
{
$size=$_FILES['fichier']['size']; 
$type=$_FILES['fichier']['type'];
$tmp=$_FILES['fichier']['tmp_name'];
$fichier=$_FILES['fichier']['name'];
list($width,$height)=getimagesize($tmp);
$lieu='images/avatars/'.$_SESSION['id_membre'].'.jpg';
if ($_FILES['fichier']['name'] && $_FILES['fichier']['name'] != "none") 
    {
	 if ($type=="image/jpeg" AND $size<=500000 AND $width<=100 AND $height<=100)
		{
		move_uploaded_file($tmp,$lieu);
		echo '<b>Votre avatar a bien été transférée!</b><br />';
		}
	else
		{
		echo 'La taille de votre image ne peut excéder 500ko, son format doit être de type .jpg et sa taille ne doit pas dépasser 100*100 <br />';
		}
    }
}


$chemin = 'images/avatars/'.$_SESSION['id_membre'].'.jpg';
if(file_exists($chemin))
	{
	echo 'Avatar actuel : <img src="'.$chemin.'" style="border:0;height:200px;" />';
	}
else 
	{
	echo 'Vous n\'avez pas encore d\'avatar';
	}
?>

<FORM ENCTYPE="multipart/form-data" ACTION="compte.php" METHOD="POST"> 
<font face="Verdana" size="2">
<INPUT TYPE="hidden" name="MAX_FILE_SIZE" value="350000000"> 
<INPUT TYPE="hidden" name="what" value="avatar"> 
Modifier (100*100 500ko Max) : <INPUT NAME="fichier" TYPE="file"> 
<INPUT TYPE="submit" VALUE="Modifier votre avatar"> 
</font></FORM> 
<br /><br />



<b>Description : </b>
<form action="compte.php" method="post">      
	<input type="hidden" name="action" value="description"/>
	<textarea name="description" cols="50" rows="4"><?php echo $description; ?></textarea>
	<br /><input type="submit" value="Valider" />
</form>	
<br /><br />

<b>Concours de chasse : </b><br />

<?php
$reponse = $bdd->query('SELECT * FROM pokemon_target_concours') or die(print_r($bdd->errorInfo()));
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{	
	echo 'Vous avez déjà remporté '.$concours.' points au concours de chasse actuel. Pour plus d\'informations, rendez-vous dans la section "concours" du forum.';
	}
else
	{
	echo 'Il n\'y a actuellement aucun concours de chasse en cours. N\'hésitez-pas à vous rendre sur le forum de temps en temps pour vous renseigner.';
	}
?>
<br /><br />

<b>Parrainage : </b><br />
Parrainez vos amis et gagnez 20% de leurs gains dans la mine!<br />
Lien à donner à vos amis : <br /> http://www.pokemon-origins.com/inscription.php?parrain=<?php echo $_SESSION['pseudo']; ?><br />
<br />
<b>Liste de vos filleuls :</b><br />
<?php
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE parrain=:parrain') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('parrain' => $_SESSION['pseudo']));  
while($donnees = $reponse->fetch())
	{
	echo '- <a href="profil.php?profil='.$donnees['pseudo'].'" style="color:black;">'.$donnees['pseudo'].'</a><br />';
	}
?>







<?php
}
else
{
echo 'Vous devez être connecté pour accéder à cette page';
}
?>
<?php include ("bas.php"); ?>
