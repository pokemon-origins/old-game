<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">

<div align="center">
<br />
<?php

if(isset($_POST['action']) AND $_POST['action']=="renvoi_mail")
{
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_POST['pseudo']));
$donnees = $reponse->fetch();
$id=$donnees['id']*$donnees['id'];
$id=$id+5;
$headers ='From: "Pokemons Origins"<admin@pokemon-origins.com>'."\n";
$headers .='Reply-To: admin@pokemon-origins.com'."\n";
$headers .='Content-Type: text/html; charset="utf-8"'."\n";
$headers .='Content-Transfer-Encoding: 8bit';
$message ='<html><head><title>Validation de l\'inscription</title>
</head><body>Bonjour '.$_POST['pseudo'].',<br /> 
Merci de vous être inscrit sur le site Pokemons Origins.<br /> 
Votre pseudo : <b>'.$_POST['pseudo'].'</b><br /> 
Votre mot de passe : <b>'.$donnees['mdp'].'</b><br /> 
Cliquer sur le lien ci-dessous pour valider votre compte. <br />
http://www.pokemon-origins.com/validation.php?po='.$po.'
<br /> <br />
Si vous ne validez pas votre compte dans la semaine, celui-ci sera supprimé et vous devrez refaire votre inscription. <br />
Cordialement, l\'équipe de Pokemon-Origins.</body></html>';
mail($donnees['mail'], 'Validation de votre inscription', $message, $headers);
echo '<b>Un email vient de vous être renvoyé à '.$donnees['mail'].'. <br /> Il se peut qu\'il se trouve dans vos courriers indésirables.</b>';
}
elseif($connection_error==1)
{
echo '<b>Echec de l\'identification, veuillez recommencer</b>';
}
elseif($connection_error==2)
{
echo '<b>Votre compte doit être validé pour pouvoir jouer! <br />Suivez le lien qui vous a été envoyé sur votre boite mails lors de votre inscription.<br /> 
Si vous n\'avez pas reçu d\'email, regardez dans vos courriers indésirables. <br /> Vous pouvez également cliquer ici pour que l\'on vous renvoie un email : <br />
<form action="connexion.php" method="post"><input type="hidden" name="pseudo" value="'.$_POST['pseudo'].' " /><input type="hidden" name="action" value="renvoi_mail" /> <input type="submit" value="Renvoyez-moi un mail" /></b>';
}
elseif($connection_error==3)
{
echo '<b>Votre compte a été bloqué car il est suspecté d\'avoir été l\'origine de triche, de complicité de triche ou d\'avoir reçu l\'aide d\'un joueur ayant triché.<br />Vérifiez dans votre boite mail si vous n\'avez reçu aucun message. <br /> Si pas, envoyez un mail à admin@pokemon-origins.com en précisant votre pseudo pour plus d\'informations.</b>';
}
else
{
?>

<h2>Bienvenue </h2>

Vous êtes maintenant connecté. <br />

<?php
if(isset($back_do) AND $back_do==1)
	{
	echo '<b>Votre compte a été crédité de 50 pépites! Bon retour parmi nous! </b><br />';
	}
?>

<br />
<a href="index.php" style="color:black;">Se rendre à l'accueil </a><br />
<a href="carte.php" style="color:black;">Se rendre à la carte </a><br />
<a href="compte.php" style="color:black;">Se rendre sur mon compte </a><br /><br />

<?php
}
?>
</div>
<?php include ("bas.php"); ?>