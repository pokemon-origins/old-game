<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">


<?php
if(isset($_SESSION['pseudo']))
{
$reponse = $bdd->prepare('SELECT victoires_D, victoires_C, victoires_B, victoires_A, ors, quete_principale, quete_principale_etape, quete_principale_active FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
$victoires_D=$donnees['victoires_D'];
$victoires_C=$donnees['victoires_C'];
$victoires_B=$donnees['victoires_B'];
$victoires_A=$donnees['victoires_A'];
$ors=$donnees['ors'];
$quete_principale=$donnees['quete_principale'];
$quete_principale_etape=$donnees['quete_principale_etape'];
$quete_principale_active=$donnees['quete_principale_active'];
$reponse = $bdd->prepare('SELECT id FROM pokemons_quetes_principales WHERE numero=:numero AND etape=:etape') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('numero' => $quete_principale, 'etape' => $quete_principale_etape));  
$donnees = $reponse->fetch();
$id_quete_principale=$donnees['id'];
if($id_quete_principale>36  AND $_SESSION['page_combat']=="NULL" OR $id_quete_principale==36 AND $quete_principale_active==1 AND $_SESSION['page_combat']=="NULL")
{
?>

<?php 
//suppression des vieux défis terminés
$time = time();
$quand_delete=$time-604800;
$reponse = $bdd->prepare('SELECT id FROM pokemons_description_blind_battles WHERE quand<=:quand AND n_tour=0') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('quand' =>$quand_delete));
while($donnees = $reponse->fetch())
	{
	$id_battles=$donnees['id'];
	$reponse2 = $bdd->prepare('DELETE FROM pokemons_liste_pokemons_blind_battles WHERE id_battles=:id_battles') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id_battles' => $id_battles)); 
	$reponse2 = $bdd->prepare('DELETE FROM pokemons_description_blind_battles WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $id_battles)); 				
	}


//suppression des vieux défis en cours
$reponse = $bdd->query('SELECT quand, id FROM pokemons_blind_battles') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	$time_delete=$donnees['quand']+604800;
	if($time>$time_delete)
		{
		$id_combat_blind_battle=$donnees['id'];
		$count_combat=0;
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_blind_battles WHERE id_battles=:id_battles') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id_battles' => $donnees['id']));  
		while($donnees2 = $reponse2->fetch()){$count_combat=$count_combat+1;}
		if($count_combat==0)
			{
			$reponse = $bdd->prepare('DELETE FROM pokemons_blind_battles WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $donnees['id'])); 
			}
		if($count_combat==1) //VICTOIRE D OFFICE
			{
			$reponse2 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_blind_battles WHERE id_battles=:id_battles') or die(print_r($bdd->errorInfo()));
			$reponse2->execute(array('id_battles' => $donnees['id'])); 
			$donnees2 = $reponse2->fetch();	
			$vainqueur=$donnees2['pseudo'];
			$classe=$donnees2['classe'];
			$reponse3 = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo ') or die(print_r($bdd->errorInfo()));
			$reponse3->execute(array('pseudo' => $vainqueur)); 
			$donnees3 = $reponse3->fetch();
			$bds_max_vainqueur=$donnees['bds'];
			$reponse3 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
			$reponse3->execute(array('pseudo' => $vainqueur));
			while($donnees3 = $reponse3->fetch()){$nb_pokemons_vainqueur=$nb_pokemons_vainqueur+1;} 
			if($bds_max_vainqueur<=$nb_pokemons_vainqueur){$max_pokemon_atteint=1;}else{$max_pokemon_atteint=0;}
			//récompense
			
			if($classe=="D")
				{
				$rec=rand(1,10000);
				if($rec<=5000){$genre_recompense="item"; $recompense=1;$nb_recompense=1;}
				elseif($rec<=7500){$genre_recompense="argent"; $recompense=250;$nb_recompense=1;}
				elseif($rec<=9000){$genre_recompense="item"; $recompense=10;$nb_recompense=5;}
				elseif($rec<=9500){$genre_recompense="item"; $recompense=2;$nb_recompense=1;}
				elseif($rec<=9900){$genre_recompense="item"; $recompense=3;$nb_recompense=1;}
				elseif($rec<=9990){$genre_recompense="argent"; $recompense=1000;$nb_recompense=1;}
				elseif($max_pokemon_atteint!=1)
					{
					if($rec<=9999){$genre_recompense="pokemon"; $recompense=143;$nb_recompense=0;}
					elseif($rec==10000){$genre_recompense="pokemon"; $recompense=143;$nb_recompense=1;}
					}
				else
					{
					if($rec<=10000){$genre_recompense="argent"; $recompense=1000;$nb_recompense=1;}
					}
				}
			if($classe=="C")
				{
				$rec=rand(1,10000);
				if($rec<=5000){$genre_recompense="item"; $recompense=2;$nb_recompense=1;}
				elseif($rec<=7500){$genre_recompense="argent"; $recompense=500;$nb_recompense=1;}
				elseif($rec<=9000){$genre_recompense="item"; $recompense=11;$nb_recompense=5;}
				elseif($rec<=9500){$genre_recompense="item"; $recompense=3;$nb_recompense=1;}
				elseif($rec<=9900){$genre_recompense="argent"; $recompense=2000;$nb_recompense=1;}
				elseif($rec<=9950){$genre_recompense="item"; $recompense=23;$nb_recompense=1;}
				elseif($max_pokemon_atteint!=1)
					{
					if($rec<=9960){$genre_recompense="pokemon"; $recompense=7;$nb_recompense=0;}
					elseif($rec<=9970){$genre_recompense="pokemon"; $recompense=11;$nb_recompense=0;}
					elseif($rec<=9980){$genre_recompense="pokemon"; $recompense=14;$nb_recompense=0;}
					elseif($rec<=9990){$genre_recompense="pokemon"; $recompense=32;$nb_recompense=0;}
					elseif($rec<=10000){$genre_recompense="pokemon"; $recompense=144;$nb_recompense=0;}
					}
				else
					{
					if($rec<=10000){$genre_recompense="item"; $recompense=23;$nb_recompense=1;}
					}
				}
			if($classe=="B")
				{
				$rec=rand(1,10000);
				if($rec<=5000){$genre_recompense="item"; $recompense=3;$nb_recompense=1;}
				elseif($rec<=7500){$genre_recompense="argent"; $recompense=1000;$nb_recompense=1;}
				elseif($rec<=9000){$genre_recompense="item"; $recompense=12;$nb_recompense=5;}
				elseif($rec<=9400){$genre_recompense="argent"; $recompense=2000;$nb_recompense=1;}
				elseif($rec<=9500){$genre_recompense="item"; $recompense=5;$nb_recompense=1;}
				elseif($rec<=9600){$genre_recompense="item"; $recompense=6;$nb_recompense=1;}
				elseif($rec<=9700){$genre_recompense="item"; $recompense=7;$nb_recompense=1;}
				elseif($rec<=9800){$genre_recompense="item"; $recompense=8;$nb_recompense=1;}
				elseif($rec<=9900){$genre_recompense="item"; $recompense=9;$nb_recompense=1;}
				elseif($max_pokemon_atteint!=1)
					{
					if($rec<=9990){$genre_recompense="pokemon"; $recompense=158;$nb_recompense=0;}
					elseif($rec<=10000){$genre_recompense="pokemon"; $recompense=158;$nb_recompense=1;}
					}
				else
					{
					if($rec<=10000){$genre_recompense="item"; $recompense=9;$nb_recompense=1;}
					}
				}
			if($classe=="A")
				{
				$rec=rand(1,10000);
				if($rec<=5000){$genre_recompense="item"; $recompense=3;$nb_recompense=5;}
				elseif($rec<=7500){$genre_recompense="argent"; $recompense=2000;$nb_recompense=1;}
				elseif($rec<=9000){$genre_recompense="item"; $recompense=13;$nb_recompense=5;}
				elseif($rec<=9500){$genre_recompense="item"; $recompense=4;$nb_recompense=1;}
				elseif($max_pokemon_atteint!=1)
					{
					if($rec<=9590){$genre_recompense="pokemon"; $recompense=7;$nb_recompense=0;}
					elseif($rec<=9600){$genre_recompense="pokemon"; $recompense=7;$nb_recompense=1;}
					elseif($rec<=9690){$genre_recompense="pokemon"; $recompense=11;$nb_recompense=0;}
					elseif($rec<=9700){$genre_recompense="pokemon"; $recompense=11;$nb_recompense=1;}
					elseif($rec<=9790){$genre_recompense="pokemon"; $recompense=14;$nb_recompense=0;}
					elseif($rec<=9800){$genre_recompense="pokemon"; $recompense=14;$nb_recompense=1;}
					elseif($rec<=9890){$genre_recompense="pokemon"; $recompense=32;$nb_recompense=0;}
					elseif($rec<=9900){$genre_recompense="pokemon"; $recompense=32;$nb_recompense=1;}
					elseif($rec<=9990){$genre_recompense="pokemon"; $recompense=144;$nb_recompense=0;}
					elseif($rec<=10000){$genre_recompense="pokemon"; $recompense=144;$nb_recompense=1;}
					}
				else
					{
					if($rec<=10000){$genre_recompense="item"; $recompense=4;$nb_recompense=1;}
					}
				}
			//Touchage de récompense
			if($genre_recompense=="item")
				{
				$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pseudo' => $vainqueur, 'id_item' => $recompense));  
					$donnees = $reponse->fetch();
				if(isset($donnees['id']))
					{
					$quantite_total=$nb_recompense + $donnees['quantite'];
					$reponse = $bdd->prepare('UPDATE pokemons_inventaire SET quantite=:quantite WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('quantite' =>$quantite_total ,'pseudo' => $vainqueur, 'id_item' => $recompense)); 
					}
				else
					{
					$req = $bdd->prepare('INSERT INTO pokemons_inventaire (pseudo, id_item, quantite) VALUES(:pseudo, :id_item, :quantite)') or die(print_r($bdd->errorInfo()));
					$req->execute(array('pseudo' => $vainqueur, 'id_item' => $recompense,'quantite' => $nb_recompense))or die(print_r($bdd->errorInfo()));
					}			
				}
			if($genre_recompense=="argent")
				{	
				$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pseudo' => $vainqueur));  
					$donnees = $reponse->fetch();
				$argent=$donnees['pokedollar'];
				$argent_now=$argent+$recompense;
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar  WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('pokedollar' => $argent_now, 'pseudo' => $vainqueur));		
				}
			if($genre_recompense=="pokemon")
				{							
				$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id' => $recompense));
				$donnees = $reponse->fetch();
				$proba_sexe=rand(1,2); if($proba_sexe==1){$sexe="M";} elseif($proba_sexe==2){$sexe="F";}
				$randpv=rand(0,4);$randatt=rand(0,4);$randdef=rand(0,4);$randvit=rand(0,4);$randattspe=rand(0,4);$randdefspe=rand(0,4);
				$pv=$donnees['pv']+4*$donnees['bonus_pv']+$randpv;
				$att=$donnees['att']+4*$donnees['bonus_att']+$randatt;
				$def=$donnees['def']+4*$donnees['bonus_def']+$randdef;
				$vit=$donnees['vit']+4*$donnees['bonus_vit']+$randvit;
				$attspe=$donnees['attspe']+4*$donnees['bonus_attspe']+$randattspe;
				$defspe=$donnees['defspe']+4*$donnees['bonus_defspe']+$randdefspe;
				if($donnees['lvl1']!=0){$attaque1=$donnees['lvl1'];}else{$attaque1=0;}
				if($donnees['lvl2']!=0){$attaque2=$donnees['lvl2'];}else{$attaque2=0;}
				if($donnees['lvl5']!=0){$attaque3=$donnees['lvl5'];}else{$attaque3=0;}
				$req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons (pseudo, id_pokemon, shiney, sexe, lvl, xp, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, actif, pa_restant, pa_max, pa_bonus) VALUES(:pseudo, :id_pokemon, :shiney, :sexe, 5, 100, :pv,:pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, 0, 0, 10, 10, 5)') or die(print_r($bdd->errorInfo()));
										$req->execute(array(
												'pseudo' => $vainqueur, 
												'id_pokemon' => $recompense,
												'shiney' => $nb_recompense,
												'sexe' => $sexe,
												'pv' => $pv,
												'pv_max' => $pv,
												'att' => $att,
												'def' => $def,
												'vit' => $vit,
												'attspe' => $attspe,
												'defspe' => $defspe,
												'attaque1' => $attaque1,
												'attaque2' => $attaque2,
												'attaque3' => $attaque3	
												))or die(print_r($bdd->errorInfo()));
				}
			//touchage d'xp
			if($classe=="D"){$xp_vainqueur=50;}if($classe=="C"){$xp_vainqueur=100;}if($classe=="B"){$xp_vainqueur=150;}if($classe=="A"){$xp_vainqueur=200;}				
			$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $donnees2['id_liste_pokemons']));  
			$donnees = $reponse->fetch();
			$xp_pokemon_actif=$donnees['xp'];
			$victoires_totales=$donnees['victoires']+1;
			$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET victoires=:victoires WHERE id=:id') or die(print_r($bdd->errorInfo()));
						  $reponse->execute(array('victoires' => $victoires_totales,'id' => $donnees2['id_liste_pokemons'])); 
			$gain_xp=$xp_vainqueur;
			$xp_apres_combat=$xp_pokemon_actif+$gain_xp;
			$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET xp=:xp WHERE id=:id') or die(print_r($bdd->errorInfo()));
						  $reponse->execute(array('xp' => $xp_apres_combat,'id' => $donnees2['id_liste_pokemons'])); 
			//envoi mp
			if($genre_recompense=="item")
				{
				$reponse = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
									$reponse->execute(array('id' => $recompense));  
									$donnees = $reponse->fetch();
				$nom_item=$donnees['nom'];
				}
			if($genre_recompense=="pokemon")
				{
				$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
									$reponse->execute(array('id' => $recompense));  
									$donnees = $reponse->fetch();
				$nom_pokemon=$donnees['nom'];
				}
			$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id' => $donnees2['id_pokemon']));  
				$donnees = $reponse->fetch();
			$nom_pokemon_vainqueur=$donnees['nom'];
			if($genre_recompense=="item"){$desc_recompense='Vous avez reçu '.$nb_recompense.' '.$nom_item.'.';}
			if($genre_recompense=="argent"){$desc_recompense='Vous avez reçu '.$recompense.'$.';}
			if($genre_recompense=="pokemon"){if($nb_recompense==1){$brille='shiney!';}else{$brille="";}$desc_recompense='Vous avez reçu un '.$nom_pokemon.''.$brille; }
				//vainqueur
			$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("Prof Saul", :destinataire, "non lu", :titre, :message, now())') or die(print_r($bdd->errorInfo()));
					$req->execute(array(
						'destinataire' => $vainqueur,	
						'titre'	=> 'Victoire déclenchée en Blind Battles classe '.$classe.'',
						'message' => 'félicitation, vous avez remporté le match de classe '.$classe.' avec votre '.$nom_pokemon_vainqueur.', puisqu\'aucun adversaire ne s\'est présenté dans les temps.
'.$desc_recompense.''
						))
						or die(print_r($bdd->errorInfo()));
			//Ajout victoire au vainqueur
			$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('pseudo' => $vainqueur));
				$donnees = $reponse->fetch();
			$points_quetes=$donnees['points_quetes'];
			$victoires_D=$donnees['victoires_D'];
			$victoires_C=$donnees['victoires_C'];
			$victoires_B=$donnees['victoires_B'];
			$victoires_A=$donnees['victoires_A'];
			if($classe=="D")
				{
				$difficulte_quete_now=$points_quetes+1;
				$victoires_D=$victoires_D+1;
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET points_quetes=:points_quetes, victoires_D=:victoires_D WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
				$reponse->execute(array('points_quetes' => $difficulte_quete_now,'victoires_D' => $victoires_D, 'pseudo' => $vainqueur)); 
				}
			if($classe=="C")
				{
				$difficulte_quete_now=$points_quetes+2;
				$victoires_C=$victoires_C+1;
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET points_quetes=:points_quetes, victoires_C=:victoires_C WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
				$reponse->execute(array('points_quetes' => $difficulte_quete_now,'victoires_C' => $victoires_C, 'pseudo' => $vainqueur)); 
				}
			if($classe=="B")
				{
				$difficulte_quete_now=$points_quetes+3;
				$victoires_B=$victoires_B+1;
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET points_quetes=:points_quetes, victoires_B=:victoires_B WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
				$reponse->execute(array('points_quetes' => $difficulte_quete_now,'victoires_B' => $victoires_B, 'pseudo' => $vainqueur)); 
				}
			if($classe=="A")
				{
				$difficulte_quete_now=$points_quetes+4;
				$victoires_A=$victoires_A+1;
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET points_quetes=:points_quetes, victoires_A=:victoires_A WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
				$reponse->execute(array('points_quetes' => $difficulte_quete_now,'victoires_A' => $victoires_A, 'pseudo' => $vainqueur)); 
				}	
			//deletage du choix
			$reponse = $bdd->prepare('DELETE FROM pokemons_blind_battles WHERE id=:id') or die(print_r($bdd->errorInfo()));
									$reponse->execute(array('id' => $id_combat_blind_battle)); 					
			}
		}
	}

//création des tests
//CLASSE D
$num_d=0;
$reponse = $bdd->query('SELECT id FROM pokemons_blind_battles WHERE type="D"') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch()){$num_d=$num_d+1;}
if($num_d<15){$nb_d_manquants=15-$num_d;}
else {$nb_d_manquants = 0;}

$d_ajouts=0;
while($d_ajouts<$nb_d_manquants)
	{
	$d_ajouts=$d_ajouts+1;
	$rand=rand(1,6);
	// 1 = lvl; 2=race ou type; 3=shiney; 4=points; 5=sexe; 6=victoires
	if($rand==1) //lvl
		{
		$lvl=rand(5,25);
		$condition_lvl=rand(1,3);if($condition_lvl==1){$condition_lvl=">";}if($condition_lvl==2){$condition_lvl="<";}if($condition_lvl==3){$condition_lvl="=";}
		$req = $bdd->prepare('INSERT INTO pokemons_blind_battles (type, lvl, condition_lvl, quand) VALUES("D", :lvl, :condition_lvl,:quand )') or die(print_r($bdd->errorInfo()));
			$req->execute(array('lvl' => $lvl, 'condition_lvl' => $condition_lvl, 'quand' =>$time))or die(print_r($bdd->errorInfo()));
		}
	if($rand==2) //race ou type
		{
		$rand2=rand(1,6);
		if($rand2<6)//type
			{
			$type=rand(1,15);
			if($type==1){$type="combat";}if($type==2){$type="dragon";}if($type==3){$type="eau";}if($type==4){$type="electrique";}if($type==5){$type="feu";}
			if($type==6){$type="glace";}if($type==7){$type="insecte";}if($type==8){$type="normal";}if($type==9){$type="plante";}if($type==10){$type="poison";}
			if($type==11){$type="psy";}if($type==12){$type="roche";}if($type==13){$type="sol";}if($type==14){$type="spectre";}if($type==15){$type="vol";}
			$req = $bdd->prepare('INSERT INTO pokemons_blind_battles (type, type_pokemon, quand) VALUES("D", :type_pokemon, :quand)') or die(print_r($bdd->errorInfo()));
			$req->execute(array('type_pokemon' => $type, 'quand' =>$time))or die(print_r($bdd->errorInfo()));
			}
		if($rand2==6)//race
			{
			$nb_pokemons_diff=0;$id_pok=0;
			$reponse = $bdd->query('SELECT * FROM pokemons_liste_pokemons ORDER BY id_pokemon') or die(print_r($bdd->errorInfo()));
			while($donnees = $reponse->fetch()){if($donnees['id_pokemon']!=$id_pok){$nb_pokemons_diff=$nb_pokemons_diff+1; $id_pok=$donnees['id_pokemon'];}}
			$race=rand(1,$nb_pokemons_diff);
			$nb_pokemons_diff=0;$id_pok=0;
			$reponse = $bdd->query('SELECT * FROM pokemons_liste_pokemons ORDER BY id_pokemon') or die(print_r($bdd->errorInfo()));
			while($donnees = $reponse->fetch()){if($donnees['id_pokemon']!=$id_pok){$nb_pokemons_diff=$nb_pokemons_diff+1; $id_pok=$donnees['id_pokemon'];if($nb_pokemons_diff==$race){$race2=$donnees['id_pokemon'];}}}
			$race=$race2;
			$req = $bdd->prepare('INSERT INTO pokemons_blind_battles (type, race, quand) VALUES("D", :race, :quand)') or die(print_r($bdd->errorInfo()));
			$req->execute(array('race' => $race, 'quand' =>$time))or die(print_r($bdd->errorInfo()));
			}
		}
	if($rand==3) //shiney
		{
		$req = $bdd->prepare('INSERT INTO pokemons_blind_battles (type, shiney, quand) VALUES("D", 1, :quand)') or die(print_r($bdd->errorInfo()));
			$req->execute(array('quand' =>$time))or die(print_r($bdd->errorInfo()));			
		}
	if($rand==4) //points
		{
		$points=rand(200,500);
		$condition_points=rand(1,2);if($condition_points==1){$condition_points=">";}if($condition_points==2){$condition_points="<";}
		$req = $bdd->prepare('INSERT INTO pokemons_blind_battles (type, points, condition_points, quand) VALUES("D", :points, :condition_points, :quand)') or die(print_r($bdd->errorInfo()));
			$req->execute(array('points' => $points, 'condition_points' => $condition_points, 'quand' =>$time))or die(print_r($bdd->errorInfo()));
		}
	if($rand==5) //sexe
		{
		$sexe=rand(1,2);if($sexe==1){$sexe="M";}if($sexe==2){$sexe="F";}
		$req = $bdd->prepare('INSERT INTO pokemons_blind_battles (type, sexe, quand) VALUES("D", :sexe, :quand)') or die(print_r($bdd->errorInfo()));
			$req->execute(array('sexe' => $sexe, 'quand' =>$time))or die(print_r($bdd->errorInfo()));
		}
	if($rand==6) //victoires
		{
		$victoires=rand(20,100);
		$condition_victoires=rand(1,2);if($condition_victoires==1){$condition_victoires=">";}if($condition_victoires==2){$condition_victoires="<";}
		$req = $bdd->prepare('INSERT INTO pokemons_blind_battles (type, victoires, condition_victoires, quand) VALUES("D", :victoires, :condition_victoires, :quand)') or die(print_r($bdd->errorInfo()));
			$req->execute(array('victoires' => $victoires, 'condition_victoires' => $condition_victoires, 'quand' =>$time))or die(print_r($bdd->errorInfo()));
		}
	}
$lvl=0; $condition_lvl="";$race=0;$type="";$shiney=0;$points=0; $condition_points="";$sexe=""; $victoires=0; $condition_victoires="";
//CLASSE C
$num_c=0;
$reponse = $bdd->query('SELECT id FROM pokemons_blind_battles WHERE type="C"') or die(print_r($bdd->errorInfo()));while($donnees = $reponse->fetch()){$num_c=$num_c+1;}
if($num_c<15){$nb_c_manquants=15-$num_c;} else {$nb_c_manquants = 0;}
$c_ajouts=0;
while($c_ajouts<$nb_c_manquants)
	{
	$c_ajouts=$c_ajouts+1;
	$rand1=0;$rand2=0;
	$rand1=rand(1,6);
	while($rand2==0 OR $rand2==$rand1){$rand2=rand(1,6);}
	$lvl=0; $condition_lvl="";$race=0;$type="";$shiney=0;$points=0; $condition_points="";$sexe=""; $victoires=0; $condition_victoires="";
	// 1 = lvl; 2=race ou type; 3=shiney; 4=points; 5=sexe; 6=victoires
	if($rand1==1 OR $rand2==1) //lvl
		{
		$lvl=rand(25,50);
		$condition_lvl=rand(1,3);if($condition_lvl==1){$condition_lvl=">";}if($condition_lvl==2){$condition_lvl="<";}if($condition_lvl==3){$condition_lvl="=";}
		}
	if($rand1==2 OR $rand2==2) //race ou type
		{
		$rand_choix=rand(1,6);
		if($rand_choix<5)//type
			{
			$type=rand(1,15);
			if($type==1){$type="combat";}if($type==2){$type="dragon";}if($type==3){$type="eau";}if($type==4){$type="electrique";}if($type==5){$type="feu";}
			if($type==6){$type="glace";}if($type==7){$type="insecte";}if($type==8){$type="normal";}if($type==9){$type="plante";}if($type==10){$type="poison";}
			if($type==11){$type="psy";}if($type==12){$type="roche";}if($type==13){$type="sol";}if($type==14){$type="spectre";}if($type==15){$type="vol";}
			}
		if($rand_choix>=5)//race
			{
			$nb_pokemons_diff=0;$id_pok=0;
			$reponse = $bdd->query('SELECT * FROM pokemons_liste_pokemons ORDER BY id_pokemon') or die(print_r($bdd->errorInfo()));
			while($donnees = $reponse->fetch()){if($donnees['id_pokemon']!=$id_pok){$nb_pokemons_diff=$nb_pokemons_diff+1; $id_pok=$donnees['id_pokemon'];}}
			$race=rand(1,$nb_pokemons_diff);
			$nb_pokemons_diff=0;$id_pok=0;
			$reponse = $bdd->query('SELECT * FROM pokemons_liste_pokemons ORDER BY id_pokemon') or die(print_r($bdd->errorInfo()));
			while($donnees = $reponse->fetch()){if($donnees['id_pokemon']!=$id_pok){$nb_pokemons_diff=$nb_pokemons_diff+1; $id_pok=$donnees['id_pokemon'];if($nb_pokemons_diff==$race){$race2=$donnees['id_pokemon'];}}}
			$race=$race2;			
			}
		}
	if($rand1==3 OR $rand2==3) //shiney
		{
		$shiney=rand(1,2);
		}
	if($rand1==4 OR $rand2==4) //points
		{
		$points=rand(200,750);
		$condition_points=rand(1,2);if($condition_points==1){$condition_points=">";}if($condition_points==2){$condition_points="<";}
		}
	if($rand1==5 OR $rand2==5) //sexe
		{
		$sexe=rand(1,2);if($sexe==1){$sexe="M";}if($sexe==2){$sexe="F";}
		}
	if($rand1==6 OR $rand2==6) //victoires
		{
		$victoires=rand(40,150);
		$condition_victoires=rand(1,2);if($condition_victoires==1){$condition_victoires=">";}if($condition_victoires==2){$condition_victoires="<";}
		}
	$req = $bdd->prepare('INSERT INTO pokemons_blind_battles (type, lvl, condition_lvl, race, type_pokemon, shiney, points, condition_points, sexe, victoires, condition_victoires, quand) VALUES("C", :lvl, :condition_lvl, :race, :type_pokemon, :shiney, :points, :condition_points, :sexe, :victoires, :condition_victoires, :quand)') or die(print_r($bdd->errorInfo()));
			$req->execute(array('lvl' => $lvl, 'condition_lvl'=>$condition_lvl, 'race' => $race, 'type_pokemon' =>$type, 'shiney' =>$shiney, 'points' => $points, 'condition_points' =>$condition_points, 'sexe' => $sexe, 'victoires' => $victoires, 'condition_victoires' => $condition_victoires, 'quand' =>$time))or die(print_r($bdd->errorInfo()));
		
	}
$lvl=0; $condition_lvl="";$race=0;$type="";$shiney=0;$points=0; $condition_points="";$sexe=""; $victoires=0; $condition_victoires="";
//CLASSE B
$num_b=0;
$reponse = $bdd->query('SELECT id FROM pokemons_blind_battles WHERE type="B"') or die(print_r($bdd->errorInfo()));while($donnees = $reponse->fetch()){$num_b=$num_b+1;}
if($num_b<15){$nb_b_manquants=15-$num_b;} else {$nb_b_manquants = 0;}
$b_ajouts=0;
while($b_ajouts<$nb_b_manquants)
	{
	$b_ajouts=$b_ajouts+1;
	$rand1=0;$rand2=0;$rand3=0;
	$rand1=rand(1,6);
	while($rand2==0 OR $rand2==$rand1){$rand2=rand(1,6);}
	while($rand3==0 OR $rand3==$rand1 OR $rand3==$rand2){$rand3=rand(1,6);}
	$lvl=0; $condition_lvl="";$race=0;$type="";$shiney=0;$points=0; $condition_points="";$sexe=""; $victoires=0; $condition_victoires="";
	// 1 = lvl; 2=race ou type; 3=shiney; 4=points; 5=sexe; 6=victoires
	if($rand1==1 OR $rand2==1 OR $rand3==1) //lvl
		{
		$lvl=rand(40,80);
		$condition_lvl=rand(1,3);if($condition_lvl==1){$condition_lvl=">";}if($condition_lvl==2){$condition_lvl=">";}if($condition_lvl==3){$condition_lvl=">";}
		}
	if($rand1==2 OR $rand2==2 OR $rand3==2) //race ou type
		{
		$rand_choix=rand(1,6);
		if($rand_choix<5)//type
			{
			$type=rand(1,15);
			if($type==1){$type="combat";}if($type==2){$type="dragon";}if($type==3){$type="eau";}if($type==4){$type="electrique";}if($type==5){$type="feu";}
			if($type==6){$type="glace";}if($type==7){$type="insecte";}if($type==8){$type="normal";}if($type==9){$type="plante";}if($type==10){$type="poison";}
			if($type==11){$type="psy";}if($type==12){$type="roche";}if($type==13){$type="sol";}if($type==14){$type="spectre";}if($type==15){$type="vol";}
			}
		if($rand_choix>=5)//race
			{
			$nb_pokemons_diff=0;$id_pok=0;
			$reponse = $bdd->query('SELECT * FROM pokemons_liste_pokemons ORDER BY id_pokemon LIMIT 10000') or die(print_r($bdd->errorInfo()));
			while($donnees = $reponse->fetch()){if($donnees['id_pokemon']!=$id_pok){$nb_pokemons_diff=$nb_pokemons_diff+1; $id_pok=$donnees['id_pokemon'];}}
			$race=rand(1,$nb_pokemons_diff);
			$nb_pokemons_diff=0;$id_pok=0;
			$reponse = $bdd->query('SELECT * FROM pokemons_liste_pokemons ORDER BY id_pokemon LIMIT 10000') or die(print_r($bdd->errorInfo()));
			while($donnees = $reponse->fetch()){if($donnees['id_pokemon']!=$id_pok){$nb_pokemons_diff=$nb_pokemons_diff+1; $id_pok=$donnees['id_pokemon'];if($nb_pokemons_diff==$race){$race2=$donnees['id_pokemon'];}}}
			$race=$race2;
			}
		}
	if($rand1==3 OR $rand2==3 OR $rand3==3) //shiney
		{
		$shiney=rand(1,2);
		}
	if($rand1==4 OR $rand2==4 OR $rand3==4) //points
		{
		$points=rand(500,1500);
		$condition_points=rand(1,2);if($condition_points==1){$condition_points=">";}if($condition_points==2){$condition_points=">";}
		}
	if($rand1==5 OR $rand2==5 OR $rand3==5) //sexe
		{
		$sexe=rand(1,2);if($sexe==1){$sexe="M";}if($sexe==2){$sexe="F";}
		}
	if($rand1==6 OR $rand2==6 OR $rand3==6) //victoires
		{
		$victoires=rand(100,250);
		$condition_victoires=">";
		}
	$req = $bdd->prepare('INSERT INTO pokemons_blind_battles (type, lvl, condition_lvl, race, type_pokemon, shiney, points, condition_points, sexe, victoires, condition_victoires, quand) VALUES("B", :lvl, :condition_lvl, :race, :type_pokemon, :shiney, :points, :condition_points, :sexe, :victoires, :condition_victoires, :quand)') or die(print_r($bdd->errorInfo()));
			$req->execute(array('lvl' => $lvl, 'condition_lvl'=>$condition_lvl, 'race' => $race, 'type_pokemon' =>$type, 'shiney' =>$shiney, 'points' => $points, 'condition_points' =>$condition_points, 'sexe' => $sexe, 'victoires' => $victoires, 'condition_victoires' => $condition_victoires, 'quand' =>$time))or die(print_r($bdd->errorInfo()));
		
	}
$lvl=0; $condition_lvl="";$race=0;$type="";$shiney=0;$points=0; $condition_points="";$sexe=""; $victoires=0; $condition_victoires="";
//CLASSE A
$num_a=0;
$reponse = $bdd->query('SELECT id FROM pokemons_blind_battles WHERE type="A"') or die(print_r($bdd->errorInfo()));while($donnees = $reponse->fetch()){$num_a=$num_a+1;}
if($num_a<15){$nb_a_manquants=15-$num_a;} else {$nb_a_manquants = 0;}
$a_ajouts=0;
while($a_ajouts<$nb_a_manquants)
	{
	$a_ajouts=$a_ajouts+1;
	$rand1=0;$rand2=0;$rand3=0;$rand4=0;
	$rand1=rand(1,6);
	while($rand2==0 OR $rand2==$rand1){$rand2=rand(1,6);}
	while($rand3==0 OR $rand3==$rand1 OR $rand3==$rand2){$rand3=rand(1,6);}
	while($rand4==0 OR $rand4==$rand1 OR $rand4==$rand2 OR $rand4==$rand3){$rand4=rand(1,6);}
	$lvl=0; $condition_lvl="";$race=0;$type="";$shiney=0;$points=0; $condition_points="";$sexe=""; $victoires=0; $condition_victoires="";
	// 1 = lvl; 2=race ou type; 3=shiney; 4=points; 5=sexe; 6=victoires
	if($rand1==1 OR $rand2==1 OR $rand3==1 OR $rand4==1) //lvl
		{
		$lvl=rand(75,90);
		$condition_lvl=rand(1,3);if($condition_lvl==1){$condition_lvl=">";}if($condition_lvl==2){$condition_lvl=">";}if($condition_lvl==3){$condition_lvl=">";}
		}
	if($rand1==2 OR $rand2==2 OR $rand3==2 OR $rand4==2) //race ou type
		{
		$rand_choix=rand(1,6);
		if($rand_choix<4)//type
			{
			$type=rand(1,15);
			if($type==1){$type="combat";}if($type==2){$type="dragon";}if($type==3){$type="eau";}if($type==4){$type="electrique";}if($type==5){$type="feu";}
			if($type==6){$type="glace";}if($type==7){$type="insecte";}if($type==8){$type="normal";}if($type==9){$type="plante";}if($type==10){$type="poison";}
			if($type==11){$type="psy";}if($type==12){$type="roche";}if($type==13){$type="sol";}if($type==14){$type="spectre";}if($type==15){$type="vol";}
			}
		if($rand_choix>=4)//race
			{
			$nb_pokemons_diff=0;$id_pok=0;
			$reponse = $bdd->query('SELECT * FROM pokemons_liste_pokemons ORDER BY id_pokemon LIMIT 10000') or die(print_r($bdd->errorInfo()));
			while($donnees = $reponse->fetch()){if($donnees['id_pokemon']!=$id_pok){$nb_pokemons_diff=$nb_pokemons_diff+1; $id_pok=$donnees['id_pokemon'];}}
			$race=rand(1,$nb_pokemons_diff);
			$nb_pokemons_diff=0;$id_pok=0;
			$reponse = $bdd->query('SELECT * FROM pokemons_liste_pokemons ORDER BY id_pokemon LIMIT 10000') or die(print_r($bdd->errorInfo()));
			while($donnees = $reponse->fetch()){if($donnees['id_pokemon']!=$id_pok){$nb_pokemons_diff=$nb_pokemons_diff+1; $id_pok=$donnees['id_pokemon'];if($nb_pokemons_diff==$race){$race2=$donnees['id_pokemon'];}}}
			$race=$race2;
			}
		}
	if($rand1==3 OR $rand2==3 OR $rand3==3 OR $rand4==3) //shiney
		{
		$shiney=rand(1,2);
		}
	if(
	$rand1==4 OR $rand2==4 OR $rand3==4 OR $rand4==4) //points
		{
		$points=rand(1000,2250);
		$condition_points=rand(1,2);if($condition_points==1){$condition_points=">";}if($condition_points==2){$condition_points=">";}
		}
	if($rand1==5 OR $rand2==5 OR $rand3==5 OR $rand4==5) //sexe
		{
		$sexe=rand(1,2);if($sexe==1){$sexe="M";}if($sexe==2){$sexe="F";}
		}
	if($rand1==6 OR $rand2==6 OR $rand3==6 OR $rand4==6) //victoires
		{
		$victoires=rand(250,750);
		$condition_victoires=">";
		}
	$req = $bdd->prepare('INSERT INTO pokemons_blind_battles (type, lvl, condition_lvl, race, type_pokemon, shiney, points, condition_points, sexe, victoires, condition_victoires, quand) VALUES("A", :lvl, :condition_lvl, :race, :type_pokemon, :shiney, :points, :condition_points, :sexe, :victoires, :condition_victoires, :quand)') or die(print_r($bdd->errorInfo()));
			$req->execute(array('lvl' => $lvl, 'condition_lvl'=>$condition_lvl, 'race' => $race, 'type_pokemon' =>$type, 'shiney' =>$shiney, 'points' => $points, 'condition_points' =>$condition_points, 'sexe' => $sexe, 'victoires' => $victoires, 'condition_victoires' => $condition_victoires, 'quand' =>$time))or die(print_r($bdd->errorInfo()));
		
	}

	
$reponse = $bdd->prepare('SELECT victoires_D, victoires_C, victoires_B, victoires_A FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
$victoires_D=$donnees['victoires_D'];
$victoires_C=$donnees['victoires_C'];
$victoires_B=$donnees['victoires_B'];
$victoires_A=$donnees['victoires_A'];
?>

<h3>Blind Battles </h3>
<!-- AFFICHAGE -->
<form action="inscription_blind_battles.php" method="post">
<input type="hidden" name="classe" value="D"/>
<input type="submit" value="Classe D" />	
</form>  
<?php 
if($victoires_D>=25)
{
?>
<form action="inscription_blind_battles.php" method="post">
<input type="hidden" name="classe" value="C"/>
<input type="submit" value="Classe C" />	
</form>
<?php 
}
if($victoires_C>=25)
{
?>
<form action="inscription_blind_battles.php" method="post">
<input type="hidden" name="classe" value="B"/>
<input type="submit" value="Classe B" />	
</form>
<?php 
}
if($victoires_B>=25)
{
?>
<form action="inscription_blind_battles.php" method="post">
<input type="hidden" name="classe" value="A"/>
<input type="submit" value="Classe A" />	
</form>
<?php 
}
?>

<p style="text-align:justify;">Venez affronter les autres joueurs dans les laboratoires du professeur Saul. <br />
Pour avoir accès à la classe suivante, vous devez avoir gagné 25 combats dans la classe précédente. <br />
Les récompenses et le nombre de restrictions sont plus importantes en fonction des classes.<br />
L'inscription d'un pokémon coute 1 pépites et consomme 2PA au pokémon inscrit.<br />
<a href="http://pokemon-origins.forumactif.com/t64-faq#126" style="color:blue;">Plus d'informations sur le Blind Battles </a></p>

<?php
if(isset($_POST['classe'])){$classe=$_POST['classe'];}else{$classe="D";}
?>
<b>Nombre de victoires en classe <?php echo $classe; ?> : <?php if($classe=="D"){echo $victoires_D;}if($classe=="C"){echo $victoires_C;}if($classe=="B"){echo $victoires_B;}if($classe=="A"){echo $victoires_A;} ?></b><br /><br /> 



<?php //inscription

if(isset($_POST['action']) AND $_POST['action']=="inscription")
	{
	$count=0;
	$reponse = $bdd->prepare('SELECT pseudo FROM pokemons_liste_pokemons_blind_battles WHERE id_battles=:id_battles') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id_battles' => $_POST['id_match']));  
	while($donnees = $reponse->fetch())
		{
		$count=$count+1;
		if($donnees['pseudo']==$_SESSION['pseudo']){$deja_inscrit=1;}
		}
	if($deja_inscrit==1){echo '<p style="color:red;text-align:center;">Vous ne pouvez pas inscrire plus d\'un pokémon par match.</p>';}
	elseif($_POST['id_liste_pokemons']=="" OR $_POST['id_liste_pokemons']==0 OR !isset($_POST['id_liste_pokemons'])){echo '<p style="color:red;text-align:center;">Vous n\'avez sélectionné aucun pokémon.</p>';}
	elseif($count>1){echo '<p style="color:red;text-align:center;">Il y a déjà deux pokémons inscris pour ce match, vous n\'avez pas été assez rapide.</p>';}
	else //inscription
		{
		$reponse = $bdd->prepare('SELECT ors FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
		$donnees = $reponse->fetch();
		$ors=$donnees['ors'];
		if($ors>=1)
			{
			$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $_POST['id_liste_pokemons']));  
			$donnees = $reponse->fetch();
			$id_pokemon=$donnees['id_pokemon'];$shiney=$donnees['shiney'];$sexe=$donnees['sexe'];$lvl=$donnees['lvl'];$pv_max=$donnees['pv_max'];
			$att=$donnees['att'];$def=$donnees['def'];$vit=$donnees['vit'];$attspe=$donnees['attspe'];$defspe=$donnees['defspe'];
			$attaque1=$donnees['attaque1'];$attaque2=$donnees['attaque2'];$attaque3=$donnees['attaque3'];$attaque4=$donnees['attaque4'];
			$bonheur=$donnees['bonheur'];$objet=$donnees['objet'];
                        if($attaque1!=0 OR $attaque2!=0 OR $attaque3!=0 OR $attaque4!=0){
                                $pa_restant=$donnees['pa_restant']-2;
                                $reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET pa_restant=:pa_restant WHERE id=:id') or die(print_r($bdd->errorInfo())); 
                                                $reponse->execute(array('pa_restant' => $pa_restant, 'id' => $_POST['id_liste_pokemons'])); 
                                $ors_restant=$ors-1;
                                $reponse = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
                                                $reponse->execute(array('ors' => $ors_restant, 'pseudo' => $_SESSION['pseudo'])); 
                                $req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons_blind_battles (id_battles, classe, pseudo, id_liste_pokemons, id_pokemon, shiney, sexe, lvl, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, bonheur, objet) VALUES(:id_battles, :classe, :pseudo, :id_liste_pokemons, :id_pokemon, :shiney, :sexe, :lvl, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, :bonheur, :objet)') or die(print_r($bdd->errorInfo()));
                                $req->execute(array(
                                                'id_battles' => $_POST['id_match'], 
                                                'classe' => $classe,
                                                'pseudo' => $_SESSION['pseudo'],
                                                'id_liste_pokemons' => $_POST['id_liste_pokemons'],
                                                'id_pokemon' => $id_pokemon,
                                                'shiney' => $shiney,
                                                'sexe' => $sexe,
                                                'lvl' => $lvl,
                                                'pv_max' => $pv_max,
                                                'att' => $att,
                                                'def' => $def,
                                                'vit' => $vit,
                                                'attspe' => $attspe,
                                                'defspe' => $defspe,
                                                'attaque1' => $attaque1,
                                                'attaque2' => $attaque2,
                                                'attaque3' => $attaque3,
                                                'attaque4' => $attaque4,
                                                'bonheur' => $bonheur,
                                                'objet' => $objet
                                                ))or die(print_r($bdd->errorInfo()));
                                echo '<p style="font-color:red;text-align:center;">Votre pokémon a bien été inscrit.</p>';
                                if($count==1) //combat
                                        {
                                        ?>
                                        <?php include ("blind_battles.php"); ?>
                                        <?php
                                        echo '<p style="font-color:red;text-align:center;">Voir le résultat : <a href="rapport_blind_battles.php?id='.$_POST['id_match'].'">ICI</a>.</p>';
                                        }
                                }else{
                                    echo '<p style="color:red;text-align:center;">Votre pokémon doit connaitre une attaque pour participer.</p>';
                                }
			}
		}
	}
	
//actualisation des données
$reponse = $bdd->prepare('SELECT victoires_D, victoires_C, victoires_B, victoires_A, ors FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
$victoires_D=$donnees['victoires_D'];
$victoires_C=$donnees['victoires_C'];
$victoires_B=$donnees['victoires_B'];
$victoires_A=$donnees['victoires_A'];
$ors=$donnees['ors'];
?>


<table id="votre_pokemon" width="550px" cellpadding="2" cellspacing="1" style="text-align:center;" >
<colgroup><COL WIDTH=5%><COL WIDTH=13%><COL WIDTH=13%><COL WIDTH=13%><COL WIDTH=13%> <COL WIDTH=13%><COL WIDTH=13%><COL WIDTH=17%></COLGROUP>
<tr style="background-color:#6495ed;"><th>Id</th><th>Lvl</th><th>Pokémon</th><th>Shiney</th><th>Points</th><th>Sexe</th><th>Victoires</th><th>Inscrire</th></tr>
<?php
$reponse = $bdd->prepare('SELECT * FROM pokemons_blind_battles WHERE type=:type') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('type' => $classe));  
while($donnees = $reponse->fetch())
	{
	$deja_inscrit=0;
	$reponse2 = $bdd->prepare('SELECT pseudo FROM pokemons_liste_pokemons_blind_battles WHERE id_battles=:id_battles') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id_battles' => $donnees['id']));  
	while($donnees2 = $reponse2->fetch())
		{if($donnees2['pseudo']==$_SESSION['pseudo']){$deja_inscrit=1;}}
	$lvl=0; $pokemon=0;$shiney=0;$points=0;$sexe=0;$victoires=0;
	if($donnees['type_pokemon']!=""){$pokemon='type '.$donnees['type_pokemon'];}
	if($donnees['race']!=0)
		{
		$reponse2 = $bdd->prepare('SELECT nom FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['race']));  
		$donnees2 = $reponse2->fetch();
		$pokemon=$donnees2['nom'];
		}
	if($donnees['type_pokemon']=="" AND $donnees['race']==0){$pokemon='';}
	$id_match=$donnees['id'];
	if($donnees['lvl']!=0){$lvl=$donnees['condition_lvl'].''.$donnees['lvl'];}else {$lvl='';}
	if($donnees['shiney']==1){$shiney='uniquement shiney';} elseif($donnees['shiney']==2){$shiney='pas de shiney';} else{$shiney='';}
	if($donnees['points']!=0){$points=$donnees['condition_points'].''.$donnees['points'];}else{$points='';}
	if($donnees['sexe']=="M"){$sexe='pas de femelle';}elseif($donnees['sexe']=="F"){$sexe='pas de mâle';}else{$sexe='';}
	if($donnees['victoires']!=0){$victoires=$donnees['condition_victoires'].''.$donnees['victoires'];}else{$victoires='';}
	echo '<tr><td>'.$id_match.'</td><td>'.$lvl.'</td><td>'.$pokemon.'</td><td>'.$shiney.'</td><td>'.$points.'</td><td>'.$sexe.'</td><td>'.$victoires.'</td><td>';
	if($ors>=1)
		{
		if($deja_inscrit!=1)
			{
			echo'<form action="inscription_blind_battles.php" method="post">';
			$count=0;
			if($tri_points==0){$reponse3 = $bdd->prepare('SELECT id_pokemon, lvl, score, victoires, defaites, pa_restant, id, shiney, surnom  FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND lvl>0 ORDER BY lvl DESC ') or die(print_r($bdd->errorInfo()));}
			if($tri_points==1){$reponse3 = $bdd->prepare('SELECT id_pokemon, lvl, score, victoires, defaites, pa_restant, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND lvl>0 ORDER BY score DESC ') or die(print_r($bdd->errorInfo()));}
			if($tri_points==2){$reponse3 = $bdd->prepare('SELECT id_pokemon, lvl, score, victoires, defaites, pa_restant, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND lvl>0 ORDER BY victoires DESC ') or die(print_r($bdd->errorInfo()));}
			if($tri_points==3){$reponse3 = $bdd->prepare('SELECT id_pokemon, lvl, score, victoires, defaites, pa_restant, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND lvl>0 ORDER BY surnom ASC ') or die(print_r($bdd->errorInfo()));}
			if($tri_points==4){$reponse3 = $bdd->prepare('SELECT id_pokemon, lvl, score, victoires, defaites, pa_restant, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND lvl>0 ORDER BY lvl DESC,surnom ASC ') or die(print_r($bdd->errorInfo()));}
			if($tri_points==5){$reponse3 = $bdd->prepare('SELECT id_pokemon, lvl, score, victoires, defaites, pa_restant, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND lvl>0 ORDER BY pa_restant DESC ') or die(print_r($bdd->errorInfo()));}
			if($tri_points==6){$reponse3 = $bdd->prepare('SELECT id_pokemon, lvl, score, victoires, defaites, pa_restant, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND lvl>0 ORDER BY id_pokemon ASC ') or die(print_r($bdd->errorInfo()));}
			$reponse3->execute(array('pseudo' => $_SESSION['pseudo']));  
			while($donnees3 = $reponse3->fetch())
				{
				$reponse4 = $bdd->prepare('SELECT type1, type2, id FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
				$reponse4->execute(array('id' => $donnees3['id_pokemon']));  
				$donnees4 = $reponse4->fetch();
				$key_lvl=0;$key_shiney=0;$key_points=0;$key_sexe=0;$key_victoires=0;$key_pokemon=0;$key_pa=0;
				if($donnees['lvl']!=0){if($donnees['condition_lvl']=="<"){if($donnees3['lvl']<$donnees['lvl']){$key_lvl=1;}}if($donnees['condition_lvl']=="="){if($donnees3['lvl']==$donnees['lvl']){$key_lvl=1;}}if($donnees['condition_lvl']==">"){if($donnees3['lvl']>$donnees['lvl']){$key_lvl=1;}}}else{$key_lvl=1;}if($donnees3['lvl']==0){$key_lvl=0;}
				if($classe=="D" AND $donnees3['lvl']>25){$key_lvl=0;}if($classe=="C" AND $donnees3['lvl']>50){$key_lvl=0;}if($classe=="B" AND $donnees3['lvl']>80){$key_lvl=0;}
				if($donnees['shiney']==1){if($donnees3['shiney']==1){$key_shiney=1;}}elseif($donnees['shiney']==2){if($donnees3['shiney']==0){$key_shiney=1;}}else{$key_shiney=1;}
				if($donnees['points']!=0){if($donnees['condition_points']=="<"){if($donnees3['score']<$donnees['points']){$key_points=1;}}if($donnees['condition_points']==">"){if($donnees3['score']>$donnees['points']){$key_points=1;}}}else{$key_points=1;} 
				if($donnees['sexe']=="M" AND $donnees3['sexe']!="F"){$key_sexe=1;}if($donnees['sexe']=="F" AND $donnees3['sexe']!="M"){$key_sexe=1;}if($donnees['sexe']==""){$key_sexe=1;}
				if($donnees['victoires']!=0){if($donnees['condition_victoires']=="<"){if($donnees3['victoires']<$donnees['victoires']){$key_victoires=1;}}if($donnees['condition_victoires']==">"){if($donnees3['victoires']>$donnees['victoires']){$key_victoires=1;}}}else{$key_victoires=1;} 	
				if($donnees['type_pokemon']!=""){if($donnees4['type1']==$donnees['type_pokemon'] OR $donnees4['type2']==$donnees['type_pokemon']){$key_pokemon=1;}}
				elseif($donnees['race']!=0){if($donnees['race']==$donnees4['id']){$key_pokemon=1;}}
				else{$key_pokemon=1;}
				if($donnees3['pa_restant']>=2){$key_pa=1;}
				if($key_lvl==1 AND $key_shiney==1 AND $key_points==1 AND $key_sexe==1 AND $key_victoires==1 AND $key_pokemon==1 AND $key_pa==1)
					{
					if($count==0){echo '<select name="id_liste_pokemons">';}
					$count=$count+1;
					echo '<option value="'.$donnees3['id'].'"';if($donnees3['shiney']==1){echo 'style="background-color:lightblue;"';}echo '>'.$donnees3['surnom'].' lvl '.$donnees3['lvl'].'</option>';
					}
				}
			if($count==0){echo 'Aucun pokémon correspondant';echo '<input type="hidden" name="permission" value="refus"/>';}
			else
				{
				echo '</select>';
				?>
				<input type="hidden" name="action" value="inscription"/>
				<input type="hidden" name="id_match" value="<?php echo $id_match; ?>"/>
				<input type="hidden" name="classe" value="<?php echo $classe; ?>"/>
				<input type="submit" value="Inscription"/>	
				<?php 
				}
			?>		
			</form>
		<?php
			}
		else
			{
			echo 'Déjà inscrit';
			}
		}
	else
		{
		echo 'Or insuffisant';
		}
	echo '</td></tr>';
	}
?>
</table>

<?php
}
else
{
if($_SESSION['page_combat']=="NULL"){echo 'Vous n\'avez pas encore accès à cette page.';}
else{echo 'Vous devez d\'abord terminer votre combat avant d\'accéder au Blind Battles. Si vous désirez toutefois y accéder, vous pouvez suivre <a href="carte.php?action=abandon">ce lien</a>. Votre combat sera alors considéré comme abandonné.<br />';
if ($_SESSION['page_combat']=="combat"){echo 'Vous pouvez retourner à votre combat via <a href="combat.php">ce lien</a>.<br />';}}
if ($_SESSION['page_combat']=="quete_combat"){echo 'Vous pouvez retourner à votre combat via <a href="quetes_combat.php">ce lien</a>.<br />';}

}
}
else
{
echo 'Vous devez être connecté pour accéder à cette page';
}
?>
<?php include ("bas.php"); ?>
