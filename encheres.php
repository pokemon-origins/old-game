<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">

<?php
if(isset($_SESSION['pseudo']))
{
$nb_pokemons_a_vendre=0;
$deja_enchere=0;
$reponse = $bdd->query('SELECT * FROM pokemons_enchere_pokemons ORDER BY prix') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
    {
	$nb_pokemons_a_vendre=$nb_pokemons_a_vendre+1;
	}
?>

<h2>Ventes aux enchères</h2>

Chaque joueur peut mettre jusqu'à deux pokémon aux enchères à la fois.<br />
Lorsque vous enchérissez sur un pokémon, l'argent est directement débité. Si quelqu'un surenchéri sur votre offre, il vous est rendu. <br />
Lorsque le délai est à 0, l'enchère se termine. A chaque fois qu'un joueur enchéri, le délai est rétabli à une durée fixée par le vendeur. <br />
<i>Une enchère ayant un délai de 2 heures ne finira donc pas forcément dans deux heures. Si un joueur enchéri, le délai sera réaugmenté. </i><br />


<?php // Validation des enchères terminées
$reponse = $bdd->query('SELECT * FROM pokemons_enchere_pokemons') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	$time=time();
	if($donnees['active']==1)
		{
		$quand_stop=$donnees['quand']+$donnees['delai'];
		if($time>$quand_stop)
			{
			$pv_max_pokemon=$donnees['pv_max'];$att_pokemon=$donnees['att'];$def_pokemon=$donnees['def'];$vit_pokemon=$donnees['vit'];$attspe_pokemon=$donnees['attspe'];$defspe_pokemon=$donnees['defspe'];
			$id_pokemon=$donnees['id_pokemon'];
			$pseudo_vendeur=$donnees['pseudo'];
			$pas=$donnees['pas'];
			$prix=$donnees['prix'];
			$gagnant=$donnees['gagnant'];
			$reponse3 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse3->execute(array('id' => $donnees['id_pokemon']));
			$donnees3 = $reponse3->fetch();
			$nom_du_pokemon_vendu=$donnees3['nom'];
			if($donnees['lvl']==0){$nom_du_pokemon_vendu= "oeuf";}
			$req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons (pseudo, id_pokemon, shiney, sexe, lvl, xp, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, actif, pa_restant, pa_max, pa_bonus, victoires, defaites, score, fin_dodo, parent_1, parent_2, bonheur, objet) VALUES(:pseudo, :id_pokemon, :shiney, :sexe, :lvl, :xp, :pv, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, 0, :pa_restant, :pa_max, :pa_bonus, :victoires, :defaites, :score, :fin_dodo, :parent_1, :parent_2, :bonheur, :objet)') or die(print_r($bdd->errorInfo()));
							$req->execute(array(
									'pseudo' => $donnees['gagnant'], 
									'id_pokemon' => $id_pokemon,
									'shiney' => $donnees['shiney'],
									'sexe' => $donnees['sexe'],
									'lvl' => $donnees['lvl'],
									'xp' => $donnees['xp'],
									'pv' => $pv_max_pokemon,
									'pv_max' => $pv_max_pokemon,
									'att' => $att_pokemon,
									'def' => $def_pokemon,
									'vit' => $vit_pokemon,
									'attspe' => $attspe_pokemon,
									'defspe' => $defspe_pokemon,
									'attaque1' => $donnees['attaque1'],
									'attaque2' => $donnees['attaque2'],
									'attaque3' => $donnees['attaque3'],
									'attaque4' => $donnees['attaque4'],
									'pa_restant' => $donnees['pa_restant'],
									'pa_max' => $donnees['pa_max'],
									'pa_bonus' => $donnees['pa_bonus'],
									'victoires' => $donnees['victoires'],
									'defaites' => $donnees['defaites'],
									'score' => $donnees['score'],
									'fin_dodo' => $donnees['fin_dodo'],
									'parent_1' => $donnees['parent_1'],
									'parent_2' => $donnees['parent_2'],
									'bonheur' => $donnees['bonheur'],
									'objet' => $donnees['objet']
									))or die(print_r($bdd->errorInfo()));
			$reponse2 = $bdd->prepare('DELETE FROM pokemons_enchere_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse2->execute(array('id' => $donnees['id'])); 
			$reponse2 = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse2->execute(array('pseudo' => $donnees['pseudo']));
				$donnees2 = $reponse2->fetch();
			$ors_vendeur=$donnees2['ors'];	
			$argent_vendeur=$ors_vendeur+$prix;
			$reponse2 = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
							$reponse2->execute(array('ors' =>$argent_vendeur ,'pseudo' => $donnees['pseudo'])); 	
			$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("marchand", :destinataire, "non lu", "Vente aux enchères réussie!", :message, now())') or die(print_r($bdd->errorInfo()));
			$req->execute(array(
				'destinataire' => $donnees['pseudo'],					
				'message' => 'félicitation, votre pokémon "'.$nom_du_pokemon_vendu.'" a été vendu au prix de '.$prix.' pépites à '.$gagnant.' en vente aux enchères.'
				))
				or die(print_r($bdd->errorInfo()));	
			$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("marchand", :destinataire, "non lu", "Vente aux enchères réussie!", :message, now())') or die(print_r($bdd->errorInfo()));
			$req->execute(array(
				'destinataire' => $gagnant,					
				'message' => 'félicitation, vous avez remporté le "'.$nom_du_pokemon_vendu.'" au prix de '.$prix.' pépites en vente aux enchères.'
				))
				or die(print_r($bdd->errorInfo()));					
			}
		}
	elseif($donnees['active']==0)
		{
		$quand_stop=$donnees['quand']+604800;
		if($time>$quand_stop)
			{
			$pv_max_pokemon=$donnees['pv_max'];$att_pokemon=$donnees['att'];$def_pokemon=$donnees['def'];$vit_pokemon=$donnees['vit'];$attspe_pokemon=$donnees['attspe'];$defspe_pokemon=$donnees['defspe'];
			$id_pokemon=$donnees['id_pokemon'];
			$pseudo_vendeur=$donnees['pseudo'];
			$pas=$donnees['pas'];
			$prix=$donnees['prix'];
			$gagnant=$donnees['gagnant'];
			$reponse3 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse3->execute(array('id' => $donnees['id_pokemon']));
			$donnees3 = $reponse3->fetch();
			$nom_du_pokemon_vendu=$donnees3['nom'];
			if($donnees['lvl']==0){$nom_du_pokemon_vendu= "oeuf";}
			$req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons (pseudo, id_pokemon, shiney, sexe, lvl, xp, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, actif, pa_restant, pa_max, pa_bonus, victoires, defaites, score, fin_dodo, parent_1, parent_2, bonheur, objet) VALUES(:pseudo, :id_pokemon, :shiney, :sexe, :lvl, :xp, :pv, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, 0, :pa_restant, :pa_max, :pa_bonus, :victoires, :defaites, :score, :fin_dodo, :parent_1, :parent_2, :bonheur, :objet)') or die(print_r($bdd->errorInfo()));
							$req->execute(array(
									'pseudo' => $donnees['pseudo'], 
									'id_pokemon' => $id_pokemon,
									'shiney' => $donnees['shiney'],
									'sexe' => $donnees['sexe'],
									'lvl' => $donnees['lvl'],
									'xp' => $donnees['xp'],
									'pv' => $pv_max_pokemon,
									'pv_max' => $pv_max_pokemon,
									'att' => $att_pokemon,
									'def' => $def_pokemon,
									'vit' => $vit_pokemon,
									'attspe' => $attspe_pokemon,
									'defspe' => $defspe_pokemon,
									'attaque1' => $donnees['attaque1'],
									'attaque2' => $donnees['attaque2'],
									'attaque3' => $donnees['attaque3'],
									'attaque4' => $donnees['attaque4'],
									'pa_restant' => $donnees['pa_restant'],
									'pa_max' => $donnees['pa_max'],
									'pa_bonus' => $donnees['pa_bonus'],
									'victoires' => $donnees['victoires'],
									'defaites' => $donnees['defaites'],
									'score' => $donnees['score'],
									'fin_dodo' => $donnees['fin_dodo'],
									'parent_1' => $donnees['parent_1'],
									'parent_2' => $donnees['parent_2'],
									'bonheur' => $donnees['bonheur'],
									'objet' => $donnees['objet']
									))or die(print_r($bdd->errorInfo()));
			$reponse2 = $bdd->prepare('DELETE FROM pokemons_enchere_pokemons WHERE id=:id ') or die(print_r($bdd->errorInfo()));
			$reponse2->execute(array('id' => $donnees['id'])); 
			$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("marchand", :destinataire, "non lu", "Vente aux enchères ratée!", :message, now())') or die(print_r($bdd->errorInfo()));
			$req->execute(array(
				'destinataire' => $donnees['pseudo'],					
				'message' => 'Désolé, votre pokémon "'.$nom_du_pokemon_vendu.'" n\'a pas trouvé d\'enchérisseur en vente aux enchères. Il revient dans votre liste de pokémon'
				))
				or die(print_r($bdd->errorInfo()));				
			}
		}
	}
include ("include/maj_pokedex.php");
?>


<?php //faire une offre
if(isset($_POST['marchandises']) AND $_POST['marchandises']=="pokemons")
{
if(isset($_POST['action']) AND $_POST['action']=="acheter")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
		$donnees = $reponse->fetch();
	$ors=$donnees['ors'];
	$reponse = $bdd->prepare('SELECT * FROM pokemons_enchere_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_POST['id']));
		$donnees = $reponse->fetch();
	$pv_max_pokemon=$donnees['pv_max'];$att_pokemon=$donnees['att'];$def_pokemon=$donnees['def'];
	$vit_pokemon=$donnees['vit'];$attspe_pokemon=$donnees['attspe'];$defspe_pokemon=$donnees['defspe'];
	$id_pokemon=$donnees['id_pokemon'];$pseudo_vendeur=$donnees['pseudo'];$pas=$donnees['pas'];$delai=$donnees['delai'];
	$active=$donnees['active'];
	if($active==1){$prix_avant=$donnees['prix'];$gagnant_avant=$donnees['gagnant'];}
	if(isset($donnees['id']))
		{
		if($ors>=$_POST['prix'] AND $_POST['prix']>=$donnees['prix']+$donnees['pas'] AND $_SESSION['pseudo']!=$pseudo_vendeur)
			{
			$reponse3 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
				$reponse3->execute(array('id' => $donnees['id_pokemon']));
				$donnees3 = $reponse3->fetch();
			$nom_du_pokemon_vendu=$donnees3['nom'];
			if($donnees['lvl']==0){$nom_du_pokemon_vendu= "oeuf";}
			
			$prix=$_POST['prix'];
			$argent_restant=$ors-$prix;
			$reponse2 = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
							$reponse2->execute(array('ors' =>$argent_restant ,'pseudo' => $_SESSION['pseudo'])); 
			if($active==1)
				{
				$reponse4 = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse4->execute(array('pseudo' => $gagnant_avant));
				$donnees4 = $reponse4->fetch();
				$ors2=$donnees4['ors'];
				$argent2_restant=$ors2+$prix_avant;
				$reponse2 = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
							$reponse2->execute(array('ors' =>$argent2_restant ,'pseudo' => $gagnant_avant)); 
				if($gagnant_avant!=$_SESSION['pseudo'])
					{
					$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("marchand", :destinataire, "non lu", "Quelqu\'un a surenchéri", :message, now())') or die(print_r($bdd->errorInfo()));
					$req->execute(array(
					'destinataire' => $gagnant_avant,					
					'message' => 'Un joueur vient de surenchérir sur votre offre de "'.$prix_avant.'" pour le pokémon "'.$nom_du_pokemon_vendu.'", désolé.'
					))
					or die(print_r($bdd->errorInfo()));		
					}
				}
			else
				{
				$reponse2 = $bdd->prepare('UPDATE pokemons_enchere_pokemons SET active=1 WHERE id=:id') or die(print_r($bdd->errorInfo()));
							$reponse2->execute(array('id' =>$_POST['id'])); 
				}
			$reponse2 = $bdd->prepare('UPDATE pokemons_enchere_pokemons SET prix=:prix, gagnant=:gagnant, quand=:quand WHERE id=:id') or die(print_r($bdd->errorInfo()));
							$reponse2->execute(array('prix'=> $prix, 'gagnant'=>$_SESSION['pseudo'], 'quand'=>$time, 'id' =>$_POST['id'])); 
			$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("marchand", :destinataire, "non lu", "Votre enchère monte", :message, now())') or die(print_r($bdd->errorInfo()));
			$req->execute(array(
				'destinataire' => $donnees['pseudo'],					
				'message' => 'L\'enchère sur votre pokémon "'.$nom_du_pokemon_vendu.'" a augmenté à '.$prix.' pépites. C\'est actuellement '.$_SESSION['pseudo'].' le meilleur enchérisseur.'
				))
				or die(print_r($bdd->errorInfo()));					
			echo '<b>Vous avez bien enchéri pour le pokémon.</b><br /><br />';
			}
		else
			{
			if($_SESSION['pseudo']==$pseudo_vendeur)
				{echo '<b>Vous ne pouvez pas vous acheter un pokémon à vous-même!</b><br /><br />';}
			else
				{echo '<b>Vous n\'avez pas suffisamment de pépites pour enchérir sur ce pokémon. </b><br /><br />';}
			}
			
		}
	else{echo '<b>Ce pokémon n\'est plus à vendre, vous avez été trop lent. Désolé. </b><br /><br />';}
	}
if($_POST['action']=="vendre")
	{
	if (preg_match("#^[0-9]+$#", $_POST['prix']))
		{
		if($nb_pokemons_joueur>1)
			{
			$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id=:id AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $_POST['id_pokemon'], 'pseudo' => $_SESSION['pseudo']));
			$donnees = $reponse->fetch();
			if(isset($donnees['id']))
				{
				$req = $bdd->prepare('INSERT INTO pokemons_enchere_pokemons (pseudo, id_pokemon, shiney, sexe, lvl, xp, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, pa_restant, pa_max, pa_bonus, victoires, defaites, score, prix, fin_dodo, pas, delai, parent_1, parent_2, bonheur, objet, quand) VALUES(:pseudo, :id_pokemon, :shiney, :sexe, :lvl, :xp, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, :pa_restant, :pa_max, :pa_bonus, :victoires, :defaites, :score, :prix, :fin_dodo, :pas, :delai, :parent_1, :parent_2, :bonheur, :objet, :quand)') or die(print_r($bdd->errorInfo()));
							$req->execute(array(
									'pseudo' => $donnees['pseudo'], 
									'id_pokemon' => $donnees['id_pokemon'],
									'shiney' => $donnees['shiney'],
									'sexe' => $donnees['sexe'],
									'lvl' => $donnees['lvl'],
									'xp' => $donnees['xp'],
									'pv_max' => $donnees['pv_max'],
									'att' => $donnees['att'],
									'def' => $donnees['def'],
									'vit' => $donnees['vit'],
									'attspe' => $donnees['attspe'],
									'defspe' => $donnees['defspe'],
									'attaque1' => $donnees['attaque1'],
									'attaque2' => $donnees['attaque2'],
									'attaque3' => $donnees['attaque3'],
									'attaque4' => $donnees['attaque4'],
									'pa_restant' => $donnees['pa_restant'],
									'pa_max' => $donnees['pa_max'],
									'pa_bonus' => $donnees['pa_bonus'],
									'victoires' => $donnees['victoires'],
									'defaites' => $donnees['defaites'],
									'score' => $donnees['score'],
									'prix' => $_POST['prix'],
									'fin_dodo' => $donnees['fin_dodo'],
									'pas' => $_POST['pas'],
									'delai' => $_POST['delai'],
									'parent_1' => $donnees['parent_1'],
									'parent_2' => $donnees['parent_2'],
									'bonheur' => $donnees['bonheur'],
									'objet' => $donnees['objet'],
									'quand' => $time
									))or die(print_r($bdd->errorInfo()));
				$reponse = $bdd->prepare('DELETE FROM pokemons_liste_pokemons WHERE id=:id ') or die(print_r($bdd->errorInfo()));
				  $reponse->execute(array('id' => $_POST['id_pokemon'])); 
				echo '<b>Votre pokémon a bien été mis en vente! </b><br /><br />';
				}
			else{echo '<b>Il est interdit de vendre un pokémon que l\'on ne possède pas.</b><br /><br />';}
			}
		else {echo '<b>Vous ne pouvez pas vendre votre dernier pokémon. </b><br /><br />';	}
		}
	else{echo '<b> Vous devez indiquer un prix en valeur numérique non nulle pour vendre ! </b><br /><br />';}
	}
}
?>

<br />
<b>Pokémons mis en vente aux enchères :</b>
<div <?php if($nb_pokemons_a_vendre>5 OR $_SESSION['pseudo']=="Dylansart") {echo 'id="marche_pokemons"';} else{echo 'width="550px"';} ?>style="text-align:center;">
<table id="marche" width="535px" cellpadding="2" cellspacing="2">
<colgroup><COL WIDTH=18%><COL WIDTH=17%><COL WIDTH=18%><COL WIDTH=10%><COL WIDTH=15%><COL WIDTH=22%></COLGROUP>
<tr> <th> Pokémons</th> <th> Nom </th> <th> Propriétaire </th> <th> Lvl </th> <th> Prix </th> <th>Acheter</th> </tr>
<?php //affichage des pokémons à vendre
$reponse = $bdd->query('SELECT * FROM pokemons_enchere_pokemons ORDER BY prix') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
    {
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['id_pokemon']));
		$donnees2 = $reponse2->fetch();
	$id_pokedex=$donnees2['id_pokedex'];
	$nom_pokemon=$donnees2['nom'];
	$prix_achat_min=$donnees['prix']+$donnees['pas'];
	?>
	<tr><td><a style="color:black;" href="encheres_pokemons.php?id=<?php echo $donnees['id']; ?>" target="_blank"><?php if($donnees['lvl']!=0){?><img src="images/pokemons/<?php if($donnees['shiney']==1){echo 'shiney/';} echo $id_pokedex; ?>.gif" height="60px" style="border-style:none;"/><?php }else{?><img src="images/oeuf.gif" height="60px" style="border-style:none;"/><?php } ?></a></td>
	<td><a style="color:black;" href="encheres_pokemons.php?id=<?php echo $donnees['id']; ?>" target="_blank"><?php if($donnees['lvl']!=0){echo $nom_pokemon;} else{echo 'Oeuf';} ?></a></td> <td><?php echo $donnees['pseudo']; ?></td> <td><?php if($donnees['lvl']!=0){echo $donnees['lvl'];} ?></td> <td><?php echo $donnees['prix']; echo '<img src="images/or.png" />';?></td>
	<td>
	<?php if($donnees['pseudo']==$_SESSION['pseudo']){echo '<span style="color:blue;">Vous êtes le vendeur </span><br />';if($donnees['active']==1){echo '<span style="color:green;">Il y a un acheteur</span><br />';}else{echo '<span style="color:green;">Il n\'y a pas d\'acheteur</span><br />';}}
	else{?>
	<form action="encheres.php" method="post">                     	         
	<input type="hidden" name="id"  value="<?php echo $donnees['id']; ?>" />	 
	<input type="hidden" name="action"  value="acheter" /> 
	<input type="hidden" name="marchandises" value="pokemons"/>	
	<input type="text" name="prix" size="4" value="<?php echo $prix_achat_min;?>" />
	<input type="submit" value="Enchérir" /> 
	</form>
	<?php if($donnees['gagnant']==$_SESSION['pseudo']){echo '<span style="color:green;">Actuellement vous gagnez </span><br />';}?>
	<?php } ?>
	<?php 
	if($donnees['active']==1){$temps_restant=$donnees['quand']+$donnees['delai']-$time;}else{$temps_restant=$donnees['quand']+604800-$time;}$jours=floor($temps_restant/86400);$reste=$temps_restant%86400;$heures=floor($reste/3600); $reste=$reste%3600;$minutes=floor($reste/60);$secondes=$reste%60; 
	echo '<span style="color:red;">Délai :'.$jours.'J '.$heures.'H '.$minutes.'M '.$secondes.'S</span>';
	?>
	</td></tr>
	<?php
	}
?>
</table>	
</div>
<br /><br />


<?php
$reponse = $bdd->query('SELECT * FROM pokemons_enchere_pokemons ORDER BY prix') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
    {
	$nb_pokemons_a_vendre=$nb_pokemons_a_vendre+1;
	if($donnees['pseudo']==$_SESSION['pseudo']){$deja_enchere=$deja_enchere+1;}
	}
if($deja_enchere<=1 OR $_SESSION['pseudo']=="admin") //Formulaire de mise aux enchères
{
?>

<b>Quel pokémon désirez-vous mettre en vente aux enchères?</b>
<div <?php if($nb_pokemons_joueur>=15){echo 'id="marche_pokemons"';}else{echo 'width="550px"';} ?>>
<form action="encheres.php" method="post">
<table id="table_pokemons_inactifs" width="535px" cellpadding="2" cellspacing="2"><colgroup><COL WIDTH=46%><COL WIDTH=27%><COL WIDTH=27%></COLGROUP>
<?php //pokemon non actif
if($tri_points==0){$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY lvl DESC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==1){$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY score DESC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==2){$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY victoires DESC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==3){$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY surnom ASC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==4){$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY lvl DESC,surnom ASC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==5){$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY pa_restant DESC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==6){$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 AND lvl>0 ORDER BY id_pokemon ASC ') or die(print_r($bdd->errorInfo()));}		
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
while($donnees = $reponse->fetch())
{
$id_pokemon_inactif=$donnees['id_pokemon'];
$lvl_pokemon_inactif=$donnees['lvl'];
$pa_restant_pokemon_inactif=$donnees['pa_restant'];
$pa_max_pokemon_inactif=$donnees['pa_max'];
$id_liste_pokemon=$donnees['id'];
$shiney_pokemon=$donnees['shiney'];
$surnom_pokemon_inactif=$donnees['surnom'];
$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse2->execute(array('id' => $id_pokemon_inactif));
$donnees2 = $reponse2->fetch();
$nom_pokemon_inactif=$donnees2['nom'];

echo '<tr '; if($shiney_pokemon==1 AND $lvl_pokemon_inactif!=0){echo 'style="background-color:lightblue;"';} echo '><td>'; if($lvl_pokemon_inactif!=0){echo $surnom_pokemon_inactif;} else{echo 'Oeuf';} echo '</td><td>'; if($lvl_pokemon_inactif!=0){echo 'lvl '.$lvl_pokemon_inactif;} echo '</td><td><img src="images/coeur.gif" />'.$pa_restant_pokemon_inactif.'/'.$pa_max_pokemon_inactif.'</td><td><input type="radio" name="id_pokemon" value="'.$donnees['id'].'"></td></tr>';
}
?>	
</table>
</div><br />
<b>Prix de mise en vente de base: </b><input type="text" name="prix" size="4"/><img src="images/or.png" /><br />
<abbr title="Il s'agit de la mise supplémentaire minimum, autrement dit le minimum d'ajout qu'un joueur doit apporter par rapport à la mise en cours pour prendre la tête"><b>Pas d'enchère</b></abbr> : <br />
<input type="radio" name="pas" value="10" checked> 10 <img src="images/or.png" /><br />
<input type="radio" name="pas" value="25"> 25 <img src="images/or.png" /><br />
<input type="radio" name="pas" value="50"> 50 <img src="images/or.png" /><br />
<input type="radio" name="pas" value="100"> 100 <img src="images/or.png" /><br />
<input type="radio" name="pas" value="250"> 250 <img src="images/or.png" /><br />
<abbr title="Il s'agit du temps durant laquel il ne doit pas y avoir de nouvelle offre pour que le meilleur enchérisseur soit défini vainqueur"><b>Délai avant victoire</b></abbr> : <br />
<input type="radio" name="delai" value="18000" checked> 5 heures (très rapide) <br />
<input type="radio" name="delai" value="43200"> 12 heures (rapide) <br />
<input type="radio" name="delai" value="86400"> 24 heures (moyen) <br />
<input type="radio" name="delai" value="172800"> 2 jours (lent)<br />
<input type="radio" name="delai" value="432000"> 5 jours (très lent) <br />
<input type="hidden" name="action" value="vendre"/>
<input type="hidden" name="marchandises" value="pokemons"/>
<input type="submit" value="Mettre en vente"/>
</form>


<?php
}
else //si déjà une enchère en cours
{
echo '<b>Vous ne pouvez pas avoir plus de deux enchères en cours. Attendez que celles-ci soient finies avant d\'en lancer une suivante.</b><br />';
}
?>



<?php
}
else
{
echo 'Vous devez être connecté pour accéder à cette page';
}
?>

<?php include ("bas.php"); ?>