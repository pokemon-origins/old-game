<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>

 
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
    
   
    <h2>Pokémons en liberté</h2>
    
<table width="400px">
<tr><th>Pokémon  </th><th>Normal  </th><th>Shiney  </th></tr>           
<?php
$total=0;
$reponse = $bdd->query('SELECT id, nom, id_pokedex FROM pokemons_base_pokemons ORDER BY id_pokedex ASC') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch()){
    $reponse2 = $bdd->prepare('SELECT COUNT(id) AS total FROM pokemons_map WHERE id_pokemon=:id_pokemon AND shiney=0 AND monstre=0') or die(print_r($bdd->errorInfo()));
    $reponse2->execute(array('id_pokemon' => $donnees['id']));
    $donnees2 = $reponse2->fetch();
    $normal=$donnees2['total'];
    $total=$total+$donnees2['total'];
    $reponse2 = $bdd->prepare('SELECT COUNT(id) AS total FROM pokemons_map WHERE id_pokemon=:id_pokemon AND shiney=1 AND monstre=0') or die(print_r($bdd->errorInfo()));
    $reponse2->execute(array('id_pokemon' => $donnees['id']));
    $donnees2 = $reponse2->fetch();
    $shiney=$donnees2['total'];
    $total=$total+$donnees2['total'];
    echo '<tr><td>'.$donnees['nom'].'</td><td>'.$normal.'</td><td>'.$shiney.'</td></tr>';
}     
?>
</table>
<br /><br />
Nombre de pokémons en liberté actuellement :<?php echo $total;?>

<?php include ("bas.php"); ?>
