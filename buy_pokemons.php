<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">

<div width="550px" style="text-align:center;color:#6495ed;margin-top:-15px;"><h1>Marché de la Team Rocket</h1></div>

<img src="images/TM.png" style="float:left;padding:20px;margin-left:-20px;" width="200px" />


<?php
if(isset($_POST['id_pokedex']) AND isset($_POST['action']) AND $_POST['id_pokedex']>0 AND $_POST['action']=="valider"){
    $reponse = $bdd->prepare('SELECT id, id_pokedex, nom, legendaire, tdc FROM pokemons_base_pokemons WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));  
    $reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));
    $donnees = $reponse->fetch(); 
    if(isset($donnees['id'])){
        $reponse = $bdd->prepare('SELECT id, id_pokedex, nom, legendaire, tdc FROM pokemons_base_pokemons WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));  
        $reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));
        $donnees = $reponse->fetch();    
        //calul nb joueurs
        $reponse = $bdd->query('SELECT COUNT(id) AS comptage FROM pokemons_membres') or die(print_r($bdd->errorInfo()));
        $donnees2 = $reponse->fetch();
        $nb_joueurs=$donnees2['comptage'];
        //calul nb pokemon
        if($_POST['shiney']==1){
            $reponse = $bdd->prepare('SELECT COUNT(id) AS comptage FROM pokemons_liste_pokemons WHERE id_pokemon=:id_pokemon AND shiney=1') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('id_pokemon' => $donnees['id']));
            $donnees2 = $reponse->fetch();
            $nb_pok=$donnees2['comptage'];
            $reponse = $bdd->prepare('SELECT COUNT(id) AS comptage FROM pokemons_buy_pokemons WHERE id_pokedex=:id_pokedex AND shiney=1') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));
            $donnees2 = $reponse->fetch();
            $nb_pok_buy=$donnees2['comptage'];
        }else{
            $reponse = $bdd->prepare('SELECT COUNT(id) AS comptage FROM pokemons_liste_pokemons WHERE id_pokemon=:id_pokemon') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('id_pokemon' => $donnees['id'])); 
            $donnees2 = $reponse->fetch();
            $nb_pok=$donnees2['comptage'];
            $reponse = $bdd->prepare('SELECT COUNT(id) AS comptage FROM pokemons_buy_pokemons WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));
            $donnees2 = $reponse->fetch();
            $nb_pok_buy=$donnees2['comptage'];
        }
        $reponse = $bdd->prepare('SELECT COUNT(id) AS comptage FROM pokemons_buy_pokemons') or die(print_r($bdd->errorInfo()));
        $reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));
        $donnees2 = $reponse->fetch();
        $nb_pok_buy_tot=$donnees2['comptage'];    
        
        //Calcul du prix
        $multi1=sqrt(sqrt(255/$donnees['tdc']));
        $multi2=1+3*$donnees['legendaire'];
        $multi3=$nb_joueurs/($nb_pok+5);
        $multi4=40*($nb_pok_buy/$nb_pok_buy_tot)+1;
        $prix=$multi1*$multi2*$multi3*$multi4;
        $prix=ceil($prix);
        $prix_max=ceil((5000/(sqrt($donnees['tdc']))*(($_POST['shiney']*2)+1)*(($donnees['legendaire']/2)+1)));
        //prix max
        if($prix>$prix_max){$prix=$prix_max;}
        echo $prix_max;
        //prix minimum
        if($prix<50){$prix=50;}
        if($_POST['shiney']==1 AND $prix<300){$prix=300;}
        if($donnees['legendaire']==1 AND $prix<500){$prix=500;}
        
        if($prix==$_POST['prix']){
            $reponse = $bdd->prepare('SELECT ors FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('pseudo' => $_SESSION['pseudo']));
            $donnees = $reponse->fetch();
            $or=$donnees['ors'];
            if($prix<=$or){
                if($_POST['id_pokedex']<=386){
                    //VENTE OK
                    //Diminuer l'or
                    $or_now=$or-$prix;
                    $reponse = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
                    $reponse->execute(array('ors' =>$or_now , 'pseudo' => $_SESSION['pseudo'])) 	or die(print_r($bdd->errorInfo()));
                    $lvl=5;
                    //Ajouter le pokémon			
                    $reponse5 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));
                    $reponse5->execute(array('id_pokedex' => $_POST['id_pokedex']));
                    $donnees5 = $reponse5->fetch();
                    $id_race=$donnees5['id'];
                    if($donnees5['sexe']=="Y"){$proba_sexe=rand(1,2); if($proba_sexe==1){$sexe="M";} elseif($proba_sexe==2){$sexe="F";}}
                    if($donnees5['sexe']=="M"){$sexe="M";}
                    if($donnees5['sexe']=="F"){$sexe="F";}
                    if($donnees5['sexe']=="N"){$sexe="";}
                    $randpv=rand(0,4);$randatt=rand(0,4);$randdef=rand(0,4);$randvit=rand(0,4);$randattspe=rand(0,4);$randdefspe=rand(0,4);
                    $pv=$donnees5['pv']+4*$donnees5['bonus_pv']+$randpv;
                    $att=$donnees5['att']+4*$donnees5['bonus_att']+$randatt;
                    $def=$donnees5['def']+4*$donnees5['bonus_def']+$randdef;
                    $vit=$donnees5['vit']+4*$donnees5['bonus_vit']+$randvit;
                    $attspe=$donnees5['attspe']+4*$donnees5['bonus_attspe']+$randattspe;
                    $defspe=$donnees5['defspe']+4*$donnees5['bonus_defspe']+$randdefspe;
                    
                    $attaque1=0;$attaque2=0;$attaque3=0;$attaque4=0;$count1=0;$count2=0;$count3=0;$count4=0;
                    while($attaque1==0 AND $count1<4)
                            {
                            $count_attaque=0;
                            $reponse = $bdd->prepare('SELECT id FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                            $reponse->execute(array('id_pokedex' => $_POST['id_pokedex'], 'lvl' => $lvl));
                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;}
                            $rand_attaque=rand(0,$count_attaque);
                            $count_attaque=0;
                            $reponse = $bdd->prepare('SELECT id_attaque FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                            $reponse->execute(array('id_pokedex' => $_POST['id_pokedex'], 'lvl' => $lvl));
                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;if($count_attaque==$rand_attaque){$attaque1=$donnees['id_attaque'];}}
                            $count1=$count1+1;
                            }
                    while($attaque2==0 AND $count2<5 OR $count2<5 AND $attaque2==$attaque1)
                            {
                            $count_attaque=0;
                            $reponse = $bdd->prepare('SELECT id FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                            $reponse->execute(array('id_pokedex' => $_POST['id_pokedex'], 'lvl' => $lvl));
                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;}
                            $rand_attaque=rand(0,$count_attaque);
                            $count_attaque=0;
                            $reponse = $bdd->prepare('SELECT id_attaque FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                            $reponse->execute(array('id_pokedex' => $_POST['id_pokedex'], 'lvl' => $lvl));
                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;if($count_attaque==$rand_attaque){$attaque2=$donnees['id_attaque'];}}
                            $count2=$count2+1;
                            }
                    if($attaque1==$attaque2){$attaque2=0;}
                    while($attaque3==0 AND $count3<7 OR $attaque3==$attaque1 AND $count3<7 OR $attaque3==$attaque2 AND $count3<7)
                            {
                            $count_attaque=0;
                            $reponse = $bdd->prepare('SELECT id FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                            $reponse->execute(array('id_pokedex' => $_POST['id_pokedex'], 'lvl' => $lvl));
                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;}
                            $rand_attaque=rand(0,$count_attaque);
                            $count_attaque=0;
                            $reponse = $bdd->prepare('SELECT id_attaque FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                            $reponse->execute(array('id_pokedex' => $_POST['id_pokedex'], 'lvl' => $lvl));
                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;if($count_attaque==$rand_attaque){$attaque3=$donnees['id_attaque'];}}
                            $count3=$count3+1;
                            }
                    if($attaque3==$attaque2 OR $attaque3==$attaque1){$attaque3=0;}
                    while($attaque4==0 AND $count4<8 OR $attaque4==$attaque1 AND $count4<8 OR $attaque4==$attaque2 AND $count4<8 OR $attaque4==$attaque3 AND $count4<8)
                            {
                            $count_attaque=0;
                            $reponse = $bdd->prepare('SELECT id FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                            $reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;}
                            $rand_attaque=rand(0,$count_attaque);
                            $count_attaque=0;
                            $reponse = $bdd->prepare('SELECT id_attaque FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                            $reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;if($count_attaque==$rand_attaque){$attaque4=$donnees['id_attaque'];}}
                            $count4=$count4+1;
                            }
                    if($attaque4==$attaque2 OR $attaque4==$attaque1 OR $attaque4==$attaque3){$attaque4=0;}
                    
                    
                    
                    
                    $req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons (pseudo, id_pokemon, shiney, sexe, lvl, xp, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, actif, pa_restant, pa_max, pa_bonus, fin_dodo, bonheur) VALUES(:pseudo, :id_pokemon, :shiney, :sexe, 5, 100, :pv,:pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, 0, 0, 10, 10, 5, 0, 70)') or die(print_r($bdd->errorInfo()));
                    $req->execute(array(
                    'pseudo' => $_SESSION['pseudo'], 
                    'id_pokemon' => $id_race,
                    'shiney' => $_POST['shiney'],
                    'sexe' => $sexe,
                    'pv' => $pv,
                    'pv_max' => $pv,
                    'att' => $att,
                    'def' => $def,
                    'vit' => $vit,
                    'attspe' => $attspe,
                    'defspe' => $defspe,
                    'attaque1' => $attaque1,
                    'attaque2' => $attaque2,
                    'attaque3' => $attaque3	
                    ))or die(print_r($bdd->errorInfo()));
                    $req = $bdd->prepare('INSERT INTO pokemons_buy_pokemons (pseudo, id_pokedex, shiney, nom, prix, quand) VALUES(:pseudo, :id_pokedex, :shiney, :nom, :prix, now())') or die(print_r($bdd->errorInfo()));
                    $req->execute(array( 
                    'pseudo' => $_SESSION['pseudo'], 
                    'id_pokedex' => $_POST['id_pokedex'],
                    'shiney' => $_POST['shiney'],
                    'nom' => $donnees5['nom'],
                    'prix' => $_POST['prix']
                    ))or die(print_r($bdd->errorInfo()));
                    echo '<span style="color:red;"><b>Et voilà mon gars, autre chose ?</b></span>';
                }else{
                    echo '<span style="color:red;"><b>Hoho, on est un connaisseur, hein ? Désolé, je ne vends pas encore ces pokémons. Reviens plus tard</b></span>';
                }
            }else{
                echo '<span style="color:red;"><b>N\'essaye pas de m\'entuber, je vois bien que tu n\'as pas un rond !</b></span>';
            }       
        }else{
            echo '<span style="color:red;"><b>Désolé mon gars, ce pokémon n\'est plus disponible à ce prix-là. Tu as été trop lent !</b></span>';
        }    
    }
    echo '<br /><br /><div style="clear:both;"></div><a href="buy_pokemons.php">Retour au choix de pokémon</a>';
}else{
    ?>


    <p>
    Vous cherchez un pokémon en particulier ? Pas de soucis, on est là pour ça !<br />
    Mais attention, plus le pokémon sera rare sur le jeu, plus il nous sera difficile de le vol... d'en trouver un...<br />
    </p>
    
    
    <div style="clear:both;"></div>
    <b>Quel pokémon désireriez-vous ?</b>
    


    <form method="post" action="buy_pokemons.php">
    <select name="id_pokedex">
    <?php
    $reponse = $bdd->query('SELECT id, id_pokedex, nom FROM pokemons_base_pokemons WHERE id_pokedex <= 386 ORDER BY id_pokedex') or die(print_r($bdd->errorInfo()));  
    while($donnees = $reponse->fetch()){
        echo '<option value="'.$donnees['id_pokedex'].'" ';if(isset($_POST['id_pokedex']) AND $donnees['id_pokedex']==$_POST['id_pokedex']){echo 'selected="selected"';} echo ' >'.$donnees['nom'].'</option>';
    }
    ?>
    </select>	
    Shiney : 
    <select name="shiney">
    <option value="0" <?php if(isset($_POST['shiney']) AND $_POST['shiney']==0){echo 'selected="selected"';}?>>Non</option>
    <option value="1" <?php if(isset($_POST['shiney']) AND $_POST['shiney']==1){echo 'selected="selected"';}?>>Oui</option>	
    </select>
    <input type="submit" value="Celui-là !" />
    </form>

    <?php
    if(isset($_POST['id_pokedex']) AND $_POST['id_pokedex']>0 AND $_POST['action']!="valider"){
        $reponse = $bdd->prepare('SELECT id, id_pokedex, nom, legendaire, tdc FROM pokemons_base_pokemons WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));  
        $reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));
        $donnees = $reponse->fetch();    
        //calul nb joueurs
        $reponse = $bdd->query('SELECT COUNT(id) AS comptage FROM pokemons_membres') or die(print_r($bdd->errorInfo()));
        $donnees2 = $reponse->fetch();
        $nb_joueurs=$donnees2['comptage'];
        //calul nb pokemon
        if($_POST['shiney']==1){
            $reponse = $bdd->prepare('SELECT COUNT(id) AS comptage FROM pokemons_liste_pokemons WHERE id_pokemon=:id_pokemon AND shiney=1') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('id_pokemon' => $donnees['id']));
            $donnees2 = $reponse->fetch();
            $nb_pok=$donnees2['comptage'];
            $reponse = $bdd->prepare('SELECT COUNT(id) AS comptage FROM pokemons_buy_pokemons WHERE id_pokedex=:id_pokedex AND shiney=1') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));
            $donnees2 = $reponse->fetch();
            $nb_pok_buy=$donnees2['comptage'];
        }else{
            $reponse = $bdd->prepare('SELECT COUNT(id) AS comptage FROM pokemons_liste_pokemons WHERE id_pokemon=:id_pokemon') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('id_pokemon' => $donnees['id'])); 
            $donnees2 = $reponse->fetch();
            $nb_pok=$donnees2['comptage'];
            $reponse = $bdd->prepare('SELECT COUNT(id) AS comptage FROM pokemons_buy_pokemons WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));
            $donnees2 = $reponse->fetch();
            $nb_pok_buy=$donnees2['comptage'];
        }
        $reponse = $bdd->prepare('SELECT COUNT(id) AS comptage FROM pokemons_buy_pokemons') or die(print_r($bdd->errorInfo()));
        $reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));
        $donnees2 = $reponse->fetch();
        $nb_pok_buy_tot=$donnees2['comptage'];   
        
        //Calcul du prix
        $multi1=sqrt(sqrt(255/$donnees['tdc']));
        $multi2=1+3*$donnees['legendaire'];
        $multi3=$nb_joueurs/($nb_pok+5);
        $multi4=40*($nb_pok_buy/$nb_pok_buy_tot)+1;
        $prix=$multi1*$multi2*$multi3*$multi4;
        $prix=ceil($prix);
        $prix_max=ceil((5000/(sqrt($donnees['tdc']))*(($_POST['shiney']*2)+1)*(($donnees['legendaire']/2)+1)));
        //prix max
        if($prix>$prix_max){$prix=$prix_max;}
        //prix minimum
        if($prix<50){$prix=50;}
        if($_POST['shiney']==1 AND $prix<300){$prix=300;}
        if($donnees['legendaire']==1 AND $prix<500){$prix=500;}
        
        
        //echo '1°'.$multi1.' 2°'.$multi2.' 3°'.$multi3.' 4°'.$multi4;
        ?>

        <img src="images/pokemons/<?php if($_POST['shiney']==1){echo 'shiney/';}?><?php echo $_POST['id_pokedex'];?>.gif" height="150px" style="border-style:none; margin-left:4px; margin-top:10px; "/>
        <br />
    
        On te vend ce <?php echo $donnees['nom'];?> au prix de <?php echo $prix;?> pépites. Qu'en dis-tu ?

        
        <form method="post" action="buy_pokemons.php">
        <input type="hidden" name="id_pokedex" value="<?php echo $_POST['id_pokedex'];?>"/>
        <input type="hidden" name="shiney" value="<?php echo $_POST['shiney'];?>"/>
        <input type="hidden" name="action" value="valider"/>
        <input type="hidden" name="prix" value="<?php echo $prix;?>"/>
        <input type="submit" value="Je le veux !" />
        </form>
        <?php

    }
}
    ?>


<?php include ("bas.php"); ?>