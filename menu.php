<?php //tri des pokémons inactifs
if(isset($_POST['tri']))
	{
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET tri_points=:tri_points WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	    $reponse->execute(array('tri_points' => $_POST['tri'], 'pseudo' => $_SESSION['pseudo'])); 
	}

?>

<div id="menu_all">
<div id="menu">
<?php


if(isset($_GET['pokemon_actif']) AND isset($_GET['pokemon_actif']) AND $_GET['pokemon_actif']!=0 AND $_SESSION['page_combat']=="NULL" OR $_SESSION['page_combat']=="" AND $_GET['pokemon_actif']!=0) //changement de pokémon actif
{
$reponse = $bdd->prepare('SELECT pseudo, lvl FROM pokemons_liste_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_GET['pokemon_actif']));
$donnees = $reponse->fetch();
if($donnees['pseudo']==$_SESSION['pseudo'] AND $donnees['lvl']!=0)
	{
	$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET  actif=0 WHERE actif=1 AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('pseudo' =>$_SESSION['pseudo'])); 
	$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET  actif=1 WHERE id=:id') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('id' => $_GET['pokemon_actif'])); 	
	}
}



if(isset($_SESSION['pseudo'])) //connect
{
$reponse = $bdd->prepare('SELECT id, lvl, attaque1, attaque2, attaque3, attaque4, shiney, victoires, defaites FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
if(isset($donnees['id']))
    { //actualisation des scores du pokémon actif

    $score=0;
    $score=$score+$donnees['lvl']*10;
    if($donnees['attaque1']!=0){$score=$score+50;}if($donnees['attaque2']!=0){$score=$score+50;}if($donnees['attaque3']!=0){$score=$score+50;}if($donnees['attaque4']!=0){$score=$score+50;}
    if($donnees['shiney']==1){$score=$score+200;}
    $score=$score+$donnees['victoires']; $score=$score-$donnees['defaites'];
   
    $reponse2 = $bdd->prepare('UPDATE pokemons_liste_pokemons SET score=:score WHERE id=:id') or die(print_r($bdd->errorInfo()));
              $reponse2->execute(array('score' =>$score, 'id' => $donnees['id'])); 
        

    }
else
    {
    $reponse = $bdd->prepare('SELECT id FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
    $reponse->execute(array('pseudo' => $_SESSION['pseudo']));
    $donnees = $reponse->fetch();
    $id_pokemon_devient_actif=$donnees['id'];
    $reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET actif=1 WHERE id=:id') or die(print_r($bdd->errorInfo()));
              $reponse->execute(array('id' => $id_pokemon_devient_actif)); 
    }

   


//actualisation du score du joueur
$score_joueur=0;
$reponse = $bdd->prepare('SELECT score FROM pokemons_liste_pokemons WHERE pseudo=:pseudo ORDER BY score DESC LIMIT 0,9') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
while($donnees = $reponse->fetch()){$score_joueur=$score_joueur+$donnees['score'];}
$score_joueur=$score_joueur/10;
$nb_pokemons_joueur=0;
$reponse = $bdd->prepare('SELECT COUNT(id) AS nb_pokemons_joueur FROM pokemons_liste_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
$nb_pokemons_joueur=$donnees['nb_pokemons_joueur'];
$score_joueur=$score_joueur+$nb_pokemons_joueur;
$score_joueur=floor($score_joueur);
$reponse2 = $bdd->prepare('UPDATE pokemons_membres SET score=:score WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	          $reponse2->execute(array('score' =>$score_joueur, 'pseudo' => $_SESSION['pseudo'])); 
//
$reponse = $bdd->prepare('SELECT id, id_pokemon, lvl, pa_restant, pa_max, pa_bonus, xp, sexe, shiney, bonheur, surnom, score FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo())); //vérification pokémon actif
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
$id_image_pokemon=$donnees['id'];
$id_pokemon_actif=$donnees['id_pokemon'];
$lvl_pokemon_actif=$donnees['lvl'];
$pa_restant=$donnees['pa_restant'];
$pa_max=$donnees['pa_max'];
$pa_bonus=$donnees['pa_bonus'];
$xp_pokemon_actif=$donnees['xp'];
$sexe=$donnees['sexe'];
$shiney=$donnees['shiney'];
$score=$donnees['score'];
$bonheur=$donnees['bonheur'];
$count_lvl=0;$xp_suivant_pokemon_actif=0;
$surnom_pokemon_actif=$donnees['surnom'];
while($count_lvl<=$lvl_pokemon_actif) {$xp_suivant_pokemon_actif=$xp_suivant_pokemon_actif+$count_lvl*10;$count_lvl=$count_lvl+1;}
$reponse = $bdd->prepare('SELECT nom, id_pokedex FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $id_pokemon_actif));
$donnees = $reponse->fetch();
$pokemon_actif=$donnees['nom'];	
$id_pokedex_pokemon_actif=$donnees['id_pokedex'];	
//DONNEES DU JOUEUR	
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_SESSION['id_membre']));
$donnees = $reponse->fetch();
$id_du_joueur_actif=$donnees['id'];
$map_rapide=$donnees['map_rapide'];
$score_pvp=$donnees['score_pvp'];
$score_joueur=$donnees['score'];
$points_quetes=$donnees['points_quetes'];
$score_joueur=$score_joueur+$points_quetes;
$missions_accomplies=$donnees['missions_accomplies'];
$score_missions=floor($missions_accomplies/10);
$score_joueur=$score_joueur+$score_missions;
$sexe_joueur=$donnees['sexe'];
$tri_points=$donnees['tri_points'];
$depenses_marche=$donnees['depenses_marche'];
$proba_naissance=$donnees['proba_naissance'];
$astuces=$donnees['astuces'];
$argent=$donnees['pokedollar'];
$pepites=$donnees['ors'];
$bds=$donnees['bds'];
$pv=$donnees['pos_ver'];
$ph=$donnees['pos_hor'];
$pos_ver=$donnees['pos_ver'];
$pos_hor=$donnees['pos_hor'];
$concours=$donnees['concours'];
$description=$donnees['description'];
$quete_principale=$donnees['quete_principale'];
$quete_principale_etape=$donnees['quete_principale_etape'];
$quete_principale_active_membre=$donnees['quete_principale_active'];
$a_envoye_duel_grade=$donnees['a_envoye_duel_grade'];
$triche=$donnees['triche'];
$nb_quete_secondaire=$donnees['nb_quete_secondaire'];
$bonus_racketteur=$donnees['bonus_racketteur'];
if($bonus_racketteur>$time){$bonus_racketteur="ok";}
$num_QP=$donnees['quete_principale'];
$adresse_ip=$donnees['adresse_ip'];
$opacity=$donnees['opacity'];
$nb_pages=$donnees['nb_pages'];
$reponse2 = $bdd->prepare('SELECT COUNT(id) AS nb_g FROM pokemons_grade WHERE pseudo=:pseudo AND quand_fin="0000-00-00 00:00:00"') or die(print_r($bdd->errorInfo()));
$reponse2->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees2 = $reponse2->fetch();
$nb_grade=$donnees2['nb_g'];


//update score_total
$reponseX = $bdd->prepare('UPDATE pokemons_membres SET score_total=:score_total WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponseX->execute(array('score_total' =>$score_joueur ,'pseudo' => $_SESSION['pseudo'])); 						

$reponse3 = $bdd->prepare('SELECT COUNT(id) AS nb_messages FROM pokemons_mails WHERE destinataire=:destinataire AND statut="non lu"') or die(print_r($bdd->errorInfo()));
$reponse3->execute(array('destinataire' => $_SESSION['pseudo']));	   
$donnees3 = $reponse3->fetch();
$nb_messages=$donnees3['nb_messages'];
$reponse3 = $bdd->prepare('SELECT COUNT(id) AS nb_messages FROM pokemons_mails WHERE destinataire=:destinataire AND statut="non lu" AND archive=0') or die(print_r($bdd->errorInfo()));
$reponse3->execute(array('destinataire' => $_SESSION['pseudo']));	   
$donnees3 = $reponse3->fetch();
$nb_messages_non_archive=$donnees3['nb_messages'];
$reponse3 = $bdd->prepare('SELECT COUNT(id) AS nb_messages FROM pokemons_mails WHERE destinataire=:destinataire AND statut="non lu" AND archive=1') or die(print_r($bdd->errorInfo()));
$reponse3->execute(array('destinataire' => $_SESSION['pseudo']));	   
$donnees3 = $reponse3->fetch();
$nb_messages_archive=$donnees3['nb_messages'];

$reponse4 = $bdd->prepare('SELECT id FROM pokemons_quetes_principales WHERE numero=:numero AND etape=:etape') or die(print_r($bdd->errorInfo()));
$reponse4->execute(array('numero' => $donnees['quete_principale'], 'etape' => $donnees['quete_principale_etape']));  
$donnees4 = $reponse4->fetch();
$id_quete_actuelle=$donnees4['id'];
$id_quete_principale=$donnees4['id'];
$nb_pokemons_capture=0;
$nb_pokemons_capture_V1=0;
$nb_pokemons_capture_V2=0;
$nb_pokemons_capture_V3=0;
$nb_pokemons_shiney_capture=0;
$nb_pokemons_shiney_capture_V1=0;
$nb_pokemons_shiney_capture_V2=0;
$nb_pokemons_shiney_capture_V3=0;
$reponse5 = $bdd->prepare('SELECT COUNT(DISTINCT id_pokemon) AS nb_pokemons_capture FROM pokemons_captures WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse5->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees5 = $reponse5->fetch();
$nb_pokemons_capture=$donnees5['nb_pokemons_capture'];
$reponse5 = $bdd->prepare('SELECT COUNT(DISTINCT id_pokemon) AS nb_pokemons_capture_V1 FROM pokemons_captures INNER JOIN pokemons_base_pokemons ON pokemons_captures.id_pokemon=pokemons_base_pokemons.id WHERE pseudo=:pseudo AND id_pokedex<=151') or die(print_r($bdd->errorInfo()));
$reponse5->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees5 = $reponse5->fetch();
$nb_pokemons_capture_V1=$donnees5['nb_pokemons_capture_V1'];
$reponse5 = $bdd->prepare('SELECT COUNT(DISTINCT id_pokemon) AS nb_pokemons_capture_V2 FROM pokemons_captures INNER JOIN pokemons_base_pokemons ON pokemons_captures.id_pokemon=pokemons_base_pokemons.id WHERE pseudo=:pseudo AND id_pokedex>151 AND id_pokedex<=251') or die(print_r($bdd->errorInfo()));
$reponse5->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees5 = $reponse5->fetch();
$nb_pokemons_capture_V2=$donnees5['nb_pokemons_capture_V2'];
$nb_pokemons_capture_V3=$nb_pokemons_capture-$nb_pokemons_capture_V2-$nb_pokemons_capture_V1;

$reponse5 = $bdd->prepare('SELECT COUNT(DISTINCT id_pokemon) AS nb_pokemons_capture FROM pokemons_captures WHERE pseudo=:pseudo AND shiney=1') or die(print_r($bdd->errorInfo()));
$reponse5->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees5 = $reponse5->fetch();
$nb_pokemons_shiney_capture=$donnees5['nb_pokemons_capture'];
$reponse5 = $bdd->prepare('SELECT COUNT(DISTINCT id_pokemon) AS nb_pokemons_capture_V1 FROM pokemons_captures INNER JOIN pokemons_base_pokemons ON pokemons_captures.id_pokemon=pokemons_base_pokemons.id WHERE pseudo=:pseudo AND shiney=1 AND id_pokedex<=151') or die(print_r($bdd->errorInfo()));
$reponse5->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees5 = $reponse5->fetch();
$nb_pokemons_shiney_capture_V1=$donnees5['nb_pokemons_capture_V1'];
$reponse5 = $bdd->prepare('SELECT COUNT(DISTINCT id_pokemon) AS nb_pokemons_capture_V2 FROM pokemons_captures INNER JOIN pokemons_base_pokemons ON pokemons_captures.id_pokemon=pokemons_base_pokemons.id WHERE pseudo=:pseudo AND shiney=1 AND id_pokedex>151 AND id_pokedex<=251') or die(print_r($bdd->errorInfo()));
$reponse5->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees5 = $reponse5->fetch();
$nb_pokemons_shiney_capture_V2=$donnees5['nb_pokemons_capture_V2'];
$nb_pokemons_shiney_capture_V3=$nb_pokemons_shiney_capture-$nb_pokemons_shiney_capture_V2-$nb_pokemons_shiney_capture_V1;


$score_total_diminue=$score_joueur-10;
$reponse7 = $bdd->prepare('SELECT COUNT(id) AS total_sup FROM pokemons_membres WHERE score_total>=:score_total') or die(print_r($bdd->errorInfo()));
$reponse7->execute(array('score_total' => $score_joueur));	
$donnees7 = $reponse7->fetch();
$rang_total=$donnees7['total_sup'];

?>
<div style="position:relative; top:-0px; left:-27px;width:40px;"><abbr title="Votre compte"><a href="compte.php"><img src="images/menu/compte.gif" height="40px" style="border:0;" /></a></abbr></div><br />
<div style="position:relative; top:-28px; left:-27px;width:40px;"><abbr title="Le classement"><a href="classement.php"><img src="images/menu/classement.gif" height="40px" style="border:0;" /></a></abbr></div><br />
<div style="position:relative; top:-56px; left:-27px;width:40px;"><abbr title="Vos récompenses"><a href="recompenses.php"><img src="images/menu/recompenses.gif" height="40px" style="border:0;" /></a></abbr></div><br />
<div style="position:relative; top:-84px; left:-27px;width:40px;"><abbr title="La mine"><a href="mine.php"><img src="images/menu/mine.gif" height="40px" style="border:0;" /></a></abbr></div><br />
<div style="position:relative; top:-112px; left:-27px;width:40px;"><abbr title="Bonus triopikeur"><a href="bonus.php"><img src="images/menu/bonus.gif" height="40px" style="border:0;" /></a></abbr></div><br />
<div style="position:relative; top:-140px; left:-27px;width:40px;"><abbr title="Le casino"><a href="casino.php"><img src="images/menu/jackpot.gif" height="40px" style="border:0;" /></a></abbr></div><br />
<div style="position:relative; top:-168px; left:-27px;width:40px;"><abbr title="Les missions"><a href="missions.php"><img src="images/menu/missions.gif" height="40px" style="border:0;" /></a></abbr></div><br />
<div style="position:relative; top:-196px; left:-27px;width:40px;"><abbr title="Les enchères"><a href="encheres.php"><img src="images/menu/encheres.gif" height="40px" style="border:0;" /></a></abbr></div><br />

<div style="position:relative; top:-505px; left:145px;width:40px;"><abbr title="La carte"><a href="carte<?php if($map_rapide==1){echo '2';}?>.php"><img src="images/menu/carte.gif" width="40px" height="40px" style="border:0;" /></a></abbr></div><br />
<div style="position:relative; top:-531px; left:145px;width:40px;"><abbr title="Journal des quêtes"><a href="journaldesquetes.php"><img src="images/menu/quetes.gif" height="40px" style="border:0;" /></a></abbr></div><br />
<div style="position:relative; top:-559px; left:145px;width:40px;"><abbr title="Vos pokémons"><a href="vos_pokemons.php"><img src="images/menu/pokemons.gif" height="40px" style="border:0;" /></a></abbr></div><br />
<?php if($id_quete_actuelle >3 OR $id_quete_actuelle==3 AND $quete_principale_active_membre==1){ ?><div style="position:relative; top:-587px; left:145px;width:40px;"><abbr title="Pokedex"><a href="pokedex.php"><img src="images/menu/pokedex.gif" height="40px" style="border:0;" /></a></abbr></div><br /><?php } ?>
<?php if($id_quete_actuelle >7 OR $id_quete_actuelle==7 AND $quete_principale_active_membre==1){ ?><div style="position:relative; top:-615px; left:145px;width:40px;"><abbr title="Shop"><a href="shop.php?items=pokeballs"><img src="images/menu/shop.gif" height="40px" style="border:0;" /></a></abbr></div><br /><?php } ?>
<?php if($id_quete_actuelle >12 OR $id_quete_actuelle==12 AND $quete_principale_active_membre==1){ ?><div style="position:relative; top:-643px; left:145px;width:40px;"><abbr title="Le marché"><a href="marche.php"><img src="images/menu/echange.gif" height="40px" style="border:0" /></a></abbr></div><br /><?php } ?>
<?php if($id_quete_actuelle >26 OR $id_quete_actuelle==26 AND $quete_principale_active_membre==1){ ?><div style="position:relative; top:-671px; left:145px;width:40px;"><abbr title="Le Dracoport"><a href="dracoport.php"><img src="images/menu/dracoport.gif" height="40px" style="border:0;" /></a></abbr></div><br /><?php } ?>
<?php if($id_quete_actuelle >36 OR $id_quete_actuelle==36 AND $quete_principale_active_membre==1){ ?><div style="position:relative; top:-699px; left:145px;width:40px;"><abbr title="Blind Battles"><a href="inscription_blind_battles.php"><img src="images/menu/blind_battles.gif" height="40px" style="border:0;" /></a></abbr></div><br /><?php } ?>
<?php if($id_quete_actuelle >38 OR $id_quete_actuelle==38 AND $quete_principale_active_membre==1){ ?><div style="position:relative; top:-727px; left:145px; width:40px;"><abbr title="Pension pokémon"><a href="elevage.php"><img src="images/menu/elevage.gif" height="40px" style="border:0;" /></a></abbr></div><br /><?php } ?>

<div style="margin-left:-19px;margin-top:
<?php if($id_quete_actuelle<3 OR $id_quete_actuelle==3 AND $quete_principale_active_membre==0){echo '-705';} 
	  elseif($id_quete_actuelle==3 AND $quete_principale_active_membre==1 OR $id_quete_actuelle>3 AND $id_quete_actuelle<7 OR $id_quete_actuelle==7 AND $quete_principale_active_membre==0){echo '-766';}
	  elseif($id_quete_actuelle==7 AND $quete_principale_active_membre==1 OR $id_quete_actuelle>7 AND $id_quete_actuelle<12 OR $id_quete_actuelle==12 AND $quete_principale_active_membre==0){echo '-829';}
	  elseif($id_quete_actuelle==12 AND $quete_principale_active_membre==1 OR $id_quete_actuelle>12 AND $id_quete_actuelle<26 OR $id_quete_actuelle==26 AND $quete_principale_active_membre==0){echo '-892';}
	  elseif($id_quete_actuelle==26 AND $quete_principale_active_membre==1 OR $id_quete_actuelle>26 AND $id_quete_actuelle<36 OR $id_quete_actuelle==36 AND $quete_principale_active_membre==0){echo '-955';}
	  elseif($id_quete_actuelle==36 AND $quete_principale_active_membre==1 OR $id_quete_actuelle>36 AND $id_quete_actuelle<38 OR $id_quete_actuelle==38 AND $quete_principale_active_membre==0){echo '-1018';}
	  elseif($id_quete_actuelle==38 AND $quete_principale_active_membre==1 OR $id_quete_actuelle>38 AND $id_quete_actuelle<1000 OR $id_quete_actuelle==1000 AND $quete_principale_active_membre==0){echo '-1081';}
	  ?>px;margin-right:15px;text-align:center;">
<span style="font-size:24px;padding-left:7px;"><b><?php echo $_SESSION['pseudo']; ?></b></span><br />
<?php
$chemin = 'images/avatars/'.$_SESSION['id_membre'].'.jpg';
echo '<a href="profil.php?profil='.$_SESSION['pseudo'].'">';
if(file_exists($chemin))
	{echo '<abbr title="Votre profil"><img src="images/avatars/'.$_SESSION['id_membre'].'.jpg" style="padding-left:7px;border:0;"/></abbr><br />';}
else 
	{
	if($sexe_joueur==0){echo '<abbr title="Votre profil"><img src="images/avatars/auto.gif" style="padding-left:7px;border:0;"/></abbr><br />';}
	if($sexe_joueur==1){echo '<abbr title="Votre profil"><img src="images/avatars/auto1.gif" style="padding-left:7px;border:0;"/></abbr><br />';}
	}
echo '</a>';
?>
<b> <?php echo $argent; ?> $</b> <br />
<b> <?php echo $pepites; ?> <img src="images/or.png" /></b> <a href="http://pokemon-origins.forumactif.com/t64-faq#127" target="_blank"><img src="images/interrogation.png" style="border:0; position:absolute;margin-top:-4px;" /></a><br />
<b>Score : <?php echo $score_joueur; ?></b><br />
<b><a href="classement.php" style="color:black;" >Rang : <?php echo $rang_total; ?></a></b><br />
<b>BDS : <?php echo $nb_pokemons_joueur; ?>/<?php echo $bds; ?> </b><br />

<?php
if($nb_pokemons_joueur==$bds)
{$max_pokemon_atteint=1;}
?>
<br />



<?php //comptage menu Pvp
$nb_defis_recu=0;
$nb_defis_doit_joue=0;
$nb_defis_urgent=0;
$reponse = $bdd->prepare('SELECT COUNT(id) AS nb_defis_recu FROM pokemons_liste_defis WHERE joueur2=:joueur2 AND statut=0 ORDER BY mode') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('joueur2' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();$nb_defis_recu=$donnees['nb_defis_recu'];
$reponse = $bdd->prepare('SELECT id, joueur1, joueur2, pokemon_actif_j1, pokemon_actif_j2, attente FROM pokemons_liste_defis WHERE joueur1=:joueur1 AND statut=1 OR joueur2=:joueur2 AND statut=1 ORDER BY mode') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('joueur1' => $_SESSION['pseudo'],'joueur2' => $_SESSION['pseudo']));
while($donnees = $reponse->fetch())
	{
	if($donnees['joueur1']==$_SESSION['pseudo']){$adversaire=$donnees['joueur2'];$num_pokemon_adv=$donnees['pokemon_actif_j2'];$num_pokemon=$donnees['pokemon_actif_j1'];}else{$adversaire=$donnees['joueur1'];$num_pokemon_adv=$donnees['pokemon_actif_j1'];$num_pokemon=$donnees['pokemon_actif_j2'];}	
	$reponse2 = $bdd->prepare('SELECT pv FROM pokemons_liste_defis_pokemons WHERE id_defis=:id_defis AND numero=:numero AND proprietaire=:proprietaire') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id_defis' => $donnees['id'],'numero' => $num_pokemon_adv, 'proprietaire' =>$adversaire));
	$donnees2 = $reponse2->fetch();
	$reponse3 = $bdd->prepare('SELECT pv FROM pokemons_liste_defis_pokemons WHERE id_defis=:id_defis AND numero=:numero AND proprietaire=:proprietaire') or die(print_r($bdd->errorInfo()));
	$reponse3->execute(array('id_defis' => $donnees['id'],'numero' => $num_pokemon, 'proprietaire' =>$_SESSION['pseudo']));
	$donnees3 = $reponse3->fetch();
	if($donnees['attente']==$_SESSION['pseudo'] OR $donnees3['pv']==0){$nb_defis_urgent=$nb_defis_urgent+1;}elseif($donnees['attente']=="" AND $donnees2['pv']!=0){$nb_defis_doit_joue=$nb_defis_doit_joue+1;}
	}
?>

<br />
<a href="messages.php" style="color:<?php if($nb_messages>0){echo 'red';}else{echo 'black';} ?>;"><b><?php echo $nb_messages; ?> message<?php if($nb_messages>1){echo 's';} ?> non lu<?php if($nb_messages>1){echo 's';} ?></b></a><br />
<a href="pvp.php" style="color:black;"><b>PvP</b></a> : <abbr title="Défis reçus"><span style="color:green;"><?php echo $nb_defis_recu;?></span></abbr>     <abbr title="Défis où vous et votre adversaire devez choisir une action"><span style="color:dodgerBlue;"><?php echo $nb_defis_doit_joue;?></span></abbr>     <abbr title="Défis où vous êtes le seul à devoir choisir une action"><span style="color:mediumBlue;"><?php echo $nb_defis_urgent?></span></abbr> <a href="http://pokemon-origins.forumactif.com/t64-faq#129" target="_blank"><img src="images/interrogation.png" style="border:0; position:absolute;margin-top:-2px;" /></a> <br />
<b>Score PvP : <?php echo $score_pvp;?></b><br />
<a href="index.php" style="color:black;"><b>Accueil</b></a><br />
<a href="http://pokemon-origins.forumactif.com" style="color:black;" target="_blank"> <b>Forum </b></a><br />
<a href="https://discord.gg/FKYrMbr" style="color:black;" target="_blank"> <b>Tchat (Discord) </b></a><br />
<a href="http://pokemon-origins.forumactif.com/t64-faq" style="color:black;" target="_blank"> <b>FAQ </b></a><br />
<a href="deconnexion.php" style="color:black;"><b>Déconnexion</b> </a><br />
</div>
<br />

<div style="margin-left:-9px;margin-right:25px;text-align:center;">
<b>Pokémon actif :</b> <br />
<?php echo '<a href="vos_pokemons.php?id='.$id_image_pokemon.'"><img src="images/pokemons/'; if($shiney==1){echo 'shiney/';} echo $id_pokedex_pokemon_actif.'.gif" height="80px" style="border-style:none;"/></a>'; ?>
<br />
<?php echo '<b><abbr title="'.$pokemon_actif.'">'.$surnom_pokemon_actif.'</abbr></b>'; echo'<img src="images/';if($sexe=="M"){echo 'male';} elseif($sexe=="F"){echo 'femelle';} else {echo 'nosexe';} echo'.png" style="position:relative;bottom:-1px;left:-2px;">'; ?> lvl <?php echo $lvl_pokemon_actif; ?> <br />
<b>XP :</b> <?php echo $xp_pokemon_actif.'/'.$xp_suivant_pokemon_actif; ?><br />
<?php 
if($xp_pokemon_actif>=$xp_suivant_pokemon_actif AND $lvl_pokemon_actif<100)
	{
	?>
	<form action="update_lvl.php" method="post">
	<input type="hidden" name="id" value="<?php echo $id_image_pokemon; ?>"/>
	<input type="submit" value="Monter de niveau" />	  
	</form>
	<?php
	}
if($xp_pokemon_actif>=50500 AND $lvl_pokemon_actif==100)
{
	?>
	<form action="update_lvl_100.php?id=<?php  echo $id_image_pokemon; ?>" method="post">
	<input type="submit" value="Entraîner le pokémon" />	  
	</form>
	<?php
	}
?>
<b>Points d'action :</b>  <?php echo $pa_restant; ?>/<?php echo $pa_max; ?> <br />
<b>PA par jour :</b>  <?php echo $pa_bonus; ?> <br />
<b>Score du pokémon:</b> <?php echo $score; ?><br />
<b>Bonheur:</b> <?php echo $bonheur; ?>/255  <a href="http://pokemon-origins.forumactif.com/t64-faq#4801" target="_blank"><img src="images/interrogation.png" style="border:0; position:absolute;margin-top:-2px;" /></a><br />
</div>

<?php
}
else //pas encore connecté
{
?>
<div style="margin-top:-40px;">


<br />
<img src="images/identification.png" width="150px" style="margin-top:0px;margin-left:0px; border:none;" />
<form action="connexion.php" method="post">
<input type="hidden" name="identification" value="identification"/>
<label for="pseudo">Pseudo</label> :<br /> <td><input type="text" name="pseudo" id="pseudo" /> <br />
<label for="mdp">Mot de passe</label> :<br /><input type="password" name="mdp"  id="mdp" /> 
<input type="hidden" name="action" value="identification"/>
<input type="submit" value="Login" />	  
</form>
<a href="mdp_oublie.php" style="color:black;"><b>Mot de passe oublié?</b></a><br />
<a href="inscription.php" style="color:black;"><b>S'inscrire</b></a><br />
<a href="http://pokemon-origins.forumactif.com" style="color:black;" target="_blank"> <b>Forum </b></a><br />
<a href="https://discord.gg/FKYrMbr" style="color:black;" target="_blank"> <b>Tchat (Discord) </b></a><br />
<br />
</div>

<?php
}
?>
</div>
<div id="back_menu"></div>


<div id="menu"> 
<?php 
if(isset($_SESSION['pseudo'])) //connect
{
?>
<div id="pokemons_inactifs">
<b>Pokémons inactifs :</b>
<table id="table_pokemons_inactifs" width="162px" cellpadding="2" cellspacing="0"><colgroup><COL WIDTH=52%><COL WIDTH=24%><COL WIDTH=24%></COLGROUP>
<?php //pokemon non actif
if($tri_points==0){$reponse = $bdd->prepare('SELECT id, id_pokemon, surnom, lvl, pa_restant, shiney FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY lvl DESC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==1){$reponse = $bdd->prepare('SELECT id, id_pokemon, surnom, lvl, pa_restant, shiney FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY score DESC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==2){$reponse = $bdd->prepare('SELECT id, id_pokemon, surnom, lvl, pa_restant, shiney FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY victoires DESC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==3){$reponse = $bdd->prepare('SELECT id, id_pokemon, surnom, lvl, pa_restant, shiney FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY surnom ASC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==4){$reponse = $bdd->prepare('SELECT id, id_pokemon, surnom, lvl, pa_restant, shiney FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY lvl DESC,surnom ASC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==5){$reponse = $bdd->prepare('SELECT id, id_pokemon, surnom, lvl, pa_restant, shiney FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY pa_restant DESC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==6){$reponse = $bdd->prepare('SELECT id, id_pokemon, surnom, lvl, pa_restant, shiney FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 AND lvl!=0 ORDER BY id_pokemon ASC ') or die(print_r($bdd->errorInfo()));}

$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
while($donnees = $reponse->fetch())
{
$id_pokemon_inactif=$donnees['id_pokemon'];
$lvl_pokemon_inactif=$donnees['lvl'];
$pa_restant_pokemon_inactif=$donnees['pa_restant'];
$id_liste_pokemon=$donnees['id'];
$shiney_pokemon=$donnees['shiney'];
$surnom_pokemon_inactif=$donnees['surnom'];
$reponse2 = $bdd->prepare('SELECT nom FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse2->execute(array('id' => $id_pokemon_inactif));
$donnees2 = $reponse2->fetch();
$nom_pokemon_inactif=$donnees2['nom'];
if($donnees['surnom']=="") // Si SURNOM = ""
	{
	$reponse3 = $bdd->prepare('UPDATE pokemons_liste_pokemons SET surnom=:surnom WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse3->execute(array('surnom' => $nom_pokemon_inactif, 'id' => $donnees['id']));
	$surnom_pokemon_inactif=$nom_pokemon_inactif;
	}
if($lvl_pokemon_inactif==0){echo '<tr style="font-size:12px;"><td>Oeuf</td><td></td><td></td></tr>';}
else{echo '<tr style="font-size:12px;'; if($shiney_pokemon==1){echo 'background-color:lightblue;';} echo '"><td><a href="carte.php?pokemon_actif='.$id_liste_pokemon.'" style="color:black;text-decoration: none;"><abbr title="'.$nom_pokemon_inactif.'">'.$surnom_pokemon_inactif.'</abbr></a></td><td>lvl '.$lvl_pokemon_inactif.'</td><td><img src="images/coeur.gif" />'.$pa_restant_pokemon_inactif.'</td></tr>';}
}
?>	
</table>
<br />

</div>
<br /> 
Trier par :<br />
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
<input type="radio" name="tri" value="0" <?php if($tri_points==0){echo 'checked';}?>> Lvl	<br />
<input type="radio" name="tri" value="1" <?php if($tri_points==1){echo 'checked';}?>> Points <br />	
<input type="radio" name="tri" value="2" <?php if($tri_points==2){echo 'checked';}?>> Victoires <br />	
<input type="radio" name="tri" value="3" <?php if($tri_points==3){echo 'checked';}?>> Nom <br />
<input type="radio" name="tri" value="4" <?php if($tri_points==4){echo 'checked';}?>> lvl puis Nom <br />
<input type="radio" name="tri" value="5" <?php if($tri_points==5){echo 'checked';}?>> PA restants <br />	
<input type="radio" name="tri" value="6" <?php if($tri_points==6){echo 'checked';}?>> N°Pokedex <abbr title="Les oeufs ne seront plus affichés!">(!)</abbr><br />	
<input type="submit" value="Valider" />
</form>
<br />

<!-- 
Le tri se fait dans :
- menu 
- profil : affichage pokémon
- vos_pokemon : liste déroulante
- pvp : choix des pokémons
- elevage : liste inactif
- inscription blind battles
- mission : liste déroulante
- marche : liste pokémon à vendre

--> 
<?php
}
?>
</div>
</div>

