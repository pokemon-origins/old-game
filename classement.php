<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">


<?php
if(isset($_SESSION['pseudo']))
{
?>

<b>Quel classement désirez-vous voir?</b>
<form action="classement.php" method="post">
<select name="classement">
<option value="score_joueurs" <?php if((isset($_POST['classement']) AND $_POST['classement']=="score_joueurs") OR !isset($_POST['classement'])){echo 'selected="selected"';}?>>Classement des joueurs par score</option>
<option value="pvp_joueurs" <?php if(isset($_POST['classement']) AND $_POST['classement']=="pvp_joueurs"){echo 'selected="selected"';}?>>Classement des joueurs par PvP</option>
<option value="score_pokemons" <?php if(isset($_POST['classement']) AND $_POST['classement']=="score_pokemons"){echo 'selected="selected"';}?>>Classement des pokémons</option>
<option value="grade" <?php if(isset($_POST['classement']) AND $_POST['classement']=="grade"){echo 'selected="selected"';}?>>Classement par grade</option>
</select>
<input type="submit" value="Voir"/>
<br />
<br />

<?php 
if((isset($_POST['classement']) AND $_POST['classement']=="score_joueurs") OR !isset($_POST['classement']))
{
?>

<div id="profil_pokemons">
<table id="profil" width="533px" cellpadding="2" cellspacing="2" style="text-align:center;" >
<colgroup><COL WIDTH=5%><COL WIDTH=50%><COL WIDTH=20%><COL WIDTH=25%></COLGROUP>
<tr><th colspan="9">Classement des joueurs par score</th></tr>
<tr><td><b> N° </b></td><td><b>Pseudo</b></td><td><b>Score </b></td><td><b>Nombre de pokémons </b></td></tr>
<?php //classement joueurs
$num=0;
$reponse = $bdd->query('SELECT pseudo, score_total FROM pokemons_membres WHERE score_total>0 ORDER BY score_total DESC LIMIT 0,200') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
        $num++;
	$reponse2 = $bdd->prepare('SELECT COUNT(id) AS nb_pokemons FROM pokemons_liste_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('pseudo' => $donnees['pseudo']));
	$donnees2 = $reponse2->fetch();
	$nb_pokemons_joueur=$donnees2['nb_pokemons'];
	?>
	<tr><td><?php echo $num; ?></td> <td><a style="color:black;" href="profil.php?profil=<?php echo $donnees['pseudo'];?>"><?php echo $donnees['pseudo']; ?></a></td><td><?php echo $donnees['score_total']; ?></td><td><?php echo $nb_pokemons_joueur; ?></td></tr>
	
	<?php
	}
?>
</table>
</div>

<?php
}
if(isset($_POST['classement']) AND $_POST['classement']=="pvp_joueurs")
{
?>
<div id="profil_pokemons">
<table id="profil" width="533px" cellpadding="2" cellspacing="2" style="text-align:center;" >
<colgroup><COL WIDTH=5%><COL WIDTH=50%><COL WIDTH=20%><COL WIDTH=25%></COLGROUP>
<tr><th colspan="9">Classement des joueurs par PvP</th></tr>
<tr><td><b> N° </b></td><td><b>Pseudo</b></td><td><b>Score </b></td><td><b>Nombre de pokémons </b></td></tr>
<?php //classement joueurs
$num=0;
$reponse = $bdd->query('SELECT pseudo, score_pvp FROM pokemons_membres WHERE score_pvp>0 ORDER BY score_pvp DESC') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	$nb_pokemons_joueur=0;
	$reponse2 = $bdd->prepare('SELECT id FROM pokemons_liste_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('pseudo' => $donnees['pseudo']));
	while($donnees2 = $reponse2->fetch())
	{
	$nb_pokemons_joueur=$nb_pokemons_joueur+1;
	}
	$num=$num+1;
	?>
	<tr><td><?php echo $num; ?></td> <td><a style="color:black;" href="profil.php?profil=<?php echo $donnees['pseudo'];?>"><?php echo $donnees['pseudo']; ?></a></td><td><?php echo $donnees['score_pvp']; ?></td><td><?php echo $nb_pokemons_joueur; ?></td></tr>
	
	<?php
	}
?>
</table>
</div>

<?php
}
if(isset($_POST['classement']) AND $_POST['classement']=="score_pokemons")
{
?>

<div id="profil_pokemons">
<table id="profil" width="533px" cellpadding="2" cellspacing="2" style="text-align:center;" >
<colgroup><COL WIDTH=5%><COL WIDTH=50%><COL WIDTH=20%><COL WIDTH=25%></COLGROUP>
<tr><th colspan="9">Classement des pokémons</th></tr>
<tr><td><b> N° </b></td><td><b>Nom</b></td><td><b>Lvl </b></td><td><b>Score </b></td><td><b>Propriétaire </b></td></tr>
<?php //classement pokémons
$num=0;
$reponse = $bdd->query('SELECT shiney, lvl, score, pseudo FROM pokemons_liste_pokemons WHERE lvl!=0 ORDER BY score DESC') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	$reponse2 = $bdd->prepare('SELECT nom FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['id_pokemon']));
	$donnees2 = $reponse2->fetch();
	$num=$num+1;
	?>
	<tr <?php if($donnees['shiney']==1){echo 'style="background-color:lightblue;"';} ?>><td><?php echo $num; ?></td> <td><?php echo $donnees2['nom']; ?></td><td><?php echo $donnees['lvl'];?></td><td><?php echo $donnees['score']; ?></td><td><a style="color:black;" href="profil.php?profil=<?php echo $donnees['pseudo'];?>"><?php echo $donnees['pseudo']; ?></a></td></tr>
	
	<?php
	}
?>
</table>
</div>


<?php
}
if(isset($_POST['classement']) AND $_POST['classement']=="grade")
{
?>
<div id="profil_pokemons">
<table id="profil" width="533px" cellpadding="2" cellspacing="2" style="text-align:center;" >
<colgroup><COL WIDTH=35%><COL WIDTH=35%><COL WIDTH=15%><COL WIDTH=15%></COLGROUP>
<tr><th colspan="9">Champions actuels</th></tr>
<tr><td><b> Grade </b></td><td><b>Pseudo</b></td><td><b>Score </b></td><td><b>Nombre de pokémons </b></td></tr>
<?php //classement joueurs
$num=0;
$reponse = $bdd->query('SELECT * FROM pokemons_grade WHERE quand_fin="0000-00-00 00:00:00" ORDER BY grade') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('pseudo' => $donnees['pseudo']));
	$donnees2 = $reponse2->fetch();
	$nb_pokemons_joueur=0;
	$score_total=$donnees2['score_total'];
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('pseudo' => $donnees2['pseudo']));
	while($donnees2 = $reponse2->fetch())
		{
		$nb_pokemons_joueur=$nb_pokemons_joueur+1;
		}
	$num=$num+1;
	?>
	<tr><td><abbr title="<?php echo $donnees['description']; ?>"><?php echo $donnees['grade']; ?></abbr></td> <td><a style="color:black;" href="profil.php?profil=<?php echo $donnees['pseudo'];?>"><?php echo $donnees['pseudo']; ?></a></td><td><?php echo $score_total; ?></td><td><?php echo $nb_pokemons_joueur; ?></td></tr>
	<?php
	}
?>
</table>
</div>

<?php
$reponse = $bdd->query('SELECT * FROM pokemons_grade WHERE quand_fin="0000-00-00 00:00:00" ORDER BY grade') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	?>
	<div id="profil_pokemons">
	<table id="profil" width="533px" cellpadding="2" cellspacing="2" style="text-align:center;" >
	<colgroup><COL WIDTH=40%><COL WIDTH=20%><COL WIDTH=20%><COL WIDTH=20%></COLGROUP>
	<tr><th colspan="9">ANCIENS <?php echo $donnees['grade']; ?></th></tr>
	<tr><td><b> Pseudo </b></td><td><b>Début</b></td><td><b>Fin</b></td><td><b>Durée</b></td></tr>
<?php //classement joueurs
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_grade WHERE grade=:grade AND quand_fin!="0000-00-00 00:00:00" ORDER BY quand') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('grade' => $donnees['grade']));
	while($donnees2 = $reponse2->fetch())
		{
		$date1 = strtotime($donnees2['quand_fin']); 
		$date2 = strtotime($donnees2['quand']); 
		$timestamp=$date1-$date2;
		$sec=$timestamp % 60;
		$X=floor($timestamp/60) ;
		$min = $X %60 ;
		$X = floor($X/60); 
		$heure = $X %24 ;
		$jour = floor($X/24);
		?>
		<tr><td><a style="color:black;" href="profil.php?profil=<?php echo $donnees2['pseudo'];?>"><?php echo $donnees2['pseudo']; ?></a></td> <td><?php echo $donnees2['quand']; ?></td><td><?php echo $donnees2['quand_fin']; ?></td><td><?php echo $jour.'J '.$heure.'H '.$min.'M '.$sec.'S '; ?></td></tr>
		<?php
		}
	?>
	</table>
	</div>
	<?php
	}
}
?>



<?php
}
else
{
echo 'Vous devez être connecté pour accéder à cette page';
}
?>
<?php include ("bas.php"); ?>