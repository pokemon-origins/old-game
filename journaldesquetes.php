<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">


<?php
if(isset($_SESSION['pseudo']))
{
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
	$donnees = $reponse->fetch();
$quete_principale=$donnees['quete_principale'];
$quete_principale_etape=$donnees['quete_principale_etape'];
$quete_principale_active=$donnees['quete_principale_active'];
$nb_quete_principale=$donnees['nb_quete_principale'];
?>



<h2>Quête principale <a href="http://pokemon-origins.forumactif.com/t64-faq#121" target="_blank"><img src="images/interrogation.png" style="border:0;position:absolute;margin-top:4px;" /></a></h2> 
<?php 
//affichage des quêtes sinccintes
$numero_de_quete=1;
while($quete_principale==$numero_de_quete AND $quete_principale_etape>1 OR $quete_principale==$numero_de_quete AND $quete_principale_active==1 OR $quete_principale>$numero_de_quete)
	{
	//titre
	echo '<b>Quête n°'.$numero_de_quete.' : '; 
	if($numero_de_quete==1){echo 'Le tutoriel';}
	if($numero_de_quete==2){echo 'Le début de l\'aventure';}
	echo '</b><br />';
	//étape
	$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_principales WHERE numero=:numero ORDER BY etape') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('numero' => $numero_de_quete));  
	while($donnees = $reponse->fetch())
		{
		$id_quete_principale=$donnees['id'];
		$numero_quete=$donnees['numero'];
		$etape_quete=$donnees['etape'];
		$titre=$donnees['titre'];
		if($quete_principale>$numero_de_quete OR $quete_principale_etape>$etape_quete OR $quete_principale_etape==$etape_quete AND $quete_principale_active==1)
			{
			echo '<span style="margin-left:40px;"><a style="color:black;" href="journaldesquetes_principales.php?id='.$id_quete_principale.'">'.$titre.'</a></span><br />';
			}
		}
	//quete suivante
	$numero_de_quete=$numero_de_quete+1;
	}
if($quete_principale==1 AND $quete_principale_active==0 AND $quete_principale_etape==1)
	{
	echo 'Vous devriez aller voir le professeur Chen. Il a probablement beaucoup de choses à vous apprendre. Il se trouve à son laboratoire, en (20,97) <br />';
	}
echo '<br />';
//affichage des résumés par quête
if(isset($_GET['id']))
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_principales WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_GET['id']));  
	$donnees = $reponse->fetch();
		{
		$id_quete_principale=$donnees['id'];
		$numero_quete=$donnees['numero'];
		$etape_quete=$donnees['etape'];
		$qui=$donnees['qui'];
		$pos_hor_quete=$donnees['pos_hor'];
		$pos_ver_quete=$donnees['pos_ver'];
		$intro=$donnees['intro'];
		$objectif=$donnees['objectif'];
		$titre=$donnees['titre'];
		$nb_reussite=$donnees['nb_reussite'];
		if($quete_principale>$numero_quete OR $quete_principale_etape>=$etape_quete)
			{//affichage de la quête
			?>
			<table id="journal_quetes" width="550px" style="text-align:center;" cellpadding="2" cellspacing="2">
			<colgroup><COL WIDTH=18%><COL WIDTH=82%></COLGROUP>
			<tr><td>Titre</td> <td> <?php echo $titre; ?> </td></tr>
			<tr><td>Source</td> <td> <?php echo $qui.' en ('.$pos_ver_quete.','.$pos_hor_quete.')'; ?> </td></tr>
			<tr><td>Consignes</td> <td><?php echo nl2br($intro); ?> </td></tr>
			<tr><td>Objectif</td> <td> <?php echo nl2br($objectif); ?> </td></tr>
			<tr><td>Etat</td><td> <?php if($quete_principale>$numero_quete OR $quete_principale==$numero_quete AND $quete_principale_etape>$etape_quete){echo 'Terminée';}else{ echo 'En cours';if($nb_reussite>1){echo '('.$nb_quete_principale.'/'.$nb_reussite.')';}}?>   </td></tr>
			</table>
			<?php
			}
		else
			{
			echo 'Vous n\'avez pas encore accès à cette quête. <br />';
			}
		}
	}
?>


<h2> Quête secondaire en cours </h2>
<?php
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
	$donnees = $reponse->fetch();
$quete_secondaire=$donnees['quete_secondaire'];
$nb_quete_secondaire=$donnees['nb_quete_secondaire'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_secondaires WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $quete_secondaire));  
	$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{
	$pos_hor_quete=$donnees['pos_hor'];
	$pos_ver_quete=$donnees['pos_ver'];
	$qui_quete=$donnees['qui'];
	$genre_quete=$donnees['genre'];
	$nb_reussite_quete=$donnees['nb_reussite'];
	$difficulte_quete=$donnees['difficulte'];
	$intro_quete=$donnees['intro'];
	?>
	<table id="journal_quetes" width="550px" style="text-align:center;" cellpadding="2" cellspacing="2">
	<colgroup><COL WIDTH=18%><COL WIDTH=82%></COLGROUP>
	<tr><td>N° de la quête</td> <td> <?php echo $quete_secondaire; ?> </td></tr>
	<tr><td>Source</td> <td> <?php echo $qui_quete.' en ('.$pos_ver_quete.','.$pos_hor_quete.')'; ?> </td></tr>
	<tr><td>Consignes</td> <td> <?php echo nl2br($intro_quete); ?> </td></tr>
	<tr><td>Niveau de difficulté</td> <td> <?php echo $difficulte_quete; ?> </td></tr>
	<?php if($genre_quete=="kill"){ ?><tr><td>Etat</td> <td> <?php if($nb_quete_secondaire==$nb_reussite_quete){echo 'Terminée, retournez voir '.$qui_quete.' pour toucher votre récompense';} else {echo $nb_quete_secondaire.'/'.$nb_reussite_quete.' accompli';} ?> </td></tr><?php } ?>
	</table>
	<?php
	}
else
	{
	echo 'Vous n\'avez actuellement aucune quête secondaire en cours. <br />';
	}
	?>


<?php
}
else
{
echo 'Vous devez être connecté pour accéder à cette page';
}
?>
<?php include ("bas.php"); ?>