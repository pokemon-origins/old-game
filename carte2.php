<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 
<div id="text_contenu">


<?php
if(isset($_SESSION['pseudo']))
{
$_SESSION['last_attaque']=0;	
$_SESSION['last_attaque_adv']=0;

$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
$quete_principale=$donnees['quete_principale'];
$quete_principale_etape=$donnees['quete_principale_etape'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_principales WHERE numero=:numero AND etape=:etape') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('numero' => $quete_principale, 'etape' => $quete_principale_etape));  
$donnees = $reponse->fetch();
$id_quete_principale=$donnees['id'];
$reponse = $bdd->prepare('UPDATE pokemons_membres SET map_rapide=1 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 			
	
if(isset($_GET['action']) AND $_GET['action']=="abandon"){$_SESSION['page_combat']="NULL";}
	
	
//remise à zéro des pokémons actif=0
if($_SESSION['page_combat']=="NULL" OR $_SESSION['page_combat']=="")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=2') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 
	while($donnees = $reponse->fetch())
		{
		$reponse2 = $bdd->prepare('UPDATE pokemons_liste_pokemons SET  actif=0, pv=:pv, participe=0, poison=0, poison_grave=0, gel=0, paralyse=0, brule=0, dodo=0, fin_dodo=0  WHERE id=:id') or die(print_r($bdd->errorInfo()));
				  $reponse2->execute(array('pv' => $donnees['pv_max'], 'id' => $donnees['id'])); 
		}
	
	//remise à zéro des pokémons actif=1
	$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 
	while($donnees = $reponse->fetch())
		{
		$reponse2 = $bdd->prepare('UPDATE pokemons_liste_pokemons SET pv=:pv, participe=0, poison=0, poison_grave=0, gel=0, paralyse=0, brule=0, dodo=0, fin_dodo=0  WHERE id=:id') or die(print_r($bdd->errorInfo()));
				  $reponse2->execute(array('pv' => $donnees['pv_max'], 'id' => $donnees['id'])); 
		}
	}

//modification préférence carte
$reponse = $bdd->prepare('UPDATE pokemons_membres SET map_rapide=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 		
	
//remise à 0 de la mine
$_SESSION['mine']=0;

//astuce
if(isset($_GET['action']) AND $_GET['action']=="end_astuces")
	{
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET astuces=1 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 
	$astuces=1;
	}
if(isset($_GET['action']) AND $_GET['action']=="begin_astuces")
	{
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET astuces=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 
	$astuces=0;
	}
if($astuces==0)
	{
	$random=rand(1,37);
	$reponse = $bdd->prepare('SELECT * FROM pokemons_astuces WHERE id=:id') or die(print_r($bdd->errorInfo()));
					 $reponse -> execute(array('id' => $random));
					 $donnees = $reponse->fetch();
	echo '<p style="color:green;padding-left:30px;padding-right:30px;"><b>Astuce n° '.$donnees['id'].'</b> : '.$donnees['text'];
	echo '<br /><a href="carte2.php?action=end_astuces" style="color:black;">Désactiver les astuces</a></p>';
	}
else
	{
	echo '<p style="color:green;padding-left:30px;padding-right:30px;">';
	echo '<a href="carte2.php?action=begin_astuces" style="color:black;">Activer les astuces</a></p>';
	}
	
// SI EN COMBAT
if($_SESSION['page_combat']!="NULL" AND $_SESSION['page_combat']!="")
{
echo '<div style="padding-left:30px;padding-right:30px;">';
echo 'Vous êtes actuellement en ';if($_SESSION['page_combat']=="combat"){echo 'combat contre un pokémon sauvage.';}elseif($_SESSION['page_combat']=="quete_combat"){echo 'duel.';}else{echo 'combat.';}echo '<br />';
echo 'Vous ne pouvez pas vous déplacer tant que vous êtes en combat. Terminez votre combat avant de pouvoir accéder à la carte.<br />';
echo 'Si vous désirez toutefois accéder à la carte, vous pouvez suivre <a href="carte.php?action=abandon">ce lien</a>. Votre combat sera alors considéré comme abandonné.<br />';
if ($_SESSION['page_combat']=="combat"){echo 'Vous pouvez retourner à votre combat via <a href="combat.php">ce lien</a>.<br />';}
if ($_SESSION['page_combat']=="quete_combat"){echo 'Vous pouvez retourner à votre combat via <a href="quetes_combat.php">ce lien</a>.<br />';}

}
else //SI PAS EN COMBAT
{
//se déplacer
if(isset($_GET['horizontal'])){
    $reponse = $bdd->prepare('SELECT * FROM pokemons_carte_tableau WHERE hor=:hor AND ver=:ver') or die(print_r($bdd->errorInfo()));
    $reponse->execute(array('hor' => $_GET['horizontal'], 'ver' => $_GET['vertical'])); 
    $donnees = $reponse->fetch();
    $col=$donnees['ver'];$lig=$donnees['hor'];
    $permission=$donnees['permission'];
    if($permission!=1000){
        $reponse = $bdd->prepare('SELECT numero, etape FROM pokemons_quetes_principales WHERE id=:id') or die(print_r($bdd->errorInfo()));
        $reponse->execute(array('id' => $permission)); 
        $donnees = $reponse->fetch();
        $permission_q=$donnees['numero'];
        $permission_e=$donnees['etape'];
        if($quete_principale>$permission_q OR ($quete_principale==$permission_q AND $quete_principale_etape>=$permission_e)){
            $reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 
            $donnees = $reponse->fetch();
            $pos_ver=$donnees['pos_ver'];
            $pos_hor=$donnees['pos_hor'];		   
            $perm=1;
            if($permission<=$id_quete_principale){
                while($col!=$pos_ver AND $perm==1 OR $lig!=$pos_hor AND $perm==1){
                    $perm_col=1;$perm_lig=1;
                    
                    if($col>$pos_ver){$col_sav=$col-1;}elseif($col<$pos_ver){$col_sav=$col+1;}if($col==$pos_ver){$perm_col=0;}                   
                    if($perm_col!=0){$reponse5 = $bdd->prepare('SELECT permission FROM pokemons_carte_tableau WHERE hor=:hor AND ver=:ver') or die(print_r($bdd->errorInfo()));$reponse5->execute(array('hor' => $lig, 'ver' => $col_sav));  $donnees5 = $reponse5->fetch();		   
                    $reponse2 = $bdd->prepare('SELECT numero, etape FROM pokemons_quetes_principales WHERE id=:id') or die(print_r($bdd->errorInfo()));
                    $reponse2->execute(array('id' => $donnees5['permission'])); 
                    $donnees2 = $reponse2->fetch();
                    $permission_q=$donnees2['numero'];
                    $permission_e=$donnees2['etape'];
                    $permission_quete=0;
                    if($quete_principale>$permission_q OR ($quete_principale==$permission_q AND $quete_principale_etape>=$permission_e) OR $donnees5['permission']==0){if($donnees5['permission']!=1000){$permission_quete=1;}}
                    if($permission_quete==1){$col=$col_sav;}else{$perm_col=0;}}
                    
                    if($lig>$pos_hor){$lig_sav=$lig-1;}elseif($lig<$pos_hor){$lig_sav=$lig+1;}if($lig==$pos_hor){$perm_lig=0;}
                    if($perm_lig!=0){$reponse5 = $bdd->prepare('SELECT permission FROM pokemons_carte_tableau WHERE hor=:hor AND ver=:ver') or die(print_r($bdd->errorInfo()));$reponse5->execute(array('hor' => $lig_sav, 'ver' => $col));  $donnees5 = $reponse5->fetch();		   
                    $reponse2 = $bdd->prepare('SELECT numero, etape FROM pokemons_quetes_principales WHERE id=:id') or die(print_r($bdd->errorInfo()));
                    $reponse2->execute(array('id' => $donnees5['permission'])); 
                    $donnees2 = $reponse2->fetch();
                    $permission_q=$donnees2['numero'];
                    $permission_e=$donnees2['etape'];
                    $permission_quete=0;
                    if($quete_principale>$permission_q OR ($quete_principale==$permission_q AND $quete_principale_etape>=$permission_e) OR $donnees5['permission']==0){if($donnees5['permission']!=1000){$permission_quete=1;}}
                    if($permission_quete==1){$lig=$lig_sav;}else{$perm_lig=0;}}
                    
                    if($perm_lig==0 AND $perm_col==0){$perm=0;}
                }
            }else{
                echo '<span style="padding-left:30px;color:red;text-align:center;">Vous devez avoir terminé la quête n° '.$permission.' pour passer par ici!<br /></span>';
            }
            if ($col==$pos_ver AND $lig==$pos_hor){
                $reponse = $bdd->prepare('UPDATE pokemons_membres SET pos_hor=:pos_hor, pos_ver=:pos_ver WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
                $reponse->execute(array('pos_hor' => $_GET['horizontal'], 'pos_ver' => $_GET['vertical'], 'pseudo' => $_SESSION['pseudo'])); 
            }
        else{echo '<span style="padding-left:200px;color:red;"><b>C\'est mal de tricher avec les liens.</b><br /></span>';}
        }
    else{echo '<span style="padding-left:150px;color:red;text-align:center;"><b>Cette zone ne vous est pas accessible pour l\'instant </b><br /><a href="http://pokemon-origins.forumactif.com/t64-faq#121" target="_blank"><img src="images/interrogation.png" style="border:0;" /></a></span>';}
    }
else{echo '<span style="padding-left:200px;color:red;"><b>Cette zone est infranchissable </b><br /></span>';}
}
?>
<span style="padding-left:200px;color:green;"><b>Déplacement rapide </b></span> (<a style="color:black;" href="carte.php">Passer en mode fouille</a>)<br />
<span style="padding-left:225px;color:blue;">Vous êtes actuellement en (<?php echo $pos_ver.','.$pos_hor;?>)</span>
<div id="game_boy">
<div id="haut_game_boy"> </div>
<div id="fond_game_boy"> 
<div id="battery"> </div>
<table class="table_carte">
<?php //affichage carte
// collonnes et lignes max//
$colonnes_max= 150;
$lignes_max=230; 
  
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 
			  $donnees = $reponse->fetch();
$pos_ver=$donnees['pos_ver'];
$pos_hor=$donnees['pos_hor'];
$quete_principale=$donnees['quete_principale'];
$quete_principale_etape=$donnees['quete_principale_etape'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_carte_tableau WHERE hor=:hor AND ver=:ver') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('hor' => $pos_hor, 'ver' => $pos_ver)); 
$donnees = $reponse->fetch();
$_SESSION['id_map_actuel'] = $donnees['id'];
?>

<?php
//boucle2 = lignes
for ($lignes=$pos_hor-6; $lignes <= $pos_hor +6; $lignes++)
	{
	if ($lignes > 0 AND $lignes <= $lignes_max)
	{
	?> 
    <tr>
        
	   <?php 
	   $ajout=0;
	   if($pos_ver<-250){$ajout=-2-$pos_ver;}
	   for ($colonnes = $pos_ver -8 ; $colonnes <=$pos_ver+8+$ajout; $colonnes++) //boucle 1 = colonnes
           {	
           if ($colonnes > -250 AND $colonnes <= $colonnes_max)
           {	 	   
                $reponse = $bdd->prepare('SELECT id_picture, permission, lieu FROM pokemons_carte_tableau WHERE hor=:hor AND ver=:ver') or die(print_r($bdd->errorInfo()));
	        $reponse->execute(array('hor' => $lignes, 'ver' => $colonnes));  
	        $donnees = $reponse->fetch();
                echo '<td title="('.$colonnes.', '.$lignes.')" style="background-image:url(images/carte/'.$donnees['id_picture'].'.jpg);background-size:cover;text-align:center;';
                $col=$colonnes;$lig=$lignes;$perm=1;
                if($donnees['permission']<=$id_quete_principale)
                    {
                    while($col!=$pos_ver AND $perm==1 OR $lig!=$pos_hor AND $perm==1)
                        {
                        $perm_col=1;$perm_lig=1;
                        if($col>$pos_ver){$col_sav=$col-1;}elseif($col<$pos_ver){$col_sav=$col+1;}
                        if($col==$pos_ver){$perm_col=0;}
                        if($perm_col!=0){$reponse5 = $bdd->prepare('SELECT permission FROM pokemons_carte_tableau WHERE hor=:hor AND ver=:ver') or die(print_r($bdd->errorInfo()));$reponse5->execute(array('hor' => $lig, 'ver' => $col_sav));  $donnees5 = $reponse5->fetch();		   					
                        $reponse2 = $bdd->prepare('SELECT numero, etape FROM pokemons_quetes_principales WHERE id=:id') or die(print_r($bdd->errorInfo()));
                        $reponse2->execute(array('id' => $donnees5['permission'])); 
                        $donnees2 = $reponse2->fetch();
                        $permission_q=$donnees2['numero'];
                        $permission_e=$donnees2['etape'];
                        $permission_quete=0;
                        if($quete_principale>$permission_q OR ($quete_principale==$permission_q AND $quete_principale_etape>=$permission_e) OR $donnees5['permission']==0){if($donnees5['permission']!=1000){$permission_quete=1;}}
                        if($permission_quete==1){$col=$col_sav;}else{$perm_col=0;}}

                        if($lig>$pos_hor){$lig_sav=$lig-1;}elseif($lig<$pos_hor){$lig_sav=$lig+1;}if($lig==$pos_hor){$perm_lig=0;}
                        if($perm_lig!=0){$reponse5 = $bdd->prepare('SELECT permission FROM pokemons_carte_tableau WHERE hor=:hor AND ver=:ver') or die(print_r($bdd->errorInfo()));$reponse5->execute(array('hor' => $lig_sav, 'ver' => $col));  $donnees5 = $reponse5->fetch();		   
                        $reponse2 = $bdd->prepare('SELECT numero, etape FROM pokemons_quetes_principales WHERE id=:id') or die(print_r($bdd->errorInfo()));
                        $reponse2->execute(array('id' => $donnees5['permission'])); 
                        $donnees2 = $reponse2->fetch();
                        $permission_q=$donnees2['numero'];
                        $permission_e=$donnees2['etape'];
                        $permission_quete=0;
                        if($quete_principale>$permission_q OR ($quete_principale==$permission_q AND $quete_principale_etape>=$permission_e) OR $donnees5['permission']==0){if($donnees5['permission']!=1000){$permission_quete=1;}}
                        if($permission_quete==1){$lig=$lig_sav;}else{$perm_lig=0;}}

                        if($perm_lig==0 AND $perm_col==0){$perm=0;}
                        }
                       }else{$col=1000;}
		   if ($col==$pos_ver AND $lig==$pos_hor)  
			  {
			  echo 'cursor:pointer;"';
			  }
		   else {echo '"';}
		   if ($col==$pos_ver AND $lig==$pos_hor)  
             {echo ' onclick="document.location.href=\'?horizontal='.$lignes.'&amp;vertical='.$colonnes.'\';"';}      //se déplacer
           echo ' class="'.$donnees['lieu'].' carte_td">';                                                      //afficher la couleur
		           if ($lignes==$pos_hor AND $colonnes==$pos_ver) {if($sexe_joueur==0){echo '<img src="images/perso.png" height="18px"/>';}if($sexe_joueur==1){echo '<img src="images/perso1.png" height="18px"/>';}} //mettre le perso
		  
		   echo '</td>'; 
           }
		   }
           echo '</tr>';
    }
	}
    ?>  
</table> 

</div>
<div id="bas_game_boy"></div>
</div>

<div id="bouton"> 

<div id="fleche" height=20px width=20px style="position:relative;left:41px;"><a href="carte.php?horizontal=<?php echo $pos_hor-1; ?>&amp;vertical=<?php echo $pos_ver; ?>"><span>.</span></a></div>
<div id="fleche" height=20px width=20px style="position:relative;left:7px;bottom:-10px;"><a href="carte.php?horizontal=<?php echo $pos_hor; ?>&amp;vertical=<?php echo $pos_ver-1; ?>"><span>.</span></a></div>
<div id="fleche" height=20px width=20px style="position:relative;left:78px;bottom:12px;"><a href="carte.php?horizontal=<?php echo $pos_hor; ?>&amp;vertical=<?php echo $pos_ver+1; ?>"><span>.</span></a></div>
<div id="fleche" height=20px width=20px style="position:relative;left:41px;"><a href="carte.php?horizontal=<?php echo $pos_hor+1; ?>&amp;vertical=<?php echo $pos_ver; ?>"><span>.</span></a></div>
</div>






<div id="text">

<div id="affichage_pokemons">
<?php //affichage pokémons
$reponse = $bdd->prepare('SELECT id, id_pokemon, shiney, lvl, sexe, pv, pv_max, objet, monstre FROM pokemons_map WHERE id_map=:id_map AND combat=0') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id_map' => $_SESSION['id_map_actuel']));  
while($donnees = $reponse->fetch())
	{
	$id_pokemon = $donnees['id_pokemon'];
	$shiney = $donnees['shiney'];
	$lvl = $donnees['lvl'];
	$sexe=$donnees['sexe'];
	$pv = $donnees['pv'];
	$pv_max = $donnees['pv_max'];
	$objet=$donnees['objet'];
	$monstre=$donnees['monstre'];
	if($monstre==0)
		{
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $id_pokemon));  
		$donnees2 = $reponse2->fetch();
		$id_pokedex = $donnees2['id_pokedex'];
		$nom = $donnees2['nom'];
		?>
		<div id="cadre_pokemons_carte">
		<div style="margin-left:10px;<?php if($pv<$pv_max){echo 'color:red;';}?>"><b><?php echo $nom; ?></b></div>
		<a href="carte_action.php?id=<?php echo $donnees['id']; ?>"><img src="images/pokemons/<?php if($shiney==1){echo 'shiney/';} ?><?php echo $id_pokedex;?>.gif" height="83px" style="border-style:none; margin-left:4px; "/></a>
		<br />
		<span style="position:relative;bottom:12px;left:8px;font-size:12px;">lvl <?php echo $lvl; ?></span>
		<?php
		if($bonus_racketteur=="ok" AND $objet>0)
			{
			echo'<img src="images/cadeau.png" style="position:relative;bottom:9px;left:7px;">';
			}
		?>
		<?php
		if($lvl <10){echo'<img src="images/';if($sexe=="M"){echo 'male';} elseif($sexe=="F"){echo 'femelle';} else {echo 'nosexe';} echo'.png" style="position:relative;bottom:10px;left:44px;">';}
		else {echo'<img src="images/';if($sexe=="M"){echo 'male';} elseif($sexe=="F"){echo 'femelle';} else {echo 'nosexe';} echo'.png" style="position:relative;bottom:10px;left:'; if($bonus_racketteur=="ok" AND $objet>0){echo 22;}else{echo 36;} echo 'px;">';}
		?>
		</div>
		<?php
		}
	elseif($monstre==1)
		{
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_monstres WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $id_pokemon));  
		$donnees2 = $reponse2->fetch();
		$nom = $donnees2['nom'];
		?>
		<div id="cadre_pokemons_carte">
		<div style="margin-left:2px;margin-top:3px;font-size:13px;"><b><?php echo $nom; ?></b></div>
		<a href="carte_action.php?id=<?php echo $donnees['id']; ?>"><img src="images/monstres/<?php echo $id_pokemon;?>.png" height="83px" style="border-style:none; margin-left:4px; "/></a>
		<br />
		<span style="position:relative;bottom:8px;left:7px;font-size:12px;">lvl <?php echo $donnees['lvl']; ?></span>
		<?php
		if($bonus_racketteur=="ok" AND $objet>0)
			{
			echo'<img src="images/cadeau.png" style="position:relative;bottom:9px;left:7px;">';
			}
		?>
		<?php
		if($lvl <10){echo'<img src="images/';if($sexe=="M"){echo 'male';} elseif($sexe=="F"){echo 'femelle';} else {echo 'nosexe';} echo'.png" style="position:relative;bottom:10px;left:44px;">';}
		else {echo'<img src="images/';if($sexe=="M"){echo 'male';} elseif($sexe=="F"){echo 'femelle';} else {echo 'nosexe';} echo'.png" style="position:relative;bottom:10px;left:'; if($bonus_racketteur=="ok" AND $objet>0){echo 22;}else{echo 36;} echo 'px;">';}
		?>
		</div>
		<?php
		}
	}

?>
</div>


<h1 style="margin-top:-25px;clear:both;"></h1>
<?php //affichage joueurs
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pos_hor=:pos_hor AND pos_ver=:pos_ver AND connecte=1 AND pseudo!=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pos_hor' => $pos_hor, 'pos_ver' => $pos_ver, 'pseudo' => $_SESSION['pseudo']));  
while($donnees = $reponse->fetch())
	{
	$sexe_joueur_carte=$donnees['sexe'];
	$pseudo_carte=$donnees['pseudo'];
	$id_carte=$donnees['id'];
	$chemin = 'images/avatars/'.$id_carte.'.jpg';
	?>
	<a href="profil.php?profil=<?php echo $pseudo_carte; ?>" style="color:black;">
	<div id="cadre_membres_carte" style="text-align:center;">
	<?php
	if(file_exists($chemin))
		{echo '<img src="images/avatars/'.$id_carte.'.jpg" style="border:0;"/><br />';}
	else 
		{
		if($sexe_joueur_carte==0){echo '<img src="images/avatars/auto.gif" style="border:0;"/><br />';}
		if($sexe_joueur_carte==1){echo '<img src="images/avatars/auto1.gif" style="border:0;"/><br />';}
		}
	?>
	<div style="text-align:center;"><b><?php echo $pseudo_carte; ?></b></div>
	</div>
	</a>
	<?php
	}
?>




<h1 style="margin-top:-25px;clear:both;"></h1>
<?php //affichage quêtes / quêtes secondaires /pnj
$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_principales WHERE pos_hor=:pos_hor AND pos_ver=:pos_ver') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pos_hor' => $pos_hor, 'pos_ver' => $pos_ver));  
while($donnees = $reponse->fetch())
	{//quetes principales actuelle
	if($quete_principale==$donnees['numero'] AND $quete_principale_etape==$donnees['etape'])
		{
		$id_quete_principale=$donnees['id'];
		$chemin = 'images/quetes_principales/'.$id_quete_principale.'.jpg';
		?>
		<a href="quetes_principales.php?id=<?php echo $id_quete_principale; ?>">
		<div id="cadre_membres_carte">
		<img src="images/quetes_principales/<?php echo $id_quete_principale; ?>.jpg" height="150px" style="border:0;"/>
		</div>
		</a>
		<?php
		}
	}


$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_secondaires WHERE pos_hor=:pos_hor AND pos_ver=:pos_ver') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pos_hor' => $pos_hor, 'pos_ver' => $pos_ver));  
while($donnees = $reponse->fetch())
	{//quetes secondaires
	$id_quete_secondaire=$donnees['id'];
	$limite_inf=$donnees['limite_inf'];
	$limite_sup=$donnees['limite_sup'];
	if($limite_inf <= $id_quete_actuelle AND $limite_sup >= $id_quete_actuelle)
		{
		$chemin = 'images/quetes_secondaires/'.$id_quete_secondaire.'.jpg';
		?>
		<a href="quetes_secondaires.php?id=<?php echo $id_quete_secondaire; ?>">
		<div id="cadre_membres_carte">
		<img src="images/quetes_secondaires/<?php echo $id_quete_secondaire; ?>.jpg" height="150px" style="border:0;"/>
		</div>
		</a>
		<?php
		}
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_pnj WHERE pos_hor=:pos_hor AND pos_ver=:pos_ver') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pos_hor' => $pos_hor, 'pos_ver' => $pos_ver));  
while($donnees = $reponse->fetch())
	{//PNJ
	$id_pnj=$donnees['id'];
	$limite_inf=$donnees['limite_inf'];
	$limite_sup=$donnees['limite_sup'];
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('pseudo' => $_SESSION['pseudo']));
	$donnees2 = $reponse2->fetch();
	$quete_principale=$donnees2['quete_principale'];
	if($id_quete_actuelle<=$limite_sup AND $id_quete_actuelle>=$limite_inf)
		{
		$chemin = 'images/quetes_secondaires/'.$id_quete_secondaire.'.jpg';
		?>
		<a href="pnj.php?id=<?php echo $id_pnj; ?>">
		<div id="cadre_membres_carte">
		<img src="images/pnj/<?php echo $id_pnj; ?>.jpg" height="150px" style="border:0;"/>
		</div>
		</a>
		<?php
		}
	}
echo'<span style="clear:both;"></span>';

?>

<h1 style="margin-top:-25px;clear:both;"></h1>



<?php
}
}
else
{
echo '<div id="text">Vous devez vous connecter pour avoir accès à la carte.';
}		
?>






	
<?php include ("bas.php"); ?>