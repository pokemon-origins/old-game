﻿<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">

<div width="550px" style="text-align:center;color:#6495ed;margin-top:-15px;"><h1>Bienvenue sur Pokémon Origins</h1></div>

<b>Actualité du jeu : </b>
<?php //actualité
$reponse = $bdd->query('SELECT * FROM pokemons_news ORDER BY id DESC LIMIT 0,1') or die(print_r($bdd->errorInfo()));
while ($donnees=$reponse->fetch())
	{
	echo '<table width="550px" style="text-align:left;"> <tr> <th>'.$donnees['titre']. '     <span style="font-size:0.8em;">(posté le '.$donnees['date_poste'].')</span></td></th>';
	echo '<tr> <td>'.nl2br($donnees['news']).'</tr> </td> </table>';
	}	    	   	   
?>
<br />Retrouver <a href="http://pokemon-origins.forumactif.com/t129-developpement" style="color:black;" target="_blank"><b>toutes les améliorations </b></a>apportées au site en temps réel<br />
Afficher <a href="pokemons_news.php" style="color:black;"> <b>Toutes les news</b> </a><br /><br />


<img src="images/index_carte.jpg" width="300px" style="float:left; margin-right:5px;" /> <p style="padding-left:5px;text-align:justify;">Pokémon Origins est un jeu de rôle massivement multijoueurs reprenant l'univers de Pokémon. A l'image de pokémon version rouge et bleu, vous y incarnez un jeune dresseur du Bourg Palette ayant pour objectif de devenir un grand maître pokémon. <br /> Les 3 premières générations de pokémon sont présentes sur le jeu !</p>
<div style="clear:both;"></div>
<img src="images/index_combat.jpg" width="300px" style="float:right; margin-left:5px;margin-top:19px;" /> <p style="padding-left:5px;text-align:justify;"> Combattez des pokémons sauvages et d'autres dresseurs. Accumulez les victoires. Capturez et collectionnez les pokémons les plus rares. Participer à de nombreuses quêtes et redécouvrez l'univers de pokémon à travers un scénario inédit. </p>
<div style="clear:both;"></div>
<img src="images/chen.jpg" width="90px" style="float:left; margin-right:20px;" /> <p style="padding-left:5px;text-align:justify;"><p><i>"Pas encore inscrit? <a href="inscription.php" style="color:black;">L'inscription </a>ne prend que quelques minutes et est entièrement gratuite. Rejoins-nous!"</i></p>
<div style="text-align:center;"> <a href="inscription.php"><img src="images/index_inscription.png" width="150px" style="margin-top:-20px;margin-left:-70px; border:none;" /></a></div>
<div style="clear:both;"></div>

<br />

<div style="float:left;">
<table id="profil" width="265px" cellpadding="2" cellspacing="2" style="text-align:center;" >
<colgroup><COL WIDTH=45%><COL WIDTH=20%><COL WIDTH=35%></COLGROUP>
<tr><th colspan="9">Classement des joueurs</th></tr>
<tr><td><b>Pseudo</b></td><td><b>Score </b></td><td><b>Nombre de pokémons </b></td></tr>
<?php //classement joueurs
$num=0;
$reponse = $bdd->query('SELECT * FROM pokemons_membres ORDER BY score_total DESC LIMIT 0,5') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	$nb_pokemons_joueur=0;
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('pseudo' => $donnees['pseudo']));
	while($donnees2 = $reponse2->fetch())
	{
	$nb_pokemons_joueur=$nb_pokemons_joueur+1;
	}
	$num=$num+1;
	?>
	<tr><td><a style="color:black;" href="profil.php?profil=<?php echo $donnees['pseudo'];?>"><?php echo $donnees['pseudo']; ?></a></td><td><?php echo $donnees['score_total']; ?></td><td><?php echo $nb_pokemons_joueur; ?></td></tr>
	
	<?php
	}
?>
</table>
</div>


<div>
<table id="profil" width="265px" cellpadding="2" cellspacing="2" style="text-align:center;" >
<colgroup><COL WIDTH=45%><COL WIDTH=20%><COL WIDTH=35%></COLGROUP>
<tr><th colspan="9">Classement PvP</th></tr>
<tr><td><b>Pseudo</b></td><td><b>Score </b></td><td><b>Nombre de pokémons </b></td></tr>
<?php //classement joueurs
$num=0;
$reponse = $bdd->query('SELECT pseudo, score_pvp FROM pokemons_membres ORDER BY score_pvp DESC LIMIT 0,5') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	$nb_pokemons_joueur=0;
	$reponse2 = $bdd->prepare('SELECT id FROM pokemons_liste_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('pseudo' => $donnees['pseudo']));
	while($donnees2 = $reponse2->fetch())
	{
	$nb_pokemons_joueur=$nb_pokemons_joueur+1;
	}
	$num=$num+1;
	?>
	<tr><td><a style="color:black;" href="profil.php?profil=<?php echo $donnees['pseudo'];?>"><?php echo $donnees['pseudo']; ?></a></td><td><?php echo $donnees['score_pvp']; ?></td><td><?php echo $nb_pokemons_joueur; ?></td></tr>
	
	<?php
	}
?>
</table>
</div>
<div style="clear:both;"></div>
<div style="margin-left:130px;">
<table id="profil" width="265px" cellpadding="2" cellspacing="2" style="text-align:center;" >
<colgroup><COL WIDTH=50%><COL WIDTH=20%><COL WIDTH=25%></COLGROUP>
<tr><th colspan="9">Classement des pokémons</th></tr>
<tr><td><b>Nom</b></td><td><b>Score </b></td><td><b>Nom du propriétaire </b></td></tr>
<?php //classement pokémons
$num=0;
$reponse = $bdd->query('SELECT id_pokemon, score, pseudo FROM pokemons_liste_pokemons ORDER BY score DESC LIMIT 0,5') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	$reponse2 = $bdd->prepare('SELECT nom FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['id_pokemon']));
	$donnees2 = $reponse2->fetch();
	$num=$num+1;
	?>
	<tr <?php if($donnees['shiney']==1){echo 'style="background-color:lightblue;"';} ?>> <td><?php echo $donnees2['nom']; ?></td><td><?php echo $donnees['score']; ?></td><td><a style="color:black;" href="profil.php?profil=<?php echo $donnees['pseudo'];?>"><?php echo $donnees['pseudo']; ?></a></td></tr>
	
	<?php
	}
?>
</table>
</div>



<?php include ("bas.php"); ?>