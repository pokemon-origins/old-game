<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">


<?php
if(isset($_SESSION['pseudo']))
{
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
$pos_hor=$donnees['pos_hor'];
$pos_ver=$donnees['pos_ver'];
$quete_principale=$donnees['quete_principale'];
$quete_principale_etape=$donnees['quete_principale_etape'];
$quete_principale_active=$donnees['quete_principale_active'];
$argent_dispo=$donnees['pokedollar'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_principales WHERE numero=:numero AND etape=:etape') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('numero' => $quete_principale, 'etape' => $quete_principale_etape));  
$donnees = $reponse->fetch();
$id_quete_principale=$donnees['id'];
if($id_quete_actuelle >7 OR $id_quete_actuelle==7 AND $quete_principale_active_membre==1)
{
?>


<?php //nettoyage des vieilles ventes

$time=time();
$time_deleted=$time-864000;
$reponseX = $bdd->query('SELECT id, quand FROM pokemons_marche_pokemons') or die(print_r($bdd->errorInfo()));
while($donneesX = $reponseX->fetch())
	{
	if($donneesX['quand']<$time_deleted)
		{
		$reponse = $bdd->prepare('SELECT * FROM pokemons_marche_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $donneesX['id']));
		$donnees = $reponse->fetch();
		$reponse3 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse3->execute(array('id' => $donnees['id_pokemon']));
						$donnees3 = $reponse3->fetch();
		$nom_du_pokemon_vendu=$donnees3['nom'];
		//renvoyer le pokemon chez lui
		$req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons (pseudo, id_pokemon, shiney, sexe, lvl, xp, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, actif, pa_restant, pa_max, pa_bonus, victoires, defaites, score, fin_dodo, parent_1, parent_2, bonheur, objet) VALUES(:pseudo, :id_pokemon, :shiney, :sexe, :lvl, :xp, :pv, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, 0, :pa_restant, :pa_max, :pa_bonus, :victoires, :defaites, :score, :fin_dodo, :parent_1, :parent_2, :bonheur, :objet)') or die(print_r($bdd->errorInfo()));
								$req->execute(array(
										'pseudo' => $donnees['pseudo'], 
										'id_pokemon' => $donnees['id_pokemon'],
										'shiney' => $donnees['shiney'],
										'sexe' => $donnees['sexe'],
										'lvl' => $donnees['lvl'],
										'xp' => $donnees['xp'],
										'pv' => $donnees['pv_max'],
										'pv_max' => $donnees['pv_max'],
										'att' => $donnees['att'],
										'def' => $donnees['def'],
										'vit' => $donnees['vit'],
										'attspe' => $donnees['attspe'],
										'defspe' => $donnees['defspe'],
										'attaque1' => $donnees['attaque1'],
										'attaque2' => $donnees['attaque2'],
										'attaque3' => $donnees['attaque3'],
										'attaque4' => $donnees['attaque4'],
										'pa_restant' => $donnees['pa_restant'],
										'pa_max' => $donnees['pa_max'],
										'pa_bonus' => $donnees['pa_bonus'],
										'victoires' => $donnees['victoires'],
										'defaites' => $donnees['defaites'],
										'score' => $donnees['score'],
										'fin_dodo' => $donnees['fin_dodo'],
										'parent_1' => $donnees['parent_1'],
										'parent_2' => $donnees['parent_2'],
										'bonheur' => $donnees['bonheur'],
										'objet' => $donnees['objet']
										))or die(print_r($bdd->errorInfo()));
		//delete du marche
		$reponse = $bdd->prepare('DELETE FROM pokemons_marche_pokemons WHERE id=:id ') or die(print_r($bdd->errorInfo()));
				  $reponse->execute(array('id' => $donneesX['id'])); 				
		//mp au joueur
		$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("marchand", :destinataire, "non lu", "Pokémon non vendu", :message, now())') or die(print_r($bdd->errorInfo()));
					$req->execute(array(
						'destinataire' => $donnees['pseudo'],					
						'message' => 'Désolé, votre pokémon "'.$nom_du_pokemon_vendu.'" lvl'.$donnees['lvl'].' n\'a pas été vendu en 10 jours. Il est de retour dans votre liste de pokémon. Vous pouvez le remettre en vente si vous le désirez.'
						))
						or die(print_r($bdd->errorInfo()));								
		}
	}
?>



<?php 
//compte des marchandises à vendre
	//pokemons


$reponse = $bdd->query('SELECT COUNT(id) AS nb_pokemons_a_vendre FROM pokemons_marche_pokemons') or die(print_r($bdd->errorInfo()));
$donnees = $reponse->fetch();
$nb_pokemons_a_vendre=$donnees['nb_pokemons_a_vendre'];

$reponse = $bdd->prepare('SELECT COUNT(id) AS nb_pokemons_a_vendre_par_joueur FROM pokemons_marche_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
$nb_pokemons_a_vendre_par_joueur=$donnees['nb_pokemons_a_vendre_par_joueur'];

$reponse = $bdd->query('SELECT COUNT(id) AS nb_items_a_vendre FROM pokemons_marche_items') or die(print_r($bdd->errorInfo()));
$donnees = $reponse->fetch();
$nb_items_a_vendre=$donnees['nb_items_a_vendre'];

$reponse = $bdd->prepare('SELECT COUNT(id) AS nb_items_a_vendre_par_joueur FROM pokemons_marche_items WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
$nb_items_a_vendre_par_joueur=$donnees['nb_items_a_vendre_par_joueur'];

$reponse = $bdd->query('SELECT COUNT(id) AS nb_or_a_vendre FROM pokemons_marche_or') or die(print_r($bdd->errorInfo()));
$donnees = $reponse->fetch();
$nb_or_a_vendre=$donnees['nb_or_a_vendre'];

$reponse = $bdd->prepare('SELECT COUNT(id) AS nb_or_a_vendre_par_joueur FROM pokemons_marche_or WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
$nb_or_a_vendre_par_joueur=$donnees['nb_or_a_vendre_par_joueur'];


?>


<table>
<tr>
<td>
<form action="marche.php" method="post">
<input type="hidden" name="marchandises" value="pokemons"/>
<input type="submit" value="Voir le marché des pokémons"/>	  
</form>
</td><td>
<form action="marche.php" method="post">
<input type="hidden" name="marchandises" value="items"/>
<input type="submit" value="Voir le marché des objets"/>	  
</form>
</td><td>
<form action="marche.php" method="post">
<input type="hidden" name="marchandises" value="or"/>
<input type="submit" value="Voir le marché de l'or"/>	  
</form>
</td></tr>
</table>
<br />





<?php //marché des pokémons
if($_POST['marchandises']=="pokemons")
{
if($_POST['action']=="recuperer")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
		$donnees = $reponse->fetch();
	$stockage_max=$donnees['bds'];
	$reponse = $bdd->prepare('SELECT * FROM pokemons_marche_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_POST['id']));
		$donnees = $reponse->fetch();
	$pv_max_pokemon=$donnees['pv_max'];$att_pokemon=$donnees['att'];$def_pokemon=$donnees['def'];
	$vit_pokemon=$donnees['vit'];$attspe_pokemon=$donnees['attspe'];$defspe_pokemon=$donnees['defspe'];
	$id_pokemon=$donnees['id_pokemon'];
	if(isset($donnees['id']) AND $_SESSION['pseudo']==$donnees['pseudo'])
		{
		if($nb_pokemons_joueur<$stockage_max)
			{			
			$req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons (pseudo, id_pokemon, shiney, sexe, lvl, xp, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, actif, pa_restant, pa_max, pa_bonus, victoires, defaites, score, fin_dodo, parent_1, parent_2, bonheur, objet) VALUES(:pseudo, :id_pokemon, :shiney, :sexe, :lvl, :xp, :pv, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, 0, :pa_restant, :pa_max, :pa_bonus, :victoires, :defaites, :score, :fin_dodo, :parent_1, :parent_2, :bonheur, :objet)') or die(print_r($bdd->errorInfo()));
								$req->execute(array(
										'pseudo' => $_SESSION['pseudo'], 
										'id_pokemon' => $id_pokemon,
										'shiney' => $donnees['shiney'],
										'sexe' => $donnees['sexe'],
										'lvl' => $donnees['lvl'],
										'xp' => $donnees['xp'],
										'pv' => $pv_max_pokemon,
										'pv_max' => $pv_max_pokemon,
										'att' => $att_pokemon,
										'def' => $def_pokemon,
										'vit' => $vit_pokemon,
										'attspe' => $attspe_pokemon,
										'defspe' => $defspe_pokemon,
										'attaque1' => $donnees['attaque1'],
										'attaque2' => $donnees['attaque2'],
										'attaque3' => $donnees['attaque3'],
										'attaque4' => $donnees['attaque4'],
										'pa_restant' => $donnees['pa_restant'],
										'pa_max' => $donnees['pa_max'],
										'pa_bonus' => $donnees['pa_bonus'],
										'victoires' => $donnees['victoires'],
										'defaites' => $donnees['defaites'],
										'score' => $donnees['score'],
										'fin_dodo' => $donnees['fin_dodo'],
										'parent_1' => $donnees['parent_1'],
										'parent_2' => $donnees['parent_2'],
										'bonheur' => $donnees['bonheur'],
										'objet' => $donnees['objet']
										))or die(print_r($bdd->errorInfo()));
				$reponse = $bdd->prepare('DELETE FROM pokemons_marche_pokemons WHERE id=:id ') or die(print_r($bdd->errorInfo()));
				  $reponse->execute(array('id' => $_POST['id'])); 				
				echo '<b>Vous avez bien récupéré votre pokémon.</b><br /><br />';
			}
		else{echo '<b>Vous devez aggrandir votre boite de stockage pour pouvoir avoir plus de pokémons.</b><br /><br />';}
		}
	else{echo '<b>Ce pokémon n\'est plus à vendre ou il ne vous appartient pas. </b><br /><br />';}
	}
if($_POST['action']=="acheter")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
		$donnees = $reponse->fetch();
	$pokedollar=$donnees['pokedollar'];
	$ors=$donnees['ors'];
	$stockage_max=$donnees['bds'];
	$reponse = $bdd->prepare('SELECT * FROM pokemons_marche_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_POST['id']));
		$donnees = $reponse->fetch();
	$pv_max_pokemon=$donnees['pv_max'];$att_pokemon=$donnees['att'];$def_pokemon=$donnees['def'];
	$vit_pokemon=$donnees['vit'];$attspe_pokemon=$donnees['attspe'];$defspe_pokemon=$donnees['defspe'];
	$id_pokemon=$donnees['id_pokemon'];$pseudo_vendeur=$donnees['pseudo'];$devise=$donnees['devise'];
	if(isset($donnees['id']))
		{
		if($nb_pokemons_joueur<$stockage_max)
			{
			if($devise==0)
				{
				if($pokedollar>=$donnees['prix'] AND $_SESSION['pseudo']!=$pseudo_vendeur)
					{
					$prix=$donnees['prix'];
					$depenses_marche=$depenses_marche+$prix;
					$argent_restant=$pokedollar-$prix;
					$reponse3 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse3->execute(array('id' => $donnees['id_pokemon']));
						$donnees3 = $reponse3->fetch();
					$nom_du_pokemon_vendu=$donnees3['nom'];
					if($donnees['lvl']==0){$nom_du_pokemon_vendu= "oeuf";}
					if($donnees3['lvl_evo']==100 OR $nom_du_pokemon_vendu=="insecateur" AND $donnees['objet']==102 OR $nom_du_pokemon_vendu=="onix" AND $donnees['objet']==102 OR $nom_du_pokemon_vendu=="ramoloss" AND $donnees['objet']==123 OR $nom_du_pokemon_vendu=="tetarte" AND $donnees['objet']==123 OR $nom_du_pokemon_vendu=="hypocéan" AND $donnees['objet']==124 OR $nom_du_pokemon_vendu=="porygon" AND $donnees['objet']==125 OR $nom_du_pokemon_vendu=="barpau" AND $donnees['objet']==159 OR $nom_du_pokemon_vendu=="coquiperl" AND $donnees['objet']==160 OR $nom_du_pokemon_vendu=="coquiperl" AND $donnees['objet']==161) //évolue pendant échange 100(rien) 102 (peau métal) 123 (roche royale) 124 (Ecaille dragon) 125(ameliorator)159 (bel'ecaille) 160 (écaille océan) 161 (dent océan)
						{
						if($donnees3['lvl_evo']!=100) //evolue par objet
							{ 
                                                        if($nom_du_pokemon_vendu=="coquiperl" AND $donnees['objet']==160){$nom_evo="rosabyss";}
                                                        if($nom_du_pokemon_vendu=="coquiperl" AND $donnees['objet']==161){$nom_evo="serpang";}
							$donnees['objet']=0;
							if($nom_du_pokemon_vendu=="insecateur"){$nom_evo="cizayox";}
							if($nom_du_pokemon_vendu=="onix"){$nom_evo="steelix";}
							if($nom_du_pokemon_vendu=="tetarte"){$nom_evo="tarpaud";}
							if($nom_du_pokemon_vendu=="ramoloss"){$nom_evo="roigada";}
							if($nom_du_pokemon_vendu=="hypocéan"){$nom_evo="hyporoi";}
							if($nom_du_pokemon_vendu=="porygon"){$nom_evo="porygon2";}
                                                        if($nom_du_pokemon_vendu=="barpau"){$nom_evo="milobellus";}
							}
						else{$nom_evo=$donnees3['evo'];}
						$pv=$donnees3['pv'];$att=$donnees3['att'];$def=$donnees3['def'];$vit=$donnees3['vit'];$attspe=$donnees3['attspe'];$defspe=$donnees3['defspe'];
						$reponse4 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE nom=:nom') or die(print_r($bdd->errorInfo()));
						$reponse4->execute(array('nom' => $nom_evo));
						$donnees4 = $reponse4->fetch();
						$pv2=$donnees4['pv'];
						$att2=$donnees4['att'];$def2=$donnees4['def'];$vit2=$donnees4['vit'];$attspe2=$donnees4['attspe'];$defspe2=$donnees4['defspe'];
						$dif_pv=$pv2-$pv;$dif_att=$att2-$att;$dif_def=$def2-$def;$dif_vit=$vit2-$vit;$dif_attspe=$attspe2-$attspe;$dif_defspe=$defspe2-$defspe;
						$pv_max_pokemon=$donnees['pv_max']+$dif_pv;$att_pokemon=$donnees['att']+$dif_att;$def_pokemon=$donnees['def']+$dif_def;
						$vit_pokemon=$donnees['vit']+$dif_vit;$attspe_pokemon=$donnees['attspe']+$dif_attspe;$defspe_pokemon=$donnees['defspe']+$dif_defspe;
						$id_pokemon=$donnees4['id'];
						$evolution=1;
						}
					$req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons (pseudo, id_pokemon, shiney, sexe, lvl, xp, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, actif, pa_restant, pa_max, pa_bonus, victoires, defaites, score, fin_dodo, parent_1, parent_2, bonheur, objet) VALUES(:pseudo, :id_pokemon, :shiney, :sexe, :lvl, :xp, :pv, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, 0, :pa_restant, :pa_max, :pa_bonus, :victoires, :defaites, :score, :fin_dodo, :parent_1, :parent_2, :bonheur, :objet)') or die(print_r($bdd->errorInfo()));
									$req->execute(array(
											'pseudo' => $_SESSION['pseudo'], 
											'id_pokemon' => $id_pokemon,
											'shiney' => $donnees['shiney'],
											'sexe' => $donnees['sexe'],
											'lvl' => $donnees['lvl'],
											'xp' => $donnees['xp'],
											'pv' => $pv_max_pokemon,
											'pv_max' => $pv_max_pokemon,
											'att' => $att_pokemon,
											'def' => $def_pokemon,
											'vit' => $vit_pokemon,
											'attspe' => $attspe_pokemon,
											'defspe' => $defspe_pokemon,
											'attaque1' => $donnees['attaque1'],
											'attaque2' => $donnees['attaque2'],
											'attaque3' => $donnees['attaque3'],
											'attaque4' => $donnees['attaque4'],
											'pa_restant' => $donnees['pa_restant'],
											'pa_max' => $donnees['pa_max'],
											'pa_bonus' => $donnees['pa_bonus'],
											'victoires' => $donnees['victoires'],
											'defaites' => $donnees['defaites'],
											'score' => $donnees['score'],
											'fin_dodo' => $donnees['fin_dodo'],
											'parent_1' => $donnees['parent_1'],
											'parent_2' => $donnees['parent_2'],
											'bonheur' => $donnees['bonheur'],
											'objet' => $donnees['objet']
											))or die(print_r($bdd->errorInfo()));
					$reponse = $bdd->prepare('DELETE FROM pokemons_marche_pokemons WHERE id=:id ') or die(print_r($bdd->errorInfo()));
					  $reponse->execute(array('id' => $_POST['id'])); 
					$reponse = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar, depenses_marche=:depenses_marche WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
									$reponse->execute(array('pokedollar' =>$argent_restant , 'depenses_marche'=> $depenses_marche, 'pseudo' => $_SESSION['pseudo'])); 	
					$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('pseudo' => $donnees['pseudo']));
						$donnees = $reponse->fetch();
					$pokedollar_vendeur=$donnees['pokedollar'];	
					$argent_vendeur=$pokedollar_vendeur+$prix;
					$reponse2 = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
									$reponse2->execute(array('pokedollar' =>$argent_vendeur ,'pseudo' => $donnees['pseudo'])); 	
					$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("marchand", :destinataire, "non lu", "Vente réussie!", :message, now())') or die(print_r($bdd->errorInfo()));
					$req->execute(array(
						'destinataire' => $donnees['pseudo'],					
						'message' => 'félicitation, votre pokémon "'.$nom_du_pokemon_vendu.'" a été vendu au prix de '.$prix.' $ à '.$_SESSION['pseudo'].'.'
						))
						or die(print_r($bdd->errorInfo()));					
					echo '<b>Vous avez bien acheté le pokémon.</b><br />'; if($evolution==1){echo '<b>Oh! Il a évolué durant la transaction!!</b><br/>';} echo'<br />';
					}
				else
					{
					if($_SESSION['pseudo']==$pseudo_vendeur)
						{echo '<b>Vous ne pouvez pas vous acheter un pokémon à vous-même!</b><br /><br />';}
					else
						{echo '<b>Vous n\'avez pas suffisamment d\'argent pour acheter ce pokémon. </b><br /><br />';}
					}
				}
			elseif($devise==1)
				{
				if($ors>=$donnees['prix'] AND $_SESSION['pseudo']!=$pseudo_vendeur)
					{
					$prix=$donnees['prix'];
					$depenses_marche=$depenses_marche+$prix;
					$argent_restant=$ors-$prix;
					$reponse3 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse3->execute(array('id' => $donnees['id_pokemon']));
						$donnees3 = $reponse3->fetch();
					$nom_du_pokemon_vendu=$donnees3['nom'];
					if($donnees['lvl']==0){$nom_du_pokemon_vendu= "oeuf";}
					if($donnees3['lvl_evo']==100 OR $nom_du_pokemon_vendu=="insecateur" AND $donnees['objet']==102 OR $nom_du_pokemon_vendu=="onix" AND $donnees['objet']==102 OR $nom_du_pokemon_vendu=="ramoloss" AND $donnees['objet']==123 OR $nom_du_pokemon_vendu=="tetarte" AND $donnees['objet']==123 OR $nom_du_pokemon_vendu=="hypocéan" AND $donnees['objet']==124 OR $nom_du_pokemon_vendu=="porygon" AND $donnees['objet']==125 OR $nom_du_pokemon_vendu=="barpau" AND $donnees['objet']==159 OR $nom_du_pokemon_vendu=="coquiperl" AND $donnees['objet']==160 OR $nom_du_pokemon_vendu=="coquiperl" AND $donnees['objet']==161) //évolue pendant échange 100(rien) 102 (peau métal) 123 (roche royale) 124 (Ecaille dragon) 125(ameliorator)159 (bel'ecaille) 160 (écaille océan) 161 (dent océan)
						{
						if($donnees3['lvl_evo']!=100) //evolue par objet
							{ 
                                                        if($nom_du_pokemon_vendu=="coquiperl" AND $donnees['objet']==160){$nom_evo="rosabyss";}
                                                        if($nom_du_pokemon_vendu=="coquiperl" AND $donnees['objet']==161){$nom_evo="serpang";}
							$donnees['objet']=0;
							if($nom_du_pokemon_vendu=="insecateur"){$nom_evo="cizayox";}
							if($nom_du_pokemon_vendu=="onix"){$nom_evo="steelix";}
							if($nom_du_pokemon_vendu=="tetarte"){$nom_evo="tarpaud";}
							if($nom_du_pokemon_vendu=="ramoloss"){$nom_evo="roigada";}
							if($nom_du_pokemon_vendu=="hypocéan"){$nom_evo="hyporoi";}
							if($nom_du_pokemon_vendu=="porygon"){$nom_evo="porygon2";}
                                                        if($nom_du_pokemon_vendu=="barpau"){$nom_evo="milobellus";}
                                                        
							}
						else{$nom_evo=$donnees3['evo'];}
						$pv=$donnees3['pv'];$att=$donnees3['att'];$def=$donnees3['def'];$vit=$donnees3['vit'];$attspe=$donnees3['attspe'];$defspe=$donnees3['defspe'];
						$reponse4 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE nom=:nom') or die(print_r($bdd->errorInfo()));
						$reponse4->execute(array('nom' => $nom_evo));
						$donnees4 = $reponse4->fetch();
						$pv2=$donnees4['pv'];
						$att2=$donnees4['att'];$def2=$donnees4['def'];$vit2=$donnees4['vit'];$attspe2=$donnees4['attspe'];$defspe2=$donnees4['defspe'];
						$dif_pv=$pv2-$pv;$dif_att=$att2-$att;$dif_def=$def2-$def;$dif_vit=$vit2-$vit;$dif_attspe=$attspe2-$attspe;$dif_defspe=$defspe2-$defspe;
						$pv_max_pokemon=$donnees['pv_max']+$dif_pv;$att_pokemon=$donnees['att']+$dif_att;$def_pokemon=$donnees['def']+$dif_def;
						$vit_pokemon=$donnees['vit']+$dif_vit;$attspe_pokemon=$donnees['attspe']+$dif_attspe;$defspe_pokemon=$donnees['defspe']+$dif_defspe;
						$id_pokemon=$donnees4['id'];
						$evolution=1;
						}
					$req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons (pseudo, id_pokemon, shiney, sexe, lvl, xp, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, actif, pa_restant, pa_max, pa_bonus, victoires, defaites, score, fin_dodo, parent_1, parent_2, bonheur, objet) VALUES(:pseudo, :id_pokemon, :shiney, :sexe, :lvl, :xp, :pv, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, 0, :pa_restant, :pa_max, :pa_bonus, :victoires, :defaites, :score, :fin_dodo, :parent_1, :parent_2, :bonheur, :objet)') or die(print_r($bdd->errorInfo()));
									$req->execute(array(
											'pseudo' => $_SESSION['pseudo'], 
											'id_pokemon' => $id_pokemon,
											'shiney' => $donnees['shiney'],
											'sexe' => $donnees['sexe'],
											'lvl' => $donnees['lvl'],
											'xp' => $donnees['xp'],
											'pv' => $pv_max_pokemon,
											'pv_max' => $pv_max_pokemon,
											'att' => $att_pokemon,
											'def' => $def_pokemon,
											'vit' => $vit_pokemon,
											'attspe' => $attspe_pokemon,
											'defspe' => $defspe_pokemon,
											'attaque1' => $donnees['attaque1'],
											'attaque2' => $donnees['attaque2'],
											'attaque3' => $donnees['attaque3'],
											'attaque4' => $donnees['attaque4'],
											'pa_restant' => $donnees['pa_restant'],
											'pa_max' => $donnees['pa_max'],
											'pa_bonus' => $donnees['pa_bonus'],
											'victoires' => $donnees['victoires'],
											'defaites' => $donnees['defaites'],
											'score' => $donnees['score'],
											'fin_dodo' => $donnees['fin_dodo'],
											'parent_1' => $donnees['parent_1'],
											'parent_2' => $donnees['parent_2'],
											'bonheur' => $donnees['bonheur'],
											'objet' => $donnees['objet']
											))or die(print_r($bdd->errorInfo()));
					$reponse = $bdd->prepare('DELETE FROM pokemons_marche_pokemons WHERE id=:id ') or die(print_r($bdd->errorInfo()));
					  $reponse->execute(array('id' => $_POST['id'])); 
					$reponse = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors, depenses_marche=:depenses_marche WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
									$reponse->execute(array('ors' =>$argent_restant , 'depenses_marche'=> $depenses_marche, 'pseudo' => $_SESSION['pseudo'])); 	
					$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('pseudo' => $donnees['pseudo']));
						$donnees = $reponse->fetch();
					$ors_vendeur=$donnees['ors'];	
					$argent_vendeur=$ors_vendeur+$prix;
					$reponse2 = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
									$reponse2->execute(array('ors' =>$argent_vendeur ,'pseudo' => $donnees['pseudo'])); 	
					$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("marchand", :destinataire, "non lu", "Vente réussie!", :message, now())') or die(print_r($bdd->errorInfo()));
					$req->execute(array(
						'destinataire' => $donnees['pseudo'],					
						'message' => 'félicitation, votre pokémon "'.$nom_du_pokemon_vendu.'" a été vendu au prix de '.$prix.' pépites à '.$_SESSION['pseudo'].'.'
						))
						or die(print_r($bdd->errorInfo()));					
					echo '<b>Vous avez bien acheté le pokémon.</b><br />'; if($evolution==1){echo '<b>Ho! Il a évolué durant la transaction!!<br/>';} echo'<br />';
					}
				else
					{
					if($_SESSION['pseudo']==$pseudo_vendeur)
						{echo '<b>Vous ne pouvez pas vous acheter un pokémon à vous-même!</b><br /><br />';}
					else
						{echo '<b>Vous n\'avez pas suffisamment de pépites pour acheter ce pokémon. </b><br /><br />';}
					}
				}
			}
		else{echo '<b>Vous devez aggrandir votre boite de stockage pour pouvoir avoir plus de pokémons.</b><br /><br />';}
		}
	else{echo '<b>Ce pokémon n\'est plus à vendre, vous avez été trop lent. Désolé. </b><br /><br />';}
	
        include ("include/maj_pokedex.php");
        }
if($_POST['action']=="vendre")
	{
	if (preg_match("#^[0-9]+$#", $_POST['prix']))
		{
		if($nb_pokemons_joueur!=1 AND ($nb_pokemons_a_vendre_par_joueur<5 OR $_SESSION['pseudo']=="admin"))
			{
			$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id=:id AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $_POST['id_pokemon'], 'pseudo' => $_SESSION['pseudo']));
			$donnees = $reponse->fetch();
			if(isset($donnees['id']))
				{
				$req = $bdd->prepare('INSERT INTO pokemons_marche_pokemons (pseudo, id_pokemon, shiney, sexe, lvl, xp, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, pa_restant, pa_max, pa_bonus, victoires, defaites, score, prix, fin_dodo, devise, reserve, parent_1, parent_2, bonheur, objet, quand) VALUES(:pseudo, :id_pokemon, :shiney, :sexe, :lvl, :xp, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, :pa_restant, :pa_max, :pa_bonus, :victoires, :defaites, :score, :prix, :fin_dodo, :devise, :reserve, :parent_1, :parent_2, :bonheur, :objet, :quand)') or die(print_r($bdd->errorInfo()));
							$req->execute(array(
									'pseudo' => $donnees['pseudo'], 
									'id_pokemon' => $donnees['id_pokemon'],
									'shiney' => $donnees['shiney'],
									'sexe' => $donnees['sexe'],
									'lvl' => $donnees['lvl'],
									'xp' => $donnees['xp'],
									'pv_max' => $donnees['pv_max'],
									'att' => $donnees['att'],
									'def' => $donnees['def'],
									'vit' => $donnees['vit'],
									'attspe' => $donnees['attspe'],
									'defspe' => $donnees['defspe'],
									'attaque1' => $donnees['attaque1'],
									'attaque2' => $donnees['attaque2'],
									'attaque3' => $donnees['attaque3'],
									'attaque4' => $donnees['attaque4'],
									'pa_restant' => $donnees['pa_restant'],
									'pa_max' => $donnees['pa_max'],
									'pa_bonus' => $donnees['pa_bonus'],
									'victoires' => $donnees['victoires'],
									'defaites' => $donnees['defaites'],
									'score' => $donnees['score'],
									'prix' => $_POST['prix'],
									'fin_dodo' => $donnees['fin_dodo'],
									'devise' => $_POST['devise'],
									'reserve' => $_POST['reserve'],
									'parent_1' => $donnees['parent_1'],
									'parent_2' => $donnees['parent_2'],
									'bonheur' => $donnees['bonheur'],
									'objet' => $donnees['objet'],
									'quand' => $time
									))or die(print_r($bdd->errorInfo()));
				$reponse = $bdd->prepare('DELETE FROM pokemons_liste_pokemons WHERE id=:id ') or die(print_r($bdd->errorInfo()));
				  $reponse->execute(array('id' => $_POST['id_pokemon'])); 
				echo '<b>Votre pokémon a bien été mis en vente! </b><br /><br />';
				}
			else{echo '<b>Il est interdit de vendre un pokémon que l\'on ne possède pas.</b><br /><br />';}
			}
		else {echo '<b>Vous ne pouvez pas vendre votre dernier pokémon ni plus de 5 pokémon en même temps. </b><br /><br />';	}
		}
	else{echo '<b> Vous devez indiquer un prix en valeur numérique non nulle pour vendre ! </b><br /><br />';}
	}

?>
<b>Rechercher un pokémon :</b>
<form action="marche.php" method="post">
<input type="hidden" name="marchandises" value="pokemons"/>
Trier par :
<select name="tri">
<option value="lvl" <?php if($_POST['tri']=="lvl"){echo 'selected';}?>>Lvl</option>
<option value="score" <?php if($_POST['tri']=="score"){echo 'selected';}?>>Score</option>
<option value="prix" <?php if($_POST['tri']=="prix"){echo 'selected';}?>>Prix</option>
<option value="att" <?php if($_POST['tri']=="att"){echo 'selected';}?>>Valeur d'attaque</option>
<option value="def" <?php if($_POST['tri']=="def"){echo 'selected';}?>>Valeur de défense</option>
<option value="vit" <?php if($_POST['tri']=="vit"){echo 'selected';}?>>Valeur de vitesse</option>
<option value="attspe" <?php if($_POST['tri']=="attspe"){echo 'selected';}?>>Valeur d'attaque spéciale</option>
<option value="defspe" <?php if($_POST['tri']=="defspe"){echo 'selected';}?>>Valeur de défense spéciale</option>
</select><br />
Chercher un pokémon précis ?
<select name="id_pokemon2">
<option value="tous">Tous</option>
<?php
$reponse = $bdd->query('SELECT id, nom FROM pokemons_base_pokemons ORDER BY id_pokedex') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch()){echo '<option value="'.$donnees['id'].'"'; if($_POST['id_pokemon2']==$donnees['id']){echo 'selected';} echo '>'.$donnees['nom'].'</option>';}
?>
</select><br />
Lvl minimum :
<select name="lvl_min">
<option value="0" <?php if($_POST['lvl_min']=="0"){echo 'selected';}?>>1</option><option value="10" <?php if($_POST['lvl_min']=="10"){echo 'selected';}?>>10</option><option value="20" <?php if($_POST['lvl_min']=="20"){echo 'selected';}?>>20</option><option value="30" <?php if($_POST['lvl_min']=="30"){echo 'selected';}?>>30</option><option value="40" <?php if($_POST['lvl_min']=="40"){echo 'selected';}?>>40</option>
<option value="50" <?php if($_POST['lvl_min']=="50"){echo 'selected';}?>>50</option><option value="60" <?php if($_POST['lvl_min']=="60"){echo 'selected';}?>>60</option><option value="70" <?php if($_POST['lvl_min']=="70"){echo 'selected';}?>>70</option><option value="80" <?php if($_POST['lvl_min']=="80"){echo 'selected';}?>>80</option><option value="90" <?php if($_POST['lvl_min']=="90"){echo 'selected';}?>>90</option>
</select><br />
Lvl maxmimum :
<select name="lvl_max">
<option value="100" <?php if($_POST['lvl_max']=="100"){echo 'selected';}?>>100</option><option value="90" <?php if($_POST['lvl_max']=="90"){echo 'selected';}?>>90</option><option value="80" <?php if($_POST['lvl_max']=="80"){echo 'selected';}?>>80</option><option value="70" <?php if($_POST['lvl_max']=="70"){echo 'selected';}?>>70</option><option value="60" <?php if($_POST['lvl_max']=="60"){echo 'selected';}?>>60</option>
<option value="50">50</option><option value="40" <?php if($_POST['lvl_max']=="40"){echo 'selected';}?>>40</option><option value="30" <?php if($_POST['lvl_max']=="30"){echo 'selected';}?>>30</option><option value="20" <?php if($_POST['lvl_max']=="20"){echo 'selected';}?>>20</option><option value="10" <?php if($_POST['lvl_max']=="10"){echo 'selected';}?>>10</option>
</select><br />
Shiney : 
<select name="shiney">
<option value="2">Tous</option>
<option value="1" <?php if($_POST['shiney']=="1"){echo 'selected';}?>>Non shiney</option>
<option value="0"  <?php if($_POST['shiney']=="0"){echo 'selected';}?>>Shiney</option>
</select><br />
Devise : 
<select name="devise2">
<option value="2">Toutes</option>
<option value="1" <?php if($_POST['devise2']=="1"){echo 'selected';}?>>pokédollards</option>
<option value="0" <?php if($_POST['devise2']=="0"){echo 'selected';}?>>pépites</option>
</select><br />
Vendeur : 
<select name="vendeur">
<option value="tous">Tous</option>
<?php
$reponse = $bdd->query('SELECT DISTINCT pseudo FROM pokemons_marche_pokemons ORDER BY pseudo ASC') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch()){   
    echo '<option value="'.$donnees['pseudo'].'"'; if($_POST['vendeur']==$donnees['pseudo']){echo 'selected';} echo '>'.$donnees['pseudo'].'</option>';
}
?>
</select><br />
<input type="submit" value="Rechercher"/>
</form>




 
<br />
<b>Pokémons à vendre sur le marché :</b>
<div <?php if($nb_pokemons_a_vendre>5) {echo 'id="marche_pokemons"';} else{echo 'width="550px"';} ?>style="text-align:center;">
<table id="marche" width="535px" cellpadding="2" cellspacing="2">
<colgroup><COL WIDTH=20%><COL WIDTH=20%><COL WIDTH=20%><COL WIDTH=10%><COL WIDTH=15%><COL WIDTH=15%></COLGROUP>
<tr> <th> Pokémons</th> <th> Nom </th> <th> Propriétaire </th> <th> Lvl </th> <th> Prix </th> <th>Acheter</th> </tr>
<?php //affichage des pokémons à vendre 
$lvl_min=$_POST['lvl_min'];
$lvl_max=$_POST['lvl_max'];
$devise_interdite=$_POST['devise2'];
$shiney_interdit=$_POST['shiney'];
if($_POST['tri']=="lvl"){$reponse = $bdd->prepare('SELECT id_pokemon, reserve, pseudo, lvl, id, shiney, prix, devise FROM pokemons_marche_pokemons WHERE lvl>=:lvl_min AND lvl <=:lvl_max AND devise!=:devise AND shiney!=:shiney ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));$reponse->execute(array('lvl_min' => $lvl_min, 'lvl_max'=>$lvl_max, 'devise'=>$devise_interdite, 'shiney'=>$shiney_interdit));}
elseif($_POST['tri']=="score"){$reponse = $bdd->prepare('SELECT id_pokemon, reserve, pseudo, lvl, id, shiney, prix, devise FROM pokemons_marche_pokemons WHERE lvl>=:lvl_min AND lvl <=:lvl_max AND devise!=:devise AND shiney!=:shiney ORDER BY score DESC') or die(print_r($bdd->errorInfo()));$reponse->execute(array('lvl_min' => $lvl_min, 'lvl_max'=>$lvl_max, 'devise'=>$devise_interdite, 'shiney'=>$shiney_interdit));}
elseif($_POST['tri']=="prix"){$reponse = $bdd->prepare('SELECT id_pokemon, reserve, pseudo, lvl, id, shiney, prix, devise FROM pokemons_marche_pokemons WHERE lvl>=:lvl_min AND lvl <=:lvl_max AND devise!=:devise AND shiney!=:shiney ORDER BY prix') or die(print_r($bdd->errorInfo()));$reponse->execute(array('lvl_min' => $lvl_min, 'lvl_max'=>$lvl_max, 'devise'=>$devise_interdite, 'shiney'=>$shiney_interdit));}
elseif($_POST['tri']=="att"){$reponse = $bdd->prepare('SELECT id_pokemon, reserve, pseudo, lvl, id, shiney, prix, devise FROM pokemons_marche_pokemons WHERE lvl>=:lvl_min AND lvl <=:lvl_max AND devise!=:devise AND shiney!=:shiney ORDER BY att DESC') or die(print_r($bdd->errorInfo()));$reponse->execute(array('lvl_min' => $lvl_min, 'lvl_max'=>$lvl_max, 'devise'=>$devise_interdite, 'shiney'=>$shiney_interdit));}
elseif($_POST['tri']=="def"){$reponse = $bdd->prepare('SELECT id_pokemon, reserve, pseudo, lvl, id, shiney, prix, devise FROM pokemons_marche_pokemons WHERE lvl>=:lvl_min AND lvl <=:lvl_max AND devise!=:devise AND shiney!=:shiney ORDER BY def DESC') or die(print_r($bdd->errorInfo()));$reponse->execute(array('lvl_min' => $lvl_min, 'lvl_max'=>$lvl_max, 'devise'=>$devise_interdite, 'shiney'=>$shiney_interdit));}
elseif($_POST['tri']=="vit"){$reponse = $bdd->prepare('SELECT id_pokemon, reserve, pseudo, lvl, id, shiney, prix, devise FROM pokemons_marche_pokemons WHERE lvl>=:lvl_min AND lvl <=:lvl_max AND devise!=:devise AND shiney!=:shiney ORDER BY vit DESC') or die(print_r($bdd->errorInfo()));$reponse->execute(array('lvl_min' => $lvl_min, 'lvl_max'=>$lvl_max, 'devise'=>$devise_interdite, 'shiney'=>$shiney_interdit));}
elseif($_POST['tri']=="attspe"){$reponse = $bdd->prepare('SELECT id_pokemon, reserve, pseudo, lvl, id, shiney, prix, devise FROM pokemons_marche_pokemons WHERE lvl>=:lvl_min AND lvl <=:lvl_max AND devise!=:devise AND shiney!=:shiney ORDER BY attspe DESC') or die(print_r($bdd->errorInfo()));$reponse->execute(array('lvl_min' => $lvl_min, 'lvl_max'=>$lvl_max, 'devise'=>$devise_interdite, 'shiney'=>$shiney_interdit));}
elseif($_POST['tri']=="defspe"){$reponse = $bdd->prepare('SELECT id_pokemon, reserve, pseudo, lvl, id, shiney, prix, devise FROM pokemons_marche_pokemons WHERE lvl>=:lvl_min AND lvl <=:lvl_max AND devise!=:devise AND shiney!=:shiney ORDER BY defspe DESC') or die(print_r($bdd->errorInfo()));$reponse->execute(array('lvl_min' => $lvl_min, 'lvl_max'=>$lvl_max, 'devise'=>$devise_interdite, 'shiney'=>$shiney_interdit));}
else{$reponse = $bdd->query('SELECT id_pokemon, reserve, pseudo, lvl, id, shiney, prix, devise  FROM pokemons_marche_pokemons ORDER BY prix') or die(print_r($bdd->errorInfo()));}
while($donnees = $reponse->fetch())
    {
	if($_POST['id_pokemon2']=="" OR $_POST['id_pokemon2']=="tous" OR $_POST['id_pokemon2']==$donnees['id_pokemon'] AND $donnees['lvl']!=0)
		{
		if($donnees['reserve']=="tous" OR $donnees['reserve']=="" OR $donnees['reserve']==$_SESSION['pseudo'])
			{
			if($_POST['vendeur']=="tous" OR $_POST['vendeur']=="" OR $_POST['vendeur']==$donnees['pseudo'])
				{
				$reponse2 = $bdd->prepare('SELECT id_pokedex, nom FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponse2->execute(array('id' => $donnees['id_pokemon']));
					$donnees2 = $reponse2->fetch();
				$id_pokedex=$donnees2['id_pokedex'];
				$nom_pokemon=$donnees2['nom'];
				?>
				<tr><td><a style="color:black;" href="marche_pokemons.php?id=<?php echo $donnees['id']; ?>" target="_blank"><?php if($donnees['lvl']!=0){?><img src="images/pokemons/<?php if($donnees['shiney']==1){echo 'shiney/';} echo $id_pokedex; ?>.gif" height="60px" style="border-style:none;"/><?php }else{?><img src="images/oeuf.gif" height="60px" style="border-style:none;"/><?php } ?></a></td>
				<td><a style="color:black;" href="marche_pokemons.php?id=<?php echo $donnees['id']; ?>" target="_blank"> <?php if($donnees['lvl']!=0){echo $nom_pokemon;} else{echo 'Oeuf';} ?></td> <td><?php echo $donnees['pseudo']; ?></a></td> <td><?php if($donnees['lvl']!=0){echo $donnees['lvl'];} ?></td> <td><?php echo $donnees['prix']; if($donnees['devise']==0){echo '$';}elseif($donnees['devise']==1){echo '<img src="images/or.png" />';}?></td>
				<td><form action="marche.php" method="post">                     	         
				<input type="hidden" name="id"  value="<?php echo $donnees['id']; ?>" />	 
				<input type="hidden" name="action"  value="acheter" /> 
				<input type="hidden" name="marchandises" value="pokemons"/>	
				<input type="submit" value="Acheter <?php if($donnees['reserve']==$_SESSION['pseudo']){echo 'en privé';}?>" <?php if($donnees['reserve']==$_SESSION['pseudo']){echo 'style="color:red;"';}?>/>           
				</form></td></tr>
				<?php
				}
			}
		}
	}
?>
</table>	
</div>
<br /><br />

<?php
if($nb_pokemons_a_vendre_par_joueur<5 OR $_SESSION['pseudo']=="admin")
{ //limité à 5 pokémon en vente
?>

<div <?php if($nb_pokemons_joueur>=15){echo 'id="marche_pokemons"';}else{echo 'width="550px"';} ?>>
<b>Quel pokémon désirez-vous mettre en vente?</b>
<form action="marche.php" method="post">
<table id="table_pokemons_inactifs" width="535px" cellpadding="2" cellspacing="2"><colgroup><COL WIDTH=46%><COL WIDTH=27%><COL WIDTH=27%></COLGROUP>
<?php //pokemon non actif
if($tri_points==0){$reponse = $bdd->prepare('SELECT id_pokemon, lvl, pa_restant, pa_max, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY lvl DESC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==1){$reponse = $bdd->prepare('SELECT id_pokemon, lvl, pa_restant, pa_max, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY score DESC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==2){$reponse = $bdd->prepare('SELECT id_pokemon, lvl, pa_restant, pa_max, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY victoires DESC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==3){$reponse = $bdd->prepare('SELECT id_pokemon, lvl, pa_restant, pa_max, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY surnom ASC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==4){$reponse = $bdd->prepare('SELECT id_pokemon, lvl, pa_restant, pa_max, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY lvl DESC,surnom ASC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==5){$reponse = $bdd->prepare('SELECT id_pokemon, lvl, pa_restant, pa_max, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 ORDER BY pa_restant DESC ') or die(print_r($bdd->errorInfo()));}
if($tri_points==6){$reponse = $bdd->prepare('SELECT id_pokemon, lvl, pa_restant, pa_max, id, shiney, surnom FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=0 AND lvl>0 ORDER BY id_pokemon ASC ') or die(print_r($bdd->errorInfo()));}		
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
while($donnees = $reponse->fetch())
{
$id_pokemon_inactif=$donnees['id_pokemon'];
$lvl_pokemon_inactif=$donnees['lvl'];
$pa_restant_pokemon_inactif=$donnees['pa_restant'];
$pa_max_pokemon_inactif=$donnees['pa_max'];
$id_liste_pokemon=$donnees['id'];
$shiney_pokemon=$donnees['shiney'];
$surnom_pokemon_inactif=$donnees['surnom'];
$reponse2 = $bdd->prepare('SELECT nom FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse2->execute(array('id' => $id_pokemon_inactif));
$donnees2 = $reponse2->fetch();
$nom_pokemon_inactif=$donnees2['nom'];

echo '<tr '; if($shiney_pokemon==1 AND $lvl_pokemon_inactif!=0){echo 'style="background-color:lightblue;"';} echo '><td>'; if($lvl_pokemon_inactif!=0){echo $surnom_pokemon_inactif;} else{echo 'Oeuf';} echo '</td><td>'; if($lvl_pokemon_inactif!=0){echo 'lvl '.$lvl_pokemon_inactif;} echo '</td><td><img src="images/coeur.gif" />'.$pa_restant_pokemon_inactif.'/'.$pa_max_pokemon_inactif.'</td><td><input type="radio" name="id_pokemon" value="'.$donnees['id'].'"></td></tr>';
}
?>	
</table>
</div>
Prix de mise en vente : <input type="text" name="prix" size="6"/> 
<input type="radio" name="devise" value="0" checked> $	
<input type="radio" name="devise" value="1"> <img src="images/or.png" />
<input type="hidden" name="action" value="vendre"/>
<input type="hidden" name="marchandises" value="pokemons"/>
<br />Réserver à un joueur en particulier? (déconseillé)
<select name="reserve">
<option value="tous">Tous</option>
<?php
$reponse = $bdd->query('SELECT pseudo FROM pokemons_membres ORDER BY pseudo') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch()){echo '<option value="'.$donnees['pseudo'].'">'.$donnees['pseudo'].'</option>';}
?>
</select><br />
<input type="submit" value="Vendre"/>
</form>

<?php
} //fin de la limite de 5 pokémon en vente
else
{
echo '<b>Quel pokémon désirez-vous mettre en vente?</b><br />';
echo '<b>Vous ne pouvez pas vendre plus de 5 pokémons à la fois! </b><br />';
}
?>

<br /><br />


<b>Vos pokémons à vendre sur le marché :</b>
<div <?php if($nb_pokemons_a_vendre_du_joueur>5) {echo 'id="marche_pokemons"';} else{echo 'width="550px"';} ?>style="text-align:center;">
<table id="marche" width="535px" cellpadding="2" cellspacing="2">
<colgroup><COL WIDTH=15%><COL WIDTH=15%><COL WIDTH=15%><COL WIDTH=10%><COL WIDTH=15%><COL WIDTH=15%><COL WIDTH=15%></COLGROUP>
<tr> <th> Pokémons</th> <th> Nom </th>  <th> Lvl </th> <th> Prix </th> <th> Réservé à </th><th>Récupérer</th> </tr>
<?php //affichage des pokémons à vendre
$reponse = $bdd->prepare('SELECT id_pokemon, lvl, shiney, id, devise, reserve, prix FROM pokemons_marche_pokemons WHERE pseudo=:pseudo ORDER BY prix') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
while($donnees = $reponse->fetch())
    {
	$reponse2 = $bdd->prepare('SELECT id_pokedex, nom FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['id_pokemon']));
		$donnees2 = $reponse2->fetch();
	$id_pokedex=$donnees2['id_pokedex'];
	$nom_pokemon=$donnees2['nom'];
	?>
	<tr><td><?php if($donnees['lvl']!=0){?><img src="images/pokemons/<?php if($donnees['shiney']==1){echo 'shiney/';} echo $id_pokedex; ?>.gif" height="60px" style="border-style:none;"/><?php } else{?><img src="images/oeuf.gif" height="60px" style="border-style:none;"/><?php } ?></td>
	<td><?php if($donnees['lvl']!=0){echo $nom_pokemon;} else{echo 'Oeuf';} ?></td> <td><?php if($donnees['lvl']!=0){echo $donnees['lvl'];} ?></td> <td><?php echo $donnees['prix']; if($donnees['devise']==0){echo '$';}elseif($donnees['devise']==1){echo '<img src="images/or.png" />';}?></td><td><?php if($donnees['reserve']==""){echo 'Tous';} else{echo $donnees['reserve'];} ?></td>
	<td><form action="marche.php" method="post">                     	         
    <input type="hidden" name="id"  value="<?php echo $donnees['id']; ?>" />	 
    <input type="hidden" name="action"  value="recuperer" /> 
<input type="hidden" name="marchandises" value="pokemons"/>	
    <input type="submit" value="Récupérer" />           
    </form></td></tr>
	<?php
	}
?>
</table>	
</div>
<?php
} //fin du marché des pokémons
?>







<?php //marché des items
if($_POST['marchandises']=="items")
{
if($_POST['action']=="recuperer")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_marche_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_POST['id']));
		$donnees = $reponse->fetch();
	if(isset($donnees['id']) AND $_SESSION['pseudo']==$donnees['pseudo'])
		{		
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
			$reponse2->execute(array('pseudo' => $_SESSION['pseudo'], 'id_item' => $donnees['id_item']));  
			$donnees2 = $reponse2->fetch();
		if(isset($donnees2['id']))
			{
			$quantite_total=$donnees['quantite'] + $donnees2['quantite'];
			$reponse = $bdd->prepare('UPDATE pokemons_inventaire SET quantite=:quantite WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('quantite' =>$quantite_total ,'pseudo' => $_SESSION['pseudo'], 'id_item' => $donnees['id_item'])); 
			}
		else
			{
			$req = $bdd->prepare('INSERT INTO pokemons_inventaire (pseudo, id_item, quantite) VALUES(:pseudo, :id_item, :quantite)') or die(print_r($bdd->errorInfo()));
					$req->execute(array(
							'pseudo' => $_SESSION['pseudo'], 
							'id_item' => $donnees['id_item'],
							'quantite' => $donnees['quantite']
							))or die(print_r($bdd->errorInfo()));
			}	
		$reponse = $bdd->prepare('DELETE FROM pokemons_marche_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
				  $reponse->execute(array('id' => $_POST['id']));
		echo '<b>Vous avez bien récupéré '; if($donnees['quantite']>1){echo 'vos objets.';}else{echo 'votre objet.';} echo '</b><br /><br />';
		}
	else{echo '<b>Cet objet n\'est plus à vendre ou ne vous appartient pas. </b><br /><br />';}
	}
if($_POST['action']=="acheter")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
		$donnees = $reponse->fetch();
	$pokedollar=$donnees['pokedollar'];
	$reponse = $bdd->prepare('SELECT * FROM pokemons_marche_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_POST['id']));
		$donnees = $reponse->fetch();
	$prix_a_payer=$donnees['prix']*$_POST['quantite'];$pseudo_vendeur=$donnees['pseudo'];
	$reponse3 = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse3->execute(array('id' => $donnees['id_item']));
		$donnees3 = $reponse3->fetch();
	$nom_item_vendu=$donnees3['nom'];
	if(isset($donnees['id']))
		{
		if($_POST['quantite']<=$donnees['quantite'] AND $_POST['quantite']>0 AND $_POST['quantite']!="")
			{
			if($pokedollar>=$prix_a_payer AND $pseudo_vendeur!=$_SESSION['pseudo'])
				{
				$depenses_marche=$depenses_marche+$prix_a_payer;
				$argent_restant=$pokedollar-$prix_a_payer;
				$reponse2 = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
				$reponse2->execute(array('pseudo' => $_SESSION['pseudo'], 'id_item' => $donnees['id_item']));  
				$donnees2 = $reponse2->fetch();
				if(isset($donnees2['id']))
					{
					$quantite_total=$_POST['quantite'] + $donnees2['quantite'];
					$reponse = $bdd->prepare('UPDATE pokemons_inventaire SET quantite=:quantite WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('quantite' =>$quantite_total ,'pseudo' => $_SESSION['pseudo'], 'id_item' => $donnees['id_item'])); 
					}
				else
					{
					$req = $bdd->prepare('INSERT INTO pokemons_inventaire (pseudo, id_item, quantite) VALUES(:pseudo, :id_item, :quantite)') or die(print_r($bdd->errorInfo()));
							$req->execute(array(
									'pseudo' => $_SESSION['pseudo'], 
									'id_item' => $donnees['id_item'],
									'quantite' => $_POST['quantite']
									))or die(print_r($bdd->errorInfo()));
					}
				if($_POST['quantite']==$donnees['quantite'])
					{
					$reponse = $bdd->prepare('DELETE FROM pokemons_marche_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $_POST['id'])); 
					}
				else
					{
					$quantite_restante=$donnees['quantite']-$_POST['quantite'];
					$reponse = $bdd->prepare('UPDATE pokemons_marche_items SET quantite=:quantite WHERE id=:id') or die(print_r($bdd->errorInfo()));
									$reponse->execute(array('quantite' =>$quantite_restante ,'id' => $_POST['id']));
					}
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar, depenses_marche=:depenses_marche WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
								$reponse->execute(array('pokedollar' =>$argent_restant , 'depenses_marche'=> $depenses_marche, 'pseudo' => $_SESSION['pseudo'])); 	
				$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('pseudo' => $donnees['pseudo']));
						$donnees = $reponse->fetch();
				$pokedollar_vendeur=$donnees['pokedollar'];	
				$argent_vendeur=$pokedollar_vendeur+$prix_a_payer;
				$reponse2 = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
								$reponse2->execute(array('pokedollar' =>$argent_vendeur ,'pseudo' => $donnees['pseudo'])); 	
				
				if($_POST['quantite']>1){$message_quantite='ont été vendus';}else{$message_quantite='a été vendu';}
				$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("marchand", :destinataire, "non lu", "Vente réussie!", :message, now())') or die(print_r($bdd->errorInfo()));
				$req->execute(array(
						'destinataire' => $donnees['pseudo'],					
						'message' => 'félicitation, "'.$_POST['quantite'].' '.$nom_item_vendu.'" '.$message_quantite.' au prix de '.$prix_a_payer.' $ à '.$_SESSION['pseudo'].'.'
						))
						or die(print_r($bdd->errorInfo()));					
				echo '<b>Vous avez bien acheté '; if($_POST['quantite']>1){echo 'ces objets.';}else{echo 'cet objet.';} echo '</b><br /><br />';
				}
			else
				{
				if($pseudo_vendeur==$_SESSION['pseudo'])
					{echo '<b>Vous ne pouvez pas vous acheter un objet à vous-même.</b><br /><br />';}
				else
					{echo '<b>Vous n\'avez pas suffisamment d\'argent pour acheter ce pokémon. </b><br /><br />';}
				}
			}
		else{echo '<b>La quantité que vous avez demandé n\'est pas valide. </b><br /><br />';}
		}
	else{echo '<b>Cet objet n\'est plus à vendre, vous avez été trop lent. Désolé. </b><br /><br />';}
	}
if($_POST['action']=="vendre")
	{
	if (preg_match("#^[0-9]+$#", $_POST['prix']))
		{
		if (preg_match("#^[0-9]+$#", $_POST['quantite']) AND $_POST['quantite']>0)
			{
			$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE id_item=:id AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $_POST['id_item'], 'pseudo' => $_SESSION['pseudo']));
			$donnees = $reponse->fetch();
			if($donnees['quantite']>=$_POST['quantite'])
				{
				$req = $bdd->prepare('INSERT INTO pokemons_marche_items (id_item, pseudo, prix, quantite) VALUES(:id_item, :pseudo, :prix, :quantite)') or die(print_r($bdd->errorInfo()));
								$req->execute(array(
										'id_item' => $donnees['id_item'], 
										'pseudo' => $_SESSION['pseudo'],
										'prix' => $_POST['prix'],
										'quantite' => $_POST['quantite']									
										))or die(print_r($bdd->errorInfo()));
				$quantite_now=$donnees['quantite']-$_POST['quantite'];
				$reponse2 = $bdd->prepare('UPDATE pokemons_inventaire SET quantite=:quantite WHERE id=:id') or die(print_r($bdd->errorInfo()));
								$reponse2->execute(array('quantite' =>$quantite_now ,'id' => $donnees['id'])); 	
				if($_POST['quantite']>1) {echo '<b>Vos objets ont bien été mis en vente.</b>';} else {echo '<b>Votre objet a bien été mis en vente. </b>';} echo '<br /><br />';
				}
			else{echo '<b>Il est interdit de vendre plus que ce que l\'on possède</b><br /><br />';}
			}
		else{echo '<b> Vous devez indiquer une quantité en valeur numérique non nulle pour vendre ! </b><br /><br />';}
		}
	else{echo '<b> Vous devez indiquer un prix en valeur numérique non nulle pour vendre ! </b><br /><br />';}
	}
?>

<b>Objet à vendre sur le marché :</b>
<div <?php if($nb_items_a_vendre>5) {echo 'id="marche_pokemons"';} else{echo 'width="550px"';} ?>style="text-align:center;">
<table id="marche" width="535px" cellpadding="2" cellspacing="2">
<colgroup><COL WIDTH=12%><COL WIDTH=20%><COL WIDTH=18%><COL WIDTH=10%><COL WIDTH=15%><COL WIDTH=15%><COL WIDTH=10%></COLGROUP>
<tr> <th> Objets</th> <th> Nom </th> <th> Propriétaire </th> <th> Prix unitaire </th> <th>Quantité en vente</th> <th>Quantité désirée</th><th>Acheter</th> </tr>
<?php //affichage des pokémons à vendre 
$reponse = $bdd->query('SELECT * FROM pokemons_marche_items ORDER BY prix') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
    {
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['id_item']));
		$donnees2 = $reponse2->fetch();
	$nom_item=$donnees2['nom'];
	?>
	<tr><td><img src="images/shop/<?php echo $donnees2['id']; ?>.png" /></td>
	<td><?php echo $nom_item; ?></td> <td><?php echo $donnees['pseudo']; ?></td> <td><?php echo $donnees['prix']; ?></td><td><?php echo $donnees['quantite']; ?></td>
	<td><form action="marche.php" method="post"><input type="text" name="quantite" size="6"/> <input type="hidden" name="id"  value="<?php echo $donnees['id']; ?>" />                  	         
    </td><td><input type="hidden" name="action" value="acheter" /><input type="hidden" name="marchandises" value="items"/>	<input type="submit" value="Acheter" /> </form></td></tr>
	<?php
	}
?>
</table>	        
</div>  
<br /><br />

<div <?php if($nb_items_joueur>=15){echo 'id="marche_pokemons"';}else{echo 'width="550px"';} ?>>
<b>Quels objets désirez-vous mettre en vente?</b>
<form action="marche.php" method="post">
<table id="table_pokemons_inactifs" width="535px" cellpadding="2" cellspacing="2"><colgroup><COL WIDTH=15%><COL WIDTH=45%><COL WIDTH=25%><COL WIDTH=15%></COLGROUP>
<tr><th>Objets</th><th>Nom</th><th>Quantité possédée</th><th>Vendre</th></tr>
<?php //pokemon non actif


$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire
                          INNER JOIN pokemons_base_items ON pokemons_base_items.id = pokemons_inventaire.id_item
                          WHERE (pokemons_inventaire.pseudo=:pseudo AND pokemons_inventaire.quantite>0 AND pokemons_base_items.shop_items != "animation");') or die(print_r($bdd->errorInfo()));
//$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE (pseudo=:pseudo AND quantite>0 ') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
while($donnees = $reponse->fetch())
{

$id_item=$donnees['id_item'];
$quantite=$donnees['quantite'];
$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse2->execute(array('id' => $id_item));
$donnees2 = $reponse2->fetch();
$nom_item=$donnees2['nom'];

echo '<tr><td><img src="images/shop/'.$donnees2['id'].'.png" /><td>'.$nom_item.'</td><td>'.$quantite.'</td><td><input type="radio" name="id_item" value="'.$donnees['id'].'"></td></tr>';
}
?>	
</table>
</div>
Quantité mise en vente : <input type="text" name="quantite" size="6"/> <br />
Prix de mise en vente UNITAIRE : <input type="text" name="prix" size="6"/> 
<input type="hidden" name="action" value="vendre"/>
<input type="hidden" name="marchandises" value="items"/>
<input type="submit" value="Vendre"/>
</form>
<br /><br />


<b>Vos objets à vendre sur le marché :</b>
<div <?php if($nb_items_a_vendre_du_joueur>5) {echo 'id="marche_pokemons"';} else{echo 'width="550px"';} ?>style="text-align:center;">
<table id="marche" width="535px" cellpadding="2" cellspacing="2">
<colgroup><COL WIDTH=20%><COL WIDTH=20%><COL WIDTH=20%><COL WIDTH=10%><COL WIDTH=15%></COLGROUP>
<tr> <th> Objets</th> <th> Nom </th> <th> Quantité </th> <th> Prix unitaire </th> <th>Récupérer</th> </tr>
<?php //affichage des objets à vendre par le joueur
$reponse = $bdd->prepare('SELECT * FROM pokemons_marche_items WHERE pseudo=:pseudo ORDER BY prix') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
while($donnees = $reponse->fetch())
    {
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['id_item']));
		$donnees2 = $reponse2->fetch();
	$nom_item=$donnees2['nom'];
	?>
	<tr><td><img src="images/shop/<?php echo $donnees2['id']; ?>.png" /></td>
	<td><?php echo $nom_item; ?></td><td><?php echo $donnees['quantite']; ?></td><td><?php echo $donnees['prix']; ?></td>
	<td><form action="marche.php" method="post">                     	         
    <input type="hidden" name="id"  value="<?php echo $donnees['id']; ?>" />	 
    <input type="hidden" name="action"  value="recuperer" /> 
<input type="hidden" name="marchandises" value="items"/>	
    <input type="submit" value="Récupérer" />           
    </form></td></tr>
	<?php
	}
?>
</table>	
</div>
<?php
} //fin du marché des items
?>





<?php //marché de l'or
if($_POST['marchandises']=="or")
{
if($_POST['action']=="recuperer")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_marche_or WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_POST['id']));
		$donnees = $reponse->fetch();
	if(isset($donnees['id']) AND $_SESSION['pseudo']==$donnees['pseudo'])
		{		
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo ') or die(print_r($bdd->errorInfo()));
			$reponse2->execute(array('pseudo' => $_SESSION['pseudo']));  
			$donnees2 = $reponse2->fetch();
		if(isset($donnees2['id']))
			{
			$quantite_total=$donnees['quantite'] + $donnees2['ors'];
			$reponse = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors WHERE pseudo=:pseudo ') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('ors' =>$quantite_total ,'pseudo' => $_SESSION['pseudo'])); 
			}
			
		$reponse = $bdd->prepare('DELETE FROM pokemons_marche_or WHERE id=:id') or die(print_r($bdd->errorInfo()));
				  $reponse->execute(array('id' => $_POST['id']));
		echo '<b>Vous avez bien récupéré votre or.</b><br /><br />';
		}
	else{echo '<b>Cet objet n\'est plus à vendre ou ne vous appartient pas. </b><br /><br />';}
	}
if($_POST['action']=="acheter")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
		$donnees = $reponse->fetch();
	$pokedollar=$donnees['pokedollar'];
	$or_actuel=$donnees['ors'];
	$or_after=$or_actuel+$_POST['quantite'];
	$reponse = $bdd->prepare('SELECT * FROM pokemons_marche_or WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_POST['id']));
		$donnees = $reponse->fetch();
	$prix_a_payer=$donnees['prix']*$_POST['quantite'];$pseudo_vendeur=$donnees['pseudo'];
	if(isset($donnees['id']))
		{
		if($_POST['quantite']<=$donnees['quantite'] AND $_POST['quantite']>0 AND $_POST['quantite']!="")
			{
			if($pokedollar>=$prix_a_payer AND $pseudo_vendeur!=$_SESSION['pseudo'])
				{
				$depenses_marche=$depenses_marche+$prix_a_payer;
				$argent_restant=$pokedollar-$prix_a_payer;				
				if($_POST['quantite']==$donnees['quantite'])
					{
					$reponse = $bdd->prepare('DELETE FROM pokemons_marche_or WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $_POST['id'])); 
					}
				else
					{
					$quantite_restante=$donnees['quantite']-$_POST['quantite'];
					$reponse = $bdd->prepare('UPDATE pokemons_marche_or SET quantite=:quantite WHERE id=:id') or die(print_r($bdd->errorInfo()));
									$reponse->execute(array('quantite' =>$quantite_restante ,'id' => $_POST['id']));
					}
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar, ors=:ors, depenses_marche=:depenses_marche WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
								$reponse->execute(array('pokedollar' =>$argent_restant , 'ors' => $or_after, 'depenses_marche'=> $depenses_marche, 'pseudo' => $_SESSION['pseudo'])); 	
				$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('pseudo' => $donnees['pseudo']));
						$donnees = $reponse->fetch();
				$pokedollar_vendeur=$donnees['pokedollar'];	
				$argent_vendeur=$pokedollar_vendeur+$prix_a_payer;
				$reponse2 = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
								$reponse2->execute(array('pokedollar' =>$argent_vendeur ,'pseudo' => $donnees['pseudo'])); 					
				if($_POST['quantite']>1){$message_quantite='ont été vendus';}else{$message_quantite='a été vendu';}
				$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("marchand", :destinataire, "non lu", "Vente réussie!", :message, now())') or die(print_r($bdd->errorInfo()));
				$req->execute(array(
						'destinataire' => $donnees['pseudo'],					
						'message' => 'félicitation, "'.$_POST['quantite'].' or" '.$message_quantite.' au prix de '.$prix_a_payer.' $ à '.$_SESSION['pseudo'].'.'
						))
						or die(print_r($bdd->errorInfo()));					
				echo '<b>Vous avez bien acheté l\'or.</b><br /><br />';
				}
			else
				{
				if($pseudo_vendeur==$_SESSION['pseudo'])
					{echo '<b> Vous ne pouvez pas vous acheter votre propre or.</b><br /><br />';}
				else
					{echo '<b>Vous n\'avez pas suffisamment d\'argent pour acheter ce pokémon. </b><br /><br />';}
				}
			}
		else{echo '<b>La quantité que vous avez demandé est plus importante que les stocks disponibles ou est égale à 0. </b><br /><br />';}
		}
	else{echo '<b>Cet objet n\'est plus à vendre, vous avez été trop lent. Désolé. </b><br /><br />';}
	}
if($_POST['action']=="vendre")
	{
	if (preg_match("#^[0-9]+$#", $_POST['prix']))
		{
		if (preg_match("#^[0-9]+$#", $_POST['quantite']))
			{
			$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
			$donnees = $reponse->fetch();
			if($donnees['ors']>=$_POST['quantite'])
				{
				$req = $bdd->prepare('INSERT INTO pokemons_marche_or ( pseudo, prix, quantite) VALUES( :pseudo, :prix, :quantite)') or die(print_r($bdd->errorInfo()));
								$req->execute(array(
										'pseudo' => $_SESSION['pseudo'],
										'prix' => $_POST['prix'],
										'quantite' => $_POST['quantite']									
										))or die(print_r($bdd->errorInfo()));
				$quantite_now=$donnees['ors']-$_POST['quantite'];
				$reponse2 = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
								$reponse2->execute(array('ors' =>$quantite_now ,'pseudo' => $_SESSION['pseudo'])); 	
				echo '<b>Votre or a bien été mis en vente.</b><br /><br />';
				}
			else{echo '<b>Il est interdit de vendre plus que ce que l\'on possède</b><br /><br />';}
			}
		else{echo '<b> Vous devez indiquer une quantité en valeur numérique non nulle pour vendre ! </b><br /><br />';}
		}
	else{echo '<b> Vous devez indiquer un prix en valeur numérique non nulle pour vendre ! </b><br /><br />';}
	}
?>

<b>Or à vendre sur le marché :</b>
<div <?php if($nb_or_a_vendre>5) {echo 'id="marche_pokemons"';} else{echo 'width="550px"';} ?>style="text-align:center;">
<table id="marche" width="535px" cellpadding="2" cellspacing="2">
<colgroup><COL WIDTH=30%><COL WIDTH=15%><COL WIDTH=15%><COL WIDTH=20%><COL WIDTH=20%></COLGROUP>
<tr> <th> Propriétaire </th> <th> Prix unitaire </th> <th>Quantité en vente</th> <th>Quantité désirée</th><th>Acheter</th> </tr>
<?php //affichage des pokémons à vendre 
$reponse = $bdd->query('SELECT * FROM pokemons_marche_or ORDER BY prix') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
    {
	?>
	<tr><td><?php echo $donnees['pseudo']; ?></td> <td><?php echo $donnees['prix']; ?></td><td><?php echo $donnees['quantite']; ?></td>
	<td><form action="marche.php" method="post"><input type="text" name="quantite" size="6"/> <input type="hidden" name="id"  value="<?php echo $donnees['id']; ?>" />                  	         
    </td><td><input type="hidden" name="action" value="acheter" /><input type="hidden" name="marchandises" value="or"/>	<input type="submit" value="Acheter" /> </form></td></tr>
	<?php
	}
?>
</table>	       

</div>  
<br /><br />


<b>Combien d'or désirez-vous mettre en vente?</b><br />
<form action="marche.php" method="post">
<?php
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
$or_dispo_joueur=$donnees['ors'];
?>
Or disponible : <?php echo $or_dispo_joueur; ?><br />
Quantité mise en vente : <input type="text" name="quantite" size="6"/> <br />
Prix de mise en vente UNITAIRE : <input type="text" name="prix" size="6"/> 
<input type="hidden" name="action" value="vendre"/>
<input type="hidden" name="marchandises" value="or"/>
<input type="submit" value="Vendre"/>
</form>
<br /><br />


<b>Votre or à vendre sur le marché :</b>
<div <?php if($nb_or_a_vendre_du_joueur>5) {echo 'id="marche_pokemons"';} else{echo 'width="550px"';} ?>style="text-align:center;">
<table id="marche" width="535px" cellpadding="2" cellspacing="2">
<colgroup><COL WIDTH=30%><COL WIDTH=30%><COL WIDTH=40%></COLGROUP>
<tr> <th> Quantité </th> <th> Prix unitaire </th> <th>Récupérer</th> </tr>
<?php //affichage de l'or à vendre par le joueur
$reponse = $bdd->prepare('SELECT * FROM pokemons_marche_or WHERE pseudo=:pseudo ORDER BY prix') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
while($donnees = $reponse->fetch())
    {
	?>
	<tr><td><?php echo $donnees['quantite']; ?></td><td><?php echo $donnees['prix']; ?></td>
	<td><form action="marche.php" method="post">                     	         
    <input type="hidden" name="id"  value="<?php echo $donnees['id']; ?>" />	 
    <input type="hidden" name="action"  value="recuperer" /> 
<input type="hidden" name="marchandises" value="or"/>	
    <input type="submit" value="Récupérer" />           
    </form></td></tr>
	<?php
	}
?>
</table>	
</div>
<?php
} //fin du marché de l'or
?>





<?php
}
else
{
echo 'Vous n\'avez pas encore accès à cette page.';
}
}
else
{
echo 'Vous devez être connecté pour accéder à cette page';
}
?>
<?php include ("bas.php"); ?>