<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">

<?php
if(isset($_SESSION['pseudo']))
{
?>

<h3> Pokedex détaillé </h3>

Vous possédez <?php echo $nb_pokemons_joueur; ?> pokémons au total. <br />
Vous avez déjà attrapé <?php echo $nb_pokemons_capture; ?> pokémons d'espèces différentes. <br /> 
Vous avez déjà attrapé <?php echo $nb_pokemons_shiney_capture; ?> pokémons shineys d'espèces différentes. <br /> 

<table id="profil" width="533px" cellpadding="2" cellspacing="2" style="text-align:center;" >
<colgroup><COL WIDTH=5%><COL WIDTH=45%><COL WIDTH=25%><COL WIDTH=25%></COLGROUP>
<tr><th colspan="9">Récapitulatif de vos captures</th></tr>
<tr><td><b> N° </b></td><td><b>Pokémon</b></td><td><b>Normal </b></td><td><b>Shiney </b></td></tr>
<?php
$reponse = $bdd->query('SELECT * FROM pokemons_base_pokemons ORDER BY id_pokedex') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch())
	{
	$capture_normal=0;$capture_shiney=0;
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_captures WHERE id_pokemon=:id_pokemon AND pseudo=:pseudo AND shiney=0') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id_pokemon' => $donnees['id'], 'pseudo' => $_SESSION['pseudo']));  
	$donnees2 = $reponse2->fetch();
	if(isset($donnees2['id'])){$capture_normal=1;}
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_captures WHERE id_pokemon=:id_pokemon AND pseudo=:pseudo AND shiney=1') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id_pokemon' => $donnees['id'], 'pseudo' => $_SESSION['pseudo']));  
	$donnees2 = $reponse2->fetch();
	if(isset($donnees2['id'])){$capture_shiney=1;}
	echo '<tr><td>'.$donnees['id_pokedex'].'</td><td>'.$donnees['nom'].'</td><td>';if($capture_normal==1){echo'<img src="images/shop/1.png"/>';}echo '</td><td>';if($capture_shiney==1){echo'<img src="images/shop/1.png"/>';}echo '</td></tr>';
	}

?>
</table>




<?php
}
else
{
echo 'Vous devez être connecté pour accéder à cette page';
}
?>

<?php include ("bas.php"); ?>