<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">

<?php //sécurité
if(isset($_SESSION['pseudo']))
{
if(isset($_GET['id']))
{
?>


<?php //ouverture quete et données du membre
$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_principales WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_GET['id']));  
	$donnees = $reponse->fetch();
$numero_quete=$donnees['numero'];
$etape_quete=$donnees['etape'];
$pos_hor_quete=$donnees['pos_hor'];
$pos_ver_quete=$donnees['pos_ver'];
$qui=$donnees['qui'];
$genre=$donnees['genre'];
$what=$donnees['what'];
$pokedex_inf=$donnees['pokedex_inf'];
$pokedex_sup=$donnees['pokedex_sup'];
$condition_reussite=$donnees['condition_reussite'];
$nb_reussite=$donnees['nb_reussite'];
$intro=$donnees['intro'];
$reussite=$donnees['reussite'];
$pas_reussi=$donnees['pas_reussi'];
$bouton_parler=$donnees['bouton_parler'];
$genre_recompense=$donnees['genre_recompense'];
$recompense=$donnees['recompense'];
$nb_recompense=$donnees['nb_recompense'];
$_POST['genre']=$donnees['genre'];
$_POST['genre_recompense']=$donnees['genre_recompense'];
$_POST['recompense']=$donnees['recompense'];
$_POST['nb_recompense']=$donnees['nb_recompense'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('id' => $recompense));  
						$donnees = $reponse->fetch();
$nom_recompense=$donnees['nom'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
	$donnees = $reponse->fetch();
$pos_hor=$donnees['pos_hor'];
$pos_ver=$donnees['pos_ver'];
$quete_principale=$donnees['quete_principale'];
$quete_principale_etape=$donnees['quete_principale_etape'];
$quete_principale_active=$donnees['quete_principale_active'];
$nb_quete_principale=$donnees['nb_quete_principale'];
$argent=$donnees['pokedollar'];
?>

<?php //recompense échange
if($_POST['action']=="reussite")
	{		
	$permission_echange=1;
	$permission_kill=1;
	$permission_montrer=1;
	if($_POST['genre']=="echange")
			{
			$permission_echange=0;
			$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE nom=:nom') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('nom' => $what));
			$donnees = $reponse->fetch();
			$id_pokemon_echange=$donnees['id'];
			$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id=:id AND id_pokemon=:id_pokemon AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $_POST['id_pokemon'], 'id_pokemon' =>$id_pokemon_echange, 'pseudo'=>$_SESSION['pseudo']));
			$donnees = $reponse->fetch();
			if(isset($donnees['id']))
				{
				$req = $bdd->prepare('DELETE FROM pokemons_liste_pokemons WHERE id=:id AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$req->execute(array('id' => $_POST['id_pokemon'],'pseudo' =>$_SESSION['pseudo'])) or die(print_r($bdd->errorInfo()));	
				$permission_echange=1;
				}
			}
	if($_POST['genre']=="kill")
			{
			if($nb_quete_principale<$nb_reussite)
				{
				$permission_kill=0;
				}
			}
	if($permission_echange==1 AND $permission_kill==1 AND $permission_montrer==1)
		{
		if($_POST['genre_recompense']=="item")
			{
			$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pseudo' => $_SESSION['pseudo'], 'id_item' => $_POST['recompense']));  
					$donnees = $reponse->fetch();
			if(isset($donnees['id']))
				{
				$quantite_total=$_POST['nb_recompense'] + $donnees['quantite'];
				$reponse = $bdd->prepare('UPDATE pokemons_inventaire SET quantite=:quantite WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('quantite' =>$quantite_total ,'pseudo' => $_SESSION['pseudo'], 'id_item' => $_POST['recompense'])); 
				}
			else
				{
				$req = $bdd->prepare('INSERT INTO pokemons_inventaire (pseudo, id_item, quantite) VALUES(:pseudo, :id_item, :quantite)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo'], 'id_item' => $_POST['recompense'],'quantite' => $_POST['nb_recompense']))or die(print_r($bdd->errorInfo()));
				}
			$reponse = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $_POST['recompense']));  
			$donnees = $reponse->fetch();
			echo 'Vous avez reçu '.$_POST['nb_recompense'].' '.$donnees['nom'].'. <br />';				
			}
		if($_POST['genre_recompense']=="argent")
			{		
			$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
				$donnees = $reponse->fetch();
			$argent=$donnees['pokedollar'];
			$argent_now=$argent+$_POST['recompense'];
			$reponse = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar  WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('pokedollar' => $argent_now, 'pseudo' => $_SESSION['pseudo']));	
			echo 'Vous avez reçu '.$_POST['recompense'].'$. <br />';				
			}
		if($_POST['genre_recompense']=="pepite")
			{		
			$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
				$donnees = $reponse->fetch();
			$ors=$donnees['ors'];
			$ors_now=$ors+$_POST['recompense'];
			$reponse = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('ors' => $ors_now, 'pseudo' => $_SESSION['pseudo']));	
			echo 'Vous avez reçu '.$_POST['recompense'].'pépites. <br />';				
			}
		//passer à la quête suivante
		$next_stap=$etape_quete+1;
                $reponse = $bdd->prepare('SELECT id FROM pokemons_quetes_principales WHERE numero=:numero AND etape=:etape') or die(print_r($bdd->errorInfo()));
                $reponse->execute(array('numero' =>$numero_quete, 'etape'=>$next_stap));  
                $donnees = $reponse->fetch();
                if(isset($donnees['id'])){
                    $reponse2 = $bdd->prepare('UPDATE pokemons_membres SET quete_principale=:quete_principale, quete_principale_etape=:quete_principale_etape, quete_principale_active=0, nb_quete_principale=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
                    $reponse2->execute(array('quete_principale' => $numero_quete, 'quete_principale_etape' => $next_stap,'pseudo' => $_SESSION['pseudo']));                                                 
                }else{
                    $next_numero=$numero_quete+1;
                    $reponse = $bdd->prepare('SELECT id FROM pokemons_quetes_principales WHERE numero=:numero AND etape=1') or die(print_r($bdd->errorInfo()));
                    $reponse->execute(array('numero' =>$next_numero));  
                    $donnees = $reponse->fetch();
                    if(isset($donnees['id'])){
                        $reponse2 = $bdd->prepare('UPDATE pokemons_membres SET quete_principale=:quete_principale, quete_principale_etape=1, quete_principale_active=0, nb_quete_principale=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
                        $reponse2->execute(array('quete_principale' => $next_numero, 'pseudo' => $_SESSION['pseudo']));                                                 
                    }else{
                        echo '<b>La quête suivante n\'existe pas encore. Impossible de valider celle-ci.</b><br /><br />';
                    }
                }
                
		echo 'La quête a bien été validée. <br /><br /><a href="carte.php">Retour à la carte</a>';
		}
	else
		{
		if($permission_echange==0){echo 'Vous devez sélectionner un pokémon pour valider la quête. <br /><br /><a href="carte.php">Retour à la carte</a>';}
		elseif($permission_kill==0){echo 'Vous n\'avez pas vaincu suffisamment de pokémons. <br /><br /><a href="carte.php">Retour à la carte</a>';}		
		}
	}
?>

<?php

if($pos_hor_quete==$pos_hor AND $pos_ver_quete==$pos_ver OR $genre=="parler" AND $quete_principale_active==1)
	{
	if($quete_principale==$numero_quete AND $quete_principale_etape==$etape_quete) 
		{	
		if($genre=="attraper" OR $genre=="kill")//------ATTRAPER-------KILL------------
			{
			if($quete_principale_active==0) // pas encore lancé
				{
				?>
				<div id="cadre_membres_carte"><img src="images/quetes_principales/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
				<h3><?php echo $qui; ?> </h3>
				<div style="margin-left:10px; clear:both;">
				<p style="text-align:justify;"><i>"<?php echo nl2br($intro); ?>"</i></p><br />		
				<?php
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET quete_principale_active=1 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 	
				if($genre_recompense=="item")
					{
					$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('pseudo' => $_SESSION['pseudo'], 'id_item' => $recompense));  
						$donnees = $reponse->fetch();
					if(isset($donnees['id']))
						{
						$quantite_total=$nb_recompense + $donnees['quantite'];
						$reponse = $bdd->prepare('UPDATE pokemons_inventaire SET quantite=:quantite WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('quantite' =>$quantite_total ,'pseudo' => $_SESSION['pseudo'], 'id_item' => $recompense)); 
						}
					else
						{
						$req = $bdd->prepare('INSERT INTO pokemons_inventaire (pseudo, id_item, quantite) VALUES(:pseudo, :id_item, :quantite)') or die(print_r($bdd->errorInfo()));
						$req->execute(array('pseudo' => $_SESSION['pseudo'], 'id_item' => $recompense,'quantite' => $nb_recompense))or die(print_r($bdd->errorInfo()));
						}
					echo '<b>Vous avez reçu '.$nb_recompense.' '.$nom_recompense.'!</b><br />';
					}
				if($genre_recompense=="argent")
					{							
					$argent_now=$argent+$_POST['recompense'];
					$reponse = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar  WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pokedollar' => $argent_now, 'pseudo' => $_SESSION['pseudo']));		
					echo '<b>Vous avez reçu '.$recompense.'$!</b><br />';
					}
					?>
				</div>
				<form action="carte.php" method="post">
				<input type="submit" value="Retourner à la carte"/>	  
				</form>
				<?php
				}
			elseif($quete_principale_active==1) //pas encore terminée
				{
				?>
				<div id="cadre_membres_carte"><img src="images/quetes_principales/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
				<h3><?php echo $qui; ?> </h3>
				<div style="margin-left:10px; clear:both;">
				<p style="text-align:justify;"><i>"<?php echo nl2br($pas_reussi); ?>"</i></p>
				</div>
				<form action="carte.php" method="post">
				<input type="submit" value="Retourner à la carte"/>	  
				</form>
				<?php
				}
			}
		elseif($genre=="parler")//------PARLER-------
			{
			if($quete_principale_active==0) // pas encore lancé
				{
				?>
				<div id="cadre_membres_carte"><img src="images/quetes_principales/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
				<h3><?php echo $qui; ?> </h3>
				<div style="margin-left:10px; clear:both;">
				<p style="text-align:justify;"><i>"<?php echo nl2br($intro); ?>"</i></p><br />		
				<?php
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET quete_principale_active=1 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 	
				if($genre_recompense=="item")
					{
					$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('pseudo' => $_SESSION['pseudo'], 'id_item' => $recompense));  
						$donnees = $reponse->fetch();
					if(isset($donnees['id']))
						{
						$quantite_total=$nb_recompense + $donnees['quantite'];
						$reponse = $bdd->prepare('UPDATE pokemons_inventaire SET quantite=:quantite WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('quantite' =>$quantite_total ,'pseudo' => $_SESSION['pseudo'], 'id_item' => $recompense)); 
						}
					else
						{
						$req = $bdd->prepare('INSERT INTO pokemons_inventaire (pseudo, id_item, quantite) VALUES(:pseudo, :id_item, :quantite)') or die(print_r($bdd->errorInfo()));
						$req->execute(array('pseudo' => $_SESSION['pseudo'], 'id_item' => $recompense,'quantite' => $nb_recompense))or die(print_r($bdd->errorInfo()));
						}
					echo '<b>Vous avez reçu '.$nb_recompense.' '.$nom_recompense.'!</b><br />';
					}
				if($genre_recompense=="argent")
					{							
					$argent_now=$argent+$recompense;
					$reponse = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar  WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pokedollar' => $argent_now, 'pseudo' => $_SESSION['pseudo']));		
					echo '<b>Vous avez reçu '.$recompense.'$!</b><br />';
					}
				if($genre_recompense=="pepite")
					{		
					$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
						$donnees = $reponse->fetch();
					$ors=$donnees['ors'];
					$ors_now=$ors+$recompense;
					$reponse = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('ors' => $ors_now, 'pseudo' => $_SESSION['pseudo']));	
					echo 'Vous avez reçu '.$recompense.' pépites. <br />';				
					}
				if($genre_recompense=="autres")
					{	
					if($recompense=="bds")
						{
						$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
						$donnees = $reponse->fetch();
							$stockage_max=$donnees['bds'];
						$stockage_max=$stockage_max+$nb_recompense;
						$reponse = $bdd->prepare('UPDATE pokemons_membres SET bds=:bds WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
							$reponse->execute(array('bds' =>$stockage_max ,'pseudo' => $_SESSION['pseudo']));	
						echo '<b>Votre boite de stockage a été aggrandie de '.$nb_recompense.'!</b><br />';
						}
					}
					?>
				</div>
				<form action="carte.php" method="post">
				<input type="submit" value="Retourner à la carte"/>	  
				</form>
				<?php
				}
			elseif($quete_principale_active==1) //pas encore terminée
				{
				if($_POST['action']=="parler")
					{
                                        //$numero_quete
                                        //$etape_quete
                                        $next_stap=$etape_quete+1;
                                        $reponse = $bdd->prepare('SELECT id FROM pokemons_quetes_principales WHERE numero=:numero AND etape=:etape') or die(print_r($bdd->errorInfo()));
                                        $reponse->execute(array('numero' =>$numero_quete, 'etape'=>$next_stap));  
                                        $donnees = $reponse->fetch();
                                        if(isset($donnees['id'])){
                                            $reponse2 = $bdd->prepare('UPDATE pokemons_membres SET quete_principale=:quete_principale, quete_principale_etape=:quete_principale_etape, quete_principale_active=0, nb_quete_principale=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
                                            $reponse2->execute(array('quete_principale' => $numero_quete, 'quete_principale_etape' => $next_stap,'pseudo' => $_SESSION['pseudo']));                                                 
                                        }else{
                                            $next_numero=$numero_quete+1;
                                            $reponse = $bdd->prepare('SELECT id FROM pokemons_quetes_principales WHERE numero=:numero AND etape=1') or die(print_r($bdd->errorInfo()));
                                            $reponse->execute(array('numero' =>$next_numero));  
                                            $donnees = $reponse->fetch();
                                            if(isset($donnees['id'])){
                                                $reponse2 = $bdd->prepare('UPDATE pokemons_membres SET quete_principale=:quete_principale, quete_principale_etape=1, quete_principale_active=0, nb_quete_principale=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
                                                $reponse2->execute(array('quete_principale' => $next_numero, 'pseudo' => $_SESSION['pseudo']));                                                 
                                            }else{
                                                echo '<b>La quête suivante n\'existe pas encore. Impossible de valider celle-ci.</b><br /><br />';
                                            }
                                        }
			
					$reponse = $bdd->prepare('SELECT * FROM pokemons_pnj WHERE id_quete=:id_quete') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('id_quete' => $_GET['id']));  
					$donnees = $reponse->fetch();
					$qui_pnj=$donnees['qui'];
					$id_pnj=$donnees['id'];
					?>
					<div id="cadre_membres_carte"><img src="images/pnj/<?php echo $id_pnj ?>.jpg" height="150px" style="border:0;"/></div>
					<h3><?php echo $qui_pnj; ?> </h3>
					<div style="margin-left:10px; clear:both;">
					<p style="text-align:justify;">"<?php echo nl2br($reussite); ?>"</p>
					<form action="carte.php" method="post">
					<input type="submit" value="Retourner à la carte"/>	  
					</form>
					</div>
					<?php
					}
				else
					{
					?>
					<div id="cadre_membres_carte"><img src="images/quetes_principales/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
					<h3><?php echo $qui; ?> </h3>
					<div style="margin-left:10px; clear:both;">
					<p style="text-align:justify;">"<?php echo nl2br($pas_reussi); ?>"</p>
					<form action="carte.php" method="post">
					<input type="submit" value="Retourner à la carte"/>	  
					</form>
					</div>
					<?php
					}
				}
			}
		elseif($genre=="duel")//------DUEL-------
			{
			?>
			<div id="cadre_membres_carte"><img src="images/quetes_principales/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
			<h3><?php echo $qui; ?> </h3>
			<div style="margin-left:10px; clear:both;">
			<?php
			if($quete_principale_active==0)
				{ 
				echo '<p style="text-align:justify;"><i>"'.nl2br($intro).'"</i></p><br />';
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET quete_principale_active=1 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 	
				}
			else
				{
				echo '<p style="text-align:justify;"><i>"'.nl2br($pas_reussi).'"</i></p><br />';
				}
				?>
			</div>
			<?php
			if($pa_restant==0)
				{
				echo 'Votre pokémon actif est trop fatigué pour démarrer un duel';
				}
			else
				{
				?>
				<form action="quetes_combat.php" method="post">
				<input type="hidden" name="action" value="combat"/> 
				<input type="hidden" name="id_quete" value="<?php echo $what; ?>"/> 
				<input type="hidden" name="type_quete" value="principale"/> 
				<input type="hidden" name="entree" value="on"/> 
				<input type="submit" value="Accepter le défi"/>	  
				</form>
				<?php
				}
			?>
			<form action="carte.php" method="post">
			<input type="submit" value="Retourner à la carte"/>	  
			</form>
			<?php
			}	
		elseif($genre=="echange" AND $_POST['action']!="reussite") //-----ECHANGE------
			{
			if($quete_principale_active==0) // pas encore lancé
				{
				?>
				<div id="cadre_membres_carte"><img src="images/quetes_principales/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
				<h3><?php echo $qui; ?> </h3>
				<div style="margin-left:10px; clear:both;">
				<p style="text-align:justify;"><i>"<?php echo nl2br($intro); ?>"</i></p><br />		
				<?php
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET quete_principale_active=1 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 	
				?>
				</div>
				<form action="carte.php" method="post">
				<input type="submit" value="Retourner à la carte"/>	  
				</form>
				<?php
				}
			elseif($quete_principale_active==1) //pas encore terminée
				{
				$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE nom=:nom') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('nom' => $what));
				$donnees = $reponse->fetch();
				$id_pokemon_echange=$donnees['id'];
				$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id_pokemon=:id_pokemon AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id_pokemon' => $id_pokemon_echange, 'pseudo'=>$_SESSION['pseudo']));
				$donnees = $reponse->fetch();
				if(isset($donnees['id']))
					{
					?>
					<div id="cadre_membres_carte"><img src="images/quetes_principales/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
					<h3><?php echo $qui; ?> </h3>
					<p style="text-align:justify;">"<?php echo nl2br($reussite); ?>"</p>
					<div style="margin-left:10px; clear:both;">
					<form action="quetes_principales.php?id=<?php echo $_GET['id']; ?>" method="post">
					<?php
					$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id_pokemon=:id_pokemon AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('id_pokemon' => $id_pokemon_echange, 'pseudo'=>$_SESSION['pseudo']));
					while($donnees = $reponse->fetch())
						{
						echo '<input type="radio" name="id_pokemon" value="'.$donnees['id'].'">'.$what.' lvl '.$donnees['lvl'].'<br />';
						}
						?>
					<input type="hidden" name="action" value="reussite"/>
					<input type="hidden" name="genre" value="<?php echo $genre; ?>"/>
					<input type="hidden" name="genre_recompense" value="<?php echo $genre_recompense; ?>"/>
					<input type="hidden" name="recompense" value="<?php echo $recompense; ?>"/>
					<input type="hidden" name="nb_recompense" value="<?php echo $nb_recompense; ?>"/>
					<input type="submit" value="Échanger ce pokémon" />	  
					</form>
					</div>
					<?php
					}
				else
					{
					?>
					<div id="cadre_membres_carte"><img src="images/quetes_principales/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
					<h3><?php echo $qui; ?> </h3>
					<p style="text-align:justify;">"<?php echo nl2br($pas_reussi); ?>"</p>
					<?php
					}
				}
			}
		elseif($genre=="montrer" AND $_POST['action']!="reussite") //-----MONTRER------
			{
			if($quete_principale_active==0) // pas encore lancé
				{
				?>
				<div id="cadre_membres_carte"><img src="images/quetes_principales/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
				<h3><?php echo $qui; ?> </h3>
				<div style="margin-left:10px; clear:both;">
				<p style="text-align:justify;"><i>"<?php echo nl2br($intro); ?>"</i></p><br />		
				<?php
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET quete_principale_active=1 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 	
				?>
				</div>
				<form action="carte.php" method="post">
				<input type="submit" value="Retourner à la carte"/>	  
				</form>
				<?php
				}
			elseif($quete_principale_active==1) //pas encore terminée
				{
				$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('pseudo'=>$_SESSION['pseudo']));
				while($donnees = $reponse->fetch())
					{
					$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponse2->execute(array('id' => $donnees['id_pokemon']));
					$donnees2 = $reponse2->fetch();
					$id_pokedex=$donnees2['id_pokedex'];
					if($id_pokedex>=$pokedex_inf AND $id_pokedex<=$pokedex_sup){$autorisation_montrer=1;}
					}
				if($autorisation_montrer==1)
					{
					?>
					<div id="cadre_membres_carte"><img src="images/quetes_principales/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
					<h3><?php echo $qui; ?> </h3>
					<p style="text-align:justify;">"<?php echo nl2br($reussite); ?>"</p>
					<div style="margin-left:10px; clear:both;">
					<form action="quetes_principales.php?id=<?php echo $_GET['id']; ?>" method="post">
					<input type="hidden" name="action" value="reussite"/>
					<input type="hidden" name="genre" value="<?php echo $genre; ?>"/>
					<input type="hidden" name="genre_recompense" value="<?php echo $genre_recompense; ?>"/>
					<input type="hidden" name="recompense" value="<?php echo $recompense; ?>"/>
					<input type="hidden" name="nb_recompense" value="<?php echo $nb_recompense; ?>"/>
					<input type="submit" value="<?php echo $bouton_parler; ?>" />	  
					</form>
					</div>
					<?php
					}
				else
					{
					?>
					<div id="cadre_membres_carte"><img src="images/quetes_principales/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
					<h3><?php echo $qui; ?> </h3>
					<p style="text-align:justify;">"<?php echo nl2br($pas_reussi); ?>"</p>
					<?php
					}
				}
			}
		}
	else {echo 'Vous n\'avez pas accès à ces informations.';}
	}
else {echo 'Vous n\'avez pas accès à ces informations.';}
?>

	
	
	














<?php //fin sécurité
	}
	else
	{
	echo 'Une erreur c\'est produite. Retournez à <a href="carte.php">la carte</a>. Si cette erreur se reproduit, merci de contacter le webmaster.<br />';
	}
}
else
{
echo 'Vous devez être connecté pour accéder à cette page';
}
?>

<?php include ("bas.php"); ?>