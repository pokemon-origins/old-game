<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">


<?php
if(isset($_SESSION['pseudo']))
{
?>

<!-- Formulaire pour voir le pokémon -->
<h3>Quelle attaque désirez-vous voir? </h3>
<form method="post" action="pokedex_attaques.php">
<select name="id">
<?php
$reponse = $bdd->query('SELECT * FROM pokemon_base_attaques') or die(print_r($bdd->errorInfo())); 
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['id'].'"'; if($donnees['id']==$_GET['id'] OR $donnees['id']==$_POST['id'] ){echo 'selected="selected"';} echo '>'.$donnees['nom'].'</option>';
	}
?>
</select>	
<input type="hidden" name="action" value="voir_attaque"/> 
<input type="submit" value="Voir l'attaque" />	  
</form>

<?php //chargement de l'attaque
if(isset($_GET['id'])){$permission=1;}
if(isset($_POST['id'])){$permission=1;}
if($permission==1)
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	if(isset($_GET['id'])){$reponse->execute(array('id' => $_GET['id']));} else{$reponse->execute(array('id' => $_POST['id']));}
	$donnees = $reponse->fetch();
	$reponse3 = $bdd->prepare('SELECT * FROM pokemon_base_ct WHERE id_attaque=:id_attaque') or die(print_r($bdd->errorInfo()));
			$reponse3->execute(array('id_attaque' => $donnees['id']));
			$donnees3 = $reponse3->fetch();
	if(isset($donnees['id']))
		{
		?>
		<br />
		<table id="votre_pokemon" width="550px" cellpadding="2" cellspacing="2" style="text-align:center;">
		<tr><th colspan="9">Description</th></tr>
		<tr><td><?php echo $donnees['description']; ?></td></tr>
		</table>
		<br />
		<table id="votre_pokemon" width="550px" cellpadding="2" cellspacing="2" style="text-align:center;">
		<colgroup><COL WIDTH=35%><COL WIDTH=65%></COLGROUP>
		<tr><th colspan="9">Attributs</th></tr>
		<tr><td>Nom : </td> <td><?php echo $donnees['nom']; ?></td></tr>
		<tr><td>Type : </td><td><?php echo $donnees['type']; ?></td></tr> 
		<tr><td>Puissance : </td><td><?php echo $donnees['puissance']; ?></td></tr> 
		<tr><td>Précision : </td><td><?php if($donnees['prec']>100){echo '100';}else{echo $donnees['prec'];} ?></td></tr> 
		<tr><td>Classe : </td><td><?php echo $donnees['classe']; ?></td></tr> 
		<tr><td>CT/CS: </td><td><?php if(isset($donnees3['id'])){ echo $donnees3['num'];} else{echo 'Aucune';} ?></td></tr> 
		</table>
		<br />
		<table id="votre_pokemon" width="270px" cellpadding="2" cellspacing="2" style="text-align:center; float:left;">
		<tr><th colspan="9">Pokémons l'apprenant normalement </th></tr>
		<tr> <td>
		<?php
		$reponse2 = $bdd->query('SELECT * FROM pokemons_base_pokemons') or die(print_r($bdd->errorInfo()));
		while($donnees2 = $reponse2->fetch())
			{
			if($donnees2['lvl1']==$donnees['id'] OR $donnees2['lvl2']==$donnees['id'] OR $donnees2['lvl5']==$donnees['id'] OR $donnees2['lvl10']==$donnees['id'] OR $donnees2['lvl14']==$donnees['id'] OR $donnees2['lvl18']==$donnees['id'] OR $donnees2['lvl22']==$donnees['id'] OR $donnees2['lvl25']==$donnees['id'] OR $donnees2['lvl30']==$donnees['id'] OR $donnees2['lvl36']==$donnees['id'] OR $donnees2['lvl41']==$donnees['id'] OR $donnees2['lvl46']==$donnees['id'] OR $donnees2['lvl52']==$donnees['id']) 
				{echo '<a href=pokedex.php?pokemon='.$donnees2['id_pokedex'].'" style="color:black;">'.$donnees2['nom'].'</a> - ';}
			}
		?>
		</td></tr>
		</table>
		<?php
		if(isset($donnees3['id']))
			{
			?>
			<table id="votre_pokemon" width="280px" cellpadding="2" cellspacing="2" style="text-align:center; float:left;">
			<tr><th colspan="9">Pokémons l'apprenant par CT/CS </th></tr>
			<tr> <td>
			<?php
			$reponse2 = $bdd->query('SELECT * FROM pokemons_base_pokemons') or die(print_r($bdd->errorInfo()));
			while($donnees2 = $reponse2->fetch())
				{
				
				if($donnees2['ct1']==$donnees3['id'] OR $donnees2['ct2']==$donnees3['id'] OR $donnees2['ct3']==$donnees3['id'] OR $donnees2['ct4']==$donnees3['id'] OR $donnees2['ct5']==$donnees3['id'] OR $donnees2['ct6']==$donnees3['id'] OR $donnees2['ct7']==$donnees3['id'] OR $donnees2['ct8']==$donnees3['id'] OR $donnees2['ct9']==$donnees3['id'] OR $donnees2['ct10']==$donnees3['id'] OR $donnees2['ct11']==$donnees3['id'] OR $donnees2['ct12']==$donnees3['id'] OR $donnees2['ct13']==$donnees3['id'] OR $donnees2['ct14']==$donnees3['id'] OR $donnees2['ct15']==$donnees3['id']) 
					{echo $donnees2['nom'].' - ';}
				}
			?>
			</td></tr>		
			</table>
			<?php
			}
		}
	else {echo '<b>Cette attaque n\'existe pas dans notre base de données. </b><br />';}
	}
?>



<?php
}
else
{
echo 'Vous devez être connecté pour accéder à cette page';
}
?>
<?php include ("bas.php"); ?>