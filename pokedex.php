<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">

<?php
if(isset($_SESSION['pseudo']))
{
include ("include/maj_pokedex.php");

?>

   
<!-- Formulaire pour voir le pokémon -->
<h3>Statistiques </h3>
Vous possédez <?php echo $nb_pokemons_joueur; ?> pokémons au total. <br />
Vous avez déjà attrapé <?php echo $nb_pokemons_capture; ?> pokémons d'espèces différentes. <br /> 
Vous avez déjà attrapé <?php echo $nb_pokemons_shiney_capture; ?> pokémons shineys d'espèces différentes. <br /> 
<a href="pokedex_capture.php">Voir les détails </a><br />
<h3>Quel pokémon désirez-vous voir? </h3>
<form method="post" action="pokedex.php">
<select name="pokemon">
<?php
$reponse = $bdd->query('SELECT * FROM pokemons_base_pokemons ORDER BY id_pokedex') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['id_pokedex']; if($donnees['id_pokedex']==$_POST['pokemon'] OR $donnees['id_pokedex']==$_GET['pokemon']){echo 'selected="selected"';} echo '">'.$donnees['nom'].'</option>';
	}
?>
</select>	
<input type="hidden" name="action" value="voir_pokemon"/> 
<input type="submit" value="Voir le pokémon" />	  
</form>
<br />


<!-- Affichage du pokémon -->
<?php
if(isset($_GET['pokemon']) OR isset($_POST['pokemon']))
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));
	if(isset($_GET['pokemon'])){$reponse->execute(array('id_pokedex' => $_GET['pokemon']));} else{$reponse->execute(array('id_pokedex' => $_POST['pokemon']));}  
	$donnees = $reponse->fetch();
	$nom=$donnees['nom'];
	$id_pokedex=$donnees['id_pokedex'];
	$type1=$donnees['type1'];
	$type2=$donnees['type2'];
	$lvl_evo=$donnees['lvl_evo'];
	$description=$donnees['description'];
	$tdc=$donnees['tdc'];
	$multiplicateur_xp=$donnees['multiplicateur_xp'];
	$lvl1=$donnees['lvl1'];$lvl2=$donnees['lvl2'];$lvl5=$donnees['lvl5'];$lvl10=$donnees['lvl10'];$lvl14=$donnees['lvl14'];$lvl18=$donnees['lvl18'];
	$lvl22=$donnees['lvl22'];$lvl25=$donnees['lvl25'];$lvl30=$donnees['lvl30'];$lvl36=$donnees['lvl36'];$lvl41=$donnees['lvl41'];$lvl46=$donnees['lvl46'];$lvl52=$donnees['lvl52'];
	$ct1=$donnees['ct1'];
	$ct2=$donnees['ct2'];
	$ct3=$donnees['ct3'];
	$ct4=$donnees['ct4'];
	$ct5=$donnees['ct5'];
	$ct6=$donnees['ct6'];
	$ct7=$donnees['ct7'];
	$ct8=$donnees['ct8'];
	$ct9=$donnees['ct9'];
	$ct10=$donnees['ct10'];
	$ct11=$donnees['ct11'];
	$ct12=$donnees['ct12'];
	$ct13=$donnees['ct13'];
	$ct14=$donnees['ct14'];
	$ct15=$donnees['ct15'];
	if($lvl_evo!=0) 
		{
		$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id_pokedex' => $id_pokedex));  
		$donnees = $reponse->fetch();
		$nom_evo=$donnees['evo'];
		$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE nom=:nom') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('nom' => $nom_evo));  
		$donnees = $reponse->fetch();
		$id_pokedex_evo=$donnees['id_pokedex'];
		}
	?>
	
	<div style="margin-left:20px; float:left;">
	<span style="font-size:25px; margin-left:15px;"><?php echo $nom; ?></span>
	<br />
	<img src="images/pokemons/<?php echo $id_pokedex;?>.gif" height="150px" style="border-style:none; margin-left:4px; margin-top:10px; "/>
	</div>
	
	
	
	<div style="margin-left:215px;">
	<table id="votre_pokemon" width="333px" cellpadding="2" cellspacing="2" style="text-align:center;" >
	<colgroup><COL WIDTH=35%><COL WIDTH=65%></COLGROUP>
	<tr><th colspan="9">FICHE</th></tr>
	<tr><td>N° pokédex : </td> <td><?php echo $id_pokedex; ?></td></tr>
	<tr><td>Type : </td> <td><?php echo $type1; if($type2!="0"){echo ' / '.$type2;} ?> </td></tr>
	<tr><td>Prochaine évolution : </td><td><?php
  if($lvl_evo!=0)
  {
    if($lvl_evo!=69)
    {
      if($lvl_evo!=100)
      {
        echo $nom_evo.' au niveau '.$lvl_evo;
      }
      else
      {
        echo 'évolue par échange';
      }
    }
    else
    {
      if($nom=="evoli")
      {
        echo 'évolue suivant la pierre';
      }
      else
      {
        echo $nom_evo.' avec une pierre';
      }
    }
  }
  else
  {
    echo 'Pas d\'évolution';
  }
?></td></tr>
	</table>
	</div>
	
	<div style="clear:both;">
	<table id="votre_pokemon" width="550px" cellpadding="2" cellspacing="2"> 
	<tr><th colspan="9">DESCRIPTIF </th></tr>
	<tr><td><?php echo $description; ?> </td></tr>
	</table>
	</div>
	
	<div style="text-align:center;float:left;">
	<table id="votre_pokemon" width="270px" cellpadding="2" cellspacing="2"> 
	<colgroup><COL WIDTH=17%><COL WIDTH=83%></COLGROUP>
	<tr><th colspan="9">Liste des attaques apprises </th></tr>
	<tr><td> <b>Niveau </b></td> <td><b>Attaque </b></td></tr>
	<?php
	$reponse = $bdd->prepare('SELECT * FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id_pokedex' => $id_pokedex));  
	while($donnees = $reponse->fetch())
		{
		$reponse2 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['id_attaque']));  
		$donnees2 = $reponse2->fetch();
		echo '<tr><td>'.$donnees['lvl'].'</td> <td><a href="pokedex_attaques.php?id='.$donnees['id_attaque'].'" style="color:black;">'.$donnees2['nom'].'</a></td></tr>';
		}
	?>
	</table>
	</div>

	<div style="text-align:center;">
	<table id="votre_pokemon" width="280px" cellpadding="2" cellspacing="2"> 
	<colgroup><COL WIDTH=17%><COL WIDTH=83%></COLGROUP>
	<tr><th colspan="9">Liste des CT/CS  </th></tr>
	<?php
	$reponse = $bdd->prepare('SELECT * FROM pokemons_apprentissage_ct WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id_pokedex' => $id_pokedex));  
	while($donnees = $reponse->fetch())
		{
		$reponse2 = $bdd->prepare('SELECT * FROM pokemon_base_ct WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['id_ct']));  
		$donnees2 = $reponse2->fetch();
		$reponse3 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse3->execute(array('id' => $donnees2['id_attaque']));  
		$donnees3 = $reponse3->fetch();
		echo '<tr><td>'.$donnees2['num'].'</td> <td><a href="pokedex_attaques.php?id='.$donnees2['id_attaque'].'" style="color:black;">'.$donnees3['nom'].'</a></td></tr>';
		}
	?>
	</table>
	</div>
	
	<?php
	}
?>


<?php
}
else
{
echo 'Vous devez être connecté pour accéder à cette page';
}
?>

<?php include ("bas.php"); ?>