<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">

<?php //sécurité
if(isset($_SESSION['pseudo']))
{
if(isset($_GET['id']))
{
?>

<?php //acceptation de la quête
if($_POST['action']=="accepter")
	{
	$reponse = $bdd->prepare('UPDATE pokemons_membres SET quete_secondaire=:quete_secondaire, nb_quete_secondaire=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
			$reponse->execute(array('quete_secondaire' => $_GET['id'], 'pseudo' => $_SESSION['pseudo'])); 
	$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_secondaires WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $_GET['id']));  
			$donnees = $reponse->fetch();
	$repetitive=$donnees['repetitive'];
	$repetee=$donnees['repetee'];
	$difficulte_quete=$donnees['difficulte'];
	$repetee_now=$repetee+1;
	$reponse = $bdd->prepare('UPDATE pokemons_quetes_secondaires SET repetee=:repetee WHERE id=:id') or die(print_r($bdd->errorInfo())); 
	$reponse->execute(array('repetee' => $repetee_now, 'id' => $_GET['id'])); 		
	echo 'Vous avez accepté la quête! Vous pouvez voir son évolution dans votre journal des quêtes <br /> <br /><a href="carte.php">Retour à la carte</a>';
	}
?>
<?php //touchage de récompense
if($_POST['action']=="reussite")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
	$donnees = $reponse->fetch();
	$id_quete_secondaire_actuelle=$donnees['quete_secondaire'];
	$points_quetes=$donnees['points_quetes'];
	$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_secondaires WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_GET['id']));  
	$donnees = $reponse->fetch();
	$repetitive=$donnees['repetitive'];
	$repetee=$donnees['repetee'];
	$difficulte_quete=$donnees['difficulte'];
	$limite_inf=$donnees['limite_inf'];
	$limite_sup=$donnees['limite_sup'];
	$what=$donnees['what'];
	$nb_reussite=$donnees['nb_reussite'];
	$_POST['genre']=$donnees['genre'];
	$_POST['genre_recompense']=$donnees['genre_recompense'];
	$_POST['recompense']=$donnees['recompense'];
	$_POST['nb_recompense']=$donnees['nb_recompense'];
	if($id_quete_secondaire_actuelle==$_GET['id'])
		{
		if($id_quete_actuelle>=$limite_inf AND $id_quete_actuelle<=$limite_sup)
			{
			$permission_echange=1;
			$permission_kill=1;
			if($_POST['genre']=="echange")
					{
					$permission_echange=0;
					$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE nom=:nom') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('nom' => $what));
					$donnees = $reponse->fetch();
					$id_pokemon_echange=$donnees['id'];
					$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id=:id AND id_pokemon=:id_pokemon AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('id' => $_POST['id_pokemon'], 'id_pokemon' =>$id_pokemon_echange, 'pseudo'=>$_SESSION['pseudo']));
					$donnees = $reponse->fetch();
					if(isset($donnees['id']))
						{
						$req = $bdd->prepare('DELETE FROM pokemons_liste_pokemons WHERE id=:id AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
						$req->execute(array('id' => $_POST['id_pokemon'],'pseudo' =>$_SESSION['pseudo'])) or die(print_r($bdd->errorInfo()));	
						$permission_echange=1;
						}
					}
			if($_POST['genre']=="kill")
					{
					if($nb_quete_secondaire<$nb_reussite)
						{
						$permission_kill=0;
						}
					}
			if($permission_echange==1 AND $permission_kill==1)
				{
				if($_POST['genre_recompense']=="item")
					{
					$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('pseudo' => $_SESSION['pseudo'], 'id_item' => $_POST['recompense']));  
						$donnees = $reponse->fetch();
					if(isset($donnees['id']))
						{
						$quantite_total=$_POST['nb_recompense'] + $donnees['quantite'];
						$reponse = $bdd->prepare('UPDATE pokemons_inventaire SET quantite=:quantite WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('quantite' =>$quantite_total ,'pseudo' => $_SESSION['pseudo'], 'id_item' => $_POST['recompense'])); 
						}
					else
						{
						$req = $bdd->prepare('INSERT INTO pokemons_inventaire (pseudo, id_item, quantite) VALUES(:pseudo, :id_item, :quantite)') or die(print_r($bdd->errorInfo()));
						$req->execute(array('pseudo' => $_SESSION['pseudo'], 'id_item' => $_POST['recompense'],'quantite' => $_POST['nb_recompense']))or die(print_r($bdd->errorInfo()));
						}
					$reponse = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('id' => $_POST['recompense']));  
					$donnees = $reponse->fetch();
					echo 'Vous avez reçu '.$_POST['nb_recompense'].' '.$donnees['nom'].'. <br />';			
					}
				if($_POST['genre_recompense']=="argent")
					{		
					$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
					$donnees = $reponse->fetch();
					$argent=$donnees['pokedollar'];
					$argent_now=$argent+$_POST['recompense'];
					$reponse = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar  WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pokedollar' => $argent_now, 'pseudo' => $_SESSION['pseudo']));		
					echo '<b>Vous avez reçu '.$_POST['recompense'].'$!</b><br />';
					}
				if($_POST['genre_recompense']=="pepite")
					{		
					$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
					$donnees = $reponse->fetch();
					$ors=$donnees['ors'];
					$ors_now=$ors+$_POST['recompense'];
					$reponse = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('ors' => $ors_now, 'pseudo' => $_SESSION['pseudo']));		
					echo 'Vous avez reçu '.$_POST['recompense'].'pépites. <br />';
					}
				$difficulte_quete_now=$difficulte_quete+$points_quetes;
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET quete_secondaire=0, nb_quete_secondaire=0, points_quetes=:points_quetes WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
				$reponse->execute(array('pseudo' => $_SESSION['pseudo'], 'points_quetes' => $difficulte_quete_now)); 
				echo '<br /> <br /><a href="carte.php">Retour à la carte</a>';
				}
			else
				{
				if($permission_echange==0){echo 'Vous devez sélectionner un pokémon pour valider la quête. <br /><br /><a href="carte.php">Retour à la carte</a>';}
				elseif($permission_kill==0){echo 'Vous n\'avez pas vaincu suffisamment de pokémons. <br /><br /><a href="carte.php">Retour à la carte</a>';}
				}
			}
		else
			{
			echo 'Vous n\'avez pas encore accès à cette quête!';
			}
		}
	else
		{
		echo 'Il est interdit d\'actualiser la page des récompenses!';
		}
	}
?>


<?php //ouverture quete
$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_secondaires WHERE id=:id AND pos_hor=:pos_hor AND pos_ver=:pos_ver') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_GET['id'], 'pos_hor'=>$ph, 'pos_ver'=>$pv));  
	$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{
	$pos_hor=$donnees['pos_hor'];
	$pos_ver=$donnees['pos_ver'];
	$qui=$donnees['qui'];
	$genre=$donnees['genre'];
	$repetitive=$donnees['repetitive'];
	$repetee=$donnees['repetee'];
	$what=$donnees['what'];
	$condition_reussite=$donnees['condition_reussite'];
	$condition_reussite2=$donnees['condition_reussite2'];
	$nb_reussite=$donnees['nb_reussite'];
	$intro=$donnees['intro'];
	$complet=$donnees['complet'];
	$reussite=$donnees['reussite'];
	$pas_reussi=$donnees['pas_reussi'];
	$genre_recompense=$donnees['genre_recompense'];
	$recompense=$donnees['recompense'];
	$nb_recompense=$donnees['nb_recompense'];
	$heure=$donnees['heure'];
	$heure_now = date("G");
	if($heure != $heure_now)
		{ //actualisation quete
		$reponse = $bdd->prepare('UPDATE pokemons_quetes_secondaires SET heure=:heure, repetee=0 WHERE id=:id') or die(print_r($bdd->errorInfo())); 
		$reponse->execute(array('heure' => $heure_now, 'id' => $_GET['id'])); 
		$repetee=0;
		}
	}
else
	{
	$interdiction_quete=1;
	echo 'c\'est mal de tricher avec les liens! <br />';
	}
?>

<?php

if($_POST['action']!="reussite" AND $_POST['action']!="accepter" AND $interdiction_quete!=1)
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
	$donnees = $reponse->fetch();
	$id_quete_secondaire_actuelle=$donnees['quete_secondaire'];
	$nb_quete_secondaire_actuelle=$donnees['nb_quete_secondaire'];
	$nb_duel=$donnees['nb_duel'];
	$bonus_duel=$donnees['bonus_duel'];
	$time=time();
	if($bonus_duel>$time){$pnj_max=15;}else{$pnj_max=5;$reponse = $bdd->prepare('UPDATE pokemons_membres SET bonus_duel=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));$reponse->execute(array('pseudo' => $_SESSION['pseudo']));}
	if($id_quete_secondaire_actuelle!=$_GET['id']) // quête pas encore acceptée, fonctionne pour tout genre de quete
		{
		if($repetitive<=$repetee)
			{
			$heure_refresh=$heure+1;
			?>
			<div id="cadre_membres_carte"><img src="images/quetes_secondaires/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
			<h3><?php echo $qui; ?> </h3>
			<div style="margin-left:10px; clear:both;">
			<p style="text-align:justify;">"<?php echo nl2br($complet); ?>"</p><br />
			<form action="carte.php" method="post">
			<input type="submit" value="Retourner à la carte"/>	  
			</form>
			</div>
			
			<?php
			}
		else
			{
			if($genre=="duel")
				{
				?>	
				<div id="cadre_membres_carte"><img src="images/quetes_secondaires/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
				<h3><?php echo $qui; ?> </h3>
				<div style="margin-left:10px; clear:both;">
				<p style="text-align:justify;">"<?php echo nl2br($intro); ?>"</p>
				<?php 
				if($pa_restant==0)
					{
					echo 'Votre pokémon actif est trop fatigué pour démarrer un duel';
					}
				elseif($nb_duel>=$pnj_max)
					{
					echo 'Vous avez déjà fait votre maximum de duel pour aujourd\'hui.';
					}
				else
					{
					$nb_duel_restant=$pnj_max-$nb_duel;
					echo 'Vous pouvez encore participer à '.$nb_duel_restant.' duel'; if($nb_duel_restant>1){echo 's';} echo ' aujourd\'hui. <br />';
					?>
					<form action="quetes_combat.php" method="post">
					<input type="hidden" name="action" value="combat"/> 
					<input type="hidden" name="id_quete" value="<?php echo $what; ?>"/> 
					<input type="hidden" name="type_quete" value="secondaire"/> 
					<input type="hidden" name="numero_quete" value="<?php echo $_GET['id']; ?>"/> 
					<input type="hidden" name="entree" value="on"/> 
					<input type="submit" value="Accepter ce défi"/> <a href="http://pokemon-origins.forumactif.com/t64-faq#129" target="_blank"><img src="images/interrogation.png" style="border:0;position:absolute;margin-top:1px;" /></a>
					</form>
					<?php
					}
				?>
				<form action="carte.php" method="post">
				<input type="submit" value="Refuser ce défi"/>	  
				</form>
				</div>
				<?php	
				}
			else
				{
				?>	
				<div id="cadre_membres_carte"><img src="images/quetes_secondaires/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
				<h3><?php echo $qui; ?> </h3>
				<div style="margin-left:10px; clear:both;">
				<p style="text-align:justify;">"<?php echo nl2br($intro); ?>"</p>
				<form action="quetes_secondaires.php?id=<?php echo $_GET['id']; ?>" method="post">
				<input type="hidden" name="action" value="accepter"/> 
				<input type="submit" value="Accepter cette quête"/>	  
				</form>
				<form action="carte.php" method="post">
				<input type="submit" value="Refuser cette quête"/>	  
				</form>
				</div>
				<?php	
				}
			}
		}
	else //quête déjà acceptée
		{
		if($genre=="kill") //quete de kill
			{
			if($nb_quete_secondaire_actuelle==$nb_reussite) //réussi
				{
				?>
				<div id="cadre_membres_carte"><img src="images/quetes_secondaires/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
				<h3><?php echo $qui; ?> </h3>
				<div style="margin-left:10px; clear:both;">
				<p style="text-align:justify;">"<?php echo nl2br($reussite); ?>"</p>
				<form action="quetes_secondaires.php?id=<?php echo $_GET['id']; ?>" method="post">
				<input type="hidden" name="genre" value="<?php echo $genre; ?>"/>
				<input type="hidden" name="genre_recompense" value="<?php echo $genre_recompense; ?>"/>
				<input type="hidden" name="recompense" value="<?php echo $recompense; ?>"/>
				<input type="hidden" name="nb_recompense" value="<?php echo $nb_recompense; ?>"/>
				<input type="hidden" name="action" value="reussite"/> 
				<input type="submit" value="Toucher la récompense"/>	  
				</form>
				</div>	
				<?php
				}
			else //pas encore réussi
				{
				?>
				<div id="cadre_membres_carte"><img src="images/quetes_secondaires/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
				<h3><?php echo $qui; ?> </h3>
				<div style="margin-left:10px; clear:both;">
				<p style="text-align:justify;">"<?php echo nl2br($pas_reussi); ?>"</p>
				<form action="carte.php" method="post">
				<input type="submit" value="Retourner à la carte"/>	  
				</form></div>
				<?php
				}
			}
		elseif($genre=="echange" AND $_POST['action']!="reussite") //quete d'échanges
			{
			$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE nom=:nom') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('nom' => $what));
			$donnees = $reponse->fetch();
			$id_pokemon_echange=$donnees['id'];
			$perm=0;
			$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id_pokemon=:id_pokemon AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id_pokemon' => $id_pokemon_echange, 'pseudo'=>$_SESSION['pseudo']));
			while($donnees = $reponse->fetch())
				{
				if($condition_reussite==""){$perm=1;}
				if($condition_reussite=="shiney" AND $donnees['shiney']==1){$perm=1;}
				if($condition_reussite=="superieur" AND $donnees['lvl']>$condition_reussite2){$perm=1;}
				if($condition_reussite=="inferieur" AND $donnees['lvl']<$condition_reussite2){$perm=1;}
				}
			if($perm==1)
				{
				?>
				<div id="cadre_membres_carte"><img src="images/quetes_secondaires/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
				<h3><?php echo $qui; ?> </h3>
				<p style="text-align:justify;">"<?php echo nl2br($reussite); ?>"</p>
				<div style="margin-left:10px; clear:both;">
				<form action="quetes_secondaires.php?id=<?php echo $_GET['id']; ?>" method="post">
				<?php
				$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id_pokemon=:id_pokemon AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id_pokemon' => $id_pokemon_echange, 'pseudo'=>$_SESSION['pseudo']));
				while($donnees = $reponse->fetch())
					{
					$perm=0;
					if($condition_reussite==""){$perm=1;}
					if($condition_reussite=="shiney" AND $donnees['shiney']==1){$perm=1;}
					if($condition_reussite=="superieur" AND $donnees['lvl']>$condition_reussite2){$perm=1;}
					if($condition_reussite=="inferieur" AND $donnees['lvl']<$condition_reussite2){$perm=1;}
					if($perm==1)
						{
						echo '<input type="radio" name="id_pokemon" value="'.$donnees['id'].'">'.$what.' lvl '.$donnees['lvl'].' (<a href="vos_pokemons.php?id='.$donnees['id'].'" target="_blank">voir</a>)<br />';
						}
					}
					?>
				<input type="hidden" name="action" value="reussite"/>
				<input type="hidden" name="genre" value="<?php echo $genre; ?>"/>
				<input type="hidden" name="genre_recompense" value="<?php echo $genre_recompense; ?>"/>
				<input type="hidden" name="recompense" value="<?php echo $recompense; ?>"/>
				<input type="hidden" name="nb_recompense" value="<?php echo $nb_recompense; ?>"/>
				<input type="submit" value="Échanger ce pokémon" />	  
				</form>
				</div>
				<?php
				}
			else //pas encore réussi
				{
				?>
				<div id="cadre_membres_carte"><img src="images/quetes_secondaires/<?php echo $_GET['id'] ?>.jpg" height="150px" style="border:0;"/></div>
				<h3><?php echo $qui; ?> </h3>
				<p style="text-align:justify;">"<?php echo nl2br($pas_reussi); ?>"</p>
				<div style="margin-left:10px; clear:both;">
				<form action="carte.php" method="post">
				<input type="submit" value="Retourner à la carte"/>	  
				</form></div>
				<?php
				}	
			}
		}
	}
?>

	
	
	














<?php //fin sécurité
}
else
{
echo 'Une erreur c\'est produite. Retournez à <a href="carte.php">la carte</a>. Si cette erreur se reproduit, merci de contacter le webmaster.<br />';
}
}
else
{
echo 'Vous devez être connecté pour accéder à cette page';
}
?>

<?php include ("bas.php"); ?>