# Jeu Pokemon-Origins

Ce projet contient le code source et base de donnée "fonctionnelle" du jeu Pokemon-Origins.

Pour plus d'information sur ce jeu-vidéo, voici le site faisant honneur aux anciens joueurs et au site lui même:
[https://pokemon-origins.gitlab.io/pantheon/](https://pokemon-origins.gitlab.io/pantheon/)


## Mise en garde

Ce site jeu-video ne doit en aucun cas être publié sur internet.
Vous pouvez cependant l'installer en local chez vous à titre d'information ou pour la nostalgie.

Ce site n'est absolument pas optimisé et stocke les mots de passe en clair.
Il est donc non recommandé de prendre ce site comme exemple pour la partie identification et sécurité de votre potentiel projet.


## Comment l'installer sur Windows


1) Installer et lancer [Wamp Server](https://sourceforge.net/projects/wampserver/), l'icone de WampServer doit être vert dans la barre des applications.

<img src="doc/wamp.png" width="300">


2) Lancer [PhpMyAdmin](http://localhost/phpmyadmin), rentrez le login `root` et pas de mot de passe pour vous connecter puis créez une nouvelle base de données nommée `po` et enfin importez le fichier `database/po.sql`

<img src="doc/phpmyadmin.png" width="300">
<img src="doc/phpmyadmin2.png" width="300">
<img src="doc/phpmyadmin3.png" width="300">


3) Dans le dossier existant `C:/wamp64/www`, décompressez tout le [projet](https://gitlab.com/pokemon-origins/old-game/-/archive/master/old-game-master.zip) dans un dossier `po`

<img src="doc/www.png" width="300">
<img src="doc/repertoire.png" width="300">


4) Sur votre navigateur internet, tapez l'url [http://localhost/po](http://localhost/po) puis entrez le login `admin` et le mot de passe `admin` afin de pouvoir jouer au jeu.

<img src="doc/login.png" width="300">



## License

Ce projet est sous license MIT.
Cependant, de nombreuses images et concepts sont tirés de Pokemon (Copyright: Pokemon Company).
Il vous est donc interdit de publier ce projet sur internet. Ce projet est uniquement là pour être pérénisé et à titre d'information.
