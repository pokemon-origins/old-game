<?php
  // Start session
  ini_set('session.use_trans_sid', 0);
  session_start();
 
  ini_set("display_errors", 0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<head>
<link rel="icon" type="image/png" href="images/pokeball.png" />
<link rel="stylesheet" media="screen" type="text/css" title="sheikoh" href="design/design_pokemons.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META name="description" content="Mmorpg sur les pokemons de la première deuxième et troisième générations">
<meta name="keywords" content="pokemons, mmorpg, attaque, pv, pokedex, shop, pokecentre, monstre, capture, elevage, combat, dressage, dresseur, collection, chen, sacha, ondine, pierre, badge, arêne, pokemon, battle">
<title>Pokemon Origins</title>
</head>


<?php	   
if($_SESSION['pseudo']=="admin")
{
try {$bdd = new PDO('mysql:host=127.0.0.1;dbname=po','root','');    
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);} 
catch(PDOException $e){ echo 'Erreur : '.$e->getMessage();}	
}
else
{
try {$bdd = new PDO('mysql:host=127.0.0.1;dbname=po','root','');} catch(Exception $e){ die('Erreur : '.$e->getMessage());}	
}

//données génériques
$time_start = microtime(true); 
$time = time();
$time_dead = $time +300;
$time_begin = $time -1800;
$jour = date('d');
$heure = date('H');


//sécurité chargement
if($time-$_SESSION['time_last_chargement']<1){
    echo 'Il est interdit de rafraichir la page directement';
    exit;
}else{
    $_SESSION['time_last_chargement']=$time;
}
    



//identification
$connection_error=0;
if(isset($_POST['action']) AND $_POST['action']=="identification")
{
$reponse = $bdd->prepare('SELECT pseudo, mdp, pos_ver, pos_hor, mail, mdp, id, back, ors FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => stripslashes($_POST['pseudo'])));
$donnees = $reponse->fetch();
if (htmlspecialchars($donnees['pseudo'])==stripslashes($_POST['pseudo']) and htmlspecialchars($donnees['mdp'])==$_POST['mdp'])
	{
	if($donnees['pos_ver']==1000 AND $donnees['pos_hor']==1000)
		{
		$connection_error=2;
		}
	$reponse2 = $bdd->query('SELECT pseudo FROM comptes_bloques') or die(print_r($bdd->errorInfo())); //compte bloqué
	while($donnees2 = $reponse2->fetch())
		{
		//if($_POST['pseudo']==$donnees2['pseudo']){$connection_error=3;}
		}
	if($connection_error==0)
		{
		$_SESSION['page_combat']="NULL";
		$_SESSION['pseudo'] = $donnees['pseudo'];
		$_SESSION['mail'] = $donnees['mail'];
		$_SESSION['mdp'] = $donnees['mdp'];
		$_SESSION['id_membre'] = $donnees['id'];
		$back=$donnees['back'];
		$ors=$donnees['ors'];
		$time = time();
		$adresse_ip=$_SERVER['REMOTE_ADDR'];
		$reponse = $bdd->prepare('UPDATE pokemons_membres SET adresse_ip=:adresse_ip WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
			   $reponse->execute(array('adresse_ip' => $adresse_ip, 'pseudo' => $_SESSION['pseudo'])); 
		if($back==0)
			{
			$ors=$ors+50;
			$reponse = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors, back=1 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
			   $reponse->execute(array('ors' => $ors, 'pseudo' => $_SESSION['pseudo'])); 
			$back_do=1;
			}
		}
	}
else
	{
	$connection_error=1;
	}
}

?>
<body> 
<div id="corps">
<div id="header"></div>
		

