<?php
$heure = date('H');
$reponse = $bdd->query('SELECT jour FROM pokemons_horloge WHERE what="elevage"') or die(print_r($bdd->errorInfo()));	           
$donnees = $reponse->fetch();
if ($heure != $donnees['jour'])
    {
    $req = $bdd->prepare('UPDATE pokemons_horloge SET jour=:jour WHERE what="elevage"') or die(print_r($bdd->errorInfo()));
    $req->execute(array('jour' => $heure));
    
    $reponse = $bdd->query('SELECT pseudo, bds, bonus_naissance, proba_naissance FROM pokemons_membres WHERE quete_principale>9 ORDER BY id ASC') or die(print_r($bdd->errorInfo())); 
    while($donnees = $reponse->fetch())
            {		
            $proba_naissance=$donnees['proba_naissance'];
            $bonus_naissance=$donnees['bonus_naissance'];
            $pseudo_oeuf=$donnees['pseudo'];
            $bds_oeuf=$donnees['bds'];
            if($bonus_naissance>$time){$bonus_naissance="ok";}
            else{$reponse4 = $bdd->prepare('UPDATE pokemons_membres SET bonus_naissance=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));$reponse4->execute(array('pseudo' => $donnees['pseudo']));}
            $nb_pokemons=0;
            $reponse2 = $bdd->prepare('SELECT id FROM pokemons_elevage WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
            $reponse2->execute(array('pseudo' => $pseudo_oeuf));
            while($donnees2 = $reponse2->fetch()){$nb_pokemons=$nb_pokemons+1;}
            if($nb_pokemons==2)
                    {
                    $reponse3 = $bdd->prepare('SELECT id_pokemon,sexe FROM pokemons_elevage WHERE pseudo=:pseudo ORDER BY id DESC') or die(print_r($bdd->errorInfo()));
                    $reponse3->execute(array('pseudo' => $pseudo_oeuf));
                    $donnees3 = $reponse3->fetch();
                    //sécurité latias et latios
                    if($donnees3['id_pokemon']==395 OR $donnees3['id_pokemon']==396){
                        $donnees3['sexe']=NULL;
                    }
                    $reponse4 = $bdd->prepare('SELECT id_pokemon, sexe FROM pokemons_elevage WHERE pseudo=:pseudo ORDER BY id ASC') or die(print_r($bdd->errorInfo()));
                    $reponse4->execute(array('pseudo' => $pseudo_oeuf));
                    $donnees4 = $reponse4->fetch();
                    //sécurité latias et latios
                    if($donnees4['id_pokemon']==395 OR $donnees4['id_pokemon']==396){
                        $donnees4['sexe']=NULL;
                    }
                    if($donnees3['sexe']!=$donnees4['sexe'])
                            {
                            $nb_pokemons_joueur=0;
                            $reponse6 = $bdd->prepare('SELECT id FROM pokemons_liste_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
                            $reponse6->execute(array('pseudo' => $pseudo_oeuf));
                            while($donnees6 = $reponse6->fetch())
                                    {
                                    $nb_pokemons_joueur=$nb_pokemons_joueur+1;
                                    }
                            //calcul des taux de capture
                            $reponse10 = $bdd->prepare('SELECT id, nom FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
                            $reponse10->execute(array('id' => $donnees3['id_pokemon']));
                            $donnees10 = $reponse10->fetch();
                            $id_pokemon_1=$donnees10['id'];
                            $nom_pokemon_1=$donnees10['nom'];
                            $reponse11 = $bdd->prepare('SELECT id, nom FROM pokemons_base_pokemons WHERE evo=:evo') or die(print_r($bdd->errorInfo()));
                            $reponse11->execute(array('evo' => $nom_pokemon_1));
                            $donnees11 = $reponse11->fetch();
                            if(isset($donnees11['id'])){$nom_pokemon_1=$donnees11['nom'];$id_pokemon_1=$donnees11['id'];}
                            $reponse12 = $bdd->prepare('SELECT id, nom FROM pokemons_base_pokemons WHERE evo=:evo') or die(print_r($bdd->errorInfo()));
                            $reponse12->execute(array('evo' => $nom_pokemon_1));
                            $donnees12 = $reponse12->fetch();
                            if(isset($donnees12['id'])){$nom_pokemon_1=$donnees12['nom'];$id_pokemon_1=$donnees12['id'];}
                            if($id_pokemon_1==145 OR $id_pokemon_1==146 OR $id_pokemon_1==147 OR $id_pokemon_1==207 OR $id_pokemon_1==208 OR $id_pokemon_1==493 OR $id_pokemon_1==494){$id_pokemon_1=144;}
                            if($id_pokemon_1==115 OR $id_pokemon_1==116 OR $id_pokemon_1==248){$id_pokemon_1=247;}
                            $reponse13 = $bdd->prepare('SELECT tdc FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
                            $reponse13->execute(array('id' => $id_pokemon_1));
                            $donnees13 = $reponse13->fetch();
                            $tdc_1=$donnees13['tdc']; //taux de capture du 1er pokémon
                            $reponse10 = $bdd->prepare('SELECT id, nom FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
                            $reponse10->execute(array('id' => $donnees4['id_pokemon']));
                            $donnees10 = $reponse10->fetch();
                            $id_pokemon_2=$donnees10['id'];
                            $nom_pokemon_2=$donnees10['nom'];
                            $reponse11 = $bdd->prepare('SELECT id,nom FROM pokemons_base_pokemons WHERE evo=:evo') or die(print_r($bdd->errorInfo()));
                            $reponse11->execute(array('evo' => $nom_pokemon_2));
                            $donnees11 = $reponse11->fetch();
                            if(isset($donnees11['id'])){$nom_pokemon_2=$donnees11['nom'];$id_pokemon_2=$donnees11['id'];}
                            $reponse12 = $bdd->prepare('SELECT id, nom FROM pokemons_base_pokemons WHERE evo=:evo') or die(print_r($bdd->errorInfo()));
                            $reponse12->execute(array('evo' => $nom_pokemon_2));
                            $donnees12 = $reponse12->fetch();
                            if(isset($donnees12['id'])){$nom_pokemon_2=$donnees12['nom'];$id_pokemon_2=$donnees12['id'];}
                            if($id_pokemon_2==145 OR $id_pokemon_2==146 OR $id_pokemon_2==147  OR $id_pokemon_2==207 OR $id_pokemon_2==208){$id_pokemon_2=144;}
                            if($id_pokemon_2==115 OR $id_pokemon_2==116 OR $id_pokemon_2==248){$id_pokemon_2=247;}
                            $reponse13 = $bdd->prepare('SELECT tdc FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
                            $reponse13->execute(array('id' => $id_pokemon_2));
                            $donnees13 = $reponse13->fetch();
                            $tdc_2=$donnees13['tdc']; //taux de capture du second pokémon
                            $dd_naissance= $tdc_1+$tdc_2; $dd_naissance=sqrt($dd_naissance*4);$dd_naissance=$dd_naissance-1;$dd_naissance = 10000/$dd_naissance;$dd_naissance=ceil($dd_naissance);$dd_naissance=$dd_naissance*35;
                            if($id_pokemon_1==$id_pokemon_2){$dd_naissance=$dd_naissance*3/4;}
                            $rand_naissance=rand(1,$dd_naissance);
                            if($rand_naissance<$proba_naissance AND $nb_pokemons_joueur<$bds_oeuf)
                                    {
                                    //calcul id du pokémon
                                    $tdc_total=$tdc_1+$tdc_2;
                                    $tdc_rand=rand(1,$tdc_total);
                                    if($tdc_rand<=$tdc_1){$id_pokemon=$id_pokemon_1;}else{$id_pokemon=$id_pokemon_2;}
                                    //calcul stats
                                    $rand=rand(1,250);if($rand==1){$shiney=1;}else{$shiney=0;}
                                    $reponse5 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
                                    $reponse5->execute(array('id' => $id_pokemon));
                                    $donnees5 = $reponse5->fetch();
                                    $id_pokedex_new=$donnees5['id_pokedex'];
                                    if($donnees5['sexe']=="Y"){$proba_sexe=rand(1,2); if($proba_sexe==1){$sexe="M";} elseif($proba_sexe==2){$sexe="F";}}
                                    if($donnees5['sexe']=="M"){$sexe="M";}
                                    if($donnees5['sexe']=="F"){$sexe="F";}
                                    if($donnees5['sexe']=="N"){$sexe="";}
                                    $randpv=rand(0,4);$randatt=rand(0,4);$randdef=rand(0,4);$randvit=rand(0,4);$randattspe=rand(0,4);$randdefspe=rand(0,4);
                                    $pv=$donnees5['pv']+4*$donnees5['bonus_pv']+$randpv;
                                    $att=$donnees5['att']+4*$donnees5['bonus_att']+$randatt;
                                    $def=$donnees5['def']+4*$donnees5['bonus_def']+$randdef;
                                    $vit=$donnees5['vit']+4*$donnees5['bonus_vit']+$randvit;
                                    $attspe=$donnees5['attspe']+4*$donnees5['bonus_attspe']+$randattspe;
                                    $defspe=$donnees5['defspe']+4*$donnees5['bonus_defspe']+$randdefspe;
                                    $attaque1=0;$attaque2=0;$attaque3=0;$attaque4=0;$count1=0;$count2=0;$count3=0;$count4=0;$lvl=5;
                                    while($attaque1==0 AND $count1<4)
                                            {
                                            $count_attaque=0;
                                            $reponse = $bdd->prepare('SELECT id FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                                            $reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
                                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;}
                                            $rand_attaque=rand(0,$count_attaque);
                                            $count_attaque=0;
                                            $reponse = $bdd->prepare('SELECT id_attaque FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                                            $reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
                                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;if($count_attaque==$rand_attaque){$attaque1=$donnees['id_attaque'];}}
                                            $count1=$count1+1;
                                            }
                                    while($attaque2==0 AND $count2<5 OR $count2<5 AND $attaque2==$attaque1)
                                            {
                                            $count_attaque=0;
                                            $reponse = $bdd->prepare('SELECT id FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                                            $reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
                                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;}
                                            $rand_attaque=rand(0,$count_attaque);
                                            $count_attaque=0;
                                            $reponse = $bdd->prepare('SELECT id_attaque FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                                            $reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
                                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;if($count_attaque==$rand_attaque){$attaque2=$donnees['id_attaque'];}}
                                            $count2=$count2+1;
                                            }
                                    if($attaque1==$attaque2){$attaque2=0;}
                                    while($attaque3==0 AND $count3<7 OR $attaque3==$attaque1 AND $count3<7 OR $attaque3==$attaque2 AND $count3<7)
                                            {
                                            $count_attaque=0;
                                            $reponse = $bdd->prepare('SELECT id FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                                            $reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
                                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;}
                                            $rand_attaque=rand(0,$count_attaque);
                                            $count_attaque=0;
                                            $reponse = $bdd->prepare('SELECT id_attaque FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
                                            $reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
                                            while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;if($count_attaque==$rand_attaque){$attaque3=$donnees['id_attaque'];}}
                                            $count3=$count3+1;
                                            }
                                    if($attaque3==$attaque2 OR $attaque3==$attaque1){$attaque3=0;}
                                    $req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons (pseudo, id_pokemon, shiney, sexe, lvl, xp, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, actif, pa_restant, pa_max, pa_bonus, fin_dodo, parent_1, parent_2, bonheur) VALUES(:pseudo, :id_pokemon, :shiney, :sexe, 0, 100, :pv,:pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, 0, 0, 10, 10, 5, 750, :parent_1, :parent_2, 120)') or die(print_r($bdd->errorInfo()));
                                                    $req->execute(array(
                                                    'pseudo' => $pseudo_oeuf, 
                                                    'id_pokemon' => $id_pokemon,
                                                    'shiney' => $shiney,
                                                    'sexe' => $sexe,
                                                    'pv' => $pv,
                                                    'pv_max' => $pv,
                                                    'att' => $att,
                                                    'def' => $def,
                                                    'vit' => $vit,
                                                    'attspe' => $attspe,
                                                    'defspe' => $defspe,
                                                    'attaque1' => $attaque1,
                                                    'attaque2' => $attaque2,
                                                    'attaque3' => $attaque3,	
                                                    'parent_1' => $donnees3['id_pokemon'],	
                                                    'parent_2' => $donnees4['id_pokemon']	
                                                    ))or die(print_r($bdd->errorInfo()));
                                    $reponseX = $bdd->prepare('UPDATE pokemons_membres SET proba_naissance=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
                                    $reponseX->execute(array('pseudo' => $pseudo_oeuf));
                                    $req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("Roger Pex", :destinataire, "non lu", "Un oeuf est apparu!", :message, now())') or die(print_r($bdd->errorInfo()));
                                    $req->execute(array(
                                            'destinataire' => $pseudo_oeuf,					
                                            'message' => 'félicitation, vos pokémons ont donné naissance à un oeuf. Il a été ajouté à la liste de vos pokémons. Mais il faudra encore attendre qu\'il éclose avant de savoir quel pokémon s\'y trouve.'
                                            ))
                                            or die(print_r($bdd->errorInfo()));			
                                    }
                            else
                                    {
                                    $ajout=1;
                                    if($bonus_naissance=="ok"){$ajout=7;}
                                    $proba_naissance=$proba_naissance+$ajout;
                                    $reponseX = $bdd->prepare('UPDATE pokemons_membres SET proba_naissance=:proba_naissance WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
                                    $reponseX->execute(array('proba_naissance' => $proba_naissance, 'pseudo' => $pseudo_oeuf));
                                    }
                            }
                    }
            }
    //modification des missions	
    $reponse = $bdd->query('SELECT id,lvl FROM pokemons_missions') or die(print_r($bdd->errorInfo())); 
    while($donnees = $reponse->fetch())	
            {
            if($donnees['lvl']>5){$lvl_new=$donnees['lvl']-1;}else{$lvl_new=$donnees['lvl'];}
            $reponseX = $bdd->prepare('UPDATE pokemons_missions SET lvl=:lvl WHERE id=:id') or die(print_r($bdd->errorInfo()));
                                    $reponseX->execute(array('lvl' => $lvl_new, 'id'=>$donnees['id']));
            }
    }
?>

