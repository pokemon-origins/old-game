<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">


<?php
if(isset($_SESSION['pseudo']))
{
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
$pos_hor=$donnees['pos_hor'];
$pos_ver=$donnees['pos_ver'];
$quete_principale=$donnees['quete_principale'];
$quete_principale_etape=$donnees['quete_principale_etape'];
$quete_principale_active=$donnees['quete_principale_active'];
$argent_dispo=$donnees['pokedollar'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_principales WHERE numero=:numero AND etape=:etape') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('numero' => $quete_principale, 'etape' => $quete_principale_etape));  
$donnees = $reponse->fetch();
$id_quete_principale=$donnees['id'];
if($id_quete_principale>2 OR $id_quete_principale==2 AND $quete_principale_active==1)
{
    

$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
	$donnees = $reponse->fetch();
$score_total=$donnees['score_total'];
$points_quetes=$donnees['points_quetes'];
$ors=$donnees['ors'];
$mine=$donnees['mine'];
$mine_pepites=$mine;
$victoires_A=$donnees['victoires_A'];
$victoires_B=$donnees['victoires_B'];
$victoires_C=$donnees['victoires_C'];
$victoires_D=$donnees['victoires_D'];
$victoires_blind_battles=$victoires_A+$victoires_B+$victoires_C+$victoires_D;
$depenses_ors=$donnees['depenses_ors'];
$depenses_pokedollars=$donnees['depenses_pokedollars'];
$quete_principale=$donnees['quete_principale'];
$quete_principale_etape=$donnees['quete_principale_etape'];
$quete_principale_active=$donnees['quete_principale_active'];
$nb_quete_principale=$donnees['nb_quete_principale'];
$naissances=$donnees['naissances'];
$depenses_dracoport=$donnees['depenses_dracoport'];
$casino=$donnees['casino'];
$victoires_totales=0;
$reponse = $bdd->prepare('SELECT SUM(victoires) AS total FROM pokemons_liste_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
$victoires_totales=$donnees['total'];
?>


<h2>Vos récompenses</h2>

<p style="text-align:justify;"><b>Augmentez votre score, accomplissez de grandes choses, et venez réclamer vos récompenses, symbôles de votre triomphe. </b><br /><br />
Obtenir un trophée de rang 1 vous rapporte 5 pépites. <br />
Obtenir un trophée de rang 2 vous rapporte 20 pépites. <br />
Obtenir un trophée de rang 3 vous rapporte 40 pépites. <br />
Obtenir un trophée de rang 4 vous rapporte 75 pépites. <br />


<?php //up des trophées
if(isset($_POST['id']))
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_POST['id']));  
	$donnees = $reponse->fetch();
	if($donnees['pseudo']==$_SESSION['pseudo'] AND $_POST['grade']==$donnees['grade'])
		{
		$grade_now=$_POST['grade']+1;
		$reponse = $bdd->prepare('UPDATE pokemons_recompenses_liste SET grade=:grade WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('grade' => $grade_now, 'id' => $_POST['id'] )); 
		if($grade_now==1){$recompense=5;}if($grade_now==2){$recompense=20;}if($grade_now==3){$recompense=40;}if($grade_now==4){$recompense=75;}
		$ors_now=$ors+$recompense;
		$reponse = $bdd->prepare('UPDATE pokemons_membres SET ors=:ors WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('ors' => $ors_now, 'pseudo' => $_SESSION['pseudo'])); 
		echo '<p><b>Félicitation! Vous avez obtenu '.$recompense.' pépites et un nouveau trophée!</b></p>';
		}
	else
		{
		$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
		$donnees = $reponse->fetch();
		$triche=$donnees['triche'];
		$triche=$triche+1;
		$reponse = $bdd->prepare('UPDATE pokemons_membres SET triche=:triche WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('triche'=>$triche, 'pseudo' => $_SESSION['pseudo'])) or die(print_r($bdd->errorInfo()));
		if($triche>=3)
			{
			$req = $bdd->prepare('INSERT INTO comptes_bloques (pseudo) VALUES(:pseudo)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));
			$_SESSION=FALSE;
			session_destroy();
			}
		}
	}
?>



<table id="votre_pokemon" width="550px" cellpadding="2" cellspacing="2" style="text-align:center">
<colgroup><COL WIDTH=20%><COL WIDTH=30%><COL WIDTH=28%><COL WIDTH=22%></COLGROUP>
<tr><th>Titres</th><th>Objectifs</th><th>Trophées</th><th>Grade suivant</th></tr>

<?php //1. ROI DES POINTS 
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=1') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=1') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 1, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=1') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($score_total>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut un score de '.$condition.' pour accéder au rang suivant">'.$score_total.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //2. AMI DU PEUPLE
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=2') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=2') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 2, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=2') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($points_quetes>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut un score de quête de '.$condition.' pour accéder au rang suivant">'.$points_quetes.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //3. MAITRE CHASSEUR
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=3') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];
$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=3') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 3, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=3') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($nb_pokemons_joueur>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut posséder '.$condition.' pokémons pour accéder au rang suivant">'.$nb_pokemons_joueur.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //4. COLLECTIONNEUR G1
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=4') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];
$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=4') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 4, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=4') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($nb_pokemons_capture_V1>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir possédé '.$condition.' pokémons d\'espèces différentes pour accéder au rang suivant">'.$nb_pokemons_capture_V1.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //5. COLLECTIONNEUR EMERITE G1
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=5') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=5') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 5, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=5') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($nb_pokemons_shiney_capture_V1>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir possédé '.$condition.' pokémons shineys d\'espèces différentes pour accéder au rang suivant">'.$nb_pokemons_shiney_capture_V1.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //16. COLLECTIONNEUR G2
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=16') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];
$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=16') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 16, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=16') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($nb_pokemons_capture_V2>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir possédé '.$condition.' pokémons d\'espèces différentes pour accéder au rang suivant">'.$nb_pokemons_capture_V2.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //17. COLLECTIONNEUR EMERITE G2
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=17') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=17') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 17, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=17') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($nb_pokemons_shiney_capture_V2>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir possédé '.$condition.' pokémons shineys d\'espèces différentes pour accéder au rang suivant">'.$nb_pokemons_shiney_capture_V2.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //23. COLLECTIONNEUR G3
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=23') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];
$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=23') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 23, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=23') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($nb_pokemons_capture_V3>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir possédé '.$condition.' pokémons d\'espèces différentes pour accéder au rang suivant">'.$nb_pokemons_capture_V3.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //24. COLLECTIONNEUR EMERITE G3
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=24') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=24') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 24, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=24') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($nb_pokemons_shiney_capture_V3>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir possédé '.$condition.' pokémons shineys d\'espèces différentes pour accéder au rang suivant">'.$nb_pokemons_shiney_capture_V3.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //6. MINEUR
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=6') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=6') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 6, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=6') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($mine_pepites>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut acheter '.$condition.' pépites à la mine pour accéder au rang suivant">'.$mine_pepites.'/'.$condition.'</abbr>';}
?>
</td></tr>


<?php //7. SCIENTIFIQUE
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=7') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=7') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 7, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=7') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($victoires_blind_battles>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut gagner '.$condition.' matchs en blind battles pour accéder au rang suivant">'.$victoires_blind_battles.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //8. COMBATTANT ACHARNE
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=8') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=8') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 8, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=8') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($victoires_totales>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut accumuler '.$condition.' victoires pour accéder au rang suivant">'.$victoires_totales.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //9. GROS ACHETEUR
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=9') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=9') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 9, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=9') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($depenses_pokedollars>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir dépensé '.$condition.' pokédollars à la boutique pour accéder au rang suivant">'.$depenses_pokedollars.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //10. ACHETEUR D ELITE
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=10') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=10') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 10, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=10') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($depenses_ors>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir dépensé '.$condition.' pépites à la boutique pour accéder au rang suivant">'.$depenses_ors.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //11. Explorateur
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=11') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=11') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 11, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=11') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($nb_pages>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir visualisé '.$condition.' pages du site pour accéder au trophée suivant">'.$nb_pages.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //12. Accoucheur
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=12') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=12') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 12, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=12') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($naissances>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir fait éclore '.$condition.' oeufs pour accéder au trophée suivant">'.$naissances.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //13. Voyageur
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=13') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=13') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 13, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=13') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($depenses_dracoport>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir dépensé '.$condition.'$ au dracoport pour accéder au trophée suivant">'.$depenses_dracoport.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //14. Joueur
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=14') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=14') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 14, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=14') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($casino>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir joué '.$condition.' fois au casino pour accéder au trophée suivant">'.$casino.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //15. Brocanteur 
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=15') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=15') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 15, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=15') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($depenses_marche>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir dépensé '.$condition.'$ au marché pour accéder au trophée suivant">'.$depenses_marche.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //18. Missionnaire
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=18') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=18') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 18, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=18') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($missions_accomplies>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir réussi '.$condition.' missions pour accéder au trophée suivant">'.$missions_accomplies.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //19. Gradé
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=19') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=19') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 19, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=19') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($nb_grade>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir '.$condition.' titres pour accéder au trophée suivant">'.$nb_grade.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //20 Chasseur
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=20') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=20') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 20, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=20') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($concours>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir un score de '.$condition.' au concours de chasse pour accéder au trophée suivant">'.$concours.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //21 duelliste
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=21') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=21') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 21, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=21') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($score_pvp>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut avoir un score de PvP de '.$condition.' pour accéder au trophée suivant">'.$score_pvp.'/'.$condition.'</abbr>';}
?>
</td></tr>

<?php //22 Queteur G1
$reponse = $bdd->query('SELECT * FROM pokemons_recompenses WHERE id=22') or die(print_r($bdd->errorInfo())); 
$donnees = $reponse->fetch();
$nom=$donnees['nom'];$objectif=$donnees['objectif'];$condition1=$donnees['condition1'];$condition2=$donnees['condition2'];$condition3=$donnees['condition3'];$condition4=$donnees['condition4'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=22') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']))
	{$grade=$donnees['grade'];}
else
	{
	$req = $bdd->prepare('INSERT INTO pokemons_recompenses_liste (pseudo, id_recompense, grade) VALUES(:pseudo, 22, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo']))or die(print_r($bdd->errorInfo()));	
	$grade=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND id_recompense=22') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
?>
<tr><td><?php echo '<b>'.$nom.'</b> <br /> Grade '.$grade; ?></td><td><?php echo $objectif; ?></td><td><img src="images/recompenses/<?php echo $grade; ?>.png" height="120px"/></td>
<td>
<?php 
if($grade==0){$condition=$condition1;}if($grade==1){$condition=$condition2;}if($grade==2){$condition=$condition3;}if($grade==3){$condition=$condition4;}
if($grade==4){echo 'Maximum atteint';}
if($num_QP>=$condition AND $grade<4){echo '<form action="recompenses.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'"/><input type="hidden" name="grade" value="'.$grade.'"/><input type="submit" value="Trophée suivant"/></form>';}
elseif($grade<4){echo '<abbr title="Il vous faut être arrivé à la quête N°'.$condition.' pour accéder au trophée suivant">'.$num_QP.'/'.$condition.'</abbr>';}
?>
</td></tr>

</table>


<?php
}
else
{
echo 'Vous devez au moins finir la première quête (professeur Chen) pour avoir accès à cette partie.';
}
}
else
{
echo 'Vous devez être connecté pour accéder à cette page';
}
?>
<?php include ("bas.php"); ?>