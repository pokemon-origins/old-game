<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 
<div id="text_contenu">
<div id="text">

<?php
if(isset($_SESSION['pseudo']))
{

?>

<?php
$combat_possible=0;
if($_POST['action']=="combat" AND $_POST['type_quete']=="secondaire") //vérification quête secondaire nb_pnj
{
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
$nb_duel=$donnees['nb_duel'];
$bonus_duel=$donnees['bonus_duel'];
$time=time();
if($bonus_duel>$time){$pnj_max=15;}else{$pnj_max=5;$reponse = $bdd->prepare('UPDATE pokemons_membres SET bonus_duel=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));$reponse->execute(array('pseudo' => $_SESSION['pseudo']));}
if($nb_duel>=$pnj_max){$trop_de_pnj_vaincus=1;}
$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_secondaires WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_POST['numero_quete']));  
$donnees = $reponse->fetch();
$repetitive=$donnees['repetitive'];
$repetee=$donnees['repetee'];	
if($repetitive<=$repetee){$pnj_vaincu_trop_de_fois=1;}
if($trop_de_pnj_vaincus!=1 AND $pnj_vaincu_trop_de_fois!=1) {$combat_possible=1;}
}
else{$combat_possible=1;}


if($_POST['action']=="combat" AND $combat_possible==1) //début du combat
{
if($_SESSION['page_combat']!="NULL"){$combat_possible=0;}
$_SESSION['page_combat']="quete_combat";
$_SESSION['id_quete_secondaire']=$_POST['id_quete'];
$_SESSION['last_attaque']=0;
$_SESSION['last_attaque_adv']=0;
if($_POST['type_quete']=="secondaire")
		{
		$_SESSION['numero_quete_secondaire']=$_POST['numero_quete'];
		$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_secondaires WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $_POST['numero_quete']));  
			$donnees = $reponse->fetch();
		$repetitive=$donnees['repetitive'];
		$repetee=$donnees['repetee'];
		$difficulte_quete=$donnees['difficulte'];
		$repetee_now=$repetee+1;
		$reponse = $bdd->prepare('UPDATE pokemons_quetes_secondaires SET repetee=:repetee WHERE id=:id') or die(print_r($bdd->errorInfo())); 
		$reponse->execute(array('repetee' => $repetee_now, 'id' => $_POST['numero_quete'])); 	
		$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
		$donnees = $reponse->fetch();
		$nb_duel_now=$donnees['nb_duel']+1;
		$reponse = $bdd->prepare('UPDATE pokemons_membres SET nb_duel=:nb_duel WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
		$reponse->execute(array('nb_duel' => $nb_duel_now, 'pseudo' => $_SESSION['pseudo'])); 	
		}
$_SESSION['xp_recu']=0;
$_SESSION['recompense_percue']=0;
$_SESSION['changement']=1;
$_SESSION['id_combat']=time();
$_SESSION['grele']=0;
$_SESSION['zenith']=0;
$_SESSION['tempete_sable']=0;
$_SESSION['danse_pluie']=0;
$_SESSION['type_quete']=$_POST['type_quete'];
if($_SESSION['type_quete']=="secondaire")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_secondaires WHERE what=:what AND genre="duel"') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('what' => $_POST['id_quete']));  
	$donnees = $reponse->fetch();
	}
if($_SESSION['type_quete']=="principale")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_principales WHERE what=:what AND genre="duel"') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('what' => $_POST['id_quete']));  
	$donnees = $reponse->fetch();
	}
$_SESSION['id_liste_quete']=$donnees['id'];
$_SESSION['id_quete']=$_POST['id_quete'];
$_SESSION['adversaire']=$donnees['qui'];
$_SESSION['difficulte_quete']=$donnees['difficulte'];
$_SESSION['nb_pokemons_dispo']=$donnees['condition_reussite'];
$_SESSION['complet']=$donnees['complet'];
$_SESSION['reussite']=$donnees['reussite'];
$_SESSION['genre_recompense']=$donnees['genre_recompense'];
$_SESSION['recompense']=$donnees['recompense'];
$_SESSION['nb_recompense']=$donnees['nb_recompense'];		
$_SESSION['nb_pokemons_adv']=0;
$_SESSION['nb_pokemons_adv_vaincu']=0;
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id_quete' => $_POST['id_quete']));  
while($donnees = $reponse->fetch()){$_SESSION['nb_pokemons_adv']=$_SESSION['nb_pokemons_adv']+1;}
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete AND num_pokemon=1') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id_quete' => $_POST['id_quete']));  
$donnees = $reponse->fetch();
if(isset($donnees['id']) )
	{
	$_SESSION['id_adv'] = $donnees['id'];
	$_SESSION['id_pokemon_adv'] = $donnees['id_pokemon'];
	$_SESSION['shiney_adv']= $donnees['shiney'];
	$_SESSION['sexe_adv']= $donnees['sexe'];
	$_SESSION['lvl_adv'] = $donnees['lvl'];
	$_SESSION['pv_adv'] = $donnees['pv'];
	$_SESSION['pv_max_adv'] = $donnees['pv_max'];
	$_SESSION['att_adv']=$donnees['att'];
	$_SESSION['def_adv']=$donnees['def'];
	$_SESSION['vit_adv']=$donnees['vit'];
	$_SESSION['attspe_adv']=$donnees['attspe'];
	$_SESSION['defspe_adv']=$donnees['defspe'];
	$_SESSION['att_max_adv']=$donnees['att'];
	$_SESSION['def_max_adv']=$donnees['def'];
	$_SESSION['vit_max_adv']=$donnees['vit'];
	$_SESSION['attspe_max_adv']=$donnees['attspe'];
	$_SESSION['defspe_max_adv']=$donnees['defspe'];
	$_SESSION['attaque1_nb_adv']=$donnees['attaque1'];
	$_SESSION['attaque2_nb_adv']=$donnees['attaque2'];
	$_SESSION['attaque3_nb_adv']=$donnees['attaque3'];
	$_SESSION['attaque4_nb_adv']=$donnees['attaque4'];
	$_SESSION['bonheur_adv']=$donnees['bonheur'];
	$_SESSION['objet_adv']=$donnees['objet'];
	$_SESSION['esq_adv']=0;
	$_SESSION['pre_adv']=0;
	$_SESSION['statut_confus_adv']=-1;
	$_SESSION['statut_poison_adv']=0;
	$_SESSION['statut_poison_grave_adv']=0;
	$_SESSION['statut_gel_adv']=0;
	$_SESSION['statut_paralyse_adv']=0;
	$_SESSION['statut_brule_adv']=0;
	$_SESSION['statut_dodo_adv']=0;
	$_SESSION['recharge_adv']=0;
	$_SESSION['mur_lumiere_adv']=0;
	$_SESSION['protection_adv']=0;
	$_SESSION['rune_protect_adv']=0;
	$_SESSION['charge_adv']=0;
	$_SESSION['attraction_adv']=0;
	$_SESSION['statut_vampigraine_adv']=0;
	$_SESSION['danse_flamme_adv']=0;
	$_SESSION['ligotage_adv']=0;
	$_SESSION['esquive_adv']=0;
	$_SESSION['attaque_auto_adv']=0;
	$_SESSION['bonus_cc_adv']=0;
	$_SESSION['lance_boue_adv']=0;
	$_SESSION['tourniquet_adv']=0;
	$_SESSION['mania_adv']=0;
	$_SESSION['abri_adv']=100;
	$_SESSION['destin_adv']=0;
	}
$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_SESSION['attaque1_nb_adv']));  
$donnees = $reponse->fetch();
	$_SESSION['nom_attaque1_adv'] = $donnees['nom']; $_SESSION['type_attaque1_adv'] = $donnees['type']; $_SESSION['puissance_attaque1_adv'] = $donnees['puissance'];
	$_SESSION['prec_attaque1_adv'] = $donnees['prec']; $_SESSION['cc_attaque1_adv'] = $donnees['cc']; $_SESSION['classe_attaque1_adv'] = $donnees['classe'];
	$_SESSION['priorite_attaque1_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque1_adv'] = $donnees['esquive']; $_SESSION['cible_attaque1_adv'] = $donnees['cible'];
	$_SESSION['id_effet_attaque1_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque1_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque1_adv'] = $donnees['proba'];
$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_SESSION['attaque2_nb_adv']));  
$donnees = $reponse->fetch();
	$_SESSION['nom_attaque2_adv'] = $donnees['nom']; $_SESSION['type_attaque2_adv'] = $donnees['type']; $_SESSION['puissance_attaque2_adv'] = $donnees['puissance'];
	$_SESSION['prec_attaque2_adv'] = $donnees['prec']; $_SESSION['cc_attaque2_adv'] = $donnees['cc']; $_SESSION['classe_attaque2_adv'] = $donnees['classe'];
	$_SESSION['priorite_attaque2_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque2_adv'] = $donnees['esquive']; $_SESSION['cible_attaque2_adv'] = $donnees['cible'];
	$_SESSION['id_effet_attaque2_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque2_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque2_adv'] = $donnees['proba'];
$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_SESSION['attaque3_nb_adv']));  
$donnees = $reponse->fetch();
	$_SESSION['nom_attaque3_adv'] = $donnees['nom']; $_SESSION['type_attaque3_adv'] = $donnees['type']; $_SESSION['puissance_attaque3_adv'] = $donnees['puissance'];
	$_SESSION['prec_attaque3_adv'] = $donnees['prec']; $_SESSION['cc_attaque3_adv'] = $donnees['cc']; $_SESSION['classe_attaque3_adv'] = $donnees['classe'];
	$_SESSION['priorite_attaque3_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque3_adv'] = $donnees['esquive']; $_SESSION['cible_attaque3_adv'] = $donnees['cible'];
	$_SESSION['id_effet_attaque3_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque3_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque3_adv'] = $donnees['proba'];
$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_SESSION['attaque4_nb_adv']));  
$donnees = $reponse->fetch();
	$_SESSION['nom_attaque4_adv'] = $donnees['nom']; $_SESSION['type_attaque4_adv'] = $donnees['type']; $_SESSION['puissance_attaque4_adv'] = $donnees['puissance'];
	$_SESSION['prec_attaque4_adv'] = $donnees['prec']; $_SESSION['cc_attaque4_adv'] = $donnees['cc']; $_SESSION['classe_attaque4_adv'] = $donnees['classe'];
	$_SESSION['priorite_attaque4_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque4_adv'] = $donnees['esquive']; $_SESSION['cible_attaque4_adv'] = $donnees['cible'];
	$_SESSION['id_effet_attaque4_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque4_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque4_adv'] = $donnees['proba'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id' => $_SESSION['id_pokemon_adv']));  
$donnees = $reponse->fetch();
	$_SESSION['id_pokedex_adv'] = $donnees['id_pokedex'];
	$_SESSION['nom_pokemon_adv'] = $donnees['nom'];
	$_SESSION['type_adv'] = $donnees['type1'];
	$_SESSION['type2_adv'] = $donnees['type2'];
	$_SESSION['tdc_adv']= $donnees['tdc'];
	$_SESSION['poids_adv']= $donnees['poids'];
	$_SESSION['multiplicateur_xp']= $donnees['multiplicateur_xp'];
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch(); //pokemon actif
	$_SESSION['id_pokemon']=$donnees['id_pokemon'];
	$_SESSION['lvl']=$donnees['lvl'];
	$_SESSION['shiney']=$donnees['shiney'];
	$_SESSION['sexe']= $donnees['sexe'];
	$_SESSION['surnom']= $donnees['surnom'];
	$_SESSION['pv']=$donnees['pv'];
	$_SESSION['pv_max']=$donnees['pv_max'];
	$_SESSION['att']=$donnees['att'];
	$_SESSION['def']=$donnees['def'];
	$_SESSION['vit']=$donnees['vit'];
	$_SESSION['attspe']=$donnees['attspe'];
	$_SESSION['defspe']=$donnees['defspe'];
	$_SESSION['att_max']=$donnees['att'];
	$_SESSION['def_max']=$donnees['def'];
	$_SESSION['vit_max']=$donnees['vit'];
	$_SESSION['attspe_max']=$donnees['attspe'];
	$_SESSION['defspe_max']=$donnees['defspe'];
	$_SESSION['attaque1_nb']=$donnees['attaque1'];
	$_SESSION['attaque2_nb']=$donnees['attaque2'];
	$_SESSION['attaque3_nb']=$donnees['attaque3'];
	$_SESSION['attaque4_nb']=$donnees['attaque4'];
	$_SESSION['bonheur']=$donnees['bonheur'];
	$_SESSION['objet']=$donnees['objet'];
	$_SESSION['esq']=0;
	$_SESSION['pre']=0;
	$_SESSION['statut_confus']=-1;
	$_SESSION['statut_poison']=0;
	$_SESSION['statut_poison_grave']=0;
	$_SESSION['statut_gel']=0;
	$_SESSION['statut_paralyse']=0;
	$_SESSION['statut_brule']=0;
	$_SESSION['statut_dodo']=0;
	$_SESSION['recharge']=0;
	$_SESSION['mur_lumiere']=0;
	$_SESSION['protection']=0;
	$_SESSION['rune_protect']=0;
	$_SESSION['charge']=0;
	$_SESSION['attraction']=0;
	$_SESSION['statut_vampigraine']=0;
	$_SESSION['danse_flamme']=0;
	$_SESSION['ligotage']=0;
	$_SESSION['esquive']=0;
	$_SESSION['attaque_auto']=0;
	$_SESSION['bonus_cc']=0;
	$_SESSION['lance_boue']=0;
	$_SESSION['tourniquet']=0;
	$_SESSION['mania']=0;
	$_SESSION['abri']=100;
	$_SESSION['destin']=0;
	$_SESSION['victoires']=$donnees['victoires'];
	$_SESSION['defaites']=$donnees['defaites'];
	//diminution de pa 
	$pa_restant=$donnees['pa_restant']-1;
	if($pa_restant <0){$pa_restant=0;}
	$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET pa_restant=:pa_restant WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pa_restant' => $pa_restant, 'pseudo' => $_SESSION['pseudo']));
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['attaque1_nb']));  
	$donnees = $reponse->fetch();
		$_SESSION['nom_attaque1'] = $donnees['nom']; $_SESSION['type_attaque1'] = $donnees['type']; $_SESSION['puissance_attaque1'] = $donnees['puissance'];
		$_SESSION['prec_attaque1'] = $donnees['prec']; $_SESSION['cc_attaque1'] = $donnees['cc']; $_SESSION['classe_attaque1'] = $donnees['classe'];
		$_SESSION['priorite_attaque1'] = $donnees['priorite']; $_SESSION['esquive_attaque1'] = $donnees['esquive']; $_SESSION['cible_attaque1'] = $donnees['cible'];
		$_SESSION['id_effet_attaque1'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque1'] = $donnees['id_effet2']; $_SESSION['proba_attaque1'] = $donnees['proba'];
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['attaque2_nb']));  
	$donnees = $reponse->fetch();
		$_SESSION['nom_attaque2'] = $donnees['nom']; $_SESSION['type_attaque2'] = $donnees['type']; $_SESSION['puissance_attaque2'] = $donnees['puissance'];
		$_SESSION['prec_attaque2'] = $donnees['prec']; $_SESSION['cc_attaque2'] = $donnees['cc']; $_SESSION['classe_attaque2'] = $donnees['classe'];
		$_SESSION['priorite_attaque2'] = $donnees['priorite']; $_SESSION['esquive_attaque2'] = $donnees['esquive']; $_SESSION['cible_attaque2'] = $donnees['cible'];
		$_SESSION['id_effet_attaque2'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque2'] = $donnees['id_effet2']; $_SESSION['proba_attaque2'] = $donnees['proba'];
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['attaque3_nb']));  
	$donnees = $reponse->fetch();
		$_SESSION['nom_attaque3'] = $donnees['nom']; $_SESSION['type_attaque3'] = $donnees['type']; $_SESSION['puissance_attaque3'] = $donnees['puissance'];
		$_SESSION['prec_attaque3'] = $donnees['prec']; $_SESSION['cc_attaque3'] = $donnees['cc']; $_SESSION['classe_attaque3'] = $donnees['classe'];
		$_SESSION['priorite_attaque3'] = $donnees['priorite']; $_SESSION['esquive_attaque3'] = $donnees['esquive']; $_SESSION['cible_attaque3'] = $donnees['cible'];
		$_SESSION['id_effet_attaque3'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque3'] = $donnees['id_effet2']; $_SESSION['proba_attaque3'] = $donnees['proba'];
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['attaque4_nb']));  
	$donnees = $reponse->fetch();
		$_SESSION['nom_attaque4'] = $donnees['nom']; $_SESSION['type_attaque4'] = $donnees['type']; $_SESSION['puissance_attaque4'] = $donnees['puissance'];
		$_SESSION['prec_attaque4'] = $donnees['prec']; $_SESSION['cc_attaque4'] = $donnees['cc']; $_SESSION['classe_attaque4'] = $donnees['classe'];
		$_SESSION['priorite_attaque4'] = $donnees['priorite']; $_SESSION['esquive_attaque4'] = $donnees['esquive']; $_SESSION['cible_attaque4'] = $donnees['cible'];
		$_SESSION['id_effet_attaque4'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque4'] = $donnees['id_effet2']; $_SESSION['proba_attaque4'] = $donnees['proba'];
	$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['id_pokemon']));
	$donnees = $reponse->fetch();
		$_SESSION['nom_pokemon']=$donnees['nom'];	
		$_SESSION['id_pokedex'] = $donnees['id_pokedex'];
		$_SESSION['type'] = $donnees['type1'];
		$_SESSION['type2'] = $donnees['type2'];
		$_SESSION['poids']= $donnees['poids'];
}
?>

<?php //changement de pokémon adv
if($_POST['action']=="pokemon_suivant") 
	{
	$_SESSION['xp_recu']=0;
	$num_du_pokemon_suivant=$_SESSION['nb_pokemons_adv_vaincu']+1;
	$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete AND num_pokemon=:num_pokemon') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id_quete' => $_SESSION['id_quete'], 'num_pokemon' => $num_du_pokemon_suivant));  
	$donnees = $reponse->fetch();
	if(isset($donnees['id']) )
		{
		$_SESSION['id_adv'] = $donnees['id'];
		$_SESSION['id_pokemon_adv'] = $donnees['id_pokemon'];
		$_SESSION['shiney_adv']= $donnees['shiney'];
		$_SESSION['sexe_adv']= $donnees['sexe'];
		$_SESSION['lvl_adv'] = $donnees['lvl'];
		$_SESSION['pv_adv'] = $donnees['pv'];
		$_SESSION['pv_max_adv'] = $donnees['pv_max'];
		$_SESSION['att_adv']=$donnees['att'];
		$_SESSION['def_adv']=$donnees['def'];
		$_SESSION['vit_adv']=$donnees['vit'];
		$_SESSION['attspe_adv']=$donnees['attspe'];
		$_SESSION['defspe_adv']=$donnees['defspe'];
		$_SESSION['att_max_adv']=$donnees['att'];
		$_SESSION['def_max_adv']=$donnees['def'];
		$_SESSION['vit_max_adv']=$donnees['vit'];
		$_SESSION['attspe_max_adv']=$donnees['attspe'];
		$_SESSION['defspe_max_adv']=$donnees['defspe'];
		$_SESSION['attaque1_nb_adv']=$donnees['attaque1'];
		$_SESSION['attaque2_nb_adv']=$donnees['attaque2'];
		$_SESSION['attaque3_nb_adv']=$donnees['attaque3'];
		$_SESSION['attaque4_nb_adv']=$donnees['attaque4'];
		$_SESSION['bonheur_adv']=$donnees['bonheur'];
		$_SESSION['objet_adv']=$donnees['objet'];
		$_SESSION['esq_adv']=0;
		$_SESSION['pre_adv']=0;
		$_SESSION['statut_confus_adv']=-1;
		$_SESSION['statut_poison_adv']=0;
		$_SESSION['statut_poison_grave_adv']=0;
		$_SESSION['statut_gel_adv']=0;
		$_SESSION['statut_paralyse_adv']=0;
		$_SESSION['statut_brule_adv']=0;
		$_SESSION['statut_dodo_adv']=0;
		$_SESSION['recharge_adv']=0;
		//$_SESSION['mur_lumiere_adv']=0;
		//$_SESSION['protection_adv']=0;
		$_SESSION['charge_adv']=0;
		$_SESSION['attraction_adv']=0;$_SESSION['attraction']=0;
		$_SESSION['statut_vampigraine_adv']=0;
		$_SESSION['danse_flamme_adv']=0;
		$_SESSION['ligotage_adv']=0;$_SESSION['ligotage']=0;
		$_SESSION['esquive_adv']=0;
		$_SESSION['attaque_auto_adv']=0;
		$_SESSION['bonus_cc_adv']=0;
		$_SESSION['lance_boue_adv']=0;
		$_SESSION['tourniquet_adv']=0;
		$_SESSION['mania_adv']=0;
		$_SESSION['destin_adv']=0;
		}
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['attaque1_nb_adv']));  
	$donnees = $reponse->fetch();
		$_SESSION['nom_attaque1_adv'] = $donnees['nom']; $_SESSION['type_attaque1_adv'] = $donnees['type']; $_SESSION['puissance_attaque1_adv'] = $donnees['puissance'];
		$_SESSION['prec_attaque1_adv'] = $donnees['prec']; $_SESSION['cc_attaque1_adv'] = $donnees['cc']; $_SESSION['classe_attaque1_adv'] = $donnees['classe'];
		$_SESSION['priorite_attaque1_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque1_adv'] = $donnees['esquive']; $_SESSION['cible_attaque1_adv'] = $donnees['cible'];
		$_SESSION['id_effet_attaque1_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque1_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque1_adv'] = $donnees['proba'];
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['attaque2_nb_adv']));  
	$donnees = $reponse->fetch();
		$_SESSION['nom_attaque2_adv'] = $donnees['nom']; $_SESSION['type_attaque2_adv'] = $donnees['type']; $_SESSION['puissance_attaque2_adv'] = $donnees['puissance'];
		$_SESSION['prec_attaque2_adv'] = $donnees['prec']; $_SESSION['cc_attaque2_adv'] = $donnees['cc']; $_SESSION['classe_attaque2_adv'] = $donnees['classe'];
		$_SESSION['priorite_attaque2_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque2_adv'] = $donnees['esquive']; $_SESSION['cible_attaque2_adv'] = $donnees['cible'];
		$_SESSION['id_effet_attaque2_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque2_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque2_adv'] = $donnees['proba'];
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['attaque3_nb_adv']));  
	$donnees = $reponse->fetch();
		$_SESSION['nom_attaque3_adv'] = $donnees['nom']; $_SESSION['type_attaque3_adv'] = $donnees['type']; $_SESSION['puissance_attaque3_adv'] = $donnees['puissance'];
		$_SESSION['prec_attaque3_adv'] = $donnees['prec']; $_SESSION['cc_attaque3_adv'] = $donnees['cc']; $_SESSION['classe_attaque3_adv'] = $donnees['classe'];
		$_SESSION['priorite_attaque3_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque3_adv'] = $donnees['esquive']; $_SESSION['cible_attaque3_adv'] = $donnees['cible'];
		$_SESSION['id_effet_attaque3_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque3_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque3_adv'] = $donnees['proba'];
	$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['attaque4_nb_adv']));  
	$donnees = $reponse->fetch();
		$_SESSION['nom_attaque4_adv'] = $donnees['nom']; $_SESSION['type_attaque4_adv'] = $donnees['type']; $_SESSION['puissance_attaque4_adv'] = $donnees['puissance'];
		$_SESSION['prec_attaque4_adv'] = $donnees['prec']; $_SESSION['cc_attaque4_adv'] = $donnees['cc']; $_SESSION['classe_attaque4_adv'] = $donnees['classe'];
		$_SESSION['priorite_attaque4_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque4_adv'] = $donnees['esquive']; $_SESSION['cible_attaque4_adv'] = $donnees['cible'];
		$_SESSION['id_effet_attaque4_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque4_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque4_adv'] = $donnees['proba'];
	$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_SESSION['id_pokemon_adv']));  
	$donnees = $reponse->fetch();
		$_SESSION['id_pokedex_adv'] = $donnees['id_pokedex'];
		$_SESSION['nom_pokemon_adv'] = $donnees['nom'];
		$_SESSION['type_adv'] = $donnees['type1'];
		$_SESSION['type2_adv'] = $donnees['type2'];
		$_SESSION['tdc_adv']= $donnees['tdc'];
		$_SESSION['multiplicateur_xp']= $donnees['multiplicateur_xp'];
	}

?>

<?php //changement de pokémon joueur
if($_POST['action']=="changer_pokemon")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_SESSION['pseudo'], 'id'=>$_POST['pokemon']));
	$donnees = $reponse->fetch();
	if(isset($donnees['id']) AND $donnees['pa_restant']>0 OR isset($donnees['id']) AND $donnees['actif']==2)
		{
		if($donnees['actif']==2){$_POST['again']="ok";}else{$_POST['again']="";}
		$time_now=time();
		$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET pv=:pv, id_combat=:id_combat, poison=:poison, poison_grave=:poison_grave, gel=:gel, paralyse=:paralyse, brule=:brule, dodo=:dodo, fin_dodo=:fin_dodo, actif=2, participe=1  WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
				  $reponse->execute(array('pv' => $_SESSION['pv'],'id_combat' => $_SESSION['id_combat'], 'poison' => $_SESSION['statut_poison'], 'poison_grave' => $_SESSION['statut_poison_grave'], 'gel' => $_SESSION['statut_gel'], 'paralyse'=> $_SESSION['statut_paralyse'], 'brule' => $_SESSION['statut_brule'], 'dodo' => $_SESSION['statut_dodo'], 'fin_dodo' => $_SESSION['fin_dodo'] ,'pseudo' => $_SESSION['pseudo'])); 
		$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET  actif=1  WHERE pseudo=:pseudo AND id=:id') or die(print_r($bdd->errorInfo()));
				  $reponse->execute(array('pseudo' => $_SESSION['pseudo'], 'id' => $_POST['pokemon'])); 
		$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
		$donnees = $reponse->fetch(); //pokemon actif
		$_SESSION['id_pokemon']=$donnees['id_pokemon'];
		$_SESSION['lvl']=$donnees['lvl'];
		$_SESSION['shiney']=$donnees['shiney'];
		$_SESSION['surnom']= $donnees['surnom'];
		$_SESSION['sexe']= $donnees['sexe'];
		$_SESSION['pv']=$donnees['pv'];
		$_SESSION['pv_max']=$donnees['pv_max'];
		$_SESSION['att']=$donnees['att'];
		$_SESSION['def']=$donnees['def'];
		if($donnees['paralyse']==1){$_SESSION['vit']=$donnees['vit']/4;}else{$_SESSION['vit']=$donnees['vit'];}
		$_SESSION['attspe']=$donnees['attspe'];
		$_SESSION['defspe']=$donnees['defspe'];
		$_SESSION['att_max']=$donnees['att'];
		$_SESSION['def_max']=$donnees['def'];
		$_SESSION['vit_max']=$donnees['vit'];
		$_SESSION['attspe_max']=$donnees['attspe'];
		$_SESSION['defspe_max']=$donnees['defspe'];
		$_SESSION['attaque1_nb']=$donnees['attaque1'];
		$_SESSION['attaque2_nb']=$donnees['attaque2'];
		$_SESSION['attaque3_nb']=$donnees['attaque3'];
		$_SESSION['attaque4_nb']=$donnees['attaque4'];
		$_SESSION['bonheur']=$donnees['bonheur'];
		$_SESSION['objet']=$donnees['objet'];
		$_SESSION['esq']=0;
		$_SESSION['pre']=0;
		$_SESSION['statut_confus']=-1;
		$_SESSION['statut_poison']=$donnees['poison'];
		$_SESSION['statut_poison_grave']=$donnees['poison_grave'];
		$_SESSION['statut_gel']=$donnees['gel'];
		$_SESSION['statut_paralyse']=$donnees['paralyse'];
		$_SESSION['statut_brule']=$donnees['brule'];
		$_SESSION['statut_dodo']=$donnees['dodo'];
		$_SESSION['fin_dodo']=$donnees['fin_dodo'];
		$_SESSION['recharge']=0;
		//$_SESSION['mur_lumiere']=0;
		//$_SESSION['protection']=0;
		$_SESSION['charge']=0;
		$_SESSION['attraction']=0;$_SESSION['attraction_adv']=0;
		$_SESSION['statut_vampigraine']=0;
		$_SESSION['danse_flamme']=0;
		$_SESSION['ligotage']=0; $_SESSION['ligotage_adv']=0;
		$_SESSION['esquive']=0;
		$_SESSION['attaque_auto']=0;
		$_SESSION['bonus_cc']=0;
		$_SESSION['lance_boue']=0;
		$_SESSION['tourniquet']=0;
		$_SESSION['mania']=0;
		$_SESSION['destin']=0;
		$_SESSION['victoires']=$donnees['victoires'];
		$_SESSION['defaites']=$donnees['defaites'];
		//diminution de pa et de changement dispo
		if($_POST['again']!="ok")
			{
			$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET pa_restant=:pa_restant WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('pa_restant' => $donnees['pa_restant']-1, 'pseudo' => $_SESSION['pseudo']));
			$_SESSION['changement']=$_SESSION['changement']+1;
			}
		$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_SESSION['attaque1_nb']));  
		$donnees = $reponse->fetch();
			$_SESSION['nom_attaque1'] = $donnees['nom']; $_SESSION['type_attaque1'] = $donnees['type']; $_SESSION['puissance_attaque1'] = $donnees['puissance'];
			$_SESSION['prec_attaque1'] = $donnees['prec']; $_SESSION['cc_attaque1'] = $donnees['cc']; $_SESSION['classe_attaque1'] = $donnees['classe'];
			$_SESSION['priorite_attaque1'] = $donnees['priorite']; $_SESSION['esquive_attaque1'] = $donnees['esquive']; $_SESSION['cible_attaque1'] = $donnees['cible'];
			$_SESSION['id_effet_attaque1'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque1'] = $donnees['id_effet2']; $_SESSION['proba_attaque1'] = $donnees['proba'];
		$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_SESSION['attaque2_nb']));  
		$donnees = $reponse->fetch();
			$_SESSION['nom_attaque2'] = $donnees['nom']; $_SESSION['type_attaque2'] = $donnees['type']; $_SESSION['puissance_attaque2'] = $donnees['puissance'];
			$_SESSION['prec_attaque2'] = $donnees['prec']; $_SESSION['cc_attaque2'] = $donnees['cc']; $_SESSION['classe_attaque2'] = $donnees['classe'];
			$_SESSION['priorite_attaque2'] = $donnees['priorite']; $_SESSION['esquive_attaque2'] = $donnees['esquive']; $_SESSION['cible_attaque2'] = $donnees['cible'];
			$_SESSION['id_effet_attaque2'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque2'] = $donnees['id_effet2']; $_SESSION['proba_attaque2'] = $donnees['proba'];
		$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_SESSION['attaque3_nb']));  
		$donnees = $reponse->fetch();
			$_SESSION['nom_attaque3'] = $donnees['nom']; $_SESSION['type_attaque3'] = $donnees['type']; $_SESSION['puissance_attaque3'] = $donnees['puissance'];
			$_SESSION['prec_attaque3'] = $donnees['prec']; $_SESSION['cc_attaque3'] = $donnees['cc']; $_SESSION['classe_attaque3'] = $donnees['classe'];
			$_SESSION['priorite_attaque3'] = $donnees['priorite']; $_SESSION['esquive_attaque3'] = $donnees['esquive']; $_SESSION['cible_attaque3'] = $donnees['cible'];
			$_SESSION['id_effet_attaque3'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque3'] = $donnees['id_effet2']; $_SESSION['proba_attaque3'] = $donnees['proba'];
		$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_SESSION['attaque4_nb']));  
		$donnees = $reponse->fetch();
			$_SESSION['nom_attaque4'] = $donnees['nom']; $_SESSION['type_attaque4'] = $donnees['type']; $_SESSION['puissance_attaque4'] = $donnees['puissance'];
			$_SESSION['prec_attaque4'] = $donnees['prec']; $_SESSION['cc_attaque4'] = $donnees['cc']; $_SESSION['classe_attaque4'] = $donnees['classe'];
			$_SESSION['priorite_attaque4'] = $donnees['priorite']; $_SESSION['esquive_attaque4'] = $donnees['esquive']; $_SESSION['cible_attaque4'] = $donnees['cible'];
			$_SESSION['id_effet_attaque4'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque4'] = $donnees['id_effet2']; $_SESSION['proba_attaque4'] = $donnees['proba'];
		$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_SESSION['id_pokemon']));
		$donnees = $reponse->fetch();
			$_SESSION['nom_pokemon']=$donnees['nom'];	
			$_SESSION['id_pokedex'] = $donnees['id_pokedex'];
			$_SESSION['type'] = $donnees['type1'];
			$_SESSION['type2'] = $donnees['type2'];	
		}
	}
//fin changement de pokémon
?>


<?php //déroulement du combat
if($_SESSION['pv']==0){$defaite=1;}if($_SESSION['pv_adv']==0){$victoire=1;}
if($_POST['action']=="attaque" AND $_POST['attaque']!=0 AND $_SESSION['pv']!=0 AND $_SESSION['pv_adv']!=0 AND $combat_possible==1)
{
if($_SESSION['attaque_auto']==1){$_SESSION['attaque_auto']=0;}
//carac de l'attaque alliée
		if($_POST['attaque']!=$_SESSION['attaque1_nb'] AND $_POST['attaque']!=$_SESSION['attaque2_nb'] AND $_POST['attaque']!=$_SESSION['attaque3_nb'] AND $_POST['attaque']!=$_SESSION['attaque4_nb']){$_POST['attaque']=250;}
		$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' =>$_POST['attaque'])); 
		$donnees = $reponse->fetch();
		$_SESSION['id_attaque']=$donnees['id'];$_SESSION['nom_attaque'] = $donnees['nom']; $_SESSION['type_attaque'] = $donnees['type']; $_SESSION['puissance_attaque'] = $donnees['puissance'];
		$_SESSION['prec_attaque'] = $donnees['prec']; $_SESSION['cc_attaque'] = $donnees['cc']; $_SESSION['classe_attaque'] = $donnees['classe'];
		$_SESSION['priorite_attaque'] = $donnees['priorite']; $_SESSION['esquive_attaque'] = $donnees['esquive']; $_SESSION['cible_attaque'] = $donnees['cible'];
		$_SESSION['id_effet_attaque'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque'] = $donnees['id_effet2']; $_SESSION['proba_attaque'] = $donnees['proba'];
		if($_SESSION['nom_attaque']=="abri"){$_SESSION['prec_attaque']=$_SESSION['abri'];$_SESSION['abri']=$_SESSION['abri']/2;}
			//balayage
		if($_SESSION['id_effet_attaque']==98 OR $_SESSION['id_effet2_attaque']==98)
			{
			if($_SESSION['poids_adv']>200){$_SESSION['puissance_attaque']=120;}
			elseif($_SESSION['poids_adv']>100){$_SESSION['puissance_attaque']=100;}
			elseif($_SESSION['poids_adv']>50){$_SESSION['puissance_attaque']=80;}
			elseif($_SESSION['poids_adv']>25){$_SESSION['puissance_attaque']=60;}
			elseif($_SESSION['poids_adv']>10){$_SESSION['puissance_attaque']=40;}
			else{$_SESSION['puissance_attaque']=20;}
			}
		//cadeau
		if($_SESSION['id_effet_attaque']==99 OR $_SESSION['id_effet2_attaque']==99)
			{
			$_SESSION['id_attaque']=$donnees['id'];$_SESSION['nom_attaque'] = $donnees['nom']; $_SESSION['type_attaque'] = $donnees['type']; 
			$_SESSION['prec_attaque'] = $donnees['prec'];  $_SESSION['classe_attaque'] = $donnees['classe'];
			$_SESSION['priorite_attaque'] = $donnees['priorite']; $_SESSION['esquive_attaque'] = $donnees['esquive']; $_SESSION['cible_attaque'] = $donnees['cible'];
			$_SESSION['id_effet_attaque'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque'] = $donnees['id_effet2']; $_SESSION['proba_attaque'] = $donnees['proba'];
			$rand_cadeau=rand(1,10);
			if($rand_cadeau<=4){$_SESSION['puissance_attaque'] = 40;$_SESSION['cc_attaque']=1;}
			elseif($rand_cadeau<=7){$_SESSION['puissance_attaque'] = 80;$_SESSION['cc_attaque']=1;}
			elseif($rand_cadeau<=8){$_SESSION['puissance_attaque'] = 120;$_SESSION['cc_attaque']=1;}
			else
				{
				$_SESSION['puissance_attaque'] = 0;$_SESSION['cc_attaque']=1;
				$_SESSION['pv_adv']=$_SESSION['pv_adv']+80;if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}
				}
			}
			
//choix de l'attaque adv
		if($_SESSION['attaque_auto_adv']==1)
			{$attaque_lancee_adv=$_SESSION['id_attaque_adv'];$_SESSION['attaque_auto_adv']=0;}
		else
			{
			while($attaque_lancee_adv==0)
				{
				$n_att_adv=rand(1,4);
				if($n_att_adv==1){$attaque_lancee_adv=$_SESSION['attaque1_nb_adv'];} 
				if($n_att_adv==2){$attaque_lancee_adv=$_SESSION['attaque2_nb_adv'];} 		
				if($n_att_adv==3){$attaque_lancee_adv=$_SESSION['attaque3_nb_adv'];} 
				if($n_att_adv==4){$attaque_lancee_adv=$_SESSION['attaque4_nb_adv'];} 
				}
			}
		//carac de l'attaque
		$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' =>$attaque_lancee_adv)); 
		$donnees = $reponse->fetch();
		$_SESSION['id_attaque_adv']=$donnees['id'];$_SESSION['nom_attaque_adv'] = $donnees['nom']; $_SESSION['type_attaque_adv'] = $donnees['type']; $_SESSION['puissance_attaque_adv'] = $donnees['puissance'];
		$_SESSION['prec_attaque_adv'] = $donnees['prec']; $_SESSION['cc_attaque_adv'] = $donnees['cc']; $_SESSION['classe_attaque_adv'] = $donnees['classe'];
		$_SESSION['priorite_attaque_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque_adv'] = $donnees['esquive']; $_SESSION['cible_attaque_adv'] = $donnees['cible'];
		$_SESSION['id_effet_attaque_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque_adv'] = $donnees['proba'];
		if($_SESSION['nom_attaque_adv']=="abri"){$_SESSION['prec_attaque_adv']=$_SESSION['abri_adv'];$_SESSION['abri_adv']=$_SESSION['abri_adv']/2;}
			//balayage
		if($_SESSION['id_effet_attaque_adv']==98 OR $_SESSION['id_effet2_attaque_adv']==98)
			{
			if($_SESSION['poids']>200){$_SESSION['puissance_attaque_adv']=120;}
			elseif($_SESSION['poids']>100){$_SESSION['puissance_attaque_adv']=100;}
			elseif($_SESSION['poids']>50){$_SESSION['puissance_attaque_adv']=80;}
			elseif($_SESSION['poids']>25){$_SESSION['puissance_attaque_adv']=60;}
			elseif($_SESSION['poids']>10){$_SESSION['puissance_attaque_adv']=40;}
			else{$_SESSION['puissance_attaque_adv']=20;}
			}
		//cadeau
		if($_SESSION['id_effet_attaque_adv']==99 OR $_SESSION['id_effet2_attaque_adv']==99)
			{
			$_SESSION['id_attaque_adv']=$donnees['id'];$_SESSION['nom_attaque_adv'] = $donnees['nom']; $_SESSION['type_attaque_adv'] = $donnees['type']; 
			$_SESSION['prec_attaque_adv'] = $donnees['prec'];  $_SESSION['classe_attaque_adv'] = $donnees['classe'];
			$_SESSION['priorite_attaque_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque_adv'] = $donnees['esquive']; $_SESSION['cible_attaque_adv'] = $donnees['cible'];
			$_SESSION['id_effet_attaque_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque_adv'] = $donnees['proba'];
			$rand_cadeau=rand(1,10);
			if($rand_cadeau<=4){$_SESSION['puissance_attaque_adv'] = 40;$_SESSION['cc_attaque_adv']=1;}
			elseif($rand_cadeau<=7){$_SESSION['puissance_attaque_adv'] = 80;$_SESSION['cc_attaque_adv']=1;}
			elseif($rand_cadeau<=8){$_SESSION['puissance_attaque_adv'] = 120;$_SESSION['cc_attaque_adv']=1;}
			else
				{
				$_SESSION['puissance_attaque_adv'] = 0;$_SESSION['cc_attaque_adv']=1;
				$_SESSION['pv']=$_SESSION['pv']+80;if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}
				}
			}
			
//dégats avant attaques
	//effet 43 : grêle
$toujours_la=1;
if($_SESSION['grele']>0)
	{
	if($_SESSION['type'] !="glace" AND $_SESSION['type2']!="glace"){$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
	if($_SESSION['type_adv'] !="glace" AND $_SESSION['type2_adv']!="glace"){$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
	if($_SESSION['pv_adv']==0){$victoire=1; $toujours_la=0;}
	elseif($_SESSION['pv']==0){$defaite=1; $toujours_la=0;}	
	}
if($_SESSION['tempete_sable']>0)
	{
	if($_SESSION['type'] !="sol" AND $_SESSION['type2']!="sol" AND $_SESSION['type'] !="roche" AND $_SESSION['type2']!="roche" AND $_SESSION['type'] !="acier" AND $_SESSION['type2']!="acier"){$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
	if($_SESSION['type_adv'] !="sol" AND $_SESSION['type2_adv']!="sol" AND $_SESSION['type_adv'] !="roche" AND $_SESSION['type2_adv']!="roche" AND $_SESSION['type_adv'] !="acier" AND $_SESSION['type2_adv']!="acier"){$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
	if($_SESSION['pv_adv']==0){$victoire=1; $toujours_la=0;}
	elseif($_SESSION['pv']==0){$defaite=1; $toujours_la=0;}	
	}
if($_SESSION['priorite_attaque_adv']==$_SESSION['priorite_attaque']){if($_SESSION['vit'] >= $_SESSION['vit_adv']) {$premier_coup=1;} else {$premier_coup=-1;}}
elseif($_SESSION['priorite_attaque_adv']>$_SESSION['priorite_attaque']){$premier_coup=-1;}
elseif($_SESSION['priorite_attaque_adv']<$_SESSION['priorite_attaque']){$premier_coup=1;}
elseif($_SESSION['vit'] >= $_SESSION['vit_adv']) {$premier_coup=1;} else {$premier_coup=-1;}
//if($_SESSION['priorite_attaque_adv']==1 AND $_SESSION['priorite_attaque']==1){if($_SESSION['vit'] >= $_SESSION['vit_adv']) {$premier_coup=1;} else {$premier_coup=-1;}}
//elseif($_SESSION['priorite_attaque_adv']==1){$premier_coup=-1;}
//elseif($_SESSION['priorite_attaque']==1){$premier_coup=1;}
//elseif($_SESSION['vit'] >= $_SESSION['vit_adv']) {$premier_coup=1;} else {$premier_coup=-1;}
$attaque_donne=0; $ordre_attaque_adv=0; $ordre_attaque=0;
while($premier_coup<2 AND $toujours_la==1)
	{
	if($premier_coup==-1)//attaque du pokémon sauvage
		{
		$_SESSION['destin_adv']=0;
		if($attaque_donne==0){$ordre_attaque_adv=1;}if($attaque_donne==1){$ordre_attaque_adv=2;}
		$attaque_possible_adv = 1;
		//78 métronome
		if($_SESSION['id_effet_attaque_adv']==78 OR $_SESSION['id_effet2_attaque_adv']==78) 
			{
			while($proba_metronome==0 OR $proba_metronome==17 OR $proba_metronome==40 OR $proba_metronome==45 OR $proba_metronome==84 OR $proba_metronome==140){$proba_metronome=rand(16,251);}
			$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' =>$proba_metronome));
			$donnees = $reponse->fetch();
			$attaque_lancee_adv=$proba_metronome;
			$_SESSION['nom_attaque_adv'] = $donnees['nom']; $_SESSION['type_attaque_adv'] = $donnees['type']; $_SESSION['puissance_attaque_adv'] = $donnees['puissance'];
			$_SESSION['prec_attaque_adv'] = $donnees['prec']; $_SESSION['cc_attaque_adv'] = $donnees['cc']; $_SESSION['classe_attaque_adv'] = $donnees['classe'];
			$_SESSION['priorite_attaque_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque_adv'] = $donnees['esquive']; $_SESSION['cible_attaque_adv'] = $donnees['cible'];
			$_SESSION['id_effet_attaque_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque_adv'] = $donnees['proba'];
			}
		//110 blabla dodo
		if($_SESSION['id_effet_attaque_adv']==110 OR $_SESSION['id_effet2_attaque_adv']==110) 
			{
                        $blabla_dodo_ok=0;$test_blabla_dodo=0;
                        while($blabla_dodo_ok==0 AND $test_blabla_dodo<10){
                            $rand_blabladodo=rand(1,4);
                            if($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque1_nb_adv'];}
                            elseif($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque2_nb_adv'];}
                            elseif($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque3_nb_adv'];}
                            elseif($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque4_nb_adv'];}
                            $test_blabla_dodo++;
                            if($proba_blabla_dodo!=45 AND $proba_blabla_dodo!=342 AND $proba_blabla_dodo!=17 AND $proba_blabla_dodo!=21 AND $proba_blabla_dodo!=140 AND $proba_blabla_dodo!=84 AND $proba_blabla_dodo!=312 AND $proba_blabla_dodo!=40 AND $proba_blabla_dodo!=325){$blabla_dodo_ok=1;}
                        }
                        $echec_blabla_dodo=0;
                        if($blabla_dodo_ok==0){$echec_blabla_dodo=1;}
			$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' =>$proba_blabla_dodo));
			$donnees = $reponse->fetch();
			$attaque_lancee_adv=$proba_metronome;
			$_SESSION['nom_attaque_adv'] = $donnees['nom']; $_SESSION['type_attaque_adv'] = $donnees['type']; $_SESSION['puissance_attaque_adv'] = $donnees['puissance'];
			$_SESSION['prec_attaque_adv'] = $donnees['prec']; $_SESSION['cc_attaque_adv'] = $donnees['cc']; $_SESSION['classe_attaque_adv'] = $donnees['classe'];
			$_SESSION['priorite_attaque_adv'] = $donnees['priorite']; $_SESSION['esquive_attaque_adv'] = $donnees['esquive']; $_SESSION['cible_attaque_adv'] = $donnees['cible'];
			$_SESSION['id_effet_attaque_adv'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque_adv'] = $donnees['id_effet2']; $_SESSION['proba_attaque_adv'] = $donnees['proba'];
			}     		
		$proba_effet_adv=rand(1,100);
		if($_SESSION['proba_attaque_adv']>=$proba_effet_adv){$effet_adv=1;} else {$effet_adv=0;}
		
		//EFFET des CLIMATS sur les attaques
		$soins_adv=0.5;
		if($_SESSION['danse_pluie']>0) 
			{
			if($_SESSION['type_attaque_adv']=="feu"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*0.5;} 
			if($_SESSION['type_attaque_adv']=="eau"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*1.5;} 
			if($_SESSION['id_attaque_adv']==40){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']/2;}//lance soleil
			if($_SESSION['id_attaque_adv']==43){$_SESSION['prec_attaque_adv']=100;}//fatal-foudre
			if($_SESSION['id_attaque_adv']==68 OR $_SESSION['id_attaque_adv']==141){$soins_adv=0.25;}//synthèse et rayon lune
			//vent violent et aurore pas encore implémenté
			}
		if($_SESSION['zenith']>0) 
			{
			if($_SESSION['type_attaque_adv']=="feu"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*1.5;} 
			if($_SESSION['type_attaque_adv']=="eau"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*0.5;} 
			//lance soleil en un tour dans "charge"
			if($_SESSION['id_attaque_adv']==68 OR $_SESSION['id_attaque_adv']==141){$soins_adv=0.66;}//synthèse et rayon lune
			}	
		if($_SESSION['tempete_sable']>0) 
			{
			if($_SESSION['id_attaque_adv']==40){$_SESSION['puissance_attaque_adv']=60;}//lance soleil
			if($_SESSION['id_attaque_adv']==68 OR $_SESSION['id_attaque_adv']==141){$soins_adv=0.25;}//synthèse et rayon lune
			//aurore pas encore implémenté
			}	
		if($_SESSION['grele']>0) 
			{
			if($_SESSION['id_attaque_adv']==33){$_SESSION['prec_attaque_adv']=100;}//blizarre
			if($_SESSION['id_attaque_adv']==40){$_SESSION['puissance_attaque_adv']=60;}//lance soleil
			if($_SESSION['id_attaque_adv']==68 OR $_SESSION['id_attaque_adv']==141){$soins_adv=0.25;}//synthèse et rayon lune
			//aurore pas encore implémenté
			}	
			
			//ECHEC AUTOMATIQUE
		//cage éclair sur roche
		if($attaque_lancee_adv==121 AND $_SESSION['type']=="roche" OR $attaque_lancee_adv==121 AND $_SESSION['type2']=="roche" OR $attaque_lancee_adv==121 AND $_SESSION['type']=="sol" OR $attaque_lancee_adv==121 AND $_SESSION['type2']=="sol"){$effet_adv=0;}
		//guillotine et empla'korn sur spectre
		if($attaque_lancee_adv==216 AND $_SESSION['type']=="spectre" OR $attaque_lancee_adv==216 AND $_SESSION['type2']=="spectre" OR $attaque_lancee_adv==133 AND $_SESSION['type']=="spectre" OR $attaque_lancee_adv==133 AND $_SESSION['type2']=="spectre"){$effet_adv=0;}
		//abime sur vol
		if($attaque_lancee_adv==157 AND $_SESSION['type']=="vol" OR $attaque_lancee_adv==157 AND $_SESSION['type2']=="vol"){$effet_adv=0;}
		
		
		//calcul du multiplicateur
			$reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('type' => $_SESSION['type_attaque_adv'], 'sur' => $_SESSION['type']));  
			$donnees = $reponse->fetch();
			$multiplicateur1=$donnees['effet'];
			if($_SESSION['type2']!="0")
			{$reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('type' => $_SESSION['type_attaque_adv'], 'sur' => $_SESSION['type2']));  
			$donnees = $reponse->fetch();
			$multiplicateur2=$donnees['effet'];}
			else {$multiplicateur2=1;}
			$multiplicateur_adv=$multiplicateur1*$multiplicateur2;
		
		if($multiplicateur_adv==0 AND $_SESSION['cible_attaque_adv']==1 AND $_SESSION['puissance_attaque_adv']>0){$effet_adv=0;}
		//verification que touche
		$hasard_precision_adv=rand(1,100);
		$hasard_precision_adv=$hasard_precision_adv + $_SESSION['esq']-$_SESSION['pre_adv'];
                if($echec_blabla_dodo==1){$hasard_precision_adv=1000;}
			//diminution tour
				//confusion
				if($_SESSION['statut_confus_adv']>0){$_SESSION['statut_confus_adv']=$_SESSION['statut_confus_adv']-1;}
				if($_SESSION['statut_confus_adv']==0){$statut_confus_end_adv=1;$_SESSION['statut_confus_adv']=$_SESSION['statut_confus_adv']-1;}
				//rune protect
				if($_SESSION['rune_protect_adv']>0){$_SESSION['rune_protect_adv']=$_SESSION['rune_protect_adv']-1;}
				//danse flamme
				if($_SESSION['danse_flamme_adv']>0){$_SESSION['danse_flamme_adv']=$_SESSION['danse_flamme_adv']-1;}
				//ligotage
				if($_SESSION['ligotage_adv']>0){$_SESSION['ligotage_adv']=$_SESSION['ligotage_adv']-1;}
				//gel
				if($_SESSION['statut_gel_adv']==1){$proba_fin_gel=rand(1,10); if($proba_fin_gel==1){$_SESSION['statut_gel_adv']=0; $degel_adv=1;}}
				//dodo
				if($_SESSION['statut_dodo_adv']==1){$_SESSION['fin_dodo_adv']=$_SESSION['fin_dodo_adv']-1;	if($_SESSION['fin_dodo_adv']==0){$_SESSION['statut_dodo_adv']=0; $reveil_adv=1;}}
				//mur lumière
				if($_SESSION['mur_lumiere_adv']>0){$_SESSION['mur_lumiere_adv']=$_SESSION['mur_lumiere_adv']-1;}
				//protection
				if($_SESSION['protection_adv']>0){$_SESSION['protection_adv']=$_SESSION['protection_adv']-1;}
		$proba_paralyse_adv=rand(1,100);
		$proba_attraction=rand(1,2); 
		$proba_confus_adv=rand(1,100);
		if($_SESSION['recharge_adv']==1)	
			//souffre de recharge (48)
			{
			$recharge_adv=1;
			$_SESSION['recharge_adv']=0; $recharge_faite_adv=1;$hasard_precision_adv=1000;
			$_SESSION['charge_adv']=0;
			}
		elseif($_SESSION['statut_dodo_adv']==1)
			//souffre du sommeil(66/69)
			{
			$statut_dodo_adv=1;$hasard_precision_adv=1000;
			$_SESSION['charge_adv']=0;
			}
		elseif($_SESSION['statut_gel_adv']==1)	
			//souffre du gel (47)
			{
			if($_SESSION['id_attaque_adv']!=331)
                            {
                            $statut_gel_adv=1;
                            $hasard_precision_adv=1000;
                            $_SESSION['charge_adv']=0;
                            }else
                            {
                            $_SESSION['statut_gel_adv']=0;
                            $degel_adv=1;
                            }
			}
		elseif($_SESSION['statut_paralyse_adv']==1 AND $proba_paralyse_adv>75)
			//souffre de paralysie(55)
			{
			$statut_paralyse_adv=1;
			$hasard_precision_adv=1000;
			$_SESSION['charge_adv']=0;
			}
		elseif($peur_adv==1)
			//souffre de peur (71)
			{
			if($peur_adv==1){$hasard_precision_adv=1000;$_SESSION['charge_adv']=0;}
			}
		elseif($_SESSION['attraction']==1 AND $proba_attraction==1)
			//souffre de attraction (67)
			{
			$_SESSION['charge_adv']=0;
			$hasard_precision_adv=1000;
			$statut_attraction=1;
			}
		elseif($_SESSION['statut_confus_adv']>0 AND $proba_confus_adv>50)
			{
			//souffre de confusion (39)
			$hasard_precision_adv=1000;
			$_SESSION['charge_adv']=0;
			$statut_confus_adv=1;
			$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/8); if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
			}
		else
			//54 charge
			{
			if($_SESSION['id_effet_attaque_adv']==54 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==54 AND $effet_adv==1){if($_SESSION['charge_adv']==0 AND $_SESSION['zenith']==0 OR $_SESSION['zenith']>0 AND $_SESSION['charge_adv']==0 AND $_SESSION['id_attaque_adv']!=40){$_SESSION['charge_adv']=1;$_SESSION['cible_attaque_adv']=0;$charge_faite_adv=1;$hasard_precision_adv=1000;$_SESSION['attaque_auto_adv']=1;} else {$_SESSION['charge_adv']=0;}}
			}
			//87 si dodo
		if($_SESSION['id_effet_attaque_adv']==87 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==87 AND $effet_adv==1) {if($_SESSION['statut_dodo']!=1){$hasard_precision_adv=200;$_SESSION['charge_adv']=0;}}
			
			// vol et tunnel au tour 1 non offenssif
		if($_SESSION['esquive_attaque_adv']==1 AND $_SESSION['charge_adv']==0){$_SESSION['cible_attaque_adv']=0;}	
				
			//esquive (abri)
		if($_SESSION['esquive']==1){if($_SESSION['cible_attaque_adv']==1 AND $_SESSION['id_attaque_adv']!=44 OR $_SESSION['cible_attaque_adv']==1 AND $_SESSION['id_attaque']!=45){$hasard_precision_adv=1500;$_SESSION['charge_adv']=0;}$_SESSION['esquive']=0;}	
		if($hasard_precision_adv<=$_SESSION['prec_attaque_adv'])
			{
			//effets avant attaque
			$_SESSION['last_attaque_adv']=$attaque_lancee_adv;
				//esquive (vol, tunnel)
			if($_SESSION['esquive_attaque_adv']==1){if($_SESSION['charge_adv']==0){$_SESSION['charge_adv']=1;$charge_faite_adv=1;$hasard_precision_adv=1000;$_SESSION['esquive_adv']=1;$_SESSION['attaque_auto_adv']=1;$_SESSION['puissance_attaque_adv']=0;} else {$_SESSION['charge_adv']=0;}}
				//38 echoue
			if($_SESSION['id_effet_attaque_adv']==38 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==38 AND $effet_adv==1) {if($degats > 0) {$_SESSION['puissance_attaque_adv']=0;$echec_attaque_adv=1;}}
				//40 fuite
			if($_SESSION['id_effet_attaque_adv']==40 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==40 AND $effet_adv==1) {$fuite_adv=1;}
				//46 zénith
			if($_SESSION['id_effet_attaque_adv']==46 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==46 AND $effet_adv==1) {$_SESSION['zenith']=5;$_SESSION['grele']=0;$_SESSION['tempete_sable']=0;$_SESSION['danse_pluie']=0;}
			if($_SESSION['zenith']>0) {if($_SESSION['type_attaque_adv']=="feu"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*1.5;} if($_SESSION['type_attaque_adv']=="eau"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*0.5;} }			
				//43 grêle
			if($_SESSION['id_effet_attaque_adv']==43 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==43 AND $effet_adv==1) {$_SESSION['grele']=5;$_SESSION['zenith']=0;$_SESSION['tempete_sable']=0;$_SESSION['danse_pluie']=0;}
				//51 danse pluie
			if($_SESSION['id_effet_attaque_adv']==51 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==51 AND $effet_adv==1) {$_SESSION['danse_pluie']=5;$_SESSION['grele']=0;$_SESSION['zenith']=0;$_SESSION['tempete_sable']=0;}		
			if($_SESSION['danse_pluie']>0) {if($_SESSION['type_attaque_adv']=="feu"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*0.5;} if($_SESSION['type_attaque_adv']=="eau"){ $_SESSION['puissance_attaque_adv'] = $_SESSION['puissance_attaque_adv']*1.5;} }
				//103 tempete de sable
			if($_SESSION['id_effet_attaque_adv']==103 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==103 AND $effet_adv==1) {$_SESSION['tempete_sable']=5;$_SESSION['grele']=0;$_SESSION['zenith']=0;$_SESSION['danse_pluie']=0;}						
				//104 ball'meteo
			if($_SESSION['id_effet_attaque_adv']==104 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==104 AND $effet_adv==1) 
				{
				if($_SESSION['danse_pluie']>0){$_SESSION['puissance_attaque_adv']=100;$_SESSION['type_attaque_adv']='eau';}
				elseif($_SESSION['zenith']>0){$_SESSION['puissance_attaque_adv']=100;$_SESSION['type_attaque_adv']='feu';}
				elseif($_SESSION['grele']>0){$_SESSION['puissance_attaque_adv']=100;$_SESSION['type_attaque_adv']='glace';}
				elseif($_SESSION['tempete_sable']>0){$_SESSION['puissance_attaque_adv']=100;$_SESSION['type_attaque_adv']='roche';}
				//calcul du multiplicateur
				$reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('type' => $_SESSION['type_attaque_adv'], 'sur' => $_SESSION['type']));  
				$donnees = $reponse->fetch();
				$multiplicateur1=$donnees['effet'];
				if($_SESSION['type2']!="0")
				{$reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('type' => $_SESSION['type_attaque_adv'], 'sur' => $_SESSION['type2']));  
				$donnees = $reponse->fetch();
				$multiplicateur2=$donnees['effet'];}
				else {$multiplicateur2=1;}
				$multiplicateur_adv=$multiplicateur1*$multiplicateur2;
				}
                                //115 force cachée
			if($_SESSION['id_effet_attaque_adv']==115 AND $effet_adv==1 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==115 AND $effet_adv==1 AND $_SESSION['statut_brule']==0 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}
			if($_SESSION['id_effet_attaque_adv']==115 AND $effet_adv==1 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==115 AND $effet_adv==1 AND $_SESSION['rune_protect']==0) 
                            {
                            if($_SESSION['danse_pluie']>0){$_SESSION['id_effet_attaque_adv']=2;}
                            elseif($_SESSION['zenith']>0){$_SESSION['id_effet_attaque_adv']=71;}
                            elseif($_SESSION['grele']>0){$_SESSION['id_effet_attaque_adv']=47;}
                            elseif($_SESSION['tempete_sable']>0){$_SESSION['id_effet_attaque_adv']=35;}
                            else{$_SESSION['id_effet_attaque_adv']=55;}
                            }
				//44 rafale
			$rafale_adv=1;
			if($_SESSION['id_effet_attaque_adv']==44 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==44 AND $effet_adv==1) {$proba_rafale_adv=rand(1,100); if($proba_rafale_adv<38){$rafale_adv=2;} elseif($proba_rafale_adv<=75){$rafale_adv=3;} elseif($proba_rafale_adv<=87){$rafale_adv=4;}else{$rafale_adv=5;}}
				//45 puissance cachée
			if($_SESSION['id_effet_attaque_adv']==45 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==45 AND $effet_adv==1) {$proba_type_attaque_adv=rand(1,17); $reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE id=:id') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id' => $proba_type_attaque_adv)); $donnees = $reponse->fetch();$_SESSION['type_attaque_adv'] = $donnees['sur']; $_SESSION['puissance_attaque_adv'] = rand(1,100); }
				//48 recharge
			if($_SESSION['id_effet_attaque_adv']==48 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==48 AND $effet_adv==1) {$_SESSION['recharge_adv']=1; }
				//49 mur lumière
			if($_SESSION['id_effet_attaque_adv']==49 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==49 AND $effet_adv==1) {$_SESSION['mur_lumiere_adv']=5; }
				//50 abri
			$abri_adv=0;
			if($_SESSION['id_effet_attaque_adv']==50 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==50 AND $effet_adv==1) {$abri_adv=1; $_SESSION['esquive_adv']=1;}
				//125 tenacite
			$tenacite_adv=0;
			if($_SESSION['id_effet_attaque_adv']==125 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==125 AND $effet_adv==1) {$tenacite_adv=1;}	
                                //53 rune protect
			if($_SESSION['id_effet_attaque_adv']==53 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==53 AND $effet_adv==1) {$_SESSION['rune_protect_adv']=6;}
				//57 casse brique
			if($_SESSION['id_effet_attaque_adv']==57 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==57 AND $effet_adv==1) {$_SESSION['mur_lumiere']=0;$_SESSION['protection']=0;}
				//58 esq +
			if($_SESSION['id_effet_attaque_adv']==58 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==58 AND $effet_adv==1) {$_SESSION['esq_adv']=$_SESSION['esq_adv']+5;if($_SESSION['esq_adv']>30){$_SESSION['esq_adv']=30;}if($_SESSION['esq_adv']<-30){$_SESSION['esq_adv']=-30;}}
				//59 esq ++
			if($_SESSION['id_effet_attaque_adv']==59 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==59 AND $effet_adv==1) {$_SESSION['esq_adv']=$_SESSION['esq_adv']+10;if($_SESSION['esq_adv']>30){$_SESSION['esq_adv']=30;}if($_SESSION['esq_adv']<-30){$_SESSION['esq_adv']=-30;}}
				//60 esq +++
			if($_SESSION['id_effet_attaque_adv']==60 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==60 AND $effet_adv==1) {$_SESSION['esq_adv']=$_SESSION['esq_adv']+15;if($_SESSION['esq_adv']>30){$_SESSION['esq_adv']=30;}if($_SESSION['esq_adv']<-30){$_SESSION['esq_adv']=-30;}}
				//61 esq -
			if($_SESSION['id_effet_attaque_adv']==61 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==61 AND $effet_adv==1) {$_SESSION['esq']=$_SESSION['esq']-5;if($_SESSION['esq']>30){$_SESSION['esq']=30;}if($_SESSION['esq']<-30){$_SESSION['esq']=-30;}}
				//62 esq --
			if($_SESSION['id_effet_attaque_adv']==62 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==62 AND $effet_adv==1) {$_SESSION['esq']=$_SESSION['esq']-10;if($_SESSION['esq']>30){$_SESSION['esq']=30;}if($_SESSION['esq']<-30){$_SESSION['esq']=-30;}}
				//63 esq ---
			if($_SESSION['id_effet_attaque_adv']==63 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==63 AND $effet_adv==1) {$_SESSION['esq']=$_SESSION['esq']-15;if($_SESSION['esq']>30){$_SESSION['esq']=30;}if($_SESSION['esq']<-30){$_SESSION['esq']=-30;}}
				//64 protection
			if($_SESSION['id_effet_attaque_adv']==64 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==64 AND $effet_adv==1) {$_SESSION['protection_adv']=5; }
				//66 repos
			if($_SESSION['id_effet_attaque_adv']==66 AND $_SESSION['pv_adv']==$_SESSION['pv_max_adv'] OR $_SESSION['id_effet2_attaque_adv']==66 AND $_SESSION['pv_adv']==$_SESSION['pv_max_adv']){$effet_adv=0;}		
			if($_SESSION['id_effet_attaque_adv']==66 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==66 AND $effet_adv==1) {$lance_repos_adv=1;$_SESSION['statut_dodo_adv']=1;$_SESSION['fin_dodo_adv']=3;$_SESSION['statut_poison_adv']=0;$_SESSION['statut_poison_grave_adv']=0;if($_SESSION['statut_paralyse_adv']==1){$_SESSION['vit_adv']=$donnees['vit_adv']*4;}$_SESSION['statut_paralyse_adv']=0;$_SESSION['statut_brule_adv']=0;$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}
				//67 attraction
			if($_SESSION['id_effet_attaque_adv']==67 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==67 AND $effet_adv==1) {if($_SESSION['sexe']=="M" AND $_SESSION['sexe_adv']=="F" OR $_SESSION['sexe']=="F" AND $_SESSION['sexe_adv']=="M"){$_SESSION['attraction_adv']=1 ;}}
				//70 soin
			if($_SESSION['id_effet_attaque_adv']==70 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==70 AND $effet_adv==1) {$_SESSION['pv_adv']=$_SESSION['pv_adv']+floor($_SESSION['pv_max_adv']*$soins_adv);if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}}
				//107 aurore
                        if($_SESSION['zenith']>0){$multi_soins_climat=0.66;}
                        elseif($_SESSION['danse_pluie']>0 OR $_SESSION['tempete_sable']>0 OR $_SESSION['grele']>0){$multi_soins_climat=0.25;}
			if($_SESSION['id_effet_attaque_adv']==107 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==107 AND $effet_adv==1) {$_SESSION['pv_adv']=$_SESSION['pv_adv']+floor($_SESSION['pv_max_adv']*$multi_soins_climat);if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}}
                                //71 peur
			if($_SESSION['id_effet_attaque_adv']==71 AND $ordre_attaque_adv==2 OR $_SESSION['id_effet2_attaque_adv']==71 AND $ordre_attaque_adv==2) {$effet_adv=0;}
			if($_SESSION['id_effet_attaque_adv']==71 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==71 AND $effet_adv==1) {$peur=1;}
				//123 piqué appeurement
                        if($_SESSION['id_effet_attaque_adv']==123 AND $ordre_attaque_adv==2 OR $_SESSION['id_effet2_attaque_adv']==123 AND $ordre_attaque_adv==2) {$effet_adv=0;}
                        if($_SESSION['id_effet_attaque_adv']==123 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==123 AND $effet_adv==1) {$rand_pr=rand(1,100); if($rand_pr<=30){$peur=1;}}
				//124 ronflementt
                        if($_SESSION['id_effet_attaque_adv']==124 AND $ordre_attaque_adv==2 OR $_SESSION['id_effet2_attaque_adv']==124 AND $ordre_attaque_adv==2) {$effet_adv=0;}
                        if($_SESSION['id_effet_attaque_adv']==124 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==124 AND $effet_adv==1) {if($_SESSION['statut_dodo_adv']>0){$rand_pr=rand(1,100); if($rand_pr<=30){$peur=1;}}}
				//72 danse flamme
			if($_SESSION['id_effet_attaque_adv']==72 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==72 AND $effet_adv==1) {$_SESSION['danse_flamme']=rand(3,6); }
				//73 tour rapide
			if($_SESSION['id_effet_attaque_adv']==73 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==73 AND $effet_adv==1) {$_SESSION['statut_vampigraine_adv']=0;$_SESSION['danse_flamme_adv']=0;$_SESSION['ligotage_adv']=0;}	
				//74 cc++
			if($_SESSION['id_effet_attaque_adv']==74 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==74 AND $effet_adv==1) {$_SESSION['bonus_cc_adv']=10; }	
				//75 croc fatal
			if($_SESSION['id_effet_attaque_adv']==75 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==75 AND $effet_adv==1) {$_SESSION['pv']=ceil($_SESSION['pv']/2); }	
				//109 balance
			if($_SESSION['id_effet_attaque_adv']==109 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==109 AND $effet_adv==1) {$pv_balance=floor(($_SESSION['pv_adv']+$_SESSION['pv'])/2);$_SESSION['pv_adv']=$pv_balance;if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}$_SESSION['pv']=$pv_balance;if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}}		
				//77 ligotage
			if($_SESSION['id_effet_attaque_adv']==77 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==77 AND $effet_adv==1) {$_SESSION['ligotage']=rand(3,6); }	
				//79 esuna
			if($_SESSION['id_effet_attaque_adv']==79 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==79 AND $effet_adv==1) { $_SESSION['statut_confus_adv']=-1;$_SESSION['statut_poison_adv']=0;$_SESSION['statut_poison_grave_adv']=0;$_SESSION['statut_gel_adv']=0;if($_SESSION['statut_paralyse_adv']==1){$_SESSION['vit_adv']=$donnees['vit_adv']*4;}$_SESSION['statut_paralyse_adv']=0;$_SESSION['statut_brule_adv']=0;$_SESSION['statut_dodo_adv']=0;}	
				//84 lance boue
			if($_SESSION['id_effet_attaque_adv']==84 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==84 AND $effet_adv==1) {$_SESSION['lance_boue_adv']=1;}	
				//97 tourniquet
			if($_SESSION['id_effet_attaque_adv']==97 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==97 AND $effet_adv==1) {$_SESSION['tourniquet_adv']=1;}	
				//89 morphing
			if($_SESSION['id_effet_attaque_adv']==89 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==89 AND $effet_adv==1) 	
				{
				$_SESSION['type_adv']=$_SESSION['type'];$_SESSION['type2_adv']=$_SESSION['type2'];
				$_SESSION['attaque1_nb_adv']=$_SESSION['attaque1_nb'];$_SESSION['attaque2_nb_adv']=$_SESSION['attaque2_nb'];$_SESSION['attaque3_nb_adv']=$_SESSION['attaque3_nb'];$_SESSION['attaque4_nb_adv']=$_SESSION['attaque4_nb'];
				$_SESSION['nom_attaque1_adv'] = $_SESSION['nom_attaque1']; $_SESSION['type_attaque1_adv'] = $_SESSION['type_attaque1']; $_SESSION['puissance_attaque1_adv'] = $_SESSION['puissance_attaque1'];
				$_SESSION['prec_attaque1_adv'] = $_SESSION['prec_attaque1']; $_SESSION['cc_attaque1_adv'] = $_SESSION['cc_attaque1']; $_SESSION['classe_attaque1_adv'] = $_SESSION['classe_attaque1'];
				$_SESSION['priorite_attaque1_adv'] = $_SESSION['priorite_attaque1']; $_SESSION['esquive_attaque1_adv'] = $_SESSION['esquive_attaque1']; $_SESSION['cible_attaque1_adv'] = $_SESSION['cible_attaque1'];
				$_SESSION['id_effet_attaque1_adv'] = $_SESSION['id_effet_attaque1']; $_SESSION['id_effet2_attaque1_adv'] = $_SESSION['id_effet2_attaque1']; $_SESSION['proba_attaque1_adv'] = $_SESSION['proba_attaque1'];
				$_SESSION['nom_attaque2_adv'] = $_SESSION['nom_attaque2']; $_SESSION['type_attaque2_adv'] = $_SESSION['type_attaque2']; $_SESSION['puissance_attaque2_adv'] = $_SESSION['puissance_attaque2'];
				$_SESSION['prec_attaque2_adv'] = $_SESSION['prec_attaque2']; $_SESSION['cc_attaque2_adv'] = $_SESSION['cc_attaque2']; $_SESSION['classe_attaque2_adv'] = $_SESSION['classe_attaque2'];
				$_SESSION['priorite_attaque2_adv'] = $_SESSION['priorite_attaque2']; $_SESSION['esquive_attaque2_adv'] = $_SESSION['esquive_attaque2']; $_SESSION['cible_attaque2_adv'] = $_SESSION['cible_attaque2'];
				$_SESSION['id_effet_attaque2_adv'] = $_SESSION['id_effet_attaque2']; $_SESSION['id_effet2_attaque2_adv'] = $_SESSION['id_effet2_attaque2']; $_SESSION['proba_attaque2_adv'] = $_SESSION['proba_attaque2'];
				$_SESSION['nom_attaque3_adv'] = $_SESSION['nom_attaque3']; $_SESSION['type_attaque3_adv'] = $_SESSION['type_attaque3']; $_SESSION['puissance_attaque3_adv'] = $_SESSION['puissance_attaque3'];
				$_SESSION['prec_attaque3_adv'] = $_SESSION['prec_attaque3']; $_SESSION['cc_attaque3_adv'] = $_SESSION['cc_attaque3']; $_SESSION['classe_attaque3_adv'] = $_SESSION['classe_attaque3'];
				$_SESSION['priorite_attaque3_adv'] = $_SESSION['priorite_attaque3']; $_SESSION['esquive_attaque3_adv'] = $_SESSION['esquive_attaque3']; $_SESSION['cible_attaque3_adv'] = $_SESSION['cible_attaque3'];
				$_SESSION['id_effet_attaque3_adv'] = $_SESSION['id_effet_attaque3']; $_SESSION['id_effet2_attaque3_adv'] = $_SESSION['id_effet2_attaque3']; $_SESSION['proba_attaque3_adv'] = $_SESSION['proba_attaque3'];
				$_SESSION['nom_attaque4_adv'] = $_SESSION['nom_attaque4']; $_SESSION['type_attaque4_adv'] = $_SESSION['type_attaque4']; $_SESSION['puissance_attaque4_adv'] = $_SESSION['puissance_attaque4'];
				$_SESSION['prec_attaque4_adv'] = $_SESSION['prec_attaque4']; $_SESSION['cc_attaque4_adv'] = $_SESSION['cc_attaque4']; $_SESSION['classe_attaque4_adv'] = $_SESSION['classe_attaque4'];
				$_SESSION['priorite_attaque4_adv'] = $_SESSION['priorite_attaque4']; $_SESSION['esquive_attaque4_adv'] = $_SESSION['esquive_attaque4']; $_SESSION['cible_attaque4_adv'] = $_SESSION['cible_attaque4'];
				$_SESSION['id_effet_attaque4_adv'] = $_SESSION['id_effet_attaque4']; $_SESSION['id_effet2_attaque4_adv'] = $_SESSION['id_effet2_attaque4']; $_SESSION['proba_attaque4_adv'] = $_SESSION['proba_attaque4'];
				$_SESSION['att_adv']=$_SESSION['att'];
				$_SESSION['def_adv']=$_SESSION['def'];
				$_SESSION['vit_adv']=$_SESSION['vit'];
				$_SESSION['attspe_adv']=$_SESSION['attspe'];
				$_SESSION['defspe_adv']=$_SESSION['defspe'];
				$_SESSION['att_max_adv']=$_SESSION['att_max'];
				$_SESSION['def_max_adv']=$_SESSION['def_max'];
				$_SESSION['vit_max_adv']=$_SESSION['vit_max'];
				$_SESSION['attspe_max_adv']=$_SESSION['attspe_max'];
				$_SESSION['defspe_max_adv']=$_SESSION['defspe_max'];
				}
			//96 mania
			if($_SESSION['id_effet_attaque_adv']==96 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==96 AND $effet_adv==1) 
				{
				if($_SESSION['mania_adv']==0){$_SESSION['mania_adv']=rand(1,2);$_SESSION['attaque_auto_adv']=1;$effet_adv=0;}
				elseif($_SESSION['mania_adv']==1){$_SESSION['mania_adv']=0;$_SESSION['statut_confus_adv']=rand(2,5);$statut_confus_begin_adv=1;}
				elseif($_SESSION['mania_adv']>1){$_SESSION['mania_adv']=$_SESSION['mania_adv']-1;$_SESSION['attaque_auto_adv']=1;$effet_adv=0;}
				}
			//105 prélevement destin	
			if($_SESSION['id_effet_attaque_adv']==105 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==105 AND $effet_adv==1) {$_SESSION['destin_adv']=1;}
			//106 ampleur	
                        if($_SESSION['id_effet_attaque_adv']==106 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==106 AND $effet_adv==1) {$_SESSION['puissance_attaque_adv']=rand(10,150);if($_SESSION['esquive_attaque']==1 AND $_SESSION['last_attaque']==45){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*2;}}
			//117 contre
                        if($_SESSION['id_effet_attaque_adv']==117 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==117 AND $effet_adv==1) {
                            $contre=($_SESSION['pv_adv']/$_SESSION['pv_max_adv'])*100;
                            if($contre<3.12){$_SESSION['puissance_attaque_adv']=200;}
                            elseif($contre<9.37){$_SESSION['puissance_attaque_adv']=150;}
                            elseif($contre<20.31){$_SESSION['puissance_attaque_adv']=100;}
                            elseif($contre<34.37){$_SESSION['puissance_attaque_adv']=80;}
                            elseif($contre<67.19){$_SESSION['puissance_attaque_adv']=40;}
                            else{$_SESSION['puissance_attaque_adv']=20;}
                            }    
                         //119 éruption	
                        if($_SESSION['id_effet_attaque_adv']==119 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==119 AND $effet_adv==1) {$_SESSION['puissance_attaque_adv']=150*$_SESSION['pv_adv']/$_SESSION['pv_max_adv'];}
			//114 façade
                        if($_SESSION['id_effet_attaque_adv']==114 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==114 AND $effet_adv==1) {if($_SESSION['statut_poison_adv']>0 OR $_SESSION['statut_poison_grave_adv']>0 OR $_SESSION['statut_brule_adv']>0 OR $_SESSION['statut_paralyse_adv']>0){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*2;}}
			//112 façade
                        if($_SESSION['id_effet_attaque_adv']==112 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==112 AND $effet_adv==1) {$_SESSION['puissance_attaque_adv']=102-ceil($_SESSION['bonheur_adv']*2/5);if($_SESSION['puissance_attaque_adv']<1){$_SESSION['puissance_attaque_adv']=1;}if($_SESSION['puissance_attaque_adv']>102){$_SESSION['puissance_attaque_adv']=102;}}
			//113 retour
                        if($_SESSION['id_effet_attaque_adv']==113 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==113 AND $effet_adv==1) {$_SESSION['puissance_attaque_adv']=ceil($_SESSION['bonheur_adv']*2/5);if($_SESSION['puissance_attaque_adv']<1){$_SESSION['puissance_attaque_adv']=1;}if($_SESSION['puissance_attaque_adv']>102){$_SESSION['puissance_attaque_adv']=102;}}
			
			//effets variation de statut
				//39 confusion
			if($_SESSION['id_effet_attaque_adv']==39 AND $effet_adv==1 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==39 AND $effet_adv==1 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}		
			if($_SESSION['id_effet_attaque_adv']==39 AND $effet_adv==1 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==39 AND $effet_adv==1 AND $_SESSION['rune_protect']==0) {if($_SESSION['statut_confus']==-1){$_SESSION['statut_confus']=rand(2,5);if($ordre_attaque_adv==2){$statut_confus_begin=1;}}else{$effet_adv=0;}}		
				//41 empoisonnement
			if($_SESSION['id_effet_attaque_adv']==41 AND $effet_adv==1 AND $_SESSION['statut_poison']==0 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==41 AND $effet_adv==1 AND $_SESSION['statut_poison']==0 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}			
			if($_SESSION['id_effet_attaque_adv']==41 AND $effet_adv==1 AND $_SESSION['statut_poison']!=0 OR $_SESSION['id_effet2_attaque_adv']==41 AND $effet_adv==1 AND $_SESSION['statut_poison']!=0){$effet_adv=0;}
			if($_SESSION['id_effet_attaque_adv']==41 AND $_SESSION['type']=="poison" OR $_SESSION['id_effet_attaque_adv']==41 AND $_SESSION['type2']=="poison" OR $_SESSION['id_effet_attaque_adv']==41 AND $_SESSION['type']=="acier" OR $_SESSION['id_effet_attaque_adv']==41 AND $_SESSION['type2']=="acier"){$effet_adv=0;}			
			if($_SESSION['id_effet_attaque_adv']==41 AND $effet_adv==1 AND $_SESSION['statut_poison']==0 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==41 AND $effet_adv==1 AND $_SESSION['statut_poison']==0 AND $_SESSION['rune_protect']==0) {$_SESSION['statut_poison']=1;}
				//42 empoisonnement grave
			if($_SESSION['id_effet_attaque_adv']==42 AND $effet_adv==1 AND $_SESSION['statut_poison_grave']==0 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==42 AND $effet_adv==1 AND $_SESSION['statut_poison_grave']==0 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}
			if($_SESSION['id_effet_attaque_adv']==42 AND $_SESSION['type']=="poison" OR $_SESSION['id_effet_attaque_adv']==42 AND $_SESSION['type2']=="poison" OR $_SESSION['id_effet_attaque_adv']==42 AND $_SESSION['type']=="acier" OR $_SESSION['id_effet_attaque_adv']==42 AND $_SESSION['type2']=="acier"){$effet_adv=0;}			
			if($_SESSION['id_effet_attaque_adv']==42 AND $effet_adv==1 AND $_SESSION['statut_poison_grave']==0 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==42 AND $effet_adv==1 AND $_SESSION['statut_poison_grave']==0 AND $_SESSION['rune_protect']==0) {$_SESSION['statut_poison_grave']=1;}
				//47 gel
			if($_SESSION['id_effet_attaque_adv']==47 AND $_SESSION['type']=="glace" OR $_SESSION['id_effet_attaque_adv']==47 AND $_SESSION['type2']=="glace"){$effet_adv=0;}
			if($_SESSION['id_effet_attaque_adv']==47 AND $effet_adv==1 AND $_SESSION['statut_gel']!=1 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==47 AND $effet_adv==1 AND $_SESSION['statut_gel']!=1 AND $_SESSION['rune_protect']!=0) {$effet_adv=0; }				
			if($_SESSION['id_effet_attaque_adv']==47 AND $effet_adv==1 AND $_SESSION['statut_gel']!=0 OR $_SESSION['id_effet2_attaque_adv']==47 AND $effet_adv==1 AND $_SESSION['statut_gel']!=0){$effet_adv=0;}	
			if($_SESSION['id_effet_attaque_adv']==47 AND $effet_adv==1 AND $_SESSION['statut_gel']!=1 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==47 AND $effet_adv==1 AND $_SESSION['statut_gel']!=1 AND $_SESSION['rune_protect']==0) {$_SESSION['statut_gel']=1; }
				//55 paralysie
			if($_SESSION['id_effet_attaque_adv']==55 AND $effet_adv==1 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==55 AND $effet_adv==1 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}
			if($_SESSION['id_effet_attaque_adv']==55 AND $effet_adv==1 AND $_SESSION['statut_paralyse']!=0 OR $_SESSION['id_effet2_attaque_adv']==55 AND $effet_adv==1 AND $_SESSION['statut_paralyse']!=0){$effet_adv=0;}	
			if($_SESSION['id_effet_attaque_adv']==55 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==55 AND $_SESSION['rune_protect']==0 AND $effet_adv==1 AND $_SESSION['rune_protect']==0) {$_SESSION['statut_paralyse']=1; $_SESSION['vit']=$_SESSION['vit']/4;}
				//65 brule
			if($_SESSION['id_effet_attaque_adv']==65 AND $_SESSION['type']=="feu" OR $_SESSION['id_effet_attaque_adv']==65 AND $_SESSION['type2']=="feu"){$effet_adv=0;}
			if($_SESSION['id_effet_attaque_adv']==65 AND $effet_adv==1 AND $_SESSION['statut_brule']==0 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==65 AND $effet_adv==1 AND $_SESSION['statut_brule']==0 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}
			if($_SESSION['id_effet_attaque_adv']==65 AND $effet_adv==1 AND $_SESSION['statut_brule']!=0 OR $_SESSION['id_effet2_attaque_adv']==65 AND $effet_adv==1 AND $_SESSION['statut_brule']!=0){$effet_adv=0;}
			if($_SESSION['id_effet_attaque_adv']==65 AND $effet_adv==1 AND $_SESSION['statut_brule']==0 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==65 AND $effet_adv==1 AND $_SESSION['statut_brule']==0 AND $_SESSION['rune_protect']==0) {$_SESSION['statut_brule']=1;$_SESSION['att']=$_SESSION['att']/2;}
				//69 sommeil
			if($_SESSION['id_effet_attaque_adv']==69 AND $effet_adv==1 AND $_SESSION['statut_dodo']!=1 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==69 AND $effet_adv==1 AND $_SESSION['statut_dodo']!=1 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}
			if($_SESSION['id_effet_attaque_adv']==69 AND $effet_adv==1 AND $_SESSION['statut_dodo']!=0 OR $_SESSION['id_effet2_attaque_adv']==69 AND $effet_adv==1 AND $_SESSION['statut_dodo']!=0){$effet_adv=0;}	
			if($_SESSION['id_effet_attaque_adv']==69 AND $effet_adv==1 AND $_SESSION['statut_dodo']!=1 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==69 AND $effet_adv==1 AND $_SESSION['statut_dodo']!=1 AND $_SESSION['rune_protect']==0) {$_SESSION['statut_dodo']=1;$_SESSION['fin_dodo']=rand(2,8);if($ordre_attaque_adv==2){$dodo_now=1;}}
				//80 triplattaque
			if($_SESSION['id_effet_attaque_adv']==80 AND $effet_adv==1 AND $_SESSION['rune_protect']!=0 OR $_SESSION['id_effet2_attaque_adv']==80 AND $effet_adv==1 AND $_SESSION['statut_brule']==0 AND $_SESSION['rune_protect']!=0) {$effet_adv=0;}
			if($_SESSION['id_effet_attaque_adv']==80 AND $effet_adv==1 AND $_SESSION['rune_protect']==0 OR $_SESSION['id_effet2_attaque_adv']==80 AND $effet_adv==1 AND $_SESSION['rune_protect']==0) {$triplattaque1_adv=rand(1,100);$triplattaque2_adv=rand(1,100); $triplattaque3_adv=rand(1,100);  if($triplattaque1_adv<=20){$_SESSION['statut_brule']=1;$triplattaque_brule_adv=1;}elseif($triplattaque2_adv<=20){$_SESSION['statut_paralyse']=1;$_SESSION['vit']=$donnees['vit']/4;$triplattaque_paralyse_adv=1;}elseif($triplattaque3_adv<=20){$_SESSION['statut_gel']=1;$triplattaque_gel_adv=1;}}
				//68 vampigraine
			if($_SESSION['id_effet_attaque_adv']==68 AND $effet_adv==1 AND $_SESSION['statut_vampigraine']!=0 OR $_SESSION['id_effet2_attaque_adv']==68 AND $effet_adv==1 AND $_SESSION['statut_vampigraine']!=0){$effet_adv=0;}
			if($_SESSION['id_effet_attaque_adv']==68 AND $effet_adv==1 AND $_SESSION['statut_vampigraine']==0 OR $_SESSION['id_effet2_attaque_adv']==68 AND $effet_adv==1 AND $_SESSION['statut_vampigraine']==0) {$_SESSION['statut_vampigraine']=1;}
				//1 ko
			if($_SESSION['id_effet_attaque_adv']==1 AND $effet_adv==1 AND $_SESSION['lvl_adv']>=$_SESSION['lvl'] OR $_SESSION['id_effet2_attaque_adv']==1 AND $effet_adv==1 AND $_SESSION['lvl_adv']>=$_SESSION['lvl']) {$_SESSION['pv']=0;}	
			if($_SESSION['id_effet_attaque_adv']==1 AND $effet_adv==1 AND $_SESSION['lvl_adv']<$_SESSION['lvl'] OR $_SESSION['id_effet2_attaque_adv']==1 AND $effet_adv==1 AND $_SESSION['lvl_adv']<=$_SESSION['lvl']) {$effet_adv=0;}
				
				
			if($_SESSION['def_adv']<=0){$_SESSION['def_adv']=1;}if($_SESSION['att_adv']<=0){$_SESSION['att_adv']=1;}if($_SESSION['vit_adv']<=0){$_SESSION['vit_adv']=1;}if($_SESSION['defspe_adv']<=0){$_SESSION['defspe_adv']=1;}if($_SESSION['attspe_adv']<=0){$_SESSION['attspe_adv']=1;}			
			if($_SESSION['def']<=0){$_SESSION['def']=1;}if($_SESSION['att']<=0){$_SESSION['att']=1;}if($_SESSION['vit']<=0){$_SESSION['vit']=1;}if($_SESSION['defspe']<=0){$_SESSION['defspe']=1;}if($_SESSION['attspe']<=0){$_SESSION['attspe']=1;}			
		//calcul des dégats
			$hasard_degats=rand(85,115);
			$hasard_cc=rand(1,100);
			$hasard_cc=$hasard_cc+$_SESSION['bonus_cc_adv'];
			if($_SESSION['cc_attaque_adv']==1){if($hasard_cc<7){$cc_adv=1.5;}else{$cc_adv=1;}}
			if($_SESSION['cc_attaque_adv']==2){if($hasard_cc<15){$cc_adv=1.5;}else{$cc_adv=1;}}
			$degats_adv=0;
			
			//influence des items
			if($_SESSION['objet_adv']==102 AND $_SESSION['type_attaque_adv']=="acier"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//peau métal
			if($_SESSION['objet_adv']==103 AND $_SESSION['type_attaque_adv']=="electrique"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//aimant
			if($_SESSION['objet_adv']==104 AND $_SESSION['type_attaque_adv']=="vol"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//bec pointu
			if($_SESSION['objet_adv']==105 AND $_SESSION['type_attaque_adv']=="combat"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//ceinture noire
			if($_SESSION['objet_adv']==106 AND $_SESSION['type_attaque_adv']=="feu"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//charbon
			if($_SESSION['objet_adv']==107 AND $_SESSION['type_attaque_adv']=="dragon"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//croc dragon
			if($_SESSION['objet_adv']==108 AND $_SESSION['type_attaque_adv']=="psy"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//cuillertordue
			if($_SESSION['objet_adv']==109 AND $_SESSION['type_attaque_adv']=="eau"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//eau mystique
			if($_SESSION['objet_adv']==110 AND $_SESSION['type_attaque_adv']=="glace"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//glaceternelle
			if($_SESSION['objet_adv']==111 AND $_SESSION['type_attaque_adv']=="plante"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//grain miracle
			if($_SESSION['objet_adv']==112 AND $_SESSION['type_attaque_adv']=="normal"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//mouchoir soie
			if($_SESSION['objet_adv']==113 AND $_SESSION['type_attaque_adv']=="poison"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//pic venin
			if($_SESSION['objet_adv']==114 AND $_SESSION['type_attaque_adv']=="insecte"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//poudre argent
			if($_SESSION['objet_adv']==115 AND $_SESSION['type_attaque_adv']=="sol"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//sable doux
			if($_SESSION['objet_adv']==117 AND $_SESSION['type_attaque_adv']=="spectre"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//rune sort
			if($_SESSION['objet_adv']==118 AND $_SESSION['type_attaque_adv']=="roche"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//pierre dure
			if($_SESSION['objet_adv']==119 AND $_SESSION['type_attaque_adv']=="tenebre"){$_SESSION['puissance_attaque_adv']=$_SESSION['puissance_attaque_adv']*1.1;}//lunette noir
			
			
			if($_SESSION['classe_attaque_adv']=="physique"){$degats_adv=$_SESSION['lvl_adv']*0.4;$degats_adv=$degats_adv+2; $degats_adv=$degats_adv*$_SESSION['att_adv']*$_SESSION['puissance_attaque_adv'];$defense_adv=$_SESSION['def']*50;$degats_adv=$degats_adv/$defense_adv;$degats_adv=$degats_adv+2; $degats_adv=$degats_adv*$multiplicateur_adv;$degats_adv=$degats_adv*$cc_adv*$hasard_degats*$rafale_adv/100;if($_SESSION['protection']>0){$degats_adv=$degats_adv/2;}}	
			if($_SESSION['classe_attaque_adv']=="speciale"){$degats_adv=$_SESSION['lvl_adv']*0.4;$degats_adv=$degats_adv+2; $degats_adv=$degats_adv*$_SESSION['attspe_adv']*$_SESSION['puissance_attaque_adv'];$defense_adv=$_SESSION['defspe']*50;$degats_adv=$degats_adv/$defense_adv;$degats_adv=$degats_adv+2; $degats_adv=$degats_adv*$multiplicateur_adv;$degats_adv=$degats_adv*$cc_adv*$hasard_degats*$rafale_adv/100;if($_SESSION['mur_lumiere']>0){$degats_adv=$degats_adv/2;}}	
			if($_SESSION['type_attaque_adv']==$_SESSION['type_adv'] OR $_SESSION['type_attaque_adv']==$_SESSION['type2_adv']){$degats_adv=$degats_adv*1.5;}			
			if($_SESSION['puissance_attaque_adv']==0){$degats_adv=0;}
			//82 frappe atlas
			if($_SESSION['id_effet_attaque_adv']==82 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==82 AND $effet_adv==1) { if($multiplicateur_adv!=0){$degats_adv=$_SESSION['lvl_adv'];}}
			//86 sonicboom
			if($_SESSION['id_effet_attaque_adv']==86 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==86 AND $effet_adv==1) { if($multiplicateur_adv!=0){$degats_adv=20;}}
			//118 effort
                        if($_SESSION['id_effet_attaque_adv']==118 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==118 AND $effet_adv==1) {if($_SESSION['pv']>$_SESSION['pv_adv']){$degats_adv=$_SESSION['pv']-$_SESSION['pv_adv'];}}
			//95 draco-rage
			if($_SESSION['id_effet_attaque_adv']==95 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==95 AND $effet_adv==1) { if($multiplicateur_adv!=0){$degats_adv=40;}}
			//88 25%deg
			if($_SESSION['id_effet_attaque_adv']==88 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==88 AND $effet_adv==1) { if($multiplicateur_adv!=0){$degats_adv=floor($_SESSION['pv_max']/4);}}
			//84 effet lance-boue
			if($_SESSION['type_attaque_adv']=="electrique" AND $_SESSION['lance_boue']==1){$degats_adv=floor($degats_adv/2);}
			//97 effet tourniquet
			if($_SESSION['type_attaque_adv']=="feu" AND $_SESSION['tourniquet']==1){$degats_adv=floor($degats_adv/2);}
			//101 riposte
			if($_SESSION['classe_attaque']=="physique" AND $_SESSION['id_effet_attaque_adv']==101){$degats_adv=$degats*2;$_SESSION['puissance_attaque_adv']=1;}elseif($_SESSION['classe_attaque']!="physique" AND $_SESSION['id_effet_attaque_adv']==101){$echec_attaque_adv=1;}
			//102 voile miroir
			if($_SESSION['classe_attaque']=="speciale" AND $_SESSION['id_effet_attaque_adv']==102){$degats_adv=$degats*2;$_SESSION['puissance_attaque_adv']=1;}elseif($_SESSION['classe_attaque']!="speciale" AND $_SESSION['id_effet_attaque_adv']==102){$echec_attaque_adv=1;}
			//effet abri
			if($abri==1){$degats_adv=0;}
			
			$degats_adv=floor($degats_adv);if($degats_adv<1 AND $degats_adv!=0) {$degats_adv=1;}
			if($degats_adv>$_SESSION['pv']){$degats_adv=$_SESSION['pv'];}
			$_SESSION['pv']=$_SESSION['pv']-$degats_adv; if($_SESSION['pv']<0){$_SESSION['pv']=0;} 
			//120 faux-chage
			if($_SESSION['pv']==0 AND $_SESSION['id_effet_attaque_adv']==120){$_SESSION['pv']=1; $degats_adv=$degats_adv-1;}
			//effet tenacite
                        if($_SESSION['pv']<=0 AND $tenacite==1){$_SESSION['pv']=1; $degats_adv=$degats_adv-1;}
			
			//2 att -
				if($_SESSION['id_effet_attaque_adv']==2 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==2 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//3 def -
				if($_SESSION['id_effet_attaque_adv']==3 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==3 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
					//4 vit -
				if($_SESSION['id_effet_attaque_adv']==4 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==4 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					}
					//5 att spe -
				if($_SESSION['id_effet_attaque_adv']==5 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==5 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//6 defspe -
				if($_SESSION['id_effet_attaque_adv']==6 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==6 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
					//7 att --
				if($_SESSION['id_effet_attaque_adv']==7 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==7 AND $effet_adv==1)
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//8 def --
				if($_SESSION['id_effet_attaque_adv']==8 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==8 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
					//9 vit --
				if($_SESSION['id_effet_attaque_adv']==9 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==9 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					}
					//10 att spe --
				if($_SESSION['id_effet_attaque_adv']==10 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==10 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//11 defspe --
				if($_SESSION['id_effet_attaque_adv']==11 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==11 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
					//12 att ---
				if($_SESSION['id_effet_attaque_adv']==12 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==12 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//13 def ---
				if($_SESSION['id_effet_attaque_adv']==13 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==13 AND $effet_adv==1)
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
					//14 vit ---
				if($_SESSION['id_effet_attaque_adv']==14 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==14 AND $effet_adv==1)
					{
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					}
					//15 att spe ---
				if($_SESSION['id_effet_attaque_adv']==15 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==15 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//16 defspe ---
				if($_SESSION['id_effet_attaque_adv']==16 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==16 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
					//17 att +
				if($_SESSION['id_effet_attaque_adv']==17 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==17 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//18 def +
				if($_SESSION['id_effet_attaque_adv']==18 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==18 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}
					//19 vit +
				if($_SESSION['id_effet_attaque_adv']==19 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==19 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					}
					//20 att spe +
				if($_SESSION['id_effet_attaque_adv']==20 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==20 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//21 defspe +
				if($_SESSION['id_effet_attaque_adv']==21 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==21 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
					// 93 vent argenté
				if($_SESSION['id_effet_attaque']==93 AND $effet==1 OR $_SESSION['id_effet2_attaque']==93 AND $effet==1)
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}	
					//22 att ++
				if($_SESSION['id_effet_attaque_adv']==22 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==22 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//23 def ++
				if($_SESSION['id_effet_attaque_adv']==23 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==23 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}
					//24 vit ++
				if($_SESSION['id_effet_attaque_adv']==24 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==24 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					}
					//25 att spe ++
				if($_SESSION['id_effet_attaque_adv']==25 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==25 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//83 att spe lanceur --
				if($_SESSION['id_effet_attaque_adv']==83 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==83 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//26 defspe ++
				if($_SESSION['id_effet_attaque_adv']==26 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==26 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
					//27 att +++
				if($_SESSION['id_effet_attaque_adv']==27 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==27 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//28 def +++
				if($_SESSION['id_effet_attaque_adv']==28 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==28 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}
					//29 vit +++
				if($_SESSION['id_effet_attaque_adv']==29 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==29 AND $effet_adv==1)
					{
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					}
					//30 att spe +++
				if($_SESSION['id_effet_attaque_adv']==30 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==30 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//31 defspe +++
				if($_SESSION['id_effet_attaque_adv']==31 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==31 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
                                        //111 boost
				if($_SESSION['id_effet_attaque_adv']==111 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==111 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$ex_mul;
                                        $ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$ex_mul;
                                        $ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$ex_mul;
                                        $ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$ex_mul;
                                        $ex_mul=$_SESSION['def_spe']/$_SESSION['def_spe_max'];
					$_SESSION['def_spe_adv']=$_SESSION['def_spe_max_adv']*$ex_mul;
					} 
                                        //116 cognobidon
				if($_SESSION['id_effet_attaque_adv']==116 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==116 AND $effet_adv==1) 
					{
					$mul=4;
                                        $_SESSION['pv']=ceil($_SESSION['pv_adv']/2);
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
                                //121 att spe ennemi +
				if($_SESSION['id_effet_attaque_adv']==121 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==121 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
                                //126 att ennemi ++
				if($_SESSION['id_effet_attaque_adv']==126 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==126 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
				//32 pré +
			if($_SESSION['id_effet_attaque_adv']==32 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==32 AND $effet_adv==1) {$_SESSION['pre_adv']=$_SESSION['pre_adv']+5; if($_SESSION['pre_adv']>30){$_SESSION['pre_adv']=30;}if($_SESSION['pre_adv']<-30){$_SESSION['pre_adv']=-30;}}
				//33 pré ++
			if($_SESSION['id_effet_attaque_adv']==33 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==33 AND $effet_adv==1) {$_SESSION['pre_adv']=$_SESSION['pre_adv']+10; if($_SESSION['pre_adv']>30){$_SESSION['pre_adv']=30;}if($_SESSION['pre_adv']<-30){$_SESSION['pre_adv']=-30;}}
				//34 pré +++
			if($_SESSION['id_effet_attaque_adv']==34 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==34 AND $effet_adv==1) {$_SESSION['pre_adv']=$_SESSION['pre_adv']+15;if($_SESSION['pre_adv']>30){$_SESSION['pre_adv']=30;}if($_SESSION['pre_adv']<-30){$_SESSION['pre_adv']=-30;}}
				//35 pré -
			if($_SESSION['id_effet_attaque_adv']==35 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==35 AND $effet_adv==1) {$_SESSION['pre']=$_SESSION['pre']-5;if($_SESSION['pre']>30){$_SESSION['pre']=30;}if($_SESSION['pre']<-30){$_SESSION['pre']=-30;}}
				//36 pré --
			if($_SESSION['id_effet_attaque_adv']==36 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==36 AND $effet_adv==1) {$_SESSION['pre']=$_SESSION['pre']-10;if($_SESSION['pre']>30){$_SESSION['pre']=30;}if($_SESSION['pre']<-30){$_SESSION['pre']=-30;}}
				//37 pré---
			if($_SESSION['id_effet_attaque_adv']==37 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==37 AND $effet_adv==1) {$_SESSION['pre']=$_SESSION['pre']-15;if($_SESSION['pre']>30){$_SESSION['pre']=30;}if($_SESSION['pre']<-30){$_SESSION['pre']=-30;}}
				//90 self-att -
			if($_SESSION['id_effet_attaque_adv']==90 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==90 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
				//91 self-def -
			if($_SESSION['id_effet_attaque_adv']==91 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==91 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}	
				//94 self-attspe --
			if($_SESSION['id_effet_attaque_adv']==94 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==94 AND $effet_adv==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
			}
		else
			{
			if($hasard_precision_adv!=1000){$echec_attaque_adv=1;}
			if($hasard_precision_adv>=1000){$_SESSION['mania_adv']=0;}
			}
		//effet après attaque
		
			//85 self-k.o.
		if($_SESSION['id_effet_attaque_adv']==85 OR $_SESSION['id_effet2_attaque_adv']==85) {if($hasard_precision_adv!=1000){$_SESSION['pv_adv']=0;}}		
			//76 damocles
		if($_SESSION['id_effet_attaque_adv']==76 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==76 AND $effet_adv==1) {$damocles_adv=floor($degats_adv/3); $_SESSION['pv_adv']=$_SESSION['pv_adv']-$damocles_adv; if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}}
			//92 bélier
		if($_SESSION['id_effet_attaque_adv']==92 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==92 AND $effet_adv==1) {$damocles_adv=floor($degats_adv/4); $_SESSION['pv_adv']=$_SESSION['pv_adv']-$damocles_adv; if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}}
			//52 sangsue
		if($_SESSION['id_effet_attaque_adv']==52 AND $effet_adv==1 OR $_SESSION['id_effet2_attaque_adv']==52 AND $effet_adv==1) {$sangsue_adv=floor($degats_adv/2); $_SESSION['pv_adv']=$_SESSION['pv_adv']+$sangsue_adv; if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$sangsue_adv=$_SESSION['pv_max_adv']-$_SESSION['pv_adv']+$sangsue_adv;$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}}		
			//souffre de poison (41)
		if($_SESSION['statut_poison_adv']!=0){$statut_poison_adv=1;$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
			//souffre de poison gravement (42)
		if($_SESSION['statut_poison_grave_adv']!=0){$statut_poison_grave_adv=1;$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['statut_poison_grave_adv']*$_SESSION['pv_max_adv']/16);$_SESSION['statut_poison_grave_adv']=$_SESSION['statut_poison_grave_adv']+1;}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
			//souffre de brule(65)
		if($_SESSION['statut_brule_adv']!=0){$statut_brule_adv=1;$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
			//souffre de danse flamme(72)
		if($_SESSION['danse_flamme_adv']>0){$danse_flamme_adv=1;$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
			//souffre de ligotage(77)
		if($_SESSION['ligotage_adv']>0){$ligotage_adv=1;$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
			//souffre de vampigraine(68)
		if($_SESSION['statut_vampigraine_adv']==1){if($_SESSION['pv']!=0){$statut_vampigraine_adv=1;$_SESSION['pv_adv']=$_SESSION['pv_adv']-floor($_SESSION['pv_max_adv']/16);$_SESSION['pv']=$_SESSION['pv']+floor($_SESSION['pv_max_adv']/16);}if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}}
		//victoire defaite?
		
		if($_SESSION['pv']==0){$defaite=1;if($_SESSION['destin']==1){$_SESSION['pv_adv']=0;}}
		elseif($_SESSION['pv_adv']==0){$victoire=1;}
		
		if($defaite==1){$attaque_donne=1;}
		if($victoire==1){$attaque_donne=1;}
		//ordre de tour
		if($attaque_donne==1){$premier_coup=3;} else {$premier_coup=1;}
		$attaque_donne=1;
		}
	if($premier_coup==1)//attaque du pokémon actif
		{
		$_SESSION['destin']=0;
		if($attaque_donne==0){$ordre_attaque=1;}if($attaque_donne==1){$ordre_attaque=2;}
		$attaque_possible = 1;
		//78 métronome
		if($_SESSION['id_effet_attaque']==78 OR $_SESSION['id_effet2_attaque']==78) 
			{
			while($proba_metronome==0 OR $proba_metronome==17 OR $proba_metronome==40 OR $proba_metronome==45 OR $proba_metronome==84 OR $proba_metronome==140){$proba_metronome=rand(16,251);}
			$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' =>$proba_metronome));
			$donnees = $reponse->fetch();
			$attaque_lancee=$proba_metronome;
			$_SESSION['nom_attaque'] = $donnees['nom']; $_SESSION['type_attaque'] = $donnees['type']; $_SESSION['puissance_attaque'] = $donnees['puissance'];
			$_SESSION['prec_attaque'] = $donnees['prec']; $_SESSION['cc_attaque'] = $donnees['cc']; $_SESSION['classe_attaque'] = $donnees['classe'];
			$_SESSION['priorite_attaque'] = $donnees['priorite']; $_SESSION['esquive_attaque'] = $donnees['esquive']; $_SESSION['cible_attaque'] = $donnees['cible'];
			$_SESSION['id_effet_attaque'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque'] = $donnees['id_effet2']; $_SESSION['proba_attaque'] = $donnees['proba'];
			}
                //110 blabla dodo
		if($_SESSION['id_effet_attaque']==110 OR $_SESSION['id_effet2_attaque']==110) 
			{
                        $blabla_dodo_ok=0;$test_blabla_dodo=0;
                        while($blabla_dodo_ok==0 AND $test_blabla_dodo<10){
                            $rand_blabladodo=rand(1,4);
                            if($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque1_nb'];}
                            elseif($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque2_nb'];}
                            elseif($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque3_nb'];}
                            elseif($rand_blabladodo==1){$proba_blabla_dodo=$_SESSION['attaque4_nb'];}
                            $test_blabla_dodo++;
                            if($proba_blabla_dodo!=45 AND $proba_blabla_dodo!=342 AND $proba_blabla_dodo!=17 AND $proba_blabla_dodo!=21 AND $proba_blabla_dodo!=140 AND $proba_blabla_dodo!=84 AND $proba_blabla_dodo!=312 AND $proba_blabla_dodo!=40 AND $proba_blabla_dodo!=325){$blabla_dodo_ok=1;}
                        }
                        $echec_blabla_dodo=0;
                        if($blabla_dodo_ok==0){$echec_blabla_dodo=1;}
			$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' =>$proba_blabla_dodo));
			$donnees = $reponse->fetch();
			$attaque_lancee=$proba_metronome;
			$_SESSION['nom_attaque'] = $donnees['nom']; $_SESSION['type_attaque'] = $donnees['type']; $_SESSION['puissance_attaque'] = $donnees['puissance'];
			$_SESSION['prec_attaque'] = $donnees['prec']; $_SESSION['cc_attaque'] = $donnees['cc']; $_SESSION['classe_attaque'] = $donnees['classe'];
			$_SESSION['priorite_attaque'] = $donnees['priorite']; $_SESSION['esquive_attaque'] = $donnees['esquive']; $_SESSION['cible_attaque'] = $donnees['cible'];
			$_SESSION['id_effet_attaque'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque'] = $donnees['id_effet2']; $_SESSION['proba_attaque'] = $donnees['proba'];
			}       
		$proba_effet=rand(1,100);
		if($_SESSION['proba_attaque']>=$proba_effet){$effet=1;} else {$effet=0;}
		
		//EFFET des CLIMATS sur les attaques
		$soins=0.5;
		if($_SESSION['danse_pluie']>0) 
			{
			if($_SESSION['type_attaque']=="feu"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*0.5;} 
			if($_SESSION['type_attaque']=="eau"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*1.5;} 
			if($_SESSION['id_attaque']==40){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']/2;}//lance soleil
			if($_SESSION['id_attaque']==43){$_SESSION['prec_attaque']=100;}//fatal-foudre
			if($_SESSION['id_attaque']==68 OR $_SESSION['id_attaque']==141){$soins=0.25;}//synthèse et rayon lune
			//vent violent et aurore pas encore implémenté
			}
		if($_SESSION['zenith']>0) 
			{
			if($_SESSION['type_attaque']=="feu"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*1.5;} 
			if($_SESSION['type_attaque']=="eau"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*0.5;} 
			//lance soleil en un tour dans "charge"
			if($_SESSION['id_attaque']==68 OR $_SESSION['id_attaque']==141){$soins=0.66;}//synthèse et rayon lune
			}	
		if($_SESSION['tempete_sable']>0) 
			{
			if($_SESSION['id_attaque']==40){$_SESSION['puissance_attaque']=60;}//lance soleil
			if($_SESSION['id_attaque']==68 OR $_SESSION['id_attaque']==141){$soins=0.25;}//synthèse et rayon lune
			//aurore pas encore implémenté
			}	
		if($_SESSION['grele']>0) 
			{
			if($_SESSION['id_attaque']==33){$_SESSION['prec_attaque']=100;}//blizarre
			if($_SESSION['id_attaque']==40){$_SESSION['puissance_attaque']=60;}//lance soleil
			if($_SESSION['id_attaque']==68 OR $_SESSION['id_attaque']==141){$soins=0.25;}//synthèse et rayon lune
			//aurore pas encore implémenté
			}	
			
			
			//ECHEC AUTOMATIQUE
		//cage éclair sur roche
		if($_POST['attaque']==121 AND $_SESSION['type_adv']=="roche" OR $_POST['attaque']==121 AND $_SESSION['type2_adv']=="roche" OR $_POST['attaque']==121 AND $_SESSION['type_adv']=="sol" OR $_POST['attaque']==121 AND $_SESSION['type2_adv']=="sol"){$effet=0;}
		//guillotine et empla'korn sur spectre
		if($_POST['attaque']==216 AND $_SESSION['type_adv']=="spectre" OR $_POST['attaque']==216 AND $_SESSION['type2_adv']=="spectre" OR $_POST['attaque']==133 AND $_SESSION['type_adv']=="spectre" OR $_POST['attaque']==133 AND $_SESSION['type2_adv']=="spectre"){$effet=0;}
		//abime sur vol
		if($_POST['attaque']==157 AND $_SESSION['type_adv']=="vol" OR $_POST['attaque']==157 AND $_SESSION['type2_adv']=="vol"){$effet=0;}
		
		
		//calcul du multiplicateur
			$reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('type' => $_SESSION['type_attaque'], 'sur' => $_SESSION['type_adv']));  
			$donnees = $reponse->fetch();
			$multiplicateur1=$donnees['effet'];
			if($_SESSION['type2_adv']!="0")
			{$reponse2 = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
			$reponse2->execute(array('type' => $_SESSION['type_attaque'], 'sur' => $_SESSION['type2_adv']));  
			$donnees2 = $reponse2->fetch();
			$multiplicateur2=$donnees2['effet'];}
			else {$multiplicateur2=1;}
			$multiplicateur=$multiplicateur1*$multiplicateur2;
		
		if($multiplicateur==0 AND $_SESSION['cible_attaque']==1 AND $_SESSION['puissance_attaque']>0){$effet=0;}
		
		//verification que touche
		$hasard_precision=rand(1,100);
		$hasard_precision=$hasard_precision + $_SESSION['esq_adv']-$_SESSION['pre'];
                if($echec_blabla_dodo==1){$hasard_precision=1000;}
			//diminution tour
				//confusion
				if($_SESSION['statut_confus']>0){$_SESSION['statut_confus']=$_SESSION['statut_confus']-1;}
				if($_SESSION['statut_confus']==0){$statut_confus_end=1;$_SESSION['statut_confus']=$_SESSION['statut_confus']-1;}
				//rune protect
				if($_SESSION['rune_protect']>0){$_SESSION['rune_protect']=$_SESSION['rune_protect']-1;}
				//danse flamme
				if($_SESSION['danse_flamme']>0){$_SESSION['danse_flamme']=$_SESSION['danse_flamme']-1;}
				//ligotage
				if($_SESSION['ligotage']>0){$_SESSION['ligotage']=$_SESSION['ligotage']-1;}
				//gel
				if($_SESSION['statut_gel']==1){$proba_fin_gel=rand(1,10); if($proba_fin_gel==1){$_SESSION['statut_gel']=0; $degel=1;}}
				//dodo
				if($_SESSION['statut_dodo']==1){$_SESSION['fin_dodo']=$_SESSION['fin_dodo']-1;if($_SESSION['fin_dodo']==0){$_SESSION['statut_dodo']=0; $reveil=1;}}
				//mur lumiere
				if($_SESSION['mur_lumiere']>0){$_SESSION['mur_lumiere']=$_SESSION['mur_lumiere']-1;}
				//protection
				if($_SESSION['protection']>0){$_SESSION['protection']=$_SESSION['protection']-1;}
		$proba_paralyse=rand(1,100);
		$proba_attraction_adv=rand(1,2); 
		$proba_confus=rand(1,100);
		if($_SESSION['recharge']==1)	
			//souffre de recharge (48)
			{
			$recharge=1;
			$_SESSION['recharge']=0; $recharge_faite=1; $hasard_precision=1000;
			$_SESSION['charge']=0;
			}
		elseif($_SESSION['statut_dodo']==1)
			//souffre du sommeil(66/69)
			{
			$statut_dodo=1;$hasard_precision=1000;
			$_SESSION['charge']=0;
			}
		elseif($_SESSION['statut_gel']==1)
			//souffre du gel(47)
			{
			if($_SESSION['id_attaque']!=331)
                            {
                            $statut_gel=1;
                            $hasard_precision=1000;
                            $_SESSION['charge']=0;
                            }else
                            {
                            $_SESSION['statut_gel']=0;
                            $degel=1;
                            }
			}
		elseif($_SESSION['statut_paralyse']==1 AND $proba_paralyse>75)
			//souffre de paralysie (55)
			{
			$statut_paralyse=1;
			$hasard_precision=1000;
			$_SESSION['charge']=0;
			}
		elseif($peur==1)
			//souffre de peur (71)
			{
			$hasard_precision=1000;
			$_SESSION['charge']=0;
			}
		elseif($_SESSION['attraction_adv']==1 AND $proba_attraction_adv==1)
			//souffre de attraction (67)
			{
			$hasard_precision=1000;
			$_SESSION['charge']=0;
			$statut_attraction_adv=1;
			}
		elseif($_SESSION['statut_confus']>0 AND $proba_confus>50)
			//souffre de confusion
			{
			$hasard_precision=1000;
			$_SESSION['charge']=0;
			$statut_confus=1;
			$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/8); if($_SESSION['pv']<0){$_SESSION['pv']=0;}
			}
		else
			//54 charge
			{
			if($_SESSION['id_effet_attaque']==54 AND $effet==1 OR $_SESSION['id_effet2_attaque']==54 AND $effet==1) {if($_SESSION['charge']==0 AND $_SESSION['zenith']==0 OR $_SESSION['zenith']>0 AND $_SESSION['charge']==0 AND $_SESSION['id_attaque']!=40){$_SESSION['charge']=1;$_SESSION['cible_attaque']=0;$charge_faite=1;$hasard_precision=1000;$_SESSION['attaque_auto']=1;} else {$_SESSION['charge']=0;}}
			}
			//87 si dodo
		if($_SESSION['id_effet_attaque']==87 AND $effet==1 OR $_SESSION['id_effet2_attaque']==87 AND $effet==1) {if($_SESSION['statut_dodo_adv']!=1){$hasard_precision=200;$_SESSION['charge']=0;}}
			
			// vol et tunnel au tour 1 non offenssif
		if($_SESSION['esquive_attaque']==1 AND $_SESSION['charge']==0){$_SESSION['cible_attaque']=0;}	
				
			//esquive (abri)
		if($_SESSION['esquive_adv']==1){if($_SESSION['cible_attaque']==1 AND $_SESSION['id_attaque']!=44 OR $_SESSION['cible_attaque']==1 AND $_SESSION['id_attaque_adv']!=45){$hasard_precision=1500;$_SESSION['charge']=0;}$_SESSION['esquive_adv']=0;}	
		if($hasard_precision<=$_SESSION['prec_attaque'])
			{
			//effets avant attaque
			$_SESSION['last_attaque']=$_POST['attaque'];
				//esquive (vol, tunnel)
			if($_SESSION['esquive_attaque']==1){if($_SESSION['charge']==0){$_SESSION['charge']=1;$charge_faite=1;$hasard_precision=1000;$_SESSION['esquive']=1;$_SESSION['attaque_auto']=1;$_SESSION['puissance_attaque']=0;} else {$_SESSION['charge']=0;}}
				//38 echoue
			if($_SESSION['id_effet_attaque']==38 AND $effet==1 OR $_SESSION['id_effet2_attaque']==38 AND $effet==1) {if($degats > 0) {$_SESSION['puissance_attaque']=0;$echec_attaque=1;}}
				//40 fuite
			if($_SESSION['id_effet_attaque']==40 AND $effet==1 OR $_SESSION['id_effet2_attaque']==40 AND $effet==1) {$fuite=1;}
				//46 zénith
			if($_SESSION['id_effet_attaque']==46 AND $effet==1 OR $_SESSION['id_effet2_attaque']==46 AND $effet==1) {$_SESSION['zenith']=5;$_SESSION['grele']=0;$_SESSION['tempete_sable']=0;$_SESSION['danse_pluie']=0;}
			if($_SESSION['zenith']>0) {if($_SESSION['type_attaque']=="feu"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*1.5;} if($_SESSION['type_attaque']=="eau"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*0.5;} }			
				//43 grêle
			if($_SESSION['id_effet_attaque']==43 AND $effet==1 OR $_SESSION['id_effet2_attaque']==43 AND $effet==1) {$_SESSION['grele']=5;$_SESSION['zenith']=0;$_SESSION['tempete_sable']=0;$_SESSION['danse_pluie']=0;}
				//51 danse pluie
			if($_SESSION['id_effet_attaque']==51 AND $effet==1 OR $_SESSION['id_effet2_attaque']==51 AND $effet==1) {$_SESSION['danse_pluie']=5;$_SESSION['grele']=0;$_SESSION['zenith']=0;$_SESSION['tempete_sable']=0;}		
			if($_SESSION['danse_pluie']>0) {if($_SESSION['type_attaque']=="feu"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*0.5;} if($_SESSION['type_attaque']=="eau"){ $_SESSION['puissance_attaque'] = $_SESSION['puissance_attaque']*1.5;} }
				//103 tempete de sable
			if($_SESSION['id_effet_attaque']==103 AND $effet==1 OR $_SESSION['id_effet2_attaque']==103 AND $effet==1) {$_SESSION['tempete_sable']=5;$_SESSION['grele']=0;$_SESSION['zenith']=0;$_SESSION['danse_pluie']=0;}									
				//104 ball'meteo
			if($_SESSION['id_effet_attaque']==104 AND $effet==1 OR $_SESSION['id_effet2_attaque']==104 AND $effet==1) 
				{
				if($_SESSION['danse_pluie']>0){$_SESSION['puissance_attaque']=100;$_SESSION['type_attaque']='eau';}
				elseif($_SESSION['zenith']>0){$_SESSION['puissance_attaque']=100;$_SESSION['type_attaque']='feu';}
				elseif($_SESSION['grele']>0){$_SESSION['puissance_attaque']=100;$_SESSION['type_attaque']='glace';}
				elseif($_SESSION['tempete_sable']>0){$_SESSION['puissance_attaque']=100;$_SESSION['type_attaque']='roche';}
				//calcul du multiplicateur
				$reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('type' => $_SESSION['type_attaque'], 'sur' => $_SESSION['type_adv']));  
				$donnees = $reponse->fetch();
				$multiplicateur1=$donnees['effet'];
				if($_SESSION['type2_adv']!="0")
				{$reponse2 = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE type=:type AND sur=:sur') or die(print_r($bdd->errorInfo()));
				$reponse2->execute(array('type' => $_SESSION['type_attaque'], 'sur' => $_SESSION['type2_adv']));  
				$donnees2 = $reponse2->fetch();
				$multiplicateur2=$donnees2['effet'];}
				else {$multiplicateur2=1;}
				$multiplicateur=$multiplicateur1*$multiplicateur2;
				}
                                //115 force cachée
			if($_SESSION['id_effet_attaque']==115 AND $effet==1 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==115 AND $effet==1 AND $_SESSION['statut_brule_adv']==0 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
			if($_SESSION['id_effet_attaque']==115 AND $effet==1 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==115 AND $effet==1 AND $_SESSION['rune_protect_adv']==0) 
                            {
                            if($_SESSION['danse_pluie']>0){$_SESSION['id_effet_attaque']=2;}
                            elseif($_SESSION['zenith']>0){$_SESSION['id_effet_attaque']=71;}
                            elseif($_SESSION['grele']>0){$_SESSION['id_effet_attaque']=47;}
                            elseif($_SESSION['tempete_sable']>0){$_SESSION['id_effet_attaque']=35;}
                            else{$_SESSION['id_effet_attaque']=55;}
                            }
				//44 rafale
			$rafale=1;
			if($_SESSION['id_effet_attaque']==44 AND $effet==1 OR $_SESSION['id_effet2_attaque']==44 AND $effet==1) {$proba_rafale=rand(1,100); if($proba_rafale<38){$rafale=2;} elseif($proba_rafale<=75){$rafale=3;} elseif($proba_rafale<=87){$rafale=4;}else{$rafale=5;}}
				//45 puissance cachée
			if($_SESSION['id_effet_attaque']==45 AND $effet==1 OR $_SESSION['id_effet2_attaque']==45 AND $effet==1) {$proba_type_attaque=rand(1,17); $reponse = $bdd->prepare('SELECT * FROM pokemons_faiblesses WHERE id=:id') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id' => $proba_type_attaque)); $donnees = $reponse->fetch();$_SESSION['type_attaque'] = $donnees['sur']; $_SESSION['puissance_attaque'] = rand(1,100); }
				//48 recharge
			if($_SESSION['id_effet_attaque']==48 AND $effet==1 OR $_SESSION['id_effet2_attaque']==48 AND $effet==1) {$_SESSION['recharge']=1; }
				//49 mur lumière
			if($_SESSION['id_effet_attaque']==49 AND $effet==1 OR $_SESSION['id_effet2_attaque']==49 AND $effet==1) {$_SESSION['mur_lumiere']=5; }
				//50 abri
			$abri=0;
			if($_SESSION['id_effet_attaque']==50 AND $effet==1 OR $_SESSION['id_effet2_attaque']==50 AND $effet==1) {$abri=1; $_SESSION['esquive']=1;}
				//125 tenacite
			$tenacite=0;
			if($_SESSION['id_effet_attaque']==125 AND $effet==1 OR $_SESSION['id_effet2_attaque']==125 AND $effet==1) {$tenacite=1;}	
                                //53 rune protect
			if($_SESSION['id_effet_attaque']==53 AND $effet==1 OR $_SESSION['id_effet2_attaque']==53 AND $effet==1) {$_SESSION['rune_protect']=6;}
				//57 casse brique
			if($_SESSION['id_effet_attaque']==57 AND $effet==1 OR $_SESSION['id_effet2_attaque']==57 AND $effet==1) {$_SESSION['mur_lumiere_adv']=0;$_SESSION['protection_adv']=0;}
				//58 esq +
			if($_SESSION['id_effet_attaque']==58 AND $effet==1 OR $_SESSION['id_effet2_attaque']==58 AND $effet==1) {$_SESSION['esq']=$_SESSION['esq']+5;if($_SESSION['esq']>30){$_SESSION['esq']=30;}if($_SESSION['esq']<-30){$_SESSION['esq']=-30;}}
				//59 esq ++
			if($_SESSION['id_effet_attaque']==59 AND $effet==1 OR $_SESSION['id_effet2_attaque']==59 AND $effet==1) {$_SESSION['esq']=$_SESSION['esq']+10;if($_SESSION['esq']>30){$_SESSION['esq']=30;}if($_SESSION['esq']<-30){$_SESSION['esq']=-30;}}
				//60 esq +++
			if($_SESSION['id_effet_attaque']==60 AND $effet==1 OR $_SESSION['id_effet2_attaque']==60 AND $effet==1) {$_SESSION['esq']=$_SESSION['esq']+15;if($_SESSION['esq']>30){$_SESSION['esq']=30;}if($_SESSION['esq']<-30){$_SESSION['esq']=-30;}}
				//61 esq -
			if($_SESSION['id_effet_attaque']==61 AND $effet==1 OR $_SESSION['id_effet2_attaque']==61 AND $effet==1) {$_SESSION['esq_adv']=$_SESSION['esq_adv']-5;if($_SESSION['esq_adv']>30){$_SESSION['esq_adv']=30;}if($_SESSION['esq_adv']<-30){$_SESSION['esq_adv']=-30;}}
				//62 esq --
			if($_SESSION['id_effet_attaque']==62 AND $effet==1 OR $_SESSION['id_effet2_attaque']==62 AND $effet==1) {$_SESSION['esq_adv']=$_SESSION['esq_adv']-10;if($_SESSION['esq_adv']>30){$_SESSION['esq_adv']=30;}if($_SESSION['esq_adv']<-30){$_SESSION['esq_adv']=-30;}}
				//63 esq ---
			if($_SESSION['id_effet_attaque']==63 AND $effet==1 OR $_SESSION['id_effet2_attaque']==63 AND $effet==1) {$_SESSION['esq_adv']=$_SESSION['esq_adv']-15;if($_SESSION['esq_adv']>30){$_SESSION['esq_adv']=30;}if($_SESSION['esq_adv']<-30){$_SESSION['esq_adv']=-30;}}
				//64 protection
			if($_SESSION['id_effet_attaque']==64 AND $effet==1 OR $_SESSION['id_effet2_attaque']==64 AND $effet==1) {$_SESSION['protection']=5; }
				//66 repos
			if($_SESSION['id_effet_attaque']==66 AND $_SESSION['pv']==$_SESSION['pv_max'] OR $_SESSION['id_effet2_attaque']==66 AND $_SESSION['pv']==$_SESSION['pv_max']){$effet=0;}
			if($_SESSION['id_effet_attaque']==66 AND $effet==1 OR $_SESSION['id_effet2_attaque']==66 AND $effet==1) {$lance_repos=1;$_SESSION['statut_dodo']=1;$_SESSION['fin_dodo']=3;$_SESSION['statut_poison']=0;$_SESSION['statut_poison_grave']=0;if($_SESSION['statut_paralyse']==1){$_SESSION['vit']=$donnees['vit']*4;}$_SESSION['statut_paralyse']=0;$_SESSION['statut_brule']=0;$_SESSION['pv']=$_SESSION['pv_max'];}
				//67 attraction
			if($_SESSION['id_effet_attaque']==67 AND $effet==1 OR $_SESSION['id_effet2_attaque']==67 AND $effet==1) {if($_SESSION['sexe']=="M" AND $_SESSION['sexe_adv']=="F" OR $_SESSION['sexe']=="F" AND $_SESSION['sexe_adv']=="M"){$_SESSION['attraction']=1 ;}}
				//70 soin
			if($_SESSION['id_effet_attaque']==70 AND $effet==1 OR $_SESSION['id_effet2_attaque']==70 AND $effet==1) {$_SESSION['pv']=$_SESSION['pv']+floor($_SESSION['pv_max']*$soins);if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}}
				//107 aurore
                        if($_SESSION['zenith']>0){$multi_soins_climat=0.66;}
                        elseif($_SESSION['danse_pluie']>0 OR $_SESSION['tempete_sable']>0 OR $_SESSION['grele']>0){$multi_soins_climat=0.25;}
			if($_SESSION['id_effet_attaque']==107 AND $effet==1 OR $_SESSION['id_effet2_attaque']==107 AND $effet==1) {$_SESSION['pv']=$_SESSION['pv']+floor($_SESSION['pv_max']*$multi_soins_climat);if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}}
                               //71 peur
			if($_SESSION['id_effet_attaque']==71 AND $ordre_attaque==2 OR $_SESSION['id_effet2_attaque']==71 AND $ordre_attaque==2) {$effet=0;}
			if($_SESSION['id_effet_attaque']==71 AND $effet==1 OR $_SESSION['id_effet2_attaque']==71 AND $effet==1) {$peur_adv=1;}
				//123 piqué appeurement
                        if($_SESSION['id_effet_attaque']==123 AND $ordre_attaque==2 OR $_SESSION['id_effet2_attaque']==123 AND $ordre_attaque==2) {$effet=0;}
                        if($_SESSION['id_effet_attaque']==123 AND $effet==1 OR $_SESSION['id_effet2_attaque']==123 AND $effet==1) {$rand_pr=rand(1,100); if($rand_pr<=30){$peur_adv=1;}}
				//124 ronflementt
                        if($_SESSION['id_effet_attaque']==124 AND $ordre_attaque==2 OR $_SESSION['id_effet2_attaque']==124 AND $ordre_attaque==2) {$effet=0;}
                        if($_SESSION['id_effet_attaque']==124 AND $effet==1 OR $_SESSION['id_effet2_attaque']==124 AND $effet==1) {if($_SESSION['statut_dodo']>0){$rand_pr=rand(1,100); if($rand_pr<=30){$peur_adv=1;}}}
				//72 danse flamme
			if($_SESSION['id_effet_attaque']==72 AND $effet==1 OR $_SESSION['id_effet2_attaque']==72 AND $effet==1) {$_SESSION['danse_flamme_adv']=rand(3,6);}
				//73 tour rapide
			if($_SESSION['id_effet_attaque']==73 AND $effet==1 OR $_SESSION['id_effet2_attaque']==73 AND $effet==1) {$_SESSION['statut_vampigraine']=0;$_SESSION['danse_flamme']=0;$_SESSION['ligotage']=0;}	
				//74 cc++
			if($_SESSION['id_effet_attaque']==74 AND $effet==1 OR $_SESSION['id_effet2_attaque']==74 AND $effet==1) {$_SESSION['bonus_cc']=10; }	
				//75 croc fatal
			if($_SESSION['id_effet_attaque']==75 AND $effet==1 OR $_SESSION['id_effet2_attaque']==75 AND $effet==1) {$_SESSION['pv_adv']=ceil($_SESSION['pv_adv']/2); }		
				//109 balance
			if($_SESSION['id_effet_attaque']==109 AND $effet==1 OR $_SESSION['id_effet2_attaque']==109 AND $effet==1) {$pv_balance=floor(($_SESSION['pv_adv']+$_SESSION['pv'])/2);$_SESSION['pv_adv']=$pv_balance;if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}$_SESSION['pv']=$pv_balance;if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}}		
				//77 ligotage
			if($_SESSION['id_effet_attaque']==77 AND $effet==1 OR $_SESSION['id_effet2_attaque']==77 AND $effet==1) {$_SESSION['ligotage_adv']=rand(3,6); }	
				//79 esuna
			if($_SESSION['id_effet_attaque']==79 AND $effet==1 OR $_SESSION['id_effet2_attaque']==79 AND $effet==1) { $_SESSION['statut_confus']=-1;$_SESSION['statut_poison']=0;$_SESSION['statut_poison_grave']=0;$_SESSION['statut_gel']=0;if($_SESSION['statut_paralyse']==1){$_SESSION['vit']=$donnees['vit']*4;}$_SESSION['statut_paralyse']=0;$_SESSION['statut_brule']=0;$_SESSION['statut_dodo']=0;}	
				//81 jackpot
			if($_SESSION['id_effet_attaque']==81 AND $effet==1 OR $_SESSION['id_effet2_attaque']==81 AND $effet==1) 
				{ 
				$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 
				$donnees = $reponse->fetch();
				$pokedollar_actuel=$donnees['pokedollar'];
				$gain_jackpot=rand(1,$_SESSION['lvl']); 
				$pokedollar_apres=$pokedollar_actuel+$gain_jackpot;
				$reponse = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('pokedollar' => $pokedollar_apres, 'pseudo' => $_SESSION['pseudo'])); 
				}	
				//84 lance boue
			if($_SESSION['id_effet_attaque']==84 AND $effet==1 OR $_SESSION['id_effet2_attaque']==84 AND $effet==1) {$_SESSION['lance_boue']=1;}	
				//97 tourniquet
			if($_SESSION['id_effet_attaque']==97 AND $effet==1 OR $_SESSION['id_effet2_attaque']==97 AND $effet==1) {$_SESSION['tourniquet']=1;}	
				//89 morphing
			if($_SESSION['id_effet_attaque']==89 AND $effet==1 OR $_SESSION['id_effet2_attaque']==89 AND $effet==1) 	
				{
				$_SESSION['type']=$_SESSION['type_adv'];$_SESSION['type2']=$_SESSION['type2_adv'];
				$_SESSION['attaque1_nb']=$_SESSION['attaque1_nb_adv'];$_SESSION['attaque2_nb']=$_SESSION['attaque2_nb_adv'];$_SESSION['attaque3_nb']=$_SESSION['attaque3_nb_adv'];$_SESSION['attaque4_nb']=$_SESSION['attaque4_nb_adv'];
				$_SESSION['nom_attaque1'] = $_SESSION['nom_attaque1_adv']; $_SESSION['type_attaque1'] = $_SESSION['type_attaque1_adv']; $_SESSION['puissance_attaque1'] = $_SESSION['puissance_attaque1_adv'];
				$_SESSION['prec_attaque1'] = $_SESSION['prec_attaque1_adv']; $_SESSION['cc_attaque1'] = $_SESSION['cc_attaque1_adv']; $_SESSION['classe_attaque1'] = $_SESSION['classe_attaque1_adv'];
				$_SESSION['priorite_attaque1'] = $_SESSION['priorite_attaque1_adv']; $_SESSION['esquive_attaque1'] = $_SESSION['esquive_attaque1_adv']; $_SESSION['cible_attaque1'] = $_SESSION['cible_attaque1_adv'];
				$_SESSION['id_effet_attaque1'] = $_SESSION['id_effet_attaque1_adv']; $_SESSION['id_effet2_attaque1'] = $_SESSION['id_effet2_attaque1_adv']; $_SESSION['proba_attaque1'] = $_SESSION['proba_attaque1_adv'];
				$_SESSION['nom_attaque2'] = $_SESSION['nom_attaque2_adv']; $_SESSION['type_attaque2'] = $_SESSION['type_attaque2_adv']; $_SESSION['puissance_attaque2'] = $_SESSION['puissance_attaque2_adv'];
				$_SESSION['prec_attaque2'] = $_SESSION['prec_attaque2_adv']; $_SESSION['cc_attaque2'] = $_SESSION['cc_attaque2_adv']; $_SESSION['classe_attaque2'] = $_SESSION['classe_attaque2_adv'];
				$_SESSION['priorite_attaque2'] = $_SESSION['priorite_attaque2_adv']; $_SESSION['esquive_attaque2'] = $_SESSION['esquive_attaque2_adv']; $_SESSION['cible_attaque2'] = $_SESSION['cible_attaque2_adv'];
				$_SESSION['id_effet_attaque2'] = $_SESSION['id_effet_attaque2_adv']; $_SESSION['id_effet2_attaque2'] = $_SESSION['id_effet2_attaque2_adv']; $_SESSION['proba_attaque2'] = $_SESSION['proba_attaque2_adv'];
				$_SESSION['nom_attaque3'] = $_SESSION['nom_attaque3_adv']; $_SESSION['type_attaque3'] = $_SESSION['type_attaque3_adv']; $_SESSION['puissance_attaque3'] = $_SESSION['puissance_attaque3_adv'];
				$_SESSION['prec_attaque3'] = $_SESSION['prec_attaque3_adv']; $_SESSION['cc_attaque3'] = $_SESSION['cc_attaque3_adv']; $_SESSION['classe_attaque3'] = $_SESSION['classe_attaque3_adv'];
				$_SESSION['priorite_attaque3'] = $_SESSION['priorite_attaque3_adv']; $_SESSION['esquive_attaque3'] = $_SESSION['esquive_attaque3_adv']; $_SESSION['cible_attaque3'] = $_SESSION['cible_attaque3_adv'];
				$_SESSION['id_effet_attaque3'] = $_SESSION['id_effet_attaque3_adv']; $_SESSION['id_effet2_attaque3'] = $_SESSION['id_effet2_attaque3_adv']; $_SESSION['proba_attaque3'] = $_SESSION['proba_attaque3_adv'];
				$_SESSION['nom_attaque4'] = $_SESSION['nom_attaque4_adv']; $_SESSION['type_attaque4'] = $_SESSION['type_attaque4_adv']; $_SESSION['puissance_attaque4'] = $_SESSION['puissance_attaque4_adv'];
				$_SESSION['prec_attaque4'] = $_SESSION['prec_attaque4_adv']; $_SESSION['cc_attaque4'] = $_SESSION['cc_attaque4_adv']; $_SESSION['classe_attaque4'] = $_SESSION['classe_attaque4_adv'];
				$_SESSION['priorite_attaque4'] = $_SESSION['priorite_attaque4_adv']; $_SESSION['esquive_attaque4'] = $_SESSION['esquive_attaque4_adv']; $_SESSION['cible_attaque4'] = $_SESSION['cible_attaque4_adv'];
				$_SESSION['id_effet_attaque4'] = $_SESSION['id_effet_attaque4_adv']; $_SESSION['id_effet2_attaque4'] = $_SESSION['id_effet2_attaque4_adv']; $_SESSION['proba_attaque4'] = $_SESSION['proba_attaque4_adv'];
				$_SESSION['att']=$_SESSION['att_adv'];
				$_SESSION['def']=$_SESSION['def_adv'];
				$_SESSION['vit']=$_SESSION['vit_adv'];
				$_SESSION['attspe']=$_SESSION['attspe_adv'];
				$_SESSION['defspe']=$_SESSION['defspe_adv'];
				$_SESSION['att_max']=$_SESSION['att_max_adv'];
				$_SESSION['def_max']=$_SESSION['def_max_adv'];
				$_SESSION['vit_max']=$_SESSION['vit_max_adv'];
				$_SESSION['attspe_max']=$_SESSION['attspe_max_adv'];
				$_SESSION['defspe_max']=$_SESSION['defspe_max_adv'];
				}
			//96 mania
			if($_SESSION['id_effet_attaque']==96 AND $effet==1 OR $_SESSION['id_effet2_attaque']==96 AND $effet==1) 
				{
				if($_SESSION['mania']==0){$_SESSION['mania']=rand(1,2);$_SESSION['attaque_auto']=1;$effet=0;}
				elseif($_SESSION['mania']==1){$_SESSION['mania']=0;$_SESSION['statut_confus']=rand(2,5);$statut_confus_begin=1;}
				elseif($_SESSION['mania']>1){$_SESSION['mania']=$_SESSION['mania']-1;$_SESSION['attaque_auto']=1;$effet=0;}
				}
			//100 gribouille
			if($_SESSION['id_effet_attaque']==100 AND $effet==1 OR $_SESSION['id_effet2_attaque']==100 AND $effet==1) 
				{
				if($_SESSION['last_attaque_adv']!=0 AND $_SESSION['last_attaque_adv']!=252) //pas lutte
					{
					$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
					$donnees = $reponse->fetch();
					if($donnees['attaque1']==300)
						{
						$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET attaque1=:attaque1 WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('attaque1' => $_SESSION['last_attaque_adv'], 'pseudo' => $_SESSION['pseudo'])); 
						}
					elseif($donnees['attaque2']==300)
						{
						$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET attaque2=:attaque2 WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('attaque2' => $_SESSION['last_attaque_adv'], 'pseudo' => $_SESSION['pseudo'])); 
						}
					elseif($donnees['attaque3']==300)
						{
						$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET attaque3=:attaque3 WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('attaque3' => $_SESSION['last_attaque_adv'], 'pseudo' => $_SESSION['pseudo']));
						}
					elseif($donnees['attaque4']==300)
						{
						$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET attaque4=:attaque4 WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
						$reponse->execute(array('attaque4' => $_SESSION['last_attaque_adv'], 'pseudo' => $_SESSION['pseudo']));
						}
					else{$echec_attaque=1;}
					$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
					$donnees = $reponse->fetch(); //pokemon actif
					$_SESSION['attaque1_nb']=$donnees['attaque1'];
					$_SESSION['attaque2_nb']=$donnees['attaque2'];
					$_SESSION['attaque3_nb']=$donnees['attaque3'];
					$_SESSION['attaque4_nb']=$donnees['attaque4'];
					$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('id' => $_SESSION['attaque1_nb']));  
					$donnees = $reponse->fetch();
							$_SESSION['nom_attaque1'] = $donnees['nom']; $_SESSION['type_attaque1'] = $donnees['type']; $_SESSION['puissance_attaque1'] = $donnees['puissance'];
							$_SESSION['prec_attaque1'] = $donnees['prec']; $_SESSION['cc_attaque1'] = $donnees['cc']; $_SESSION['classe_attaque1'] = $donnees['classe'];
							$_SESSION['priorite_attaque1'] = $donnees['priorite']; $_SESSION['esquive_attaque1'] = $donnees['esquive']; $_SESSION['cible_attaque1'] = $donnees['cible'];
							$_SESSION['id_effet_attaque1'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque1'] = $donnees['id_effet2']; $_SESSION['proba_attaque1'] = $donnees['proba'];
					$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('id' => $_SESSION['attaque2_nb']));  
					$donnees = $reponse->fetch();
							$_SESSION['nom_attaque2'] = $donnees['nom']; $_SESSION['type_attaque2'] = $donnees['type']; $_SESSION['puissance_attaque2'] = $donnees['puissance'];
							$_SESSION['prec_attaque2'] = $donnees['prec']; $_SESSION['cc_attaque2'] = $donnees['cc']; $_SESSION['classe_attaque2'] = $donnees['classe'];
							$_SESSION['priorite_attaque2'] = $donnees['priorite']; $_SESSION['esquive_attaque2'] = $donnees['esquive']; $_SESSION['cible_attaque2'] = $donnees['cible'];
							$_SESSION['id_effet_attaque2'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque2'] = $donnees['id_effet2']; $_SESSION['proba_attaque2'] = $donnees['proba'];
					$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('id' => $_SESSION['attaque3_nb']));  
					$donnees = $reponse->fetch();
							$_SESSION['nom_attaque3'] = $donnees['nom']; $_SESSION['type_attaque3'] = $donnees['type']; $_SESSION['puissance_attaque3'] = $donnees['puissance'];
							$_SESSION['prec_attaque3'] = $donnees['prec']; $_SESSION['cc_attaque3'] = $donnees['cc']; $_SESSION['classe_attaque3'] = $donnees['classe'];
							$_SESSION['priorite_attaque3'] = $donnees['priorite']; $_SESSION['esquive_attaque3'] = $donnees['esquive']; $_SESSION['cible_attaque3'] = $donnees['cible'];
							$_SESSION['id_effet_attaque3'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque3'] = $donnees['id_effet2']; $_SESSION['proba_attaque3'] = $donnees['proba'];
					$reponse = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponse->execute(array('id' => $_SESSION['attaque4_nb']));  
					$donnees = $reponse->fetch();
							$_SESSION['nom_attaque4'] = $donnees['nom']; $_SESSION['type_attaque4'] = $donnees['type']; $_SESSION['puissance_attaque4'] = $donnees['puissance'];
							$_SESSION['prec_attaque4'] = $donnees['prec']; $_SESSION['cc_attaque4'] = $donnees['cc']; $_SESSION['classe_attaque4'] = $donnees['classe'];
							$_SESSION['priorite_attaque4'] = $donnees['priorite']; $_SESSION['esquive_attaque4'] = $donnees['esquive']; $_SESSION['cible_attaque4'] = $donnees['cible'];
							$_SESSION['id_effet_attaque4'] = $donnees['id_effet']; $_SESSION['id_effet2_attaque4'] = $donnees['id_effet2']; $_SESSION['proba_attaque4'] = $donnees['proba'];
					}
				else{$echec_attaque=1;}
				}	
			//105 prélevement destin	
			if($_SESSION['id_effet_attaque']==105 AND $effet==1 OR $_SESSION['id_effet2_attaque']==105 AND $effet==1) {$_SESSION['destin']=1;}
			//106 ampleur	
                        if($_SESSION['id_effet_attaque']==106 AND $effet==1 OR $_SESSION['id_effet2_attaque']==106 AND $effet==1) {$_SESSION['puissance_attaque']=rand(10,150);if($_SESSION['esquive_attaque_adv']==1 AND $_SESSION['last_attaque_adv']==45){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*2;}}
			//117 contre
                        if($_SESSION['id_effet_attaque']==117 AND $effet==1 OR $_SESSION['id_effet2_attaque']==117 AND $effet==1) {
                            $contre=($_SESSION['pv']/$_SESSION['pv_max'])*100;
                            if($contre<3.12){$_SESSION['puissance_attaque']=200;}
                            elseif($contre<9.37){$_SESSION['puissance_attaque']=150;}
                            elseif($contre<20.31){$_SESSION['puissance_attaque']=100;}
                            elseif($contre<34.37){$_SESSION['puissance_attaque']=80;}
                            elseif($contre<67.19){$_SESSION['puissance_attaque']=40;}
                            else{$_SESSION['puissance_attaque']=20;}
                            }
                        //119 éruption	
                        if($_SESSION['id_effet_attaque']==119 AND $effet==1 OR $_SESSION['id_effet2_attaque']==119 AND $effet==1) {$_SESSION['puissance_attaque']=150*$_SESSION['pv']/$_SESSION['pv_max'];}
			//114 façade
                        if($_SESSION['id_effet_attaque']==114 AND $effet==1 OR $_SESSION['id_effet2_attaque']==114 AND $effet==1) {if($_SESSION['statut_poison']>0 OR $_SESSION['statut_poison_grave']>0 OR $_SESSION['statut_brule']>0 OR $_SESSION['statut_paralyse']>0){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*2;}}
			//112 frustration
                        if($_SESSION['id_effet_attaque']==112 AND $effet==1 OR $_SESSION['id_effet2_attaque']==112 AND $effet==1) {$_SESSION['puissance_attaque']=102-ceil($_SESSION['bonheur']*2/5);if($_SESSION['puissance_attaque']<1){$_SESSION['puissance_attaque']=1;}if($_SESSION['puissance_attaque']>102){$_SESSION['puissance_attaque']=102;}}
			//113 retour
                        if($_SESSION['id_effet_attaque']==113 AND $effet==1 OR $_SESSION['id_effet2_attaque']==113 AND $effet==1) {$_SESSION['puissance_attaque']=ceil($_SESSION['bonheur']*2/5);if($_SESSION['puissance_attaque']<1){$_SESSION['puissance_attaque']=1;}if($_SESSION['puissance_attaque']>102){$_SESSION['puissance_attaque']=102;}}
			
			//effets variation de statut
				//39 confusion
			if($_SESSION['id_effet_attaque']==39 AND $effet==1 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==39 AND $effet==1 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
			if($_SESSION['id_effet_attaque']==39 AND $effet==1 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==39 AND $effet==1 AND $_SESSION['rune_protect_adv']==0) {if($_SESSION['statut_confus_adv']==-1){$_SESSION['statut_confus_adv']=rand(2,5);if($ordre_attaque==2){$statut_confus_begin_adv=1;}}else{$effet=0;}}		
				//41 empoisonnement
			if($_SESSION['id_effet_attaque']==41 AND $effet==1 AND $_SESSION['statut_poison_adv']==0 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==41 AND $effet==1 AND $_SESSION['statut_poison_adv']==0 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
			if($_SESSION['id_effet_attaque']==41 AND $effet==1 AND $_SESSION['statut_poison_adv']!=0 OR $_SESSION['id_effet2_attaque']==41 AND $effet==1 AND $_SESSION['statut_poison_adv']!=0){$effet=0;}
			if($_SESSION['id_effet_attaque']==41 AND $_SESSION['type_adv']=="poison" OR $_SESSION['id_effet_attaque']==41 AND $_SESSION['type2_adv']=="poison" OR $_SESSION['id_effet_attaque']==41 AND $_SESSION['type_adv']=="acier" OR $_SESSION['id_effet_attaque']==41 AND $_SESSION['type2_adv']=="acier"){$effet=0;}
			if($_SESSION['id_effet_attaque']==41 AND $effet==1 AND $_SESSION['statut_poison_adv']==0 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==41 AND $effet==1 AND $_SESSION['statut_poison_adv']==0 AND $_SESSION['rune_protect_adv']==0) {$_SESSION['statut_poison_adv']=1;}
				//42 empoisonnement grave
			if($_SESSION['id_effet_attaque']==42 AND $effet==1 AND $_SESSION['statut_poison_grave_adv']==0 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==42 AND $effet==1 AND $_SESSION['statut_poison_grave_adv']==0 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
			if($_SESSION['id_effet_attaque']==42 AND $_SESSION['type_adv']=="poison" OR $_SESSION['id_effet_attaque']==42 AND $_SESSION['type2_adv']=="poison" OR $_SESSION['id_effet_attaque']==42 AND $_SESSION['type_adv']=="acier" OR $_SESSION['id_effet_attaque']==42 AND $_SESSION['type2_adv']=="acier"){$effet=0;}			
			if($_SESSION['id_effet_attaque']==42 AND $effet==1 AND $_SESSION['statut_poison_grave_adv']==0 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==42 AND $effet==1 AND $_SESSION['statut_poison_grave_adv']==0 AND $_SESSION['rune_protect_adv']==0) {$_SESSION['statut_poison_grave_adv']=1;}
				//47 gel
			if($_SESSION['id_effet_attaque']==47 AND $_SESSION['type_adv']=="glace" OR $_SESSION['id_effet_attaque']==47 AND $_SESSION['type2_adv']=="glace"){$effet=0;}
			if($_SESSION['id_effet_attaque']==47 AND $effet==1 AND $_SESSION['statut_gel_adv']!=1 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==47 AND $effet==1 AND $_SESSION['statut_gel_adv']!=1 AND $_SESSION['rune_protect_adv']!=0) {$effet=0; }
			if($_SESSION['id_effet_attaque']==47 AND $effet==1 AND $_SESSION['statut_gel_adv']!=0 OR $_SESSION['id_effet2_attaque']==47 AND $effet==1 AND $_SESSION['statut_gel_adv']!=0){$effet=0;}				
			if($_SESSION['id_effet_attaque']==47 AND $effet==1 AND $_SESSION['statut_gel_adv']!=1 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==47 AND $effet==1 AND $_SESSION['statut_gel_adv']!=1 AND $_SESSION['rune_protect_adv']==0) {$_SESSION['statut_gel_adv']=1; }
				//55 paralysie
			if($_SESSION['id_effet_attaque']==55 AND $effet==1 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==55 AND $effet==1 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
			if($_SESSION['id_effet_attaque']==55 AND $effet==1 AND $_SESSION['statut_paralyse_adv']!=0 OR $_SESSION['id_effet2_attaque']==55 AND $effet==1 AND $_SESSION['statut_paralyse_adv']!=0){$effet=0;}	
			if($_SESSION['id_effet_attaque']==55 AND $effet==1 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==55 AND $effet==1 AND $_SESSION['rune_protect_adv']==0) {$_SESSION['statut_paralyse_adv']=1; $_SESSION['vit_adv']=$_SESSION['vit_adv']/4;}
				//65 brule
			if($_SESSION['id_effet_attaque']==65 AND $_SESSION['type_adv']=="feu" OR $_SESSION['id_effet_attaque']==65 AND $_SESSION['type2_adv']=="feu"){$effet=0;}
			if($_SESSION['id_effet_attaque']==65 AND $effet==1 AND $_SESSION['statut_brule_adv']==0 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==65 AND $effet==1 AND $_SESSION['statut_brule_adv']==0 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
			if($_SESSION['id_effet_attaque']==65 AND $effet==1 AND $_SESSION['statut_brule_adv']!=0 OR $_SESSION['id_effet2_attaque']==65 AND $effet==1 AND $_SESSION['statut_brule_adv']!=0){$effet=0;}
			if($_SESSION['id_effet_attaque']==65 AND $effet==1 AND $_SESSION['statut_brule_adv']==0 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==65 AND $effet==1 AND $_SESSION['statut_brule_adv']==0 AND $_SESSION['rune_protect_adv']==0) {$_SESSION['statut_brule_adv']=1;$_SESSION['att_adv']=$_SESSION['att_adv']/2;}
				//69 sommeil
			if($_SESSION['id_effet_attaque']==69 AND $effet==1 AND $_SESSION['statut_dodo_adv']!=1 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==69 AND $effet==1 AND $_SESSION['statut_dodo_adv']!=1 AND $_SESSION['rune_protect_adv']!=0) {$effet=0;}
			if($_SESSION['id_effet_attaque']==69 AND $effet==1 AND $_SESSION['statut_dodo_adv']!=0 OR $_SESSION['id_effet2_attaque']==69 AND $effet==1 AND $_SESSION['statut_dodo_adv']!=0){$effet=0;}	
			if($_SESSION['id_effet_attaque']==69 AND $effet==1 AND $_SESSION['statut_dodo_adv']!=1 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==69 AND $effet==1 AND $_SESSION['statut_dodo_adv']!=1 AND $_SESSION['rune_protect_adv']==0) {$_SESSION['statut_dodo_adv']=1;$_SESSION['fin_dodo_adv']=rand(2,8);if($ordre_attaque==2){$dodo_adv_now=1;}}
				//80 triplattaque
			if($_SESSION['id_effet_attaque']==80 AND $effet==1 AND $_SESSION['rune_protect_adv']!=0 OR $_SESSION['id_effet2_attaque']==80 AND $effet==1 AND $_SESSION['statut_brule_adv']==0 AND $_SESSION['rune_protect_adv']!=0) {$effet_adv=0;}
			if($_SESSION['id_effet_attaque']==80 AND $effet==1 AND $_SESSION['rune_protect_adv']==0 OR $_SESSION['id_effet2_attaque']==80 AND $effet==1 AND $_SESSION['rune_protect_adv']==0) {$triplattaque1=rand(1,100);$triplattaque2=rand(1,100);$triplattaque3=rand(1,100); if($triplattaque1<=20){$_SESSION['statut_brule_adv']=1;$triplattaque_brule=1;}elseif($triplattaque2<=20){$_SESSION['statut_paralyse_adv']=1;$_SESSION['vit_adv']=$donnees['vit_adv']/4;$triplattaque_paralyse=1;}elseif($triplattaque3<=20){$_SESSION['statut_gel_adv']=1;$triplattaque_gel=1;}}
				//68 vampigraine
			if($_SESSION['id_effet_attaque']==68 AND $effet==1 AND $_SESSION['statut_vampigraine_adv']!=0 OR $_SESSION['id_effet2_attaque']==68 AND $effet==1 AND $_SESSION['statut_vampigraine_adv']!=0){$effet=0;}
			if($_SESSION['id_effet_attaque']==68 AND $effet==1 AND $_SESSION['statut_vampigraine_adv']==0 OR $_SESSION['id_effet2_attaque']==68 AND $effet==1 AND $_SESSION['statut_vampigraine_adv']==0) {$_SESSION['statut_vampigraine_adv']=1;}
				//1 ko
			if($_SESSION['id_effet_attaque']==1 AND $effet==1 AND $_SESSION['lvl']>=$_SESSION['lvl_adv'] OR $_SESSION['id_effet2_attaque']==1 AND $effet==1 AND $_SESSION['lvl']>=$_SESSION['lvl_adv']) {$_SESSION['pv_adv']=0;}	
			if($_SESSION['id_effet_attaque']==1 AND $effet==1 AND $_SESSION['lvl']<$_SESSION['lvl_adv'] OR $_SESSION['id_effet2_attaque']==1 AND $effet==1 AND $_SESSION['lvl']<=$_SESSION['lvl_adv']) {$effet=0;}
				
			
			if($_SESSION['def_adv']<=0){$_SESSION['def_adv']=1;}if($_SESSION['att_adv']<=0){$_SESSION['att_adv']=1;}if($_SESSION['vit_adv']<=0){$_SESSION['vit_adv']=1;}if($_SESSION['defspe_adv']<=0){$_SESSION['defspe_adv']=1;}if($_SESSION['attspe_adv']<=0){$_SESSION['attspe_adv']=1;}			
			if($_SESSION['def']<=0){$_SESSION['def']=1;}if($_SESSION['att']<=0){$_SESSION['att']=1;}if($_SESSION['vit']<=0){$_SESSION['vit']=1;}if($_SESSION['defspe']<=0){$_SESSION['defspe']=1;}if($_SESSION['attspe']<=0){$_SESSION['attspe']=1;}			
			//calcul des dégats
			$hasard_degats=rand(85,100);
			$hasard_cc=rand(1,100);
			$hasard_cc=$hasard_cc+$_SESSION['bonus_cc'];
			if($_SESSION['cc_attaque']==1){if($hasard_cc<7){$cc=1.5;}else{$cc=1;}}
			if($_SESSION['cc_attaque']==2){if($hasard_cc<15){$cc=1.5;}else{$cc=1;}}
			$degats=0;
			
					//influence des items
				if($_SESSION['objet']==102 AND $_SESSION['type_attaque']=="acier"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//peau métal
				if($_SESSION['objet']==103 AND $_SESSION['type_attaque']=="electrique"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//aimant
				if($_SESSION['objet']==104 AND $_SESSION['type_attaque']=="vol"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//bec pointu
				if($_SESSION['objet']==105 AND $_SESSION['type_attaque']=="combat"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//ceinture noire
				if($_SESSION['objet']==106 AND $_SESSION['type_attaque']=="feu"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//charbon
				if($_SESSION['objet']==107 AND $_SESSION['type_attaque']=="dragon"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//croc dragon
				if($_SESSION['objet']==108 AND $_SESSION['type_attaque']=="psy"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//cuillertordue
				if($_SESSION['objet']==109 AND $_SESSION['type_attaque']=="eau"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//eau mystique
				if($_SESSION['objet']==110 AND $_SESSION['type_attaque']=="glace"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//glaceternelle
				if($_SESSION['objet']==111 AND $_SESSION['type_attaque']=="plante"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//grain miracle
				if($_SESSION['objet']==112 AND $_SESSION['type_attaque']=="normal"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//mouchoir soie
				if($_SESSION['objet']==113 AND $_SESSION['type_attaque']=="poison"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//pic venin
				if($_SESSION['objet']==114 AND $_SESSION['type_attaque']=="insecte"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//poudre argent
				if($_SESSION['objet']==115 AND $_SESSION['type_attaque']=="sol"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//sable doux
				if($_SESSION['objet']==117 AND $_SESSION['type_attaque']=="spectre"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//rune sort
				if($_SESSION['objet']==118 AND $_SESSION['type_attaque']=="roche"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//pierre dure
				if($_SESSION['objet']==119 AND $_SESSION['type_attaque']=="tenebre"){$_SESSION['puissance_attaque']=$_SESSION['puissance_attaque']*1.1;}//lunette noir
			
				
			if($_SESSION['classe_attaque']=="physique"){$degats=$_SESSION['lvl']*0.4;$degats=$degats+2; $degats=$degats*$_SESSION['att']*$_SESSION['puissance_attaque'];$defense=$_SESSION['def_adv']*50;$degats=$degats/$defense;$degats=$degats+2; $degats=$degats*$multiplicateur;$degats=$degats*$cc*$hasard_degats*$rafale/100;if($_SESSION['protection_adv']>0){$degats=$degats/2;}}	
			if($_SESSION['classe_attaque']=="speciale"){$degats=$_SESSION['lvl']*0.4;$degats=$degats+2; $degats=$degats*$_SESSION['attspe']*$_SESSION['puissance_attaque'];$defense=$_SESSION['defspe_adv']*50;$degats=$degats/$defense;$degats=$degats+2; $degats=$degats*$multiplicateur;$degats=$degats*$cc*$hasard_degats*$rafale/100;if($_SESSION['mur_lumiere_adv']>0){$degats=$degats/2;}}	
			if($_SESSION['type_attaque']==$_SESSION['type'] OR $_SESSION['type_attaque']==$_SESSION['type2']){$degats=$degats*1.5;}
			if($_SESSION['puissance_attaque']==0){$degats=0;}
			//82 frappe atlas
			if($_SESSION['id_effet_attaque']==82 AND $effet==1 OR $_SESSION['id_effet2_attaque']==82 AND $effet==1) { if($multiplicateur!=0){$degats=$_SESSION['lvl'];}}	
			//86 sonicboom
			if($_SESSION['id_effet_attaque']==86 AND $effet==1 OR $_SESSION['id_effet2_attaque']==86 AND $effet==1) { if($multiplicateur!=0){$degats=20;}}
			//118 effort
                        if($_SESSION['id_effet_attaque']==118 AND $effet==1 OR $_SESSION['id_effet2_attaque']==118 AND $effet==1) {if($_SESSION['pv_adv']>$_SESSION['pv']){$degats=$_SESSION['pv_adv']-$_SESSION['pv'];}}
			//95 draco-rage
			if($_SESSION['id_effet_attaque']==95 AND $effet==1 OR $_SESSION['id_effet2_attaque']==95 AND $effet==1) { $degats=40;}
			//88 25%deg
			if($_SESSION['id_effet_attaque']==88 AND $effet==1 OR $_SESSION['id_effet2_attaque']==88 AND $effet==1) { if($multiplicateur!=0){$degats=floor($_SESSION['pv_max_adv']/4);}}
			//84 effet lance-boue
			if($_SESSION['type_attaque']=="electrique" AND $_SESSION['lance_boue_adv']==1){$degats=floor($degats/2);}
			//97 effet tourniquet
			if($_SESSION['type_attaque']=="feu" AND $_SESSION['tourniquet_adv']==1){$degats=floor($degats/2);}
			//101 riposte
			if($_SESSION['classe_attaque_adv']=="physique" AND $_SESSION['id_effet_attaque']==101){$degats=$degats_adv*2;$_SESSION['puissance_attaque']=1;}elseif($_SESSION['classe_attaque_adv']!="physique" AND $_SESSION['id_effet_attaque']==101){$echec_attaque=1;}
			//102 voile miroir
			if($_SESSION['classe_attaque_adv']=="speciale" AND $_SESSION['id_effet_attaque']==102){$degats=$degats_adv*2;$_SESSION['puissance_attaque']=1;}elseif($_SESSION['classe_attaque_adv']!="speciale" AND $_SESSION['id_effet_attaque']==102){$echec_attaque=1;}
			// effet abri
			if($abri_adv==1){$degats=0;}
			
			$degats=floor($degats);if($degats<1 AND $degats!=0) {$degats=1;}
			if($degats>$_SESSION['pv_adv']){$degats=$_SESSION['pv_adv'];}
			$_SESSION['pv_adv']=$_SESSION['pv_adv']-$degats; if($_SESSION['pv_adv']<0){$_SESSION['pv_adv']=0;}
			//120 faux-chage
			if($_SESSION['pv_adv']==0 AND $_SESSION['id_effet_attaque']==120){$_SESSION['pv_adv']=1; $degats=$degats-1;}
			//effet tenacite
                        if($_SESSION['pv_adv']<=0 AND $tenacite_adv==1){$_SESSION['pv_adv']=1; $degats=$degats-1;}
			
			//2 att -
				if($_SESSION['id_effet_attaque']==2 AND $effet==1 OR $_SESSION['id_effet2_attaque']==2 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//3 def -
				if($_SESSION['id_effet_attaque']==3 AND $effet==1 OR $_SESSION['id_effet2_attaque']==3 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}
					//4 vit -
				if($_SESSION['id_effet_attaque']==4 AND $effet==1 OR $_SESSION['id_effet2_attaque']==4 AND $effet==1)
					{
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					}
					//5 att spe -
				if($_SESSION['id_effet_attaque']==5 AND $effet==1 OR $_SESSION['id_effet2_attaque']==5 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//6 defspe -
				if($_SESSION['id_effet_attaque']==6 AND $effet==1 OR $_SESSION['id_effet2_attaque']==6 AND $effet==1) 
					{
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
					//7 att --
				if($_SESSION['id_effet_attaque']==7 AND $effet==1 OR $_SESSION['id_effet2_attaque']==7 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//8 def --
				if($_SESSION['id_effet_attaque']==8 AND $effet==1 OR $_SESSION['id_effet2_attaque']==8 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}
					//9 vit --
				if($_SESSION['id_effet_attaque']==9 AND $effet==1 OR $_SESSION['id_effet2_attaque']==9 AND $effet==1) 
					{
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					}
					//10 att spe --
				if($_SESSION['id_effet_attaque']==10 AND $effet==1 OR $_SESSION['id_effet2_attaque']==10 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//11 defspe --
				if($_SESSION['id_effet_attaque']==11 AND $effet==1 OR $_SESSION['id_effet2_attaque']==11 AND $effet==1)
					{
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
					//12 att ---
				if($_SESSION['id_effet_attaque']==12 AND $effet==1 OR $_SESSION['id_effet2_attaque']==12 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
					//13 def ---
				if($_SESSION['id_effet_attaque']==13 AND $effet==1 OR $_SESSION['id_effet2_attaque']==13 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def_adv']=$_SESSION['def_max_adv']*$mul;
					}
					//14 vit ---
				if($_SESSION['id_effet_attaque']==14 AND $effet==1 OR $_SESSION['id_effet2_attaque']==14 AND $effet==1) 
					{
					$ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit_adv']=$_SESSION['vit_max_adv']*$mul;
					}
					//15 att spe ---
				if($_SESSION['id_effet_attaque']==15 AND $effet==1 OR $_SESSION['id_effet2_attaque']==15 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
					//16 defspe ---
				if($_SESSION['id_effet_attaque']==16 AND $effet==1 OR $_SESSION['id_effet2_attaque']==16 AND $effet==1) 
					{
					$ex_mul=$_SESSION['defspe_adv']/$_SESSION['defspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe_adv']=$_SESSION['defspe_max_adv']*$mul;
					}
					//17 att +
				if($_SESSION['id_effet_attaque']==17 AND $effet==1 OR $_SESSION['id_effet2_attaque']==17 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//18 def +
				if($_SESSION['id_effet_attaque']==18 AND $effet==1 OR $_SESSION['id_effet2_attaque']==18 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
					//19 vit +
				if($_SESSION['id_effet_attaque']==19 AND $effet==1 OR $_SESSION['id_effet2_attaque']==19 AND $effet==1) 
					{
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					}
					//20 att spe +
				if($_SESSION['id_effet_attaque']==20 AND $effet==1 OR $_SESSION['id_effet2_attaque']==20 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//21 defspe +
				if($_SESSION['id_effet_attaque']==21 AND $effet==1 OR $_SESSION['id_effet2_attaque']==21 AND $effet==1)
					{
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
				//vent argenté
				if($_SESSION['id_effet_attaque']==93 AND $effet==1 OR $_SESSION['id_effet2_attaque']==93 AND $effet==1)
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
					//22 att ++
				if($_SESSION['id_effet_attaque']==22 AND $effet==1 OR $_SESSION['id_effet2_attaque']==22 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//23 def ++
				if($_SESSION['id_effet_attaque']==23 AND $effet==1 OR $_SESSION['id_effet2_attaque']==23 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
					//24 vit ++
				if($_SESSION['id_effet_attaque']==24 AND $effet==1 OR $_SESSION['id_effet2_attaque']==24 AND $effet==1) 
					{
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					}
					//25 att spe ++
				if($_SESSION['id_effet_attaque']==25 AND $effet==1 OR $_SESSION['id_effet2_attaque']==25 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//83 att spe lanceur --
				if($_SESSION['id_effet_attaque']==83 AND $effet==1 OR $_SESSION['id_effet2_attaque']==83 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//26 defspe ++
				if($_SESSION['id_effet_attaque']==26 AND $effet==1 OR $_SESSION['id_effet2_attaque']==26 AND $effet==1) 
					{
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
					//27 att +++
				if($_SESSION['id_effet_attaque']==27 AND $effet==1 OR $_SESSION['id_effet2_attaque']==27 AND $effet==1)
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
					//28 def +++
				if($_SESSION['id_effet_attaque']==28 AND $effet==1 OR $_SESSION['id_effet2_attaque']==28 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
					//29 vit +++
				if($_SESSION['id_effet_attaque']==29 AND $effet==1 OR $_SESSION['id_effet2_attaque']==29 AND $effet==1) 
					{
					$ex_mul=$_SESSION['vit']/$_SESSION['vit_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['vit']=$_SESSION['vit_max']*$mul;
					}
					//30 att spe +++
				if($_SESSION['id_effet_attaque']==30 AND $effet==1 OR $_SESSION['id_effet2_attaque']==30 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
					//31 defspe +++
				if($_SESSION['id_effet_attaque']==31 AND $effet==1 OR $_SESSION['id_effet2_attaque']==31 AND $effet==1) 
					{
					$ex_mul=$_SESSION['defspe']/$_SESSION['defspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+3;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['defspe']=$_SESSION['defspe_max']*$mul;
					}
                                        //111 boost
				if($_SESSION['id_effet_attaque']==111 AND $effet==1 OR $_SESSION['id_effet2_attaque']==111 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					$_SESSION['att']=$_SESSION['att_max']*$ex_mul;
                                        $ex_mul=$_SESSION['def_adv']/$_SESSION['def_max_adv'];
					$_SESSION['def']=$_SESSION['def_max']*$ex_mul;
                                        $ex_mul=$_SESSION['vit_adv']/$_SESSION['vit_max_adv'];
					$_SESSION['vit']=$_SESSION['vit_max']*$ex_mul;
                                        $ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					$_SESSION['attspe']=$_SESSION['attspe_max']*$ex_mul;
                                        $ex_mul=$_SESSION['def_spe_adv']/$_SESSION['def_spe_max_adv'];
					$_SESSION['def_spe']=$_SESSION['def_spe_max']*$ex_mul;
					}
                                        //116 cognobidon
				if($_SESSION['id_effet_attaque']==116 AND $effet==1 OR $_SESSION['id_effet2_attaque']==116 AND $effet==1) 
					{
					$mul=4;
                                        $_SESSION['pv']=ceil($_SESSION['pv_adv']/2);
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
                                //121 att spe ennemi +
				if($_SESSION['id_effet_attaque']==121 AND $effet==1 OR $_SESSION['id_effet2_attaque']==121 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe_adv']/$_SESSION['attspe_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe_adv']=$_SESSION['attspe_max_adv']*$mul;
					}
                                //126 att ennemi ++
				if($_SESSION['id_effet_attaque']==126 AND $effet==1 OR $_SESSION['id_effet2_attaque']==126 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att_adv']/$_SESSION['att_max_adv'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus+2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att_adv']=$_SESSION['att_max_adv']*$mul;
					}
				//32 pre +
			if($_SESSION['id_effet_attaque']==32 AND $effet==1 OR $_SESSION['id_effet2_attaque']==32 AND $effet==1) {$_SESSION['pre']=$_SESSION['pre']+5;if($_SESSION['pre']>30){$_SESSION['pre']=30;}if($_SESSION['pre']<-30){$_SESSION['pre']=-30;}}
				//33 pre +
			if($_SESSION['id_effet_attaque']==33 AND $effet==1 OR $_SESSION['id_effet2_attaque']==33 AND $effet==1) {$_SESSION['pre']=$_SESSION['pre']+10;if($_SESSION['pre']>30){$_SESSION['pre']=30;}if($_SESSION['pre']<-30){$_SESSION['pre']=-30;}}
				//34 pre +
			if($_SESSION['id_effet_attaque']==34 AND $effet==1 OR $_SESSION['id_effet2_attaque']==34 AND $effet==1) {$_SESSION['pre']=$_SESSION['pre']+15;if($_SESSION['pre']>30){$_SESSION['pre']=30;}if($_SESSION['pre']<-30){$_SESSION['pre']=-30;}}
				//35 pre -
			if($_SESSION['id_effet_attaque']==35 AND $effet==1 OR $_SESSION['id_effet2_attaque']==35 AND $effet==1) {$_SESSION['pre_adv']=$_SESSION['pre_adv']-5;if($_SESSION['pre_adv']>30){$_SESSION['pre_adv']=30;}if($_SESSION['pre_adv']<-30){$_SESSION['pre_adv']=-30;}}
				//36 pre--
			if($_SESSION['id_effet_attaque']==36 AND $effet==1 OR $_SESSION['id_effet2_attaque']==36 AND $effet==1) {$_SESSION['pre_adv']=$_SESSION['pre_adv']-10;if($_SESSION['pre_adv']>30){$_SESSION['pre_adv']=30;}if($_SESSION['pre_adv']<-30){$_SESSION['pre_adv']=-30;}}
				//37 pre---
			if($_SESSION['id_effet_attaque']==37 AND $effet==1 OR $_SESSION['id_effet2_attaque']==37 AND $effet==1) {$_SESSION['pre_adv']=$_SESSION['pre_adv']-15;if($_SESSION['pre_adv']>30){$_SESSION['pre_adv']=30;}if($_SESSION['pre_adv']<-30){$_SESSION['pre_adv']=-30;}}
				//90 self-att -
			if($_SESSION['id_effet_attaque']==90 AND $effet==1 OR $_SESSION['id_effet2_attaque']==90 AND $effet==1) 
					{
					$ex_mul=$_SESSION['att']/$_SESSION['att_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['att']=$_SESSION['att_max']*$mul;
					}
				//91 self-def -
			if($_SESSION['id_effet_attaque']==91 AND $effet==1 OR $_SESSION['id_effet2_attaque']==91 AND $effet==1) 
					{
					$ex_mul=$_SESSION['def']/$_SESSION['def_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-1;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['def']=$_SESSION['def_max']*$mul;
					}
			//94 self-attspe --
				if($_SESSION['id_effet_attaque']==94 AND $effet==1 OR $_SESSION['id_effet2_attaque']==94 AND $effet==1) 
					{
					$ex_mul=$_SESSION['attspe']/$_SESSION['attspe_max'];
					if($ex_mul<0.28){$bonus=-6;}elseif($ex_mul<0.32){$bonus=-5;}elseif($ex_mul<0.39){$bonus=-4;}elseif($ex_mul<0.49){$bonus=-3;}elseif($ex_mul<0.65){$bonus=-2;}elseif($ex_mul<0.99){$bonus=-1;}elseif($ex_mul<1.4){$bonus=0;}elseif($ex_mul<1.9){$bonus=1;}elseif($ex_mul<2.4){$bonus=2;}elseif($ex_mul<2.9){$bonus=3;}elseif($ex_mul<3.4){$bonus=4;}elseif($ex_mul<3.9){$bonus=5;}else{$bonus=6;}
					$bonus=$bonus-2;if($bonus<-6){$bonus=-6;}if($bonus>6){$bonus=6;}
					if($bonus==6){$mul=4;}if($bonus==5){$mul=3.5;}if($bonus==4){$mul=3;}if($bonus==3){$mul=2.5;}if($bonus==2){$mul=2;}if($bonus==1){$mul=1.5;}if($bonus==0){$mul=1;}if($bonus==-1){$mul=0.66;}if($bonus==-2){$mul=0.5;}if($bonus==-3){$mul=0.4;}if($bonus==-4){$mul=0.33;}if($bonus==-5){$mul=0.285;}if($bonus==-6){$mul=0.25;}
					$_SESSION['attspe']=$_SESSION['attspe_max']*$mul;
					}
			}
		else
			{
			if($hasard_precision!=1000){$echec_attaque=1;}
			if($hasard_precision>=1000){$_SESSION['mania']=0;}
			}
		//effet après attaque
		
			//85 self-k.o.
		if($_SESSION['id_effet_attaque']==85 OR $_SESSION['id_effet2_attaque']==85) {if($hasard_precision!=1000){$_SESSION['pv']=0;}}	
			//76 damocles
		if($_SESSION['id_effet_attaque']==76 AND $effet==1 OR $_SESSION['id_effet2_attaque']==76 AND $effet==1) {$damocles=floor($degats/3); $_SESSION['pv']=$_SESSION['pv']-$damocles; if($_SESSION['pv']<0){$_SESSION['pv']=0;}}
			//92 bélier
		if($_SESSION['id_effet_attaque']==92 AND $effet==1 OR $_SESSION['id_effet2_attaque']==92 AND $effet==1) {$damocles=floor($degats/4); $_SESSION['pv']=$_SESSION['pv']-$damocles; if($_SESSION['pv']<0){$_SESSION['pv']=0;}}
			//52 sangsue
		if($_SESSION['id_effet_attaque']==52 AND $effet==1 OR $_SESSION['id_effet2_attaque']==52 AND $effet==1) {$sangsue=floor($degats/2); $_SESSION['pv']=$_SESSION['pv']+$sangsue; if($_SESSION['pv']>$_SESSION['pv_max']){$sangsue=$_SESSION['pv_max']-$_SESSION['pv']+$sangsue;$_SESSION['pv']=$_SESSION['pv_max'];}}				
			//souffre de poison (41)
		if($_SESSION['statut_poison']==1){$statut_poison=1;$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
			//souffre de poison gravement (42)
		if($_SESSION['statut_poison_grave']!=0){$statut_poison_grave=1;$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['statut_poison_grave']*$_SESSION['pv_max']/16);$_SESSION['statut_poison_grave']=$_SESSION['statut_poison_grave']+1;}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
			//souffre de brule (65)
		if($_SESSION['statut_brule']==1){$statut_brule=1;$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
			//souffre de danse flamme(72)
		if($_SESSION['danse_flamme']>0){$danse_flamme=1;$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
			//souffre de ligotage(77)
		if($_SESSION['ligotage']>0){$ligotage=1;$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}
			//souffre de vampigraine(68)
		if($_SESSION['statut_vampigraine']==1){if($_SESSION['pv_adv']!=0){$statut_vampigraine=1;$_SESSION['pv']=$_SESSION['pv']-floor($_SESSION['pv_max']/16);$_SESSION['pv_adv']=$_SESSION['pv_adv']+floor($_SESSION['pv_max']/16);}if($_SESSION['pv']<0){$_SESSION['pv']=0;}if($_SESSION['pv_adv']>$_SESSION['pv_max_adv']){$_SESSION['pv_adv']=$_SESSION['pv_max_adv'];}}
				
		//victoire défaite?
		if($_SESSION['pv_adv']==0){$victoire=1;if($_SESSION['destin_adv']==1){$_SESSION['pv']=0;}}	
		elseif($_SESSION['pv']==0){$defaite=1;}
		
		if($victoire==1){$attaque_donne=1;}
		if($defaite==1){$attaque_donne=1;}
		//ordre de tour
		if($attaque_donne==1){$premier_coup=3;} else {$premier_coup=-1;}
		$attaque_donne=1;
		}
	}
}
?>


<?php
if($_POST['action']=="objet")//utilisation d'objet
{
$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE id_item=:id_item AND pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id_item' => $_POST['id_item'], 'pseudo' => $_SESSION['pseudo']));
$donnees = $reponse->fetch();
$quantite=$donnees['quantite'];
if($quantite>=1)
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $_POST['id_item']));
	$donnees = $reponse->fetch();
	$quantite=$quantite-1;
	$reponse = $bdd->prepare('UPDATE pokemons_inventaire SET quantite=:quantite WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('quantite' => $quantite,'pseudo' => $_SESSION['pseudo'], 'id_item' => $_POST['id_item'])); 
	if($_POST['id_item']==10 OR $_POST['id_item']==11 OR $_POST['id_item']==12 OR $_POST['id_item']==13) //potions
		{
		if($_POST['id_item']==10){$pv_rendu=20;}if($_POST['id_item']==11){$pv_rendu=50;}if($_POST['id_item']==12){$pv_rendu=100;}if($_POST['id_item']==13){$pv_rendu=1000;}
		$_SESSION['pv']=$_SESSION['pv']+$pv_rendu; if($_SESSION['pv']>$_SESSION['pv_max']){$_SESSION['pv']=$_SESSION['pv_max'];}
		$potion_utilisee=1;
		}	
	if($_POST['id_item']==68){$_SESSION['statut_poison']=0;$_SESSION['statut_poison_grave']=0;}
	if($_POST['id_item']==69){$_SESSION['statut_dodo']=0;}
	if($_POST['id_item']==70){$_SESSION['statut_brule']=0;}
	if($_POST['id_item']==71){$_SESSION['statut_gel']=0;}
	if($_POST['id_item']==72){$_SESSION['statut_paralyse']=0;$_SESSION['vit']=$_SESSION['vit']*4;}
	}
}
?>

<?php
if($_SESSION['page_combat']=="NULL"){$combat_possible=0;}
if($combat_possible==1)
{
?>

<br />
<div id="combat">
<?php //affichage de l'adversaire
if($_SESSION['shiney_adv']==1){echo ' <br /> <img src="images/pokemons/shiney/'.$_SESSION['id_pokedex_adv'].'.gif" id="img_combat_gauche" />';}
else {echo ' <br /> <img src="images/pokemons/'.$_SESSION['id_pokedex_adv'].'.gif" id="img_combat_gauche" /><br />';}
?>
<p id="texte_combat_haut">
<span style="font-size:22px;"><?php echo $_SESSION['nom_pokemon_adv']; ?><?php echo'<img src="images/';if($_SESSION['sexe_adv']=="M"){echo 'male';} elseif($_SESSION['sexe_adv']=="F"){echo 'femelle';} else {echo 'nosexe';} echo'.png" style="position:relative;bottom:-1px;left:-2px;">';?></span>
<span style="font-size:12px;">N.<?php echo $_SESSION['lvl_adv']; ?></span><br />

<?php //statut adversaire
if($_SESSION['statut_confus_adv']>=1){ echo'<img src="images/statut/confus_actif.png" id="statut_haut" title="Le pokémon est confus" height=18px />';} else { echo'<img src="images/statut/confus_passif.png" id="statut_haut" title="Le pokémon n\'est pas confus" height=18px/>';}
if($_SESSION['statut_dodo_adv']==1){ echo'<img src="images/statut/dodo_actif.png" id="statut_haut" title="Le pokémon dort" height=18px />';} else { echo'<img src="images/statut/dodo_passif.png" id="statut_haut" title="Le pokémon est éveillé" height=18px/>';}
if($_SESSION['statut_brule_adv']==1){ echo'<img src="images/statut/feu_actif.png" id="statut_haut" title="Le pokémon brûle" height=18px />';} else { echo'<img src="images/statut/feu_passif.png" id="statut_haut" title="Le pokémon ne brûle pas" height=18px/>';}
if($_SESSION['statut_gel_adv']==1){ echo'<img src="images/statut/gel_actif.png" id="statut_haut" title="Le pokémon est gelé" height=18px />';} else { echo'<img src="images/statut/gel_passif.png" id="statut_haut" title="Le pokémon n\'est pas gelé" height=18px/>';}
if($_SESSION['statut_paralyse_adv']==1){ echo'<img src="images/statut/para_actif.png" id="statut_haut" title="Le pokémon est paralysé" height=18px />';} else { echo'<img src="images/statut/para_passif.png" id="statut_haut" title="Le pokémon n\'est pas paralysé" height=18px/>';}
if($_SESSION['statut_poison_adv']==1){ echo'<img src="images/statut/psn_actif.png" id="statut_haut" title="Le pokémon est empoisonné" height=18px />';} else { echo'<img src="images/statut/psn_passif.png" id="statut_haut" title="Le pokémon n\'est pas empoisonné" height=18px/>';}
if($_SESSION['statut_poison_grave_adv']>=1){ echo'<img src="images/statut/psn_grave_actif.png" id="statut_haut" title="Le pokémon est empoisonné gravement" height=18px />';} else { echo'<img src="images/statut/psn_grave_passif.png" id="statut_haut" title="Le pokémon n\'est pas empoisonné gravement" height=18px/>';}
?>
<?php //pv adversaire
if($_SESSION['pv_adv']==0){$pv_img_adv=0;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.05){$pv_img_adv=5;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.1){$pv_img_adv=10;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.15){$pv_img_adv=15;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.2){$pv_img_adv=20;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.25){$pv_img_adv=25;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.3){$pv_img_adv=30;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.35){$pv_img_adv=35;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.4){$pv_img_adv=40;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.45){$pv_img_adv=45;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.5){$pv_img_adv=50;}
elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.55){$pv_img_adv=55;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.6){$pv_img_adv=60;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.65){$pv_img_adv=65;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.7){$pv_img_adv=70;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.75){$pv_img_adv=75;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.8){$pv_img_adv=80;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.85){$pv_img_adv=85;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.9){$pv_img_adv=90;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=0.95){$pv_img_adv=95;}elseif($_SESSION['pv_adv']/$_SESSION['pv_max_adv']<=1){$pv_img_adv=100;}
?>
<img src="images/barre/<?php echo $pv_img_adv; ?>.png" id="pv_haut" title="<?php echo $_SESSION['pv_adv']; ?>/<?php echo $_SESSION['pv_max_adv']; ?>"/>
</p>
<p id="texte_combat_bas">
<span style="font-size:22px;"><?php echo $_SESSION['surnom']; ?><?php echo'<img src="images/';if($_SESSION['sexe']=="M"){echo 'male';} elseif($_SESSION['sexe']=="F"){echo 'femelle';} else {echo 'nosexe';} echo'.png" style="position:relative;bottom:-1px;left:-2px;">';?></span>
<span style="font-size:12px;">N.<?php echo $_SESSION['lvl']; ?></span><br />
<?php //statut
if($_SESSION['statut_confus']>=1){ echo'<img src="images/statut/confus_actif.png" id="statut_bas" title="Le pokémon est confus" height=18px />';} else { echo'<img src="images/statut/confus_passif.png" id="statut_bas" title="Le pokémon n\'est pas confus" height=18px/>';}
if($_SESSION['statut_dodo']==1){ echo'<img src="images/statut/dodo_actif.png" id="statut_bas" title="Le pokémon dort" height=18px />';} else { echo'<img src="images/statut/dodo_passif.png" id="statut_bas" title="Le pokémon est éveillé" height=18px/>';}
if($_SESSION['statut_brule']==1){ echo'<img src="images/statut/feu_actif.png" id="statut_bas" title="Le pokémon brûle" height=18px />';} else { echo'<img src="images/statut/feu_passif.png" id="statut_bas" title="Le pokémon ne brûle pas" height=18px/>';}
if($_SESSION['statut_gel']==1){ echo'<img src="images/statut/gel_actif.png" id="statut_bas" title="Le pokémon est gelé" height=18px />';} else { echo'<img src="images/statut/gel_passif.png" id="statut_bas" title="Le pokémon n\'est pas gelé" height=18px/>';}
if($_SESSION['statut_paralyse']==1){ echo'<img src="images/statut/para_actif.png" id="statut_bas" title="Le pokémon est paralysé" height=18px />';} else { echo'<img src="images/statut/para_passif.png" id="statut_bas" title="Le pokémon n\'est pas paralysé" height=18px/>';}
if($_SESSION['statut_poison']==1){ echo'<img src="images/statut/psn_actif.png" id="statut_bas" title="Le pokémon est empoisonné" height=18px />';} else { echo'<img src="images/statut/psn_passif.png" id="statut_bas" title="Le pokémon n\'est pas empoisonné" height=18px/>';}
if($_SESSION['statut_poison_grave']>=1){ echo'<img src="images/statut/psn_grave_actif.png" id="statut_bas" title="Le pokémon est empoisonné gravement" height=18px />';} else { echo'<img src="images/statut/psn_grave_passif.png" id="statut_bas" title="Le pokémon n\'est pas empoisonné gravement" height=18px/>';}
?>
<?php //pv 
if($_SESSION['pv']==0){$pv_img=0;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.05){$pv_img=5;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.1){$pv_img=10;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.15){$pv_img=15;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.2){$pv_img=20;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.25){$pv_img=25;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.3){$pv_img=30;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.35){$pv_img=35;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.4){$pv_img=40;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.45){$pv_img=45;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.5){$pv_img=50;}
elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.55){$pv_img=55;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.6){$pv_img=60;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.65){$pv_img=65;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.7){$pv_img=70;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.75){$pv_img=75;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.8){$pv_img=80;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.85){$pv_img=85;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.9){$pv_img=90;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=0.95){$pv_img=95;}elseif($_SESSION['pv']/$_SESSION['pv_max']<=1){$pv_img=100;}
?>
<img src="images/barre/<?php echo $pv_img; ?>.png" id="pv_bas" title="<?php echo $_SESSION['pv']; ?>/<?php echo $_SESSION['pv_max']; ?>"/>

</p>
<?php  // affichage du pokémon actif
 if($_SESSION['shiney']==1){echo ' <br /> <img src="images/pokemons/shiney/'.$_SESSION['id_pokedex'].'.gif" id="img_combat_droite"/>';}
else {echo ' <br /> <img src="images/pokemons/'.$_SESSION['id_pokedex'].'.gif" id="img_combat_droite" /><br />';}
?>
</div>



<br />

<?php //le pokémon fait son entrée
if($_POST['entree']=="on")
	{
	echo '<b>'.$_SESSION['adversaire'].' fait appel à '.$_SESSION['nom_pokemon_adv'].'. </b><br />';
	}
?>


<?php

if($_POST['action']=="attaque")
{
echo '<div id="description_combat">';
//dégats avant attaques
	//FIN des effets CLIMAT
if($_SESSION['grele']>0)
	{
	if($_SESSION['type'] !="glace" AND $_SESSION['type']!="glace"){echo $_SESSION['nom_pokemon'].' subit des dégats à cause de la grêle <br />';}
	if($_SESSION['type_adv'] !="glace" AND $_SESSION['type_adv']!="glace"){echo $_SESSION['nom_pokemon_adv'].' subit des dégats à cause de la grêle <br />';}
	$_SESSION['grele']=$_SESSION['grele']-1;
	if($_SESSION['grele']==0){echo 'Il a cessé de grêler <br />';}
	}
if($_SESSION['zenith']>0)
	{
	$_SESSION['zenith']=$_SESSION['zenith']-1;
	if($_SESSION['zenith']==0){echo 'Le soleil cesse de briller <br />';}
	}
if($_SESSION['danse_pluie']>0)
	{
	$_SESSION['danse_pluie']=$_SESSION['danse_pluie']-1;
	if($_SESSION['danse_pluie']==0){echo 'La pluie s\'est arretée <br />';}
	}
if($_SESSION['tempete_sable']>0)
	{
	if($_SESSION['type'] !="sol" AND $_SESSION['type2']!="sol" AND $_SESSION['type'] !="roche" AND $_SESSION['type2']!="roche" AND $_SESSION['type'] !="acier" AND $_SESSION['type2']!="acier"){echo $_SESSION['nom_pokemon'].' subit des dégats à cause de la tempête de sable <br />';}
	if($_SESSION['type_adv'] !="sol" AND $_SESSION['type2_adv']!="sol" AND $_SESSION['type_adv'] !="roche" AND $_SESSION['type2_adv']!="roche" AND $_SESSION['type_adv'] !="acier" AND $_SESSION['type2_adv']!="acier"){echo $_SESSION['nom_pokemon_adv'].' subit des dégats à cause de la tempête de sable <br />';}
	$_SESSION['tempete_sable']=$_SESSION['tempete_sable']-1;
	if($_SESSION['tempete_sable']==0){echo 'La tempête de sable s\'est arretée <br />';}
	}

if($ordre_attaque_adv==0 AND $ordre_attaque==0){$ordre_description=3;}
if($ordre_attaque_adv==1){$ordre_description=-1;}
if($ordre_attaque==1){$ordre_description=1;}
$premiere_description_faite=0;
while($ordre_description<2)
	{
	if($ordre_description==-1)
		{
		//description du combat adv
		if($degel_adv==1){echo $_SESSION['nom_pokemon_adv'].' n\'est plus gelé <br />';}
		if($reveil_adv==1){echo $_SESSION['nom_pokemon_adv'].' est reveillé <br />';}
		if($statut_confus_end_adv==1){echo $_SESSION['nom_pokemon_adv'].' n\'est plus confus <br />'; $statut_confus_end_adv=0;}
		if($_SESSION['statut_confus_adv']>0 AND $statut_confus_begin_adv!=1){echo $_SESSION['nom_pokemon_adv'].' est confus <br />';}
		echo $_SESSION['nom_pokemon_adv'].' tente d\'utiliser l\'attaque <b><span style="font-size:20px;">'.$_SESSION['nom_attaque_adv'].'</span></b><br />';
		if($fuite_adv==1){$echec_attaque_adv=1;}
		if($echec_attaque_adv==1){echo 'l\'attaque a échoué <br />';}
		elseif($recharge_faite_adv==1){echo $_SESSION['nom_pokemon_adv'].' se repose <br />';}
		elseif($statut_dodo_adv==1){echo $_SESSION['nom_pokemon_adv'].' est endormi, il ne peut pas attaquer <br />';}
		elseif($statut_gel_adv==1){echo $_SESSION['nom_pokemon_adv'].' est gelé, il ne peut attaquer <br />';} 
		elseif($statut_paralyse_adv==1){echo $_SESSION['nom_pokemon_adv'].' est paralysé, il ne peut attaquer <br />';}	
		elseif($peur_adv==1 AND $premiere_description_faite==1){echo $_SESSION['nom_pokemon_adv'].' a peur, il n\'attaque pas <br />';}				
		elseif($statut_attraction==1){echo $_SESSION['nom_pokemon_adv'].' refuse d\'attaquer <br />';}
		elseif($statut_confus_adv==1){echo $_SESSION['nom_pokemon_adv'].' est confus, sa folie lui inflige des dégâts <br />';}
		elseif($charge_faite_adv==1){echo $_SESSION['nom_pokemon_adv'].' charge son attaque, dés le prochain tour, il pourra la lancer <br />';}
		else
			{
			if($cc_adv==1.5 AND $degats_adv!=0){echo 'coup critique!<br />';}
			if ($rafale_adv>1){echo 'Touché '.$rafale_adv.' fois <br />';}
			if ($_SESSION['puissance_attaque_adv']!=0){if ($multiplicateur_adv>1) {echo 'C\'est très efficace! <br />';} if($multiplicateur_adv==0 AND $degats_adv==0) {if($degats_adv==0){echo 'Ca n\'a aucun effet <br />';}} elseif($multiplicateur_adv<1){echo 'C\'est peu efficace <br />';}}
			if ($_SESSION['puissance_attaque_adv']!=0){echo 'Il inflige '.$degats_adv.' dégâts à '.$_SESSION['nom_pokemon'].'.<br />';}
			if($sangsue_adv>0){echo 'Il gagne '.$sangsue_adv.' PV <br />';}
			if ($_SESSION['id_effet_attaque_adv']!=0) 
				{
				$reponse = $bdd->prepare('SELECT * FROM pokemon_base_effets WHERE id=:id') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id' => $_SESSION['id_effet_attaque_adv']));  
				$donnees = $reponse->fetch();
				if($_SESSION['id_effet_attaque_adv']==99){if($_SESSION['puissance_attaque_adv']){$donnees['descr_reussi']="";}} //cadeau
				if($effet_adv==1 AND $donnees['descr_reussi']!="") {echo $donnees['descr_reussi']; echo '<br />';} if($effet_adv==0 AND $donnees['descr_echec']!="") {echo $donnees['descr_echec']; echo '<br />';}
				}
			if ($_SESSION['id_effet2_attaque_adv']!=0) 
				{
				$reponse = $bdd->prepare('SELECT * FROM pokemon_base_effets WHERE id=:id') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id' => $_SESSION['id_effet2_attaque_adv']));  
				$donnees = $reponse->fetch();
				if($effet_adv==1 AND $donnees['descr_reussi']!="") {echo $donnees['descr_reussi']; echo '<br />';} if($effet_adv==0 AND $donnees['descr_echec']!="") {echo $donnees['descr_echec']; echo '<br />';}
				}
			}
		if($triplattaque_brule_adv==1){echo $_SESSION['nom_pokemon'].' est brûlé par la triplattaque <br />';}
		if($triplattaque_paralyse_adv==1){echo $_SESSION['nom_pokemon'].' est paralysé par la triplattaque <br />';}
		if($triplattaque_gel_adv==1){echo $_SESSION['nom_pokemon'].' est gelé par la triplattaque <br />';}
		if($statut_poison_adv==1 ){echo $_SESSION['nom_pokemon_adv'].' souffre du poison <br />';} //poison
		if($statut_poison_grave_adv==1 ){echo $_SESSION['nom_pokemon_adv'].' souffre du poison <br />';} 
		if($statut_brule_adv==1 ){echo $_SESSION['nom_pokemon_adv'].' souffre de la brûlure <br />';} 
		if($danse_flamme_adv==1){echo $_SESSION['nom_pokemon_adv'].' souffre de danse flamme <br />';} 
		if($ligotage_adv==1){echo $_SESSION['nom_pokemon_adv'].' souffre de l\'étreinte <br />';} 
		if($statut_vampigraine_adv==1) {echo $_SESSION['nom_pokemon_adv'].' se fait drainer par la vampigraine<br />';} 
		
		if($ordre_attaque==0){$ordre_description=3;}
		if($ordre_attaque==2){$ordre_description=1;}
		if($premiere_description_faite==1){$ordre_description=3;}
		$premiere_description_faite=1;
		}
	if($ordre_description==1)
		{
		echo '<div align="right">';
		//description du combat
		if($degel==1){echo $_SESSION['nom_pokemon'].' n\'est plus gelé <br />';}
		if($reveil==1){echo $_SESSION['nom_pokemon'].' est reveillé <br />';}
		if($statut_confus_end==1){echo $_SESSION['nom_pokemon'].' n\'est plus confus <br />';$statut_confus_end=0;}
		if($_SESSION['statut_confus']>0 AND $statut_confus_begin!=1){echo $_SESSION['nom_pokemon'].' est confus <br />';}
		echo $_SESSION['nom_pokemon'].' tente d\'utiliser l\'attaque <b><span style="font-size:20px;">'.$_SESSION['nom_attaque'].'</span></b>.<br />';
		if($fuite==1){$echec_attaque=1;}
		if($echec_attaque==1){echo 'l\'attaque a échoué. <br />';}
		elseif($recharge_faite==1){echo $_SESSION['nom_pokemon'].' se repose <br />';}
		elseif($statut_dodo==1){echo $_SESSION['nom_pokemon'].' est endormi, il ne peut pas attaquer <br />';}		
		elseif($statut_gel==1){echo $_SESSION['nom_pokemon'].' est gelé, il ne peut attaquer <br />';}	
		elseif($statut_paralyse==1){echo $_SESSION['nom_pokemon'].' est paralysé, il ne peut attaquer <br />';}	
		elseif($peur==1 AND $premiere_description_faite==1){echo $_SESSION['nom_pokemon'].' a peur, il n\'attaque pas <br />';}		
		elseif($statut_attraction_adv==1){echo $_SESSION['nom_pokemon'].' refuse d\'attaquer <br />';}
		elseif($statut_confus==1){echo $_SESSION['nom_pokemon'].' est confus, sa folie lui inflige des dégâts <br />';}
		elseif($charge_faite==1){echo $_SESSION['nom_pokemon'].' charge son attaque, dés le prochain tour, il pourra la lancer <br />';}
		else
			{
			if($cc==1.5 AND $degats!=0){echo 'coup critique!<br />';}
			if ($rafale>1){echo 'Touché '.$rafale.' fois <br />';}
			if ($_SESSION['puissance_attaque']!=0){if ($multiplicateur>1) {echo 'C\'est très efficace! <br />';} if($multiplicateur==0) {if($degats==0){echo 'Ca n\'a aucun effet <br />';}} elseif($multiplicateur<1){echo 'C\'est peu efficace <br />';}}
			if ($_SESSION['puissance_attaque']!=0){echo 'Il inflige '.$degats.' dégâts à '.$_SESSION['nom_pokemon_adv'].'.<br />';}
			if($sangsue>0){echo 'Il gagne '.$sangsue.' PV <br />';}
			if ($_SESSION['id_effet_attaque']!=0) 
				{
				$reponse = $bdd->prepare('SELECT * FROM pokemon_base_effets WHERE id=:id') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id' => $_SESSION['id_effet_attaque']));  
				$donnees = $reponse->fetch();
				if($_SESSION['id_effet_attaque']==99){if($_SESSION['puissance_attaque']){$donnees['descr_reussi']="";}} //cadeau
				if($effet==1 AND $donnees['descr_reussi']!="") {echo $donnees['descr_reussi']; echo '<br />';} if($effet==0 AND $donnees['descr_echec']!="") {echo $donnees['descr_echec']; echo '<br />';}
				}
			if ($_SESSION['id_effet2_attaque']!=0) 
				{
				$reponse = $bdd->prepare('SELECT * FROM pokemon_base_effets WHERE id=:id') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id' => $_SESSION['id_effet2_attaque']));  
				$donnees = $reponse->fetch();
				if($effet==1 AND $donnees['descr_reussi']!="") {echo $donnees['descr_reussi']; echo '<br />';} if($effet==0 AND $donnees['descr_echec']!="") {echo $donnees['descr_echec']; echo '<br />';}
				}
			}
		if($triplattaque_brule==1){echo $_SESSION['nom_pokemon_adv'].' est brûlé par la triplattaque <br />';}
		if($triplattaque_paralyse==1){echo $_SESSION['nom_pokemon_adv'].' est paralysé par la triplattaque <br />';}
		if($triplattaque_gel==1){echo $_SESSION['nom_pokemon_adv'].' est gelé par la triplattaque <br />';}
		if($gain_jackpot>0) {echo 'Vous gagnez '.$gain_jackpot.' pokedollars <br />';}
		if($statut_poison==1){echo $_SESSION['nom_pokemon'].' souffre du poison <br />';} //poison
		if($statut_poison_grave==1){echo $_SESSION['nom_pokemon'].' souffre du poison <br />';}
		if($statut_brule1){echo $_SESSION['nom_pokemon'].' souffre de la brûlure <br />';}
		if($danse_flamme==1){echo $_SESSION['nom_pokemon'].' souffre de danse flamme <br />';} 
		if($ligotage==1){echo $_SESSION['nom_pokemon'].' souffre de l\'étreinte <br />';} 			
		if($statut_vampigraine==1) {echo $_SESSION['nom_pokemon'].' se fait drainer par la vampigraine<br />';} 			
		
		if($ordre_attaque_adv==0){$ordre_description=3;}
		if($ordre_attaque_adv==2){$ordre_description=-1;}
		if($premiere_description_faite==1){$ordre_description=3;}
		$premiere_description_faite=1;
		
		echo '<br /></div>';
		}
	}
echo '</div>';	
}
?>			



<?php
if($_POST['changement_attaque']>0) //changement d'attaque
{
if($_POST['changement_attaque']==1)
	{
	$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET attaque1=:attaque1 WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('attaque1' => $_POST['id_attaque'], 'pseudo' =>$_SESSION['pseudo'] )); 
	}
if($_POST['changement_attaque']==2)
	{
	$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET attaque2=:attaque2 WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('attaque2' => $_POST['id_attaque'], 'pseudo' =>$_SESSION['pseudo'] )); 
	}
if($_POST['changement_attaque']==3)
	{
	$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET attaque3=:attaque3 WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('attaque3' => $_POST['id_attaque'], 'pseudo' =>$_SESSION['pseudo'] )); 
	}
if($_POST['changement_attaque']==4)
	{
	$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET attaque4=:attaque4 WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('attaque4' => $_POST['id_attaque'], 'pseudo' =>$_SESSION['pseudo'] )); 
	}
$attaque_apprise_succes=1;
}

if($victoire==1)//victoire
{
if($_SESSION['xp_recu']==0)
{
$_SESSION['xp_recu']=1;
echo 'Vous avez vaincu le '.$_SESSION['nom_pokemon_adv'].'!<br />';
$nombre_pokemons_dans_combat=1;
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=2 AND participe=1') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo'])); 
while($donnees = $reponse->fetch())
	{
	$nombre_pokemons_dans_combat=$nombre_pokemons_dans_combat+1;
	}
//xp des aidants
$reponse2 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=2 AND participe=1') or die(print_r($bdd->errorInfo()));
$reponse2->execute(array('pseudo' => $_SESSION['pseudo'])); 
while($donnees2 = $reponse2->fetch())
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $donnees2['id']));  
	$donnees = $reponse->fetch();
	$xp_pokemon_actif=$donnees['xp'];
	$lvl_pokemon_actif=$donnees['lvl'];
	$victoires_totales=$donnees['victoires']+1;
	$shiney_inactif=$donnees['shiney'];
	$id_pokemon_aidant=$donnees['id_pokemon'];
	$objet_aidant=$donnees['objet'];
	$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $id_pokemon_aidant));  
	$donnees = $reponse->fetch();
	$nom_pokemon_aidant=$donnees['nom'];
	$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET victoires=:victoires WHERE id=:id') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('victoires'=>$victoires_totales, 'id' => $donnees2['id'])); 
	$gain_xp=$_SESSION['lvl_adv']*30/5; $multiplicateur_xp=$_SESSION['lvl_adv']*$_SESSION['multiplicateur_xp']+10; $gain_xp=$gain_xp*$multiplicateur_xp;$diviseur_xp=$lvl_pokemon_actif+$_SESSION['lvl_adv']+10; $diviseur_xp=$diviseur_xp^2.5; $gain_xp=$gain_xp/$diviseur_xp; $gain_xp=$gain_xp+1;
	if($shiney_inactif==1){$gain_xp=$gain_xp*1.2;}
	$gain_xp=$gain_xp/$nombre_pokemons_dans_combat;$gain_xp=$gain_xp*0.7;
	if($objet_aidant==128){$gain_xp=$gain_xp*1.33;}//multiEXP
	$gain_xp=floor($gain_xp);
	$xp_apres_combat=$xp_pokemon_actif+$gain_xp;
	$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET xp=:xp, participe=0 WHERE id=:id') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('xp' => $xp_apres_combat,'id' => $donnees2['id'])); 
	echo $nom_pokemon_aidant.' gagne '.$gain_xp.' xp <br />';
	}
// xp et lvl up pokémon actif
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
$donnees = $reponse->fetch();
$xp_pokemon_actif=$donnees['xp'];
$lvl_pokemon_actif=$donnees['lvl'];
$victoires_totales=$donnees['victoires']+1;
$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET victoires=:victoires WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('victoires' => $victoires_totales, 'pseudo' => $_SESSION['pseudo'])); 
$gain_xp=$_SESSION['lvl_adv']*30/5; $multiplicateur_xp=$_SESSION['lvl_adv']*$_SESSION['multiplicateur_xp']+10; $gain_xp=$gain_xp*$multiplicateur_xp;$diviseur_xp=$_SESSION['lvl']+$_SESSION['lvl_adv']+10; $diviseur_xp=$diviseur_xp^2.5; $gain_xp=$gain_xp/$diviseur_xp; $gain_xp=$gain_xp+1;
if($_SESSION['shiney']==1){$gain_xp=$gain_xp*1.2;}
$gain_xp=$gain_xp/$nombre_pokemons_dans_combat;$gain_xp=$gain_xp*0.7;
if($_SESSION['objet']==128){$gain_xp=$gain_xp*1.33;}//multiEXP
$gain_xp=floor($gain_xp);
$xp_apres_combat=$xp_pokemon_actif+$gain_xp;
$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET xp=:xp WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('xp' => $xp_apres_combat,'pseudo' => $_SESSION['pseudo'])); 
echo $_SESSION['nom_pokemon'].' gagne '.$gain_xp.' xp <br />';
$_SESSION['nb_pokemons_adv_vaincu']=$_SESSION['nb_pokemons_adv_vaincu']+1;
}
if($_SESSION['nb_pokemons_adv']>$_SESSION['nb_pokemons_adv_vaincu'])
	{
	?>
	<form action="quetes_combat.php" method="post">
	<input type="hidden" name="action" value="pokemon_suivant"/>
	<input type="hidden" name="entree" value="on"/>
	<input type="submit" value="Continuer le combat" />	
	<?php
	}
else
	{
	$_SESSION['page_combat']="NULL";
	echo 'Félicitation! Vous avez vaincu '.$_SESSION['adversaire'].'<br />'.$_SESSION['adversaire'].':<br /><br /> <i>'.nl2br($_SESSION['reussite']).'</i><br /><br />';
	$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
	$donnees = $reponse->fetch();
	$points_quetes=$donnees['points_quetes'];
	if($_SESSION['recompense_percue']==0)
		{
		$_SESSION['recompense_percue']=1;
		if($_SESSION['genre_recompense']=="item")
			{
			$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('pseudo' => $_SESSION['pseudo'], 'id_item' => $_SESSION['recompense']));  
				$donnees = $reponse->fetch();
			if(isset($donnees['id']))
				{
				$quantite_total=$_SESSION['nb_recompense'] + $donnees['quantite'];
				$reponse = $bdd->prepare('UPDATE pokemons_inventaire SET quantite=:quantite WHERE pseudo=:pseudo AND id_item=:id_item') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('quantite' =>$quantite_total ,'pseudo' => $_SESSION['pseudo'], 'id_item' => $_SESSION['recompense'])); 
				}
			else
				{
				$req = $bdd->prepare('INSERT INTO pokemons_inventaire (pseudo, id_item, quantite) VALUES(:pseudo, :id_item, :quantite)') or die(print_r($bdd->errorInfo()));
				$req->execute(array('pseudo' => $_SESSION['pseudo'], 'id_item' => $_SESSION['recompense'],'quantite' => $_SESSION['nb_recompense']))or die(print_r($bdd->errorInfo()));
				}
			$reponse = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id' => $_SESSION['recompense']));  
				$donnees = $reponse->fetch();
			echo 'Vous avez gagné '.$_SESSION['nb_recompense'].' '.$donnees['nom']; if($_SESSION['nb_recompense']>1){echo 's';}echo '. <br />';
			}
		if($_SESSION['genre_recompense']=="argent")
			{		
			$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('pseudo' => $_SESSION['pseudo']));  
			$donnees = $reponse->fetch();
			$argent=$donnees['pokedollar'];
			$argent_now=$argent+$_SESSION['recompense'];
			$reponse = $bdd->prepare('UPDATE pokemons_membres SET pokedollar=:pokedollar  WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('pokedollar' => $argent_now, 'pseudo' => $_SESSION['pseudo']));		
			echo 'Vous avez gagné '.$_SESSION['recompense'].'$.<br />';
			}
		$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
		$donnees = $reponse->fetch();
		$points_quetes=$donnees['points_quetes'];
		
		echo '<a href="carte.php"> Retour à la carte </a>';
		}
	else
		{
		echo 'Il est interdit d\'actualiser la page des récompenses!';
		}	
	if($_SESSION['type_quete']=="principale")
		{
		$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_principales WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' =>$_SESSION['id_liste_quete']));  
		$donnees = $reponse->fetch();
                $numero_quete=$donnees['numero'];
                $etape_quete=$donnees['etape'];
                $next_stap=$etape_quete+1;
                $reponse = $bdd->prepare('SELECT id FROM pokemons_quetes_principales WHERE numero=:numero AND etape=:etape') or die(print_r($bdd->errorInfo()));
                $reponse->execute(array('numero' =>$numero_quete, 'etape'=>$next_stap));  
                $donnees = $reponse->fetch();
                if(isset($donnees['id'])){
                    $reponse2 = $bdd->prepare('UPDATE pokemons_membres SET quete_principale=:quete_principale, quete_principale_etape=:quete_principale_etape, quete_principale_active=0, nb_quete_principale=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
                    $reponse2->execute(array('quete_principale' => $numero_quete, 'quete_principale_etape' => $next_stap,'pseudo' => $_SESSION['pseudo']));                                                 
                }else{
                    $next_numero=$numero_quete+1;
                    $reponse = $bdd->prepare('SELECT id FROM pokemons_quetes_principales WHERE numero=:numero AND etape=1') or die(print_r($bdd->errorInfo()));
                    $reponse->execute(array('numero' =>$next_numero));  
                    $donnees = $reponse->fetch();
                    if(isset($donnees['id'])){
                        $reponse2 = $bdd->prepare('UPDATE pokemons_membres SET quete_principale=:quete_principale, quete_principale_etape=1, quete_principale_active=0, nb_quete_principale=0 WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
                        $reponse2->execute(array('quete_principale' => $next_numero, 'pseudo' => $_SESSION['pseudo']));                                                 
                    }else{
                        echo '<b>La quête suivante n\'existe pas encore. Impossible de valider celle-ci.</b><br /><br />';
                    }
                }
			
		}
	if($_SESSION['type_quete']=="secondaire")
		{
		//actualisation quetes secondaires
		$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_secondaires WHERE id=:id') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id' => $_SESSION['numero_quete_secondaire']));  
				$donnees = $reponse->fetch();
			$difficulte_quete=$donnees['difficulte'];
			$difficulte_quete_now=$difficulte_quete+$points_quetes;
		$reponse = $bdd->prepare('UPDATE pokemons_membres SET points_quetes=:points_quetes WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo())); 
			$reponse->execute(array('points_quetes' => $difficulte_quete_now, 'pseudo' => $_SESSION['pseudo'])); 
		}
	}
}
elseif($defaite==1) //un pokémon KO
	{
	echo '<br />Votre pokémon est K.O.<br /><br />';
	$reponse99 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=2 AND id_combat=:id_combat AND pv!=0 ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
	$reponse99->execute(array('pseudo' => $_SESSION['pseudo'], 'id_combat' => $_SESSION['id_combat']));  
	while($donnees99 = $reponse99->fetch()) {$encore_pokemon=1;}
	$reponse99 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND pa_restant>=1 AND actif=0 ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
	$reponse99->execute(array('pseudo' => $_SESSION['pseudo']));  
	while($donnees99 = $reponse99->fetch()){if($_SESSION['changement']<$_SESSION['nb_pokemons_dispo']){$encore_pokemon=1;}}
	if($encore_pokemon==1)
		{
		?>
		<h3>Que voulez vous faire? </h3>
		<b> Fuir le combat : </b><br />
		<form action="carte.php" method="post">
		<input type="submit" value="Se sauver" />	  
		</form><br />
		<?php 
		if($_SESSION['changement']!=1)
			{	
			?>
			<b> Renvoyer un pokémon : </b><br />
			<form action="quetes_combat.php" method="post">
			<select name="pokemon">
			<?php
			$reponse99 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=2 AND id_combat=:id_combat AND pv!=0 ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
			$reponse99->execute(array('pseudo' => $_SESSION['pseudo'], 'id_combat' => $_SESSION['id_combat']));  
			while($donnees99 = $reponse99->fetch())
			{
			$id_du_pokemon=$donnees99['id'];
			$id_pokemon_du_pokemon=$donnees99['id_pokemon'];
			$lvl_du_pokemon=$donnees99['lvl'];
			$shiney_du_pokemon=$donnees99['shiney'];
			$pv=$donnees99['pv'];
			$pv_max=$donnees99['pv_max'];
			$surnom=$donnees99['surnom'];
			$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $id_pokemon_du_pokemon));  
			$donnees = $reponse->fetch();
			echo '<option value="'.$id_du_pokemon.'" >'.$surnom.' lvl '.$lvl_du_pokemon.' ('.$pv.'/'.$pv_max.'PV)</option>';
			}
			?>
			</select>	
			<input type="hidden" name="again" value="ok"/>
			<input type="hidden" name="action" value="changer_pokemon"/>
			<input type="submit" value="Selectionner" />	  
			</form><br />
			<?php
			}
		$count_pokemon_passif=0;
		$reponse99 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND pa_restant>=1 AND actif=0 ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
			$reponse99->execute(array('pseudo' => $_SESSION['pseudo']));  
			while($donnees99 = $reponse99->fetch()){$count_pokemon_passif=$count_pokemon_passif+1;}
		if($_SESSION['changement']>=$_SESSION['nb_pokemons_dispo'] OR $count_pokemon_passif==0)
			{
			}
		else
			{
			$changement_restant=$_SESSION['nb_pokemons_dispo']-$_SESSION['changement'];
			?>
			<b> Changer de pokémon (<?php echo $changement_restant; ?>): </b><br />
			<form action="quetes_combat.php" method="post">
			<select name="pokemon">
			<?php
                        $reponse99 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND pa_restant>=1 AND actif=0 AND lvl>0 ORDER BY lvl DESC, surnom ASC') or die(print_r($bdd->errorInfo()));
			$reponse99->execute(array('pseudo' => $_SESSION['pseudo']));  
			while($donnees99 = $reponse99->fetch())
			{
			$id_du_pokemon=$donnees99['id'];
			$id_pokemon_du_pokemon=$donnees99['id_pokemon'];
			$lvl_du_pokemon=$donnees99['lvl'];
			$shiney_du_pokemon=$donnees99['shiney'];
			$surnom=$donnees99['surnom'];
			$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $id_pokemon_du_pokemon));  
			$donnees = $reponse->fetch();
			echo '<option value="'.$id_du_pokemon.'" '; if($shiney_du_pokemon==1){echo 'style="background-color:lightblue;"';} echo '>'.$surnom.' lvl '.$lvl_du_pokemon.'</option>';
			}
			?>
			</select>	
			<input type="hidden" name="action" value="changer_pokemon"/>
			<input type="submit" value="Selectionner" />	  
			</form>
			<?php
			}
		}
	else //défaite
		{
		$_SESSION['page_combat']="NULL";
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=2') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('pseudo' => $_SESSION['pseudo'])); 
		while($donnees2 = $reponse2->fetch())
			{
			$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $donnees2['id']));  
			$donnees = $reponse->fetch();
			$defaites_totales=$donnees['defaites']+1;
			$id_pokemon_aidant=$donnees['id_pokemon'];
			$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET defaites=:defaites WHERE id=:id') or die(print_r($bdd->errorInfo()));
					  $reponse->execute(array('defaites' => $defaites_totales,'id' => $donnees2['id'])); 
			}
		$defaites_totales=$_SESSION['defaites']+1;
		$reponse = $bdd->prepare('UPDATE pokemons_liste_pokemons SET defaites=:defaites WHERE pseudo=:pseudo AND actif=1') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('defaites' => $defaites_totales, 'pseudo' => $_SESSION['pseudo'])); 
		$reponse = $bdd->prepare('UPDATE pokemons_map SET combat=0, depuis=0 WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' =>  $_SESSION['id_adv']));
echo 'Vous avez perdu! <br />';
echo '<a href="carte.php">Retour à la carte </a>';
		}
	}
elseif($attaque_apprise_succes==1)
{
echo 'L\'attaque a été apprise avec succès! <br />';
echo '<br /> <a href="carte.php">Retour à la carte </a>';
}
else
{
?>
<?php //si oubli de sélection d'attaque
if($_POST['action']!="combat" AND $_POST['attaque']==0 AND $_POST['action']!="objet" AND $_POST['action']!="changer_pokemon" AND $_POST['action']!="pokemon_suivant")
	{
	echo '<b> Vous devez sélectionner une attaque. </b><br />';
	}
?>	
	
<?php  //description utilisation objet

if($potion_utilisee==1){echo $_SESSION['nom_pokemon'].' récupère des PV. <br />';}
if($_POST['id_item']==68){echo 'Votre pokémon est soigné du poison.';}
if($_POST['id_item']==69){echo 'Votre pokémon est réveillé';}
if($_POST['id_item']==70){echo 'Votre pokémon est soigné de la brûlure';}
if($_POST['id_item']==71){echo 'Votre pokémon est soigné du gel';}
if($_POST['id_item']==72){echo 'Votre pokémon est soigné de la paralysie';}
?>



<?php //comptage pokémon adverse
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete AND num_pokemon=1') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id_quete' => $_SESSION['id_quete_secondaire']));  $donnees = $reponse->fetch(); if(isset($donnees['id'])){$nb_po_adv=1;}
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete AND num_pokemon=2') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id_quete' => $_SESSION['id_quete_secondaire']));  $donnees = $reponse->fetch(); if(isset($donnees['id'])){$nb_po_adv=2;}
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete AND num_pokemon=3') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id_quete' => $_SESSION['id_quete_secondaire']));  $donnees = $reponse->fetch(); if(isset($donnees['id'])){$nb_po_adv=3;}
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete AND num_pokemon=4') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id_quete' => $_SESSION['id_quete_secondaire']));  $donnees = $reponse->fetch(); if(isset($donnees['id'])){$nb_po_adv=4;}
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete AND num_pokemon=5') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id_quete' => $_SESSION['id_quete_secondaire']));  $donnees = $reponse->fetch(); if(isset($donnees['id'])){$nb_po_adv=5;}
$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete AND num_pokemon=6') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id_quete' => $_SESSION['id_quete_secondaire']));  $donnees = $reponse->fetch(); if(isset($donnees['id'])){$nb_po_adv=6;}
$_SESSION['nb_po_adv_restants']=$nb_po_adv-$_SESSION['nb_pokemons_adv_vaincu'];
echo '<br /> Il reste '.$_SESSION['nb_po_adv_restants'].' pokémon(s) apte(s) à combattre à votre adversaire.<br /><br />';
?>
	
<table width="580px" cellpadding="4" cellspacing="0" style="margin-top:-20px;">
<colgroup><COL WIDTH=33%><COL WIDTH=33%><COL WIDTH=34%></colgroup>
<tr>
<td valign="top">
<b> Utiliser une attaque : </b><br />
<form action="quetes_combat.php" method="post">
<?php
if($_SESSION['attaque_auto']==1)
{echo '<input type="radio" name="attaque" value="'.$_SESSION['id_attaque'].'">'.$_SESSION['nom_attaque'].'<br />';}
else
{
?>
<?php if($_SESSION['attaque1_nb']!=0){ ?><input type="radio" name="attaque" value="<?php echo $_SESSION['attaque1_nb']; ?>" <?php if($_POST['attaque']==$_SESSION['attaque1_nb']){echo 'checked';}?>> <?php echo $_SESSION['nom_attaque1'].'<br />'; }?> 
<?php if($_SESSION['attaque2_nb']!=0){ ?><input type="radio" name="attaque" value="<?php echo $_SESSION['attaque2_nb']; ?>" <?php if($_POST['attaque']==$_SESSION['attaque2_nb']){echo 'checked';}?>> <?php echo $_SESSION['nom_attaque2'].'<br />'; }?> 
<?php if($_SESSION['attaque3_nb']!=0){ ?><input type="radio" name="attaque" value="<?php echo $_SESSION['attaque3_nb']; ?>" <?php if($_POST['attaque']==$_SESSION['attaque3_nb']){echo 'checked';}?>> <?php echo $_SESSION['nom_attaque3'].'<br />'; }?>
<?php if($_SESSION['attaque4_nb']!=0){ ?><input type="radio" name="attaque" value="<?php echo $_SESSION['attaque4_nb']; ?>" <?php if($_POST['attaque']==$_SESSION['attaque4_nb']){echo 'checked';}?>> <?php echo $_SESSION['nom_attaque4'].'<br />'; }?> 
<?php } ?>
<input type="hidden" name="action" value="attaque"/>
<input type="submit" value="Utiliser l'attaque" />	  
</form>
</td>

<td valign="top">
<b> Utiliser un objet : </b><br />
<form action="quetes_combat.php" method="post">
<?php
$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_SESSION['pseudo']));
while($donnees = $reponse->fetch())
	{
	$id_item=$donnees['id_item'];
	$quantite=$donnees['quantite'];
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $id_item));
	$donnees2 = $reponse2->fetch();
	$nom_item=$donnees2['nom'];
	if($donnees2['combat']==1 AND $quantite>0 AND $donnees2['bonus_ball']==0)
		{
		echo '<input type="radio" name="id_item" value="'.$id_item.'">'.$nom_item.' ('.$quantite.')<br />';
		}
	}
?>
<input type="hidden" name="action" value="objet"/>
<input type="submit" value="Utiliser l'objet" />	  
</form>
</td>

<td valign="top">
<b> Fuir le combat : </b><br />
<form action="carte.php?action=abandon" method="post">
<input type="submit" value="Se sauver" />	  
</form><br />

<?php 
if($_SESSION['changement']==1 OR $_SESSION['ligotage']!=0)
	{
	}
else
	{
	?>
	<b> Renvoyer un pokémon : </b><br />
	<form action="quetes_combat.php" method="post">
	<select name="pokemon">
	<?php
	$reponse99 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND actif=2 AND id_combat=:id_combat AND pv!=0 ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
	$reponse99->execute(array('pseudo' => $_SESSION['pseudo'], 'id_combat' => $_SESSION['id_combat']));  
	while($donnees99 = $reponse99->fetch())
	{
	$id_du_pokemon=$donnees99['id'];
	$id_pokemon_du_pokemon=$donnees99['id_pokemon'];
	$lvl_du_pokemon=$donnees99['lvl'];
	$shiney_du_pokemon=$donnees99['shiney'];
	$surnom=$donnees99['surnom'];
	$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $id_pokemon_du_pokemon));  
	$donnees = $reponse->fetch();
	echo '<option value="'.$id_du_pokemon.'" '; if($shiney_du_pokemon==1){echo 'style="background-color:lightblue;"';} echo '>'.$surnom.' lvl '.$lvl_du_pokemon.'</option>';
	}
	?>
	</select>	
	<input type="hidden" name="again" value="ok"/>
	<input type="hidden" name="action" value="changer_pokemon"/>
	<input type="submit" value="Selectionner" />	  
	</form><br />
	<?php
	}
?>

<?php 
$count_pokemon_passif=0;
$reponse99 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND pa_restant>=1 AND actif=0 ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
	$reponse99->execute(array('pseudo' => $_SESSION['pseudo']));  
	while($donnees99 = $reponse99->fetch()){$count_pokemon_passif=$count_pokemon_passif+1;}
if($_SESSION['changement']>=$_SESSION['nb_pokemons_dispo'] OR $count_pokemon_passif==0 OR $_SESSION['ligotage']!=0)
	{
	}
else
	{
	$changement_restant=$_SESSION['nb_pokemons_dispo']-$_SESSION['changement'];
	?>
	<b> Changer de pokémon (<?php echo $changement_restant; ?>): </b><br />
	<form action="quetes_combat.php" method="post">
	<select name="pokemon">
	<?php
        $reponse99 = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND pa_restant>=1 AND actif=0 AND lvl>0 ORDER BY lvl DESC, surnom ASC') or die(print_r($bdd->errorInfo()));
	$reponse99->execute(array('pseudo' => $_SESSION['pseudo']));  
	while($donnees99 = $reponse99->fetch())
	{
	$id_du_pokemon=$donnees99['id'];
	$id_pokemon_du_pokemon=$donnees99['id_pokemon'];
	$lvl_du_pokemon=$donnees99['lvl'];
	$shiney_du_pokemon=$donnees99['shiney'];
	$surnom=$donnees99['surnom'];
	$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id' => $id_pokemon_du_pokemon));  
	$donnees = $reponse->fetch();
	echo '<option value="'.$id_du_pokemon.'" '; if($shiney_du_pokemon==1){echo 'style="background-color:lightblue;"';} echo '>'.$surnom.' lvl '.$lvl_du_pokemon.'</option>';
	}
	?>
	</select>	
	<input type="hidden" name="action" value="changer_pokemon"/>
	<input type="submit" value="Selectionner" />	  
	</form>
	<?php
	}
?>
</td>
</tr>
</table>

<?php
}
}
else
{
if($trop_de_pnj_vaincus==1){echo '<b>Vous avez affronté trop de pnj aujourd\'hui. Attendez demain. </b><br /><a href="carte.php">Retour à la carte</a>';}
if($pnj_vaincu_trop_de_fois==1){echo '<b>Le pnj est trop fatigué pour l\'instant. Revenez plus tard. </b><br /><a href="carte.php">Retour à la carte</a>';}
if($_SESSION['page_combat']=="NULL" AND $combat_possible==0){echo 'Vous avez abandonné ce duel. <a href="carte.php">Retour à la carte</a>.';}
}
}
else
{
echo 'Vous devez être connecté pour avoir accès à cette page';
}
?>
<?php include ("bas.php"); ?>
