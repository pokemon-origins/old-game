<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>

 
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
 

<?php
if($_SESSION['is_admin'] == true){
    if($_POST['action']=="modifier"){
        //
        //MODIFICATION
        // 
        if($_POST['action2']=="valider"){
            if($_POST['numero']<=42){
                echo '<span style="color:red;"><b>Impossible de modifier les quêtes G1 et G2 :) </b></span><br /><br />';
            }else{
                if($_POST['genre']=="duel"){$_POST['what']=$_POST['id_quete'];}
                $reponse = $bdd->prepare('UPDATE pokemons_quetes_principales SET 
                    titre=:titre, 
                    qui=:qui,
                    pos_hor=:pos_hor,
                    pos_ver=:pos_ver,
                    genre=:genre,
                    what=:what,
                    condition_reussite=:condition_reussite,
                    pokedex_inf=:pokedex_inf,
                    pokedex_sup=:pokedex_sup,
                    nb_reussite=:nb_reussite,
                    intro=:intro,
                    bouton_parler=:bouton_parler,
                    reussite=:reussite,
                    pas_reussi=:pas_reussi,
                    objectif=:objectif,
                    genre_recompense=:genre_recompense,
                    recompense=:recompense,
                    nb_recompense=:nb_recompense 
                    WHERE id=:id') or die(print_r($bdd->errorInfo()));
                $reponse->execute(array(
                    'titre' => $_POST['titre'], 
                    'qui' => $_POST['qui'],
                    'pos_hor' => $_POST['pos_hor'],
                    'pos_ver' => $_POST['pos_ver'],
                    'genre' => $_POST['genre'],
                    'what' => stripslashes($_POST['what']),
                    'condition_reussite' => $_POST['condition_reussite'],
                    'pokedex_inf' => $_POST['pokedex_inf'],
                    'pokedex_sup' => $_POST['pokedex_sup'],
                    'nb_reussite' => $_POST['nb_reussite'],
                    'intro' => stripslashes($_POST['intro']),
                    'bouton_parler' => stripslashes($_POST['bouton_parler']),
                    'reussite' => stripslashes($_POST['reussite']),
                    'pas_reussi' => stripslashes($_POST['pas_reussi']),
                    'objectif' => stripslashes($_POST['objectif']),
                    'genre_recompense' => $_POST['genre_recompense'],
                    'recompense' => $_POST['recompense'],
                    'nb_recompense' => $_POST['nb_recompense'],
                    'id' => $_POST['id_quete']
                )); 
                echo '<span style="color:red;"><b>Modifications effectuées</b></span><br /><br />';
            }
        }elseif($_POST['what'] == "image"){
            
            if ($_FILES['image']['error'] > 0){ echo '<b>Erreur lors du transfert :</b>'.$_FILES['image']['error'];}
            $size=$_FILES['image']['size']; 
            $type=$_FILES['image']['type'];
            $tmp=$_FILES['image']['tmp_name'];
            $fichier=$_FILES['image']['name'];
            list($width,$height)=getimagesize($tmp);
            $lieu='../images/quetes_principales/'.$_POST['id_quete'].'.jpg';
            if ($_FILES['image']['name'] && $_FILES['image']['name'] != "none"){
                if ($type=="image/jpeg"){
                    move_uploaded_file($tmp,$lieu);
                    echo '<b>Image ajoutée!</b><br />';
                }else{
                    echo '<b>Une erreur s\'est produite</b><br />';
                }
            }else{
                echo '<b>Une erreur s\'est produite.</b><br />';
            }
        }
        $reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_principales WHERE id=:id') or die(print_r($bdd->errorInfo()));
        $reponse->execute(array('id' => $_POST['id_quete'])); 
        $donnees = $reponse->fetch();
        if($donnees['numero']<=42){
            echo '<span style="color:red;"><b>Impossible de modifier les quêtes G1 et G2 :) </b></span>';
        }
        ?>
        <form action="quete_principale.php" method="post">
        <table id="tableau_modifier" width="600px" cellpadding="2" cellspacing="2">
        <colgroup><COL WIDTH=15%><COL WIDTH=85%></COLGROUP>        
        <tr>
            <td>ID quête</td>
            <td><?php echo $donnees['id']; ?> </td>
        </tr><tr>
            <td>Quête N°</td>
            <td><?php echo $donnees['numero'].'-'.$donnees['etape']; ?> </td>
        </tr><tr>
            <td>Titre</td>
            <td><input type="text" name="titre"  value="<?php echo $donnees['titre'];?>"></td>
        </tr><tr>
            <td>Qui</td>
            <td><input type="text" name="qui"  value="<?php echo $donnees['qui'];?>"></td>
        </tr><tr>
            <td>Position</td>
            <td><input type="text" name="pos_ver"  value="<?php echo $donnees['pos_ver'];?>">/<input type="text" name="pos_hor"  value="<?php echo $donnees['pos_hor'];?>"></td>
        </tr><tr>
            <td>Genre</td>
            <td>
                <select name="genre">
                <option value="">à définir</option>
                <option value="attraper" <?php if($donnees['genre']=="attraper"){echo 'selected="selected"';}?>>attraper</option>
                <option value="parler" <?php if($donnees['genre']=="parler"){echo 'selected="selected"';}?>>parler</option>
                <option value="kill" <?php if($donnees['genre']=="kill"){echo 'selected="selected"';}?>>kill</option>
                <option value="duel" <?php if($donnees['genre']=="duel"){echo 'selected="selected"';}?>>duel</option>
                <option value="echange" <?php if($donnees['genre']=="echange"){echo 'selected="selected"';}?>>echange</option>
                <option value="montrer" <?php if($donnees['genre']=="montrer"){echo 'selected="selected"';}?>>montrer</option>
                </select>
            </td>
        </tr><tr>
            <td>What</td>
            <td><input type="text" name="what"  value="<?php echo $donnees['what'];?>"></td>
        </tr><tr>
            <td>Condition réussite</td>
            <td><input type="text" name="condition_reussite" value="<?php echo $donnees['condition_reussite'];?>"></td>
        </tr><tr>
            <td>Limite Pokedex</td>
            <td><input type="text" name="pokedex_inf" value="<?php echo $donnees['pokedex_inf'];?>"> - <input type="text" name="pokedex_sup" value="<?php echo $donnees['pokedex_sup'];?>"></td>
        </tr><tr>
            <td>Nombre de réussite</td>
            <td><input type="text" name="nb_reussite" value="<?php echo $donnees['nb_reussite'];?>"></td>
        </tr><tr>
            <td>Bouton de dialogue</td>
            <td><input type="text" name="bouton_parler" value="<?php echo $donnees['bouton_parler'];?>"></td>
        </tr><tr>
            <td>Objectif</td>
            <td><input type="text" name="objectif" value="<?php echo $donnees['objectif'];?>"></td>
        </tr><tr>
            <td>Genre de récompense</td>
            <td>
                <select name="genre_recompense">
                 <option value="" >aucun</option>
                <option value="argent" <?php if($donnees['genre_recompense']=="argent"){echo 'selected="selected"';}?>>argent</option>
                <option value="item" <?php if($donnees['genre_recompense']=="item"){echo 'selected="selected"';}?>>item</option>
                <option value="pepite" <?php if($donnees['genre_recompense']=="pepite"){echo 'selected="selected"';}?>>pépite(s)</option>
                </select>
            </td>
        </tr><tr>
            <td>Argent / ID objet</td>
            <td><input type="text" name="recompense" value="<?php echo $donnees['recompense'];?>"></td>
        </tr><tr>
            <td>Quantité objet</td>
            <td><input type="text" name="nb_recompense" value="<?php echo $donnees['nb_recompense'];?>"></td>
        </tr><tr>
            <td>Introduction</td>
            <td><textarea name="intro" cols="50" rows="5"><?php echo $donnees['intro']; ?></textarea></td>
        </tr><tr>
            <td>Phrase si réussite</td>
            <td><textarea name="reussite" cols="50" rows="5"><?php echo $donnees['reussite']; ?></textarea></td>
        </tr><tr>
            <td>Phrase si échec</td>
            <td><textarea name="pas_reussi" cols="50" rows="5"><?php echo $donnees['pas_reussi']; ?></textarea></td>
        </tr>      
        </table>
            
        <input type="hidden" name="id_quete"  value="<?php echo $_POST['id_quete']; ?>" />	 
        <input type="hidden" name="action" value="modifier" />
        <input type="hidden" name="action2" value="valider" />
        <input type="hidden" name="numero"  value="<?php echo $donnees['numero'];?>" />
        <input type="submit" value="Valider" />
        </form>
    
        <br /><br />
        <div style="margin-left:40px;">    
        <?php
        $chemin='../images/quetes_principales/'.$donnees['id'].'.jpg'; 
        if(file_exists($chemin)){
            echo 'Image actuelle du personnage : <img src="'.$chemin.'" />';
	}else{
            echo 'Image actuelle du personnage : pas d\'image';
	}
        ?>
        <br />
        <form method="POST" action="quete_principale.php" enctype="multipart/form-data" >         
        <input type="hidden" name="what" value="image"> 
        <input type="hidden" name="action"  value="modifier" />
        <input type="hidden" name="id_quete"  value="<?php echo $donnees['id']; ?>" />
        Modifier : <input name="image" type="file" />
        <br />
        <input type="submit" value="Envoyer l'image"> 
        </form> 
        
        </div> 
        <a href="quete_principale.php" style="color:blue;">Retour</a>
        
        <?php
    }elseif($_POST['action']=="modifier_pokemons"){
        if($_POST['action2']=="valider"){
            if($_POST['numero']<=42){
                echo '<span style="color:red;"><b>Impossible de modifier les quêtes G1 et G2 :) </b></span><br /><br />';
            }else{
                $reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete AND num_pokemon=:num_pokemon') or die(print_r($bdd->errorInfo()));
                $reponse->execute(array('id_quete'=>$_POST['id_quete'], 'num_pokemon'=>$_POST['num_pokemon'])); 
                $donnees = $reponse->fetch();
                if(isset($donnees['id'])){
                    $reponseX = $bdd->prepare('UPDATE pokemons_liste_pokemons_pnj SET id_pokemon=:id_pokemon, shiney=:shiney, sexe=:sexe, lvl=:lvl, pv=:pv, pv_max=:pv_max, att=:att, def=:def, vit=:vit, attspe=:attspe, defspe=:defspe, attaque1=:attaque1, attaque2=:attaque2, attaque3=:attaque3, attaque4=:attaque4 WHERE id_quete=:id_quete AND num_pokemon=:num_pokemon') or die(print_r($bdd->errorInfo()));
                    $reponseX->execute(array('id_pokemon'=>$_POST['id_pokemon'], 'shiney'=>$_POST['shiney'], 'sexe'=>$_POST['sexe'], 'lvl'=>$_POST['lvl'], 'pv'=>$_POST['pv'],'pv_max'=>$_POST['pv_max'],'att'=>$_POST['att'],'def'=>$_POST['def'],'vit'=>$_POST['vit'],'attspe'=>$_POST['attspe'],'defspe'=>$_POST['defspe'],'attaque1'=>$_POST['attaque1'],'attaque2'=>$_POST['attaque2'],'attaque3'=>$_POST['attaque3'],'attaque4'=>$_POST['attaque4'], 'id_quete'=>$_POST['id_quete'], 'num_pokemon'=>$_POST['num_pokemon']));
                    echo '<b>Modification bien effectuées.</b><br /><br />';
                }else{
                    $req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons_pnj (id_quete, num_pokemon, id_pokemon, shiney, sexe, lvl, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4) VALUES(:id_quete, :num_pokemon, :id_pokemon, :shiney, :sexe, :lvl, :pv, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4)') or die(print_r($bdd->errorInfo()));
                    $req->execute(array('id_quete'=>$_POST['id_quete'], 'num_pokemon'=>$_POST['num_pokemon'], 'id_pokemon'=>$_POST['id_pokemon'], 'shiney'=>$_POST['shiney'], 'sexe'=>$_POST['sexe'], 'lvl'=>$_POST['lvl'], 'pv'=>$_POST['pv'],'pv_max'=>$_POST['pv_max'],'att'=>$_POST['att'],'def'=>$_POST['def'],'vit'=>$_POST['vit'],'attspe'=>$_POST['attspe'],'defspe'=>$_POST['defspe'],'attaque1'=>$_POST['attaque1'],'attaque2'=>$_POST['attaque2'],'attaque3'=>$_POST['attaque3'],'attaque4'=>$_POST['attaque4']))or die(print_r($bdd->errorInfo()));
                    echo '<b>Ajout bien effectuées.</b><br /><br />';
                }
                $req = $bdd->prepare('INSERT INTO pokemons_survey_admin (pseudo, action, quand, a_qui) VALUES(:pseudo, "Modifier un pokémon QP", now(),:a_qui)') or die(print_r($bdd->errorInfo()));
                $req->execute(array('pseudo' => $_SESSION['pseudo'], 'a_qui' => stripslashes($_POST['id_quete'])))or die(print_r($bdd->errorInfo()));
            }
        }
        $i=1;
        while($i<=6){
            $reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete AND num_pokemon=:num_pokemon') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('id_quete'=>$_POST['id_quete'], 'num_pokemon'=>$i)); 
            $donnees = $reponse->fetch();
            ?>
            <form method="post" action="quete_principale.php">
            <b>Pokémon N° <?php echo $i;?></b><br />
            Pokémon : <select name="id_pokemon">
            <option value="0">Aucun</option>
            <?php
            $reponse2 = $bdd->query('SELECT * FROM pokemons_base_pokemons ORDER BY id_pokedex') or die(print_r($bdd->errorInfo()));  
            while($donnees2 = $reponse2->fetch()){
                echo '<option value="'.$donnees2['id'].'"'; if($donnees2['id']==$donnees['id_pokemon']){echo 'selected="selected"';} echo '">'.$donnees2['nom'].'</option>';
            }
            ?>
            </select>
            <br />
            Shiney : <select name="shiney">
            <option value="0" <?php if($donnees['shiney']==0){echo 'selected="selected"';}?>>Non</option>
            <option value="1" <?php if($donnees['shiney']==1){echo 'selected="selected"';}?>>Oui</option>	
            </select><br />
            Sexe : <select name="sexe">
            <option value="M" <?php if($donnees['sexe']=="M"){echo 'selected="selected"';}?>>Male</option>
            <option value="F" <?php if($donnees['sexe']=="F"){echo 'selected="selected"';}?>>Femelle</option>	
            <option value="" <?php if($donnees['sexe']==""){echo 'selected="selected"';}?>>Aucun</option>	
            </select><br />
            Lvl : <input type="text" name="lvl" size="2" value="<?php echo $donnees['lvl']; ?>"><br />
            PV : <input type="text" name="pv" size="2" value="<?php echo $donnees['pv']; ?>"><br />
            PV maximum: <input type="text" name="pv_max" size="2" value="<?php echo $donnees['pv_max']; ?>"><br />
            attaque : <input type="text" name="att" size="2" value="<?php echo $donnees['att']; ?>"><br />
            défense : <input type="text" name="def" size="2" value="<?php echo $donnees['def']; ?>"><br />
            vitesse : <input type="text" name="vit" size="2" value="<?php echo $donnees['vit']; ?>"><br />
            attaque spéciale : <input type="text" name="attspe" size="2" value="<?php echo $donnees['attspe']; ?>"><br />
            defense spéciale : <input type="text" name="defspe" size="2" value="<?php echo $donnees['defspe']; ?>"><br />
            Attaque 1 :
            <select name="attaque1">
            <option value="0">Aucune</option>
            <?php
            $reponse3 = $bdd->query('SELECT * FROM pokemon_base_attaques') or die(print_r($bdd->errorInfo())); 
            while($donnees3 = $reponse3->fetch())
                    {
                    echo '<option value="'.$donnees3['id'].'"'; if($donnees3['id']==$donnees['attaque1']){echo 'selected="selected"';} echo '>'.$donnees3['nom'].'</option>';
                    }
            ?>
            </select><br />
            Attaque 2 :
            <select name="attaque2">
            <option value="0">Aucune</option>
            <?php
            $reponse3 = $bdd->query('SELECT * FROM pokemon_base_attaques') or die(print_r($bdd->errorInfo())); 
            while($donnees3 = $reponse3->fetch())
                    {
                    echo '<option value="'.$donnees3['id'].'"'; if($donnees3['id']==$donnees['attaque2']){echo 'selected="selected"';} echo '>'.$donnees3['nom'].'</option>';
                    }
            ?>
            </select><br />
            Attaque 3 :
            <select name="attaque3">
            <option value="0">Aucune</option>
            <?php
            $reponse3 = $bdd->query('SELECT * FROM pokemon_base_attaques') or die(print_r($bdd->errorInfo())); 
            while($donnees3 = $reponse3->fetch())
                    {
                    echo '<option value="'.$donnees3['id'].'"'; if($donnees3['id']==$donnees['attaque3']){echo 'selected="selected"';} echo '>'.$donnees3['nom'].'</option>';
                    }
            ?>
            </select><br />
            Attaque 4 :
            <select name="attaque4">
            <option value="0">Aucune</option>
            <?php
            $reponse3 = $bdd->query('SELECT * FROM pokemon_base_attaques') or die(print_r($bdd->errorInfo())); 
            while($donnees3 = $reponse3->fetch())
                    {
                    echo '<option value="'.$donnees3['id'].'"'; if($donnees3['id']==$donnees['attaque4']){echo 'selected="selected"';} echo '>'.$donnees3['nom'].'</option>';
                    }
            ?>
            </select><br />
            <INPUT TYPE="hidden" name="action" value="modifier_pokemons">
            <INPUT TYPE="hidden" name="action2" value="valider">
            <INPUT TYPE="hidden" name="id_quete" value="<?php echo $_POST['id_quete'];?>">
            <INPUT TYPE="hidden" name="num_pokemon" value="<?php echo $i;?>">
            <input type="hidden" name="numero" value="<?php echo $_POST['numero'];?>" />
            <INPUT TYPE="submit" VALUE="<?php if($donnees['lvl']!=0){echo 'Modifier ce pokémon';}else{echo 'Ajouter ce pokémon';}?>">
            </form>
            <br /><br />
            <?php
            $i=$i+1;
        } 
        ?>
         
        <form method="post" action="quete_principale.php">
        <INPUT TYPE="hidden" name="action" value="modifier">
        <INPUT TYPE="hidden" name="id_quete" value="<?php echo $_POST['id_quete'];?>">
        <INPUT TYPE="submit" VALUE="retour à la quête">
        </form> 
        <br />
        <a href="quete_principale.php" style="color:blue;">Retour au menu des quêtes</a>
        <?php
    }else{  
        //
        // PAGE DE BASE
        //
        if($_POST['action']=="ajouter_quete"){
            $reponse = $bdd->query('SELECT MAX(numero) AS numero FROM pokemons_quetes_principales') or die(print_r($bdd->errorInfo()));
            $donnees = $reponse->fetch();
            $numero=$donnees['numero']+1;
            $req = $bdd->prepare('INSERT INTO pokemons_quetes_principales (numero, etape) VALUES(:numero, 1)') or die(print_r($bdd->errorInfo()));
            $req->execute(array('numero' => $numero))or die(print_r($bdd->errorInfo()));
            echo '<span style="color:red;"><b>Quête ajoutée </b></span><br /><br />';
        }elseif($_POST['action']=="ajouter_phase"){
            $reponse = $bdd->prepare('SELECT MAX(etape) AS etape FROM pokemons_quetes_principales WHERE numero=:numero') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('numero' => $_POST['numero_quete']));  
            $donnees = $reponse->fetch();
            $etape=$donnees['etape']+1;
            $req = $bdd->prepare('INSERT INTO pokemons_quetes_principales (numero, etape) VALUES(:numero, :etape)') or die(print_r($bdd->errorInfo()));
            $req->execute(array('numero' => $_POST['numero_quete'], 'etape' => $etape))or die(print_r($bdd->errorInfo()));
            echo '<span style="color:red;"><b>Phase ajoutée </b></span><br /><br />';
        }elseif($_POST['action']=="supprimer_phase"){
            $reponse = $bdd->prepare('SELECT MAX(etape) AS etape FROM pokemons_quetes_principales WHERE numero=:numero') or die(print_r($bdd->errorInfo()));
            $reponse->execute(array('numero' => $_POST['numero_quete']));  
            $donnees = $reponse->fetch();
            $etape=$donnees['etape'];
            $req = $bdd->prepare('DELETE FROM pokemons_quetes_principales WHERE numero=:numero AND etape=:etape') or die(print_r($bdd->errorInfo()));
            $req->execute(array('numero' => $_POST['numero_quete'], 'etape' => $etape)) or die(print_r($bdd->errorInfo()));	
            echo '<span style="color:red;"><b>Phase supprimée</b></span><br /><br />';
        }
        ?>
        <h1>Gestion quête principale</h1>

        <?php
        $numero_quete=0;
        $reponse = $bdd->query('SELECT * FROM pokemons_quetes_principales WHERE numero!="0" ORDER BY numero ASC, etape ASC') or die(print_r($bdd->errorInfo()));
        while($donnees = $reponse->fetch()){
            //N° de quête
            if($donnees['numero']!=$numero_quete){
                echo '<span style=margin-left:-10px;"><b>quête n°'.$donnees['numero'].'</b></span>'; 
                $numero_quete=$donnees['numero'];
                echo '<br />';
            }

            //titre
            echo '<span onclick="voir_quete('.$donnees['id'].')" style="color:blue;">';   
            echo $donnees['etape'].' - '.$donnees['titre'];
            echo '</span>';

            //description
            echo '<div id="id_quete_'.$donnees['id'].'" style="display:none;margin-left:30px;">';
            ?>
            Lieu : <?php echo $donnees['pos_ver'];?>/<?php echo $donnees['pos_hor'];?> (<?php echo $donnees['qui'];?>) <br />
            Récompense : 
                <?php
                if($donnees['genre_recompense']=="argent"){
                    echo $donnees['recompense']." pokedollars";             
                }elseif($donnees['genre_recompense']=="item"){
                    $reponse2 = $bdd->prepare('SELECT nom FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
                    $reponse2->execute(array('id' => $donnees['recompense']));  
                    $base_items = $reponse2->fetch(); 
                    echo $donnees['nb_recompense'].' x '.$base_items['nom'];
                }elseif($donnees['genre_recompense']=="pepite"){                
                    echo $donnees['nb_recompense'].' pépite(s)';
                }else{
                    echo "aucune";
                }
                ?>
                <br />
            Objectif : <?php echo $donnees['objectif'];?> <br />

            Genre : <?php echo $donnees['genre'];?> <br /> 
            <?php
            //description lié au genre
            if($donnees['genre']=="attraper"){
                  echo 'Cible : '.$donnees['nb_reussite'].' x '; 
                  if($donnees['what']==""){echo "n'importe";}else{echo $donnees['what'];}echo '<br />';
            }elseif($donnees['genre']=="parler"){
                  echo 'Texte : '.$donnees['bouton_parler'].' x '; 
                  echo '<br />';
            }elseif($donnees['genre']=="kill"){
                  echo 'Cible : '.$donnees['nb_reussite'].' x '; 
                  if($donnees['what']==""){echo "n'importe";}else{echo $donnees['what'];}echo '<br />';
            }elseif($donnees['genre']=="duel"){
                  echo 'Nb pokémons utilisables : '.$donnees['condition_reussite']; 
                  ?>
                  <form action="quete_principale.php" method="post">                     	         
                  <input type="hidden" name="id_quete"  value="<?php echo $donnees['id']; ?>" />	 
                  <input type="hidden" name="action"  value="modifier_pokemons" />  
                  <input type="hidden" name="numero"  value="<?php echo $donnees['numero'];?>" />
                  <input type="submit" value="Modifier les pokémons" />           
                  </form>
                  <?php
                  echo '<br />';
            }elseif($donnees['genre']=="echange"){
                  echo 'Cible : '; 
                  if($donnees['what']==""){echo "n'importe";}else{echo $donnees['what'];}echo '<br />';
            }elseif($donnees['genre']=="montrer"){
                  echo 'Pokedex : '.$donnees['pokedex_inf'].' - '.$donnees['pokedex_sup'];
            }

            //description TEXTE
            echo '<br />';
            echo '<span onclick="voir_texte('.$donnees['id'].')" style="color:green;">';
            echo 'Voir la description';
            echo '</span>';
            echo '<div id="texte_id_quete_'.$donnees['id'].'" style="display:none;">';
            echo '<b>Intro :</b><br />';
            echo $donnees['intro'];
            echo '<br /><br />';
            echo '<b>Réussite :</b><br />';
            echo $donnees['reussite'];
            echo '<br /><br />';
            echo '<b>Pas réussi :</b><br />';
            echo $donnees['pas_reussi'];
            echo '<br /><br />';
            echo '</div>';
            ?>

            <form action="quete_principale.php" method="post">                     	         
            <input type="hidden" name="id_quete"  value="<?php echo $donnees['id']; ?>" />	 
            <input type="hidden" name="action"  value="modifier" /> 
            <input type="submit" value="Modifier la quête" />           
            </form>    

            <?php
            echo '</div><br />';
        }
        ?>

        <br /><br />
        <form action="quete_principale.php" method="post">                     	         	 
        <input type="hidden" name="action"  value="ajouter_quete" /> 
        <input type="submit" value="Ajouter une quête" />           
        </form>     
        
        <?php
        $reponse = $bdd->query('SELECT MAX(numero) AS numero FROM pokemons_quetes_principales') or die(print_r($bdd->errorInfo()));
        $donnees = $reponse->fetch();
        $numero=$donnees['numero']-1;
        ?>
        <br />
        <form action="quete_principale.php" method="post">                     	         	 
        <input type="hidden" name="action"  value="ajouter_phase" /> 
        <input type="hidden" name="numero_quete"  value="<?php echo $numero;?>" /> 
        <input type="submit" style="color:green;" value="Ajouter une phase à la quête <?php echo $numero;?>" />           
        </form>
        
        <?php
        $reponse = $bdd->prepare('SELECT MAX(etape) AS etape FROM pokemons_quetes_principales WHERE numero=:numero') or die(print_r($bdd->errorInfo()));
        $reponse->execute(array('numero' =>$numero));  
        $donnees = $reponse->fetch();
        ?>
        <br />
        <form action="quete_principale.php" method="post">                     	         	 
        <input type="hidden" name="action"  value="supprimer_phase" /> 
        <input type="hidden" name="numero_quete"  value="<?php echo $numero;?>" /> 
        <input type="submit" style="color:red;" value="Supprimer la phase <?php echo $donnees['etape'];?> de la quête <?php echo $numero;?>" />           
        </form>

        <p>
        Attention : Après la dernière quête, il doit toujours y avoir une quête vide ! <br />
        Une fois terminée, le joueur passe à la quête suivante dans la base de données. S'il a réussi la dernière quête et qu'il n'y a plus de quête ensuite, il risque de retomber à la quête 0...<br />
        <i>Exemple : à la fin du scénario G2, on est à 42 quêtes. Pourtant, l'étape 1 de la quête 43 existe, vide ! Comme ça, la base de données du joueur passe à 43-1 après le 42-5.</i> 
        </p>

    <?php 
    }
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>	
   
<!-- JAVASCRIPT -->
<script type="text/javascript" language="javascript">
//<![CDATA[
function voir_quete(item)
{
if(document.getElementById("id_quete_"+item).style.display == "none"){document.getElementById("id_quete_"+item).style.display = "block";}
else if(document.getElementById("id_quete_"+item).style.display == "block"){document.getElementById("id_quete_"+item).style.display = "none";}
}
function voir_texte(item)
{
if(document.getElementById("texte_id_quete_"+item).style.display == "none"){document.getElementById("texte_id_quete_"+item).style.display = "block";}
else if(document.getElementById("texte_id_quete_"+item).style.display == "block"){document.getElementById("texte_id_quete_"+item).style.display = "none";}
}
//]]>
</script>
<?php include ("bas.php"); ?>
