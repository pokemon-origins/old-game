<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>

<?php
if($_SESSION['is_big_boss'] == true)
{
?>

<div id="text_contenu" style="margin-top:0px;">
<div id="text">

<h2 align="center">e-mails invalides</h2>

<?php
if($_POST['action']=="tri"){

    $texte = $_POST['message'];
    // l'expression pour reperer une adresse email
    $email_regex = "/^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}/";
    // Passe tout le texte dans un tableau
    $regs = preg_split("/[\s,:]+/", $texte);

    $i=0; // boucle principale
    $j=0; // boucle quand une adresse mail est trouvé
    $nb = sizeof($regs);
    foreach($regs as $key) {

         if ($i < $nb) {
          // on regarde si chaque element du tableau contient ou poas une adresse email
          preg_match($email_regex, $key, $matches, PREG_OFFSET_CAPTURE);
          // Si oui on la conserve dans le tableau $tab
          if (!empty($matches[0]))
            { $tab[$j] = $matches[0];  $j++;};
              $i++;
         }
             else
              break;
    }
    // Affiche le contenu du tableau $tab qui contient le resultat final
    //echo '<pre>';
    //print_r($tab);
    //echo '</pre>';
    
    $mails_invalides=0;
    $i=0;
    $nb_mails=count($tab);
    while($i<$nb_mails){
        echo $i.'. ';
        echo '<b>'.$tab[$i][0].'</b>';
        echo ' - ';
        $reponse = $bdd->prepare('SELECT id, pseudo, mail_encore_valide FROM pokemons_membres WHERE mail=:mail') or die(print_r($bdd->errorInfo()));
        $reponse->execute(array('mail' => $tab[$i][0]));
        $donnees = $reponse->fetch();
        if(isset($donnees['id'])){
            echo 'mail de '.$donnees['pseudo'].' - ';
            if($donnees['mail_encore_valide']==0){
                $reponse = $bdd->prepare('UPDATE pokemons_membres SET mail_encore_valide=1 WHERE id=:id') or die(print_r($bdd->errorInfo()));
                $reponse->execute(array('id' => $donnees['id'])); 
                echo '<span style="color:red;">mail passé invalide</span>';  
                $mails_invalides++;
            }else{
                echo 'Ce mail était déjà invalide';
            }
        }else{
            echo 'pas de propriétaire';
        }
        echo '<br />';
        $i++;
    }
    echo '<br />';
    echo '<b>'.$mails_invalides.' mails devenus invalides </b>';
}else{
?>


<form method="post" action="tri_emails.php">
<textarea name="message" cols="70" rows="8"></textarea><br />
<input type="hidden" name="action" value="tri">
<input type="submit" value="Trier" />
</form>

<?php
}
}
?>

<?php include ("bas.php"); ?>