<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>

<?php
if($_SESSION['is_admin'] == true)
{
?>

<div id="text_contenu" style="margin-top:0px;">
<div id="text"> 

<?php
if($_POST['action']=="modifier")
	{
	 $reponse = $bdd->prepare('UPDATE pokemons_carte_tableau SET lieu=:lieu  WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('lieu' => $_POST['lieu'], 'id' => $_POST['id']));
		echo '<h2> Modification bien effectuée </h2>';
	}
?>
<?php
if($_POST['action']=="modifier_picture")
	{
	 $reponse = $bdd->prepare('UPDATE pokemons_carte_tableau SET id_picture=:id_picture  WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id_picture' => $_POST['id_picture'], 'id' => $_POST['id']));
		echo '<h2> Modification bien effectuée </h2>';
	}
?>
<?php
if($_POST['action']=="permission")
	{
	if($_POST['permission']==1)
		{
		$reponse = $bdd->prepare('UPDATE pokemons_carte_tableau SET permission=:permission WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('permission' => $_POST['verrou'], 'id' => $_POST['id']));
		}
	if($_POST['permission']==0)
		{
		$reponse = $bdd->prepare('UPDATE pokemons_carte_tableau SET permission=0 WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_POST['id']));
		}
	echo '<b>Modification effectuée </b><br />';
	}
?>
<?php
if($_GET['action']=="modify")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_carte_tableau WHERE hor=:hor AND ver=:ver') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('hor' => $_GET['horizontal'], 'ver' => $_GET['vertical']));
	$donnees = $reponse->fetch();
	$permission=$donnees['permission'];
?>

	<form action="admin_pokemon_carte.php" method="post">
    <p>
	 h<input type="text" name="h" value="<?php echo $_GET['horizontal']; ?>"/> <br /><br />
	 v<input type="text" name="v" value="<?php echo $_GET['vertical']; ?>"/> <br /><br />
	 lieu  <input type="text" name="lieu" value="<? echo $donnees['lieu']; ?>" /> <br /> <br /> 
	 <input type="hidden" name="action" value="modifier" />
    <input type="hidden" name="id" value="<?php echo $donnees['id']; ?>" />		
    <input type="submit" value="Valider" />	   
	</form><br /><br />
	Image actuelle :
	<img src="../images/carte/<?php echo $donnees['id_picture'];?>.jpg" alt="aucune" /> <br />
	Modifier l'image? <br />
	<form action="admin_pokemon_carte.php" method="post">
	<?php
	for ($picture=1; $picture <= 100; $picture++)
		{
		$lien ="../images/carte/".$picture.".jpg";
		if(file_exists($lien))
			{
			echo '<input type="radio" name="id_picture" value="'.$picture.'">
		  	<img src="../images/carte/'.$picture.'.jpg" />    ';
			}
		}
	echo '<br />';
	for ($picture=1; $picture <= 10; $picture++)
		{
		$lien ="../images/carte/azuria_".$picture.".jpg";
		if(file_exists($lien))
			{
			echo '<input type="radio" name="id_picture" value="azuria_'.$picture.'">
		  	<abbr title="'.$picture.'"><img src="../images/carte/azuria_'.$picture.'.jpg" /></abbr>    ';
			}
		}
	?>
	<input type="hidden" name="action" value="modifier_picture" />	
    <input type="hidden" name="id" value="<?php echo $donnees['id']; ?>" />	<br />		
    <input type="submit" value="Valider" />	 </form><br /> 
	

	<?php 
	if($permission==0)
		{
		echo 'Cette map n\'est pas verouillée';
	?>
		<form action="admin_pokemon_carte.php" method="post">
		<input type="hidden" name="permission" value="1" />
		<input type="hidden" name="action" value="permission" />
		<input type="text" name="verrou" value="1000" />
		<input type="hidden" name="id" value="<?php echo $donnees['id']; ?>" />		
		<input type="submit" value="Verouiller" />	   
		</form><br /><br />	
	<?php
		}
	else
		{
		echo 'Cette map est verouillée';
	?>
		<form action="admin_pokemon_carte.php" method="post">
		<input type="hidden" name="permission" value="0" />
		<input type="hidden" name="action" value="permission" />
		<input type="hidden" name="id" value="<?php echo $donnees['id']; ?>" />		
		<input type="submit" value="Deverouiller" />	   
		</form><br /><br />	
	<?php
		}
	}
	?>

<div class="carte_complete">
<table class="table_carte_complete">
<?php

// collonnes et lignes max//

$colonnes_max= 150;
$lignes_max=150;
  
?>     
   <tr>
       <td class="nb_colonnes"></td>
	   <?php for ($nombre_de_colonnes = -162; $nombre_de_colonnes <=$colonnes_max ; $nombre_de_colonnes++)
	   {echo '<td class="nb_colonnes"><span style="font-size:12px;">'.$nombre_de_colonnes.'</span></td>';
	   }?>   
  </tr>
 

<?php

//boucle2 = lignes
for ($lignes=18; $lignes <= $lignes_max; $lignes++)
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_carte_tableau WHERE hor=:hor ORDER BY ver') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('hor' => $lignes)); 	
	?> 
    <tr>
       <td class="nb_colonnes"><span style="font-size:12px;"><?php echo $lignes; ?></span></td>   
	   <?php 
	   $colonnes = -162;
       while ($donnees = $reponse->fetch()) //boucle 1 = colonnes
           {
		   if($donnees['ver']>=$colonnes)
			   {
			   if($donnees['permission']==0){echo '<td title="('.$colonnes.', '.$lignes.')" style="background-image:url(../images/carte/'.$donnees['id_picture'].'.jpg);background-size:cover;" ';}
			   elseif($donnees['permission']==1000){echo '<td style="background-color:black;" ';}
			   else{echo '<td style="background-color:red;text-align:center;" ';}
			   echo 'onclick="document.location.href=\'admin_pokemon_carte.php?action=modify&amp;horizontal='.$lignes.'&amp;vertical='.$colonnes.'\';"';      //modifier
			   echo 'class="carte_td" >';                                                      //afficher la couleur
			   $colonnes = $colonnes + 1;
			   if($donnees['permission']<1000 AND $donnees['permission']>0){echo $donnees['permission'];}
			   echo ' </td>'; 
			   }
			}
			echo '</tr>';
			
    }
    ?>
</table> 
</div>



<?php
}
?>
 <?php include("bas.php"); ?>
