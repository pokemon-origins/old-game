<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
<?php
if($_SESSION['is_admin'] == true)
{
?>
 
 
<h2> Modification de la position d'un joueur </h2>
<p>Ce menu vous permet de modifier la position d'un joueur sur la carte<br/>
Cet outil est à utiliser avec précaution et à tester sur vous même avant de le faire sur un autre joueur.<br/>
<br/>
</p>

<?php
	if($_POST['action']=="update") //modifier la position d'un joueur
	{
    // Update position
    $update_pos = "UPDATE pokemons_membres SET pos_hor=:pos_hor, pos_ver=:pos_ver WHERE pseudo=:pseudo;";
    $req = $bdd->prepare($update_pos) or die(print_r($bdd->errorInfo()));
    $req->execute(array('pos_hor' => $_POST['hor'], 'pos_ver' => $_POST['vert'], 'pseudo' => $_POST['qui']));
    echo "<b>La position du joueur a été modifiée.</b><br/>";
	}
?>

<br/><br/>
<b>Modifier la position d'un joueur :</b><br/>

<form action="admin_update_player_position.php" method="post">                     	         
	<input name="action" value="update" type="hidden"> 	
	Joueur:
	<select name="qui">
		<?php
		echo '<option value="'.$_SESSION["pseudo"].'">'.$_SESSION["pseudo"].'</option>';
		$reponse = $bdd->query('SELECT * FROM pokemons_membres ORDER BY pseudo') or die(print_r($bdd->errorInfo()));
		while($all_members = $reponse->fetch())
		{
			echo '<option value="'.$all_members['pseudo'].'">'.$all_members['pseudo'].'</option>';
		}
		?>
	</select><br /><br/>
	Vertical : <input type="text" name="vert" value=""><br/>
	 Horizontal : <input type="text" name="hor" value=""><br/>
	<input value="Modifier la position" type="submit">           
</form>
	
<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>	
   
<?php include ("bas.php"); ?>
