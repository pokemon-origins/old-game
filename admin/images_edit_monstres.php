<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text"> 


<?php
if($_SESSION['is_image_uploader'] == true)
{
?>

<h2> Modifier l'image d'un monstre </h2>

<p>
Ce formulaire vous permet d'editer les images des monstres.<br/>
Merci de ne mettre que des images au format <b>PNG</b> et de taille <b>196x195</b> (ou très proche de cette taille).<br/>
Note importante: Il est fort probable que la modification de l'image ne soit pas visible instantanément.<br/>
Cela est dû à votre navigateur qui affiche les images du site en "cache". Pour vérifier que l'image a bien été modifiée,<br/>
il faut faire clic droit sur l'image puis "afficher dans un nouvel onglet" puis rafraichir la page (avec F5 par exemple).<br/>
</p>

<?php
if($_POST['action']=="ajouter_image")
	{
	if($_POST['action2']=="go_menu")
		 {
		 ?>
		 <h2> Ajouter une image </h2>
		 <FORM ENCTYPE="multipart/form-data" ACTION="images_edit_monstres.php" METHOD="POST"> 
		<INPUT TYPE="hidden" name="action" value="ajouter_image">
		<INPUT TYPE="hidden" name="action2" value="ajouter">
		<INPUT TYPE="hidden" name="id" value="<?php echo $_POST['id'];?>">
		(uniquement .png de taille 196x195)) : <br /><INPUT NAME="fichier" TYPE="file"> 
		<INPUT TYPE="submit" VALUE="Modifier l'image"> 
		</FORM>
		<?php
		 }
	elseif($_POST['action2']=="ajouter")
		{
		$size=$_FILES['fichier']['size']; 
		$type=$_FILES['fichier']['type'];
		$tmp=$_FILES['fichier']['tmp_name'];
		$fichier=$_FILES['fichier']['name'];
		list($width,$height)=getimagesize($tmp);
		$lieu='../images/monstres/'.$_POST['id'].'.png';	
		move_uploaded_file($tmp,$lieu);	
		echo '<b>Votre image a bien été transférée!</b><br /><br /><a href="images_edit_monstres.php">Retour à la plateforme</a>';
		}
	}
else
	{
?>

<b>Quel image de monstre souhaitez vous modifier ?</b> <br />


<?php
echo '<table id="profil" width="550px" cellpadding="2" cellspacing="2" style="text-align:center;" >';
echo '<colgroup><COL WIDTH=14%><COL WIDTH=10%><COL WIDTH=20%><COL WIDTH=18%><COL WIDTH=15%><COL WIDTH=28%><COL WIDTH=4%></COLGROUP>';
echo '<tr><td><b>Nom</b></td><td><b>Lvl</b></td><td><b>Stats</b></td><td><b>Attaques</b></td><td><b>Récomp.</b></td><td><b>Image</b></td><td><b>Choix</b></td></tr>';
$reponse = $bdd->query('SELECT * FROM pokemons_base_monstres') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	//nom des attaques
	$reponse2 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['attaque1']));  
	$donnees2 = $reponse2->fetch();
	$attaque1=$donnees2['nom'];
	$reponse2 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['attaque2']));  
	$donnees2 = $reponse2->fetch();
	$attaque2=$donnees2['nom'];
	$reponse2 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['attaque3']));  
	$donnees2 = $reponse2->fetch();
	$attaque3=$donnees2['nom'];
	$reponse2 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['attaque4']));  
	$donnees2 = $reponse2->fetch();
	$attaque4=$donnees2['nom'];
	//récompense
	if($donnees['genre_recompense']=="item")
		{
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse2->execute(array('id' => $donnees['recompense']));  
			$donnees2 = $reponse2->fetch();
		$recompense=$donnees['nb_recompense'].' '.$donnees2['nom']; if($donnees['nb_recompense']>1){echo 's';}
		}
	elseif($donnees['genre_recompense']=="argent"){$recompense=$donnees['recompense'].'$';}
	elseif($donnees['genre_recompense']=="or"){$recompense=$donnees['recompense'].' pépites';}
	else {$recompense=$donnees['nb_recompense'].' '.$donnees['genre_recompense'];}
	//tableau
	echo '<tr><td>'.$donnees['nom'].'</td>
	<td>'.$donnees['lvl'].'</td>
	<td>PV : '.$donnees['pv_max'].'<br />Att. : '.$donnees['att'].'<br />Déf. : '.$donnees['def'].'<br />Vit. : '.$donnees['vit'].'<br />Att.spé. : '.$donnees['attspe'].'<br />Déf.spé. : '.$donnees['defspe'].'<br /></td>
	<td>'.$attaque1.'<br />'.$attaque2.'<br />'.$attaque3.'<br />'.$attaque4.'<br /></td>
	<td>'.$recompense.'</td>
	<td>';
	$chemin='../images/monstres/'.$donnees['id'].'.png';
	if(file_exists($chemin))
	{
	?>
	<div id="cadre_pokemons_carte">
	<div style="margin-left:-3px;margin-top:2px;font-size:13px;"><b><?php echo $donnees['nom']; ?></b></div>
	<img src="../images/monstres/<?php echo $donnees['id'];?>.png" height="83px" style="border-style:none; margin-left:-2px; "/>
	<br />
	<span style="position:relative;bottom:8px;left:1px;font-size:12px;">lvl <?php echo $donnees['lvl']; ?></span>
	<?php
	if($bonus_racketteur=="ok" AND $objet>0)
		{
		echo'<img src="images/cadeau.png" style="position:relative;bottom:9px;left:7px;">';
		}
	?>
	<?php
	if($lvl <10){echo'<img src="images/';if($sexe=="M"){echo 'male';} if($sexe=="F"){echo 'femelle';} echo'.png" style="position:relative;bottom:10px;left:44px;">';}
	else {echo'<img src="images/';if($sexe=="M"){echo 'male';} if($sexe=="F"){echo 'femelle';} echo'.png" style="position:relative;bottom:10px;left:'; if($bonus_racketteur=="ok" AND $objet>0){echo 22;}else{echo 36;} echo 'px;">';}
	?>
	</div>
	<?php  
  }
	echo'</td><td>';
	echo '<form enctype="multipart/form-data" action="images_edit_monstres.php" method="post">';
	echo '<INPUT TYPE="submit" VALUE="Ajout image">';
	echo '<INPUT TYPE="hidden" name="action" value="ajouter_image">';
	echo '<INPUT TYPE="hidden" name="action2" value="go_menu">';
	echo '<INPUT TYPE="hidden" name="id" value="'.$donnees['id'].'">';
	echo "</form>";
 /*?>
	<form enctype="multipart/form-data" action="images_edit_monstres.php" method="post"> 
	<INPUT TYPE="hidden" name="action" value="ajouter_image">
	<INPUT TYPE="hidden" name="action2" value="go_menu">
	<INPUT TYPE="hidden" name="id" value="<?php echo $donnees['id'];?>">
	<INPUT TYPE="submit" VALUE="Ajouter image"> 
	</form>
<?*/
	echo'</td></tr>';
	}
echo '</table>';
?>

<?php
	}

}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>


 <?php include("bas.php"); ?>
