<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text"> 


<?php
if($_SESSION['is_admin'] == true)
{
?>

<h2> Ajouter/supprimer des attaques aux pokémons </h2>


<?php
if($_POST['action']=="voir" OR $_POST['action']=="ajout_attaque" OR $_POST['action']=="ajout_ct" OR $_GET['id_pokedex']>=1)
{
if($_POST['action']=="ajout_attaque")
	{
	$req = $bdd->prepare('INSERT INTO pokemons_apprentissage_attaques (id_pokedex, lvl, id_attaque) VALUES(:id_pokedex, :lvl, :id_attaque)') or die(print_r($bdd->errorInfo()));
		$req->execute(array(
			'id_pokedex' => $_POST['id_pokedex'],	
			'lvl'=> $_POST['lvl'],
			'id_attaque'=> $_POST['id_attaque']
			))
			or die(print_r($bdd->errorInfo()));	
	echo '<b>L\'attaque a bien été ajoutée!</b><br />';
	}
if($_POST['action']=="ajout_ct")
	{
	$req = $bdd->prepare('INSERT INTO pokemons_apprentissage_ct (id_pokedex, id_ct) VALUES(:id_pokedex, :id_ct)') or die(print_r($bdd->errorInfo()));
		$req->execute(array(
			'id_pokedex' => $_POST['id_pokedex'],	
			'id_ct'=> $_POST['id_ct']
			))
			or die(print_r($bdd->errorInfo()));	
	echo '<b>L\'attaque a bien été ajoutée!</b><br />';
	}
if($_POST['action2']=="delete_attaque")
	{
	$req = $bdd->prepare('DELETE FROM pokemons_apprentissage_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
        $req->execute(array(
                    'id' => $_POST['id']				    
					)) or die(print_r($bdd->errorInfo()));
	echo '<b>L\'attaque a bien été supprimée!</b><br />';
	}
if($_POST['action2']=="delete_ct")
	{
	$req = $bdd->prepare('DELETE FROM pokemons_apprentissage_ct WHERE id=:id') or die(print_r($bdd->errorInfo()));
        $req->execute(array(
                    'id' => $_POST['id']				    
					)) or die(print_r($bdd->errorInfo()));
	echo '<b>La CT/CS a bien été supprimée!</b><br />';
	}


if($_GET['id_pokedex']>=1){
    $_POST['id_pokedex']=$_GET['id_pokedex'];
}
$id_pokedex_plus1=$_POST['id_pokedex']+1; 
$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));  
$donnees = $reponse->fetch();
$nom=$donnees['nom'];
?>
<div style="margin-left:180px; float:left;">
<span style="font-size:25px; margin-left:15px;"><?php echo $nom; ?></span>
<br />
<a href="admin_edit_attaques_pokemons.php?id_pokedex=<?php echo $id_pokedex_plus1;?>">
<img src="../images/pokemons/<?php echo $_POST['id_pokedex'];?>.gif" height="150px" style="border-style:none; margin-left:4px; margin-top:10px; "/>
</a>
</div>

<div style="clear:both;"></div>

<b>Apprendre une attaque?</b>
<form method="post" action="admin_edit_attaques_pokemons.php">
<select name="id_attaque">
<option value="0">Aucun</option>
<?php
$reponse = $bdd->query('SELECT id, nom FROM pokemon_base_attaques ORDER BY nom ASC') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].'</option>';
	}
?>
</select>   
Au niveau : <input type="text" name="lvl" size="3">   
<input type="hidden" name="action" value="ajout_attaque">
<input type="hidden" name="id_pokedex" value="<?php echo $_POST['id_pokedex'];?>">
<input type="submit" value="Ajouter l'attaque" />
</form>
<br />
<b>Pouvoir apprendre une CT/CS?</b>
<form method="post" action="admin_edit_attaques_pokemons.php">
<select name="id_ct">
<option value="0">Aucun</option>
<?php
$reponse = $bdd->query('SELECT * FROM pokemon_base_ct ORDER BY num') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	$reponse2 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['id_attaque']));  
	$donnees2 = $reponse2->fetch();
	echo '<option value="'.$donnees['id'].'">'.$donnees['num'].' '.$donnees2['nom'].'</option>';
	}
?>
</select>     
<input type="hidden" name="action" value="ajout_ct">
<input type="hidden" name="id_pokedex" value="<?php echo $_POST['id_pokedex'];?>">
<input type="submit" value="Ajouter la CT/CS" />
</form>
<br />

<!-- LISTE DES ATTAQUES -->
<table id="votre_pokemon" width="550px" cellpadding="2" cellspacing="2" style="text-align:center;" >
<colgroup><COL WIDTH=20%><COL WIDTH=55%><COL WIDTH=25%></COLGROUP>
<tr><th colspan="9">Attaques apprises par montée de niveau</th></tr>
<tr><td> Niveau </td><td>Attaque apprise </td><td>Supprimer </td></tr>
<?php
$reponse = $bdd->prepare('SELECT * FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex ORDER BY lvl') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));  
while($donnees = $reponse->fetch())
	{
	$reponse2 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id ORDER BY nom') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['id_attaque']));  
	$donnees2 = $reponse2->fetch();
	echo '<tr><td>'.$donnees['lvl'].'</td><td>'.$donnees2['nom'].'</td><td><form method="post" action="admin_edit_attaques_pokemons.php"><input type="hidden" name="id" value="'.$donnees['id'].'"><input type="hidden" name="action" value="voir"><input type="hidden" name="id_pokedex" value="'.$_POST['id_pokedex'].'"><input type="hidden" name="action2" value="delete_attaque"><input type="submit" value="Supprimer" /></form></td></tr>';
	}
?>
</table>

<!-- LISTE DES CT -->
<table id="votre_pokemon" width="550px" cellpadding="2" cellspacing="2" style="text-align:center;" >
<colgroup><COL WIDTH=20%><COL WIDTH=55%><COL WIDTH=25%></COLGROUP>
<tr><th colspan="9">Attaques apprises par CT/CS</th></tr>
<tr><td> CT/CS </td><td>Attaque apprise </td><td>Supprimer </td></tr>
<?php
$reponse = $bdd->prepare('SELECT pokemons_apprentissage_ct.id AS id, pokemon_base_ct.num AS num, pokemon_base_ct.id_attaque AS id_attaque FROM pokemons_apprentissage_ct INNER JOIN pokemon_base_ct ON pokemons_apprentissage_ct.id_ct=pokemon_base_ct.id WHERE id_pokedex=:id_pokedex ORDER BY num') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));  
while($donnees = $reponse->fetch())
	{
	$reponse3 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse3->execute(array('id' => $donnees['id_attaque']));  
	$donnees3 = $reponse3->fetch();
	echo '<tr><td>'.$donnees['num'].'</td><td>'.$donnees3['nom'].'</td><td><form method="post" action="admin_edit_attaques_pokemons.php"><input type="hidden" name="id" value="'.$donnees['id'].'"><input type="hidden" name="action" value="voir"><input type="hidden" name="id_pokedex" value="'.$_POST['id_pokedex'].'"><input type="hidden" name="action2" value="delete_ct"><input type="submit" value="Supprimer" /></form></td></tr>';
	}
?>
</table>
<?php
}
else
{
?>
<p>
Ce menu sert à ajouter et modifier les attaques que chaque pokémon peut apprendre dans le jeu.<br />
Vous commencez par choisir un pokémon. Vous avez alors accès aux attaques qu'il peut apprendre. Vous pouvez alors en ajouter/supprimer.
</p>

Quel pokémon souhaitez vous voir? <br />

<form method="post" action="admin_edit_attaques_pokemons.php">
<select name="id_pokedex">
<?php
$reponse = $bdd->query('SELECT * FROM pokemons_base_pokemons ORDER BY id_pokedex') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['id_pokedex'].'">'.$donnees['nom'].'</option>';
	}
?>
</select>
<br />
<input type="hidden" name="action" value="voir">
<input type="submit" value="Voir le pokémon" />
</form>

<?php
}
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>


 <?php include("bas.php"); ?>
