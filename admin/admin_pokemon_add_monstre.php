<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text"> 


<?php
if($_SESSION['is_animateur'] == true)
{
?>

<?php
if($_POST['action']=="creer")
	{
	if($_POST['genre_recompense']!="item"){$_POST['nb_recompense']=1;}
	if($_POST['recompense']==""){$_POST['recompense']=1;}
	$req = $bdd->prepare('INSERT INTO pokemons_base_monstres (nom, lvl, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, genre_recompense, recompense, nb_recompense, poids, type1, type2) 
				VALUES(:nom, :lvl, :pv, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, :genre_recompense, :recompense, :nb_recompense, :poids, :type1, :type2)') or die(print_r($bdd->errorInfo()));
				$req->execute(array(
				'nom' => $_POST['nom'],
				'lvl' => $_POST['lvl'],
				'pv' => $_POST['pv'],
				'pv_max' => $_POST['pv'],
				'att' => $_POST['att'],
				'def' => $_POST['def'],
				'vit' => $_POST['vit'],
				'attspe' => $_POST['attspe'],
				'defspe' => $_POST['defspe'],
				'attaque1' => $_POST['attaque1'],
				'attaque2' => $_POST['attaque2'],
				'attaque3' => $_POST['attaque3'],
				'attaque4' => $_POST['attaque4'],
				'genre_recompense' => $_POST['genre_recompense'],
				'recompense' => $_POST['recompense'],
				'nb_recompense' => $_POST['nb_recompense'],
				'poids' => $_POST['poids'],
				'type1' => $_POST['type1'],
				'type2' => $_POST['type2']
				))or die(print_r($bdd->errorInfo()));	
	echo '<b>La fiche du monstre a bien été crée! </b><br />';
	}
if($_POST['action']=="ajouter") //inutile jusqu'ici
	{
	$reponse = $bdd->prepare('SELECT id FROM pokemons_carte_tableau WHERE hor=:hor AND ver=:ver') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('hor' => $_POST['pos_hor'], 'ver' => $_POST['pos_ver'])); 
	$donnees = $reponse->fetch();
	$id_map = $donnees['id'];
	if($id_map>0)
		{
		$reponse = $bdd->prepare('SELECT * FROM pokemons_base_monstres WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id' => $_POST['id']));
		$donnees = $reponse->fetch();
		$nom=$donnees['nom'];
		$req = $bdd->prepare('INSERT INTO pokemons_map (id_map, id_pokemon, shiney, sexe, lvl, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, combat, objet) 
				VALUES(:id_map, :id_pokemon, 2, :sexe, :lvl, :pv, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, 0, 0)') or die(print_r($bdd->errorInfo()));
				$req->execute(array(
				'id_map' => $id_map,
				'id_pokemon' => $_POST['id'],
				'sexe' => $_POST['sexe'],
				'lvl' => $_POST['lvl'],
				'pv' => $_POST['pv'],
				'pv_max' => $_POST['pv'],
				'att' => $_POST['att'],
				'def' => $_POST['def'],
				'vit' => $_POST['vit'],
				'attspe' => $_POST['attspe'],
				'defspe' => $_POST['defspe'],
				'attaque1' => $_POST['attaque1'],
				'attaque2' => $_POST['attaque2'],
				'attaque3' => $_POST['attaque3'],
				'attaque4' => $_POST['attaque4']
				))or die(print_r($bdd->errorInfo()));	
		$a_qui='('.$_POST['pos_ver'].'/'.$_POST['pos_hor'].')';
		$req = $bdd->prepare('INSERT INTO pokemons_survey_admin (pseudo, action, quand, a_qui, quoi, combien, justification) VALUES(:pseudo, "ajout pokémon", now(), :a_qui, :quoi, :combien, :justification)') or die(print_r($bdd->errorInfo()));
		$req->execute(array(
                    'pseudo' => $_SESSION['pseudo'],
					'a_qui' => $a_qui,
					'quoi' => $nom, 
					'combien' => $lvl,
					'justification' => $_POST['justification']
					))or die(print_r($bdd->errorInfo()));
		echo '<b>Monstre bien ajouté!</b><br />';
		}
	else
		{
		if($id_map == 0)
			{
			echo '<b>La map indiquée n\'existe pas.</b><br />';
			}
		}
	}
?>

<h2> Ajouter un monstre </h2>

<p>
Ce formulaire vous sert principalement à animer le jeu en créant des évents, avec l'apparition de MONSTRES. <br />
Les monstres sont des créatures pokémons ou non, qui ne peuvent pas être capturés. Nous pourrons par exemple ainsi faire intervenir cette bonne vieille team rocket.<br />
Vous pouvez choisir les stats et les dégats de la créature. Veillez à choisir un lvl approprié, il servira pour le calcul d'xp.<br />
Comme les monstres ne sont pas encore dans la base de donnée, il faut d'abord créer le profil d'un monstre. On peut alors l'ajouter autant de fois que l'on veut sur la map.
</p>

<?php
if($_POST['action']=="ajouter_image")
	{
	if($_POST['action2']=="go_menu")
		 {
		 ?>
		 <h2> Ajouter une image </h2>
		 <FORM ENCTYPE="multipart/form-data" ACTION="admin_pokemon_add_monstre.php" METHOD="POST"> 
		<INPUT TYPE="hidden" name="action" value="ajouter_image">
		<INPUT TYPE="hidden" name="action2" value="ajouter">
		<INPUT TYPE="hidden" name="id" value="<?php echo $_POST['id'];?>">
		(uniquement .png) : <br /><INPUT NAME="fichier" TYPE="file"> 
		<INPUT TYPE="submit" VALUE="Ajouter l'image"> 
		</FORM>
		<?php
		 }
	elseif($_POST['action2']=="ajouter")
		{
		$size=$_FILES['fichier']['size']; 
		$type=$_FILES['fichier']['type'];
		$tmp=$_FILES['fichier']['tmp_name'];
		$fichier=$_FILES['fichier']['name'];
		list($width,$height)=getimagesize($tmp);
		$lieu='../images/monstres/'.$_POST['id'].'.png';	
		move_uploaded_file($tmp,$lieu);	
		echo '<b>Votre image a bien été transférée!</b><br /><br /><a href="admin_pokemon_add_monstre.php">Retour à la plateforme</a>';
		}
	}
elseif($_POST['action']=="ajouter_monstre")
	{
	if($_POST['action2']=="go_menu")
		{
		?>
		<h2> Ajouter un monstre </h2>
		<form action="admin_pokemon_add_monstre.php" method="post"> 
		Coordonnée horizontale :<input type="text" name="pos_ver" size="4"><br />
		Coordonnée verticale :<input type="text" name="pos_hor" size="4"><br />
		Combien :<input type="text" name="quantite" size="4"><br />
		<textarea name="justification" rows="8" cols="55">Justification...</textarea> <br />
		<INPUT TYPE="hidden" name="action" value="ajouter_monstre">
		<INPUT TYPE="hidden" name="action2" value="ajouter">
		<INPUT TYPE="hidden" name="id" value="<?php echo $_POST['id'];?>">
		<INPUT TYPE="submit" VALUE="Ajouter le monstre"> 
		</FORM>
		<?php
		}
	elseif($_POST['action2']=="ajouter")
		{
		$i=1;
		while($i<=$_POST['quantite'])
			{
			$i=$i+1;
			$reponse = $bdd->prepare('SELECT id FROM pokemons_carte_tableau WHERE hor=:hor AND ver=:ver') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('hor' => $_POST['pos_hor'], 'ver' => $_POST['pos_ver'])); 
			$donnees = $reponse->fetch();
			$id_map = $donnees['id'];
			$reponse = $bdd->prepare('SELECT * FROM pokemons_base_monstres WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $_POST['id']));
			$donnees = $reponse->fetch();
			$shiney=0;
			$sexe="";
			$lvl=$donnees['lvl'];
			$pv=$donnees['pv'];
			$att=$donnees['att'];
			$def=$donnees['def'];
			$vit=$donnees['vit'];
			$attspe=$donnees['attspe'];
			$defspe=$donnees['defspe'];
			$attaque1=$donnees['attaque1'];
			$attaque2=$donnees['attaque2'];
			$attaque3=$donnees['attaque3'];
			$attaque4=$donnees['attaque4'];
			$req = $bdd->prepare('INSERT INTO pokemons_map (id_map, id_pokemon, shiney, sexe, lvl, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, combat, objet, monstre) 
					VALUES(:id_map, :id_pokemon, :shiney, :sexe, :lvl, :pv, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, 0, 0, 1)') or die(print_r($bdd->errorInfo()));
					$req->execute(array(
					'id_map' => $id_map,
					'id_pokemon' => $_POST['id'],
					'shiney' => $shiney,
					'sexe' => $sexe,
					'lvl' => $lvl,
					'pv' => $pv,
					'pv_max' => $pv,
					'att' => $att,
					'def' => $def,
					'vit' => $vit,
					'attspe' => $attspe,	
					'defspe' => $defspe,
					'attaque1' => $attaque1,
					'attaque2' => $attaque2,
					'attaque3' => $attaque3,
					'attaque4' => $attaque4
					))or die(print_r($bdd->errorInfo()));	
			}
		echo '<b>Monstre(s) bien ajouté(s)</b>';
		}
	}
else
	{
?>

<b>Quel monstre souhaitez vous ajouter?</b> <br />


<?php
echo '<table id="profil" width="550px" cellpadding="2" cellspacing="2" style="text-align:center;" >';
echo '<colgroup><COL WIDTH=14%><COL WIDTH=10%><COL WIDTH=20%><COL WIDTH=18%><COL WIDTH=15%><COL WIDTH=28%><COL WIDTH=4%></COLGROUP>';
echo '<tr><td><b>Nom</b></td><td><b>Lvl</b></td><td><b>Stats</b></td><td><b>Attaques</b></td><td><b>Récomp.</b></td><td><b>Image</b></td><td><b>Choix</b></td></tr>';
$reponse = $bdd->query('SELECT * FROM pokemons_base_monstres') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	//nom des attaques
	$reponse2 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['attaque1']));  
	$donnees2 = $reponse2->fetch();
	$attaque1=$donnees2['nom'];
	$reponse2 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['attaque2']));  
	$donnees2 = $reponse2->fetch();
	$attaque2=$donnees2['nom'];
	$reponse2 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['attaque3']));  
	$donnees2 = $reponse2->fetch();
	$attaque3=$donnees2['nom'];
	$reponse2 = $bdd->prepare('SELECT * FROM pokemon_base_attaques WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['attaque4']));  
	$donnees2 = $reponse2->fetch();
	$attaque4=$donnees2['nom'];
	//récompense
	if($donnees['genre_recompense']=="item")
		{
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse2->execute(array('id' => $donnees['recompense']));  
			$donnees2 = $reponse2->fetch();
		$recompense=$donnees['nb_recompense'].' '.$donnees2['nom']; if($donnees['nb_recompense']>1){echo 's';}
		}
	elseif($donnees['genre_recompense']=="argent"){$recompense=$donnees['recompense'].'$';}
	elseif($donnees['genre_recompense']=="or"){$recompense=$donnees['recompense'].' pépites';}
	else {$recompense=$donnees['nb_recompense'].' '.$donnees['genre_recompense'];}
	//tableau
	echo '<tr><td>'.$donnees['nom'].'</td>
	<td>'.$donnees['lvl'].'</td>
	<td>PV : '.$donnees['pv_max'].'<br />Att. : '.$donnees['att'].'<br />Déf. : '.$donnees['def'].'<br />Vit. : '.$donnees['vit'].'<br />Att.spé. : '.$donnees['attspe'].'<br />Déf.spé. : '.$donnees['defspe'].'<br /></td>
	<td>'.$attaque1.'<br />'.$attaque2.'<br />'.$attaque3.'<br />'.$attaque4.'<br /></td>
	<td>'.$recompense.'</td>
	<td>';
	$chemin='../images/monstres/'.$donnees['id'].'.png';
	if(file_exists($chemin))
	{
	?>
	<div id="cadre_pokemons_carte">
	<div style="margin-left:-3px;margin-top:2px;font-size:13px;"><b><?php echo $donnees['nom']; ?></b></div>
	<img src="../images/monstres/<?php echo $donnees['id'];?>.png" height="83px" style="border-style:none; margin-left:-2px; "/>
	<br />
	<span style="position:relative;bottom:8px;left:1px;font-size:12px;">lvl <?php echo $donnees['lvl']; ?></span>
	<?php
	if($bonus_racketteur=="ok" AND $objet>0)
		{
		echo'<img src="images/cadeau.png" style="position:relative;bottom:9px;left:7px;">';
		}
	?>
	<?php
	if($lvl <10){echo'<img src="images/';if($sexe=="M"){echo 'male';} if($sexe=="F"){echo 'femelle';} echo'.png" style="position:relative;bottom:10px;left:44px;">';}
	else {echo'<img src="images/';if($sexe=="M"){echo 'male';} if($sexe=="F"){echo 'femelle';} echo'.png" style="position:relative;bottom:10px;left:'; if($bonus_racketteur=="ok" AND $objet>0){echo 22;}else{echo 36;} echo 'px;">';}
	?>
	</div>
	<?php 
	}else{
        echo '<form enctype="multipart/form-data" action="admin_pokemon_add_monstre.php" method="post">';
	echo '<INPUT TYPE="submit" VALUE="Ajout image">';
	echo '<INPUT TYPE="hidden" name="action" value="ajouter_image">';
	echo '<INPUT TYPE="hidden" name="action2" value="go_menu">';
	echo '<INPUT TYPE="hidden" name="id" value="'.$donnees['id'].'">';
	echo "</form>";
 /*?>
	<form enctype="multipart/form-data" action="admin_pokemon_add_monstre.php" method="post"> 
	<INPUT TYPE="hidden" name="action" value="ajouter_image">
	<INPUT TYPE="hidden" name="action2" value="go_menu">
	<INPUT TYPE="hidden" name="id" value="<?php echo $donnees['id'];?>">
	<INPUT TYPE="submit" VALUE="Ajouter image"> 
	</form>
<?*/
	}
	echo'</td><td>';
	?>
	<form action="admin_pokemon_add_monstre.php" method="post">
	<INPUT TYPE="hidden" name="action" value="ajouter_monstre">
	<INPUT TYPE="hidden" name="action2" value="go_menu">
	<INPUT TYPE="hidden" name="id" value="<?php echo $donnees['id'];?>">
	<INPUT TYPE="submit" VALUE="Ajout"> 
	</FORM>
	<?php
	echo'</td></tr>';
	}
echo '</table>';
?>

<b>Créer un monstre</b> <br />

<form method="post" action="admin_pokemon_add_monstre.php">
Nom :<input type="text" name="nom" size="12"><br />	
Lvl :<input type="text" name="lvl" size="4"><br />	
PV :<input type="text" name="pv" size="4"><br />	
Attaque :<input type="text" name="att" size="4"><br />
Defense :<input type="text" name="def" size="4"><br />
Vitesse :<input type="text" name="vit" size="4"><br />
Attaque spéciale :<input type="text" name="attspe" size="4"><br />
Défense spéciale :<input type="text" name="defspe" size="4"><br />
Poids (de 1 à 1000 Kg) :<input type="text" name="poids" size="4"><br />
Type 1: <select name="type1">
<option value="acier">acier</option><option value="combat">combat</option><option value="dragon">dragon</option><option value="eau">eau</option>
<option value="electrique">electrique</option><option value="spectre">spectre</option><option value="feu">feu</option><option value="glace">glace</option><option value="insecte">insecte</option>
<option value="normal">normal</option><option value="plante">plante</option><option value="poison">poison</option><option value="psy">psy</option>
<option value="roche">roche</option><option value="sol">sol</option><<option value="tenebre">tenebre</option><option value="vol">vol</option>
</select><br />
Type 2: <select name="type2">
<option value="0">aucun</option><option value="acier">acier</option><option value="combat">combat</option><option value="dragon">dragon</option><option value="eau">eau</option>
<option value="electrique">electrique</option><option value="spectre">spectre</option><option value="feu">feu</option><option value="glace">glace</option><option value="insecte">insecte</option>
<option value="normal">normal</option><option value="plante">plante</option><option value="poison">poison</option><option value="psy">psy</option>
<option value="roche">roche</option><option value="sol">sol</option><<option value="tenebre">tenebre</option><option value="vol">vol</option>
</select><br />
Attaque 1 
<select name="attaque1">
<option value="0">Pas d'attaque</option>
<?php
$reponse = $bdd->query('SELECT * FROM pokemon_base_attaques ORDER BY nom ASC') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].'</option>';
	}
?>
</select><br />
Attaque 2 
<select name="attaque2">
<option value="0">Pas d'attaque</option>
<?php
$reponse = $bdd->query('SELECT * FROM pokemon_base_attaques ORDER BY nom ASC') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].'</option>';
	}
?>
</select><br />
Attaque 3 
<select name="attaque3">
<option value="0">Pas d'attaque</option>
<?php
$reponse = $bdd->query('SELECT * FROM pokemon_base_attaques ORDER BY nom ASC') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].'</option>';
	}
?>
</select><br />
Attaque 4 
<select name="attaque4">
<option value="0">Pas d'attaque</option>
<?php
$reponse = $bdd->query('SELECT * FROM pokemon_base_attaques ORDER BY nom ASC') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].'</option>';
	}
?>
</select><br />
<b>Récompense pour celui qui le tue</b><br />
Quoi : <select name="genre_recompense">
		<option value="argent">Argent</option>
		<option value="or">Or</option>	
		<option value="points">points</option>
		<option value="item">Item</option>
	</select><br />
Quantité (si objet), sinon 1 :<input type="text" name="nb_recompense" size="4"><br />
ID de l'objet (si item), somme (reste) :<input type="text" name="recompense" size="4">

<textarea name="justification" rows="8" cols="55">Justification...</textarea> <br />
<input type="hidden" name="action" value="creer">
<input type="submit" value="Créer" />
</form><br />

<?php
	}

}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>


 <?php include("bas.php"); ?>
