<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>

  
<div id="text_contenu" style="margin-top:0px;">
<div id="text">



<?php

if($_SESSION['is_admin'] == true)
{


if($_POST['action']=="pokemons"){
    $req = $bdd->prepare('UPDATE pokemons_base_pokemons SET pv=:pv, bonus_pv=:bonus_pv, att=:att, bonus_att=:bonus_att, def=:def, bonus_def=:bonus_def, vit=:vit, bonus_vit=:bonus_vit, attspe=:attspe, bonus_attspe=:bonus_attspe, defspe=:defspe, bonus_defspe=:bonus_defspe WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));
    $req->execute(array(   
        'pv' => stripslashes($_POST['pv']),
        'bonus_pv' => stripslashes($_POST['bonus_pv']),
        'att' => stripslashes($_POST['att']),
        'bonus_att' => stripslashes($_POST['bonus_att']),
        'def' => stripslashes($_POST['def']),
        'bonus_def' => stripslashes($_POST['bonus_def']),
        'vit' => stripslashes($_POST['vit']),
        'bonus_vit' => stripslashes($_POST['bonus_vit']),
        'attspe' => stripslashes($_POST['attspe']),
        'bonus_attspe' => stripslashes($_POST['bonus_attspe']),
        'defspe' => stripslashes($_POST['defspe']),
        'bonus_defspe' => stripslashes($_POST['bonus_defspe']),
        'id_pokedex' => stripslashes($_POST['id_pokedex'])
        ))or die(print_r($bdd->errorInfo()));
    echo 'Pokemon bien modifié!';
}
?>	

<h2> Modifier un pokémon </h2>

<form method="post" action="admin_modif_stat.php">
<select name="id_pokedex">
<?php
$reponse = $bdd->query('SELECT id_pokedex, nom FROM pokemons_base_pokemons ORDER BY id_pokedex ASC') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch()){
    echo '<option value="'.$donnees['id_pokedex'].'" ';
    if(isset($_POST['id_pokedex']) AND $_POST['id_pokedex']==$donnees['id_pokedex']){echo ' selected ';}
    echo '>'.$donnees['nom'].'</option>';
}
?>
</select>       
<input type="submit" value="Recherche" />
</form>       
<br /><br />

<?php 
if(isset($_POST['id_pokedex'])){
    $reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));  
    $reponse->execute(array('id_pokedex' => $_POST['id_pokedex'])); 
    $donnees = $reponse->fetch();
?>
    <b><u>Stats actuelles du pokémon</u></b>
    <br /><br />
    <form method="post" action="admin_modif_stat.php">
    <table>
      <tr>
        <td nowrap align="right"><p>PV de base :</p></td>
        <td><input type="text" name="pv" value="<?php echo $donnees['pv'];?>" size="10"></td>
      </tr>  
               <tr>
        <td nowrap align="right"><p>Bonus PV :</p></td>
        <td><input type="text" name="bonus_pv" value="<?php echo $donnees['bonus_pv'];?>" size="10"></td>
      </tr>  
               <tr>
        <td nowrap align="right"><p>Attaque de base :</p></td>
        <td><input type="text" name="att" value="<?php echo $donnees['att'];?>" size="10"></td>
      </tr>  
               <tr>
        <td nowrap align="right"><p>Bonus attaque :</p></td>
        <td><input type="text" name="bonus_att" value="<?php echo $donnees['bonus_att'];?>" size="10"></td>
      </tr>  
               <tr>
        <td nowrap align="right"><p>Défense de base :</p></td>
        <td><input type="text" name="def" value="<?php echo $donnees['def'];?>" size="10"></td>
      </tr> 
              <tr>
        <td nowrap align="right"><p>Bonus défense :</p></td>
        <td><input type="text" name="bonus_def" value="<?php echo $donnees['bonus_def'];?>" size="10"></td>
      </tr> 		
               <tr>
        <td nowrap align="right"><p>Vitesse de base :</p></td>
        <td><input type="text" name="vit" value="<?php echo $donnees['vit'];?>" size="10"></td>
      </tr>  
               <tr>
        <td nowrap align="right"><p>Bonus vitesse :</p></td>
        <td><input type="text" name="bonus_vit" value="<?php echo $donnees['bonus_vit'];?>" size="10"></td>
      </tr> 
               <tr>
        <td nowrap align="right"><p>Attaque spéciale de base :</p></td>
        <td><input type="text" name="attspe" value="<?php echo $donnees['attspe'];?>" size="10"></td>
      </tr> 
              <tr>
        <td nowrap align="right"><p>Bonus attaque spéciale :</p></td>
        <td><input type="text" name="bonus_attspe" value="<?php echo $donnees['bonus_attspe'];?>" size="10"></td>
      </tr> 		
               <tr>
        <td nowrap align="right"><p>Défense spéciale de base :</p></td>
        <td><input type="text" name="defspe" value="<?php echo $donnees['defspe'];?>" size="10"></td>
      </tr>  
               <tr>
        <td nowrap align="right"><p>Bonus défense spéciale :</p></td>
        <td><input type="text" name="bonus_defspe" value="<?php echo $donnees['bonus_defspe'];?>" size="10"></td>
      </tr> 
    </table>
    <input type="hidden" name="id_pokedex" value="<?php echo $_POST['id_pokedex'];?>">
    <input type="hidden" name="action" value="pokemons">
         <input type="submit" value="Modifier" />
    </form>
    <br /><br />
<?php
}
?>
    
<b><u>Les stats, comment ça marche ?</u></b>
<br /><br />
Chaque statistique a une valeur de base et une valeur "bonus". <br />
Au niveau 1, le pokémon ne possède que la valeur de base.<br />
A chaque montée de lvl, ce pokémon gagne les bonus indiqués dans chaque stat.<br />
Il a également 1 chance sur 2 de gagner un point supplémentaire, pour chacune de ses stats. <br />
<br />    
Le maximum possible dans une stat est calculé comme suit :   <br />  
PV : base + 150*bonus + 300<br />
Autres stat : base + 150*bonus + 200<br />

<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>
 <?php include("bas.php"); ?>
