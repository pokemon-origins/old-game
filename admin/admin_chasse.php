<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
<?php
if($_SESSION['is_animateur'] == true)
{
?>
 
 
<h2> Administrer le concours de chasse </h2>
<p>Ce menu vous permet de gérer le concours de chasse en cours<br />
Sur cette page vous pouvez créer un nouveau concours de chasse, observer son évolution durant la semaine et le cloturer.</p>

<?php
if($_POST['action']=="supprimer") //SUPPRESSION
	{
	$reponse = $bdd->prepare('DELETE FROM pokemon_target_concours WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $_POST['id'])); 
	echo '<span style="color:red;"><b>La cible a bien été supprimée. </b><br /><br /></span>';
	}
if($_POST['action']=="ajouter" AND $_POST['points']!=0) //SUPPRESSION
	{
	$req = $bdd->prepare('INSERT INTO pokemon_target_concours (nom, points) VALUES(:nom, :points)') or die(print_r($bdd->errorInfo()));
		$req->execute(array(
                    'nom' => $_POST['nom'],
					'points' => $_POST['points']
					))or die(print_r($bdd->errorInfo()));
	echo '<span style="color:red;"><b>La cible a bien été ajoutée. </b><br /><br /></span>';
	}
if($_POST['action']=="mois_suivant") //SUPPRESSION
	{
	$reponse = $bdd->query('SELECT * FROM pokemon_target_concours WHERE nom="mois" ') or die(print_r($bdd->errorInfo()));
	$donnees = $reponse->fetch();
	$mois=$donnees['points'];
	if($mois!=12){$mois=$mois+1;}else{$mois=1;}
	$reponse = $bdd->prepare('UPDATE pokemon_target_concours SET points=:points WHERE nom="mois"') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('points' => $mois)); 
	$reponse = $bdd->query('UPDATE pokemons_membres SET concours=0') or die(print_r($bdd->errorInfo())); //remise à 0 des points!!
	echo '<span style="color:red;"><b>Vous venez de passer au mois suivant </b><br /><br /></span>';
	}
//chargement du mois
$reponse = $bdd->query('SELECT * FROM pokemon_target_concours WHERE nom="mois" ') or die(print_r($bdd->errorInfo()));
$donnees = $reponse->fetch();
$mois=$donnees['points'];
?>







<b>Ajouter une cible :</b>
<form action="admin_chasse.php" method="post">                     	         
<select name="nom">
<?php
$reponse = $bdd->query('SELECT * FROM pokemons_base_pokemons ORDER BY id_pokedex') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['nom'].'">'.$donnees['nom'].'</option>';
	}
?>
</select><br />	 
points : <input type="text" name="points" size="4"><br />		
<input type="hidden" name="action"  value="ajouter" /> 	
<input type="submit" value="Ajouter" />           
</form>

<h2>Cibles</h2>
Voici les cibles actuelles pour le concours de chasse <b>du <?php echo $mois; ?>ème mois</b>.<br />
<?php //CIBLES
$num=1;
echo '<table id="profil" width="550px" cellpadding="2" cellspacing="2" style="text-align:center;" >';
echo '<colgroup><COL WIDTH=10%><COL WIDTH=30%><COL WIDTH=20%><COL WIDTH=30%></COLGROUP>';
echo '<tr><td><b>N°</b></td><td><b>Pokémon</b></td><td><b>points</b></td><td><b>supprimer</b></td></tr>';
$reponse = $bdd->query('SELECT * FROM pokemon_target_concours WHERE nom!="mois" AND nom!="" ORDER by points DESC') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	echo '<tr><td>'.$num.'</td><td>'.$donnees['nom'].'</a></td>';
	echo '<td>'.$donnees['points'].'</td>';
	echo '<td><form action="admin_chasse.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'" /><input type="hidden" name="action"  value="supprimer" /><input type="submit" value="Supprimer" /></form></td></tr>';
	$num=$num+1;
	}
echo '</table>';
?>


<h2>Classement</h2>
Voici le classement actuel pour le concours de chasse <b>du <?php echo $mois; ?>ème mois</b>.<br />
<h3>Classement des plus hauts scores</h3>
<?php //CLASSEMENT +
$num=1;
echo '<table id="profil" width="550px" cellpadding="2" cellspacing="2" style="text-align:center;" >';
echo '<colgroup><COL WIDTH=10%><COL WIDTH=30%><COL WIDTH=20%></COLGROUP>';
echo '<tr><td><b>N°</b></td><td><b>Pseudo</b></td><td><b>points au concours</b></td></tr>';
$reponse = $bdd->query('SELECT * FROM pokemons_membres ORDER BY concours DESC LIMIT 0,25') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	echo '<tr><td>'.$num.'</td><td><a href="survey_players.php?player='.$donnees['pseudo'].'">'.$donnees['pseudo'].'</a></td>';
	echo '<td>'.$donnees['concours'].'</td></tr>';
	$num=$num+1;
	}
echo '</table>';
?>
<h3>Classement des plus bas scores</h3>
<?php //CLASSEMENT -
$num=1;
echo '<table id="profil" width="550px" cellpadding="2" cellspacing="2" style="text-align:center;" >';
echo '<colgroup><COL WIDTH=10%><COL WIDTH=30%><COL WIDTH=20%></COLGROUP>';
echo '<tr><td><b>N°</b></td><td><b>Pseudo</b></td><td><b>points au concours</b></td></tr>';
$reponse = $bdd->query('SELECT * FROM pokemons_membres ORDER BY concours ASC LIMIT 0,25') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	echo '<tr><td>'.$num.'</td><td><a href="survey_players.php?player='.$donnees['pseudo'].'">'.$donnees['pseudo'].'</a></td>';
	echo '<td>'.$donnees['concours'].'</td></tr>';
	$num=$num+1;
	}
echo '</table>';
?>

	
<br /><br /><br />
Attention!!!!
Ne cliquez sur ceci que lorsque vous avez :<br />
- récompensez tout le monde, ou au moins copié les résultats sur le forum<br />
- copié la liste des cibles sur le forum
<form action="admin_chasse.php" method="post">  
<input type="hidden" name="action"  value="mois_suivant" /> 	
<input type="submit" value="Passer au mois suivant" />           
</form>

	
<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>	
   

 

<?php include ("bas.php"); ?>
