<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text"> 


<?php
if($_SESSION['is_image_uploader'] == true)
{
?>

<h2> Modifier l'image d'un pokemon </h2>

<p>
Ce formulaire vous permet d'editer les images des pokemon.<br/>
Merci de ne mettre que des images au format <b>GIF</b> et de taille <b>EXACTEMENT 196x195</b>.<br/>
Note importante: Il est fort probable que la modification de l'image ne soit pas visible instantanément.<br/>
Cela est dû à votre navigateur qui affiche les images du site en "cache". Pour vérifier que l'image a bien été modifiée,<br/>
il faut faire clic droit sur l'image puis "afficher dans un nouvel onglet" puis rafraichir l'image (avec F5 par exemple).<br/>
</p>

<?php
if($_POST['action']=="ajouter_image")
	{
	if($_POST['action2']=="go_menu")
		 {
		 ?>
		 <h2> Ajouter une image </h2>
     Ancienne image:<br/>
     <img src="../images/pokemons/<?php
        if ($_POST['is_shiny'] == "true") { echo "shiney/"; }
        echo $_POST['id'];
                                  ?>.gif" />
		 <FORM ENCTYPE="multipart/form-data" ACTION="images_edit_pokemons.php" METHOD="POST"> 
		<INPUT TYPE="hidden" name="action" value="ajouter_image">
		<INPUT TYPE="hidden" name="action2" value="ajouter">
		<INPUT TYPE="hidden" name="id" value="<?php echo $_POST['id'];?>">
		<INPUT TYPE="hidden" name="is_shiny" value="<?php echo $_POST['is_shiny'];?>">
		<b>(uniquement .gif de taille 196x195)</b> : <br /><INPUT NAME="fichier" TYPE="file"> 
		<INPUT TYPE="submit" VALUE="Modifier l'image"> 
		</FORM>
		<?php
		 }
	elseif($_POST['action2']=="ajouter")
		{
		$size=$_FILES['fichier']['size']; 
		$type=$_FILES['fichier']['type'];
		$tmp=$_FILES['fichier']['tmp_name'];
		$fichier=$_FILES['fichier']['name'];
		list($width,$height)=getimagesize($tmp);
		$lieu='../images/pokemons/';
    if ($_POST['is_shiny'] == "true") { $lieu.="shiney/";} 
    $lieu=$lieu.$_POST['id'].'.gif';	
		move_uploaded_file($tmp,$lieu);	
		echo '<b>Votre image a bien été transférée!</b><br /><br /><a href="images_edit_pokemons.php">Retour à la plateforme</a>';
		}
	}
else
	{
?>

<b>Quel image de pokemon souhaitez vous modifier ?</b> <br />


<?php
	echo '<form enctype="multipart/form-data" action="images_edit_pokemons.php" method="post">';
	echo '<INPUT TYPE="hidden" name="action" value="ajouter_image">';
	echo '<INPUT TYPE="hidden" name="action2" value="go_menu">';
  echo 'Shiny?<br/>';
  echo '<input type="radio" name="is_shiny" value="false" checked> Non<br>';
  echo '<input type="radio" name="is_shiny" value="true"> Oui<br>';
  echo 'Pokemon à editer:<br/>';
  echo'<SELECT name="id" size="1">';
  $reponse = $bdd->query('SELECT id_pokedex, nom FROM pokemons_base_pokemons ORDER BY id_pokedex ASC') or die(print_r($bdd->errorInfo()));
  while($donnees = $reponse->fetch())
	{
    $id_pokedex = $donnees["id_pokedex"];
    $name = $donnees["nom"];
    echo '<option value="'.$id_pokedex.'">'.$name.' (n°'.$id_pokedex.')</option>';
	}
  echo '</SELECT>';
  echo '<br/>';
	echo '<INPUT TYPE="submit" VALUE="Ajout image (étape suivante)">';
  echo "</form>";
?>

<?php
	}

}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>


 <?php include("bas.php"); ?>
