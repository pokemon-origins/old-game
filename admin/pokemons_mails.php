<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>

 
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
 

<?php
if($_SESSION['is_big_boss'] == true)
{
?>
 
<?php

if($_POST['action']=="envoyer")
	{
	$req = $bdd->prepare('INSERT INTO pokemons_survey_admin (pseudo, action, quand, titre, texte) VALUES(:pseudo, "envoie d\'un email à tous", now(), :titre, :texte)') or die(print_r($bdd->errorInfo()));
	$req->execute(array(
        'pseudo' => $_SESSION['pseudo'],
        'titre' => stripslashes($_POST['titre']), 
        'texte' => stripslashes($_POST['message']) 
        ))or die(print_r($bdd->errorInfo()));
	$message=nl2br(stripslashes($_POST['message']));
	$titre=$_POST['titre'];
	$headers ='From: "Pokémons Origins"<admin@pokemon-origins.com>'."\r\n";
	$headers .='Reply-To: admin@pokemon-origins.com'."\r\n";
	$headers .='Content-Type: text/html; charset="utf-8"'."\n";
	$headers .='Content-Transfer-Encoding: 8bit';
	$texte ='<html><head><title>'.$titre.'</title>
	</head><body>'.$message.'<br /><br />Vous recevez cette email car vous êtes inscrit au site de jeu en ligne <a href="http://www.pokemon-origins.com">Pokémon Origins</a>. Si vous désirez ne plus recevoir cette newsletter, vous pouvez modifier vos préférences dans votre compte à partir du site.
	</body></html>';
        
        //avant
        //$reponse = $bdd->query('SELECT mail FROM pokemons_membres WHERE newsletter=0 AND pos_hor!=1000 AND pos_ver!=1000 AND mail_encore_valide=0') or die(print_r($bdd->errorInfo()));
	
	$reponse = $bdd->query('SELECT mail, mail_encore_valide FROM pokemons_membres WHERE newsletter=0 LIMIT 12000,2000') or die(print_r($bdd->errorInfo()));
	while($donnees = $reponse->fetch()){
            if($donnees['mail_encore_valide']==0){
                mail($donnees['mail'], $titre, $texte, $headers);
            }
	}
	
	echo 'Message envoyé!<br />';
	}
else
	{
?>
<h2> Envoie d'une newsletter aux joueurs </h2>
<p>Ce formulaire vous permet d'envoyer un email à chaque joueur acceptant la newsletter. C'est fort utile pour prévenir lors du lancement d'un évent ou d'un concours important par exemple. <br />
Veillez cependant à ne pas en envoyer trop souvent. Les joueurs n'aiment pas être submergés de "spams". Une bonne newsletter reprennant plusieurs points est souvent mieux accueilli que 5 newsletters d'affilées. De manière générale, il faudrait éviter d'en envoyer plus d'une tous les 10 jours.<br />
Dès que vous avez cliqué sur "envoi", les emails partent. Vous ne pouvez donc plus les modifier!! Vérifiez donc bien tout avant de cliquer (message clair, complet, orthographe, signature).<br />
le script va alors envoyer plus de 2000 emails. Ne vous étonnez pas si la page met un peu de temps à charger. Il ne faut pas la couper, sous peine de ne voir que certains joueurs déservis et d'autres pas.<br />
</p>
	<form action="pokemons_mails.php" method="post" enctype="multipart/form-data" name="mailto">
		<table border="0" cellspacing="2">
			<tr><td valign="top"><p>Titre :</p></td><td><input name="titre" type="text" size="72"></td></tr>
			<tr><td valign="top"><p>Message : </p></td><td><textarea name="message" cols="60" rows="15"></textarea></td></tr>
			<tr><td><input name="action" type="hidden" value="envoyer"></td>
				<td><input name="envoi" type="submit" value="Envoyer"></td></tr>
		</table>
	</form>
	
<?php
	}
?>

<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>	
   

 

<?php include ("bas.php"); ?>
