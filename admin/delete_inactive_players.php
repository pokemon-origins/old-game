<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
<?php
if($_SESSION['is_big_boss'] == true)
{
?>
 
<h2> Supprimer tous les joueurs inactifs </h2>


<?php

function deleteUser($pseudo, $bdd)
{
		// Delete all places where the user is used
		$req = $bdd->prepare('DELETE FROM bans WHERE username=:pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));


		$req = $bdd->prepare('DELETE FROM comptes_bloques WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_black_liste WHERE pseudo = :pseudo OR cible = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_buy_pokemons WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_captures WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_elevage WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_enchere_pokemons WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_grade WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_inventaire WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_liste_defis WHERE joueur1 = :pseudo OR joueur2 = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_liste_defis_pokemons WHERE proprietaire = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_liste_pokemons WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_liste_pokemons_blind_battles WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_mails_boite_envoi WHERE expediteur = :pseudo OR destinataire = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_mails WHERE expediteur = :pseudo OR destinataire = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_marche_items WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_marche_or WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_marche_pokemons WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_monstres_killed WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_recompenses_liste WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		$req = $bdd->prepare('DELETE FROM pokemons_membres WHERE pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$req->execute(array('pseudo' => $pseudo)) or die(print_r($bdd->errorInfo()));

		echo '<b>Le joueur "'.$pseudo.'" a été effacé du jeu!</b> <br />';
}

if(isset($_POST['delete_all']))
{
	$sql_get_inactive_acc = "SELECT * FROM pokemons_membres WHERE score <= 60 AND score_total <= 60 AND bds <= 20 AND quete_principale <= 3 AND nb_pages <= 200;";
	
	$reponse = $bdd->query($sql_get_inactive_acc) or die(print_r($bdd->errorInfo()));
	$count_inactive = 0;
	while($all_inactive_members = $reponse->fetch())
	{
		deleteUser($all_inactive_members['pseudo'], $bdd);
		$count_inactive += 1;
	}

	echo "<br/><br/>Deleted ".$count_inactive." accounts !<br/>";
}

?>


<form action="delete_inactive_players.php" method="post">
<input type="hidden" name="delete_all" value="delete_all" />
<input type="submit" value="Supprimer tous les joueurs inactifs" />	  
</form>
	
<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>	
  
<?php include ("bas.php"); ?>
