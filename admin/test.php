<?php include ("banniere.php"); ?>
<?php include ("menu.php"); ?> 

<div id="text_contenu">
<div id="text">

<?php
//apparition des pokémons
$reponse_first = $bdd->prepare('SELECT id_map, max_pokemons, nb_ajout, duree FROM pokemons_map_cases WHERE derniere<:derniere') or die(print_r($bdd->errorInfo()));
$reponse_first->execute(array('derniere' => $time)); 
while($donnees_first = $reponse_first->fetch())
	{
	$id_map=$donnees_first['id_map'];
	$max_pokemons=$donnees_first['max_pokemons'];
	$nb_ajout=$donnees_first['nb_ajout'];
	$duree=$donnees_first['duree'];	
	$nb_pokemons_cases=0;
	$reponse = $bdd->prepare('SELECT COUNT(id) AS comptage FROM pokemons_map WHERE id_map=:id_map') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id_map' => $id_map));
	$donnees = $reponse->fetch();
	$nb_pokemons_cases=$donnees['comptage'];
	if($nb_pokemons_cases<$max_pokemons)
		{
		$nb_pokemons_manquants=$max_pokemons-$nb_pokemons;
		if($nb_pokemons_manquants>$nb_ajout){$nb_pokemons_manquants=$nb_ajout;}
		while($nb_pokemons_manquants>0)
			{
			$reponse = $bdd->prepare('SELECT proba FROM pokemons_map_proba WHERE id_map=:id_map AND num=1') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id_map' => $id_map));$donnees = $reponse->fetch();
			$proba_pokemon1=$donnees['proba'];
			$reponse = $bdd->prepare('SELECT proba FROM pokemons_map_proba WHERE id_map=:id_map AND num=2') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id_map' => $id_map));$donnees = $reponse->fetch();
			$proba_pokemon2=$proba_pokemon1+$donnees['proba'];
			$reponse = $bdd->prepare('SELECT proba FROM pokemons_map_proba WHERE id_map=:id_map AND num=3') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id_map' => $id_map));$donnees = $reponse->fetch();
			$proba_pokemon3=$proba_pokemon2+$donnees['proba'];
			$reponse = $bdd->prepare('SELECT proba FROM pokemons_map_proba WHERE id_map=:id_map AND num=4') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id_map' => $id_map));$donnees = $reponse->fetch();
			$proba_pokemon4=$proba_pokemon3+$donnees['proba'];
			$reponse = $bdd->prepare('SELECT proba FROM pokemons_map_proba WHERE id_map=:id_map AND num=5') or die(print_r($bdd->errorInfo()));$reponse->execute(array('id_map' => $id_map));$donnees = $reponse->fetch();
			$proba_pokemon5=$proba_pokemon4+$donnees['proba'];
			$proba_pokemons_selected=rand(1,$proba_pokemon5);
			if($proba_pokemons_selected<=$proba_pokemon1){$pokemons_selected=1;}elseif($proba_pokemons_selected<=$proba_pokemon2){$pokemons_selected=2;}elseif($proba_pokemons_selected<=$proba_pokemon3){$pokemons_selected=3;}elseif($proba_pokemons_selected<=$proba_pokemon4){$pokemons_selected=4;}elseif($proba_pokemons_selected<=$proba_pokemon5){$pokemons_selected=5;}
			$reponse = $bdd->prepare('SELECT id_pokemon, lvl_min, lvl_max FROM pokemons_map_proba WHERE id_map=:id_map AND num=:num') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id_map' => $id_map, 'num' => $pokemons_selected));
			$donnees = $reponse->fetch();
				$id_pokemon=$donnees['id_pokemon'];
				$lvl_min=$donnees['lvl_min'];
				$lvl_max=$donnees['lvl_max'];
			$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $id_pokemon));
			$donnees = $reponse->fetch();
			$type1=$donnees['type1'];
			$type2=$donnees['type2'];
			$id_pokedex_new=$donnees['id_pokedex'];
			$rand=rand(1,250);if($rand==1){$shiney=1;}else{$shiney=0;}
			if($donnees['sexe']=="Y"){$proba_sexe=rand(1,2); if($proba_sexe==1){$sexe="M";} elseif($proba_sexe==2){$sexe="F";}} elseif ($donnees['sexe']=="F"){$sexe="F";}elseif ($donnees['sexe']=="M"){$sexe="M";} else {$sexe="";}
			$lvl=rand($lvl_min, $lvl_max);
			$randpv=rand(0,$lvl);$randatt=rand(0,$lvl);$randdef=rand(0,$lvl);$randvit=rand(0,$lvl);$randattspe=rand(0,$lvl);$randdefspe=rand(0,$lvl);
			$pv=$donnees['pv']+$lvl*$donnees['bonus_pv']+$randpv;
			$att=$donnees['att']+$lvl*$donnees['bonus_att']+$randatt;
			$def=$donnees['def']+$lvl*$donnees['bonus_def']+$randdef;
			$vit=$donnees['vit']+$lvl*$donnees['bonus_vit']+$randvit;
			$attspe=$donnees['attspe']+$lvl*$donnees['bonus_attspe']+$randattspe;
			$defspe=$donnees['defspe']+$lvl*$donnees['bonus_defspe']+$randdefspe;
			$attaque1=0;$attaque2=0;$attaque3=0;$attaque4=0;$count1=0;$count2=0;$count3=0;$count4=0;
			while($attaque1==0 AND $count1<4)
				{
				$count_attaque=0;
				$reponse = $bdd->prepare('SELECT id FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
				while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;}
				$rand_attaque=rand(0,$count_attaque);
				$count_attaque=0;
				$reponse = $bdd->prepare('SELECT * FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
				while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;if($count_attaque==$rand_attaque){$attaque1=$donnees['id_attaque'];}}
				$count1=$count1+1;
				}
			while($attaque2==0 AND $count2<5 OR $count2<5 AND $attaque2==$attaque1)
				{
				$count_attaque=0;
				$reponse = $bdd->prepare('SELECT id FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
				while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;}
				$rand_attaque=rand(0,$count_attaque);
				$count_attaque=0;
				$reponse = $bdd->prepare('SELECT * FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
				while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;if($count_attaque==$rand_attaque){$attaque2=$donnees['id_attaque'];}}
				$count2=$count2+1;
				}
			if($attaque1==$attaque2){$attaque2=0;}
			while($attaque3==0 AND $count3<7 OR $attaque3==$attaque1 AND $count3<7 OR $attaque3==$attaque2 AND $count3<7)
				{
				$count_attaque=0;
				$reponse = $bdd->prepare('SELECT id FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
				while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;}
				$rand_attaque=rand(0,$count_attaque);
				$count_attaque=0;
				$reponse = $bdd->prepare('SELECT * FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
				while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;if($count_attaque==$rand_attaque){$attaque3=$donnees['id_attaque'];}}
				$count3=$count3+1;
				}
			if($attaque3==$attaque2 OR $attaque3==$attaque1){$attaque3=0;}
			while($attaque4==0 AND $count4<8 OR $attaque4==$attaque1 AND $count4<8 OR $attaque4==$attaque2 AND $count4<8 OR $attaque4==$attaque3 AND $count4<8)
				{
				$count_attaque=0;
				$reponse = $bdd->prepare('SELECT id FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
				while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;}
				$rand_attaque=rand(0,$count_attaque);
				$count_attaque=0;
				$reponse = $bdd->prepare('SELECT * FROM pokemons_apprentissage_attaques WHERE id_pokedex=:id_pokedex AND lvl<=:lvl ORDER BY lvl DESC') or die(print_r($bdd->errorInfo()));
				$reponse->execute(array('id_pokedex' => $id_pokedex_new, 'lvl' => $lvl));
				while($donnees = $reponse->fetch()){$count_attaque=$count_attaque+1;if($count_attaque==$rand_attaque){$attaque4=$donnees['id_attaque'];}}
				$count4=$count4+1;
				}
			if($attaque4==$attaque2 OR $attaque4==$attaque1 OR $attaque4==$attaque3){$attaque4=0;}
			
			
			$objet=0;
			$reponse = $bdd->prepare('SELECT * FROM pokemons_items_pokemons_sauvages WHERE lvl_inf<=:lvl_inf AND lvl_sup>=:lvl_sup') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('lvl_inf' => $lvl, 'lvl_sup' => $lvl));
			while($donnees = $reponse->fetch())
				{
				$permission=1;
				if($donnees['id_pokemon']!=0 AND $donnees['id_pokemon']!=$id_pokemon){$permission=0;}
				elseif($donnees['shiney']!=2 AND $donnees['shiney']!=$shiney){$permission=0;}
				elseif($donnees['type']!="tous" AND $donnees['type']!=$type1 AND $donnees['type']!=$type2){$permission=0;}
				elseif($donnees['sexe']!="Y" AND $donnees['sexe']!=$sexe){$permission=0;}			
				if($permission==1)
					{
					$rand_objet=rand(1,$donnees['proba']);
					if($rand_objet==1){$objet=$donnees['id_objet'];}
					}
				}
			
			$req = $bdd->prepare('INSERT INTO pokemons_map (id_map, id_pokemon, shiney, sexe, lvl, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4, combat, objet) 
				VALUES(:id_map, :id_pokemon, :shiney, :sexe, :lvl, :pv, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4, 0, :objet)') or die(print_r($bdd->errorInfo()));
				$req->execute(array(
				'id_map' => $id_map,
				'id_pokemon' => $id_pokemon,
				'shiney' => $shiney,
				'sexe' => $sexe,
				'lvl' => $lvl,
				'pv' => $pv,
				'pv_max' => $pv,
				'att' => $att,
				'def' => $def,
				'vit' => $vit,
				'attspe' => $attspe,	
				'defspe' => $defspe,
				'attaque1' => $attaque1,
				'attaque2' => $attaque2,
				'attaque3' => $attaque3,
				'attaque4' => $attaque4,
				'objet' => $objet	
				))or die(print_r($bdd->errorInfo()));	
				$nb_pokemons_manquants=$nb_pokemons_manquants-1;
				}
			}	
		$time_derniere=$time+$duree;
		$reponse = $bdd->prepare('UPDATE pokemons_map_cases SET derniere=:derniere WHERE id_map=:id_map') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('derniere' => $time_derniere, 'id_map' => $id_map));	
	}
							
?>
<?php include ("bas.php"); ?>