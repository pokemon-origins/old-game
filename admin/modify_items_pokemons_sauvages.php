<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
<?php
if($_SESSION['is_big_boss'] == true)
{
?>
 
 
<h2> Choisir les items tenus par les pokémons sauvages </h2>
<p>Ce menu vous permet de voir quelles sont les conventions actuellement utilisées en matière d'objets équipés sur les pokémons sauvages.<br />
il vous permet également de supprimer certaines de ces conventions, ou d'en ajouter de nouvelles<br />
C'est donc un menu fort utile pour les animations. Songez toutefois à bien communiquer les changements que vous y effectuez sur le forum, ainsi que la raison.</p>

<?php
if($_POST['action']=="supprimer") //SUPPRESSION
	{
	$reponse = $bdd->prepare('DELETE FROM pokemons_items_pokemons_sauvages WHERE id=:id') or die(print_r($bdd->errorInfo()));
			$reponse->execute(array('id' => $_POST['id'])); 
	echo '<span style="color:red;"><b>La convention a bien été supprimée. </b></span>';
	}
if($_POST['action']=="ajouter") //AJOUTER
	{
	if($_POST['proba']>0)
		{
		if($_POST['lvl_sup']==0 OR $_POST['lvl_sup']==""){$_POST['lvl_sup']=100;}
		$req = $bdd->prepare('INSERT INTO pokemons_items_pokemons_sauvages (id_pokemon, lvl_inf, lvl_sup, shiney, type, sexe, id_objet, proba) 
				VALUES(:id_pokemon, :lvl_inf, :lvl_sup, :shiney, :type, :sexe, :id_objet, :proba)') or die(print_r($bdd->errorInfo()));
				$req->execute(array(
				'id_pokemon' => $_POST['id_pokemon'],
				'lvl_inf' => $_POST['lvl_inf'],
				'lvl_sup' => $_POST['lvl_sup'],
				'shiney' => $_POST['shiney'],
				'type' => $_POST['type'],
				'sexe' => $_POST['sexe'],
				'id_objet' => $_POST['id_objet'],
				'proba' => $_POST['proba']
				))or die(print_r($bdd->errorInfo()));	
		}
	}
?>

<h3>Tableau des conventions actuellement en vigueur </h3>
<span style="font-size:10px;">*Les champs vides sont considérés comme non contraignants </span>	
<?php // Tableau des conventions
echo '<table id="profil" width="550px" cellpadding="2" cellspacing="2" style="text-align:center;" >';
echo '<colgroup><COL WIDTH=15%><COL WIDTH=6%><COL WIDTH=6%><COL WIDTH=6%><COL WIDTH=15%><COL WIDTH=6%><COL WIDTH=20%><COL WIDTH=10%><COL WIDTH=16%></COLGROUP>';
echo '<tr><td><b>Race</b></td><td><b>Lvl inf.</b></td><td><b>Lvl sup.</b></td><td><b>Shiney</b></td><td><b>Type</b></td><td><b>Sexe</b></td><td><b>Objet</b></td><td><b>Proba.</b></td><td><b>Supprimer</b></td></tr>';
$reponse = $bdd->query('SELECT * FROM pokemons_items_pokemons_sauvages ORDER BY id') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch())
	{
	//nom
	if($donnees['shiney']==2){$donnees['shiney']="tous";}elseif($donnees['shiney']==1){$donnees['shiney']="shiney";}elseif($donnees['shiney']==0){$donnees['shiney']="non shiney";}
	if($donnees['sexe']=="Y"){$donnees['sexe']="tous";}elseif($donnees['sexe']=="M"){$donnees['sexe']="mâle";}elseif($donnees['sexe']=="F"){$donnees['sexe']="femelle";}elseif($donnees['sexe']=="N"){$donnees['sexe']="assexué";}
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('id' => $donnees['id_pokemon']));  
	$donnees2 = $reponse2->fetch();
	if($donnees2['nom']==""){$donnees2['nom']="toutes";}
	//objet
	$reponse3 = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse3->execute(array('id' => $donnees['id_objet']));  
	$donnees3 = $reponse3->fetch();
	echo '<tr><td>'.$donnees2['nom'].'</td>';
	echo '<td>'.$donnees['lvl_inf'].'</td>';
	echo '<td>'.$donnees['lvl_sup'].'</td>';
	echo '<td>'.$donnees['shiney'].'</td>';
	echo '<td>'.$donnees['type'].'</td>';
	echo '<td>'.$donnees['sexe'].'</td>';
	echo '<td>'.$donnees3['nom'].'</td>';
	echo '<td> 1/'.$donnees['proba'].'</td>';
	echo '<td><form action="modify_items_pokemons_sauvages.php" method="post"><input type="hidden" name="id" value="'.$donnees['id'].'" /><input type="hidden" name="action"  value="supprimer" /><input type="submit" value="Supprimer" /></form></td>';
	echo'</tr>';
	}
echo '</table>';
?>



<h3>Ajouter une convention </h3>
<form action="modify_items_pokemons_sauvages.php" method="post">
Race de pokémon
<select name="id_pokemon">
<option value="">Toutes</option>
<?php
$reponse = $bdd->query('SELECT * FROM pokemons_base_pokemons ORDER BY id_pokedex') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].'</option>';
	}
?>
</select><br />	
Lvl inf.:<input type="text" name="lvl_inf" size="4"><br />
Lvl sup.:<input type="text" name="lvl_sup" size="4"><br />
Shiney : <select name="shiney"><option value="2">peu importe</option><option value="0">Non</option><option value="1">Oui</option></select><br />
Sexe : <select name="sexe"><option value="Y">Tous</option><option value="F">Femelle</option><option value="M">Mâle</option><option value="">assexué</option></select><br />
Type : <select name="type">
		  <option value="tous">Tous</option>
		  <option value="acier">acier</option>
		  <option value="combat">combat</option>
		  <option value="dragon">dragon</option>
		  <option value="eau">eau</option>
		  <option value="electrique">electrique</option>
		  <option value="feu">feu</option>
		  <option value="glace">glace</option>
		  <option value="insecte">insecte</option>
		  <option value="normal">normal</option>
		  <option value="plante">plante</option>
		  <option value="poison">poison</option>
		  <option value="psy">psy</option>
		  <option value="roche">roche</option>
		  <option value="sol">sol</option>
		  <option value="spectre">spectre</option>
		   <option value="tenebre">tenebre</option>
		  <option value="vol">vol</option>
		</select><br />
Objet tenu : <select name="id_objet">
<?php
$reponse = $bdd->query('SELECT * FROM pokemons_base_items WHERE tenir=1') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].'</option>';
	}
?>
</select><br />	
Proba : 1 chance sur <input type="text" name="proba" size="4"><br />
<input type="hidden" name="action"  value="ajouter" />
<input type="submit" value="Ajouter" />
</form>	
<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>	
   

 

<?php include ("bas.php"); ?>
