<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
<?php
if($_SESSION['is_admin'] == true)
{
?>
 
 
<h2> Surveiller les joueurs </h2>
<p>Ce menu vous permet de voir certaines informations sur les joueurs<br />
Il ne vous permet par contre pas de les modifier. <br />
Cliquez sur le pseudo d'un joueur pour voir plus de détails sur lui.</p>

<?php

?>


<b>Voir la liste des joueurs triés par :</b><br />
- <a href="survey_players.php?tri=argent">Pokédollars</a><br />
- <a href="survey_players.php?tri=ors">Ors</a><br />
- <a href="survey_players.php?tri=score">Score</a><br />
- <a href="survey_players.php?tri=pseudo">Pseudo</a><br />
<br />	
	

	
<?php //TRI
if(isset($_GET['tri']))
{
echo '<table id="profil" width="550px" cellpadding="2" cellspacing="2" style="text-align:center;" >';
echo '<colgroup><COL WIDTH=5%><COL WIDTH=20%><COL WIDTH=15%><COL WIDTH=15%><COL WIDTH=15%><COL WIDTH=15%><COL WIDTH=15%></COLGROUP>';
echo '<tr><td><b>ID</b></td><td><b>Pseudo</b></td><td><b>Argent</b></td><td><b>or</b></td><td><b>Score</b></td><td><b>Nb pokémons</b></td><td><b>Nb pok. shiney</b></td></tr>';
if($_GET['tri']=="argent"){$reponse = $bdd->query('SELECT pseudo, ors, id, score_total, pokedollar FROM pokemons_membres ORDER BY pokedollar DESC LIMIT 0,200') or die(print_r($bdd->errorInfo()));}
if($_GET['tri']=="ors"){$reponse = $bdd->query('SELECT pseudo, ors, id, score_total, pokedollar FROM pokemons_membres ORDER BY ors DESC LIMIT 0,200') or die(print_r($bdd->errorInfo()));}
if($_GET['tri']=="score"){$reponse = $bdd->query('SELECT pseudo, ors, id, score_total, pokedollar FROM pokemons_membres ORDER BY score_total DESC LIMIT 0,200') or die(print_r($bdd->errorInfo()));}
if($_GET['tri']=="pseudo"){$reponse = $bdd->query('SELECT pseudo, ors, id, score_total, pokedollar FROM pokemons_membres ORDER BY pseudo DESC LIMIT 0,200') or die(print_r($bdd->errorInfo()));}
while($donnees = $reponse->fetch())
	{
	$nb_pokemons=0;
	$nb_pokemons_shiney=0;
        $reponse2 = $bdd->prepare('SELECT COUNT(id) AS nb FROM pokemons_liste_pokemons WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse2->execute(array('pseudo' => $donnees['pseudo']));
	$donnees2 = $reponse2->fetch();
        $nb_pokemons=$donnees2['nb'];
        $reponse3 = $bdd->prepare('SELECT COUNT(id) AS nb FROM pokemons_liste_pokemons WHERE pseudo=:pseudo AND shiney=1') or die(print_r($bdd->errorInfo()));
	$reponse3->execute(array('pseudo' => $donnees['pseudo']));
	$donnees3 = $reponse3->fetch();
        $nb_pokemons_shiney=$donnees3['nb'];
	echo '<tr><td>'.$donnees['id'].'</td><td><a href="survey_players.php?player='.$donnees['pseudo'].'">'.$donnees['pseudo'].'</a></td>';
	echo '<td>'.$donnees['pokedollar'].'</td><td>'.$donnees['ors'].'</td><td>'.$donnees['score_total'].'</td>';
	echo '<td>'.$nb_pokemons.'</td><td>'.$nb_pokemons_shiney.'</td></tr>';
	}
echo '</table>';
}
?>

<?php //VOIR JOUEUR
if(isset($_GET['player']))
{

$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_GET['player']));
$donnees = $reponse->fetch();
$id_profil=$donnees['id'];
$description_profil=$donnees['description'];
$score_total=$donnees['score_total'];
$score_pvp=$donnees['score_pvp'];
$accepte_pvp=$donnees['pvp'];
$argent=$donnees['pokedollar'];
$ors=$donnees['ors'];
$bds=$donnees['bds'];
$mine=$donnees['mine'];
$depenses_ors=$donnees['depenses_ors'];
$depenses_pokedollars=$donnees['depenses_pokedollars'];
$id_joueur=$donnees['id'];
$reponse4 = $bdd->prepare('SELECT * FROM pokemons_quetes_principales WHERE numero=:numero AND etape=:etape') or die(print_r($bdd->errorInfo()));
$reponse4->execute(array('numero' => $donnees['quete_principale'], 'etape' => $donnees['quete_principale_etape']));  
$donnees4 = $reponse4->fetch();
$id_quete_actuelle=$donnees4['id'];
?>

<h1> Profil de <?php echo $_GET['player']; ?> </h1> 


<table id="profil" width="533px" cellpadding="2" cellspacing="2" style="text-align:center;" >
<colgroup><COL WIDTH=35%><COL WIDTH=65%></COLGROUP>
<tr> <th> Avatar </th> <th> Description </th> </tr>
<tr> <td> 
<?php //image du joueur
$chemin = '../images/avatars/'.$id_profil.'.jpg';
if(file_exists($chemin)){echo '<img src="../images/avatars/'.$id_profil.'.jpg" style="border:0;"/><br />';}
else {echo '<img src="../images/avatars/auto.gif" style="padding-left:7px;border:0;"/><br />';}
?>
</td><td> <?php echo nl2br($description_profil); ?> </td></tr>
<tr><th> Score </th> <th> Badges </th></tr>
<tr><td> Score : <?php echo $score_total; ?> <br />  
PvP : <?php echo $score_pvp; ?>   <br />  
argent : <?php echo $argent; ?> $   <br />  
Ors : <?php echo $ors; ?> <br />
Capacité max BDS : <?php echo $bds; ?> <br />
Or payé à la mine : <?php echo $mine; ?> <br />
dépense au shop (or) : <?php echo $depenses_ors; ?> <br />
dépense au shop ($) : <?php echo $depenses_pokedollars; ?> <br />
ID joueur : <?php echo $id_joueur; ?>   <br />  

</td><td>

Kanto : <br />
<?php if($id_quete_actuelle>21){?> <img src="../images/badges/roche_on.png" width="40px" height="40px" style="border:0;" /> <?php } else { ?> <img src="images/badges/roche_off.png" width="40px" height="40px" style="border:0;" /> <?php } ?>
<?php if($id_quete_actuelle>34){?> <img src="../images/badges/cascade_on.png" width="40px" height="40px" style="border:0;" /> <?php } else { ?> <img src="images/badges/cascade_off.png" width="40px" height="40px" style="border:0;" /> <?php } ?>
<?php if($id_quete_actuelle>50){?> <img src="../images/badges/foudre_on.png" width="40px" height="40px" style="border:0;" /> <?php } else { ?> <img src="images/badges/foudre_off.png" width="40px" height="40px" style="border:0;" /> <?php } ?>
<?php if($id_quete_actuelle>61){?> <img src="../images/badges/marais_on.png" width="40px" height="40px" style="border:0;" /> <?php } else { ?> <img src="images/badges/marais_off.png" width="40px" height="40px" style="border:0;" /> <?php } ?>
<br />
<?php if($id_quete_actuelle>68){?> <img src="../images/badges/prisme_on.png" width="40px" height="40px" style="border:0;" /> <?php } else { ?> <img src="images/badges/prisme_off.png" width="40px" height="40px" style="border:0;" /> <?php } ?>
<?php if($id_quete_actuelle>69){?> <img src="../images/badges/ame_on.png" width="40px" height="40px" style="border:0;" /> <?php } else { ?> <img src="images/badges/ame_off.png" width="40px" height="40px" style="border:0;" /> <?php } ?>
<?php if($id_quete_actuelle>83){?> <img src="../images/badges/terre_on.png" width="40px" height="40px" style="border:0;" /> <?php } else { ?> <img src="images/badges/terre_off.png" width="40px" height="40px" style="border:0;" /> <?php } ?>
<?php if($id_quete_actuelle>84){?> <img src="../images/badges/volcan_on.png" width="40px" height="40px" style="border:0;" /> <?php } else { ?> <img src="images/badges/volcan_off.png" width="40px" height="40px" style="border:0;" /> <?php } ?>
<br />
<?php if($id_quete_actuelle>103){?> <img src="../images/badges/conseil4_on.png" width="40px" height="40px" style="border:0;" /> <?php } else { ?> <img src="images/badges/conseil4_off.png" width="40px" height="40px" style="border:0;" /> <?php } ?>

</td></tr>
</table>
<br />

<div id="profil_pokemons">
<table Width="533px" id="profil" cellpadding="2" cellspacing="2" style="text-align:center;">
<tr> <th>Pokémons </th> <th> Noms </th> <th> Sexe </th> <th> Lvl </th></tr>
<?php //pokémons du joueur
$reponse = $bdd->prepare('SELECT id_pokemon, shiney, sexe, lvl FROM pokemons_liste_pokemons WHERE pseudo=:pseudo ORDER BY lvl DESC ') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_GET['player']));
while($donnees = $reponse->fetch())
	{
	$id_pokemon_profil=$donnees['id_pokemon'];
	$shiney_pokemon_profil=$donnees['shiney'];
	$sexe_pokemon_profil=$donnees['sexe'];
	$lvl_pokemon_profil=$donnees['lvl'];
	$reponse2 = $bdd->prepare('SELECT nom, id_pokedex FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $id_pokemon_profil));
		$donnees2 = $reponse2->fetch();
	$id_pokedex_pokemon_profil=$donnees2['id_pokedex'];
	$nom_pokemon_profil=$donnees2['nom'];
	?>
<tr> 
	 <?php
	 if($lvl_pokemon_profil!=0) 
		{
		?>
		<td> <img src="../images/pokemons/<?php if($shiney_pokemon_profil==1){echo 'shiney/';} echo $id_pokedex_pokemon_profil; ?>.gif" height="60px" style="border-style:none;"/></td>
		<td> <?php echo $nom_pokemon_profil; ?> </td>
		<td>   <img src="../images/<?php if($sexe_pokemon_profil=="M"){echo 'male';} elseif($sexe_pokemon_profil=="F"){echo 'femelle';} else {echo 'nosexe';} ?>.png"> </td>	 
		<td> <?php echo $lvl_pokemon_profil; ?> </td>
		<?php
		}
	else
		{
		?>
		<td> <img src="../images/oeuf.gif" height="60px" style="border-style:none;"/></td>
		<td> Oeuf </td>
		<td></td><td></td>
		<?php
		}
		?>
	 
</tr>
	
	<?php
	}
	?>
</table>
</div>
	
<?php //inventaire ?>

	<h2>Inventaire </h2>
	<b>Pokéballs</b>
	<table id="profil" width="533px" cellpadding="2" cellspacing="2">
	<colgroup><COL WIDTH=10%><COL WIDTH=7%><COL WIDTH=25%><COL WIDTH=58%></COLGROUP>
	<?php
	$reponse = $bdd->prepare('SELECT quantite, id_item FROM pokemons_inventaire WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_GET['player']));  
	while($donnees = $reponse->fetch())
		{
		$quantite_item=$donnees['quantite'];
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['id_item']));  
		$donnees2 = $reponse2->fetch();	
		if($quantite_item>0 AND $donnees2['shop_items']=="pokeballs")
			{
			echo '<tr><td>'.$quantite_item.'</td><td><img src="../images/shop/'.$donnees2['id'].'.png" /></td><td>'.$donnees2['nom'].'</td><td>'.$donnees2['description'].'</td></tr>';
			}
		}
	?>
	</table>
	<b>Objets</b>
	<table id="profil" width="533px" cellpadding="2" cellspacing="2">
	<colgroup><COL WIDTH=10%><COL WIDTH=7%><COL WIDTH=25%><COL WIDTH=58%></COLGROUP>
	<?php
	$reponse = $bdd->prepare('SELECT quantite, id_item FROM pokemons_inventaire WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_GET['player']));  
	while($donnees = $reponse->fetch())
		{
		$quantite_item=$donnees['quantite'];
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['id_item']));  
		$donnees2 = $reponse2->fetch();	
		if($quantite_item>0 AND $donnees2['shop_items']=="items")
			{
			echo '<tr><td>'.$quantite_item.'</td><td><img src="../images/shop/'.$donnees2['id'].'.png" /></td><td>'.$donnees2['nom'].'</td><td>'.$donnees2['description'].'</td></tr>';
			}
		}
	?>
	</table>
	<b>Pierres</b>
	<table id="profil" width="533px" cellpadding="2" cellspacing="2">
	<colgroup><COL WIDTH=10%><COL WIDTH=7%><COL WIDTH=25%><COL WIDTH=58%></COLGROUP>
	<?php
	$reponse = $bdd->prepare('SELECT quantite, id_item FROM pokemons_inventaire WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_GET['player']));  
	while($donnees = $reponse->fetch())
		{
		$quantite_item=$donnees['quantite'];
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['id_item']));  
		$donnees2 = $reponse2->fetch();	
		if($quantite_item>0 AND $donnees2['shop_items']=="pierres")
			{
			echo '<tr><td>'.$quantite_item.'</td><td><img src="../images/shop/'.$donnees2['id'].'.png" /></td><td>'.$donnees2['nom'].'</td><td>'.$donnees2['description'].'</td></tr>';
			}
		}
	?>
	</table>
	<b>CT/CS</b>
	<table  id="profil" width="533px" cellpadding="2" cellspacing="2">
	<colgroup><COL WIDTH=10%><COL WIDTH=7%><COL WIDTH=25%><COL WIDTH=58%></COLGROUP>
	<?php
	$reponse = $bdd->prepare('SELECT quantite, id_item FROM pokemons_inventaire WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_GET['player']));  
	while($donnees = $reponse->fetch())
		{
		$quantite_item=$donnees['quantite'];
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_items WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['id_item']));  
		$donnees2 = $reponse2->fetch();	
		if($quantite_item>0 AND $donnees2['shop_items']=="CT")
			{
			echo '<tr><td>'.$quantite_item.'</td><td><img src="../images/shop/'.$donnees2['id'].'.png" /></td><td>'.$donnees2['nom'].'</td><td>'.$donnees2['description'].'</td></tr>';
			}
		}
	?>
	</table>
	<?php
	

	 //récompenses 
	?>
	<h2>Récompenses obtenues </h2>
	<div id="profil_pokemons">
	<table id="votre_pokemon" width="533px" cellpadding="2" cellspacing="2" style="text-align:center">
	<colgroup><COL WIDTH=40%><COL WIDTH=60%></COLGROUP>
	<tr><th>Titres</th><th>Trophées</th></tr>
	<?php
	$reponse = $bdd->prepare('SELECT * FROM pokemons_recompenses_liste WHERE pseudo=:pseudo AND grade>0 ORDER BY id_recompense') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('pseudo' => $_GET['player']));  
	while($donnees = $reponse->fetch())
		{
		$reponse2 = $bdd->prepare('SELECT * FROM pokemons_recompenses WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['id_recompense']));  
		while($donnees2 = $reponse2->fetch())
		echo '<tr><td><b>'.$donnees2['nom'].'</b><br /> Grade '.$donnees['grade'].'</td><td><img src="../images/recompenses/'.$donnees['grade'].'.png" height="120px"/></td></tr>';
		}
	?>
	</table>
	</div>

<h2>Pokémons en pension :</h2>
<div width="550px" style="text-align:center;">
<table id="marche" width="535px" cellpadding="2" cellspacing="2">
<colgroup><COL WIDTH=20%><COL WIDTH=35%><COL WIDTH=15%><COL WIDTH=15%><COL WIDTH=15%></COLGROUP>
<tr> <th> Pokémons</th> <th> Nom </th>  <th> Lvl </th><th>XP gagné</th><th>Prix </th> </tr>
<?php //affichage des pokémons à vendre
$reponse = $bdd->prepare('SELECT * FROM pokemons_elevage WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('pseudo' => $_GET['player']));
while($donnees = $reponse->fetch())
    {
	$sexe=$donnees['sexe'];
	$reponse2 = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse2->execute(array('id' => $donnees['id_pokemon']));
		$donnees2 = $reponse2->fetch();
	$id_pokedex=$donnees2['id_pokedex'];
	$nom_pokemon=$donnees2['nom'];
	$time_now=time();
	$xp_gagne=$time_now-$donnees['quand']; $xp_gagne=$xp_gagne/864; $xp_gagne=floor($xp_gagne);
	?>
	<tr><td><img src="../images/pokemons/<?php if($donnees['shiney']==1){echo 'shiney/';} echo $id_pokedex; ?>.gif" height="60px" style="border-style:none;"/></td>
	<td><?php echo $nom_pokemon.'<img src="../images/';if($sexe=="M"){echo 'male';} elseif($sexe=="F"){echo 'femelle';} else {echo 'nosexe';} echo'.png" style="position:relative;bottom:-2px;left:0px;">'; ?></td> 
	<td><?php echo $donnees['lvl']; ?></td> <td><?php echo $xp_gagne; ?></td><td><?php echo $xp_gagne; ?>$</td>
	</tr>
	<?php
	}
?>
</table>	
</div>	
	
	

<h2> Ses messages reçus </h2>   
     	

<?php 	
//lecture du message
if (isset($_POST['lire']))
    {	
	$reponse = $bdd->prepare('SELECT * FROM pokemons_mails WHERE id=:id AND destinataire=:destinataire') or die(print_r($bdd->errorInfo()));
    $reponse->execute(array('id' => $_POST['id'],'destinataire' => $_GET['player']));
	$donnees = $reponse->fetch();
	echo ' Message de <b>'.htmlspecialchars($donnees['expediteur']).'</b> <br /> Titre : <b>'.htmlspecialchars($donnees['titre']).' </b><br /> <textarea name="message" rows="8" cols="45">'.htmlspecialchars($donnees['message']).'</textarea><br />'; 
	}
?>
<div id="messages_pokemons">
<table id="messages" width="535px" cellpadding="2" cellspacing="2">
<colgroup><COL WIDTH=10%><COL WIDTH=22%><COL WIDTH=50%><COL WIDTH=13%><COL WIDTH=5%></COLGROUP>
<tr> <th> Lu</th> <th> Expéditeur </th> <th> Objet du message </th> <th> Date </th> <th> Lire </th> </tr>
<?php // les mails reçus
$reponse = $bdd->prepare('SELECT * FROM pokemons_mails WHERE destinataire=:destinataire ORDER BY quand DESC') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('destinataire' => $_GET['player']));	   
while($donnees = $reponse->fetch())
    {
	echo '<tr> <td>'.htmlspecialchars($donnees['statut']).'</td> <td>'.htmlspecialchars($donnees['expediteur']).'</td> <td>'.htmlspecialchars($donnees['titre']).'</td> <td>'.$donnees['quand'].'</td> <td>
	<form action="survey_players.php?player='.$_GET['player'].'" method="post">                     	         
           <input type="hidden" name="id"  value="'.$donnees['id'].'" />	 
           <input type="hidden" name="lire"  value="lire" />            	   
           <input type="submit" value="Lire" />           
            </form></td></tr>';	
	}
?>
</table>	
</div>

<h2> Ses messages envoyés </h2>   
     	

<?php 	
//lecture du message
if (isset($_POST['lire2']))
    {	
	$reponse = $bdd->prepare('SELECT * FROM pokemons_mails WHERE id=:id AND expediteur=:expediteur') or die(print_r($bdd->errorInfo()));
    $reponse->execute(array('id' => $_POST['id'],'expediteur' => $_GET['player']));
	$donnees = $reponse->fetch();
	echo ' Message à <b>'.htmlspecialchars($donnees['destinataire']).'</b> <br /> Titre : <b>'.htmlspecialchars($donnees['titre']).' </b><br /> <textarea name="message" rows="8" cols="45">'.htmlspecialchars($donnees['message']).'</textarea><br />'; 
	}
?>
<div id="messages_pokemons">
<table id="messages" width="535px" cellpadding="2" cellspacing="2">
<colgroup><COL WIDTH=10%><COL WIDTH=22%><COL WIDTH=50%><COL WIDTH=13%><COL WIDTH=5%></COLGROUP>
<tr> <th> Lu</th> <th> Destinataire </th> <th> Objet du message </th> <th> Date </th> <th> Lire </th> </tr>
<?php // les mails reçus
$reponse = $bdd->prepare('SELECT * FROM pokemons_mails WHERE expediteur=:expediteur ORDER BY quand DESC') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('expediteur' => $_GET['player']));	   
while($donnees = $reponse->fetch())
    {
	echo '<tr> <td>'.htmlspecialchars($donnees['statut']).'</td> <td>'.htmlspecialchars($donnees['destinataire']).'</td> <td>'.htmlspecialchars($donnees['titre']).'</td> <td>'.$donnees['quand'].'</td> <td>
	<form action="survey_players.php?player='.$_GET['player'].'" method="post">                     	         
           <input type="hidden" name="id"  value="'.$donnees['id'].'" />	 
           <input type="hidden" name="lire2"  value="lire" />            	   
           <input type="submit" value="Lire" />           
            </form></td></tr>';	
	}
?>
</table>	
</div>



	
	
	
	
	
	
<?php	
} //fin du if isset player
?>	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>	
   

 

<?php include ("bas.php"); ?>
