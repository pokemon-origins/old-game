<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
<?php
if($_SESSION['is_animateur'] == true)
{
?>
 
 
<h2> Surveiller les items </h2>
<p>Ce menu vous permet de voir certaines informations utiles pour les animations avec des items<br />
Choisissez l'objet dont vous désirez voir le classement. Les joueurs sont triés en fonction de la quantité possédée dans l'inventaire (hors objet porté ou marché donc)<br />
Le lien pour surveiller les joueurs ne fonctionnent que SI on a également des droits d'administrateur.</p>

<?php

?>


<b>Pour quel objet désirez-vous voir le classement :</b><br />
<form action="survey_items.php" method="post">                     	         
<select name="objet">
<?php
$reponse = $bdd->query('SELECT * FROM pokemons_base_items WHERE (tenir=1 OR shop_items="animation")') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['id'].'"'; if($_POST['objet']==$donnees['id']){echo 'selected="selected"';} echo'>'.$donnees['nom'].'</option>';
	}
?>
</select>	 
<input type="hidden" name="action"  value="voir" /> 	
<input type="submit" value="Choisir" />           
</form>
<br />	
	

	
<?php //CLASSEMENT
if($_POST['action']=="voir")
{
echo '<table id="profil" width="550px" cellpadding="2" cellspacing="2" style="text-align:center;" >';
echo '<colgroup><COL WIDTH=5%><COL WIDTH=20%><COL WIDTH=15%><COL WIDTH=15%><COL WIDTH=15%><COL WIDTH=15%><COL WIDTH=15%></COLGROUP>';
echo '<tr><td><b>Pseudo</b></td><td><b>Quantité inventaire</b></td></tr>';
$reponse = $bdd->prepare('SELECT * FROM pokemons_inventaire WHERE id_item=:id_item ORDER BY quantite DESC') or die(print_r($bdd->errorInfo()));
$reponse->execute(array('id_item' => $_POST['objet']));
while($donnees = $reponse->fetch())
	{
	echo '<tr><td><a href="survey_players.php?player='.$donnees['pseudo'].'">'.$donnees['pseudo'].'</a></td>';
	echo '<td>'.$donnees['quantite'].'</td></tr>';
	}
echo '</table>';
}
?>


	
<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>	
   

 

<?php include ("bas.php"); ?>
