<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
<?php
if($_SESSION['is_animateur'] == true)
{
?>
 
 
<h2> Ajout d'un PNJ (quête secondaire ou PNJ classique) </h2>
<p>Ce menu vous permet d'ajouter/et de modifier tous les PNJ, mis à part ceux de la quête principale.<br />
Vous pouvez modifier les textes, positions, récompenses, etc... de ces PNJ. Pour les récompenses, elles fonctionnent de la même façon que dans l'option "faire un cadeau" quant aux items. <br />
Pour ajouter un PNJ, cliquer sur le type de PNJ que vous désirez (par exemple PNJ duel), cliquer sur "ajouter un PNJ", remplissez toutes les informations SAUF la position. Lorsque tout est complet (sans oublier l'image et y compris les pokémons pour les PNJ duel), modifier la position. Votre PNJ apparait alors sur la case désirée. Vérifiez alors que tout fonctionne parfaitement!<br />
Note : pour les quêtes "kill", il s'agit de tuer un certains nombre de pokémons déterminés. Si aucun pokémon n'est sélectionné, il s'agit de tuer X pokémons, quelque soit la race.<br />
Note 2 : limite inf et limite sup sont les limites de visibilité pour le joueur. Si la limite inf est de 5, seuls les joueurs étant à la quête d'ID 5 ou + pourront voir cette quête. Pour connaitre l'ID de chaque quête, rendez-vous dans votre journal de quête et cliquez sur le lien de la quête. L'ID est affichée dans le lien. </p>

<?php
if($_POST['action']=="modifier") //modifier un PNJ
	{
	if($_POST['genre']=="duel")
		 {
		 $reponseX = $bdd->prepare('UPDATE pokemons_quetes_secondaires SET pos_hor=:pos_hor, pos_ver=:pos_ver, qui=:qui, difficulte=:difficulte, repetitive=:repetitive, condition_reussite=:condition_reussite, intro=:intro, complet=:complet, reussite=:reussite, pas_reussi=:pas_reussi, genre_recompense=:genre_recompense, recompense=:recompense, nb_recompense=:nb_recompense, limite_inf=:limite_inf, limite_sup=:limite_sup WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponseX->execute(array('pos_hor'=>$_POST['pos_hor'], 'pos_ver'=>$_POST['pos_ver'], 'qui'=>$_POST['qui'], 'difficulte'=>$_POST['difficulte'], 'repetitive'=>$_POST['repetitive'], 'condition_reussite'=>$_POST['condition_reussite'], 'intro'=>stripslashes($_POST['intro']), 'complet'=>stripslashes($_POST['complet']), 'reussite'=>stripslashes($_POST['reussite']), 'pas_reussi'=>stripslashes($_POST['pas_reussi']), 'genre_recompense'=>$_POST['genre_recompense'], 'recompense'=>$_POST['recompense'], 'nb_recompense'=>$_POST['nb_recompense'],'limite_inf'=>$_POST['limite_inf'], 'limite_sup'=>$_POST['limite_sup'], 'id' => $_POST['id']));
		 echo '<b>Modifications bien effectuées.</b><br /><br />';
		 $_POST['action']="voir";
		 }
	if($_POST['genre']=="echange")
		 {
		 $reponseX = $bdd->prepare('UPDATE pokemons_quetes_secondaires SET pos_hor=:pos_hor, pos_ver=:pos_ver, qui=:qui, difficulte=:difficulte, repetitive=:repetitive, what=:what, condition_reussite=:condition_reussite, condition_reussite2=:condition_reussite2, intro=:intro, complet=:complet, reussite=:reussite, pas_reussi=:pas_reussi, genre_recompense=:genre_recompense, recompense=:recompense, nb_recompense=:nb_recompense, limite_inf=:limite_inf, limite_sup=:limite_sup WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponseX->execute(array('pos_hor'=>$_POST['pos_hor'], 'pos_ver'=>$_POST['pos_ver'], 'qui'=>$_POST['qui'], 'difficulte'=>$_POST['difficulte'], 'repetitive'=>$_POST['repetitive'], 'what'=>$_POST['what'], 'condition_reussite'=>$_POST['condition_reussite'], 'condition_reussite2'=>$_POST['condition_reussite2'], 'intro'=>stripslashes($_POST['intro']), 'complet'=>stripslashes($_POST['complet']), 'reussite'=>stripslashes($_POST['reussite']), 'pas_reussi'=>stripslashes($_POST['pas_reussi']), 'genre_recompense'=>$_POST['genre_recompense'], 'recompense'=>$_POST['recompense'], 'nb_recompense'=>$_POST['nb_recompense'],'limite_inf'=>$_POST['limite_inf'], 'limite_sup'=>$_POST['limite_sup'], 'id' => $_POST['id']));
		 echo '<b>Modifications bien effectuées.</b><br /><br />';
		 $_POST['action']="voir";
		 }
	if($_POST['genre']=="kill")
		 {
		 $reponseX = $bdd->prepare('UPDATE pokemons_quetes_secondaires SET pos_hor=:pos_hor, pos_ver=:pos_ver, qui=:qui, difficulte=:difficulte, repetitive=:repetitive, what=:what, condition_reussite=:condition_reussite, condition_reussite2=:condition_reussite2, nb_reussite=:nb_reussite, intro=:intro, complet=:complet, reussite=:reussite, pas_reussi=:pas_reussi, genre_recompense=:genre_recompense, recompense=:recompense, nb_recompense=:nb_recompense, limite_inf=:limite_inf, limite_sup=:limite_sup WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponseX->execute(array('pos_hor'=>$_POST['pos_hor'], 'pos_ver'=>$_POST['pos_ver'], 'qui'=>$_POST['qui'], 'difficulte'=>$_POST['difficulte'], 'repetitive'=>$_POST['repetitive'], 'what'=>$_POST['what'], 'condition_reussite'=>$_POST['condition_reussite'], 'condition_reussite2'=>$_POST['condition_reussite2'], 'nb_reussite'=>$_POST['nb_reussite'], 'intro'=>stripslashes($_POST['intro']), 'complet'=>stripslashes($_POST['complet']), 'reussite'=>stripslashes($_POST['reussite']), 'pas_reussi'=>stripslashes($_POST['pas_reussi']), 'genre_recompense'=>$_POST['genre_recompense'], 'recompense'=>$_POST['recompense'], 'nb_recompense'=>$_POST['nb_recompense'],'limite_inf'=>$_POST['limite_inf'], 'limite_sup'=>$_POST['limite_sup'], 'id' => $_POST['id']));
		 echo '<b>Modifications bien effectuées.</b><br /><br />';
		 $_POST['action']="voir";
		 }
	if($_POST['genre']=="normal")
		 {
		 $reponseX = $bdd->prepare('UPDATE pokemons_pnj SET pos_hor=:pos_hor, pos_ver=:pos_ver, qui=:qui, texte=:texte, limite_inf=:limite_inf, limite_sup=:limite_sup, id_quete=:id_quete WHERE id=:id') or die(print_r($bdd->errorInfo()));
					$reponseX->execute(array('pos_hor'=>$_POST['pos_hor'], 'pos_ver'=>$_POST['pos_ver'], 'qui'=>$_POST['qui'], 'texte'=>stripslashes($_POST['texte']),'limite_inf'=>$_POST['limite_inf'], 'limite_sup'=>$_POST['limite_sup'], 'id_quete'=>$_POST['id_quete'], 'id' => $_POST['id']));
		 echo '<b>Modifications bien effectuées.</b><br /><br />';
		 $_POST['action']="voir";
		 }
	$req = $bdd->prepare('INSERT INTO pokemons_survey_admin (pseudo, action, quand, a_qui, quoi) VALUES(:pseudo, "modification PNJ", now(),:a_qui, :quoi)') or die(print_r($bdd->errorInfo()));
	$req->execute(array(
                    'pseudo' => $_SESSION['pseudo'],
					'a_qui' => stripslashes($_POST['qui']),
					'quoi' => stripslashes($_POST['genre'])
					))or die(print_r($bdd->errorInfo()));
	}
if($_POST['action']=="modifier_image") //modifier l'image d'un PNJ
	{
	if($_POST['genre']=="normal")
		{
		$size=$_FILES['fichier']['size']; 
		$type=$_FILES['fichier']['type'];
		$tmp=$_FILES['fichier']['tmp_name'];
		$fichier=$_FILES['fichier']['name'];
		list($width,$height)=getimagesize($tmp);
		$lieu='../images/pnj/'.$_POST['id'].'.jpg';	
		move_uploaded_file($tmp,$lieu);	
		echo '<b>Votre image a bien été transférée!</b><br /><br />';
		$_POST['action']="voir";
		$req = $bdd->prepare('INSERT INTO pokemons_survey_admin (pseudo, action, quand, a_qui) VALUES(:pseudo, "modification image PNJ normal", now(),:a_qui)') or die(print_r($bdd->errorInfo()));
		$req->execute(array(
                    'pseudo' => $_SESSION['pseudo'],
					'a_qui' => stripslashes($_POST['id'])
					))or die(print_r($bdd->errorInfo()));
		}
	else
		{
		$size=$_FILES['fichier']['size']; 
		$type=$_FILES['fichier']['type'];
		$tmp=$_FILES['fichier']['tmp_name'];
		$fichier=$_FILES['fichier']['name'];
		list($width,$height)=getimagesize($tmp);
		$lieu='../images/quetes_secondaires/'.$_POST['id'].'.jpg';	
		move_uploaded_file($tmp,$lieu);	
		echo '<b>Votre image a bien été transférée!</b><br /><br />';
		$_POST['action']="voir";
		$req = $bdd->prepare('INSERT INTO pokemons_survey_admin (pseudo, action, quand, a_qui) VALUES(:pseudo, "modification image PNJ", now(),:a_qui)') or die(print_r($bdd->errorInfo()));
		$req->execute(array(
                    'pseudo' => $_SESSION['pseudo'],
					'a_qui' => stripslashes($_POST['id'])
					))or die(print_r($bdd->errorInfo()));
		}
	}
if($_POST['action']=="modifier_pokemons") //modifier/ajouter un pokémon
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete AND num_pokemon=:num_pokemon') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id_quete'=>$_POST['id_quete'], 'num_pokemon'=>$_POST['num_pokemon'])); 
	$donnees = $reponse->fetch();
	if(isset($donnees['id']))
		{
		$reponseX = $bdd->prepare('UPDATE pokemons_liste_pokemons_pnj SET id_pokemon=:id_pokemon, shiney=:shiney, sexe=:sexe, lvl=:lvl, pv=:pv, pv_max=:pv_max, att=:att, def=:def, vit=:vit, attspe=:attspe, defspe=:defspe, attaque1=:attaque1, attaque2=:attaque2, attaque3=:attaque3, attaque4=:attaque4 WHERE id_quete=:id_quete AND num_pokemon=:num_pokemon') or die(print_r($bdd->errorInfo()));
					$reponseX->execute(array('id_pokemon'=>$_POST['id_pokemon'], 'shiney'=>$_POST['shiney'], 'sexe'=>$_POST['sexe'], 'lvl'=>$_POST['lvl'], 'pv'=>$_POST['pv'],'pv_max'=>$_POST['pv_max'],'att'=>$_POST['att'],'def'=>$_POST['def'],'vit'=>$_POST['vit'],'attspe'=>$_POST['attspe'],'defspe'=>$_POST['defspe'],'attaque1'=>$_POST['attaque1'],'attaque2'=>$_POST['attaque2'],'attaque3'=>$_POST['attaque3'],'attaque4'=>$_POST['attaque4'], 'id_quete'=>$_POST['id_quete'], 'num_pokemon'=>$_POST['num_pokemon']));
		echo '<b>Modification bien effectuées.</b><br /><br />';
		}
	else
		{
		$req = $bdd->prepare('INSERT INTO pokemons_liste_pokemons_pnj (id_quete, num_pokemon, id_pokemon, shiney, sexe, lvl, pv, pv_max, att, def, vit, attspe, defspe, attaque1, attaque2, attaque3, attaque4) VALUES(:id_quete, :num_pokemon, :id_pokemon, :shiney, :sexe, :lvl, :pv, :pv_max, :att, :def, :vit, :attspe, :defspe, :attaque1, :attaque2, :attaque3, :attaque4)') or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_quete'=>$_POST['id_quete'], 'num_pokemon'=>$_POST['num_pokemon'], 'id_pokemon'=>$_POST['id_pokemon'], 'shiney'=>$_POST['shiney'], 'sexe'=>$_POST['sexe'], 'lvl'=>$_POST['lvl'], 'pv'=>$_POST['pv'],'pv_max'=>$_POST['pv_max'],'att'=>$_POST['att'],'def'=>$_POST['def'],'vit'=>$_POST['vit'],'attspe'=>$_POST['attspe'],'defspe'=>$_POST['defspe'],'attaque1'=>$_POST['attaque1'],'attaque2'=>$_POST['attaque2'],'attaque3'=>$_POST['attaque3'],'attaque4'=>$_POST['attaque4']))or die(print_r($bdd->errorInfo()));
		echo '<b>Ajout bien effectuées.</b><br /><br />';
		}
	$req = $bdd->prepare('INSERT INTO pokemons_survey_admin (pseudo, action, quand, a_qui) VALUES(:pseudo, "Modifier un pokémon", now(),:a_qui)') or die(print_r($bdd->errorInfo()));
	$req->execute(array(
                    'pseudo' => $_SESSION['pseudo'],
					'a_qui' => stripslashes($_POST['id_quete'])
					))or die(print_r($bdd->errorInfo()));
	$_POST['action']="voir_pokemons";
	}
if($_POST['action']=="ajouter_pnj") //ajouter un pnj
	{
	if($_POST['genre']=="duel")
		{
		$reponse = $bdd->query('SELECT id FROM pokemons_quetes_secondaires ORDER BY id desc LIMIT 0,1') or die(print_r($bdd->errorInfo()));
		$donnees = $reponse->fetch();
		$what=$donnees['id']+1;
		$what='QS'.$what;
		$req = $bdd->prepare('INSERT INTO pokemons_quetes_secondaires (genre, what) VALUES(:genre, :what)') or die(print_r($bdd->errorInfo()));
		$req->execute(array(
                    'genre' => $_POST['genre'],
					'what' => $what
					))or die(print_r($bdd->errorInfo()));
		$_GET['see']="duel";
		}
	if($_POST['genre']=="echange")
		{
		$req = $bdd->prepare('INSERT INTO pokemons_quetes_secondaires (genre) VALUES(:genre)') or die(print_r($bdd->errorInfo()));
		$req->execute(array(
                    'genre' => $_POST['genre']
					))or die(print_r($bdd->errorInfo()));
		$_GET['see']="echange";
		}
	if($_POST['genre']=="kill")
		{
		$req = $bdd->prepare('INSERT INTO pokemons_quetes_secondaires (genre) VALUES(:genre)') or die(print_r($bdd->errorInfo()));
		$req->execute(array(
                    'genre' => $_POST['genre']
					))or die(print_r($bdd->errorInfo()));
		$_GET['see']="kill";
		}
	if($_POST['genre']=="normal")
		{
		$req = $bdd->query('INSERT INTO pokemons_pnj () VALUES()') or die(print_r($bdd->errorInfo()));
		$_GET['see']="normal";
		}
	$req = $bdd->prepare('INSERT INTO pokemons_survey_admin (pseudo, action, quand, a_qui) VALUES(:pseudo, "ajouter un pnj", now(),:quoi)') or die(print_r($bdd->errorInfo()));
	$req->execute(array(
                    'pseudo' => $_SESSION['pseudo'],
					'quoi' => $_POST['genre']
					))or die(print_r($bdd->errorInfo()));
	}
?>


<b>Voir les PNJ existants :</b><br />
- <a href="pokemons_add_pnj.php?see=duel">PNJ Duels</a><br />
- <a href="pokemons_add_pnj.php?see=echange">PNJ échange</a><br />
- <a href="pokemons_add_pnj.php?see=kill">PNJ kills (tue X pokémons)</a><br />
- <a href="pokemons_add_pnj.php?see=normal">PNJ normal (juste blabla)</a><br />
<br />	
	

	
<?php //SEE
if($_GET['see']=="duel")
	{
	echo '<table id="profil" width="533px" cellpadding="2" cellspacing="2" style="text-align:center;" >';
	echo '<colgroup><COL WIDTH=5%><COL WIDTH=20%><COL WIDTH=50%><COL WIDTH=5%><COL WIDTH=20%></COLGROUP>';
	echo '<tr><td><b>ID</b></td><td><b>Position</b></td><td><b>Nom</b></td><td><b>Points</b></td><td><b>Voir</b></td></tr>';
	$reponse = $bdd->query('SELECT * FROM pokemons_quetes_secondaires WHERE genre="duel"') or die(print_r($bdd->errorInfo()));
	while($donnees = $reponse->fetch())
		{
		echo '<tr><td>'.$donnees['id'].'</td><td>'.$donnees['pos_ver'].'/'.$donnees['pos_hor'].'</td><td>'.$donnees['qui'].'</td><td>'.$donnees['difficulte'].'</td><td><form method="post" action="pokemons_add_pnj.php"><input type="hidden" name="action" value="voir"><input type="hidden" name="genre" value="'.$donnees['genre'].'"><input type="hidden" name="id" value="'.$donnees['id'].'"><input type="submit" value="Voir/Modifier" /></form></td></tr>';
		}
	echo '</table>';
	?>
	<br />
	<form method="post" action="pokemons_add_pnj.php">
	<INPUT TYPE="hidden" name="action" value="ajouter_pnj">
	<INPUT TYPE="hidden" name="genre" value="<?php echo $_GET['see'];?>">
	<INPUT TYPE="submit" VALUE="Ajouter un PNJ duel"> 
	</form>
	<?php
	}
if($_GET['see']=="echange")
	{
	echo '<table id="profil" width="533px" cellpadding="2" cellspacing="2" style="text-align:center;" >';
	echo '<colgroup><COL WIDTH=5%><COL WIDTH=10%><COL WIDTH=30%><COL WIDTH=30%><COL WIDTH=5%><COL WIDTH=20%></COLGROUP>';
	echo '<tr><td><b>ID</b></td><td><b>Position</b></td><td><b>Nom</b></td><td><b>Pokémon</b></td><td><b>Points</b></td><td><b>Voir</b></td></tr>';
	$reponse = $bdd->query('SELECT * FROM pokemons_quetes_secondaires WHERE genre="echange"') or die(print_r($bdd->errorInfo()));
	while($donnees = $reponse->fetch())
		{
		echo '<tr><td>'.$donnees['id'].'</td><td>'.$donnees['pos_ver'].'/'.$donnees['pos_hor'].'</td><td>'.$donnees['qui'].'</td><td>'.$donnees['what'].'</td><td>'.$donnees['difficulte'].'</td><td><form method="post" action="pokemons_add_pnj.php"><input type="hidden" name="action" value="voir"><input type="hidden" name="genre" value="'.$donnees['genre'].'"><input type="hidden" name="id" value="'.$donnees['id'].'"><input type="submit" value="Voir/Modifier" /></form></td></tr>';
		}
	echo '</table>';
	?>
	<br />
	<form method="post" action="pokemons_add_pnj.php">
	<INPUT TYPE="hidden" name="action" value="ajouter_pnj">
	<INPUT TYPE="hidden" name="genre" value="<?php echo $_GET['see'];?>">
	<INPUT TYPE="submit" VALUE="Ajouter un PNJ echange"> 
	</form>
	<?php
	}
if($_GET['see']=="kill")
	{
	echo '<table id="profil" width="533px" cellpadding="2" cellspacing="2" style="text-align:center;" >';
	echo '<colgroup><COL WIDTH=5%><COL WIDTH=10%><COL WIDTH=30%><COL WIDTH=30%><COL WIDTH=5%><COL WIDTH=20%></COLGROUP>';
	echo '<tr><td><b>ID</b></td><td><b>Position</b></td><td><b>Nom</b></td><td><b>Pokémon</b></td><td><b>Points</b></td><td><b>Voir</b></td></tr>';
	$reponse = $bdd->query('SELECT * FROM pokemons_quetes_secondaires WHERE genre="kill"') or die(print_r($bdd->errorInfo()));
	while($donnees = $reponse->fetch())
		{
		echo '<tr><td>'.$donnees['id'].'</td><td>'.$donnees['pos_ver'].'/'.$donnees['pos_hor'].'</td><td>'.$donnees['qui'].'</td><td>'.$donnees['nb_reussite'].' '.$donnees['what'].'</td><td>'.$donnees['difficulte'].'</td><td><form method="post" action="pokemons_add_pnj.php"><input type="hidden" name="action" value="voir"><input type="hidden" name="genre" value="'.$donnees['genre'].'"><input type="hidden" name="id" value="'.$donnees['id'].'"><input type="submit" value="Voir/Modifier" /></form></td></tr>';
		}
	echo '</table>';
	?>
	<br />
	<form method="post" action="pokemons_add_pnj.php">
	<INPUT TYPE="hidden" name="action" value="ajouter_pnj">
	<INPUT TYPE="hidden" name="genre" value="<?php echo $_GET['see'];?>">
	<INPUT TYPE="submit" VALUE="Ajouter un PNJ kill"> 
	</form>
	<?php
	}
if($_GET['see']=="normal")
	{
	echo '<table id="profil" width="533px" cellpadding="2" cellspacing="2" style="text-align:center;" >';
	echo '<colgroup><COL WIDTH=5%><COL WIDTH=15%><COL WIDTH=30%><COL WIDTH=10%><COL WIDTH=10%><COL WIDTH=7%><COL WIDTH=23%></COLGROUP>';
	echo '<tr><td><b>ID</b></td><td><b>Position</b></td><td><b>Nom</b></td><td><b>Limite inf</b></td><td><b>Limite sup</b></td><td><b>ID QP</b></td><td><b>Voir</b></td></tr>';
	$reponse = $bdd->query('SELECT * FROM pokemons_pnj') or die(print_r($bdd->errorInfo())); 
	while($donnees = $reponse->fetch())
		{
		echo '<tr><td>'.$donnees['id'].'</td><td>'.$donnees['pos_ver'].'/'.$donnees['pos_hor'].'</td><td>'.$donnees['qui'].'</td><td>'.$donnees['limite_inf'].'</td><td>'.$donnees['limite_sup'].'</td><td>'.$donnees['id_quete'].'</td><td><form method="post" action="pokemons_add_pnj.php"><input type="hidden" name="action" value="voir"><input type="hidden" name="genre" value="normal"><input type="hidden" name="id" value="'.$donnees['id'].'"><input type="submit" value="Voir/Modifier" /></form></td></tr>';
		}
	echo '</table>';
	?>
	<br />
	<form method="post" action="pokemons_add_pnj.php">
	<INPUT TYPE="hidden" name="action" value="ajouter_pnj">
	<INPUT TYPE="hidden" name="genre" value="<?php echo $_GET['see'];?>">
	<INPUT TYPE="submit" VALUE="Ajouter un PNJ normal"> 
	</form>
	<?php
	}
?>	
	
<?php //VOIR ou MODIFIER
if($_POST['action']=="voir")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_quetes_secondaires WHERE id=:id') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id'=>$_POST['id'])); 
	$donnees = $reponse->fetch();
	if($_POST['genre']=="duel")
		{
		?>
		<form method="post" action="pokemons_add_pnj.php">
		<b>Description : </b>
		Position :<input type="text" name="pos_ver" size="2" value="<?php echo $donnees['pos_ver']; ?>"> / <input type="text" name="pos_hor" size="2" value="<?php echo $donnees['pos_hor']; ?>"><br />
		Nom : <input type="text" name="qui" size="20" value="<?php echo $donnees['qui']; ?>"><br />
		Points : <input type="text" name="difficulte" size="2" value="<?php echo $donnees['difficulte']; ?>"><br />
		Repetable (/heure) : <input type="text" name="repetitive" size="2" value="<?php echo $donnees['repetitive']; ?>"><br />
		Nombre maximum de pokémons : <input type="text" name="condition_reussite" size="2" value="<?php echo $donnees['condition_reussite']; ?>"><br />
		Intro : <br />
		<textarea name="intro" rows="4" cols="55"><?php echo $donnees['intro']; ?></textarea> <br />
		Victoire : <br />
		<textarea name="reussite" rows="4" cols="55"><?php echo $donnees['reussite']; ?></textarea> <br />
		Defaite : <br />
		<textarea name="pas_reussi" rows="4" cols="55"><?php echo $donnees['pas_reussi']; ?></textarea> <br />
		Complet (plus dispo cette heure-ci) : <br />
		<textarea name="complet" rows="4" cols="55"><?php echo $donnees['complet']; ?></textarea><br />
		<b>Récompense :</b><br />
		Type : <select name="genre_recompense">
			<option value="item" <?php if($donnees['genre_recompense']=="item"){echo 'checked="checked"';}?>>Item</option>
			<option value="argent" <?php if($donnees['genre_recompense']=="argent"){echo 'selected="selected"';}?>>pokédollars</option>
		</select><br />
		Nombre (si $)/ID (si item): <input type="text" name="recompense" size="2" value="<?php echo $donnees['recompense']; ?>"><br />
		Nombre (si item)/ 1 (si $): <input type="text" name="nb_recompense" size="2" value="<?php echo $donnees['nb_recompense']; ?>"><br />
		<b>Limites : </b><br />
		Limite inférieure: <input type="text" name="limite_inf" size="2" value="<?php echo $donnees['limite_inf']; ?>"><br />
		Limite suppérieure: <input type="text" name="limite_sup" size="2" value="<?php echo $donnees['limite_sup']; ?>"><br />
		
		<input type="hidden" name="genre" value="<?php echo $donnees['genre']; ?>">
		<input type="hidden" name="id" value="<?php echo $donnees['id']; ?>">	
		<input type="hidden" name="action" value="modifier">
		<input type="submit" value="Modifier" />
		</form>
		<br />
		<b>Image : </b>	<br />
		<div id="cadre_membres_carte">
		<img src="../images/quetes_secondaires/<?php echo $_POST['id']; ?>.jpg" height="150px" style="border:0;"/>
		</div>
		<FORM ENCTYPE="multipart/form-data" ACTION="pokemons_add_pnj.php" METHOD="POST"> 
		<INPUT TYPE="hidden" name="action" value="modifier_image">
		<INPUT TYPE="hidden" name="id" value="<?php echo $_POST['id'];?>">
		<b>Ajouter/Modifier l'image </b>(uniquement .jpg) : <br /><INPUT NAME="fichier" TYPE="file"> 
		<INPUT TYPE="submit" VALUE="Envoyer l'image"> 
		</FORM> <br /><br />
		
		<div style="clear:both;"></div><br />
		<b>Voir/modifier ses pokémons</b>
		<form method="post" action="pokemons_add_pnj.php">
		<INPUT TYPE="hidden" name="action" value="voir_pokemons">
		<INPUT TYPE="hidden" name="id_quete" value="<?php echo $donnees['what'];?>">
		<INPUT TYPE="submit" VALUE="Voir"> 
		</form>
		<?php
		}
	if($_POST['genre']=="echange")
		{
		?>
		<form method="post" action="pokemons_add_pnj.php">
		<b>Description : </b>
		Position :<input type="text" name="pos_ver" size="2" value="<?php echo $donnees['pos_ver']; ?>"> / <input type="text" name="pos_hor" size="2" value="<?php echo $donnees['pos_hor']; ?>"><br />
		Nom : <input type="text" name="qui" size="20" value="<?php echo $donnees['qui']; ?>"><br />
		Points : <input type="text" name="difficulte" size="2" value="<?php echo $donnees['difficulte']; ?>"><br />
		Repetable (/heure) : <input type="text" name="repetitive" size="2" value="<?php echo $donnees['repetitive']; ?>"><br />
		Pokémon : <select name="what">
		<option value="0">Aucun</option>
		<?php
		$reponse2 = $bdd->query('SELECT * FROM pokemons_base_pokemons ORDER BY id_pokedex') or die(print_r($bdd->errorInfo()));  
		while($donnees2 = $reponse2->fetch())
			{
			echo '<option value="'.$donnees2['nom'].'"'; if($donnees2['nom']==$donnees['what']){echo 'selected="selected"';} echo '">'.$donnees2['nom'].'</option>';
			}
		?>
		</select><br />
		Condition de réussite :<br />
		<select name="condition_reussite">
			<option value="">Aucun</option>
			<option value="shiney" <?php if($donnees['condition_reussite']=="shiney"){echo 'selected="selected"';}?>>Shiney</option>
			<option value="superieur" <?php if($donnees['condition_reussite']=="superieur"){echo 'selected="selected"';}?>>Niveau supérieur à...</option>
			<option value="inferieur" <?php if($donnees['condition_reussite']=="inferieur"){echo 'selected="selected"';}?>>Niveau inférieur à...</option>
		</select><br />
		Condition de réussite : suite (à complèter si la condition de réussite se termine par "..." <br />
		<input type="text" name="condition_reussite2" size="2" value="<?php echo $donnees['condition_reussite2']; ?>"><br />
		Intro : <br />
		<textarea name="intro" rows="4" cols="55"><?php echo $donnees['intro']; ?></textarea> <br />
		Réussi : <br />
		<textarea name="reussite" rows="4" cols="55"><?php echo $donnees['reussite']; ?></textarea> <br />
		Pas encore réussi mais activée : <br />
		<textarea name="pas_reussi" rows="4" cols="55"><?php echo $donnees['pas_reussi']; ?></textarea> <br />
		Complet (plus dispo cette heure-ci) : <br />
		<textarea name="complet" rows="4" cols="55"><?php echo $donnees['complet']; ?></textarea><br />
		<b>Récompense :</b><br />
		Type : <select name="genre_recompense">
			<option value="item" <?php if($donnees['genre_recompense']=="item"){echo 'checked="checked"';}?>>Item</option>
			<option value="argent" <?php if($donnees['genre_recompense']=="argent"){echo 'selected="selected"';}?>>pokédollars</option>
		</select><br />
		Nombre (si $)/ID (si item): <input type="text" name="recompense" size="2" value="<?php echo $donnees['recompense']; ?>"><br />
		Nombre (si item)/ 1 (si $): <input type="text" name="nb_recompense" size="2" value="<?php echo $donnees['nb_recompense']; ?>"><br />
		<b>Limites : </b><br />
		Limite inférieure: <input type="text" name="limite_inf" size="2" value="<?php echo $donnees['limite_inf']; ?>"><br />
		Limite suppérieure: <input type="text" name="limite_sup" size="2" value="<?php echo $donnees['limite_sup']; ?>"><br />
		
		<input type="hidden" name="genre" value="<?php echo $donnees['genre']; ?>">
		<input type="hidden" name="id" value="<?php echo $donnees['id']; ?>">	
		<input type="hidden" name="action" value="modifier">
		<input type="submit" value="Modifier" />
		</form>
		<br />
		<b>Image : </b>	<br />
		<div id="cadre_membres_carte">
		<img src="../images/quetes_secondaires/<?php echo $_POST['id']; ?>.jpg" height="150px" style="border:0;"/>
		</div>
		<FORM ENCTYPE="multipart/form-data" ACTION="pokemons_add_pnj.php" METHOD="POST"> 
		<INPUT TYPE="hidden" name="action" value="modifier_image">
		<INPUT TYPE="hidden" name="id" value="<?php echo $_POST['id'];?>">
		<b>Ajouter/Modifier l'image </b>(uniquement .jpg) : <br /><INPUT NAME="fichier" TYPE="file"> 
		<INPUT TYPE="submit" VALUE="Envoyer l'image"> 
		</FORM> <br /><br />
		<?php
		}
	if($_POST['genre']=="kill")
		{
		?>
		<form method="post" action="pokemons_add_pnj.php">
		<b>Description : </b>
		Position :<input type="text" name="pos_ver" size="2" value="<?php echo $donnees['pos_ver']; ?>"> / <input type="text" name="pos_hor" size="2" value="<?php echo $donnees['pos_hor']; ?>"><br />
		Nom : <input type="text" name="qui" size="20" value="<?php echo $donnees['qui']; ?>"><br />
		Points : <input type="text" name="difficulte" size="2" value="<?php echo $donnees['difficulte']; ?>"><br />
		Repetable (/heure) : <input type="text" name="repetitive" size="2" value="<?php echo $donnees['repetitive']; ?>"><br />
		Pokémon : <select name="what">
		<option value="">Aucun</option>
		<?php
		$reponse2 = $bdd->query('SELECT * FROM pokemons_base_pokemons ORDER BY id_pokedex') or die(print_r($bdd->errorInfo()));  
		while($donnees2 = $reponse2->fetch())
			{
			echo '<option value="'.$donnees2['nom'].'"'; if($donnees2['nom']==$donnees['what']){echo 'selected="selected"';} echo '">'.$donnees2['nom'].'</option>';
			}
		?>
		</select><br />
		Condition de réussite :<br />
		<select name="condition_reussite">
			<option value="">Aucun</option>
			<option value="shiney" <?php if($donnees['condition_reussite']=="shiney"){echo 'selected="selected"';}?>>Shiney</option>
			<option value="superieur" <?php if($donnees['condition_reussite']=="superieur"){echo 'selected="selected"';}?>>Niveau supérieur à...</option>
			<option value="inferieur" <?php if($donnees['condition_reussite']=="inferieur"){echo 'selected="selected"';}?>>Niveau inférieur à...</option>
		</select><br />
		Condition de réussite : suite (à complèter si la condition de réussite se termine par "..." <br />
		<input type="text" name="condition_reussite2" size="2" value="<?php echo $donnees['condition_reussite2']; ?>"><br />
		Nombre de kills : <input type="text" name="nb_reussite" size="2" value="<?php echo $donnees['nb_reussite']; ?>"><br />
		Intro : <br />
		<textarea name="intro" rows="4" cols="55"><?php echo $donnees['intro']; ?></textarea> <br />
		Réussi : <br />
		<textarea name="reussite" rows="4" cols="55"><?php echo $donnees['reussite']; ?></textarea> <br />
		Pas encore réussi mais activée : <br />
		<textarea name="pas_reussi" rows="4" cols="55"><?php echo $donnees['pas_reussi']; ?></textarea> <br />
		Complet (plus dispo cette heure-ci) : <br />
		<textarea name="complet" rows="4" cols="55"><?php echo $donnees['complet']; ?></textarea><br />
		<b>Récompense :</b><br />
		Type : <select name="genre_recompense">
			<option value="item" <?php if($donnees['genre_recompense']=="item"){echo 'checked="checked"';}?>>Item</option>
			<option value="argent" <?php if($donnees['genre_recompense']=="argent"){echo 'selected="selected"';}?>>pokédollars</option>
		</select><br />
		Nombre (si $)/ID (si item): <input type="text" name="recompense" size="2" value="<?php echo $donnees['recompense']; ?>"><br />
		Nombre (si item)/ 1 (si $): <input type="text" name="nb_recompense" size="2" value="<?php echo $donnees['nb_recompense']; ?>"><br />
		<b>Limites : </b><br />
		Limite inférieure: <input type="text" name="limite_inf" size="2" value="<?php echo $donnees['limite_inf']; ?>"><br />
		Limite suppérieure: <input type="text" name="limite_sup" size="2" value="<?php echo $donnees['limite_sup']; ?>"><br />
		
		<input type="hidden" name="genre" value="<?php echo $donnees['genre']; ?>">
		<input type="hidden" name="id" value="<?php echo $donnees['id']; ?>">	
		<input type="hidden" name="action" value="modifier">
		<input type="submit" value="Modifier" />
		</form>
		<br />
		<b>Image : </b>	<br />
		<div id="cadre_membres_carte">
		<img src="../images/quetes_secondaires/<?php echo $_POST['id']; ?>.jpg" height="150px" style="border:0;"/>
		</div>
		<FORM ENCTYPE="multipart/form-data" ACTION="pokemons_add_pnj.php" METHOD="POST"> 
		<INPUT TYPE="hidden" name="action" value="modifier_image">
		<INPUT TYPE="hidden" name="id" value="<?php echo $_POST['id'];?>">
		<b>Ajouter/Modifier l'image </b>(uniquement .jpg) : <br /><INPUT NAME="fichier" TYPE="file"> 
		<INPUT TYPE="submit" VALUE="Envoyer l'image"> 
		</FORM> <br /><br />
		<?php
		}
	if($_POST['genre']=="normal")
		{
		$reponse = $bdd->prepare('SELECT * FROM pokemons_pnj WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id'=>$_POST['id'])); 
		$donnees = $reponse->fetch();
		?>
		<form method="post" action="pokemons_add_pnj.php">
		<b>Description : </b>
		Position :<input type="text" name="pos_ver" size="2" value="<?php echo $donnees['pos_ver']; ?>"> / <input type="text" name="pos_hor" size="2" value="<?php echo $donnees['pos_hor']; ?>"><br />
		Nom : <input type="text" name="qui" size="20" value="<?php echo $donnees['qui']; ?>"><br />	
		Texte : <br />
		<textarea name="texte" rows="4" cols="55"><?php echo $donnees['texte']; ?></textarea> <br />
		<b>Limites : </b><br />
		Limite inférieure: <input type="text" name="limite_inf" size="2" value="<?php echo $donnees['limite_inf']; ?>"><br />
		Limite suppérieure: <input type="text" name="limite_sup" size="2" value="<?php echo $donnees['limite_sup']; ?>"><br />		
		ID quête principale (parler) : <input type="text" name="id_quete" size="20" value="<?php echo $donnees['id_quete']; ?>"><br />
                <input type="hidden" name="genre" value="<?php echo $_POST['genre']; ?>">
		<input type="hidden" name="id" value="<?php echo $donnees['id']; ?>">	
		<input type="hidden" name="action" value="modifier">
		<input type="submit" value="Modifier" />
		</form>
		<br />
		<b>Image : </b>	<br />
		<div id="cadre_membres_carte">
		<img src="../images/pnj/<?php echo $_POST['id']; ?>.jpg" height="150px" style="border:0;"/>
		</div>
		<FORM ENCTYPE="multipart/form-data" ACTION="pokemons_add_pnj.php" METHOD="POST"> 
		<INPUT TYPE="hidden" name="action" value="modifier_image">
		<input type="hidden" name="genre" value="<?php echo $_POST['genre']; ?>">
		<INPUT TYPE="hidden" name="id" value="<?php echo $_POST['id'];?>">
		<b>Ajouter/Modifier l'image </b>(uniquement .jpg) : <br /><INPUT NAME="fichier" TYPE="file"> 
		<INPUT TYPE="submit" VALUE="Envoyer l'image"> 
		</FORM> <br /><br />
		<?php
		}
	}
if($_POST['action']=="voir_pokemons")
	{
	$i=1;
	while($i<=6)
		{
		$reponse = $bdd->prepare('SELECT * FROM pokemons_liste_pokemons_pnj WHERE id_quete=:id_quete AND num_pokemon=:num_pokemon') or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id_quete'=>$_POST['id_quete'], 'num_pokemon'=>$i)); 
		$donnees = $reponse->fetch();
		?>
		<form method="post" action="pokemons_add_pnj.php">
		<b>Pokémon N° <?php echo $i;?></b><br />
		Pokémon : <select name="id_pokemon">
		<option value="0">Aucun</option>
		<?php
		$reponse2 = $bdd->query('SELECT * FROM pokemons_base_pokemons ORDER BY id_pokedex') or die(print_r($bdd->errorInfo()));  
		while($donnees2 = $reponse2->fetch())
			{
			echo '<option value="'.$donnees2['id'].'"'; if($donnees2['id']==$donnees['id_pokemon']){echo 'selected="selected"';} echo '">'.$donnees2['nom'].'</option>';
			}
		?>
		</select>
		<br />
		Shiney : <select name="shiney">
		<option value="0" <?php if($donnees['shiney']==0){echo 'selected="selected"';}?>>Non</option>
		<option value="1" <?php if($donnees['shiney']==1){echo 'selected="selected"';}?>>Oui</option>	
		</select><br />
		Sexe : <select name="sexe">
		<option value="M" <?php if($donnees['sexe']=="M"){echo 'selected="selected"';}?>>Male</option>
		<option value="F" <?php if($donnees['sexe']=="F"){echo 'selected="selected"';}?>>Femelle</option>	
		<option value="" <?php if($donnees['sexe']==""){echo 'selected="selected"';}?>>Aucun</option>	
		</select><br />
		Lvl : <input type="text" name="lvl" size="2" value="<?php echo $donnees['lvl']; ?>"><br />
		PV : <input type="text" name="pv" size="2" value="<?php echo $donnees['pv']; ?>"><br />
		PV maximum: <input type="text" name="pv_max" size="2" value="<?php echo $donnees['pv_max']; ?>"><br />
		attaque : <input type="text" name="att" size="2" value="<?php echo $donnees['att']; ?>"><br />
		défense : <input type="text" name="def" size="2" value="<?php echo $donnees['def']; ?>"><br />
		vitesse : <input type="text" name="vit" size="2" value="<?php echo $donnees['vit']; ?>"><br />
		attaque spéciale : <input type="text" name="attspe" size="2" value="<?php echo $donnees['attspe']; ?>"><br />
		defense spéciale : <input type="text" name="defspe" size="2" value="<?php echo $donnees['defspe']; ?>"><br />
		Attaque 1 :
		<select name="attaque1">
		<option value="0">Aucune</option>
		<?php
		$reponse3 = $bdd->query('SELECT * FROM pokemon_base_attaques') or die(print_r($bdd->errorInfo())); 
		while($donnees3 = $reponse3->fetch())
			{
			echo '<option value="'.$donnees3['id'].'"'; if($donnees3['id']==$donnees['attaque1']){echo 'selected="selected"';} echo '>'.$donnees3['nom'].'</option>';
			}
		?>
		</select><br />
		Attaque 2 :
		<select name="attaque2">
		<option value="0">Aucune</option>
		<?php
		$reponse3 = $bdd->query('SELECT * FROM pokemon_base_attaques') or die(print_r($bdd->errorInfo())); 
		while($donnees3 = $reponse3->fetch())
			{
			echo '<option value="'.$donnees3['id'].'"'; if($donnees3['id']==$donnees['attaque2']){echo 'selected="selected"';} echo '>'.$donnees3['nom'].'</option>';
			}
		?>
		</select><br />
		Attaque 3 :
		<select name="attaque3">
		<option value="0">Aucune</option>
		<?php
		$reponse3 = $bdd->query('SELECT * FROM pokemon_base_attaques') or die(print_r($bdd->errorInfo())); 
		while($donnees3 = $reponse3->fetch())
			{
			echo '<option value="'.$donnees3['id'].'"'; if($donnees3['id']==$donnees['attaque3']){echo 'selected="selected"';} echo '>'.$donnees3['nom'].'</option>';
			}
		?>
		</select><br />
		Attaque 4 :
		<select name="attaque4">
		<option value="0">Aucune</option>
		<?php
		$reponse3 = $bdd->query('SELECT * FROM pokemon_base_attaques') or die(print_r($bdd->errorInfo())); 
		while($donnees3 = $reponse3->fetch())
			{
			echo '<option value="'.$donnees3['id'].'"'; if($donnees3['id']==$donnees['attaque4']){echo 'selected="selected"';} echo '>'.$donnees3['nom'].'</option>';
			}
		?>
		</select><br />
		<INPUT TYPE="hidden" name="action" value="modifier_pokemons">
		<INPUT TYPE="hidden" name="id_quete" value="<?php echo $_POST['id_quete'];?>">
		<INPUT TYPE="hidden" name="num_pokemon" value="<?php echo $i;?>">
		<INPUT TYPE="submit" VALUE="<?php if($donnees['lvl']!=0){echo 'Modifier ce pokémon';}else{echo 'Ajouter ce pokémon';}?>">
		</form>
		<br /><br />
		<?php
		$i=$i+1;
		}
	}
?>		
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>	
   

 

<?php include ("bas.php"); ?>
