<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>

 
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
 

<?php
if($_SESSION['is_animateur'] == true)
{
?>

 
<?php

if(isset($_POST['titre']))
{
$req = $bdd->prepare('INSERT INTO pokemons_news (date_poste, titre, news) VALUES(now(), :titre, :news)') or die(print_r($bdd->errorInfo()));
$req->execute(array(
                    'titre' => stripslashes($_POST['titre']), 
					'news' => stripslashes($_POST['message']) 
					))or die(print_r($bdd->errorInfo()));
$req = $bdd->prepare('INSERT INTO pokemons_survey_admin (pseudo, action, quand, titre, texte) VALUES(:pseudo, "ajout d\'une news", now(), :titre, :texte)') or die(print_r($bdd->errorInfo()));
$req->execute(array(
                    'pseudo' => $_SESSION['pseudo'],
					'titre' => stripslashes($_POST['titre']), 
					'texte' => stripslashes($_POST['message']) 
					))or die(print_r($bdd->errorInfo()));
echo 'News bien ajoutée!';
}
else
{
?>	
<h2>Ajout d'un news </h2>
<p>Ce formulaire vous permet d'ajouter une news sur l'acceuil du site. Elle remplacera aussitôt l'ancienne news (les anciennes news sont toujours lisibles en suivant le lien adéquat).<br />
Evitez de créer trop de news. Une tous les 4 ou 5 jours semble un maximum. Au pire, tentez de les rassembler sur une seule news avec plusieurs éléments (quitte à citer des news précédentes).<br />
Veillez à avoir une orthographe correcte avant d'envoyer. Relisez-vous plusieurs fois. En cas d'erreur, envoyer mon un email (admin@pokemon-origins.com) en précisant le soucis pour que je le corrige. Vous ne savez pas modifier/supprimer une news vous-même.<br />
S'il s'agit d'une news importante, n'oubliez pas d'envoyer également un <a href="pokemons_mails.php">email aux joueurs</a>.<br />
<br />

           <form action="pokemons_news.php" method="post">                     	         
           <label for="titre">Titre</label> : <input type="text" name="titre" id="titre" /> <br />	          
           <textarea name="message" rows="8" cols="55">	Votre message ici.</textarea> <br />	   
            <input type="submit" value="envoyer" />           
            </form>
<?php 
}
?>

	
<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>  

 

<?php include ("bas.php"); ?>
