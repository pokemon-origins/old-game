<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>

 
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
    
Liste des pokémons G2 : nombre de cases sur lesquels on peut les trouver    
<br /><br />
    
<?php
$reponse = $bdd->query('SELECT id, nom, id_pokedex FROM pokemons_base_pokemons WHERE id_pokedex>151 ORDER BY id_pokedex ASC') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch()){
    $reponse2 = $bdd->prepare('SELECT COUNT(DISTINCT id_map) AS total FROM pokemons_map_proba WHERE id_pokemon=:id_pokemon AND monstre=0') or die(print_r($bdd->errorInfo()));
    $reponse2->execute(array('id_pokemon' => $donnees['id']));
    $donnees2 = $reponse2->fetch();
    
    echo $donnees['nom'].' : '.$donnees2['total'].' case(s)<br />';
}     
?>
<br /><br />    
    
Liste des pokémons G1 : nombre de cases sur lesquels on peut les trouver    
<br />
<br />
<?php
$reponse = $bdd->query('SELECT id, nom, id_pokedex FROM pokemons_base_pokemons WHERE id_pokedex<=151 ORDER BY id_pokedex ASC') or die(print_r($bdd->errorInfo()));
while($donnees = $reponse->fetch()){
    $reponse2 = $bdd->prepare('SELECT COUNT(DISTINCT id_map) AS total FROM pokemons_map_proba WHERE id_pokemon=:id_pokemon AND monstre=0') or die(print_r($bdd->errorInfo()));
    $reponse2->execute(array('id_pokemon' => $donnees['id']));
    $donnees2 = $reponse2->fetch();
    
    echo $donnees['nom'].' : '.$donnees2['total'].' case(s)<br />';
}     
?>   

<?php include ("bas.php"); ?>
