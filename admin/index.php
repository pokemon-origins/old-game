<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>

 
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
        
<h2> Bienvenue sur la plateforme d'administration </h2>
<p>Pour rappel, il vous faut être loggé en tant que joueur pour avoir accès au menu de gauche et pour administrer le site. <br />
Songez à lire les descriptions des différentes options qui vous sont proposées avant de les utiliser. <br />
Si vous ne comprennez pas exactement comment fonctionne une option. Faites des tests "sans grande conséquence" ou demandez! <br />
Chacune de vos actions est enregistrée, évitez donc d'abuser même si je ne suis pas du genre à jouer la gestapo ;-) </br />
</p>

<b>Pour rappel : </b>

<u>L'admin</u> <br />

<p>L'administrateur est là pour veillez à ce que tout se passe bien sur le jeu de façon général. <br />
Il corrige, répare et dédommage lorsqu'il en a la possibilité. La plateforme d'administration lui permet de modifier les données directement sur le jeu.<br />
Il a beaucoup plus d'accès par rapport aux données des joueurs, mais n'a pas la possibilité de créer des news ou d'envoyer une newsletter.<br />
</p>

<u>L'animateur</u> <br />

<p>Il crée des évents, organise des tournois et concours, les clôture le moment venu et veille à ce que les récompenses soient bien distribuées.<br />
Il a la possibilité de promouvoir ses actions via un emailing de masse (newsletter) et le création de news sur la page d'accueil.<br />
Une grosse partie de son travail se fait via le forum et le jeu. La plateforme de gestion est surtout un outil pour mettre en place un évent ou pour pouvoir récompenser les joueurs par la suite. <br />
La communication est donc un aspect important de ce job. <br />
Il ne s'occupe pas de tout ce qui est gestion et réparation de bug.<br />
</p>

<?php include ("bas.php"); ?>
