<?php ini_set('session.use_trans_sid', 0); session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<head>
<link rel="icon" type="image/png" href="/images/pokeball.png" />
<link rel="stylesheet" media="screen" type="text/css" title="sheikoh" href="../design/design_pokemons.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META name="description" content="Mmorpg sur les pokemons de la première génération">
<meta name="keywords" content="pokemons, mmorpg, attaque, pv, pokedex, shop, pokecentre, monstre, capture, elevage, combat, dressage, dresseur, collection, chen, sacha, ondine, pierre, badge, arêne, pokemon, battle">
<title>Pokemons Origins </title>
</head>
<?php	 
try {
	$bdd = new PDO('mysql:host=127.0.0.1;dbname=po','root',''); 

	$debug = false;
	if ($debug)
	{
		$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
		error_reporting(E_ALL);
		ini_set("display_errors", 1);
		ini_set("error_reporting", E_ALL);
	}
    else
    {
        ini_set("display_errors", 0);
    }
}
catch(PDOException $e){
	echo 'Erreur : '.$e->getMessage();
}	


//identification
$connection_error=0;
if($_POST['action']=="identification")
{
$reponse = $bdd->prepare('SELECT * FROM pokemons_membres WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
$reponse->execute(array(
                        'pseudo' => $_POST['pseudo']));
$donnees = $reponse->fetch();
if (htmlspecialchars($donnees['pseudo'])==$_POST['pseudo'] and htmlspecialchars($donnees['mdp'])==$_POST['mdp'])
{
$_SESSION['pseudo'] = $donnees['pseudo'];
$_SESSION['mail'] = $donnees['mail'];
$_SESSION['mdp'] = $donnees['mdp'];
$time = time();
$time_dead = time() +300;
$reponse = $bdd->prepare('UPDATE pokemons_membres SET connecte=1, quand=:quand, quand_dead=:quand_dead WHERE pseudo=:pseudo') or die(print_r($bdd->errorInfo()));
	          $reponse->execute(array('quand' => $time,'quand_dead' => $time_dead,'pseudo' => $_SESSION['pseudo']));
}
else
{
$connection_error=1;
}
}

$_SESSION['is_big_boss'] = false;
$_SESSION['is_admin'] = false;
$_SESSION['is_animateur'] = false;
$_SESSION['is_image_uploader'] = false;

if ($_SESSION['pseudo']=="admin")
{
  $_SESSION['is_big_boss'] = true;
}
if ($_SESSION['pseudo']=="admin"/* OR $_SESSION['pseudo']=="PUT LOGIN HERE" */)
{
  $_SESSION['is_admin'] = true;
}
if ($_SESSION['pseudo']=="admin" /* OR $_SESSION['pseudo']=="PUT LOGIN HERE" OR $_SESSION['pseudo']=="PUT LOGIN HERE" OR $_SESSION['pseudo']=="PUT LOGIN HERE" OR $_SESSION['pseudo']=="PUT LOGIN HERE" */)
{
  $_SESSION['is_animateur'] = true;
}
if ($_SESSION['pseudo']=="admin" /* OR $_SESSION['pseudo']=="PUT LOGIN HERE" OR $_SESSION['pseudo']=="PUT LOGIN HERE" */)
{
  $_SESSION['is_image_uploader'] = true;
}
  

?>
<body>

<div id="corps">
<div id="header"></div>
		

