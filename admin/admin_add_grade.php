<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
<?php
if($_SESSION['is_animateur'] == true)
{
?>
 
 
<h2> Ajout d'un grade </h2>
<p>Ce menu vous permet d'ajouter tous les grades qui ne sont pas encore dans le jeu.<br />
Ce menu vous permet également de visualiser tous les grades actuellement dans le jeu ainsi que les joueurs qui les possèdent.<br />
Pour ajouter un grade, il faut spécifier :<br/>
 - le format (maitre, simulation, simulation +)<br/>
 - le nombre de pokémons (1, 3 ou 6)<br/>
 - la limite de niveau (50 ou 100)<br/>
 - le type de pokémons (tous, acier, combat, dragon, eau, electrique, feu, glace, insecte, normal, plante, poison, psy, roche, sol, spectre, tenebre, vol)<br/>
 - le joueur qui recevra le grade<br/>
 - Le nom du grade (IMPORTANT) qui sera visible des joueurs<br/>
 - La description du grade (IMPORTANT) qui sera visible des joueurs<br/>
<br/>
Notez que vous ne pouvez pas ajouter un grade qui existe déjà.<br/>
<br/>
</p>

<?php
	if($_POST['action']=="add") //ajouter un grade
	{
		// Check data consistency
		if (($_POST['format'] >= 0 and $_POST['format'] <= 2)
		    and ($_POST['nombre'] == 1 or $_POST['nombre'] == 3 or $_POST['nombre'] == 6)
		    and ($_POST['limite'] == 50 or $_POST['limite'] == 100)
		    and ($_POST['type'] == "tous" or $_POST['type'] == "acier" or $_POST['type'] == "combat" or $_POST['type'] == "dragon" or $_POST['type'] == "eau" or $_POST['type'] == "electrique" or $_POST['type'] == "feu" or $_POST['type'] == "glace" or $_POST['type'] == "insecte" or $_POST['type'] == "normal" or $_POST['type'] == "plante" or $_POST['type'] == "poison" or $_POST['type'] == "psy" or $_POST['type'] == "roche" or $_POST['type'] == "sol" or $_POST['type'] == "spectre" or $_POST['type'] == "tenebre" or $_POST['type'] == "vol"))
		{
			// Be sure the grade is not already in the game
			$sql_req_count = "SELECT COUNT(*) AS nb_grades FROM pokemons_grade WHERE quand_fin=\"0000-00-00 00:00:00\" AND format=:format AND nombre=:nombre AND limite=:limite AND type=:type;";
			$req = $bdd->prepare($sql_req_count) or die(print_r($bdd->errorInfo()));
			$req->execute(array('format' => $_POST['format'], 'nombre' => $_POST['nombre'], 'limite' => $_POST['limite'], 'type' => $_POST['type']));
			if (intval($req->fetch()['nb_grades']) == 0)
			{
				// Add the grade
				$sql_add_grade = "INSERT INTO pokemons_grade (grade, description, format, nombre, limite, type, pseudo, quand, quand_fin) VALUES (:grade, :description, :format, :nombre, :limite, :type, :pseudo, now(), \"0000-00-00 00:00:00\");";
				$req = $bdd->prepare($sql_add_grade) or die(print_r($bdd->errorInfo()));
				$req->execute(array('grade' => $_POST['titre'], 'description' => $_POST['description'], 'format' => $_POST['format'], 'nombre' => $_POST['nombre'], 'limite' => $_POST['limite'], 'type' => $_POST['type'], 'pseudo' => $_POST['qui']));
				echo "<b>Le grade a été rajouté.</b><br/>";
			}
			else
			{
				echo "<b>Le grade que vous essayez d'ajouter existe déjà. Il est donc impossible de l'ajouter.</b><br/>";
			}
		}
		else
		{
			echo "<b>Le grade que vous essayez d'ajouter n'est pas valide. Verifiez le format/nombre/limte/type puis rééssayez.</b><br/>";
		}
	}
?>

<br/><br/>
<b>Ajouter un grade :</b><br/>

<form action="admin_add_grade.php" method="post">                     	         
	<input name="action" value="add" type="hidden"> 	
	Format du combat:
	<select name="format">
		<option value="0">Maitre</option>
		<option value="1">Simulation</option>
		<option value="2">Simulation+</option>
	</select><br/><br/>
	Nombre de pokémons:
	<select name="nombre">
		<option value="1">1</option>
		<option value="3">3</option>
		<option value="6">6</option>
	</select><br/><br/>
	Limite:
	<select name="limite">
		<option value="50">Max level 50</option>
		<option value="100">Max level 100</option>
	</select><br/><br/>
	Type des pokémons:
	<select name="type">
		<option value="tous">tous</option>
		<option value="acier">acier</option>
		<option value="combat">combat</option>
		<option value="dragon">dragon</option>
		<option value="eau">eau</option>
		<option value="electrique">electrique</option>
		<option value="feu">feu</option>
		<option value="glace">glace</option>
		<option value="insecte">insecte</option>
		<option value="normal">normal</option>
		<option value="plante">plante</option>
		<option value="poison">poison</option>
		<option value="psy">psy</option>
		<option value="roche">roche</option>
		<option value="sol">sol</option>
		<option value="spectre">spectre</option>
		<option value="tenebre">tenebre</option>
		<option value="vol">vol</option>
	</select><br/><br/>
	A qui offrir le grade:
	<select name="qui">
		<?php
		echo '<option value="admin">admin</option>';
		$reponse = $bdd->query('SELECT * FROM pokemons_membres ORDER BY pseudo') or die(print_r($bdd->errorInfo()));
		while($all_members = $reponse->fetch())
		{
			echo '<option value="'.$all_members['pseudo'].'">'.$all_members['pseudo'].'</option>';
		}
		?>
	</select><br /><br/>
	Titre du grade : <input type="text" name="titre" value=""><br/>
	(exemples: "Maître pokémon ténèbres", "Maître pokémon ténèbre S", "Maître pokémon ténèbre S+")<br/><br/>
	Description du grade : <input type="text" name="description" value=""><br/>
	(exemples: "format normal, 6 VS 6, uniquement type ténèbres", "simulation, 6 VS 6, uniquement type ténèbres", "simulation+, 6 VS 6, uniquement type ténèbres")<br/><br/>
	 
	<input value="Ajouter le grade" type="submit">           
</form>

<br/><br/>
<b>Les grades actuellement dans le jeu :</b><br/>	

<style type="text/css">
table {
  border-collapse: collapse;
}
table td {
  border: 1px solid black; 
}
table tr:first-child td {
  border-top: 0;
}
table tr td:first-child {
  border-left: 0;
}
table tr:last-child td {
  border-bottom: 0;
}
table tr td:last-child {
  border-right: 0;
}
</style>

<table style="border:1px solid black">
<tr><td><b>Titre du grade</b></td><td><b>Format</b></td><td><b>Nombre de pokémons</b></td><td><b>Limite</b></td><td><b>Type de pokémons</b></td><td><b>Joueur</b></td><td><b>Description</b></td></tr>
<?php
	$reponse = $bdd->query('SELECT * FROM pokemons_grade WHERE quand_fin="0000-00-00 00:00:00" ORDER BY type, grade') or die(print_r($bdd->errorInfo()));
	while($donnees = $reponse->fetch())
	{
		$format = "";
		if ($donnees['format'] == 0) { $format = "Maitre"; } else if ($donnees['format'] == 1) { $format = "Simulation"; } else { $format = "Simulation+"; }
		echo '<tr><td>'.$donnees['grade'].'</td><td>'.$format.'</td><td>'.$donnees['nombre'].'</td><td>'.$donnees['limite'].'</td><td>'.$donnees['type'].'</td><td>'.$donnees['pseudo'].'</td><td>'.$donnees['description'].'</td></tr>';
	}
?>	
</table>
	
	
<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>	
   
<?php include ("bas.php"); ?>
