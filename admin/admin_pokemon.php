<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>

<div id="text_contenu" style="margin-top:0px;">
<div id="text">



<?php
if($_SESSION['is_big_boss'] == true)
{
if($_POST['action']=="ct")
{
$req = $bdd->prepare('INSERT INTO pokemon_base_ct (num, id_attaque) VALUES(:num, :id_attaque)') or die(print_r($bdd->errorInfo()));
$req->execute(array(
					'num' => stripslashes($_POST['num']), 
					'id_attaque' => stripslashes($_POST['id_attaque']) 
					))or die(print_r($bdd->errorInfo()));
echo 'CT/CS bien ajoutée!';
}
if($_POST['action']=="effets")
{
$req = $bdd->prepare('INSERT INTO pokemon_base_effets (nom, nb_tour_min, nb_tour_max, description) VALUES(:nom, :nb_tour_min, :nb_tour_max, :description)') or die(print_r($bdd->errorInfo()));
$req->execute(array(
					'nom' => stripslashes($_POST['nom']), 
					'nb_tour_min' => stripslashes($_POST['nb_tour_min']),
					'nb_tour_max' => stripslashes($_POST['nb_tour_max']),
					'description' => stripslashes($_POST['description'])					
					))or die(print_r($bdd->errorInfo()));
echo 'effet bien ajoutée!';
}
if($_POST['action']=="attaques")
{
$req = $bdd->prepare('INSERT INTO pokemon_base_attaques (nom, type, puissance, prec, pp, cc, classe, priorite, esquive, cible, id_effet, id_effet2, proba, description) VALUES(:nom, :type, :puissance, :prec, :pp, :cc, :classe, :priorite, :esquive, :cible, :id_effet, :id_effet2, :proba, :description)') or die(print_r($bdd->errorInfo()));
$req->execute(array(
					'nom' => stripslashes($_POST['nom']),
					'type' => stripslashes($_POST['type']),
					'puissance' => stripslashes($_POST['puissance']),
					'prec' => stripslashes($_POST['prec']),
					'pp' => stripslashes($_POST['pp']),
					'cc' => stripslashes($_POST['cc']),
					'classe' => stripslashes($_POST['classe']),
					'priorite' => stripslashes($_POST['priorite']),
					'esquive' => stripslashes($_POST['esquive']),
					'cible' => stripslashes($_POST['cible']),
					'id_effet' => stripslashes($_POST['id_effet']),
					'id_effet2' => stripslashes($_POST['id_effet2']),
					'proba' => stripslashes($_POST['proba']),
					'description' => stripslashes($_POST['description'])
					))or die(print_r($bdd->errorInfo()));
echo 'Attaque bien ajoutée!';
}

?>	

<h2> Ajout d'une attaque </h2>
<form method="post" action="admin_pokemon.php">
      <table>
	    <tr>
          <td nowrap align="right"><p>Nom :</p></td>
          <td><input type="text" name="nom" size="30"></td>
        </tr>     
		<tr>
          <td nowrap align="right"><p>Type:</p></td>
          <td><select name="type">
		  <option value="acier">acier</option>
		  <option value="combat">combat</option>
		  <option value="dragon">dragon</option>
		  <option value="eau">eau</option>
		  <option value="electrique">electrique</option>
		  <option value="feu">feu</option>
		  <option value="glace">glace</option>
		  <option value="insecte">insecte</option>
		  <option value="normal">normal</option>
		  <option value="plante">plante</option>
		  <option value="poison">poison</option>
		  <option value="psy">psy</option>
		  <option value="roche">roche</option>
		  <option value="sol">sol</option>
		  <option value="spectre">spectre</option>
		   <option value="tenebre">tenebre</option>
		  <option value="vol">vol</option>
		  </select>
         </td>
        </tr>
		 <tr>
          <td nowrap align="right"><p>Puissance :</p></td>
          <td><input type="text" name="puissance" size="10"></td>
        </tr>   
		 <tr>
          <td nowrap align="right"><p>Précision :</p></td>
          <td><input type="text" name="prec" size="10"></td>
        </tr>   
		 <tr>
          <td nowrap align="right"><p>Point de pouvoir :</p></td>
          <td><input type="text" name="pp" size="10"></td>
        </tr> 
		<tr>
          <td nowrap align="right"><p>Coup critique:</p></td>
          <td><select name="cc">
		  <option value="1">normal</option>
		  <option value="0">inexistant</option>
		  <option value="2">élevé</option> 
		</select>
         </td>
        </tr>		  		
		<tr>
          <td nowrap align="right"><p>Classe:</p></td>
          <td><select name="classe">
		  <option value="speciale">spéciale</option>
		  <option value="physique">physique</option>
		  <option value="autre">autre</option> 
		</select>
         </td>
        </tr>		  
        <tr>
        <tr>
          <td nowrap align="right"><p>Priorité:</p></td>
          <td><select name="priorite">
		  <option value="0">Non</option>
		  <option value="1">Oui</option>
		</select>
         </td>
        </tr>	 
	  <tr>
          <td nowrap align="right"><p>Esquive:</p></td>
          <td><select name="esquive">
		   <option value="0">Non</option>
		  <option value="1">Oui</option>
		</select>
         </td>
        </tr>	
		<tr>
          <td nowrap align="right"><p>Cible:</p></td>
          <td><select name="cible">
		   <option value="1">Adversaire</option>
		  <option value="0">Soi</option>
		  <option value="2">Autre</option>
		  </select>
         </td>
        </tr>	
		<tr>
          <td nowrap align="right"><p>Effet:</p></td>
          <td><select name="id_effet">
		  <option value="0">Aucun</option>
		  <?php
		  $reponse = $bdd->query('SELECT * FROM pokemon_base_effets') or die(print_r($bdd->errorInfo()));
			  while($donnees = $reponse->fetch())
				{
				echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].' : '.$donnees['description'].'</option>';
				}
		  ?>
            </select>
         </td>
        </tr>
		<tr>
          <td nowrap align="right"><p>Effet 2:</p></td>
          <td><select name="id_effet2">
		  <option value="0">Aucun</option>
		  <?php
		  $reponse = $bdd->query('SELECT * FROM pokemon_base_effets') or die(print_r($bdd->errorInfo()));
			  while($donnees = $reponse->fetch())
				{
				echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].' : '.$donnees['description'].'</option>';
				}
		  ?>
            </select>
         </td>
        </tr>
		 <tr>
          <td nowrap align="right"><p>Probabilité :</p></td>
          <td><input type="text" name="proba" size="10"></td>
        </tr>   
		<tr>
          <td nowrap align="right"><p>Description :</p></td>
          <td><textarea name="description" cols="40" rows="5">description</textarea>
          </td>
        </tr>
      </table>
      <input type="hidden" name="action" value="attaques">
	   <input type="submit" value="Ajouter" />
    </form>

<h2> Ajout d'un effet </h2>
<form method="post" action="admin_pokemon.php">
      <table>
	    <tr>
          <td nowrap align="right"><p>Nom :</p></td>
          <td><input type="text" name="nom" size="30"></td>
        </tr>     
         <tr>
          <td nowrap align="right"><p>Nombre de tour min:</p></td>
          <td><input type="text" name="nb_tour_min" size="30"></td>
        </tr>  
		<tr>
          <td nowrap align="right"><p>Nombre de tour max :</p></td>
          <td><input type="text" name="nb_tour_max" size="30"></td>
        </tr>  
		<tr>
          <td nowrap align="right"><p>Description :</p></td>
          <td><textarea name="description" cols="40" rows="5">description</textarea>
          </td>
        </tr>
      </table>
      <input type="hidden" name="action" value="effets">
	   <input type="submit" value="Ajouter" />
    </form>
	
<h2> Ajout d'une CT/CS </h2>
<form method="post" action="admin_pokemon.php">
      <table>
	    <tr>
          <td nowrap align="right"><p>Num :</p></td>
          <td><input type="text" name="num" size="30"></td>
        </tr>     
        <tr>
          <td nowrap align="right"><p>Nom:</p></td>
          <td><select name="id_attaque">
		  <?php
		  $reponse = $bdd->query('SELECT * FROM pokemon_base_attaques') or die(print_r($bdd->errorInfo()));
			  while($donnees = $reponse->fetch())
				{
				echo '<option value="'.$donnees['id'].'">'.$donnees['nom'].'</option>';
				}
		  ?>
            </select>
         </td>
        </tr>
      </table>
      <input type="hidden" name="action" value="ct">
	   <input type="submit" value="Ajouter" />
    </form>
         








<?php 
}
?>
 <?php include("bas.php"); ?>
