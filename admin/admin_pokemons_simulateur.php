<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>
<div id="text_contenu" style="margin-top:0px;">
<div id="text"> 


<?php
if($_SESSION['is_animateur'] == true)
{
?>

<h2> Simulateur de pokémon </h2>

<p>
4 données vous sont renseigner sur les stats du pokémon simulé : <br />
<b>Minimum</b> : est le minimum qu'un pokémon d'un tel niveau peut avoir dans sa stat, s'il n'a vraiment pas de bol en montant de niveau.<br />
<b>Moyen</b> : représente les statistiques d'un pokémon d'un tel niveau ayant eu une chance moyenne dans ses montées de lvl.<br />
<b>Maximum</b> : est le maximum qu'un pokémon d'un tel niveau peut avoir dans sa stat, s'il a vraiment eu du bol en montant de niveau.<br />
<b>Max race</b> : est le maximum qu'un pokémon d'une tel race peut avoir dans ses stats, même en s'entrainant après le lvl 100.<br />
</p>


<b>Quel pokémon souhaitez-vous simuler?</b><br />
<form method="post" action="admin_pokemons_simulateur.php">
<select name="id_pokedex">
<?php
$reponse = $bdd->query('SELECT * FROM pokemons_base_pokemons ORDER BY id_pokedex') or die(print_r($bdd->errorInfo()));  
while($donnees = $reponse->fetch())
	{
	echo '<option value="'.$donnees['id_pokedex'].'">'.$donnees['nom'].'</option>';
	}
?>
</select>
<br />
Lvl :<input type="text" name="lvl" size="4"><br />	
<input type="hidden" name="action" value="simuler">
<input type="submit" value="Simuler" />
</form>


<?php //SIMULATION
if($_POST['action']=="simuler")
	{
	$reponse = $bdd->prepare('SELECT * FROM pokemons_base_pokemons WHERE id_pokedex=:id_pokedex') or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id_pokedex' => $_POST['id_pokedex']));
	$donnees = $reponse->fetch();
	$id_pokemon=$donnees['id'];
	$nom=$donnees['nom'];
	$ajout=ceil($_POST['lvl']/2);
	$lvl=$_POST['lvl'];$lvl=$lvl-1;
	$pv_min=$donnees['pv']+$lvl*$donnees['bonus_pv'];
	$att_min=$donnees['att']+$lvl*$donnees['bonus_att'];
	$def_min=$donnees['def']+$lvl*$donnees['bonus_def'];
	$vit_min=$donnees['vit']+$lvl*$donnees['bonus_vit'];
	$attspe_min=$donnees['attspe']+$lvl*$donnees['bonus_attspe'];
	$defspe_min=$donnees['defspe']+$lvl*$donnees['bonus_defspe'];
	$pv_moy=$donnees['pv']+$lvl*$donnees['bonus_pv']+$ajout;
	$att_moy=$donnees['att']+$lvl*$donnees['bonus_att']+$ajout;
	$def_moy=$donnees['def']+$lvl*$donnees['bonus_def']+$ajout;
	$vit_moy=$donnees['vit']+$lvl*$donnees['bonus_vit']+$ajout;
	$attspe_moy=$donnees['attspe']+$lvl*$donnees['bonus_attspe']+$ajout;
	$defspe_moy=$donnees['defspe']+$lvl*$donnees['bonus_defspe']+$ajout;
	$pv_max=$donnees['pv']+$lvl*$donnees['bonus_pv']+$lvl;
	$att_max=$donnees['att']+$lvl*$donnees['bonus_att']+$lvl;
	$def_max=$donnees['def']+$lvl*$donnees['bonus_def']+$lvl;
	$vit_max=$donnees['vit']+$lvl*$donnees['bonus_vit']+$lvl;
	$attspe_max=$donnees['attspe']+$lvl*$donnees['bonus_attspe']+$lvl;
	$defspe_max=$donnees['defspe']+$lvl*$donnees['bonus_defspe']+$lvl;
	$max_pv=$donnees['pv']+$donnees['bonus_pv']*150+300;
	$max_att=$donnees['att']+$donnees['bonus_att']*150+200;
	$max_def=$donnees['def']+$donnees['bonus_def']*150+200;
	$max_vit=$donnees['vit']+$donnees['bonus_vit']*150+200;
	$max_attspe=$donnees['attspe']+$donnees['bonus_attspe']*150+200;
	$max_defspe=$donnees['defspe']+$donnees['bonus_defspe']*150+200;
	?>
	<table id="profil" width="533px" cellpadding="2" cellspacing="2" style="text-align:center;" >
	<colgroup><COL WIDTH=20%><COL WIDTH=20%><COL WIDTH=20%><COL WIDTH=20%><COL WIDTH=20%></COLGROUP>
	<tr><th colspan="9"><?php echo $nom.' lvl '.$_POST['lvl'];?></th></tr>
	<tr><td><b> Stats </b></td><td><b>Minimum</b></td><td><b>Moyen </b></td><td><b>Maximum </b></td><td><b>Max race </b></td></tr>
	<tr><td>PV</td><td><?php echo $pv_min;?></td><td><?php echo $pv_moy;?></td><td><?php echo $pv_max;?></td><td><?php echo $max_pv;?></td></tr>
	<tr><td>Attaque</td><td><?php echo $att_min;?></td><td><?php echo $att_moy;?></td><td><?php echo $att_max;?></td><td><?php echo $max_att;?></td></tr>
	<tr><td>Défense</td><td><?php echo $def_min;?></td><td><?php echo $def_moy;?></td><td><?php echo $def_max;?></td><td><?php echo $max_def;?></td></tr>
	<tr><td>Vitesse</td><td><?php echo $vit_min;?></td><td><?php echo $vit_moy;?></td><td><?php echo $vit_max;?></td><td><?php echo $max_vit;?></td></tr>
	<tr><td>Attaque spéciale</td><td><?php echo $attspe_min;?></td><td><?php echo $attspe_moy;?></td><td><?php echo $attspe_max;?></td><td><?php echo $max_attspe;?></td></tr>
	<tr><td>Défense spéciale</td><td><?php echo $defspe_min;?></td><td><?php echo $defspe_moy;?></td><td><?php echo $defspe_max;?></td><td><?php echo $max_defspe;?></td></tr>
	</table>
	<?php
	}
?>




<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>


 <?php include("bas.php"); ?>
