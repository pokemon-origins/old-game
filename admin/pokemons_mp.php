<?php include("banniere.php"); ?>
<?php include("menu.php"); ?>

 
<div id="text_contenu" style="margin-top:0px;">
<div id="text">
 

<?php
if($_SESSION['is_animateur'] == true)
{
?>
 
<?php

if($_POST['action']=="envoyer")
	{
	$req = $bdd->prepare('INSERT INTO pokemons_survey_admin (pseudo, action, quand, titre, texte) VALUES(:pseudo, "envoie d\'un mp à tous", now(), :titre, :texte)') or die(print_r($bdd->errorInfo()));
	$req->execute(array(
                    'pseudo' => $_SESSION['pseudo'],
					'titre' => stripslashes($_POST['titre']), 
					'texte' => stripslashes($_POST['message']) 
					))or die(print_r($bdd->errorInfo()));
	$message=(stripslashes($_POST['message']));
	$titre=(stripslashes($_POST['titre']));
	$reponse = $bdd->query('SELECT pseudo FROM pokemons_membres') or die(print_r($bdd->errorInfo()));
	while($donnees = $reponse->fetch())
		{
		$req = $bdd->prepare('INSERT INTO pokemons_mails (expediteur, destinataire, statut, titre, message, quand) VALUES("admin", :destinataire, "non lu", :titre, :message, now())') or die(print_r($bdd->errorInfo()));
						$req->execute(array(
						'destinataire' => $donnees['pseudo'],					
						'titre' => $titre,
						'message' => $message
		))or die(print_r($bdd->errorInfo()));	
		}
	echo 'Message envoyé!<br />';
	}
else
	{
?>
<h2> Envoie d'un mp aux joueurs </h2>
<p>Ce formulaire vous permet d'envoyer un message privé à chaque joueur. C'est fort utile pour prévenir lors du lancement d'un évent ou d'un concours important par exemple. <br />
Dès que vous avez cliqué sur "envoi", les mp partent. Vous ne pouvez donc plus les modifier!! Vérifiez donc bien tout avant de cliquer (message clair, complet, orthographe, signature).<br />
le script va alors envoyer plus de 2000 mp. Ne vous étonnez pas si la page met un peu de temps à charger. Il ne faut pas la couper, sous peine de ne voir que certains joueurs déservis et d'autres pas.<br />
</p>
	<form method="post" action="pokemons_mp.php">
		<table border="0" cellspacing="2">
			<tr><td valign="top"><p>Titre :</p></td><td><input name="titre" type="text" size="72"></td></tr>
			<tr><td valign="top"><p>Message : </p></td><td><textarea name="message" cols="60" rows="15"></textarea></td></tr>
			<tr><td><input name="action" type="hidden" value="envoyer"></td>
				<td><input name="envoi" type="submit" value="Envoyer"></td></tr>
		</table>
	</form>
	
<?php
	}
?>

<?php
}
else
{
echo 'Vous devez être loggé sur le jeu pour accéder à l\'administration.';
}
?>	
   

 

<?php include ("bas.php"); ?>
